//
//  CTSliderView.h
//  CameraGPUImage
//
//  Created by BaoNQ on 6/27/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageView.h"
#import "UXOverlayView.h"

@protocol HorizontalImageViewDataSource;

@interface HorizontalImageView : UIView <UIScrollViewDelegate>

@property UIScrollView *slider;
@property NSMutableArray<ImageView *> *pages;
@property (readonly) BOOL isDisabledResponder;
@property NSInteger numberOfImages;
@property NSInteger curImageIndex;

/* Data source */
@property(nonatomic, weak) id<HorizontalImageViewDataSource> dataSource;

/**
 *  Reload data in HorizontalImageView
 */
- (void)reloadData;

- (void)disableReceiveResponder;

- (void)enableReceiveResponder;

- (void)moveToPosition:(NSInteger)nextIndex;

@end


@protocol HorizontalImageViewDataSource <NSObject>

/**
 *  Return number of images in image list
 */
- (NSInteger)numberOfImages:(HorizontalImageView *)horizontalImageView;

/**
 *  Return image for page view at a given index
 */
- (UIImage *)imageInHorizontalImageView:(HorizontalImageView *)horizontalImageView atIndex:(NSInteger)index;

@end

