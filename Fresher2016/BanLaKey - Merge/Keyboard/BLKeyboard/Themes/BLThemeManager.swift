//
//  BLThemeManager.swift
//  BanLaKey
//
//  Created by BaoNQ on 8/17/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

class BLThemeManager: NSObject {
    
    static var currentTheme: BLTheme {
        get {
            return self.colorfulTheme()
        }
        set {
            self.currentTheme = newValue
        }
    }
    
    static func lightAppearanceTheme() -> BLTheme {
        struct StaticVar {
            static var lightTheme: BLTheme = BLTheme()
            static var onceToken: dispatch_once_t = 0
        }
        
        dispatch_once(&StaticVar.onceToken, {
            StaticVar.lightTheme.backgroundColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
            StaticVar.lightTheme.backgroundImage = "b1.jpg"
            
            StaticVar.lightTheme.buttonTitleFont = "Avenir"
            StaticVar.lightTheme.buttonBorderColor = UIColor.blackColor()
            
            StaticVar.lightTheme.specialButtonTitleColor = UIColor.whiteColor()
            StaticVar.lightTheme.characterButtonTitleColor = UIColor.blackColor()
            
            StaticVar.lightTheme.specialButtonHighLightedTitleColor = UIColor.whiteColor()
            StaticVar.lightTheme.characterButtonHighLightedTitleColor = UIColor.blackColor()
            
            
            StaticVar.lightTheme.characterButtonShadowColor = UIColor.init(red: 139/255.0, green: 142/255.0, blue: 146/255.0, alpha: 1.0)
            StaticVar.lightTheme.specialButtonShadowColor = UIColor.grayColor()
            
            StaticVar.lightTheme.specialButtonHighlightedColor = UIColor.grayColor()
            StaticVar.lightTheme.characterButtonHightlightedColor = UIColor.grayColor()
            
            StaticVar.lightTheme.useSameColorForSpecialButtonHighlighted = false
            StaticVar.lightTheme.useSameColorForCharacterButtonHighlighted = false
            
            StaticVar.lightTheme.buttonShadowYOffset = 1.0
            StaticVar.lightTheme.buttonLabelYOffset = -1.5
            StaticVar.lightTheme.buttonImageYOffset = -0.5
            
            StaticVar.lightTheme.buttonPhoneBorderWidth = 1.0
            StaticVar.lightTheme.buttonPhoneShadowWidth = 1.0
            StaticVar.lightTheme.buttonPhoneCornerRadius = 4.0
            StaticVar.lightTheme.buttonPhonePopupCornerRadius = 9.0
            StaticVar.lightTheme.buttonPhonePortraitFontSize = 19.0
            StaticVar.lightTheme.buttonPhoneLandscapeFontSize = 22.0
            StaticVar.lightTheme.buttonPhonePopupPortraitFontSize = 28.0
            StaticVar.lightTheme.buttonPhonePopupLandscapeFontSize = 31.0
            
            
            StaticVar.lightTheme.buttonPadBorderWidth = 1.0
            StaticVar.lightTheme.buttonPadShadowWidth = 1.5
            StaticVar.lightTheme.buttonPadCornerRadius = 5.0
            StaticVar.lightTheme.buttonPadPopupCornerRadius = 11.0
            StaticVar.lightTheme.buttonPadPortraitFontSize = 25.0
            StaticVar.lightTheme.buttonPadLandscapeFontSize = 29.0
            StaticVar.lightTheme.buttonPadPopupPortraitFontSize = 35.0
            StaticVar.lightTheme.buttonPadPopupLandscapeFontSize = 38.0
            
            let lightColor = UIColor(white: 254/255.0, alpha: 0.5)
            let darkColor = UIColor(white: 85/255.0, alpha: 0.5)

            // Butons color
            // 1st row
            StaticVar.lightTheme.QButtonColor = lightColor
            StaticVar.lightTheme.WButton = lightColor
            StaticVar.lightTheme.EButton = lightColor
            StaticVar.lightTheme.RButton = lightColor
            StaticVar.lightTheme.TButton = lightColor
            StaticVar.lightTheme.YButton = lightColor
            StaticVar.lightTheme.UButton = lightColor
            StaticVar.lightTheme.IButton = lightColor
            StaticVar.lightTheme.OButton = lightColor
            StaticVar.lightTheme.PButton = lightColor
            // 2nd row
            StaticVar.lightTheme.AButton = lightColor
            StaticVar.lightTheme.SButton = lightColor
            StaticVar.lightTheme.DButton = lightColor
            StaticVar.lightTheme.FButton = lightColor
            StaticVar.lightTheme.GButton = lightColor
            StaticVar.lightTheme.HButton = lightColor
            StaticVar.lightTheme.JButton = lightColor
            StaticVar.lightTheme.KButton = lightColor
            StaticVar.lightTheme.LButton = lightColor
            // 3rd row
            StaticVar.lightTheme.ZButton = lightColor
            StaticVar.lightTheme.XButton = lightColor
            StaticVar.lightTheme.CButton = lightColor
            StaticVar.lightTheme.VButton = lightColor
            StaticVar.lightTheme.BButton = lightColor
            StaticVar.lightTheme.NButton = lightColor
            StaticVar.lightTheme.MButton = lightColor

            StaticVar.lightTheme.shiftButton = darkColor
            StaticVar.lightTheme.deleteButton = darkColor
            StaticVar.lightTheme.globeButton = darkColor
            StaticVar.lightTheme.spaceButton = darkColor
            StaticVar.lightTheme.returnButton = darkColor
            StaticVar.lightTheme.keyModeButton = darkColor

            StaticVar.lightTheme.shiftDefaultButtonImage = "shift.png"
            StaticVar.lightTheme.shiftOnceButtonImage = "shift_lock.png"
            StaticVar.lightTheme.shiftForeverButtonImage = "shift_lock.png"
            
            StaticVar.lightTheme.deleteButtonImage = "delete.png"
            StaticVar.lightTheme.deleteButtonHighlightedImage = "delete.png"
            
            StaticVar.lightTheme.globeButtonImage = "globe_white.png"
            StaticVar.lightTheme.globeButtonHighlightedImage = "globe_white.png"

            StaticVar.lightTheme.spaceButtonImage = ""
            StaticVar.lightTheme.spaceButtonHighlightedImage = ""

            StaticVar.lightTheme.returnButtonImage = ""
            StaticVar.lightTheme.returnButtonHighlightedImage = ""

            StaticVar.lightTheme.keyModeButtonImage = ""
            StaticVar.lightTheme.keyModeButtonHighlightedImage = ""

            StaticVar.lightTheme.scribbleLineColor = UIColor.init(red: 104/255.0, green: 163/255.0, blue: 1, alpha: 1)
            
            // Number keys
            StaticVar.lightTheme.zeroButton = lightColor
            StaticVar.lightTheme.oneButton = lightColor
            StaticVar.lightTheme.twoButton = lightColor
            StaticVar.lightTheme.threeButton = lightColor
            StaticVar.lightTheme.fourButton = lightColor
            StaticVar.lightTheme.fiveButton = lightColor
            StaticVar.lightTheme.sixButton = lightColor
            StaticVar.lightTheme.sevenButton = lightColor
            StaticVar.lightTheme.eightButton = lightColor
            StaticVar.lightTheme.nineButton = lightColor
            
            StaticVar.lightTheme.minusButton = lightColor
            StaticVar.lightTheme.slashButton = lightColor
            StaticVar.lightTheme.colonButton = lightColor
            StaticVar.lightTheme.semicolonButton = lightColor
            StaticVar.lightTheme.leftParenthesisButton = lightColor
            StaticVar.lightTheme.rightParenthesisButton = lightColor
            StaticVar.lightTheme.dollarSignButton = lightColor
            StaticVar.lightTheme.ampersandButton = lightColor
            StaticVar.lightTheme.atSignButton = lightColor
            StaticVar.lightTheme.quotationMarkButton = lightColor
            StaticVar.lightTheme.fullStopButton = lightColor
            StaticVar.lightTheme.commaButton = lightColor
            StaticVar.lightTheme.questionMarkButton = lightColor
            StaticVar.lightTheme.exclamationMarkButton = lightColor
            StaticVar.lightTheme.apostropheButton = lightColor
            StaticVar.lightTheme.graveAccentButton = lightColor
            
            
            // Number keys page 2
            StaticVar.lightTheme.leftSquareBracketButton = lightColor
            StaticVar.lightTheme.rightSquareBracketButton = lightColor
            StaticVar.lightTheme.leftCurlyBracketButton = lightColor
            StaticVar.lightTheme.rightCurlyBracketButton = lightColor
            StaticVar.lightTheme.sharpButton = lightColor
            StaticVar.lightTheme.percentSignButton = lightColor
            StaticVar.lightTheme.circumflexAccentButton = lightColor
            StaticVar.lightTheme.asteriskButton = lightColor
            StaticVar.lightTheme.plusSignButton = lightColor
            StaticVar.lightTheme.equalSignButton = lightColor
            StaticVar.lightTheme.lowLineButton = lightColor
            StaticVar.lightTheme.backslashButton = lightColor
            StaticVar.lightTheme.verticalBarButton = lightColor
            StaticVar.lightTheme.tildeButton = lightColor
            StaticVar.lightTheme.lessThanSignButton = lightColor
            StaticVar.lightTheme.greaterThanSignButton = lightColor
            StaticVar.lightTheme.euroSignButton = lightColor
            StaticVar.lightTheme.poundSignButton = lightColor
            StaticVar.lightTheme.yenSignButton = lightColor
            StaticVar.lightTheme.centSignButton = lightColor
            
            StaticVar.lightTheme.blackCircleButton = lightColor
            StaticVar.lightTheme.registerdSignButton = lightColor
            StaticVar.lightTheme.copyrightSignButton = lightColor
            StaticVar.lightTheme.degreeCelsiusButton = lightColor
            StaticVar.lightTheme.degreeFahrenheitButton = lightColor
            
            StaticVar.lightTheme.settingIconPath = "setting_light_portrait"
            StaticVar.lightTheme.giphyIconPath = "gif_portrait"
            StaticVar.lightTheme.emojiIconPath = "emoji_portrait"
            StaticVar.lightTheme.upIconPath = "up_arrow_light_portrait"
            StaticVar.lightTheme.downIconPath = "down_arrow_light_portrait"
            
            StaticVar.lightTheme.happyGiphyIconPath =  "Happy_light"
            StaticVar.lightTheme.happyFilledGiphyIconPath = "Happy Filled_light"
            StaticVar.lightTheme.homeGiphyIconPath = "Home_light"
            StaticVar.lightTheme.homeFilledGiphyIconPath = "Home Filled_light"
            StaticVar.lightTheme.lineChartGiphyIconPath = "Line Chart_light"
            StaticVar.lightTheme.lineChartFilledGiphyIconPath = "Line Chart Filled_light"
            StaticVar.lightTheme.timeGiphyIconPath = "Time_light"
            StaticVar.lightTheme.timeFilledGiphyIconPath = "Time Filled_light"
            StaticVar.lightTheme.deleteFilledGiphyIconPath = "Delete Filled_light"
        })

        return StaticVar.lightTheme
    }
    
    static func darkTheme() -> BLTheme {
        struct StaticVar {
            static var darkTheme: BLTheme = BLTheme()
            static var onceToken: dispatch_once_t = 0
        }
        
        dispatch_once(&StaticVar.onceToken, {
            StaticVar.darkTheme.backgroundColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
            StaticVar.darkTheme.backgroundImage = "b3.jpg"
            
            StaticVar.darkTheme.buttonTitleFont = "Avenir"
            StaticVar.darkTheme.buttonBorderColor = UIColor.blackColor()
            
            StaticVar.darkTheme.specialButtonTitleColor = UIColor.whiteColor()
            StaticVar.darkTheme.characterButtonTitleColor = UIColor.blackColor()
            
            StaticVar.darkTheme.specialButtonHighLightedTitleColor = UIColor.whiteColor()
            StaticVar.darkTheme.characterButtonHighLightedTitleColor = UIColor.blackColor()
            
            
            StaticVar.darkTheme.characterButtonShadowColor = UIColor.init(red: 139/255.0, green: 142/255.0, blue: 146/255.0, alpha: 1.0)
            StaticVar.darkTheme.specialButtonShadowColor = UIColor.grayColor()
            
            StaticVar.darkTheme.specialButtonHighlightedColor = UIColor.grayColor()
            StaticVar.darkTheme.characterButtonHightlightedColor = UIColor.grayColor()
            
            StaticVar.darkTheme.useSameColorForSpecialButtonHighlighted = false
            StaticVar.darkTheme.useSameColorForCharacterButtonHighlighted = false
            
            StaticVar.darkTheme.buttonShadowYOffset = 1.0
            StaticVar.darkTheme.buttonLabelYOffset = -1.5
            StaticVar.darkTheme.buttonImageYOffset = -0.5
            
            StaticVar.darkTheme.buttonPhoneBorderWidth = 1.0
            StaticVar.darkTheme.buttonPhoneShadowWidth = 1.0
            StaticVar.darkTheme.buttonPhoneCornerRadius = 4.0
            StaticVar.darkTheme.buttonPhonePopupCornerRadius = 9.0
            StaticVar.darkTheme.buttonPhonePortraitFontSize = 19.0
            StaticVar.darkTheme.buttonPhoneLandscapeFontSize = 22.0
            StaticVar.darkTheme.buttonPhonePopupPortraitFontSize = 28.0
            StaticVar.darkTheme.buttonPhonePopupLandscapeFontSize = 31.0
            
            
            StaticVar.darkTheme.buttonPadBorderWidth = 1.0
            StaticVar.darkTheme.buttonPadShadowWidth = 1.5
            StaticVar.darkTheme.buttonPadCornerRadius = 5.0
            StaticVar.darkTheme.buttonPadPopupCornerRadius = 11.0
            StaticVar.darkTheme.buttonPadPortraitFontSize = 25.0
            StaticVar.darkTheme.buttonPadLandscapeFontSize = 29.0
            StaticVar.darkTheme.buttonPadPopupPortraitFontSize = 35.0
            StaticVar.darkTheme.buttonPadPopupLandscapeFontSize = 38.0
            
            let lightColor = UIColor(white: 254/255.0, alpha: 0.25)
            let darkColor = UIColor(white: 254/255.0, alpha: 0.35)
            
            // Butons color
            // 1st row
            StaticVar.darkTheme.QButtonColor = lightColor
            StaticVar.darkTheme.WButton = lightColor
            StaticVar.darkTheme.EButton = lightColor
            StaticVar.darkTheme.RButton = lightColor
            StaticVar.darkTheme.TButton = lightColor
            StaticVar.darkTheme.YButton = lightColor
            StaticVar.darkTheme.UButton = lightColor
            StaticVar.darkTheme.IButton = lightColor
            StaticVar.darkTheme.OButton = lightColor
            StaticVar.darkTheme.PButton = lightColor
            // 2nd row
            StaticVar.darkTheme.AButton = lightColor
            StaticVar.darkTheme.SButton = lightColor
            StaticVar.darkTheme.DButton = lightColor
            StaticVar.darkTheme.FButton = lightColor
            StaticVar.darkTheme.GButton = lightColor
            StaticVar.darkTheme.HButton = lightColor
            StaticVar.darkTheme.JButton = lightColor
            StaticVar.darkTheme.KButton = lightColor
            StaticVar.darkTheme.LButton = lightColor
            // 3rd row
            StaticVar.darkTheme.ZButton = lightColor
            StaticVar.darkTheme.XButton = lightColor
            StaticVar.darkTheme.CButton = lightColor
            StaticVar.darkTheme.VButton = lightColor
            StaticVar.darkTheme.BButton = lightColor
            StaticVar.darkTheme.NButton = lightColor
            StaticVar.darkTheme.MButton = lightColor
            
            StaticVar.darkTheme.shiftButton = darkColor
            StaticVar.darkTheme.deleteButton = darkColor
            StaticVar.darkTheme.globeButton = darkColor
            StaticVar.darkTheme.spaceButton = darkColor
            StaticVar.darkTheme.returnButton = darkColor
            StaticVar.darkTheme.keyModeButton = darkColor
            
            StaticVar.darkTheme.shiftDefaultButtonImage = "shift.png"
            StaticVar.darkTheme.shiftOnceButtonImage = "shift_lock.png"
            StaticVar.darkTheme.shiftForeverButtonImage = "shift_lock.png"
            
            StaticVar.darkTheme.deleteButtonImage = "delete.png"
            StaticVar.darkTheme.deleteButtonHighlightedImage = "delete.png"
            
            StaticVar.darkTheme.globeButtonImage = "globe_white.png"
            StaticVar.darkTheme.globeButtonHighlightedImage = "globe_white.png"
            
            StaticVar.darkTheme.spaceButtonImage = ""
            StaticVar.darkTheme.spaceButtonHighlightedImage = ""
            
            StaticVar.darkTheme.returnButtonImage = ""
            StaticVar.darkTheme.returnButtonHighlightedImage = ""
            
            StaticVar.darkTheme.keyModeButtonImage = ""
            StaticVar.darkTheme.keyModeButtonHighlightedImage = ""
            
            StaticVar.darkTheme.scribbleLineColor = UIColor.init(red: 47/255.0, green: 69/255.0, blue: 93/255.0, alpha: 1)
            
            // Number keys
            StaticVar.darkTheme.zeroButton = lightColor
            StaticVar.darkTheme.oneButton = lightColor
            StaticVar.darkTheme.twoButton = lightColor
            StaticVar.darkTheme.threeButton = lightColor
            StaticVar.darkTheme.fourButton = lightColor
            StaticVar.darkTheme.fiveButton = lightColor
            StaticVar.darkTheme.sixButton = lightColor
            StaticVar.darkTheme.sevenButton = lightColor
            StaticVar.darkTheme.eightButton = lightColor
            StaticVar.darkTheme.nineButton = lightColor
            
            StaticVar.darkTheme.minusButton = lightColor
            StaticVar.darkTheme.slashButton = lightColor
            StaticVar.darkTheme.colonButton = lightColor
            StaticVar.darkTheme.semicolonButton = lightColor
            StaticVar.darkTheme.leftParenthesisButton = lightColor
            StaticVar.darkTheme.rightParenthesisButton = lightColor
            StaticVar.darkTheme.dollarSignButton = lightColor
            StaticVar.darkTheme.ampersandButton = lightColor
            StaticVar.darkTheme.atSignButton = lightColor
            StaticVar.darkTheme.quotationMarkButton = lightColor
            StaticVar.darkTheme.fullStopButton = lightColor
            StaticVar.darkTheme.commaButton = lightColor
            StaticVar.darkTheme.questionMarkButton = lightColor
            StaticVar.darkTheme.exclamationMarkButton = lightColor
            StaticVar.darkTheme.apostropheButton = lightColor
            StaticVar.darkTheme.graveAccentButton = lightColor
            
            
            // Number keys page 2
            StaticVar.darkTheme.leftSquareBracketButton = lightColor
            StaticVar.darkTheme.rightSquareBracketButton = lightColor
            StaticVar.darkTheme.leftCurlyBracketButton = lightColor
            StaticVar.darkTheme.rightCurlyBracketButton = lightColor
            StaticVar.darkTheme.sharpButton = lightColor
            StaticVar.darkTheme.percentSignButton = lightColor
            StaticVar.darkTheme.circumflexAccentButton = lightColor
            StaticVar.darkTheme.asteriskButton = lightColor
            StaticVar.darkTheme.plusSignButton = lightColor
            StaticVar.darkTheme.equalSignButton = lightColor
            StaticVar.darkTheme.lowLineButton = lightColor
            StaticVar.darkTheme.backslashButton = lightColor
            StaticVar.darkTheme.verticalBarButton = lightColor
            StaticVar.darkTheme.tildeButton = lightColor
            StaticVar.darkTheme.lessThanSignButton = lightColor
            StaticVar.darkTheme.greaterThanSignButton = lightColor
            StaticVar.darkTheme.euroSignButton = lightColor
            StaticVar.darkTheme.poundSignButton = lightColor
            StaticVar.darkTheme.yenSignButton = lightColor
            StaticVar.darkTheme.centSignButton = lightColor
            
            StaticVar.darkTheme.blackCircleButton = lightColor
            StaticVar.darkTheme.registerdSignButton = lightColor
            StaticVar.darkTheme.copyrightSignButton = lightColor
            StaticVar.darkTheme.degreeCelsiusButton = lightColor
            StaticVar.darkTheme.degreeFahrenheitButton = lightColor
            
            StaticVar.darkTheme.settingIconPath = "setting_light_portrait"
            StaticVar.darkTheme.giphyIconPath = "gif_portrait"
            StaticVar.darkTheme.emojiIconPath = "emoji_portrait"
            StaticVar.darkTheme.upIconPath = "up_arrow_light_portrait"
            StaticVar.darkTheme.downIconPath = "down_arrow_light_portrait"
            
            StaticVar.darkTheme.happyGiphyIconPath =  "Happy_light"
            StaticVar.darkTheme.happyFilledGiphyIconPath = "Happy Filled_light"
            StaticVar.darkTheme.homeGiphyIconPath = "Home_light"
            StaticVar.darkTheme.homeFilledGiphyIconPath = "Home Filled_light"
            StaticVar.darkTheme.lineChartGiphyIconPath = "Line Chart_light"
            StaticVar.darkTheme.lineChartFilledGiphyIconPath = "Line Chart Filled_light"
            StaticVar.darkTheme.timeGiphyIconPath = "Time_light"
            StaticVar.darkTheme.timeFilledGiphyIconPath = "Time Filled_light"
            StaticVar.darkTheme.deleteFilledGiphyIconPath = "Delete Filled_light"
        })
        
        return StaticVar.darkTheme
    }

    static func colorfulTheme() -> BLTheme {
        struct StaticVar {
            static var colorfulTheme: BLTheme = BLTheme()
            static var onceToken: dispatch_once_t = 0
        }
        
        dispatch_once(&StaticVar.onceToken, {
            StaticVar.colorfulTheme.backgroundColor = UIColor.whiteColor()
            StaticVar.colorfulTheme.backgroundImage = ""
            
            StaticVar.colorfulTheme.buttonTitleFont = "Avenir-Heavy"
            StaticVar.colorfulTheme.buttonBorderColor = UIColor.blackColor()
            
            StaticVar.colorfulTheme.specialButtonTitleColor = UIColor.whiteColor()
            StaticVar.colorfulTheme.characterButtonTitleColor = UIColor.whiteColor()
            
            StaticVar.colorfulTheme.specialButtonHighLightedTitleColor = UIColor.whiteColor()
            StaticVar.colorfulTheme.characterButtonHighLightedTitleColor = UIColor.whiteColor()
            
            
            StaticVar.colorfulTheme.characterButtonShadowColor = UIColor.init(red: 139/255.0, green: 142/255.0, blue: 146/255.0, alpha: 1.0)
            StaticVar.colorfulTheme.specialButtonShadowColor = UIColor.grayColor()
            
            StaticVar.colorfulTheme.specialButtonHighlightedColor = UIColor.grayColor()
            StaticVar.colorfulTheme.characterButtonHightlightedColor = UIColor.grayColor()
            
            StaticVar.colorfulTheme.useSameColorForSpecialButtonHighlighted = false
            StaticVar.colorfulTheme.useSameColorForCharacterButtonHighlighted = false
            
            StaticVar.colorfulTheme.buttonShadowYOffset = 1.0
            StaticVar.colorfulTheme.buttonLabelYOffset = -1.5
            StaticVar.colorfulTheme.buttonImageYOffset = -0.5
            
            StaticVar.colorfulTheme.buttonPhoneBorderWidth = 1.0
            StaticVar.colorfulTheme.buttonPhoneShadowWidth = 1.0
            StaticVar.colorfulTheme.buttonPhoneCornerRadius = 4.0
            StaticVar.colorfulTheme.buttonPhonePopupCornerRadius = 9.0
            StaticVar.colorfulTheme.buttonPhonePortraitFontSize = 19.0
            StaticVar.colorfulTheme.buttonPhoneLandscapeFontSize = 22.0
            StaticVar.colorfulTheme.buttonPhonePopupPortraitFontSize = 28.0
            StaticVar.colorfulTheme.buttonPhonePopupLandscapeFontSize = 31.0
            
            
            StaticVar.colorfulTheme.buttonPadBorderWidth = 1.0
            StaticVar.colorfulTheme.buttonPadShadowWidth = 1.5
            StaticVar.colorfulTheme.buttonPadCornerRadius = 5.0
            StaticVar.colorfulTheme.buttonPadPopupCornerRadius = 11.0
            StaticVar.colorfulTheme.buttonPadPortraitFontSize = 25.0
            StaticVar.colorfulTheme.buttonPadLandscapeFontSize = 29.0
            StaticVar.colorfulTheme.buttonPadPopupPortraitFontSize = 35.0
            StaticVar.colorfulTheme.buttonPadPopupLandscapeFontSize = 38.0
            
           // let red = UIColor.init(red: 236/255.0, green: 11/255.0, blue: 0/255.0, alpha: 1.0)

            let pink = UIColor.init(red: 238/255.0, green: 33/255.0, blue: 87/255.0, alpha: 1.0)
            let lightPurple = UIColor.init(red: 171/255.0, green: 84/255.0, blue: 112/255.0, alpha: 1.0)

            let purple = UIColor.init(red: 155/255.0, green: 82/255.0, blue: 145/255.0, alpha: 1.0)
            let bluePurple = UIColor.init(red: 173/255.0, green: 164/255.0, blue: 216/255.0, alpha: 1.0)
            let blue = UIColor.init(red: 124/255.0, green: 183/255.0, blue: 233/255.0, alpha: 1.0)
            let blueGreen = UIColor.init(red: 74/255.0, green: 181/255.0, blue: 196/255.0, alpha: 1.0)
            let green1 = UIColor.init(red: 51/255.0, green: 172/255.0, blue: 73/255.0, alpha: 1.0)
            let green = UIColor.init(red: 50/255.0, green: 196/255.0, blue: 0/255.0, alpha: 1.0)
            let lightGreen = UIColor.init(red: 117/255.0, green: 213/255.0, blue: 0/255.0, alpha: 1.0)

            // Butons color
            // 1st row
            StaticVar.colorfulTheme.QButtonColor = lightGreen
            StaticVar.colorfulTheme.WButton = green
            StaticVar.colorfulTheme.EButton = green1
            StaticVar.colorfulTheme.RButton = blueGreen
            StaticVar.colorfulTheme.TButton = blue
            StaticVar.colorfulTheme.YButton = bluePurple
            StaticVar.colorfulTheme.UButton = purple
            StaticVar.colorfulTheme.IButton = lightPurple
            StaticVar.colorfulTheme.OButton = pink
            StaticVar.colorfulTheme.PButton = pink
            // 2nd row
            StaticVar.colorfulTheme.AButton = lightGreen
            StaticVar.colorfulTheme.SButton = green
            StaticVar.colorfulTheme.DButton = green1
            StaticVar.colorfulTheme.FButton = blueGreen
            StaticVar.colorfulTheme.GButton = blue
            StaticVar.colorfulTheme.HButton = bluePurple
            StaticVar.colorfulTheme.JButton = purple
            StaticVar.colorfulTheme.KButton = lightPurple
            StaticVar.colorfulTheme.LButton = pink
            // 3rd row
            StaticVar.colorfulTheme.ZButton = lightGreen
            StaticVar.colorfulTheme.XButton = green
            StaticVar.colorfulTheme.CButton = green1
            StaticVar.colorfulTheme.VButton = blueGreen
            StaticVar.colorfulTheme.BButton = blue
            StaticVar.colorfulTheme.NButton = bluePurple
            StaticVar.colorfulTheme.MButton = purple
            
            StaticVar.colorfulTheme.shiftButton = lightGreen
            StaticVar.colorfulTheme.deleteButton = pink
            StaticVar.colorfulTheme.globeButton = lightGreen
            StaticVar.colorfulTheme.spaceButton = blueGreen
            StaticVar.colorfulTheme.returnButton = pink
            StaticVar.colorfulTheme.keyModeButton = lightGreen
            
            StaticVar.colorfulTheme.shiftDefaultButtonImage = "shift.png"
            StaticVar.colorfulTheme.shiftOnceButtonImage = "shift_lock.png"
            StaticVar.colorfulTheme.shiftForeverButtonImage = "shift_lock.png"
            
            StaticVar.colorfulTheme.deleteButtonImage = "delete.png"
            StaticVar.colorfulTheme.deleteButtonHighlightedImage = "delete.png"
            
            StaticVar.colorfulTheme.globeButtonImage = "globe_white.png"
            StaticVar.colorfulTheme.globeButtonHighlightedImage = "globe_white.png"
            
            StaticVar.colorfulTheme.spaceButtonImage = ""
            StaticVar.colorfulTheme.spaceButtonHighlightedImage = ""
            
            StaticVar.colorfulTheme.returnButtonImage = ""
            StaticVar.colorfulTheme.returnButtonHighlightedImage = ""
            
            StaticVar.colorfulTheme.keyModeButtonImage = ""
            StaticVar.colorfulTheme.keyModeButtonHighlightedImage = ""
            
            StaticVar.colorfulTheme.scribbleLineColor = UIColor.init(red: 47/255.0, green: 69/255.0, blue: 93/255.0, alpha: 1)
            
            
            let lightColor = UIColor.init(red: 85/255.0, green: 177/255.0, blue: 86/255.0, alpha: 1.0)
            
            // Number keys
            StaticVar.colorfulTheme.zeroButton = lightGreen
            StaticVar.colorfulTheme.oneButton = lightGreen
            StaticVar.colorfulTheme.twoButton = lightGreen
            StaticVar.colorfulTheme.threeButton = lightGreen
            StaticVar.colorfulTheme.fourButton = lightGreen
            StaticVar.colorfulTheme.fiveButton = lightGreen
            StaticVar.colorfulTheme.sixButton = lightGreen
            StaticVar.colorfulTheme.sevenButton = lightGreen
            StaticVar.colorfulTheme.eightButton = lightGreen
            StaticVar.colorfulTheme.nineButton = lightGreen
            
            StaticVar.colorfulTheme.minusButton = lightGreen
            StaticVar.colorfulTheme.slashButton = lightGreen
            StaticVar.colorfulTheme.colonButton = lightGreen
            StaticVar.colorfulTheme.semicolonButton = lightGreen
            StaticVar.colorfulTheme.leftParenthesisButton = lightGreen
            StaticVar.colorfulTheme.rightParenthesisButton = lightColor
            StaticVar.colorfulTheme.dollarSignButton = lightColor
            StaticVar.colorfulTheme.ampersandButton = lightColor
            StaticVar.colorfulTheme.atSignButton = lightColor
            StaticVar.colorfulTheme.quotationMarkButton = lightColor
            StaticVar.colorfulTheme.fullStopButton = lightColor
            StaticVar.colorfulTheme.commaButton = lightColor
            StaticVar.colorfulTheme.questionMarkButton = lightColor
            StaticVar.colorfulTheme.exclamationMarkButton = lightColor
            StaticVar.colorfulTheme.apostropheButton = lightColor
            StaticVar.colorfulTheme.graveAccentButton = lightColor
            
            
            // Number keys page 2
            StaticVar.colorfulTheme.leftSquareBracketButton = lightColor
            StaticVar.colorfulTheme.rightSquareBracketButton = lightColor
            StaticVar.colorfulTheme.leftCurlyBracketButton = lightColor
            StaticVar.colorfulTheme.rightCurlyBracketButton = lightColor
            StaticVar.colorfulTheme.sharpButton = lightColor
            StaticVar.colorfulTheme.percentSignButton = lightColor
            StaticVar.colorfulTheme.circumflexAccentButton = lightColor
            StaticVar.colorfulTheme.asteriskButton = lightColor
            StaticVar.colorfulTheme.plusSignButton = lightColor
            StaticVar.colorfulTheme.equalSignButton = lightColor
            StaticVar.colorfulTheme.lowLineButton = lightColor
            StaticVar.colorfulTheme.backslashButton = lightColor
            StaticVar.colorfulTheme.verticalBarButton = lightColor
            StaticVar.colorfulTheme.tildeButton = lightColor
            StaticVar.colorfulTheme.lessThanSignButton = lightColor
            StaticVar.colorfulTheme.greaterThanSignButton = lightColor
            StaticVar.colorfulTheme.euroSignButton = lightColor
            StaticVar.colorfulTheme.poundSignButton = lightColor
            StaticVar.colorfulTheme.yenSignButton = lightColor
            StaticVar.colorfulTheme.centSignButton = lightColor
            
            StaticVar.colorfulTheme.blackCircleButton = lightColor
            StaticVar.colorfulTheme.registerdSignButton = lightColor
            StaticVar.colorfulTheme.copyrightSignButton = lightColor
            StaticVar.colorfulTheme.degreeCelsiusButton = lightColor
            StaticVar.colorfulTheme.degreeFahrenheitButton = lightColor
            
            StaticVar.colorfulTheme.settingIconPath = "setting_dark_portrait"
            StaticVar.colorfulTheme.giphyIconPath = "gif_portrait"
            StaticVar.colorfulTheme.emojiIconPath = "emoji_portrait"
            StaticVar.colorfulTheme.upIconPath = "up_arrow_light_portrait"
            StaticVar.colorfulTheme.downIconPath = "down_arrow_light_portrait"
            
            StaticVar.colorfulTheme.happyGiphyIconPath =  "Happy_light"
            StaticVar.colorfulTheme.happyFilledGiphyIconPath = "Happy Filled_light"
            StaticVar.colorfulTheme.homeGiphyIconPath = "Home_light"
            StaticVar.colorfulTheme.homeFilledGiphyIconPath = "Home Filled_light"
            StaticVar.colorfulTheme.lineChartGiphyIconPath = "Line Chart_light"
            StaticVar.colorfulTheme.lineChartFilledGiphyIconPath = "Line Chart Filled_light"
            StaticVar.colorfulTheme.timeGiphyIconPath = "Time_light"
            StaticVar.colorfulTheme.timeFilledGiphyIconPath = "Time Filled_light"
            StaticVar.colorfulTheme.deleteFilledGiphyIconPath = "Delete Filled_light"
        })
        
        return StaticVar.colorfulTheme
    }

    
//    private var color: UIColor
//    private var shadowColor: UIColor
//    
//    var style: BLKeyStyle {
//        get {
//            return self.style
//        }
//        set {
//            if self.style == newValue {
//                return
//            }
//            self.style = newValue
//            self.updateState()
//        }
//    }
//    var appearance: BLKeyAppearance {
//        get {
//            return self.appearance
//        }
//        set {
//            if (self.appearance == newValue) {
//                return
//            }
//            self.appearance = newValue
//            self.updateState()
//        }
//    }
//    var cornerRadius: CGFloat {
//        get {
//            return self.cornerRadius
//        }
//        set {
//            if (self.cornerRadius == newValue) {
//                return
//            }
//            self.cornerRadius = newValue
//            self.setNeedsDisplay()
//        }
//    }
//    
//    func getTitleFont() -> UIFont! {
//        return self.label!.font
//    }
//    
//    func setTitleFont(newFont: UIFont) {
//        self.label!.font = newFont
//    }
//    
//    func updateState() {
//        switch self.appearance {
//        case BLKeyAppearance.Dark:
//            
//            switch self.style {
//            case BLKeyStyle.Light:
//                self.label!.textColor = UIColor.whiteColor()
//                
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLDarkAppearance.darkKeyColor()
//                    self.shadowColor = BLDarkAppearance.darkKeyShadowColor()
//                    
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLDarkAppearance.lightKeyColor()
//                    self.shadowColor = BLDarkAppearance.lightKeyShadowColor()
//                    
//                    break
//                }
//                
//                break
//            case BLKeyStyle.Dark:
//                self.label?.textColor = UIColor.whiteColor()
//                self.imageView?.image = self.imageView?.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
//                self.imageView?.tintColor = BLDarkAppearance.whiteColor()
//                
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLDarkAppearance.lightKeyColor()
//                    self.shadowColor = BLDarkAppearance.lightKeyShadowColor()
//                    
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLDarkAppearance.darkKeyColor()
//                    self.shadowColor = BLDarkAppearance.darkKeyShadowColor()
//                    
//                    break
//                }
//                break
//            default: // BLKeyStyle.Blue
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLDarkAppearance.lightKeyColor()
//                    self.shadowColor = BLDarkAppearance.lightKeyShadowColor()
//                    self.label?.textColor = BLDarkAppearance .blackColor()
//                    
//                    break
//                case UIControlState.Disabled:
//                    self.color = BLDarkAppearance.darkKeyColor()
//                    self.shadowColor = BLDarkAppearance.darkKeyShadowColor()
//                    self.label?.textColor = BLDarkAppearance.blueKeyDisabledTitleColor()
//                    
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLDarkAppearance.blueKeyColor()
//                    self.shadowColor = BLDarkAppearance.blueKeyShadowColor()
//                    self.label?.textColor = UIColor.whiteColor()
//                    
//                    break
//                }
//                break
//            }
//            
//            break
//        default: // BLKeyAppearance.Light
//            switch self.style {
//            case BLKeyStyle.Light:
//                self.label!.textColor = UIColor.blackColor()
//                
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLLightAppearance.darkKeyColor()
//                    self.shadowColor = BLLightAppearance.darkKeyShadowColor()
//                    
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLLightAppearance.lightKeyColor()
//                    self.shadowColor = BLLightAppearance.lightKeyShadowColor()
//                    
//                    break
//                }
//                
//                break
//            case BLKeyStyle.Dark:
//                self.label?.textColor = UIColor.blackColor()
//                
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLLightAppearance.lightKeyColor()
//                    self.shadowColor = BLLightAppearance.lightKeyShadowColor()
//                    self.imageView?.tintColor = BLLightAppearance.blackColor()
//                    
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLLightAppearance.darkKeyColor()
//                    self.shadowColor = BLLightAppearance.darkKeyShadowColor()
//                    self.imageView?.tintColor = BLLightAppearance.whiteColor()
//                    
//                    break
//                }
//                break
//            default: // BLKeyStyle.Blue
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLLightAppearance.lightKeyColor()
//                    self.shadowColor = BLLightAppearance.lightKeyShadowColor()
//                    self.label?.textColor = BLLightAppearance .blackColor()
//                    
//                    break
//                case UIControlState.Disabled:
//                    self.color = BLLightAppearance.darkKeyColor()
//                    self.shadowColor = BLLightAppearance.darkKeyShadowColor()
//                    self.label?.textColor = BLLightAppearance.blueKeyDisabledTitleColor()
//                    
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLLightAppearance.blueKeyColor()
//                    self.shadowColor = BLLightAppearance.blueKeyShadowColor()
//                    self.label?.textColor = UIColor.whiteColor()
//                    
//                    break
//                }
//                break
//            }
//            
//            break
//        }
//    }

    
}
