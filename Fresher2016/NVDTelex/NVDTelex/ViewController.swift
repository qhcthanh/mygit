//
//  ViewController.swift
//  NVDTelex
//
//  Created by VanDao on 8/15/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UITextFieldDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var typingMode: UISegmentedControl!
    @IBOutlet weak var convertTextField: UILabel!
    @IBOutlet weak var textFieldBottomConstraint: NSLayoutConstraint!
    var typingType: TypingType!
    var numberOfItems: Int!
    var currentWord: String!
    var transformWord: String!
    var index: String.Index!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        numberOfItems = 0
        currentWord = ""
        transformWord = ""
        typingType = TypingType.Telex
        convertTextField.text = ""
        index = textField.text?.startIndex
        
        let notificationCenter: NSNotificationCenter! = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self, selector: #selector(ViewController.didShowKeyboard(_:)), name: UIKeyboardDidShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(ViewController.didHideKeyboard(_:)), name: UIKeyboardDidHideNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {

        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func typingMode(sender: AnyObject, forEvent event: UIEvent) {
        
        switch typingMode.selectedSegmentIndex {
        case 0:
            typingType = TypingType.Telex
        case 1:
            typingType = TypingType.VNI
        default:
            break
        }
    }
    
    func didShowKeyboard(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                
                UIView.animateWithDuration(0.2, animations: { 
                    self.textFieldBottomConstraint.constant = keyboardSize.height
                    self.view.layoutIfNeeded()
                })
            } else {
                // no UIKeyboardFrameBeginUserInfoKey entry in userInfo
            }
        } else {
            // no userInfo dictionary in notification
        }
    }
    
    func didHideKeyboard(notification: NSNotification) {
        
        UIView.animateWithDuration(0.2, animations: {
            self.textFieldBottomConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    func getLastWordFromText(text: String) -> (lastString: String, range: Range<String.Index>) {
        
        // Find index of space.
        var start = text.startIndex
        for i in (text.startIndex ..< text.endIndex).reverse() {
                // If character is a space, the second word starts at the next index.
                if text[i] == " " {
                    // First char of second word is next index.
                    start = i.successor()

                    break
                }
        }
        // Return substring.
        
        return (text[start..<text.endIndex], Range(start..<text.endIndex))
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        return CGSizeMake(collectionView.frame.width, 40)
    }
    
    enum eventType {
        case eventTypeBackspace
        case eventTypeCharacter
        case eventTypeSpace
    }
    
    func shouldChangeCharactersInLabel(character: Character, type: eventType) {
        
        switch type {
        case .eventTypeCharacter:
           // convertTextField.text?.replaceRange((index..<convertTextField.text!.endIndex), with: res)
            currentWord.append(character)
        case .eventTypeSpace:
          //  convertTextField.text = convertTextField.text! + " "
          //  index = convertTextField.text!.endIndex
            index = index.advancedBy(currentWord.characters.count, limit: textField.text!.endIndex)
            currentWord = ""
        case .eventTypeBackspace:
           // convertTextField.text?.replaceRange((index..<convertTextField.text!.endIndex), with: res)
            if currentWord != "" {
                currentWord.removeAtIndex(currentWord.endIndex.advancedBy(-1))
            }
        }
        
        let res: String = WordSplitting.init(_rawWord: currentWord, typingType: typingType).getVietnameseWord()
        convertTextField.text = res
    }
    
    
    
    var beforeText: String! = ""
    func textDidChange(sender: AnyObject, forEvent event: UIEvent) {
        
        NSDate.startCount()
        
        let lastWord = textField.text?.componentsSeparatedByString(" ").last ?? ""
        if (lastWord != "") {
            convertTextField.text = WordSplitting.init(_rawWord: lastWord, typingType: typingType).getVietnameseWord()
        }
        if let text = textField.text {
            let lastChar: Character = text.characters.last ?? " "
            
            if text.characters.count > beforeText.characters.count {
                
                if lastChar == " " {
                    shouldChangeCharactersInLabel(" ", type: eventType.eventTypeSpace)
                } else {
                    shouldChangeCharactersInLabel(lastChar, type: eventType.eventTypeCharacter)
                }
            } else {
                shouldChangeCharactersInLabel(Character.init(" "), type: eventType.eventTypeBackspace)
            }
        }
        
        beforeText = textField.text
        
        print("\(round(NSDate.endCount() * 1000000) / 1000)" + " ms")
    }

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfItems
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: TelextCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! TelextCollectionViewCell
        cell.contentTextField.text = convertTextField.text
        
        return cell
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        textField.text = nil
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField.text != nil {
            numberOfItems = numberOfItems + 1
            collectionView .insertItemsAtIndexPaths([NSIndexPath.init(forItem: numberOfItems - 1, inSection: 0)])
        }
        
        textField.text = ""
        convertTextField.text = ""
        index = convertTextField.text!.startIndex
        
        return true
    }
    
    
}

