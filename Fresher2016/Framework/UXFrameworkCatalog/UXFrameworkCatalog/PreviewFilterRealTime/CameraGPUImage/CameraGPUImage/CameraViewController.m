//
//  CameraViewController.m
//  ZaloCameraCustom
//
//  Created by qhcthanh on 6/14/16.
//  Copyright © 2016 qhcthanh. All rights reserved.
//

#import "CameraViewController.h"
#import "PreviewImageViewController.h"
#import "GPUStickerView.h"
#import "PreiviewFilterRealTimeView.h"

@interface CameraViewController () <UIGestureRecognizerDelegate, PreiviewFilterRealTimeViewDelegate>

@end

@implementation CameraViewController {
    GPUImageOutput<GPUImageInput> *filter;
    GPUImageMovieWriter *movieWriter;
    GPUImageStillCamera *stillCamera;
    

    NSMutableArray<Class> *_filters;
    NSUInteger _curFilter;
    NSURL *outputFileURL;
    BOOL isRecording;
    BOOL isFlashOn;
    
    //Sticker view
    GPUStickerView *stickerView;
    NSMutableArray<UIImage *> *imageList;
    PreiviewFilterRealTimeView* previewFilter;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    // Set up filters;
    isRecording = YES;
    isFlashOn = NO;
    _curFilter = 0;
    _filters = [[NSMutableArray alloc] init];
   
    /*
    [_filters addObject:[[GPUImageFilter alloc] init]];
    [_filters addObject:[[GPUImageGrayscaleFilter alloc] init]];
    [_filters addObject:[[GPUImageSepiaFilter alloc] init]];
    [_filters addObject:[[GPUImageSketchFilter alloc] init]];
    [_filters addObject:[[GPUImagePixellateFilter alloc] init]];
    [_filters addObject:[[GPUImageColorInvertFilter alloc] init]];
    [_filters addObject:[[GPUImageToonFilter alloc] init]];
    [_filters addObject:[[GPUImagePinchDistortionFilter alloc] init]];
    */
    [_filters addObject:[IFNormalFilter class]];
    [_filters addObject:[IF1977Filter class]];
    [_filters addObject:[IFBrannanFilter class]];
    [_filters addObject:[IFEarlybirdFilter class]];
    [_filters addObject:[IFInkwellFilter class]];
    [_filters addObject:[IFLomofiFilter class]];
    [_filters addObject:[IFLordKelvinFilter class]];
    [_filters addObject:[IFNashvilleFilter class]];
    [_filters addObject:[IFValenciaFilter class]];
    

    [_filters addObject:[IFWaldenFilter class]];
    [_filters addObject:[IFXproIIFilter class]];
    [_filters addObject:[IFAmaroFilter class]];
    [_filters addObject:[IFHefeFilter class]];
    [_filters addObject:[IFHudsonFilter class]];
    [_filters addObject:[IFRiseFilter class]];
    [_filters addObject:[IFSierraFilter class]];
    [_filters addObject:[IFSutroFilter class]];
    [_filters addObject:[IFToasterFilter class]];


    //set private property
    _lastScale = 1;
    
    //Set camera delegate
    //_cameraView.delegate = self;
    
    
    // Add swipe gsture in camera view
    UISwipeGestureRecognizer* swipeLeftCameraViewGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeFilterGesture:)];
    swipeLeftCameraViewGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    
    UISwipeGestureRecognizer* swipeRightCameraViewGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeFilterGesture:)];
    swipeRightCameraViewGesture.direction = UISwipeGestureRecognizerDirectionRight;
    
    [_cameraView addGestureRecognizer: swipeRightCameraViewGesture];
    [_cameraView addGestureRecognizer: swipeLeftCameraViewGesture];
    
    
    // add swipe gesture in mode view
    UISwipeGestureRecognizer* swipeLeftModeViewGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeModeGesture:)];
    swipeLeftModeViewGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    
    UISwipeGestureRecognizer* swipeRightModeViewGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeModeGesture:)];
    swipeRightModeViewGesture.direction = UISwipeGestureRecognizerDirectionRight;
    
    [_modeView addGestureRecognizer: swipeRightModeViewGesture];
    [_modeView addGestureRecognizer: swipeLeftModeViewGesture];
    
    //Add tap gesture in label mode
    UITapGestureRecognizer* photoTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeModePhoto)];
    [photoTapGesture setNumberOfTapsRequired:1];
    UITapGestureRecognizer* videoTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeModeVideo)];
    [videoTapGesture setNumberOfTapsRequired:1];
    [_photoModeLabel addGestureRecognizer:photoTapGesture];
    [_videoModeLabel addGestureRecognizer:videoTapGesture];
    
    //Add pinch gesture in cameraview
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
    pinchGesture.delegate = self;
    [_cameraView addGestureRecognizer:pinchGesture];
    
    //setup camera
    [self setupCamera];
    
    //Sticker setup
    stickerView = [[GPUStickerView alloc] initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    [stickerView setOpaque:NO];
    [self.view addSubview:stickerView];
    
    [self.view bringSubviewToFront:stickerView];
    [self.view bringSubviewToFront:self.topBar];
    [self.view bringSubviewToFront:self.bottomBar];
    
    stickerView.userInteractionEnabled = YES;
    stickerView.multipleTouchEnabled = YES;
    stickerView.backgroundColor = [UIColor clearColor];
    
    imageList = [[NSMutableArray alloc] init];
    [imageList addObject:[UIImage imageNamed:@"image.png"]];
    [imageList addObject:[UIImage imageNamed:@"sticker.png"]];
    
    previewFilter = [[PreiviewFilterRealTimeView alloc]
                     initWithCamera: stillCamera
                     frame:
                     CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height - 90 - 50) filters:_filters];
    [self.view addSubview:previewFilter];
    [self.view bringSubviewToFront:_topBar];
    [self.view bringSubviewToFront:_bottomBar];
    previewFilter.delegate = self;
}

-(void)setupCamera {
    // Setup photo
    stillCamera = [[GPUImageStillCamera alloc] initWithSessionPreset:AVCaptureSessionPresetHigh cameraPosition:AVCaptureDevicePositionBack];
    stillCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
    
   // filter = [[[_filters objectAtIndex:_curFilter] alloc] init];

   // GPUImageView *filterView = self.cameraView;
    
   // [filter addTarget:filterView];
   // [stillCamera addTarget:filter];
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Remove old filter
    [stillCamera removeTarget:filter];
    
    //filter = [[[_filters objectAtIndex:_curFilter] alloc] init];
    //[filter addTarget:self.cameraView];
    
    //[stillCamera addTarget:filter];
    [stillCamera startCameraCapture];
    
    [self changeModeCamera];
}

- (void)viewWillDisappear:(BOOL)animated{
    [stickerView removeAllSticker];
    
}

-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
//    [stickerView removeAllSticker];
//    [stillCamera removeAllTargets];
//    
}

//MARK: Change mode camera
-(void)changeModeCamera {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, false), ^{
        if (stillCamera && _leadingModeViewConstraint.constant == LEADING_PHOTO_MODEVIEW_CONTRAINST) {
            if (isFlashOn == YES) {
                [stillCamera.inputCamera lockForConfiguration:nil];
                
                [stillCamera.inputCamera setTorchMode:AVCaptureTorchModeOff];
                [stillCamera.inputCamera setFlashMode:AVCaptureFlashModeOn];
                
                [stillCamera.inputCamera unlockForConfiguration];
            }
            
        } else if (stillCamera && _leadingModeViewConstraint.constant == LEADING_VIDEO_MODEVIEW_CONTRAINST) {
            if (isFlashOn == YES) {
                [stillCamera.inputCamera lockForConfiguration:nil];
                
                [stillCamera.inputCamera setFlashMode:AVCaptureFlashModeOff];
                [stillCamera.inputCamera setTorchMode:AVCaptureTorchModeOn];
                
                [stillCamera.inputCamera unlockForConfiguration];
            }
        }
    });
}

-(void)changeModePhoto {
    _photoModeLabel.textColor = [UIColor yellowColor];
    _videoModeLabel.textColor = [UIColor whiteColor];
    [_shutterButton setImage: [UIImage imageNamed:@"Circle Thin-96"] forState:UIControlStateNormal];
    _leadingModeViewConstraint.constant = LEADING_PHOTO_MODEVIEW_CONTRAINST;
    _timeVideoLabel.hidden = true;
    [_countTimer invalidate];
    [movieWriter finishRecording];
    //Stop recording
    //[_cameraView stopRecording];
  
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    [self changeModeCamera];
}

-(void)changeModeVideo {
    _photoModeLabel.textColor = [UIColor whiteColor];
    _videoModeLabel.textColor = [UIColor yellowColor];
    [_shutterButton setImage: [UIImage imageNamed:@"Record"] forState:UIControlStateNormal];
    _leadingModeViewConstraint.constant = LEADING_VIDEO_MODEVIEW_CONTRAINST;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    [self changeModeCamera];
}

//MARK: UIGestureRecognizer
-(void)swipeModeGesture:(UISwipeGestureRecognizer*)swipeGesture {
    if (swipeGesture.direction == UISwipeGestureRecognizerDirectionRight) {
        [self changeModePhoto];
    } else {
        [self changeModeVideo];
    }
}

-(void)swipeFilterGesture:(UISwipeGestureRecognizer*)swipeGesture {
    if (swipeGesture.direction == UISwipeGestureRecognizerDirectionRight) {
        // swipe right
        if (_curFilter == 0)
            return;
        _curFilter--;
        NSLog(@"swipe right");
    } else {
        // swipe left
        if (_curFilter == _filters.count - 1)
            return;
        _curFilter++;
        NSLog(@"swipe left");
    }
    [stillCamera removeTarget:filter];
    
    if (movieWriter)
        [filter removeTarget:movieWriter];
    
    filter = [[[_filters objectAtIndex:_curFilter] alloc] init];
    
    [filter addTarget:self.cameraView];
    
    if (movieWriter)
        [filter addTarget:movieWriter];
    
    [stillCamera addTarget:filter];
  
}

- (void)handlePinchGesture:(UIPinchGestureRecognizer *)gestureRecognizer {
    
//    if (UIGestureRecognizerStateBegan == gestureRecognizer.state ||
//        UIGestureRecognizerStateChanged == gestureRecognizer.state) {
//        
//        float scale = gestureRecognizer.scale;
//        
//        if (scale < CAMERA_MINSCALE ) {
//            scale = CAMERA_MINSCALE;
//        } else if (scale > CAMERA_MAXSCALE) {
//            scale = CAMERA_MAXSCALE;
//        }
//        _lastScale = scale;
//        //[_cameraView zoomPreview:scale];
//        NSLog(@"%f",1/scale);
//        cropFilter.cropRegion = CGRectMake(0,0, 1/scale, 1/scale);
//        // Reset to 1 for scale delta's
//        //  Note: not 0, or we won't see a size: 0 * width = 0
//        //pinchGesture.scale = 1;
//    }
    
        if([gestureRecognizer state] == UIGestureRecognizerStateBegan) {
            // Reset the last scale, necessary if there are multiple objects with different scales
            _lastScale = [gestureRecognizer scale];
        }
    
        if ([gestureRecognizer state] == UIGestureRecognizerStateBegan ||
            [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
    
            CGFloat currentScale = [[[gestureRecognizer view].layer valueForKeyPath:@"transform.scale"] floatValue];
    
            // Constants to adjust the max/min values of zoom
            const CGFloat kMaxScale = 5.0;
            const CGFloat kMinScale = 1.0;
    
            CGFloat newScale = 1 -  (_lastScale - [gestureRecognizer scale]);
            newScale = MIN(newScale, kMaxScale / currentScale);
            newScale = MAX(newScale, kMinScale / currentScale);
            CGAffineTransform transform = CGAffineTransformScale([[gestureRecognizer view] transform], newScale, newScale);
            [gestureRecognizer view].transform = transform;
    
    
            _lastScale = [gestureRecognizer scale];  // Store the previous scale factor for the next pinch gesture call
        }
}

//MARK: UIButton action
- (IBAction)onTapFlashAction:(id)sender {
    
    NSString* imageFlashName = @"Flash Off";
    NSInteger mode = AVCaptureTorchModeOff;
    if (isFlashOn == NO) {
        mode = AVCaptureTorchModeOn;
        imageFlashName = @"Flash On";
        isFlashOn = YES;
    }
    else
        isFlashOn = NO;
    
    [_flashButton setImage:[UIImage imageNamed:imageFlashName] forState: UIControlStateNormal];

    [stillCamera.inputCamera lockForConfiguration:nil];
        
    if (stillCamera && _leadingModeViewConstraint.constant == LEADING_PHOTO_MODEVIEW_CONTRAINST) {
        [stillCamera.inputCamera setFlashMode:mode];
    }
    else if (stillCamera && _leadingModeViewConstraint.constant == LEADING_VIDEO_MODEVIEW_CONTRAINST) {
        [stillCamera.inputCamera setTorchMode:mode];
    }
    [stillCamera.inputCamera unlockForConfiguration];

}

- (IBAction)onTapBackAction:(id)sender {
    //[_cameraView onTapDismissButton];
    //[self.navigationController popViewControllerAnimated:true];
    //[stillCamera removeTarget:filter];
    [stillCamera removeAllTargets];
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)onTapRotateAction:(id)sender {
    //[_cameraView onTapToggleButton];
    [stillCamera rotateCamera];
    
}

- (IBAction)onTapShutterAction:(id)sender {
    if (_leadingModeViewConstraint.constant == LEADING_PHOTO_MODEVIEW_CONTRAINST) {
        static BOOL canCapture = YES;
        if (!canCapture) {
            return;
        }
        // Remove current filter and target
        [stillCamera removeTarget:filter];
        	
        filter = [[[_filters objectAtIndex:0] alloc] init];
        [filter addTarget:self.cameraView];
        
        [stillCamera addTarget:filter];
        
        canCapture = NO;
        
        // Handler process image
        [stillCamera capturePhotoAsImageProcessedUpToFilter:filter withCompletionHandler:^(UIImage *processedImage, NSError *error) {
            GPUImageTransformFilter *xff = [[GPUImageTransformFilter alloc] init];
            xff.affineTransform = [_cameraView transform];
            processedImage = [xff imageByFilteringImage:processedImage];
            
            canCapture = YES;
            // Present to preview image controller
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle: nil];
            PreviewImageViewController *showImageVC = (PreviewImageViewController*)[mainStoryboard
                                                               instantiateViewControllerWithIdentifier: @"PreviewImageViewController"];
            showImageVC.previewImage = processedImage;
            showImageVC.indexFilter = _curFilter;
            showImageVC.filterList = _filters;
            
            [stillCamera stopCameraCapture];
            
            [self presentViewController:showImageVC animated:true completion:nil];
        }];
    } else {
        if (isRecording) {
            //Setup const
            isRecording = NO;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, false), ^{
                [filter removeTarget:movieWriter];
                // create url save temp video
                NSString* outputFileName = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Movie.m4v"];
                // If a file already exists, AVAssetWriter won't let you record new frames, so delete the old movie
                unlink([outputFileName UTF8String]);
                outputFileURL = [NSURL fileURLWithPath:outputFileName];
                //setup movie writer
                
                movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:outputFileURL size:CGSizeMake(480.0, 640.0)];
                stillCamera.audioEncodingTarget = movieWriter;
                if (isFlashOn) {
                        [stillCamera.inputCamera lockForConfiguration:nil];
                        [stillCamera.inputCamera setTorchMode:isFlashOn];
                        [stillCamera.inputCamera unlockForConfiguration];
                }
                
                movieWriter.encodingLiveVideo = YES;
                [filter addTarget:movieWriter];
                
                //Start recording
                [self didStartRecordingVideo];
            });
        } else {
            [self didFinishRecordingVideo];
            
        }
    }
}



-(NSString*)getImagePathName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *directory = [paths objectAtIndex:0];
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    NSString* uniqueString = [NSString stringWithFormat:@"%f",timeInSeconds];
    uniqueString = [uniqueString stringByReplacingOccurrencesOfString:@"." withString:@""];
    return [NSString stringWithFormat:@"%@/Image%@.jpg", directory ,uniqueString ];
}

- (IBAction)trashAction:(id)sender {
  
}

- (IBAction)textAction:(id)sender {
    
}

- (IBAction)stickerAction:(id)sender {
    
}

- (IBAction)previewFilterAction:(id)sender {
    if (previewFilter && !previewFilter.isShowMenu) {
        [UIView animateWithDuration:0.3 animations:^{
            _topBar.alpha = 1;
            _bottomBar.alpha = 1;
            _shutterButton.alpha = 0;
            _modeView.alpha = 0;
            _backButton.alpha = 0;
        }];
        
        [self.view addSubview:previewFilter];
        [self.view bringSubviewToFront:_bottomBar];
        [self.view bringSubviewToFront:_topBar];
        [previewFilter showFilterMenu];
    } else {
        [previewFilter showFilterSelect];
        [self hidePrevieFilter];
    }
}
//MARK: PreiviewFilterRealTimeViewDelegate
-(void)didSelectFilter:(id)filterT {
//    [stillCamera removeTarget:filter];
//    filter = [[filterT alloc] init];
//    [filter addTarget:_cameraView];
//    [stillCamera addTarget:filter];
//
    previewFilter.isShowMenu = false;
    [self hidePrevieFilter];
}

//MARK: private function
-(void)hidePrevieFilter {
    if (previewFilter.superview == self.view) {
        [UIView animateWithDuration:0.3 animations:^{
            _topBar.alpha = 0.5;
            _bottomBar.alpha = 0.5;
            _shutterButton.alpha = 1;
            _modeView.alpha = 1;
            _backButton.alpha = 1;
        }];
        //[previewFilter removeFromSuperview];
    }
}
-(void)didStartRecordingVideo {
    if(_countTimer) {
        _timeCount = 0;
        [_countTimer invalidate];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        _modeView.hidden = true;
    });
    
    
    [movieWriter startRecording];
    if (![NSThread isMainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            _timeVideoLabel.hidden = false;
            _countTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateCountdown) userInfo:nil repeats: YES];
        });
    } else {
        _timeVideoLabel.hidden = false;
        _countTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateCountdown) userInfo:nil repeats: YES];
    }
    
}

-(void)didFinishRecordingVideo {
    _timeVideoLabel.hidden = true;
    dispatch_async(dispatch_get_main_queue(), ^{
        _modeView.hidden = false;
    });
    [_modeView setNeedsDisplay];
    [_countTimer invalidate];
    _timeVideoLabel.text = DEFAULT_VIDEO_TIME;
    stillCamera.audioEncodingTarget = nil;
    [movieWriter finishRecording];
    [stillCamera stopCameraCapture];
    NSLog(@"finishRecording");
    //UISaveVideoAtPathToSavedPhotosAlbum(outputFileName, nil, nil, nil);
    AVPlayer* avPlayer = [AVPlayer playerWithURL:outputFileURL];
    PreviewVideoViewController* previewVC = [PreviewVideoViewController new];
    previewVC.avPlayer = avPlayer;
    
    [self presentViewController:previewVC animated:true completion:nil];
    isRecording = YES;
}

-(void) updateCountdown
{
    _timeCount++;
    NSDate *now = [NSDate dateWithTimeIntervalSince1970:_timeCount];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *componentMint = [calendar components:NSCalendarUnitMinute fromDate:now];
    NSDateComponents *componentSec = [calendar components:NSCalendarUnitSecond fromDate:now];
    
    NSString* minute = [NSString stringWithFormat:@"%02ld",(componentMint.minute)];
    NSString* second = [NSString stringWithFormat:@"%02ld",(componentSec.second)];
    
    _timeVideoLabel.text = [NSString stringWithFormat:@"%@:%@",minute,second];
    
    if (_timeCount == 15) {
        [_countTimer invalidate];
        [self didFinishRecordingVideo];
    }
    
}



@end
