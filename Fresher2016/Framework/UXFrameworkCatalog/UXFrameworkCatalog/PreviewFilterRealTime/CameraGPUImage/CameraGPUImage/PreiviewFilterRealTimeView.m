//
//  PreiviewFilterRealTimeView.m
//  CameraGPUImage
//
//  Created by qhcthanh on 6/30/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "PreiviewFilterRealTimeView.h"
#import "InstaFilters.h"
#import "PreviewFilterGPUImageView.h"

@implementation PreiviewFilterRealTimeView {
    UICollectionView* _collectionView;
    NSMutableArray<Class> *_filters;
    NSMutableArray* cameraViewFilter;
    NSInteger _curFilter;
    CGFloat widthFilterItem;
    CGFloat heightFilterItem;
    BOOL canTouch;
    dispatch_queue_t addFilterInternalQueue;
}

-(id)initWithCamera:(GPUImageStillCamera*)videoCamera frame:(CGRect)frame filters:(id)filters {
    NSTimeInterval timerS = [NSDate timeIntervalSinceReferenceDate];
    
    self = [super initWithFrame:frame];
    // Setup property
     _videoCamera = videoCamera;
    _isShowMenu = false;
    _filters = filters;
    canTouch = true;
    cameraViewFilter = [[NSMutableArray alloc] initWithArray: @[@"",@"",@"",@"",@"",@"",@"",@"",@""] ];
   
    widthFilterItem = self.frame.size.width * 1/3 - 5;
    heightFilterItem = self.frame.size.height * 1/3 - 5;
    [self setBackgroundColor:[UIColor blackColor]];

    // Start top space Y = 5, leading = 5
    CGFloat posY = 5;
    int startPos = 1;
    // For row
    for(int i = 0 ; i<3 ;i++) {
        // posX: origin.x
        CGFloat posX = 5;
        // For column
        for(int j = 0;j<3;j++) {
            // Create each GPUImageVIew display realtime for each filter
            PreviewFilterGPUImageView* imageV = [[PreviewFilterGPUImageView alloc]
                                    initWithFrame:CGRectMake(posX, posY, widthFilterItem, heightFilterItem)];
            imageV.tag = startPos;
            
            UILabel* nameFilterLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, imageV.frame.size.height - 16, imageV.frame.size.width - 10, 16)];
            [nameFilterLabel setFont:[UIFont fontWithName:@"Arial" size:10]];
            nameFilterLabel.textColor = [UIColor whiteColor];
            nameFilterLabel.tag = 536;
            nameFilterLabel.textAlignment = NSTextAlignmentRight;
            [imageV addSubview:nameFilterLabel];
             
            // Get filter at tag index and add ImageV + Camera
            id filter = [[[_filters objectAtIndex:startPos] alloc] init];
            
            [filter forceProcessingAtSize:imageV.sizeInPixels];
            [filter addTarget:imageV];
            [_videoCamera addTarget:filter];
            
            // Add Gesture when user tap in filterView
            UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectFilterView:)];
            [imageV addGestureRecognizer:tapGesture];
            
            // Add filter and imageV to array at imageV.tag position
            cameraViewFilter[startPos] = imageV;
            imageV.filter = filter;
            imageV.rPosition = imageV.frame.origin;
            nameFilterLabel.text = [IFImageFilter getNameFilter:((IFImageFilter*)filter).name];
            
            [self addSubview:imageV];
            
            // New posX + width Item + space
            posX += widthFilterItem + 5;
            startPos += 2;
            if (startPos > 8) {
                startPos = 0;
            }
        }
        // New posY + height + space
        posY += heightFilterItem + 5;
    }
    
    //Set current select filter default
    _curFilter = 0;
    [self showFilterSelect];
    
    
    NSTimeInterval timerE = [NSDate timeIntervalSinceReferenceDate];
    NSLog(@"Time %f",timerE - timerS);
    
    return self;
}

-(void)didSelectFilterView:(UITapGestureRecognizer*)filterViewGesture {
    if (canTouch) {
        canTouch = false;
        GPUImageView* filterView = (GPUImageView*)filterViewGesture.view;
        _curFilter = filterView.tag;
        [filterView removeGestureRecognizer:filterViewGesture];
        [self showFilterSelect];
    }
}

-(void)showFilterSelect {
    _isShowMenu = false;
    // Get filterview current and bring to front display
    PreviewFilterGPUImageView* filterView = (PreviewFilterGPUImageView*)[cameraViewFilter objectAtIndex:_curFilter];
    [self bringSubviewToFront:filterView];
    UIView* nameView = [filterView viewWithTag:536];
    nameView.hidden = true;
    
    //animation for zoom out full screnn
    [UIView animateWithDuration:0.3 animations:^{
        filterView.frame = CGRectMake(0, -50, self.frame.size.width, self.frame.size.height + 50 + 90);
    } completion:^(BOOL success) {
        // set can touch when finish animation
        canTouch = true;
        _isShowMenu = false;
    }];
    
    // Get filter of view
    id filter = filterView.filter;
    
    //Rest filter and forceProcessingFull size
    [_videoCamera removeTarget:filter];
    [filter removeTarget:filterView];
    
    filter = [[[_filters objectAtIndex:filterView.tag] alloc] init];
    [filter forceProcessingAtSize:CGSizeMake(self.frame.size.width, self.frame.size.height + 50 + 90)];
    [filter addTarget:filterView];
    filterView.filter = filter;
    [_videoCamera addTarget:filter];
    
    // Call delegate to update layout in camera view controller
    if (_delegate && [_delegate respondsToSelector:@selector(didSelectFilter:)]) {
        [_delegate didSelectFilter: [_filters objectAtIndex:filterView.tag]];
    }
    
}

-(void)showFilterMenu {

    _isShowMenu = true;
    PreviewFilterGPUImageView* filterView = (PreviewFilterGPUImageView*)[cameraViewFilter objectAtIndex:_curFilter];
    UIView* nameView = [filterView viewWithTag:536];
    nameView.hidden = false;
    CGFloat posX = filterView.rPosition.x;
    CGFloat posY = filterView.rPosition.y;
    [UIView animateWithDuration:0.3 animations:^{
        filterView.frame = CGRectMake(posX, posY, widthFilterItem, heightFilterItem);
    } completion:^(BOOL success) {
        
    }];
    id filter = filterView.filter;
    
    [_videoCamera removeTarget:filter];
    [filter removeTarget:filterView];
    
    filter = [[[_filters objectAtIndex:filterView.tag] alloc] init];
    [filter forceProcessingAtSize:CGSizeMake(widthFilterItem, heightFilterItem)];
    [filter addTarget:filterView];
    filterView.filter = filter;
    [_videoCamera addTarget:filter];
    
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectFilterView:)];
    [filterView addGestureRecognizer:tapGesture];
}

@end
