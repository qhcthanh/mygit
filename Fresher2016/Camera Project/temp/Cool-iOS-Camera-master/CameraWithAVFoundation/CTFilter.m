//
//  CTCIFilter.m
//  CameraGPUImage
//
//  Created by BaoNQ on 6/13/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "CTFilter.h"

@implementation CTFilter

+ (CTFilter *)filterImage:(CIImage *)image withFilterName:(CTFilterName)filterName {
    CTFilter *result = [[CTFilter alloc] init];
    
    switch (filterName) {
        case CTColorMonochrome:
            result.outputImage = [CTFilter filterWithColorMonochrome:image];
            break;
            
        case CTToneCurve:
            result.outputImage = [CTFilter filterWithToneCurve:image];
            break;
            
        case CTBlur:
            result.outputImage = [CTFilter filterWithBlur:image];
            break;
            
        default:
            result = nil;
            break;
    }
    return result;
}

- (BOOL)clear {
    if (self.outputImage == nil)
        return NO;
    self.outputImage = nil;
    return YES;
}


// Private methods

+ (CIImage *)filterWithColorMonochrome:(CIImage *)ciImage {
    CIFilter *filteredImage = [CIFilter filterWithName:@"CIColorMonochrome" keysAndValues: kCIInputImageKey, ciImage, @"inputIntensity", @0.8, nil];
    return filteredImage.outputImage;
}

+ (CIImage *)filterWithToneCurve:(CIImage *)ciImage {
    CIFilter *toneCurveFilter = [CIFilter filterWithName:@"CIToneCurve"];
    [toneCurveFilter setDefaults];
    [toneCurveFilter setValue:ciImage forKey:kCIInputImageKey];
    [toneCurveFilter setValue:[CIVector vectorWithX:0.0  Y:0.0] forKey:@"inputPoint0"]; // default
    [toneCurveFilter setValue:[CIVector vectorWithX:0.27 Y:0.26] forKey:@"inputPoint1"];
    [toneCurveFilter setValue:[CIVector vectorWithX:0.5  Y:0.80] forKey:@"inputPoint2"];
    [toneCurveFilter setValue:[CIVector vectorWithX:0.7  Y:1.0] forKey:@"inputPoint3"];
    [toneCurveFilter setValue:[CIVector vectorWithX:1.0  Y:1.0] forKey:@"inputPoint4"]; // default
    
    return toneCurveFilter.outputImage;
}

+ (CIImage *)filterWithBlur:(CIImage *)ciImage {
    CIFilter * blurFilter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [blurFilter setValue:ciImage forKey:kCIInputImageKey];
    
    return blurFilter.outputImage;
}

@end
