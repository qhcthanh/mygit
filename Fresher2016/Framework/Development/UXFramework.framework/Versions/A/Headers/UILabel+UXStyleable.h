/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@class UXCSSRuleset;
@class UXDOM;

@interface UILabel (UXStyleable)

/**
 * Applies the given rule set to this label.
 *
 * This method is exposed primarily for subclasses to use when implementing the
 * applyStyleWithRuleSet: method from UXStyleable.
 */
- (void)applyLabelStyleWithRuleSet:(UXCSSRuleset *)ruleSet DEPRECATED_ATTRIBUTE;

/**
 * Applies the given rule set to this label.
 *
 * This method is exposed primarily for subclasses to use when implementing the
 * applyStyleWithRuleSet: method from UXStyleable.
 */
- (void)applyLabelStyleWithRuleSet:(UXCSSRuleset *)ruleSet inDOM: (UXDOM*) dom;

/**
 * Applies the given rule set to this label.
 *
 * This method is exposed primarily for subclasses to use when implementing the
 * applyStyleWithRuleSet: method from UXStyleable. Since some of the view
 * styles (e.g. positioning) may rely on some label elements (like text), this is called
 * before the view styling is done.
 */
- (void)applyLabelStyleBeforeViewWithRuleSet:(UXCSSRuleset *)ruleSet inDOM: (UXDOM*) dom;

@end

