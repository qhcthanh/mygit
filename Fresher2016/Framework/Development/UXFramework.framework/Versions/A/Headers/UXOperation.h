/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXPreprocessor.h"

@class  UXOperation;

typedef void (^ UXOperationBlock)( UXOperation* operation);
typedef void (^ UXOperationDidFailBlock)( UXOperation* operation, NSError* error);

/**
 * For writing code that runs concurrently.
 *
 * @ingroup UXFrameworkCore
 * @defgroup Operations Operations
 *
 * This collection of NSOperation implementations is meant to provide a set of common
 * operations that might be used in an application to offload complex processing to a separate
 * thread.
 */

@protocol  UXOperationDelegate;

/**
 * A base implementation of an NSOperation that supports traditional delegation and blocks.
 *
 * <h2>Subclassing</h2>
 *
 * A subclass should call the operationDid* methods to notify the delegate on the main thread
 * of changes in the operation's state. Calling these methods will notify the delegate and the
 * blocks if provided.
 *
 * @ingroup Operations
 */
@interface  UXOperation : NSOperation

@property (weak) id< UXOperationDelegate> delegate;
@property (readonly, strong) NSError* lastError;
@property (assign) NSInteger tag;

@property (copy)  UXOperationBlock didStartBlock;
@property (copy)  UXOperationBlock didFinishBlock;
@property (copy)  UXOperationDidFailBlock didFailWithErrorBlock;
@property (copy)  UXOperationBlock willFinishBlock;

- (void)didStart;
- (void)didFinish;
- (void)didFailWithError:(NSError *)error;
- (void)willFinish;

@end

/**
 * The delegate protocol for an  UXOperation.
 *
 * @ingroup Operations
 */
@protocol  UXOperationDelegate <NSObject>
@optional

/** @name [ UXOperationDelegate] State Changes */

/** The operation has started executing. */
- (void)operationDidStart:( UXOperation *)operation;

/**
 * The operation is about to complete successfully.
 *
 * This will not be called if the operation fails.
 *
 * This will be called from within the operation's runloop and must be thread safe.
 */
- (void)operationWillFinish:( UXOperation *)operation;

/**
 * The operation has completed successfully.
 *
 * This will not be called if the operation fails.
 */
- (void)operationDidFinish:( UXOperation *)operation;

/**
 * The operation failed in some way and has completed.
 *
 * operationDidFinish: will not be called.
 */
- (void)operationDidFail:( UXOperation *)operation withError:(NSError *)error;

@end


//  UXOperation

/** @name Delegation */

/**
 * The delegate through which changes are notified for this operation.
 *
 * All delegate methods are performed on the main thread.
 *
 * @fn  UXOperation::delegate
 */


/** @name Post-Operation Properties */

/**
 * The error last passed to the didFailWithError notification.
 *
 * @fn  UXOperation::lastError
 */


/** @name Identification */

/**
 * A simple tagging mechanism for identifying operations.
 *
 * @fn  UXOperation::tag
 */


/** @name Blocks */

/**
 * The operation has started executing.
 *
 * Performed on the main thread.
 *
 * @fn  UXOperation::didStartBlock
 */

/**
 * The operation has completed successfully.
 *
 * This will not be called if the operation fails.
 *
 * Performed on the main thread.
 *
 * @fn  UXOperation::didFinishBlock
 */

/**
 * The operation failed in some way and has completed.
 *
 * didFinishBlock will not be executed.
 *
 * Performed on the main thread.
 *
 * @fn  UXOperation::didFailWithErrorBlock
 */

/**
 * The operation is about to complete successfully.
 *
 * This will not be called if the operation fails.
 *
 * Performed in the operation's thread.
 *
 * @fn  UXOperation::willFinishBlock
 */


/**
 * @name Subclassing
 *
 * The following methods are provided to aid in subclassing and are not meant to be
 * used externally.
 */

/**
 * On the main thread, notify the delegate that the operation has begun.
 *
 * @fn  UXOperation::didStart
 */

/**
 * On the main thread, notify the delegate that the operation has finished.
 *
 * @fn  UXOperation::didFinish
 */

/**
 * On the main thread, notify the delegate that the operation has failed.
 *
 * @fn  UXOperation::didFailWithError:
 */

/**
 * In the operation's thread, notify the delegate that the operation will finish successfully.
 *
 * @fn  UXOperation::willFinish
 */

