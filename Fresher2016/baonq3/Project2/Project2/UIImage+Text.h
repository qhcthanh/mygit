//
//  UIImage+Text.h
//  Project2
//
//  Created by BaoNQ on 6/6/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage (Text)

/**
 *  Create an return an image object with the specified text, scale and colors
 *
 *  @param text            This text is at the center of the image
 *  @param size            The size of image
 *  @param textColor       Color of text
 *  @param backgroundColor Color of background
 *
 *  @return An image object with the specified text, scale and colors
 */
+ (UIImage *)imageWithText:(NSString *)text imageSize:(CGSize)size textColor:(UIColor *)textColor backgroundColor:(UIColor *)backgroundColor;

@end
