//
//  PreiviewFilterRealTimeView.h
//  CameraGPUImage
//
//  Created by qhcthanh on 6/30/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PreviewFilterCell.h"
#import "InstaFilters.h"
@protocol PreiviewFilterRealTimeViewDelegate <NSObject>

@optional
-(void)didSelectFilter:(id)filter;

@end

@interface PreiviewFilterRealTimeView : UIView {
    GPUImageView *view1, *view2, *view3, *view4;
    
}

@property GPUImageStillCamera *videoCamera;
@property (nonatomic,weak) id<PreiviewFilterRealTimeViewDelegate> delegate;
@property BOOL isShowMenu;

-(id)initWithCamera:(GPUImageStillCamera*)videoCamera frame:(CGRect)frame filters:(id)filters;
-(void)showFilterMenu;
-(void)showFilterSelect;

@end
