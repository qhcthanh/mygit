/* Copyright (c) 2015-Zalo, VNG Corp.
 * Copyright by Terry Worona. Forked by Zalo, Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 *  Three base message bar types. Their look & feel is defined within the MessageBarStyleSheet.
 */
typedef NS_ENUM(NSInteger, UXMessageBarMessageType) {
    UXMessageBarMessageTypeError,
    UXMessageBarMessageTypeSuccess,
    UXMessageBarMessageTypeInfo
};

@protocol UXMessageBarStyleSheet <NSObject>

/**
 *  Background color of message view.
 *
 *  @param type A MessageBarMessageType (error, information, success, etc).
 *
 *  @return UIColor istance representing the message view's background color.
 */
- (nonnull UIColor *)backgroundColorForMessageType:(UXMessageBarMessageType)type;

/**
 *  Bottom stroke color of message view.
 *
 *  @param type A MessageBarMessageType (error, information, success, etc).
 *
 *  @return UIColor istance representing the message view's bottom stroke color.
 */
- (nonnull UIColor *)strokeColorForMessageType:(UXMessageBarMessageType)type;

/**
 *  Icon image of the message view.
 *
 *  @param type A MessageBarMessageType (error, information, success, etc).
 *
 *  @return UIImage istance representing the message view's icon.
 */
- (nonnull UIImage *)iconImageForMessageType:(UXMessageBarMessageType)type;

@optional

/**
 *  The (optional) UIFont to be used for the message's title.
 *
 *  Default: 16pt bold
 *
 *  @param type A MessageBarMessageType (error, information, success, etc).
 *
 *  @return UIFont instance representing the title font.
 */
- (nonnull UIFont *)titleFontForMessageType:(UXMessageBarMessageType)type;

/**
 *  The (optional) UIFont to be used for the message's description.
 *
 *  Default: 14pt regular
 *
 *  @param type A MessageBarMessageType (error, information, success, etc).
 *
 *  @return UIFont instance representing the description font.
 */
- (nonnull UIFont *)descriptionFontForMessageType:(UXMessageBarMessageType)type;

/**
 *  The (optional) UIColor to be used for the message's title.
 *
 *  Default: white
 *
 *  @param type A MessageBarMessageType (error, information, success, etc).
 *
 *  @return UIColor instance representing the title color.
 */
- (nonnull UIColor *)titleColorForMessageType:(UXMessageBarMessageType)type;

/**
 *  The (optional) UIColor to be used for the message's description.
 *
 *  Default: white
 *
 *  @param type A MessageBarMessageType (error, information, success, etc).
 *
 *  @return UIColor instance representing the description color.
 */
- (nonnull UIColor *)descriptionColorForMessageType:(UXMessageBarMessageType)type;

@end

@interface UXMessageBarManager : NSObject

/**
 *  Singleton instance through which all presentation is managed.
 *
 *  @return MessageBarManager instance (singleton).
 */
+ (nonnull UXMessageBarManager *)sharedUXMessageBarManager;

/**
 *  Default display duration for each message.
 *  This can be customized on a per-message basis (see presentation functions below).
 *
 *  @return Default display duration (3 seconds).
 */
+ (CGFloat)defaultDuration;

/**
 *  Flag indicating if message is currently visible on screen.
 */
@property (nonatomic, readonly, getter = isMessageVisible) BOOL messageVisible;

/**
 *  The orientations supported by the manager. 
 *  In most cases, this value will match the caller's orientation mask. 
 *
 *  @return Default behaviour - all orientations.
 */
@property (nonatomic, assign) UIInterfaceOrientationMask managerSupportedOrientationsMask;

/**
 *  An object conforming to the UXMessageBarStyleSheet protocol defines the message bar's look and feel.
 *  If no style sheet is supplied, a default class is provided on initialization (see implementation for details).
 */
@property (nonnull, nonatomic, strong) NSObject<UXMessageBarStyleSheet> *styleSheet;

/**
 *  Shows a message with the supplied title, description and type.
 *
 *  @param title        Header text in the message view.
 *  @param description  Description text in the message view.
 *  @param type         Type dictates color, stroke and icon shown in the message view.
 */
- (void)showMessageWithTitle:(nullable NSString *)title description:(nullable NSString *)description type:(UXMessageBarMessageType)type;

/**
 *  Shows a message with the supplied title, description, type & callback block.
 *
 *  @param title        Header text in the message view.
 *  @param description  Description text in the message view.
 *  @param type         Type dictates color, stroke and icon shown in the message view.
 *  @param callback     Callback block to be executed if a message is tapped.
 */
- (void)showMessageWithTitle:(nullable NSString *)title description:(nullable NSString *)description type:(UXMessageBarMessageType)type callback:(nullable void (^)())callback;

/**
 *  Shows a message with the supplied title, description, type & duration.
 *
 *  @param title        Header text in the message view.
 *  @param description  Description text in the message view.
 *  @param type         Type dictates color, stroke and icon shown in the message view.
 *  @param duration     Default duration is 3 seconds, this can be overridden by supplying an optional duration parameter.
 */
- (void)showMessageWithTitle:(nullable NSString *)title description:(nullable NSString *)description type:(UXMessageBarMessageType)type duration:(CGFloat)duration;

/**
 *  Shows a message with the supplied title, description, type, duration and callback block.
 *
 *  @param title        Header text in the message view.
 *  @param description  Description text in the message view.
 *  @param type         Type dictates color, stroke and icon shown in the message view.
 *  @param duration     Default duration is 3 seconds, this can be overridden by supplying an optional duration parameter.
 *  @param callback     Callback block to be executed if a message is tapped.
 */
- (void)showMessageWithTitle:(nullable NSString *)title description:(nullable NSString *)description type:(UXMessageBarMessageType)type duration:(CGFloat)duration callback:(nullable void (^)())callback;

/**
 *  Shows a message with the supplied title, description, type, status bar style and callback block.
 *
 *  @param title            Header text in the message view.
 *  @param description      Description text in the message view.
 *  @param type             Type dictates color, stroke and icon shown in the message view.
 *  @param statusBarStyle   Applied during the presentation of the message. If not supplied, style will default to UIStatusBarStyleDefault.
 *  @param callback         Callback block to be executed if a message is tapped.
 */
- (void)showMessageWithTitle:(nullable NSString *)title description:(nullable NSString *)description type:(UXMessageBarMessageType)type statusBarStyle:(UIStatusBarStyle)statusBarStyle callback:(nullable void (^)())callback;

/**
 *  Shows a message with the supplied title, description, type, duration, status bar style and callback block.
 *
 *  @param title            Header text in the message view.
 *  @param description      Description text in the message view.
 *  @param type             Type dictates color, stroke and icon shown in the message view.
 *  @param duration         Default duration is 3 seconds, this can be overridden by supplying an optional duration parameter.
 *  @param statusBarStyle   Applied during the presentation of the message. If not supplied, style will default to UIStatusBarStyleDefault.
 *  @param callback         Callback block to be executed if a message is tapped.
 */
- (void)showMessageWithTitle:(nullable NSString *)title description:(nullable NSString *)description type:(UXMessageBarMessageType)type duration:(CGFloat)duration statusBarStyle:(UIStatusBarStyle)statusBarStyle callback:(nullable void (^)())callback;

/**
 *  Shows a message with the supplied title, description, type, status bar hidden toggle and callback block.
 *
 *  @param title            Header text in the message view.
 *  @param description      Description text in the message view.
 *  @param type             Type dictates color, stroke and icon shown in the message view.
 *  @param statusBarHidden  Status bars are shown by default. To hide it during the presentation of a message, set to NO.
 *  @param callback         Callback block to be executed if a message is tapped.
 */
- (void)showMessageWithTitle:(nullable NSString *)title description:(nullable NSString *)description type:(UXMessageBarMessageType)type statusBarHidden:(BOOL)statusBarHidden callback:(nullable void (^)())callback;

/**
 *  Shows a message with the supplied title, description, type, duration, status bar hidden toggle and callback block.
 *
 *  @param title            Header text in the message view.
 *  @param description      Description text in the message view.
 *  @param type             Type dictates color, stroke and icon shown in the message view.
 *  @param duration         Default duration is 3 seconds, this can be overridden by supplying an optional duration parameter.
 *  @param statusBarHidden  Status bars are shown by default. To hide it during the presentation of a message, set to NO.
 *  @param callback         Callback block to be executed if a message is tapped.
 */
- (void)showMessageWithTitle:(nullable NSString *)title description:(nullable NSString *)description type:(UXMessageBarMessageType)type duration:(CGFloat)duration statusBarHidden:(BOOL)statusBarHidden callback:(nullable void (^)())callback;

/**
 *  Hides the topmost message and removes all remaining messages in the queue.
 *
 *  @param animated     Animates the current message view off the screen.
 */
- (void)hideAllAnimated:(BOOL)animated;
- (void)hideAll; // non-animated

@end