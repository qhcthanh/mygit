//
//  PreviewVideoViewController.m
//  CameraGPUImage
//
//  Created by qhcthanh on 6/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "PreviewVideoViewController.h"

@implementation PreviewVideoViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    // Ad preview video layer
    UIView* previewVideoView = [[UIView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:previewVideoView];
    if (_avPlayer) {
        AVPlayerLayer *layer = [AVPlayerLayer playerLayerWithPlayer:_avPlayer];
        _avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        layer.frame = self.view.bounds;
        [previewVideoView.layer addSublayer: layer];
        [_avPlayer play];
        [self performSelector:@selector(pauseVideo) withObject:nil afterDelay:0.2];
        
    }
    
    //Back button
    UIButton* backButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 30, self.view.frame.size.height - 100, 60, 40)];
    [backButton setTitle:@"Back" forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    
    //Add play/stop button
    UIButton* playButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0, 200, 80)];
    playButton.center = self.view.center;
    [playButton setTitle:@"Play" forState:UIControlStateNormal];
    [playButton addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:playButton];
    
}

-(void)playVideo:(id)sender {
    if (_avPlayer) {
        ((UIButton*)sender).hidden = true;
        [_avPlayer play];
    }
}

-(void)pauseVideo {
    if (_avPlayer)
        [_avPlayer pause];
}

-(void)backAction {
    [self dismissViewControllerAnimated:true completion:nil];
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:true];
}


@end
