//
//  EmojiView.swift
//  EmojiKeyboard
//
//  Created by Quach Ha Chan Thanh on 8/13/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import Foundation
import UIKit

internal let emojiMinimumLineSpacing: CGFloat = 5
internal let emojiMinimumItemSpacing: CGFloat = 5

public let customLightGrayColor = UIColor(red: 210.0/255, green: 210.0/255, blue: 210.0/255, alpha: 1)
public let customDarkGrayColor = UIColor(red: 195.0/255, green: 195.0/255, blue: 195.0/255, alpha: 1)

@objc public protocol EmojiViewDelegate: NSObjectProtocol {

    optional func didTouchReturn() // The delegate called when use tap in retrun button
    
    optional func didTouchBackspace() // The delegate called when use tap in backspace button
    
    func didSelectEmoji(emoji: String) // The delegate called when use tap in emoji
}

public class EmojiView: UIView, EmojiCollectionViewCellDelegate, PopoverDelegate {
    
    // Public properties
    public var titleLabel: UILabel!
    public var emojiCollectionView: UICollectionView!
    public var emojiCategoryCollectionView: UICollectionView!
    public var returnButton: UIButton!
    public var backspaceButton: UIButton!
    
    public var emojiCollectionViewLayout = UICollectionViewFlowLayout()
    public var emojiCategoryCollectionViewLayout = UICollectionViewFlowLayout()
    
    public private(set) var emojiManager: EmojiManager = EmojiManager.shareInstance()
    
    weak public var delegate: EmojiViewDelegate?
    
    //weak var delegate: EmojiCategoryCollectionViewCellDelegate?
    
    // Setting
    
    var minimumPressDuration: Double = 0.06
    var minimumTimeShowPopup: Double = 0.5
    
    let options = [
        .Type(.Up),
        .CornerRadius(15),
        .SideEdge(25),
        .AnimationIn(0),
        .AnimationOut(0),
        .ArrowSize(CGSizeZero),
        .BlackOverlayColor(UIColor.clearColor()),
        .Color(customLightGrayColor),
        .ArrowSize(CGSizeMake(14.0, 12.0))
        ] as [PopoverOption]
    
    // Last Cell selected property
    private weak var lastCellPress: UICollectionViewCell? {
        willSet {
            if let newValue = newValue {
                if newValue != self.lastCellPress {
                    timerShowPopup.invalidate()
                    if let newValue = newValue as? EmojiCollectionViewCell {
                        self.willShowPopupEmoji(newValue)
                    }
                }
            } else {
                self.popoverView.dismiss()
                self.popoverView.delegate = nil
                timerShowPopup.invalidate()
            }
            
            self.lastCellPress = newValue
        }
    }
    
    var timerShowPopup: NSTimer = NSTimer()
    var popoverView: Popover = Popover()
    var isShowPopoverView = false
    let viewPopup = UIView()
    
    func showPopupInView(fromView: UIView, customView: UIView, size: CGSize) {
        
        popoverView.dismiss()
        popoverView.delegate = nil
        
        customView.frame.size = size
        
        popoverView = Popover(options: options, showHandler: nil, dismissHandler: nil)
        popoverView.delegate = self
        popoverView.show(customView, fromView: fromView)
    }
    
    func willShowPopupEmoji(emojiCell: UICollectionViewCell) {
        timerShowPopup = NSTimer.scheduledTimerWithTimeInterval(self.minimumTimeShowPopup, target: self, selector: #selector(didShowPupupEmoji), userInfo: ["emoji":emojiCell], repeats: false)
    }
    
    func didShowPupupEmoji() {
        if let emojiCell = self.timerShowPopup.userInfo!["emoji"] as? EmojiCollectionViewCell {
            if let popupView = emojiCell.generatePopupView() {
                self.showPopupInView(emojiCell ,customView: popupView, size: CGSizeMake(emojiItemCellSize.width * 5 + 10, emojiItemCellSize.width + 10))
            }
        }
        
    }
    
    public func didDismissPopupOver(popover: Popover) {
        if let cell = self.lastCellPress {
            cell.contentView.backgroundColor = .clearColor()
        }
    }
    
    // Private properties
    private var selectCricleView: UIView!
    
    var emojiItemCellSize: CGSize {
        get {
            
            let itemCellWidth = min(self.bounds.width * 0.125, self.bounds.height * 0.2)
            return CGSizeMake(itemCellWidth,itemCellWidth)
        }
    }
    
    // MARK: Initialize
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setup()
    }
    
    
    private func setup() {
        self.titleLabel = UILabel()
        self.returnButton = UIButton.init(type: .System)
        self.backspaceButton = UIButton.init(type: .System)
        
        self.selectCricleView = UIView(frame: CGRectMake(0,0,25,25))
        self.selectCricleView.layer.cornerRadius = 12
        self.selectCricleView.backgroundColor = customLightGrayColor.colorWithAlphaComponent(0.9)
        
        self.emojiCollectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: self.emojiCollectionViewLayout)
        self.emojiCategoryCollectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: self.emojiCategoryCollectionViewLayout)
        
        // Add subview
        self.addSubview(titleLabel)
        self.addSubview(returnButton)
        self.addSubview(backspaceButton)
        self.addSubview(emojiCollectionView)
        self.addSubview(emojiCategoryCollectionView)
        
        // Update UI
        self.updateUI()
        self.setupEmojiCollectionView()
        self.setupEmojiCategoryCollectionView()
        
        // Settup target to backspaceButton & returnButton
        self.setupTargetButton()
        
        setupConstraints()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(didChangedDeviceOrientation), name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    func setupConstraints() {
        // Set constrain
        
        // Set contrain to titleView (height = 0.1 * superView, and in top left superview)
        self.titleLabel.constrain(.Height, being: .Equal, to: .Height, of: self, multipliedBy: 0.1)
        self.titleLabel.constrain(.Left, to: .Left, of: self, offsetBy: 5)
        self.titleLabel.constrain(.Top, to: .Top, of: self)
        
        // Set returnButton is bottom left of supperView and width = 0.15
        self.returnButton.constrain(.Height, being: .Equal, to: .Height, of: self, multipliedBy: 0.125)
        self.returnButton.constrain(.Width, being: .Equal, to: .Width, of: self, multipliedBy: 0.08)
        self.returnButton.constrain(.Left, to: .Left, of: self, offsetBy: 5)
        self.returnButton.constrain(.Bottom, to: .Bottom, of: self)
        
        // Set backSpaceButton is bottom right of supperView and width = return button
        self.backspaceButton.constrain(.Height, to: .Height, of: returnButton)
        self.backspaceButton.constrain(.Width, to: .Width, of: returnButton)
        self.backspaceButton.constrain(.Right, to: .Right, of: self, offsetBy: -5)
        self.backspaceButton.constrain(.Bottom, to: .Bottom, of: self)
        
        // Set collectionView is between title and return button
        self.emojiCollectionView.constrain(.Right, to: .Right, of: self)
        self.emojiCollectionView.constrain(.Left, to: .Left, of: self)
        self.emojiCollectionView.constrain(.Top, to: .Bottom, of: titleLabel)
        self.emojiCollectionView.constrain(.Bottom, to: .Top, of: returnButton, offsetBy: 5)
        
        // Set collectionView is between title and return button
        self.emojiCategoryCollectionView.constrain(.Right, to: .Left, of: backspaceButton)
        self.emojiCategoryCollectionView.constrain(.Left, to: .Right, of: returnButton, offsetBy: 5)
        self.emojiCategoryCollectionView.constrain(.Top, to: .Top, of: backspaceButton)
        self.emojiCategoryCollectionView.constrain(.Bottom, to: .Bottom, of: backspaceButton)

    }
    
    func didChangedDeviceOrientation() {
        self.emojiCollectionView.reloadData()
        self.emojiCategoryCollectionView.reloadData()
    }
    
    private func setupTargetButton() {
        self.backspaceButton.addTarget(self, action: #selector(self.onTouchBackspace), forControlEvents: .TouchUpInside)
        self.returnButton.addTarget(self, action: #selector(self.onTouchReturn), forControlEvents: .TouchUpInside)
    }
    
    private func updateUI() {
        // Set first title of emoji category
        if let emojiCategory = self.emojiManager.emojiCategorys.first {
            self.titleLabel.text = emojiCategory.category
        }
        
        // Set UI titleButton
        self.titleLabel.textColor = .grayColor()
        
        // Set UI Return Button
        self.returnButton.setImage(UIImage(named: "Keyboard"), forState: .Normal)
        self.returnButton.tintColor = .blackColor()
        
        // Set UI Backspace Button
        self.backspaceButton.setImage(UIImage(named: "Backspace"), forState: .Normal)
        self.backspaceButton.tintColor = .blackColor()
        
        // Set UI EmojiCollectionView + EmojiCategory
        self.emojiCollectionView.backgroundColor = .clearColor()
        self.emojiCategoryCollectionView.backgroundColor = .clearColor()
           
    }
    
    private func setupEmojiCollectionView() {
        
        //self.emojiCollectionView.registerClass(EmojiCategoryCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: "EmojiCategoryCell")
        let emojiItemCellNib = UINib(nibName: "EmojiCollectionViewCell", bundle: nil)
        self.emojiCollectionView.registerNib(emojiItemCellNib, forCellWithReuseIdentifier: "EmojiItemCell")
        
        self.emojiCollectionViewLayout.itemSize = CGSizeMake(self.bounds.width, self.bounds.height - 5)
        self.emojiCollectionViewLayout.minimumLineSpacing = 10
        self.emojiCollectionViewLayout.minimumInteritemSpacing = 0
        self.emojiCollectionViewLayout.scrollDirection = .Horizontal
        
        let testGesture = BLLongPressGesture(owerView: self.emojiCollectionView)
        testGesture.delegate = self
        
      //  self.emojiCollectionView.pagingEnabled = true
        self.emojiCollectionView.showsVerticalScrollIndicator = false
        self.emojiCollectionView.showsHorizontalScrollIndicator = false
        self.emojiCollectionView.clipsToBounds = false
        
        self.emojiCollectionView.delegate = self
        self.emojiCollectionView.dataSource = self
    
    }
    
    private func setupEmojiCategoryCollectionView() {
        
        let emojiItemCellNib = UINib(nibName: "EmojiCollectionViewCell", bundle: nil)
        self.emojiCategoryCollectionView.registerNib(emojiItemCellNib, forCellWithReuseIdentifier: "EmojiItemCell")
        
        self.emojiCategoryCollectionViewLayout.itemSize = CGSizeMake(25,25)
        self.emojiCategoryCollectionViewLayout.minimumLineSpacing = 8
        self.emojiCategoryCollectionViewLayout.minimumInteritemSpacing = 5
        self.emojiCategoryCollectionViewLayout.scrollDirection = .Horizontal
        
        self.emojiCategoryCollectionView.showsVerticalScrollIndicator = false
        self.emojiCategoryCollectionView.showsHorizontalScrollIndicator = false
        
        self.emojiCategoryCollectionView.delegate = self
        self.emojiCategoryCollectionView.dataSource = self
        
    }
    
    // MARK: Target
    
     func onTouchReturn(sender: UIButton!) {
        
        if let delegate = self.delegate,
            let returnFunc = delegate.didTouchReturn
        {
            returnFunc()
        }
    }
    
     func onTouchBackspace(sender: UIButton!) {
        
        if let delegate = self.delegate,
            let backspaceFunc = delegate.didTouchBackspace
        {
            backspaceFunc()
        }
    }
    
    func didSelectEmoji(emoji: String) {
        if let delegate = self.delegate {
            delegate.didSelectEmoji(emoji)
        }
    }
}

extension EmojiView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        if collectionView != self.emojiCollectionView {
            return 1
        }
        
        return emojiManager.emojiCategorys.count
    }
    
    public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView != self.emojiCollectionView {
            return emojiManager.emojiCategorys.count
        }
        
        return emojiManager.emojiCategorys[section].emojis.count
    }
    
    public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if collectionView == emojiCollectionView {
//            let emojiCell = collectionView.dequeueReusableCellWithReuseIdentifier("EmojiCategoryCell", forIndexPath: indexPath) as! EmojiCategoryCollectionViewCell
//        
//            let emojiCategory = emojiManager.emojiCategorys[indexPath.row]
//            emojiCell.bindingUI(emojiCategory)
//            emojiCell.delegate = self
            
            let emojiCell = collectionView.dequeueReusableCellWithReuseIdentifier("EmojiItemCell", forIndexPath: indexPath) as! EmojiCollectionViewCell
            let emojiProtocol = emojiManager.emojiCategorys[indexPath.section].emojis[indexPath.row]
            emojiCell.bindingUI(emojiProtocol)
            emojiCell.delegate = self
            
            return emojiCell
        } else {
            let emojiCategoryCell = collectionView.dequeueReusableCellWithReuseIdentifier("EmojiItemCell", forIndexPath: indexPath) as! EmojiCollectionViewCell
            
            let representCategory = emojiManager.emojiCategorys[indexPath.row].represent
            emojiCategoryCell.emojiLabel.adjustsFontSizeToFitWidth = true
            emojiCategoryCell.emojiLabel.text = representCategory
            
            if indexPath.row == 0 {
                emojiCategoryCell.backgroundView = self.selectCricleView
            }
            
            return emojiCategoryCell
        }
        
    }
    
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        if collectionView == self.emojiCollectionView {
            return self.emojiItemCellSize
        }
        return CGSizeMake(25,25)
    }
    
//    public func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
//        
//        let reusableView = UICollectionReusableView(frame: CGRectMake(0,0,collectionView.frame.height, 20))
//        return reusableView
//    }
//    
    
    public func scrollViewDidScroll(scrollView: UIScrollView) {
//        let row = Int(scrollView.contentOffset.x / scrollView.bounds.width)
//        if row >= 0 && row < self.emojiManager.emojiCategorys.count {
//            self.titleLabel.text = self.emojiManager.emojiCategorys[row].category
//            
//            self.highlightEmojiCategoryCell(NSIndexPath(forItem: row, inSection: 0))
//        }
    }
    
    public func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
        self.titleLabel.text = self.emojiManager.emojiCategorys[indexPath.section].category
        self.highlightEmojiCategoryCell(NSIndexPath(forItem: indexPath.section, inSection: 0))
    }

    public func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: NSInteger) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0,5, 0, 30)
    }
    
    public func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if collectionView == emojiCollectionView {
            
        } else {
            //self.highlightEmojiCategoryCell(indexPath)
            self.emojiCollectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .Left, animated: false)
        }
    }
    
    private func highlightEmojiCategoryCell(indexPath: NSIndexPath) {
        if let cell = emojiCategoryCollectionView.cellForItemAtIndexPath(indexPath) {
            if let _ = cell.backgroundView {
                cell.backgroundView = nil
            }
            if cell.backgroundView != self.selectCricleView {
                cell.backgroundView = self.selectCricleView
            }
        }
    }
    
}


extension EmojiView: BLLongPressGestureDelegate {
    
    public func longPressGestureOnTouchDown(view: UIView) {
        if let cell = view as? EmojiCollectionViewCell {
            if let lastCellPress = self.lastCellPress {
                lastCellPress.contentView.backgroundColor = .clearColor()
                self.lastCellPress = nil
            }
            
            cell.contentView.backgroundColor = customLightGrayColor
            self.lastCellPress = cell
            
        }
        
        
    }
    
    public func longPressGestureOnTouchUp(view: UIView) {
        if view is EmojiCollectionViewCell {
            if self.popoverView.isShow() {
                return
            }
            
            if let lastCellPress = self.lastCellPress
            {
                lastCellPress.contentView.backgroundColor = .clearColor()
                if let lastCellPress = lastCellPress as? EmojiCollectionViewCell,
                    let emojiProtocol = lastCellPress.itemProtocol {
                    
                    // Call delegate touch emoji
                    self.didSelectEmoji(emojiProtocol.titleContent)
                }
            }
            
            self.lastCellPress = nil
        }
    }
    
    public func longPressGestureOnTouchMove(view: UIView) {
        if let cell = view as? EmojiCollectionViewCell {
            // Kiem tra neu khong phai la cell cu thi doi mau bthg cell cu~ va to mau` cell moi
            if cell != self.lastCellPress {
                if let lastCellPress = self.lastCellPress {
                    lastCellPress.contentView.backgroundColor = .clearColor()
                }
                cell.contentView.backgroundColor = customLightGrayColor
                self.lastCellPress = cell
            }
            
        }
    }
    
    public func longPressGestureOnTouchFail(view: UIView) {
        if let lastCellPress = self.lastCellPress {
            lastCellPress.contentView.backgroundColor = .clearColor()
        }
        self.lastCellPress = nil
        
    }
    
}






