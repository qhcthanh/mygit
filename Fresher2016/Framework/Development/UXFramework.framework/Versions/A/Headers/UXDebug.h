/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

/**
 * For inspecting code and writing to logs in debug builds.
 *
 * Nearly all of the following macros will only do anything if the DEBUG macro is defined.
 * The recommended way to enable the debug tools is to specify DEBUG in the "Preprocessor Macros"
 * field in your application's Debug target settings. Be careful not to set this for your release
 * or app store builds because this will enable code that may cause your app to be rejected.
 *
 *
 * <h2>Debug Assertions</h2>
 *
 * Debug assertions are a lightweight "sanity check". They won't crash the app, nor will they
 * be included in release builds. They <i>will</i> halt the app's execution when debugging so
 * that you can inspect the values that caused the failure.
 *
 * @code
 *  UXDASSERT(statement);
 * @endcode
 *
 * If <i>statement</i> is false, the statement will be written to the log and if a debugger is
 * attached, the app will break on the assertion line.
 *
 *
 * <h2>Debug Logging</h2>
 *
 * @code
 *  UXDLOG(@"formatted log text %d", param1);
 * @endcode
 *
 * Print the given formatted text to the log.
 *
 * @code
 *  UXDLOGMETHODNAME();
 * @endcode
 *
 * Print the current method name to the log.
 *
 * @code
 *  UXDCONDITIONLOG(statement, @"formatted log text %d", param1);
 * @endcode
 *
 * If statement is true, then the formatted text will be written to the log.
 *
 * @code
 *  UXDINFO/UXDWARNING/UXDERROR(@"formatted log text %d", param1);
 * @endcode
 *
 * Will only write the formatted text to the log if UXMaxLogLevel is greater than the respective
 * UXD* method's log level. See below for log levels.
 *
 * The default maximum log level is UXLOGLEVEL_WARNING.
 *
 * <h3>Turning up the log level while the app is running</h3>
 *
 * UXMaxLogLevel is declared a non-const extern so that you can modify it at runtime. This can
 * be helpful for turning logging on while the application is running.
 *
 * @code
 *  UXMaxLogLevel = UXLOGLEVEL_INFO;
 * @endcode
 *
 * @ingroup UXFrameworkCore
 * @defgroup Debugging-Tools Debugging Tools
 * @{
 */

#if defined(DEBUG) || defined(UX_DEBUG)

/**
 * Assertions that only fire when DEBUG is defined.
 *
 * An assertion is like a programmatic breakpoint. Use it for sanity checks to save headache while
 * writing your code.
 */
#import <TargetConditionals.h>

#if defined __cplusplus
extern "C" {
#endif
    
    int UXIsInDebugger(void);
    
#if defined __cplusplus
}
#endif

#if TARGET_IPHONE_SIMULATOR
// We leave the __asm__ in this macro so that when a break occurs, we don't have to step out of
// a "breakInDebugger" function.
#define UXDASSERT(xx) { if (!(xx)) { UXDLOG(@"UXDASSERT failed: %s", #xx); \
if (UXDebugAssertionsShouldBreak && UXIsInDebugger()) { __asm__("int $3\n" : : ); } } \
} ((void)0)
#else
#define UXDASSERT(xx) { if (!(xx)) { UXDLOG(@"UXDASSERT failed: %s", #xx); \
if (UXDebugAssertionsShouldBreak && UXIsInDebugger()) { raise(SIGTRAP); } } \
} ((void)0)
#endif // #if TARGET_IPHONE_SIMULATOR

#else
#define UXDASSERT(xx) ((void)0)
#endif // #if defined(DEBUG) || defined(UX_DEBUG)


#define UXLOGLEVEL_INFO     5
#define UXLOGLEVEL_WARNING  3
#define UXLOGLEVEL_ERROR    1

/**
 * The maximum log level to output for UXFramework debug logs.
 *
 * This value may be changed at run-time.
 *
 * The default value is UXLOGLEVEL_WARNING.
 */
extern NSInteger UXMaxLogLevel;

/**
 * Whether or not debug assertions should halt program execution like a breakpoint when they fail.
 *
 * An example of when this is used is in unit tests, when failure cases are tested that will
 * fire debug assertions.
 *
 * The default value is YES.
 */
extern BOOL UXDebugAssertionsShouldBreak;

/**
 * Only writes to the log when DEBUG is defined.
 *
 * This log method will always write to the log, regardless of log levels. It is used by all
 * of the other logging methods in UXFramework' debugging library.
 */
#if defined(DEBUG) || defined(UX_DEBUG)
#define UXDLOG(xx, ...)  NSLog(@"%s(%d): " xx, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define UXDLOG(xx, ...)  ((void)0)
#endif // #if defined(DEBUG) || defined(UX_DEBUG)

/**
 * Write the containing method's name to the log using UXDLOG.
 */
#define UXDLOGMETHODNAME() UXDLOG(@"%s", __PRETTY_FUNCTION__)

#if defined(DEBUG) || defined(UX_DEBUG)
/**
 * Only writes to the log if condition is satisified.
 *
 * This macro powers the level-based loggers. It can also be used for conditionally enabling
 * families of logs.
 */
#define UXDCONDITIONLOG(condition, xx, ...) { if ((condition)) { UXDLOG(xx, ##__VA_ARGS__); } \
} ((void)0)
#else
#define UXDCONDITIONLOG(condition, xx, ...) ((void)0)
#endif // #if defined(DEBUG) || defined(UX_DEBUG)


/**
 * Only writes to the log if UXMaxLogLevel >= UXLOGLEVEL_ERROR.
 */
#define UXDERROR(xx, ...)  UXDCONDITIONLOG((UXLOGLEVEL_ERROR <= UXMaxLogLevel), xx, ##__VA_ARGS__)

/**
 * Only writes to the log if UXMaxLogLevel >= UXLOGLEVEL_WARNING.
 */
#define UXDWARNING(xx, ...)  UXDCONDITIONLOG((UXLOGLEVEL_WARNING <= UXMaxLogLevel), \
xx, ##__VA_ARGS__)

/**
 * Only writes to the log if UXMaxLogLevel >= UXLOGLEVEL_INFO.
 */
#define UXDINFO(xx, ...)  UXDCONDITIONLOG((UXLOGLEVEL_INFO <= UXMaxLogLevel), xx, ##__VA_ARGS__)

/**@}*/// End of Debugging Tools //////////////////////////////////////////////////////////////////
