//
//  Error.swift
//  testSuggestion
//
//  Created by manhduydl on 8/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

import Foundation

public struct Error {
    /// The domain
    public static let Domain = "com.vng.banlakey.error"
    
    /// The custom error codes
    public enum Code: Int {
        case FoundNotFile = 1
    }
    
    /// Custom keys contained within certain NSError `userInfo` dictionaries
    public struct UserInfoKeys {
        /// The content type user info key for a `.ContentTypeValidationFailed` error stored as a `String` value.
        public static let ContentType = "ContentType"
        
        /// The status code user info key for a `.StatusCodeValidationFailed` error stored as an `Int` value.
        public static let StatusCode = "StatusCode"
    }
    
    static func error(domain domain: String = Error.Domain, code: Code, failureReason: String) -> NSError {
        return error(domain: domain, code: code.rawValue, failureReason: failureReason)
    }
    
    static func error(domain domain: String = Error.Domain, code: Int, failureReason: String) -> NSError {
        let userInfo = [NSLocalizedFailureReasonErrorKey: failureReason]
        return NSError(domain: domain, code: code, userInfo: userInfo)
    }
}