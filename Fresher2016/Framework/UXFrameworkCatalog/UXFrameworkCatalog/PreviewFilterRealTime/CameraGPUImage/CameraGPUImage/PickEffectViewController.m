//
//  PickEffectViewController.m
//  CameraGPUImage
//
//  Created by qhcthanh on 6/23/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "PickEffectViewController.h"
#import "EffectCell.h"
@interface PickEffectViewController() <UICollectionViewDelegate, UICollectionViewDataSource>

@end

@implementation PickEffectViewController {
    NSMutableArray *imageList;
    __weak IBOutlet UICollectionView *effectCollectionView;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    effectCollectionView.allowsSelection = true;
    
    imageList = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < self.filterList.count; ++i) {
        NSString *imageName = NSStringFromClass([self.filterList objectAtIndex:i]);
        [imageList addObject:[UIImage imageNamed:imageName]];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

//MARK: UICollectionView Delegate + UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _filterList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    EffectCell* cell = (EffectCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"EffectCell" forIndexPath:indexPath];    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(EffectCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {

    NSString *imageName = NSStringFromClass([self.filterList objectAtIndex:[indexPath row]]);
    [cell renderUI:[imageList objectAtIndex:[indexPath row]] name:imageName];
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if( _effectDelegate && [_effectDelegate respondsToSelector:@selector(didSelectFilter:)]) {
        [_effectDelegate didSelectFilter:[[[_filterList objectAtIndex:indexPath.row] alloc] init]];
    }
}

@end
