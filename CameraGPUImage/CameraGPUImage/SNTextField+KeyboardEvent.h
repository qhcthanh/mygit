//
//  SNTextField+KeyboardEvent.h
//  CameraGPUImage
//
//  Created by BaoNQ on 8/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SNTextField.h"

@interface SNTextField (KeyboardEvent)

- (void)keyboardWillShow:(NSNotification *)notification;

- (void)keyboardTypeChanged:(NSNotification *)notification;

- (void)keyboardWillHide:(NSNotification *)notification;

- (void)updatePosition:(NSNotification *)notification;

@end
