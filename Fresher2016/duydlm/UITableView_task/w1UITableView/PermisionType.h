//
//  PermisionType.h
//  w1UITableView
//
//  Created by manhduydl on 6/1/16.
//  Copyright © 2016 admin. All rights reserved.
//

#ifndef PermisionType_h
#define PermisionType_h

typedef NS_ENUM(NSUInteger, MDAccessContactPermition)
{
    MDAccessContactPermitionUnknown = 0,
    MDAccessContactPermitionGranted = 1,
    MDAccessContactPermitionDenied  = 2
};

#endif /* PermisionType_h */
