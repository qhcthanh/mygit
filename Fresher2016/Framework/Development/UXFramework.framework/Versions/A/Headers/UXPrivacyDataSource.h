/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSUInteger, UXPrivacyType){
    UXPrivacyTypePhoto, // Photos within camera roll
    UXPrivacyTypeCamera, // Use the Camera
    UXPrivacyTypeLocation, // Location Services
    UXPrivacyTypeHealth, // HealthKit
    UXPrivacyTypeHomeKit, // Motion Activity
    UXPrivacyTypeMotionActivity,
    UXPrivacyTypeContacts, // Access to Contacts
    UXPrivacyTypeNotifications, // Push Notifications
    UXPrivacyTypeReminders, // Reminders
    UXPrivacyTypeCalendars, // Calendars
    UXPrivacyTypeMicrophone, // Microphone
    UXPrivacyTypeTwitter, // Twitter
    UXPrivacyTypeFacebook // Facebook
};

@interface UXPrivacyDataSource : NSObject <UITableViewDataSource, UITableViewDelegate>

/**
 *  Personal App Icon name for UXPrivacyTypeNotifications
 */
@property (nonatomic, strong) NSString *appIcon;

/**
 *  DBPrivacyHelper data dictionary
 */
@property (nonatomic, readonly) NSDictionary *cellData;

/**
 *  DBPrivacy type selected
 */
@property (nonatomic, assign) UXPrivacyType type;

@end
