//
//  ContactProtocol.h
//  ZaloContactInviting
//
//  Created by VanDao on 6/6/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactDetail.h"

@protocol ContactProtocol <NSObject>
/**
 *
 *
 *  @param newContact The contact
 */
- (void)getNewContact:(ContactDetail *)newContact;

@end
