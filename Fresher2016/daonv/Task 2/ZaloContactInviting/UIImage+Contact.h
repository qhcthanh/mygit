//
//  UIImage+Contact.h
//  ZaloContactInviting
//
//  Created by VanDao on 6/6/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage (Contact)

/**
 *  Create an UIImage Object by first letters of person's name.
 *
 *  @param letters NSString contain firest letters of given name and family name of person
 *
 *  @return UIImage
 */
+ (UIImage * _Nonnull)createImageFromLetters:(NSString * _Nullable)letters;

/**
 *  Crop an rectange image to circle image using circle mask image
 *
 *  @param mask Original image
 *
 *  @return Circle mask image
 */
+ (UIImage * _Nonnull)clipToCircleImage:(UIImage * _Nonnull)image withMask:(UIImage * _Nonnull)maskImage;

@end
