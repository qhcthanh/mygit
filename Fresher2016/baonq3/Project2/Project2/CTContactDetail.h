//
//  CTContactDetail.h
//  Project2
//
//  Created by BaoNQ on 5/23/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ContactImageCaching.h"
#import "UIImage+Text.h"
#import "UIImage+Mask.h"

/**
 *  To get the profile image of contact, call getProfileImageWithCompletionHandler. This will not block the app while the device is being processed to get the profile image of the contact. When the process is finished, profileImage will contains the image of contact if the profile image is created successfully and error is nil. Otherwise, error contains the error code and prodileImage will be nil. The completion handler is called on the queue, which you pass to the getProfileImageWithCompletionHandler function.
 *
 *  @param UIImage The profile image of this contact.
 *  @param NSError If profile image is created successfully, the error will be nil. Otherwise, it contains the error code.
 */
typedef void(^GetProfileImageCompletionHandler)(UIImage *profileImage, NSError *error);

@interface CTContactDetail : NSObject

@property (nonatomic) NSString *fullName;
@property (nonatomic) NSString *firstName;
@property (nonatomic) NSString *lastName;
@property (nonatomic) NSMutableArray *phoneNumbers;
@property (nonatomic) NSString *contactIdentifier;

/**
 *  Get the profile image of contact.
 *  Firstly, find profile image from cache, if it does not exist, this function will get 2 first chacracter of firstname and lastname and create an image contains these characters
 *
 *  @param completionHandler This block will be dispatched to returnedQueue
 *  @param returnedQueue     CompletionHandler will be dispatch to this queue
 */
- (void)getProfileImageWithCompletionHandler:(GetProfileImageCompletionHandler)completionHandler dispatchTo:(dispatch_queue_t)returnedQueue;

@end
