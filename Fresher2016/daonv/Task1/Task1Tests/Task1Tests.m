//
//  Task1Tests.m
//  Task1Tests
//
//  Created by VanDao on 5/31/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "STMutableArray.h"

@interface Task1Tests : XCTestCase

@property (nonatomic) NSInteger iterations;

@end

@implementation Task1Tests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.iterations = 100000;
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// ---------------------------------------
//| Non-SafeThread MutableArray Testcase  |
// ---------------------------------------

// Test thêm "self.iterations" số phần tử vào NSMutableArray bằng một  global queue với độ ưu tiên high
// Kết quả mong muốn: Crash
//
- (void)testAddObject{
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    NSMutableArray *array = [[NSMutableArray alloc]init];

    dispatch_apply(self.iterations, concurrentQueue, ^(size_t num){
        NSString *obj = [NSString stringWithFormat:@"%zu", num];
        [array addObject:obj];
    });
}

// Khởi tạo một NSMutableArray với "self.iterations" phần tử, và lần lượt xoá các phần sử sau cùng
// Kết quả mong muốn: Throw exception
//
- (void)testRemoveObjectAtIndex{
    dispatch_queue_t serialQueue = dispatch_queue_create("serialQueue-initArray", NULL);
    NSMutableArray *array = [[NSMutableArray alloc]init];
    //Create array with iterations object
    dispatch_apply(self.iterations, serialQueue, ^(size_t num){
        NSString *obj = [NSString stringWithFormat:@"%zu", num];
        XCTAssertNoThrowSpecific([array addObject:obj], NSException, @"NSMutableArray - Add object at index is not support safe thread");
    });
    
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    
    __block NSInteger index = [array count];
    //Remove object
    dispatch_apply(self.iterations, concurrentQueue, ^(size_t num){
        index--;
        XCTAssertNoThrowSpecific([array removeObjectAtIndex:index], NSException, @"NSMutableArray - Remove object at index is not support safe thread");
    });
}

// Khởi tạo một NSMutableArray với 1 phần tử
// Lần lượt insert "self.iterations" phần tử vào mảng tại index: 0
// Kết quả mong muốn: Crash khi insert
//
- (void)testInsertObjectAtIndex{
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    NSMutableArray *array = [[NSMutableArray alloc]init];
    [array addObject:@"Obj"];

    dispatch_apply(self.iterations, concurrentQueue, ^(size_t num){
        NSString *obj = [NSString stringWithFormat:@"%zu", num];
        XCTAssertNoThrowSpecific([array insertObject:obj atIndex:0], NSException, @"NSMutableArray - Insert object at index is not support safe thread");
    });
    
}

// ---------------------------------------
//| SafeThread MutableArray Testcase  |
// ---------------------------------------

// Test thêm "self.iterations" số phần tử vào STMutableArray bằng một  global queue với độ ưu tiên high
// Kết quả mong muốn: Pass
//
- (void)testAddObjectWithSafeThread{
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    STMutableArray *array = [[STMutableArray alloc]init];
    
    dispatch_apply(self.iterations, concurrentQueue, ^(size_t num){
        NSString *obj = [NSString stringWithFormat:@"%zu", num];
        XCTAssertNoThrowSpecific([array addObject:obj], NSException, @"NSMutableArray - Add object method is not support safe thread");
    });
}

// Khởi tạo một STMutableArray với "self.iterations" phần tử, và lần lượt xoá các phần sử sau cùng
// Kết quả mong muốn: Crash trong khi xoá
- (void)testRemoveObjectAtIndexWithSafeThread{
    dispatch_queue_t serialQueue = dispatch_queue_create("serialQueue-initArray", NULL);
    STMutableArray *array = [[STMutableArray alloc]init];
    //Create array with iterations object
    dispatch_apply(self.iterations, serialQueue, ^(size_t num){
        [array addObject:[NSString stringWithFormat:@"%zu", num]];
    });
    
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    
    __block NSInteger index = [array count];
    //Remove object
    dispatch_apply(self.iterations, concurrentQueue, ^(size_t num){
        index--;
        XCTAssertNoThrowSpecific([array removeObjectAtIndex:index], NSException, @"NSMutableArray - Remove object at index is not support safe thread");
    });
}

// Khởi tạo một STMutableArray với 1 phần tử
// Lần lượt insert "self.iterations" phần tử vào mảng tại index: 0
// Kết quả mong muốn: Pass
//
- (void)testInsertObjectAtIndexWithSafeThread{
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    STMutableArray *array = [[STMutableArray alloc]init];
    [array addObject:@"Obj"];
    
    dispatch_apply(self.iterations, concurrentQueue, ^(size_t num){
        NSString *obj = [NSString stringWithFormat:@"%zu", num];
        XCTAssertNoThrowSpecific([array insertObject:obj atIndex:0], NSException, @"NSMutableArray - Insert object at index is not support safe thread");;
    });
}

@end
