/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@class UXCSSStyleSheet;

/**
 * A simple in-memory cache for stylesheets.
 *
 * @ingroup UXCSS
 *
 * It is recommended that you use this object to store stylesheets in a centralized location.
 * Ideally you would have one stylesheet cache throughout the lifetime of your application.
 *
 *
 * <h2>Using a stylesheet cache with Chameleon</h2>
 *
 * A stylesheet cache must be used with the Chameleon observer so that changes can be sent
 * for a given stylesheet. This is because changes are sent using the stylesheet object as
 * the notification object, so a listener must register notifications with the stylesheet as
 * the object.
 *
 @code
 UXStylesheet* stylesheet = [stylesheetCache stylesheetWithPath:@"common.css"];
 NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
 [nc addObserver:self
 selector:@selector(stylesheetDidChange)
 name:UXStylesheetDidChangeNotification
 object:stylesheet];
 @endcode
 */
@interface UXStylesheetCache : NSObject {
@private
    NSMutableDictionary* _pathToStylesheet;
    NSString* _pathPrefix;
}

/**
 * The path prefix that will be used to load stylesheets.
 *
 */
@property (nonatomic, readonly, copy) NSString* pathPrefix;

/**
 * Initializes a newly allocated stylesheet cache with a given path prefix.
 *
 */
- (id)initWithPathPrefix:(NSString *)pathPrefix;

/**
 * Fetches a stylesheet from the in-memory cache if it exists or loads the stylesheet from disk if
 * loadFromDisk is YES.
 *
 */
- (UXCSSStyleSheet *)stylesheetWithPath:(NSString *)path loadFromDisk:(BOOL)loadFromDisk;

/**
 * Fetches a stylesheet from the in-memory cache if it exists or loads the stylesheet from disk.
 *
 * Short form for calling [cache stylesheetWithPath:path loadFromDisk:YES]
 *
 */
- (UXCSSStyleSheet *)stylesheetWithPath:(NSString *)path;

@end








