//
//  CameraViewController.h
//  CameraGPUImage
//
//  Created by qhcthanh on 6/14/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"
#import "PreviewVideoViewController.h"
#import "InstaFilters.h"

#define LEADING_PHOTO_MODEVIEW_CONTRAINST -15
#define LEADING_VIDEO_MODEVIEW_CONTRAINST -63
#define DEFAULT_VIDEO_TIME @"00:00"
#define CAMERA_MINSCALE 1
#define CAMERA_MAXSCALE 8


@interface CameraViewController : UIViewController



@property (weak, nonatomic) IBOutlet UIButton *flashButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *cameraRotateButton;
@property (weak, nonatomic) IBOutlet GPUImageView *cameraView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingModeViewConstraint;
@property (weak, nonatomic) IBOutlet UIButton *shutterButton;
@property (weak, nonatomic) IBOutlet UIView *modeView;
@property (weak, nonatomic) IBOutlet UILabel *photoModeLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoModeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeVideoLabel;
@property (weak, nonatomic) IBOutlet UIView *topBar;
@property (weak, nonatomic) IBOutlet UIView *bottomBar;

//@property AVPlayer* avPlayer;
@property NSTimer* countTimer;
@property CGFloat lastScale;
@property int timeCount;


@end
