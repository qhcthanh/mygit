/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */


#import <UIKit/UIKit.h>

#import <UXFramework/UXFramework.h>

// This is the page view object that will be displayed for each page of the paging scroll view.
// Pages that will be displayed in a NIPagingScrollView must implement the NIPagingScrollViewPage
// protocol.

@interface PageView : UIView <UXPagingScrollViewPage>
@property (nonatomic, retain) UILabel* label;
- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

@end
