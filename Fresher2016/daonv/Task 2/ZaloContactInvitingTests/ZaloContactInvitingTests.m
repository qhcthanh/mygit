//
//  ZaloContactInvitingTests.m
//  ZaloContactInvitingTests
//
//  Created by VanDao on 5/26/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ContactManager.h"
#import "ImageCacheManager.h"
#import "UIImage+Contact.h"

@interface ZaloContactInvitingTests : XCTestCase

@end

@implementation ZaloContactInvitingTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// ___________________
//|                   |
//|   ContactManager  |
//|___________________|

/**
 *  Test multiple get permission access.
 */
- (void)testMultipleGetPermission {
    ContactManager *contactManager = [ContactManager sharedManager];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_queue_t callbackQueue = dispatch_queue_create("Call back queue", NULL);
    
    __block NSInteger count = 0;
    NSInteger loops = 1000;
    dispatch_apply(loops, queue, ^(size_t index){
        XCTAssertNoThrowSpecific(
                                 [contactManager requestPermissionAccessWithCallbackBlock:^(ContactAuthorizationStatus state){
                                        NSLog(@"%lu", count++);
                                    } onQueue: callbackQueue],
                                 NSException,
                                 @"Error");
    });
    NSLog(@"Done");
                   
    while (true) {
        if (count == loops)
            break;
    }
}

/**
 *  Test multiple writing on contact store.
 */
- (void)testMultipleGetAllContact {
    ContactManager *contactManager = [ContactManager sharedManager];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_queue_t callbackQueue = dispatch_queue_create("Call back queue", NULL);

    __block NSInteger count = 0;
    NSInteger loops = 1000;
    dispatch_apply(loops, queue, ^(size_t index){
        XCTAssertNoThrowSpecific(
                                 [contactManager getAllContactWithCallbackBlock:^(NSMutableArray<ContactDetail *> * _Nullable contactList, NSError * _Nullable error){
                                        NSLog(@"%lu", count++);
                                    } onQueue: callbackQueue],
                                 NSException,
                                 @"Error");
    });
    NSLog(@"Done");
    
    while (true) {
        if (count == loops)
            break;
    }
}


//_______________________________________________________


// ____________________
//|                    |
//|  ImageCacheManager |
//|____________________|

/**
 *  Test multiple writing on contact store.
 */
- (void)testMultipleSetImageToCache {
    ImageCacheManager *imageCacheManager = [[ImageCacheManager alloc]init];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    
    
    __block NSInteger count = 0;
    NSInteger loops = 1000;
    dispatch_apply(loops, queue, ^(size_t index){
        UIImage *image = [UIImage imageNamed:@"icon-user.png"];
        NSString *ID = [NSString stringWithFormat:@"%zu", index];
        XCTAssertNoThrowSpecific([imageCacheManager setImage:image withID:ID], NSException, @"Image must be not null");
        NSLog(@"%lu", count++);
    });
    NSLog(@"Done");
    
    while (true) {
        if (count == loops)
            break;
    }
}

/**
 *  Test that set image = nil to cache
 */
- (void)testSetNilImageToCache{
    ImageCacheManager *imageCacheManager = [[ImageCacheManager alloc]init];
    XCTAssertNoThrowSpecific([imageCacheManager setImage:nil withID:@"ID"], NSException, @"Image must be not null");
}

/**
 *  Test that set image with id = nil to cache
 */
- (void)testSetImageWithNilIDToCache{
    ImageCacheManager *imageCacheManager = [[ImageCacheManager alloc]init];
    XCTAssertNoThrowSpecific([imageCacheManager setImage:[UIImage imageNamed:@"icon-user.png"] withID:nil], NSException, @"Image must be not null");
}

/**
 *  Test that get image with Nil ID
 */
- (void)testGetImageWithNilID{
    ImageCacheManager *imageCacheManager = [[ImageCacheManager alloc]init];
    XCTAssertNoThrowSpecific([imageCacheManager getImageWithID:nil], NSException, @"ID must be not null");
}

/**
 *  Test that get exist image in cache
 */
- (void)testGetExistImageInCache{
    ImageCacheManager *imageCacheManager = [[ImageCacheManager alloc]init];
    [imageCacheManager setImage:[UIImage imageNamed:@"icon-user.png"] withID:@"1"];
    
    XCTAssertNotNil([imageCacheManager getImageWithID:@"1"], @"Image must be exist");
}

//----------------------------------------------------------

//|--------------------------
//|     UIImage+Contact      |
//|--------------------------

// CreateImageWithLetters method

/**
 *  Test that create Image with letters is not null, image must be create successfully
 */
- (void)testCreateImageWithNotNilLetters{
    XCTAssertNotNil([UIImage createImageFromLetters:@"NA"], @"Error while create image with letters");
}

/**
 *  Test that create Image with letters is nil, image must be create successflly
 */
- (void)testCreateImageWithNilLetters{
    XCTAssertNotNil([UIImage createImageFromLetters:nil], @"Image must be not nil");
}

/**
 *  Test that create Image with long letters, image must be create successfully
 */
- (void)testCreateImageWithLongLetters{
    XCTAssertNotNil([UIImage createImageFromLetters:@"AAAAAAAAAAAAAAAAAAAAAAAAAA"], @"Error while create image with letters");
}

// ----------------------------------

// ClipToCirleImage method

/**
 *  Test that clip nil image with nil mask
 */
- (void)testClipNilImageToCirleWithNilMaks{
    UIImage *image = nil;
    UIImage *mask = nil;
    XCTAssertNil([UIImage clipToCircleImage:image withMask:mask], @"Image must be nil");
}

/**
 *  Test that clip nil image with not nil mask
 */
- (void)testClipNilImageToCirleWithNotNilMaks{
    UIImage *image = nil;
    UIImage *mask = [UIImage imageNamed:@"mask4.png"];
    XCTAssertNil([UIImage clipToCircleImage:image withMask:mask], @"Image must be nil");
}

/**
 *  Test that clip not nil image with nil mask
 */
- (void)testClipNotNilImageToCirleWithNilMaks{
    UIImage *image = [UIImage imageNamed:@"icon-user.png"];
    UIImage *mask = nil;
    XCTAssertNil([UIImage clipToCircleImage:image withMask:mask], @"Image must be nil");
}

/**
 *  Test that clip not nil image with not nil mask
 */
- (void)testClipNotNilImageToCirleWithNotNilMaks{
    UIImage *image = [UIImage imageNamed:@"icon-user.png"];
    UIImage *mask = [UIImage imageNamed:@"mask4.png"];
    XCTAssertNotNil([UIImage clipToCircleImage:image withMask:mask], @"Image must be create successfully");
}

//---------------------------------

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
