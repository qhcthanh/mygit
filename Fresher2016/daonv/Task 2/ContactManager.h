//
//  ContactLoadManager.h
//  ZaloContactInviting
//
//  Created by VanDao on 5/26/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactDetail.h"
#import "ImageCacheManager.h"

@import UIKit;
@import Contacts;
@import AddressBook;

//Define permission states
typedef NS_ENUM(NSInteger, ContactAuthorizationStatus){
    kPermissionDenied,
    kPermissionAuthorized,
    kPermissionRetricted,
    kPermissionNotDetermine
};

//callback block
typedef void(^getPermissionBlock)(ContactAuthorizationStatus state);
typedef void(^loadContactBlock)(NSMutableArray<ContactDetail *>  * _Nullable contactList, NSError * _Nullable error);

@interface ContactManager : NSObject

+ (nonnull instancetype)sharedManager;

/**
 *  Get current permission access of user and require permission if not determine
 *
 *  @param queue         The queue that the callbackBlock called by dispatch_ascync
 *  @param callbackBlock callBack block with parameter state is permisson acess state
 */
- (void)requestPermissionAccessWithCallbackBlock:(_Nullable getPermissionBlock)callbackBlock onQueue:(_Nullable dispatch_queue_t)callbackQueue;

/**
 *  If have access permission, get all contact by contact framework if iOS version >= 9.0 or ABAdressFrameWork with otherwise
 *
 *  @param queue The queue that callbackblock called by dispath_ascync
 *  @param block callBack block with parameter error that method throw error while get contact, error = nil if sucessfully
 */
- (void)getAllContactWithCallbackBlock:(_Nullable loadContactBlock)callbackBlock onQueue:(_Nullable dispatch_queue_t)callbackQueue;

@end
