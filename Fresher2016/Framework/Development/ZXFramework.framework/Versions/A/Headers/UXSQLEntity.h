/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

typedef enum tagDataFieldType {
    kDataFieldTypeUnknown = -1,
    kDataFieldTypeString,
    kDataFieldTypeInteger,
    kDataFieldTypeNumber,
    kDataFieldTypeDecimal,
    kDataFieldTypeBoolean,
    kDataFieldTypeDatetime,
    kDataFieldTypeObject
} DataFieldType;

@interface UXSQLEntity : NSObject

+(NSDictionary*)properties;
+(NSDictionary*)loadProperties;
+(NSString*)propertiesAsStringList;

+(NSString*)getEntityName;
+(NSString*)getEntityName:(Class)theClass;
+(NSString*)getRelationName;

-(NSString*)getEntityName;
-(NSDictionary*)properties;
-(NSDictionary*)getEntityInformation;

@end
