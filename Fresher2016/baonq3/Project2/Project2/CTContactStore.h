//
//  ContactStore.h
//  Project2
//
//  Created by BaoNQ on 5/19/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CTContactDetail.h"
#import "ContactImageCaching.h"
@import AddressBook;
@import Contacts;

typedef enum  {
    CTAuthorizationStatusDenied, //User denied contact permission
    CTAuthorizationStatusRestricted,
    CTAuthorizationStatusNotDetermined, //Contact permission is not determine
    CTNilContactAdded,
} CTContactError;

/**
 *  Users are able to grant or deny access to contact data on a per-app basis. To request
    access to contact data, call getAllContactsWithCompletionHandler. This will not block 
    the app while user is being asked to grant or deny access.
 *
 *  @param contactList  All contact in the device
 *  @param error        Contains the error code. It will be nil if getAllContactsWithCompletionHandler function
                        is executed successfully.
 */
typedef void(^GetAllContactsCompletionHandler)(NSMutableArray *contactList, NSError *error);

/**
 *  Users are able to grant or deny access to contact data on a per-app basis. To request
    access to contact data, call getAllContactsWithCompletionHandler. This will not block
    the app while user is being asked to grant or deny access.
 *
 *  @param aContact A new contact
 *  @param error    Contains the error code. It will be nil if the new contact is added successfully.
 */
typedef void(^AddContactCompletionHandler)(CTContactDetail *aContact, NSError *error);

/**
 *  Users are able to grant or deny access to contact data on a per-app basis. To request
    access to contact data, call getAllContactsWithCompletionHandler. This will not block
    the app while user is being asked to grant or deny access.
 *
 *  @param error An error code
 */
typedef void(^RequestAccessCompletionHandler)(NSError *error);

@interface CTContactStore : NSObject {
}
/**
 *  Singletion method of contact store.
 */
+ (id)sharedContactStore;

/**
 *  Returns the current authorization status to access the contact data.
 *
 *  @param error Error will be nil if the current status is Authorization. Otherwise, it will contain the information of the error.
 *
 *  @return Yes if the current status is Authorization, otherwise returen NO.
 */
- (BOOL)checkContactAuthorizationStatus:(NSError **)error;

/**
 *  Requests access to the user's contacts
 *
 *  @param completionHandler This block will be dispatched to returedQueue
 *  @param returnedQueue     After request the permission, completionHandler will be dispatch to this queue
 */
- (void)requestAccessWithCompletionHandler:(RequestAccessCompletionHandler)completionHandler dispatchTo:(dispatch_queue_t)returnedQueue;

/**
 *  Get all contacts in device
 *
 *  @param completionHandler This block will be dispatched to returedQueue
 *  @param returnedQueue     After getting all contact, callbackBlock will be dispatched to this queue to continue with previous task.
 */
- (void)getAllContactsWithCompletionHandler:(GetAllContactsCompletionHandler)completionHandler dispatchTo:(dispatch_queue_t)returnedQueue;

/**
 *  Add a contact to device. After contact was added, this function will call the callbackBlock in the returnedQueue to process the previous tasks
 *
 *  @param newContact    A contact to add
 *  @param newImage      A new image profile to add
 *  @param returnedQueue After contact was added to device, callbackBlock will be dispatched to this queue
 *  @param callbackBlock The block will be called in returnedQueue after the contact was added to device
 */
- (void)addContact:(CTContactDetail *)newContact withImage:(UIImage *)newImage dispatchTo:(dispatch_queue_t)returnedQueue withCompletionHandler:(AddContactCompletionHandler)completionHandler;

@end
