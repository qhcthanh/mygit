//
//  ViewController.m
//  Project2
//
//  Created by BaoNQ on 5/17/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "TableViewController.h"
#import "CTTableViewCell.h"

@implementation TableViewController

/**
 *  Initialize
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.leftBarButton.title = @"Edit";
    
    //Initialize search controller
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    [self.searchController.searchBar sizeToFit];
    self.definesPresentationContext = YES;
    self.searchController.searchBar.barTintColor = [UIColor colorWithRed:246.0/255.0 green:248.0/255.0 blue:250.0/255.0 alpha:1.0];
    self.searchController.searchBar.layer.borderWidth = 1;
    self.searchController.searchBar.layer.borderColor = [[UIColor colorWithRed:246.0/255.0 green:248.0/255.0 blue:250.0/255.0 alpha:1.0] CGColor];
    [self.searchBarView addSubview:self.searchController.searchBar];
    
    //Turn on checkbox
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    [self.tableView setEditing:YES];
    
    self.tableView.sectionIndexColor = [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0];
    
    // Initialize table data
    self.contacts = [[NSMutableArray alloc] init];
    self.filteredContacts = [[NSMutableArray alloc] init];
    self.isFiltered = NO;
    self.filteredSectionIndex = [[NSMutableArray alloc] init];
    self.sectionIndex = [[NSMutableArray alloc] init];
    self.checkedList = [[NSMutableArray alloc] init];
    self.sectionIndexTitles = [[NSMutableArray alloc] init];
    self.numberOfCheckedCell = 0;
    
    //Initialize collection view
    self.collectionViewHeightConstraint.constant = 0;
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)[self.collectionView collectionViewLayout];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    
    //Initialize image caching
    self.cachedImage = [ContactImageCaching sharedContactImageCaching];
    
    //Get contacts
    CTContactStore *contactStore = [CTContactStore sharedContactStore];
    
    dispatch_queue_t processingQueue = dispatch_queue_create("com.vng.processingQueue", NULL);
    [contactStore getAllContactsWithCompletionHandler:^(NSMutableArray * returnedContactList, NSError *error) {
        //If we have some errors when getting contact list
        if (returnedContactList == nil) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
            
            return;
        }
        
        [self.addContactButton setEnabled:YES];
        
        //Step 1: sort contact list by fullname
        self.contacts = [NSMutableArray arrayWithArray:[returnedContactList sortedArrayUsingComparator: ^(CTContactDetail *c1, CTContactDetail *c2) {
            
            NSString *fullname1 = [c1.fullName uppercaseString];
            NSString *fullname2 = [c2.fullName uppercaseString];
            
            char char1 = '#';
            char char2 = '#';
            if (fullname1.length > 0)
                char1= [fullname1 characterAtIndex:0];
            if (fullname2.length > 0)
                char2= [fullname2 characterAtIndex:0];
            
            if (char1 >= 'A' && char1 <= 'Z'){
                if (!(char2 >= 'A' && char2 <= 'Z')){
                    return NSOrderedDescending;
                }
            }
            else if ((char2 >= 'A' && char2 <= 'Z')){
                if (!(char1 >= 'A' && char1 <= 'Z')){
                    return NSOrderedAscending;
                }
            }
            
            return [fullname1 compare:fullname2];
            
        }]];
        
        //Step 2: Create section indexes for contact list.
        NSString *firstChar = @"None";
        unichar firstUniChar;
        NSString *firstCharOfContactName;
        NSUInteger numberOfContacts = [self.contacts count];
        for (int i = 0; i < numberOfContacts; ++i) {
            if ([[self.contacts objectAtIndex:i] fullName].length > 0)
                firstCharOfContactName = [[[[self.contacts objectAtIndex:i] fullName] substringToIndex:1] uppercaseString];
            else
                firstCharOfContactName = @"#";
            
            if ([firstChar isEqual: @"None"]) {
                firstChar = firstCharOfContactName;
                firstUniChar = [firstChar characterAtIndex:0];
                if (firstUniChar >= 'A' && firstUniChar <= 'Z')
                    [self.sectionIndexTitles addObject:firstChar];
                else
                    [self.sectionIndexTitles addObject:@"#"];
                [self.sectionIndex addObject:[[NSNumber alloc] initWithInt:i]];
            }
            else if (!([firstChar isEqual:firstCharOfContactName])) {
                firstChar = firstCharOfContactName;
                firstUniChar = [firstChar characterAtIndex:0];
                if (firstUniChar >= 'A' && firstUniChar <= 'Z'){
                    [self.sectionIndexTitles addObject:firstChar];
                    [self.sectionIndex addObject:[[NSNumber alloc] initWithInt:i]];
                }
            }
        }
        
        //Step 3: Mark all contact cells as uncheck
        for (int i = 0; i < numberOfContacts; ++i) {
            [self.checkedList addObject:@NO];
        }
        
        //Step 4: Reload data
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    } dispatchTo:processingQueue];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//SEARCH CONTROLLER

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    dispatch_queue_t updateSearchResultQueue = dispatch_queue_create("updateSearchResultQueue", NULL);
    dispatch_async(updateSearchResultQueue, ^{
        NSString *searchText = searchController.searchBar.text;
        
        if (searchText.length == 0) {
            self.isFiltered = NO;
            [self.filteredContacts removeAllObjects];
            [self.filteredSectionIndex removeAllObjects];
        }
        else {
            self.isFiltered = YES;
            
            [self.filteredContacts removeAllObjects];
            [self.filteredSectionIndex removeAllObjects];
            
            //Step 1: Observe each contact in contact list.
            NSUInteger numberOfContacts = [self.contacts count];
            for (int i = 0; i < numberOfContacts; ++i) {
                CTContactDetail *contact = [self.contacts objectAtIndex:i];
                NSRange stringRange = [contact.fullName rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if (stringRange.location != NSNotFound) {
                    //Add contact to array
                    [self.filteredContacts addObject:[NSNumber numberWithInteger:i]];
                }
            }
            //Step 2: Create section indexes for filtered contacts
            NSString *firstChar = @"None";
            NSString *firstCharOfContactName;
            unichar firstUniChar;
            numberOfContacts = [self.filteredContacts count];
            for (int i = 0; i < numberOfContacts; ++i) {
                NSString *tmp = [[self.contacts objectAtIndex:[[self.filteredContacts objectAtIndex:i] intValue]] fullName];
                if (tmp.length > 0)
                    firstCharOfContactName = [[tmp substringToIndex:1] uppercaseString];
                else
                    firstCharOfContactName = @"#";
                
                if ([firstChar isEqual: @"None"]) {
                    firstChar = firstCharOfContactName;
                    [self.filteredSectionIndex addObject:[[NSNumber alloc] initWithInt:i]];
                }
                else if (!([firstChar isEqual:firstCharOfContactName])) {
                    firstChar = firstCharOfContactName;
                    firstUniChar = [[firstChar uppercaseString] characterAtIndex:0];
                    if (firstUniChar >= 'A' && firstUniChar <= 'Z')
                        [self.filteredSectionIndex addObject:[[NSNumber alloc] initWithInt:i]];
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            //Reload table view data
            [self.tableView reloadData];
            
            //Reselect cells which were selected before data reloaded.
            NSInteger numberOfSections = [self.tableView numberOfSections];
            for (int i = 0; i < numberOfSections; ++i) {
                NSInteger numberOfRows = [self.tableView numberOfRowsInSection:i];
                for (int j = 0; j< numberOfRows; ++j) {
                    NSUInteger indexOfContact;
                    if (!self.isFiltered) {
                        NSInteger sectionIndex = [[self.sectionIndex objectAtIndex:i] integerValue];
                        indexOfContact = (sectionIndex + j);
                    }
                    else {
                        NSInteger sectionIndex = [[self.filteredSectionIndex objectAtIndex:i] integerValue];
                        indexOfContact = [[self.filteredContacts objectAtIndex:(sectionIndex + j)] intValue];
                    }
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                    if ([[self.checkedList objectAtIndex:indexOfContact] boolValue] == YES)
                        [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
            }
        });
    });
}

//TABLE VIEW

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == ([self.sectionIndex count] - 1) || section == ([self.filteredSectionIndex count] - 1)) {
        if (self.isFiltered) {
            return ([self.filteredContacts count] - [[self.filteredSectionIndex objectAtIndex:(section)] integerValue]);
        }
        return ([self.contacts count] - [[self.sectionIndex objectAtIndex:(section)] integerValue]);
    }
    else {
        if (self.isFiltered) {
            return ([[self.filteredSectionIndex objectAtIndex:(section + 1)] integerValue] - [[self.filteredSectionIndex objectAtIndex:(section)] integerValue]);
        }
        return ([[self.sectionIndex objectAtIndex:(section + 1)] integerValue] - [[self.sectionIndex objectAtIndex:(section)] integerValue]);
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.isFiltered) {
        return [self.filteredSectionIndex count];
    }
    return [self.sectionIndex count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSInteger indexOfFirstItemInSection;
    NSString *fullName;
    if (self.isFiltered) {
        indexOfFirstItemInSection = [[self.filteredSectionIndex objectAtIndex:section] integerValue];
        fullName = [[self.contacts objectAtIndex:[[self.filteredContacts objectAtIndex:indexOfFirstItemInSection] intValue]] fullName];
    }
    else {
        indexOfFirstItemInSection = [[self.sectionIndex objectAtIndex:section] integerValue];
        fullName = [[self.contacts objectAtIndex:indexOfFirstItemInSection] fullName];
    }

    fullName = [fullName uppercaseString];
    if (fullName.length == 0)
        return @"#";
    unichar firstChar = [fullName characterAtIndex:0];

    if (firstChar >= 'A' && firstChar <= 'Z') {
        return [fullName substringToIndex:1];
    }
    return @"#";
}

- (CTTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    NSString *cellIdentifier = @"CustomCell";
    CTTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil){
        cell = [[CTTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    CTContactDetail *curContact = nil;
    NSUInteger indexOfContact;
    if (!self.isFiltered) {
        NSInteger sectionIndex = [[self.sectionIndex objectAtIndex:[indexPath section]] integerValue];
        indexOfContact = (sectionIndex + [indexPath row]);
    }
    else {
        NSInteger sectionIndex = [[self.filteredSectionIndex objectAtIndex:[indexPath section]] integerValue];
        indexOfContact = [[self.filteredContacts objectAtIndex:(sectionIndex + [indexPath row])] intValue];
    }
    curContact = [self.contacts objectAtIndex:indexOfContact];
    if (curContact == nil)
        return nil;
    cell.nameLabel.text = curContact.fullName;
    
    cell.profilePicture.image = [self.cachedImage imageForKey:curContact.contactIdentifier];
    if (cell.profilePicture.image == nil) {
        [curContact getProfileImageWithCompletionHandler:^(UIImage *profileImage, NSError *error){
            if (error == nil) {
                cell.profilePicture.image = profileImage;
            }
        } dispatchTo:nil];
    }

    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {

        //Step 1: Get index of the contact which you want to delete
        NSUInteger indexOfContact;
        if (!self.isFiltered) {
            NSInteger sectionIndex = [[self.sectionIndex objectAtIndex:[indexPath section]] integerValue];
            indexOfContact = (sectionIndex + [indexPath row]);
        }
        else {
            NSInteger sectionIndex = [[self.filteredSectionIndex objectAtIndex:[indexPath section]] integerValue];
            indexOfContact = [[self.filteredContacts objectAtIndex:(sectionIndex + [indexPath row])] intValue];
        }
        //Step 2: remove image from cache
        [self.cachedImage removeObjectForKey:[[self.contacts objectAtIndex:indexOfContact] contactIdentifier]];
        
        //Step 3: Remove contact in the list.
        [self.contacts removeObjectAtIndex:indexOfContact];
        
        //Step 4: Remove contact in checkedlist.
        if ([[self.checkedList objectAtIndex:indexOfContact] boolValue] == YES)
            self.numberOfCheckedCell--;
        [self.checkedList removeObjectAtIndex:indexOfContact];
        
        //Step 5: Reload data on collection view
        [self.collectionView reloadData];
        if (self.numberOfCheckedCell == 0) {
            self.collectionViewHeightConstraint.constant = 0;
            [self.searchBarView addSubview:self.searchController.searchBar];
            [UIView animateWithDuration:.5 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
        
        NSUInteger numberOfSectionBeforeDeletion = self.sectionIndex.count;
        NSUInteger numberOfSectionAfterDeletion = self.sectionIndex.count;
        //Step 6: Create section index and setion index titles
        [self.sectionIndex removeAllObjects];
        [self.sectionIndexTitles removeAllObjects];
        
        //Step 7: Create section indexes for contact list.
        NSString *firstChar = @"None";
        unichar firstUniChar;
        NSString *firstCharOfContactName;
        NSUInteger numberOfContacts = [self.contacts count];
        for (int i = 0; i < numberOfContacts; ++i) {
            NSString *tmp = [[self.contacts objectAtIndex:i] fullName];
            if (tmp.length > 0)
                firstCharOfContactName = [[tmp substringToIndex:1] uppercaseString];
            else
                firstCharOfContactName = @"#";
            
            if ([firstChar isEqual: @"None"]) {
                firstChar = firstCharOfContactName;
                firstUniChar = [firstChar characterAtIndex:0];
                if (firstUniChar >= 'A' && firstUniChar <= 'Z')
                    [self.sectionIndexTitles addObject:firstChar];
                else
                    [self.sectionIndexTitles addObject:@"#"];
                [self.sectionIndex addObject:[[NSNumber alloc] initWithInt:i]];
            }
            else if (!([firstChar isEqual:firstCharOfContactName])) {
                firstChar = firstCharOfContactName;
                firstUniChar = [firstChar characterAtIndex:0];
                if (firstUniChar >= 'A' && firstUniChar <= 'Z'){
                    [self.sectionIndexTitles addObject:firstChar];
                    [self.sectionIndex addObject:[[NSNumber alloc] initWithInt:i]];
                }
            }
        }
        

        numberOfSectionAfterDeletion = self.sectionIndex.count;
        
        if (self.isFiltered) {
            //Remove the deleted object in filtered list.
            for (int i = 0; i< self.filteredContacts.count; ++i) {
                NSInteger temp = [[self.filteredContacts objectAtIndex:i] integerValue];
                if (temp == indexOfContact) {
                    [self.filteredContacts removeObjectAtIndex:i];
                    if (self.filteredContacts.count == 0) {
                        break;
                    }
                    temp = [[self.filteredContacts objectAtIndex:i] integerValue];
                }
                if (temp > indexOfContact) {
                    [self.filteredContacts replaceObjectAtIndex:i withObject:[NSNumber numberWithLong:(temp - 1)]];
                }
            }
            
            numberOfSectionBeforeDeletion = self.filteredSectionIndex.count;
            [self.filteredSectionIndex removeAllObjects];
            //Create section indexes for filtered contacts
            NSString *firstChar = @"None";
            NSString *firstCharOfContactName;
            unichar firstUniChar;
            numberOfContacts = [self.filteredContacts count];
            for (int i = 0; i < numberOfContacts; ++i) {
                NSString *tmp = [[self.contacts objectAtIndex:[[self.filteredContacts objectAtIndex:i] intValue]] fullName];
                if (tmp.length > 0)
                    firstCharOfContactName = [[tmp substringToIndex:1] uppercaseString];
                else
                    firstCharOfContactName = @"#";
                
                if ([firstChar isEqual: @"None"]) {
                    firstChar = firstCharOfContactName;
                    [self.filteredSectionIndex addObject:[[NSNumber alloc] initWithInt:i]];
                }
                else if (!([firstChar isEqual:firstCharOfContactName])) {
                    firstChar = firstCharOfContactName;
                    firstUniChar = [[firstChar uppercaseString] characterAtIndex:0];
                    if (firstUniChar >= 'A' && firstUniChar <= 'Z')
                        [self.filteredSectionIndex addObject:[[NSNumber alloc] initWithInt:i]];
                }
            }
            numberOfSectionAfterDeletion = self.filteredSectionIndex.count;
            NSLog(@"%@", self.filteredContacts);
             NSLog(@"%@", self.filteredSectionIndex);

        }
        [tableView beginUpdates];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        //If the deleted row is the last row of the section, we have to delete the seciton
        if (numberOfSectionBeforeDeletion > numberOfSectionAfterDeletion) {
            NSLog(@"Delete section");
            [tableView deleteSections:[NSIndexSet indexSetWithIndex: indexPath.section] withRowAnimation:(UITableViewRowAnimationFade)];
        }
        
        
        [tableView endUpdates];
    } else {
        NSLog(@"Unhandled editing style! %ld", (long)editingStyle);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.tableView.editing == NO)
        return;
    NSUInteger indexOfContact;
    if (!self.isFiltered) {
        NSInteger sectionIndex = [[self.sectionIndex objectAtIndex:[indexPath section]] integerValue];
        indexOfContact = (sectionIndex + [indexPath row]);
    }
    else {
        NSInteger sectionIndex = [[self.filteredSectionIndex objectAtIndex:[indexPath section]] integerValue];
        indexOfContact = [[self.filteredContacts objectAtIndex:(sectionIndex + [indexPath row])] intValue];
    }
    
    NSInteger indexOfCollectionItem = 0;
    for (int i = 0; i< self.checkedList.count; ++i) {
        if (i == indexOfContact)
            break;
        else if ([[self.checkedList objectAtIndex:i] boolValue] == YES) {
            indexOfCollectionItem++;
            
        }
    }
    
    [self.checkedList replaceObjectAtIndex:indexOfContact withObject:@YES];
    self.numberOfCheckedCell++;
    
    NSIndexPath *collectionIndexPath =[NSIndexPath indexPathForRow:indexOfCollectionItem inSection:0];
    [self.collectionView insertItemsAtIndexPaths:[NSArray arrayWithObject:collectionIndexPath]];
    
    //[self.collectionView reloadData];
    if (self.numberOfCheckedCell == 1) {
        self.collectionViewHeightConstraint.constant = 50;
        [self.searchBarView addSubview:self.searchController.searchBar];
        [UIView animateWithDuration:.5 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.tableView.editing == NO)
        return;
    NSUInteger indexOfContact;
    if (!self.isFiltered) {
        NSInteger sectionIndex = [[self.sectionIndex objectAtIndex:[indexPath section]] integerValue];
        indexOfContact = (sectionIndex + [indexPath row]);
    }
    else {
        NSInteger sectionIndex = [[self.filteredSectionIndex objectAtIndex:[indexPath section]] integerValue];
        indexOfContact = [[self.filteredContacts objectAtIndex:(sectionIndex + [indexPath row])] intValue];
    }
    
    NSInteger indexOfCollectionItem = 0;
    for (int i = 0; i< self.checkedList.count; ++i) {
        if ([[self.checkedList objectAtIndex:i] boolValue] == YES) {
            if (i == indexOfContact)
                break;
            else
                indexOfCollectionItem++;
            
        }
    }
    
    [self.checkedList replaceObjectAtIndex:indexOfContact withObject:@NO];
    self.numberOfCheckedCell--;
    
    NSIndexPath *collectionIndexPath =[NSIndexPath indexPathForRow:indexOfCollectionItem inSection:0];
    [self.collectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:collectionIndexPath]];
    
    [self.collectionView reloadData];
    if (self.numberOfCheckedCell == 0) {
        self.collectionViewHeightConstraint.constant = 0;
        [self.searchBarView addSubview:self.searchController.searchBar];
        [UIView animateWithDuration:.5 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

-(NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (self.isFiltered)
        return nil;
    return self.sectionIndexTitles;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *v = (UITableViewHeaderFooterView *)view;
    v.backgroundView.backgroundColor = [UIColor whiteColor];
    v.textLabel.font = [UIFont systemFontOfSize:13];
    v.textLabel.textColor = [UIColor colorWithRed:121.0/255.0 green:123.0/255.0 blue:132.0/255.0 alpha:1.0];
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *v = (UITableViewHeaderFooterView *)view;
    v.backgroundView.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:248.0/255.0 blue:250.0/255.0 alpha:1.0];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}

//COLLECTION VIEW

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.numberOfCheckedCell;
}

- (CTCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CTCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"viewControllerCell" forIndexPath:indexPath];
    //cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"yellow_seat.png"]];
    
    NSMutableArray *checkedContacts = [[NSMutableArray alloc] init];
    NSUInteger length = [self.checkedList count];
    for (int i = 0; i< length; ++i) {
        if ([[self.checkedList objectAtIndex:i] boolValue] == YES)
            [checkedContacts addObject:([NSNumber numberWithInteger:i])];
    }
    
    CTContactDetail *curContact = [self.contacts objectAtIndex:[[checkedContacts objectAtIndex:[indexPath row]] integerValue]];
    //Get profile image
    cell.imageView.image = [self.cachedImage imageForKey:curContact.contactIdentifier];

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //Remove cell from collection view
    NSMutableArray *checkedContacts = [[NSMutableArray alloc] init];
    NSUInteger length = [self.checkedList count];
    for (int i = 0; i< length; ++i) {
        if ([[self.checkedList objectAtIndex:i] boolValue] == YES)
            [checkedContacts addObject:([NSNumber numberWithInteger:i])];
    }
    NSUInteger indexOfSelectedContact = [[checkedContacts objectAtIndex:indexPath.row] integerValue];
    [self.checkedList replaceObjectAtIndex:indexOfSelectedContact withObject:@NO];
    self.numberOfCheckedCell--;
    
    [self.collectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
    
    [self.collectionView reloadData];
    if (self.numberOfCheckedCell == 0) {
        self.collectionViewHeightConstraint.constant = 0;
        [self.searchBarView addSubview:self.searchController.searchBar];
        [UIView animateWithDuration:.5 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
    
    //Deselect cell in table view.
    NSInteger numberOfSections = [self.tableView numberOfSections];
    for (int i = 0; i < numberOfSections; ++i) {
        NSInteger numberOfRows = [self.tableView numberOfRowsInSection:i];
        for (int j = 0; j< numberOfRows; ++j) {
            NSUInteger indexOfContact;
            if (!self.isFiltered) {
                NSInteger sectionIndex = [[self.sectionIndex objectAtIndex:i] integerValue];
                indexOfContact = (sectionIndex + j);
            }
            else {
                NSInteger sectionIndex = [[self.filteredSectionIndex objectAtIndex:i] integerValue];
                indexOfContact = [[self.filteredContacts objectAtIndex:(sectionIndex + j)] intValue];
            }
            if (indexOfSelectedContact == indexOfContact) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:j inSection:i];
                [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
                [self.collectionView reloadData];
                return;
            }
        }
    }
}


//ACTIONS
- (IBAction)editMode:(id)sender {
    if (self.tableView.editing == YES) {
        NSLog(@"Edit mode");
        [self.searchController setActive:NO];
        
        self.tableView.allowsMultipleSelectionDuringEditing = NO;
        [self.tableView setEditing:NO];
        
        self.numberOfCheckedCell = 0;
        for (int i = 0; i < self.checkedList.count; ++i) {
            [self.checkedList replaceObjectAtIndex:i withObject:@NO];
        }
        [self.collectionView reloadData];
        //[self.tableView reloadData];
        
        self.collectionViewHeightConstraint.constant = 0;
        [self.searchBarView addSubview:self.searchController.searchBar];
        [UIView animateWithDuration:.5 animations:^{
            [self.view layoutIfNeeded];
        }];
        self.leftBarButton.title = @"Done";
    }
    else {
        NSLog(@"Editing done");
        [self.searchController setActive:NO];
        
        NSLog(@"%f", self.collectionViewHeightConstraint.constant);
        self.tableView.allowsMultipleSelectionDuringEditing = YES;
        [self.tableView setEditing:YES];
        self.leftBarButton.title = @"Edit";

    }

}

//DELEGATE and PROTOCOL

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"segue");
    AddContactController *addContactController = [segue destinationViewController];
    addContactController.delegate = self;
    //Deactive searbar
    NSLog(@"Deactive searchbar");
    [self.searchController setActive:NO];
}

- (void)sendNewContactToTableViewController:(CTContactDetail *)newContact withImage:(UIImage *)newImage{
    if(newContact == nil)
        return;
    
    NSLog(@"We have new contact. Process it");
    CTContactStore *contactStore = [CTContactStore sharedContactStore];
    
    dispatch_queue_t processingQueue = dispatch_queue_create("com.vng.processingQueue", NULL);
    [contactStore addContact:newContact withImage:newImage dispatchTo:processingQueue withCompletionHandler:^(CTContactDetail * contact, NSError *error){
        NSLog(@"Add done");
        
        //If we have some errors when getting contact list
        if (contact == nil) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
            
            return;
        }
        
        NSMutableArray *oldContacts = [NSMutableArray arrayWithArray:self.contacts];
        //ContactStore *contactStore = [ContactStore sharedContactStore];
        [self.contacts addObject:contact];
        
        //Step 1: sort contact list by fullname
        self.contacts = [NSMutableArray arrayWithArray:[self.contacts sortedArrayUsingComparator: ^(CTContactDetail *c1, CTContactDetail *c2) {
            NSString *fullname1 = [c1.fullName uppercaseString];
            NSString *fullname2 = [c2.fullName uppercaseString];
            
            char char1 = '#';
            char char2 = '#';
            if (fullname1.length > 0)
                char1= [fullname1 characterAtIndex:0];
            if (fullname2.length > 0)
                char2= [fullname2 characterAtIndex:0];
            
            if (char1 >= 'A' && char1 <= 'Z'){
                if (!(char2 >= 'A' && char2 <= 'Z')){
                    return NSOrderedDescending;
                }
            }
            else if ((char2 >= 'A' && char2 <= 'Z')){
                if (!(char1 >= 'A' && char1 <= 'Z')){
                    return NSOrderedAscending;
                }
            }
            
            return [fullname1 compare:fullname2];
        }]];
        
        //Step 2: Create checked list
        NSUInteger addedIndex = -1;
        NSUInteger size = oldContacts.count;
        for (int i = 0; i < size; ++i) {
            if (![[[oldContacts objectAtIndex:i] contactIdentifier] isEqual:[[self.contacts objectAtIndex:i] contactIdentifier]]) {
                [self.checkedList insertObject:@NO atIndex:i];
                addedIndex = i;
                break;
            }
        }
        if (addedIndex == -1) {
            addedIndex = size;
            [self.checkedList addObject:@NO];
        }
        
        //Step 3: Create section indexes for contact list.
        NSString *firstChar = @"None";
        unichar firstUniChar;
        NSString *firstCharOfContactName;
        
        NSUInteger row = 0, section = 0;
        
        NSUInteger numberOfSectionBeforeInsertion = self.sectionIndex.count;
        NSUInteger numberOfSectionAfterInsertion = self.sectionIndex.count;
        
        [self.sectionIndex removeAllObjects];
        [self.sectionIndexTitles removeAllObjects];
        
        NSUInteger numberOfContacts = [self.contacts count];
        for (int i = 0; i < numberOfContacts; ++i) {
            NSString *tmp = [[self.contacts objectAtIndex:i] fullName];
            if (tmp.length > 0)
                firstCharOfContactName = [[tmp substringToIndex:1] uppercaseString];
            else
                firstCharOfContactName = @"#";
            
            if ([firstChar isEqual: @"None"]) {
                firstChar = firstCharOfContactName;
                firstUniChar = [firstChar characterAtIndex:0];
                if (firstUniChar >= 'A' && firstUniChar <= 'Z')
                    [self.sectionIndexTitles addObject:firstChar];
                else
                    [self.sectionIndexTitles addObject:@"#"];
                [self.sectionIndex addObject:[[NSNumber alloc] initWithInt:i]];
            }
            else if (!([firstChar isEqual:firstCharOfContactName])) {
                firstChar = firstCharOfContactName;
                firstUniChar = [firstChar characterAtIndex:0];
                if (firstUniChar >= 'A' && firstUniChar <= 'Z'){
                    [self.sectionIndexTitles addObject:firstChar];
                    [self.sectionIndex addObject:[[NSNumber alloc] initWithInt:i]];
                }
            }
            if (i == addedIndex) {
                row = addedIndex - [[self.sectionIndex objectAtIndex:(self.sectionIndex.count - 1)] integerValue];
                section = self.sectionIndex.count - 1;
            }
        }
        numberOfSectionAfterInsertion = self.sectionIndex.count;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
        
        //If new image != nil. cache it
        if (newImage != nil) {
            [_cachedImage cacheImage:newImage forKey:contact.contactIdentifier];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView beginUpdates];
            
            //If inserted contact is in a new section. We have to add new section
            if (numberOfSectionAfterInsertion > numberOfSectionBeforeInsertion) {
                [self.tableView insertSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            //Add row to section
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            [self.tableView endUpdates];
        });
    }];
}

@end

