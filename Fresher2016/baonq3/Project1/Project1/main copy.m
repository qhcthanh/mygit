//
//  main.m
//  Project1
//
//  Created by admin on 5/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <Foundation/Foundation.h>
#import "AppDelegate.h"

//using barrier
@interface MutableArrayExtension : NSObject {
    NSMutableArray* array;
    dispatch_queue_t myQueue;
}
@end

@implementation MutableArrayExtension

- (id)init{
    self = [super init];
    if (!self)
        return nil;
    array = [[NSMutableArray alloc] init];
    myQueue = dispatch_queue_create("com.VNG.Project1", DISPATCH_QUEUE_CONCURRENT);
    return self;
}

- (id)objectAtIndex:(NSUInteger)index
{
    if (array != nil && index < [array count])
    {
        __block id res;
        dispatch_async(myQueue, ^{
            res = [array objectAtIndex:index];
            //NSLog(@"%i", index);
            NSLog(@"%i", [res intValue]);
        });
        //NSLog(@"%i", [res intValue]);
        return res;
    }
    return nil;
}

- (void)addObject:(id)anObject
{
    dispatch_barrier_async(myQueue, ^{
        [array addObject:anObject];
        //NSLog(@"%i", [anObject intValue]);
    });
}

 
@end

//using semaphore
@interface MutableArrayExtension1 : NSObject {
    NSMutableArray* array;
    dispatch_queue_t myQueue;
    dispatch_semaphore_t semaphore;
}
@end

@implementation MutableArrayExtension1

- (id)init{
    self = [super init];
    if (!self)
        return nil;
    array = [[NSMutableArray alloc] init];
    myQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    semaphore = dispatch_semaphore_create(1);
    return self;
}

- (id)objectAtIndex:(NSUInteger)index
{
    if (array != nil && index < [array count])
    {
        __block id res;
        dispatch_async(myQueue, ^{
            res = [array objectAtIndex:index];
            //NSLog(@"%i", index);
            NSLog(@"%i", [res intValue]);
        });
        //NSLog(@"%i", [res intValue]);
        return res;
    }
    return nil;
}

- (void)addObject:(id)anObject
{
    
    dispatch_async(myQueue, ^{
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        [array addObject:anObject];
        //NSLog(@"%i", [anObject intValue]);
        dispatch_semaphore_signal(semaphore);
    });
}


@end

@interface MyCustomClass : NSMutableArray {
    NSMutableArray * internalArray;
}


@end

@implementation MyCustomClass

- (id)init{
    self = [super init];
    //
    if (!self)
        return nil;
    self = [[NSMutableArray alloc]init];
    return self;
}

- (id)objectAtIndex:(NSUInteger)index
{
    NSLog(@"hihi");
    return nil;
}

@end

int main(int argc, char * argv[])
{
    @autoreleasepool {
        
        MyCustomClass *a = [[MyCustomClass alloc] init];
        [a addObject: [NSNumber numberWithInt:10]];
        NSLog(@"%i", [a[0] intValue]);
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

/*
int main(int argc, char * argv[]) {
    @autoreleasepool {
        
        MutableArrayExtension *a = [[MutableArrayExtension alloc] init];
        
        for (int i = 0; i<10000; ++i)
        {
            [a addObject:[NSNumber numberWithInt:i]];
        }
        
        for (int i = 0; i<10000; ++i)
        {
            [[a objectAtIndex:i] intValue];
            //NSLog(@"a[%i] = %i", i, [[a objectAtIndex:i] intValue]);
        }
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
*/

/*
int main(int argc, char * argv[]) {
    @autoreleasepool {
        
        dispatch_queue_t myQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        NSMutableArray* array = [[NSMutableArray alloc] init];
        for (int i = 0; i < 10000; ++i)
        {
            dispatch_async(myQueue, ^{
                [array addObject:[NSNumber numberWithInt:i]];
            });
            //dispatch_barrier_async(myQueue1, ^{
            //[array addObject:[NSNumber numberWithInt:i]];
            //   NSLog(@"%i", [[array objectAtIndex:i] intValue]); });
        }
        for (int i = 0; i < 10000; ++i)
        {
            dispatch_async(myQueue, ^{
                NSLog(@"%i", i, [[array objectAtIndex:i] intValue]);
            });
        }
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
 */
