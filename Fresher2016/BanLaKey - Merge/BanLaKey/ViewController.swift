//
//  ViewController.swift
//  BanLaKey
//
//  Created by qhcthanh on 8/30/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var tableView: CTTableView!
    
    var settingInfo = [
        [("Chủ đề bàn phím", "paint")],
        [("Kiểu gõ","keyboard"), ("Tuỳ chỉnh","setting")],
        [("Hướng dẫn cài đặt","guide"), ("Góp ý & thông tin","comment"), ("Đánh giá Banla Key","like")]
    ]
    
    
    
    
  //  var topExtraView: BLTopExtraView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let kieuGoDataSource = [
            CTCategoryInfo(headerTitle: "CHỌN KIỂU GÕ", footerTitle: "Kiểu gõ Telex đơn giản không dùng phím w=ư", headerHeight: 50, footerHeight: 35,
                items: [
                    CTTableItem(title: "Telex cở bản", isCheckMark: true, isCheck: true,
                        selectBlock: {
                            // select Block
                            
                        }, deSelectBlock: {
                            // deselect block
                    }),
                    CTTableItem(title: "Telex đầy đủ", isCheckMark: true,
                        selectBlock: {
                            // select Block
                            
                        }, deSelectBlock: {
                            // deselect block
                    }),
                    CTTableItem(title: "VNI", isCheckMark: true,
                        selectBlock: {
                            // select Block
                            
                        }, deSelectBlock: {
                            // deselect block
                    }),
                ]
            ),
            CTCategoryInfo(headerTitle: "NÂNG CAO", footerTitle: "Kiểu gõ TELEX cải tiến cho phép gõ cc = ch, gg=gi, kk= kh, nn=ng, qq=qu, pp=ph, tt=th, uu=uơ", headerHeight: 35, footerHeight: 45,
                items: [
                    CTTableItem(title: "Kiểu gõ Telex cải tiến",
                        switchBlock: {
                            on in
                            
                        }
                    ),
                ]
            ),
            
            CTCategoryInfo(footerTitle: "Ngừng bỏ dấu với từ không phải tiếng Việt", headerHeight: 15, footerHeight: 30,
                items: [
                    CTTableItem(title: "Kiễm tra chính tả",
                        switchBlock: {
                            on in
                            
                        }
                    ),
                ]
            ),
            
            CTCategoryInfo(footerTitle: "Dùng hoà thuý, (thay cho hòa túy)", headerHeight: 15, footerHeight: 30,
                items: [
                    CTTableItem(title: "Bỏ dấu kiểu mới",
                        switchBlock: {
                            on in
                            
                        }
                    ),
                ]
            ),
            
            CTCategoryInfo(footerTitle: "Gõ f,j,w cho ph,gi , qua VD: wá -> quá", headerHeight: 15, footerHeight: 30,
                items: [
                    CTTableItem(title: "Gõ nhanh phụ âm đầu",
                        switchBlock: {
                            on in
                            
                        }
                    ),
                ]
            ),
            
            CTCategoryInfo(footerTitle: "Gõ h,k,g cho nh,ch,ng  VD: ảh -> ảnh", headerHeight: 15, footerHeight: 30,
                items: [
                    CTTableItem(title: "Gõ nhanh phụ âm cuối",
                        switchBlock: {
                            on in
                            
                        }
                    ),
                ]
            ),
            
        ]
        
        var tuyChinhDataSources = [
            CTCategoryInfo(headerTitle: "CHỌN SỐ TỪ GỢI Ý", headerHeight: 50,
                items: [
                    CTTableItem(title: "Tự động", isCheckMark: true,  isCheck: true,
                        selectBlock: {
                            // select Block
                            
                        }, deSelectBlock: {
                            // deselect block
                    }),
                    CTTableItem(title: "3 từ", isCheckMark: true,
                        selectBlock: {
                            // select Block
                            
                        }, deSelectBlock: {
                            // deselect block
                    }),
                    CTTableItem(title: "5 từ", isCheckMark: true,
                        selectBlock: {
                            // select Block
                            
                        }, deSelectBlock: {
                            // deselect block
                    }),
                    CTTableItem(title: "Tắt", isCheckMark: true,
                        selectBlock: {
                            // select Block
                            
                        }, deSelectBlock: {
                            // deselect block
                    }),
                ]
            ),
            CTCategoryInfo(headerHeight: 15, footerHeight: 15,
                items: [
                    CTTableItem(title: "Âm bàn phím",
                        switchBlock: {
                            on in
                            
                        }
                    ),
                ]
            ),
            CTCategoryInfo(headerHeight: 15, footerHeight: 15,
                items: [
                    CTTableItem(title: "Tự động viết hoa",
                        switchBlock: {
                            on in
                            
                        }
                    ),
                ]
            ),
            CTCategoryInfo(footerTitle: "Tự động thêm dấu cách sau dấu .,!? hoặc sau khi chọn từ gợi ý",headerHeight: 15, footerHeight: 45,
                items: [
                    CTTableItem(title: "Tự động thêm dấu cách",
                        switchBlock: {
                            on in
                            
                        }
                    ),
                ]
            ),
            CTCategoryInfo(footerTitle: "Luôn hiển thị hàng phím số trong tất cả kiểu gõ (Telex đơn giản, Telex đầy đủ, VNI)",headerHeight: 20, footerHeight: 45,
                items: [
                    CTTableItem(title: "Hàng phím số",
                        switchBlock: {
                            on in
                            
                        }
                    ),
                ]
            ),
            CTCategoryInfo(footerTitle: "Nếu bật tình năng này bàn phím sẽ luôn hiện thị phím ., ở dòng cuối cùng. Nếu tắt: phím Global mặc định của iOS luôn hiển thị",headerHeight: 20, footerHeight: 60,
                items: [
                    CTTableItem(title: "Phím ., bổ sung",
                        switchBlock: {
                            on in
                            
                        }
                    ),
                ]
            ),

        ]
        
        var dataSources = [
            CTCategoryInfo(headerHeight: 1, items: [
                CTTableItem(title: "Chủ đề bàn phím", image: UIImage(named: "paint"),
                    navigationBlock: {
                        let vc = self.storyboard?.instantiateViewControllerWithIdentifier(NSStringFromClass(ThemeViewController.classForCoder()).componentsSeparatedByString(".").last!)
                        self.navigationController?.pushViewController(vc!, animated: true)
                })
                ]
            ),
            CTCategoryInfo(
                items: [
                    CTTableItem(title: "Kiểu gõ", image: UIImage(named: "keyboard"),
                        navigationBlock: {
                            let kieuGoViewController = self.storyboard?.instantiateViewControllerWithIdentifier(NSStringFromClass(CTTableViewController.classForCoder()).componentsSeparatedByString(".").last!) as! CTTableViewController
                            kieuGoViewController.dataSources = kieuGoDataSource
                            kieuGoViewController.title = "Kiểu gõ"
                            
                            self.navigationController?.pushViewController(kieuGoViewController, animated: true)
                    }),
                    CTTableItem(title: "Tuỳ chỉnh", image: UIImage(named: "setting"),
                        navigationBlock: {
                            let settingViewController = self.storyboard?.instantiateViewControllerWithIdentifier(NSStringFromClass(CTTableViewController.classForCoder()).componentsSeparatedByString(".").last!) as! CTTableViewController
                            settingViewController.dataSources = tuyChinhDataSources
                            settingViewController.title = "Tuỳ chỉnh"
                            
                            self.navigationController?.pushViewController(settingViewController, animated: true)
                    }),
                ]
            ),
            CTCategoryInfo(
                items: [
                    CTTableItem(title: "Hướng dẫn cài đặt", image: UIImage(named: "guide"),
                        navigationBlock: {
                            let guideViewController = self.storyboard?.instantiateViewControllerWithIdentifier(NSStringFromClass(GuideViewController.classForCoder()).componentsSeparatedByString(".").last!) as! GuideViewController
//                            settingViewController.dataSources = tuyChinhDataSources
                            guideViewController.title = "Hướng dẫn cài đặt"
                            
                            self.navigationController?.pushViewController(guideViewController, animated: true)
                    }),
                    CTTableItem(title: "Góp ý & thông tin", image: UIImage(named: "comment"),
                        navigationBlock: {
                            let feedBackViewController = self.storyboard?.instantiateViewControllerWithIdentifier(NSStringFromClass(FeedbackViewController.classForCoder()).componentsSeparatedByString(".").last!) as! FeedbackViewController
                            
                            let navigationController = UINavigationController(rootViewController: feedBackViewController)
                            
                            self.navigationController!.presentViewController(navigationController, animated: true, completion: nil)
                    }),
                    CTTableItem(title: "Đánh giá BanLa Key", image: UIImage(named: "like"),
                        navigationBlock: {
                            
                    }),
                ]
            ),
        ]
        
        self.tableView.dataSources = dataSources
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        
        
    }
    
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
    }

}








