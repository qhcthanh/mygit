//
//  BLKeyboard.swift
//  BanLaKey
//
//  Created by BaoNQ on 8/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

let isIPad: Bool = {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
        return true
    }
    return false
}()

#if isIPad
    typealias KeyboardMetrics = PadKeyboardMetrics
#else
    typealias KeyboardMetrics = PhoneKeyboardMetrics
#endif

class BLKeyboard : UIView {

    var textInputVC: UIInputViewController?
    var imageView: UIImageView?
    var timer: NSTimer?
    

    // Buttons color
    // 1st row
    var QButton: BLKey?
    var WButton: BLKey?
    var EButton: BLKey?
    var RButton: BLKey?
    var TButton: BLKey?
    var YButton: BLKey?
    var UButton: BLKey?
    var IButton: BLKey?
    var OButton: BLKey?
    var PButton: BLKey?
    
    // 2nd row
    var AButton: BLKey?
    var SButton: BLKey?
    var DButton: BLKey?
    var FButton: BLKey?
    var GButton: BLKey?
    var HButton: BLKey?
    var JButton: BLKey?
    var KButton: BLKey?
    var LButton: BLKey?
    
    // 3rd row
    var ZButton: BLKey?
    var XButton: BLKey?
    var CButton: BLKey?
    var VButton: BLKey?
    var BButton: BLKey?
    var NButton: BLKey?
    var MButton: BLKey?
    
    // Other keys
    var shiftButton: BLLockKey?
    var deleteButton: BLKey?
    var globeButton: BLKey?
    var spaceButton: BLKey?
    var returnButton: BLKey?
    var keyModeButton: BLKey?
    
    
    // Number keys
    var zeroButton: BLKey?
    var oneButton: BLKey?
    var twoButton: BLKey?
    var threeButton: BLKey?
    var fourButton: BLKey?
    var fiveButton: BLKey?
    var sixButton: BLKey?
    var sevenButton: BLKey?
    var eightButton: BLKey?
    var nineButton: BLKey?
    
    var minusButton: BLKey? // -
    var slashButton: BLKey? // /
    var colonButton: BLKey? // :
    var semicolonButton: BLKey? // ;
    var leftParenthesisButton: BLKey? // (
    var rightParenthesisButton: BLKey? // )
    var dollarSignButton: BLKey? // $
    var ampersandButton: BLKey? // &
    var atSignButton: BLKey? // @
    var quotationMarkButton: BLKey? // "
    var fullStopButton: BLKey? // .
    var commaButton: BLKey? // ,
    var questionMarkButton : BLKey? // ?
    var exclamationMarkButton: BLKey? // !
    var apostropheButton: BLKey? // '
    var graveAccentButton: BLKey? // `
    
    
    // Number keys page 2
    var leftSquareBracketButton: BLKey? // [
    var rightSquareBracketButton: BLKey? // ]
    var leftCurlyBracketButton: BLKey? // {
    var rightCurlyBracketButton: BLKey? // }
    var sharpButton: BLKey? // #
    var percentSignButton: BLKey? // %
    var circumflexAccentButton: BLKey? // ^
    var asteriskButton: BLKey? // *
    var plusSignButton: BLKey? // +
    var equalSignButton: BLKey? // =
    var lowLineButton: BLKey? // _
    var backslashButton: BLKey? // \
    var verticalBarButton: BLKey? // |
    var tildeButton: BLKey? // ~
    var lessThanSignButton: BLKey? // <
    var greaterThanSignButton: BLKey? // >
    var euroSignButton: BLKey? // €
    var poundSignButton: BLKey? // £
    var yenSignButton: BLKey? // ¥
    var centSignButton: BLKey? // ¢
    
    var blackCircleButton: BLKey? // ●
    var registerdSignButton: BLKey? // ®
    var copyrightSignButton: BLKey? // ©
    var degreeCelsiusButton: BLKey? // ℃
    var degreeFahrenheitButton: BLKey? // ℉
    
    init(frame: CGRect, inputViewController: UIInputViewController) {
        
        self.textInputVC = inputViewController
        
        var screenWidth: CGFloat = frame.size.width
        var screenHeight: CGFloat = frame.size.height
        
        var keyboardMetrics: KeyboardMetrics
        
        #if isIPad
            keyboardMetrics = generatePadKeyboardMetrics(screenWidth, screenHeight)
        #else
            keyboardMetrics = generatePhoneKeyboardMetrics(screenWidth, screenHeight)
        #endif
        
        self.QButton = BLKey.init(frame: keyboardMetrics.QButton, title: "Q", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.QButton)
        self.WButton = BLKey.init(frame: keyboardMetrics.WButton, title: "W", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.WButton)
        self.EButton = BLKey.init(frame: keyboardMetrics.EButton, title: "E", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.EButton)
        self.RButton = BLKey.init(frame: keyboardMetrics.RButton, title: "R", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.RButton)
        self.TButton = BLKey.init(frame: keyboardMetrics.TButton, title: "T", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.TButton)
        self.YButton = BLKey.init(frame: keyboardMetrics.YButton, title: "Y", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.YButton)
        self.UButton = BLKey.init(frame: keyboardMetrics.UButton, title: "U", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.UButton)
        self.IButton = BLKey.init(frame: keyboardMetrics.IButton, title: "I", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.IButton)
        self.OButton = BLKey.init(frame: keyboardMetrics.OButton, title: "O", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.OButton)
        self.PButton = BLKey.init(frame: keyboardMetrics.PButton, title: "P", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.PButton)
       
        self.AButton = BLKey.init(frame: keyboardMetrics.AButton, title: "A", extendedCharacters: ["Â", "Ă", "Á", "À"], originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.AButton)
        self.SButton = BLKey.init(frame: keyboardMetrics.SButton, title: "S", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.SButton)
        self.DButton = BLKey.init(frame: keyboardMetrics.DButton, title: "D", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.DButton)
        self.FButton = BLKey.init(frame: keyboardMetrics.FButton, title: "F", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.FButton)
        self.GButton = BLKey.init(frame: keyboardMetrics.GButton, title: "G", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.GButton)
        self.HButton = BLKey.init(frame: keyboardMetrics.HButton, title: "H", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.HButton)
        self.JButton = BLKey.init(frame: keyboardMetrics.JButton, title: "J", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.JButton)
        self.KButton = BLKey.init(frame: keyboardMetrics.KButton, title: "K", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.KButton)
        self.LButton = BLKey.init(frame: keyboardMetrics.LButton, title: "L", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.LButton)
       
        self.ZButton = BLKey.init(frame: keyboardMetrics.ZButton, title: "Z", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.ZButton)
        self.XButton = BLKey.init(frame: keyboardMetrics.XButton, title: "X", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.XButton)
        self.CButton = BLKey.init(frame: keyboardMetrics.CButton, title: "C", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.CButton)
        self.VButton = BLKey.init(frame: keyboardMetrics.VButton, title: "V", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.VButton)
        self.BButton = BLKey.init(frame: keyboardMetrics.BButton, title: "B", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.BButton)
        self.NButton = BLKey.init(frame: keyboardMetrics.NButton, title: "N", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.NButton)
        self.MButton = BLKey.init(frame: keyboardMetrics.MButton, title: "M", originalPopupFrame: keyboardMetrics.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: BLThemeManager.currentTheme.MButton)

        var image: UIImage = UIImage.init(named: BLThemeManager.currentTheme.shiftDefaultButtonImage)!
        self.shiftButton = BLLockKey.init(frame: keyboardMetrics.leftShiftButton, title: "", image: image, keyType: BLKeyType.SpecialKey, backgroundColor: BLThemeManager.currentTheme.shiftButton)
        
        image = UIImage.init(named: BLThemeManager.currentTheme.deleteButtonImage)!
        self.deleteButton = BLKey.init(frame: keyboardMetrics.deleteButton, title: "", image: image, keyType: BLKeyType.SpecialKey, backgroundColor: BLThemeManager.currentTheme.deleteButton)
        
        self.keyModeButton = BLKey.init(frame: keyboardMetrics.keyModeButton, title: "123", keyType: BLKeyType.SpecialKey, backgroundColor: BLThemeManager.currentTheme.keyModeButton)
        
        image = UIImage.init(named: BLThemeManager.currentTheme.globeButtonImage)!
        self.globeButton = BLKey.init(frame: keyboardMetrics.globeButton, title: "", image: image, keyType: BLKeyType.SpecialKey, backgroundColor: BLThemeManager.currentTheme.globeButton)
        
        self.spaceButton = BLKey.init(frame: keyboardMetrics.spaceButton, title: "Space", keyType: BLKeyType.SpecialKey, backgroundColor: BLThemeManager.currentTheme.spaceButton)
        self.returnButton = BLKey.init(frame: keyboardMetrics.returnButton, title: "Return", keyType: BLKeyType.SpecialKey, backgroundColor: BLThemeManager.currentTheme.returnButton)

        self.touchToView = [:]
        
        super.init(frame: frame)
        
        self.contentMode = UIViewContentMode.Redraw
        self.multipleTouchEnabled = true
        self.userInteractionEnabled = true
        self.opaque = false
        self.backgroundColor = BLThemeManager.currentTheme.backgroundColor
        
        self.addSubview(self.QButton!)
        self.addSubview(self.WButton!)
        self.addSubview(self.EButton!)
        self.addSubview(self.RButton!)
        self.addSubview(self.TButton!)
        self.addSubview(self.YButton!)
        self.addSubview(self.UButton!)
        self.addSubview(self.IButton!)
        self.addSubview(self.OButton!)
        self.addSubview(self.PButton!)
        self.addSubview(self.AButton!)
        self.addSubview(self.SButton!)
        self.addSubview(self.DButton!)
        self.addSubview(self.FButton!)
        self.addSubview(self.GButton!)
        self.addSubview(self.HButton!)
        self.addSubview(self.JButton!)
        self.addSubview(self.KButton!)
        self.addSubview(self.LButton!)
        self.addSubview(self.ZButton!)
        self.addSubview(self.XButton!)
        self.addSubview(self.CButton!)
        self.addSubview(self.VButton!)
        self.addSubview(self.BButton!)
        self.addSubview(self.NButton!)
        self.addSubview(self.MButton!)
        
        self.addSubview(self.shiftButton!)
        self.addSubview(self.deleteButton!)
        self.addSubview(self.keyModeButton!)
        self.addSubview(self.globeButton!)
        self.addSubview(self.spaceButton!)
        self.addSubview(self.returnButton!)
     
        
        

        
        self.KButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.KButton?.addTarget(self.KButton, action: #selector(self.KButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.KButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        
        
        self.QButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.QButton?.addTarget(self.QButton, action: #selector(self.QButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.QButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.WButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.WButton?.addTarget(self.WButton, action: #selector(self.WButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.WButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.EButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.EButton?.addTarget(self.EButton, action: #selector(self.EButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.EButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.RButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.RButton?.addTarget(self.RButton, action: #selector(self.RButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.RButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.TButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.TButton?.addTarget(self.TButton, action: #selector(self.TButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.TButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.YButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.YButton?.addTarget(self.YButton, action: #selector(self.YButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.YButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.UButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.UButton?.addTarget(self.UButton, action: #selector(self.UButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.UButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.IButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.IButton?.addTarget(self.IButton, action: #selector(self.IButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.IButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.OButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.OButton?.addTarget(self.OButton, action: #selector(self.OButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.OButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.PButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.PButton?.addTarget(self.PButton, action: #selector(self.PButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.PButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        
        
        self.AButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.AButton?.addTarget(self.AButton, action: #selector(self.AButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.AButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.SButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.SButton?.addTarget(self.SButton, action: #selector(self.SButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.SButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.DButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.DButton?.addTarget(self.DButton, action: #selector(self.DButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.DButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.FButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.FButton?.addTarget(self.FButton, action: #selector(self.FButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.FButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.GButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.GButton?.addTarget(self.GButton, action: #selector(self.GButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.GButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.HButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.HButton?.addTarget(self.HButton, action: #selector(self.HButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.HButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.JButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.JButton?.addTarget(self.JButton, action: #selector(self.JButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.JButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.KButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.KButton?.addTarget(self.KButton, action: #selector(self.KButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.KButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.LButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.LButton?.addTarget(self.LButton, action: #selector(self.LButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.LButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        
        
        self.ZButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.ZButton?.addTarget(self.ZButton, action: #selector(self.ZButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.ZButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.XButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.XButton?.addTarget(self.XButton, action: #selector(self.XButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.XButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.CButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.CButton?.addTarget(self.CButton, action: #selector(self.CButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.CButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.VButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.VButton?.addTarget(self.VButton, action: #selector(self.VButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.VButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.BButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.BButton?.addTarget(self.BButton, action: #selector(self.BButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.BButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.NButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.NButton?.addTarget(self.NButton, action: #selector(self.NButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.NButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        
        self.MButton?.addTarget(self, action: #selector(showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
        self.MButton?.addTarget(self.MButton, action: #selector(self.MButton!.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
        self.MButton?.addTarget(self, action: #selector(hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
        

        self.QButton?.addTarget(self, action: #selector(QButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.WButton?.addTarget(self, action: #selector(WButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.EButton?.addTarget(self, action: #selector(EButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.RButton?.addTarget(self, action: #selector(RButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.TButton?.addTarget(self, action: #selector(TButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.YButton?.addTarget(self, action: #selector(YButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.UButton?.addTarget(self, action: #selector(UButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.IButton?.addTarget(self, action: #selector(IButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.OButton?.addTarget(self, action: #selector(OButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.PButton?.addTarget(self, action: #selector(PButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)

        
        self.AButton?.addTarget(self, action: #selector(AButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.SButton?.addTarget(self, action: #selector(SButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.DButton?.addTarget(self, action: #selector(DButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.FButton?.addTarget(self, action: #selector(FButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.GButton?.addTarget(self, action: #selector(GButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.HButton?.addTarget(self, action: #selector(HButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.JButton?.addTarget(self, action: #selector(JButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.KButton?.addTarget(self, action: #selector(KButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.LButton?.addTarget(self, action: #selector(LButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)

        
        self.ZButton?.addTarget(self, action: #selector(ZButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.XButton?.addTarget(self, action: #selector(XButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.CButton?.addTarget(self, action: #selector(CButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.VButton?.addTarget(self, action: #selector(VButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.BButton?.addTarget(self, action: #selector(BButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.NButton?.addTarget(self, action: #selector(NButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.MButton?.addTarget(self, action: #selector(MButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)

        
        self.shiftButton?.addTarget(self, action: #selector(shiftButtonTapped), forControlEvents: UIControlEvents.TouchDown)
        self.shiftButton?.addTarget(self, action: #selector(shiftButtonDoubleTapped), forControlEvents: UIControlEvents.TouchDownRepeat)
        
        self.deleteButton?.addTarget(self, action: #selector(deleteButtonTapped), forControlEvents: UIControlEvents.TouchDown)
        self.deleteButton?.addTarget(self, action: #selector(deleteButtonReleased), forControlEvents: UIControlEvents.TouchUpInside)
        self.deleteButton?.addTarget(self, action: #selector(deleteButtonReleased), forControlEvents: UIControlEvents.TouchUpOutside)

        self.globeButton?.addTarget(self, action: #selector(globeButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.spaceButton?.addTarget(self, action: #selector(spaceButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.returnButton?.addTarget(self, action: #selector(returnButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    
    // POPUP DELAY //
    
    var keyWithDelayedPopup: BLKey?
    var popupDelayTimer: NSTimer?
    
    func showPopup(sender: BLKey) {
        print("------------------------------")
        print("showpopup touchdown")
        if sender == self.keyWithDelayedPopup {
            self.popupDelayTimer?.invalidate()
        }
        sender.showPopup()
    }
    
    func hidePopupDelay(sender: BLKey) {
        print("hidepopupdelay")
        self.popupDelayTimer?.invalidate()
        
        if sender != self.keyWithDelayedPopup {
            self.keyWithDelayedPopup?.hidePopup()
            self.keyWithDelayedPopup = sender
        }
        
        if sender.popupShowed == true {
            self.popupDelayTimer = NSTimer.scheduledTimerWithTimeInterval(0.05, target: self, selector: #selector(hidePopupCallback), userInfo: nil, repeats: false)
        }
    }
    
    func hidePopupCallback() {
        print("hidepopupcallback")
        self.keyWithDelayedPopup?.hidePopup()
        self.keyWithDelayedPopup = nil
        self.popupDelayTimer = nil
    }
    
    // POPUP DELAY END //
    
    func updateState(frame: CGRect) {
        
        self.frame = frame
        
        var screenWidth: CGFloat = frame.size.width
        var screenHeight: CGFloat = frame.size.height
        
        var keyboardMetrics: KeyboardMetrics
        
        #if isIPad
            keyboardMetrics = generatePadKeyboardMetrics(screenWidth, screenHeight)
        #else
            keyboardMetrics = generatePhoneKeyboardMetrics(screenWidth, screenHeight)
        #endif
        
        // Update popup frame
        self.QButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.WButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.EButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.RButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.TButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.YButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.UButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.IButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.OButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.PButton!.updatePopupFrame(keyboardMetrics.popupFrame)

        
        self.AButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.SButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.DButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.FButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.GButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.HButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.JButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.KButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.LButton!.updatePopupFrame(keyboardMetrics.popupFrame)

        
        self.ZButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.XButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.CButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.VButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.BButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.NButton!.updatePopupFrame(keyboardMetrics.popupFrame)
        self.MButton!.updatePopupFrame(keyboardMetrics.popupFrame)

        
        
        // Update key frame
        self.QButton!.frame = keyboardMetrics.QButton
        self.WButton!.frame = keyboardMetrics.WButton
        self.EButton!.frame = keyboardMetrics.EButton
        self.RButton!.frame = keyboardMetrics.RButton
        self.TButton!.frame = keyboardMetrics.TButton
        self.YButton!.frame = keyboardMetrics.YButton
        self.UButton!.frame = keyboardMetrics.UButton
        self.IButton!.frame = keyboardMetrics.IButton
        self.OButton!.frame = keyboardMetrics.OButton
        self.PButton!.frame = keyboardMetrics.PButton
        
        self.AButton!.frame = keyboardMetrics.AButton
        self.SButton!.frame = keyboardMetrics.SButton
        self.DButton!.frame = keyboardMetrics.DButton
        self.FButton!.frame = keyboardMetrics.FButton
        self.GButton!.frame = keyboardMetrics.GButton
        self.HButton!.frame = keyboardMetrics.HButton
        self.JButton!.frame = keyboardMetrics.JButton
        self.KButton!.frame = keyboardMetrics.KButton
        self.LButton!.frame = keyboardMetrics.LButton
        
        self.ZButton!.frame = keyboardMetrics.ZButton
        self.XButton!.frame = keyboardMetrics.XButton
        self.CButton!.frame = keyboardMetrics.CButton
        self.VButton!.frame = keyboardMetrics.VButton
        self.BButton!.frame = keyboardMetrics.BButton
        self.NButton!.frame = keyboardMetrics.NButton
        self.MButton!.frame = keyboardMetrics.MButton
        
        self.shiftButton!.frame = keyboardMetrics.leftShiftButton
        self.deleteButton!.frame = keyboardMetrics.deleteButton
        self.keyModeButton!.frame = keyboardMetrics.keyModeButton
        self.globeButton!.frame = keyboardMetrics.globeButton
        self.spaceButton!.frame = keyboardMetrics.spaceButton
        self.returnButton!.frame = keyboardMetrics.returnButton

        self.setNeedsDisplay()
    }
    
    
    func QButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("q")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("Q")
        }
    }
    func WButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("w")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("W")
        }    }
    func EButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("e")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("E")
        }    }
    func RButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("r")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("R")
        }    }
    func TButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("t")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("T")
        }    }
    func YButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("y")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("Y")
        }    }
    func UButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("u")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("U")
        }
    }
    func IButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("i")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("I")
        }
    }
    func OButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("o")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("O")
        }
    }
    func PButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            
            self.textInputVC!.textDocumentProxy.insertText("p")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("P")
        }
    }
    func AButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("a")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("A")
        }
    }
    func SButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("s")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("S")
        }
    }
    func DButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("d")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("D")
        }
    }
    func FButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("f")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("F")
        }
    }
    func GButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("g")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("G")
        }
    }
    func HButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("h")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("H")
        }
    }
    func JButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("j")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("J")
        }
    }
    func KButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("k")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("K")
        }
    }
    func LButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("l")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("L")
        }
    }
    func ZButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("z")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("Z")
        }
    }
    func XButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("x")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("X")
        }
    }
    func CButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            
            self.textInputVC!.textDocumentProxy.insertText("c")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("C")
        }
    }
    
    func VButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("v")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("V")
        }
    }
    
    func BButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("b")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("B")
        }
    }
    
    func NButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("n")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("N")
        }
    }
    
    func MButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.textInputVC!.textDocumentProxy.insertText("m")
        }
        else {
            self.textInputVC!.textDocumentProxy.insertText("M")
        }
    }

    func shiftButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.shiftButton!.lockState = BLLockKeyType.Once
            self.shiftButton!.selected = true
        }
        else {
            self.shiftButton!.lockState = BLLockKeyType.Default
            self.shiftButton!.selected = false
        }
    }
    func shiftButtonDoubleTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Forever {
            self.shiftButton!.lockState = BLLockKeyType.Default
            self.shiftButton!.selected = false
        }
        else {
            self.shiftButton!.lockState = BLLockKeyType.Forever
            self.shiftButton!.selected = true
        }
    }
    func deleteButtonTapped() {
        self.deleteBackward()
        
        self.timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(deleteTimer), userInfo: nil, repeats: true)
        
        weak var weakSelf = self
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (Int64)(NSEC_PER_SEC/2)), dispatch_get_main_queue(), {
            weakSelf?.timer?.fire()
        })
    }
    func deleteButtonReleased() {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    func globeButtonTapped() {
        self.textInputVC?.advanceToNextInputMode()
    }
    func spaceButtonTapped() {
        self.textInputVC!.textDocumentProxy.insertText(" ")
    }
    func returnButtonTapped() {
        self.textInputVC!.textDocumentProxy.insertText("\n")
    }
    
    func deleteTimer(timer: NSTimer) {
        if self.deleteButton!.highlighted {
            self.deleteBackward()
        }
        else {
            timer.invalidate()
            self.timer = nil
        }
    }
    
    func deleteBackward() {
        self.textInputVC!.textDocumentProxy.deleteBackward()
        
        self.updateReturnButtonState()
        self.updateShiftButtonState()
    }
    
    func updateReturnButtonState() {
        if  self.textInputVC!.textDocumentProxy.enablesReturnKeyAutomatically == false &&
            self.textInputVC!.textDocumentProxy.documentContextBeforeInput!.characters.count == 0 &&
            self.textInputVC!.textDocumentProxy.documentContextAfterInput!.characters.count == 0 {
            
            self.returnButton!.enabled = false
        }
        else {
            self.returnButton!.enabled = true
        }
    }
    
    func updateShiftButtonState() {
//        if self.shiftButton!.lockState == BLLockKeyType.Forever {
//            return
//        }
//        
//        var selected: Bool = false
//        
//        let beforeInput: String = self.textInputVC!.textDocumentProxy.documentContextBeforeInput!
//        
//        switch self.textInputVC!.textDocumentProxy.autocapitalizationType {
//        case UITextAutocapitalizationType.AllCharacters:
//            selected = true
//            
//            break
//        case UITextAutocapitalizationType.Sentences:
//            self
//            if beforeInput.characters.count == 0 ||
//            
//            break
//        default:
//            
//        }
    }
    
    
    
    /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///
    
    var touchToView: [UITouch:UIView]
    var swipeTimer: NSTimer?
    let kBLSwipeTimeInterval: NSTimeInterval = 0.2
    var cancelTouchesEnd: Bool = false
    
    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
        if (self.hidden || self.alpha == 0 || !self.userInteractionEnabled) {
            return nil
        }
        else {
            return CGRectContainsPoint(self.bounds, point) ? self : nil
        }
    }
    
    func handleControl(view: UIView?, controlEvent: UIControlEvents) {
        if let control = view as? BLKey {
            let targets = control.allTargets()
            for target in targets {
                if let actions = control.actionsForTarget(target, forControlEvent: controlEvent) {
                    for action in actions {
                        let selectorString = action
                        let selector = Selector(selectorString)
                        
                        control.sendAction(selector, to: target, forEvent: nil)
                    }
                }
            }
        }
    }
    
    func findNearestView(position: CGPoint) -> UIView? {
        if !self.bounds.contains(position) {
            return nil
        }
        
        var closest: (UIView, CGFloat)? = nil
        
        for anyView in self.subviews {
            let view = anyView
            
            if !view.hidden {
                view.alpha = 1
                
                let distance = distanceBetween(view.frame, point: position)
                
                if closest != nil {
                    if distance < closest!.1 {
                        closest = (view, distance)
                    }
                }
                else {
                    closest = (view, distance)
                }
            }
        }
        
        if (closest != nil) {
            return closest!.0
        }
        else {
            return nil
        }
    }
    
    func distanceBetween(rect: CGRect, point: CGPoint) -> CGFloat {
        if CGRectContainsPoint(rect, point) {
            return 0
        }
        
        var closest = rect.origin
        
        if (rect.origin.x + rect.size.width < point.x) {
            closest.x += rect.size.width
        }
        else if (point.x > rect.origin.x) {
            closest.x = point.x
        }
        if (rect.origin.y + rect.size.height < point.y) {
            closest.y += rect.size.height
        }
        else if (point.y > rect.origin.y) {
            closest.y = point.y
        }
        
        let a = pow(Double(closest.y - point.y), 2)
        let b = pow(Double(closest.x - point.x), 2)
        
        return CGFloat(sqrt(a + b));
    }
    
    func resetTrackedViews() {
        for view in self.touchToView.values {
            self.handleControl(view, controlEvent: .TouchCancel)
        }
        self.touchToView.removeAll(keepCapacity: true)
    }
    
    func ownView(newTouch: UITouch, viewToOwn: UIView?) -> Bool {
        var foundView = false
        
        if viewToOwn != nil {
            for (touch, view) in self.touchToView {
                if viewToOwn == view {
                    if touch == newTouch {
                        break
                    }
                    else {
                        self.touchToView[touch] = nil
                        foundView = true
                    }
                    break
                }
            }
        }
        self.touchToView[newTouch] = viewToOwn
        
        return foundView
    }
    
    func swipeHandler() {
        
        if let swipeTimer = self.swipeTimer  {
            if (swipeTimer.valid) {
                self.cancelTouchesEnd = true
                
                let touch: UITouch = self.swipeTimer!.userInfo as! UITouch
                
                let view = self.touchToView[touch]
                
                self.swipeTimer?.invalidate()
                
                let touchPosition = touch.locationInView(self)
                
                if self.bounds.contains(touchPosition) {
                    self.handleControl(view, controlEvent: .TouchUpInside)
                }
                else {
                    self.handleControl(view, controlEvent: .TouchCancel)
                }
            }
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.cancelTouchesEnd = false
        
        for touch in touches {
            let position = touch.locationInView(self)
            let view = findNearestView(position)
            
            let viewChangedOwnership = self.ownView(touch, viewToOwn: view)
            
            if !viewChangedOwnership {
                self.swipeTimer = NSTimer.scheduledTimerWithTimeInterval(kBLSwipeTimeInterval, target: self, selector: #selector(swipeHandler), userInfo: touch, repeats: false)
                
                self.handleControl(view, controlEvent: .TouchDown)
                
                if touch.tapCount > 1 {
                    // Two events
                    self.handleControl(view, controlEvent: .TouchDownRepeat)
                }
            }
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let position = touch.locationInView(self)
            
            let oldView = self.touchToView[touch]
            let newView = findNearestView(position)
            
            if oldView != newView {
                
                self.swipeTimer?.invalidate()
                self.swipeTimer = NSTimer.scheduledTimerWithTimeInterval(kBLSwipeTimeInterval, target: self, selector: #selector(swipeHandler), userInfo: touch, repeats: false)
                
                self.handleControl(oldView, controlEvent: .TouchDragExit)
                
                let viewChangedOwnership = self.ownView(touch, viewToOwn: newView)
                
                if !viewChangedOwnership {
                    self.handleControl(newView, controlEvent: .TouchDragEnter)
                }
                else {
                    self.handleControl(newView, controlEvent: .TouchDragInside)
                }
            }
            else {
                self.handleControl(oldView, controlEvent: .TouchDragInside)
            }
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.swipeTimer?.invalidate()
        
        if !self.cancelTouchesEnd {
            for touch in touches {
                let view = self.touchToView[touch]
            
                let touchPosition = touch.locationInView(self)
                
                if self.bounds.contains(touchPosition) {
                    self.handleControl(view, controlEvent: .TouchUpInside)
                }
                else {
                    self.handleControl(view, controlEvent: .TouchCancel)
                }
                self.touchToView[touch] = nil
            }
        }
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        
        self.swipeTimer?.invalidate()

        if let touches = touches {
            for touch in touches {
                let view = self.touchToView[touch]
                
                self.handleControl(view, controlEvent: .TouchCancel)
                
                self.touchToView[touch] = nil
                
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
