//
//  TextStickerNode.h
//  CameraGPUImage
//
//  Created by VanDao on 6/21/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "StickerNode.h"

@interface TextStickerNode : StickerNode

@property NSString *content;
@property UIColor *color;


- (id)initWithFrame:(CGRect)frame withContent:(NSString*)content;

@end
