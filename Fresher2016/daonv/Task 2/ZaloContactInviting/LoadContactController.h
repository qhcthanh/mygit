//
//  ContactViewController.h
//  ZaloContactInviting
//
//  Created by VanDao on 5/26/16.
//  Copyright © 2016 . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactManager.h"
#import "NewContactController.h"
#import "SectionInfo.h"
#import "UIImage+Contact.h"

@interface LoadContactController : UIViewController <UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate, UINavigationBarDelegate, ContactProtocol>

// Outlet
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UICollectionView *colectionView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightCollectionView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationBar;
- (IBAction)editBarButton:(id)sender;

// Search bar
@property (strong, nonatomic) NSMutableArray<ContactDetail *> *contactFilterList;
// Use on tableView
@property (strong, nonatomic) NSMutableArray<ContactDetail *> *contactList;
@property (strong, nonatomic) NSMutableArray<SectionInfo *> *sectionList;
@property (strong, nonatomic) ImageCacheManager *imageManager;
@property (nonatomic) BOOL isSearching;
@property (nonatomic) BOOL isEditing;
// Use on collectionView
@property (strong, nonatomic) NSMutableArray<ContactDetail *> *collectionContact;
@property(readwrite, nonatomic) NSInteger selectedCount;
// NavigationBar title
@property(strong, nonatomic) UILabel* navigationTitleLabel;
@property(strong, nonatomic) UILabel* selectedPersonNumberLabel;


@end
