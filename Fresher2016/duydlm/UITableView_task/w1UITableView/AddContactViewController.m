//
//  AddContactViewController.m
//  w1UITableView
//
//  Created by manhduydl on 6/3/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "AddContactViewController.h"
#import "Contact.h"

@interface AddContactViewController() <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>

@property UIActionSheet *action;
@property UIImagePickerController *picker;
@property UITextField *currentTextField;
@property UIImage *avatarImage;

@end

@implementation AddContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.width / 2;
    self.avatarImageView.clipsToBounds = YES;
    
    // add click event for avatarImageView
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
    [self.avatarImageView addGestureRecognizer:tap];
    
    self.givenNameTextField.delegate = self;
    self.familyNameTextField.delegate = self;
    self.phoneTextField.delegate = self;
}

- (IBAction)themButtonClick:(id)sender {
    NSString *givenName = self.givenNameTextField.text;
    NSString *familyName = self.familyNameTextField.text;
    NSString *fullName = [NSString stringWithFormat:@"%@ %@", givenName, familyName];
    NSString *identifier = [[NSUUID UUID] UUIDString];
    Contact *newContact = [[Contact alloc] initContactWithGivenname:givenName FamilyName:familyName FullName:fullName Image:self.avatarImage andIdentifier:identifier];
    [self.delegate addNewContact:newContact];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.currentTextField = textField;
}

- (void)tapped:(UITapGestureRecognizer*)tap
{
    [self.currentTextField resignFirstResponder];
    self.action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancle" destructiveButtonTitle:nil otherButtonTitles:@"Take New Photo",@"Choose From Existing",@"Remove Photo", nil];
    [self.action showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (buttonIndex)
    {
        case 0:
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                      message:@"Device has no camera"
                                                                     delegate:nil
                                                            cancelButtonTitle:@"OK"
                                                            otherButtonTitles: nil];
                
                [myAlertView show];
                
            } else {
                self.picker = [[UIImagePickerController alloc] init];
                self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                self.picker.allowsEditing = YES;
                self.picker.delegate = self;
                [self.navigationController presentViewController: self.picker animated: YES completion: nil];
            }
            break;
        case 1:
            self.picker = [[UIImagePickerController alloc] init];
            self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            self.picker.allowsEditing = YES;
            self.picker.delegate = self;
            [self.navigationController presentViewController: self.picker animated: YES completion: nil];
            break;
        case 2:
            self.avatarImageView.image = [UIImage imageNamed:@"defaultAvatar"];
            break;
        default:
            [self.navigationController dismissViewControllerAnimated: YES completion: nil];
            [self.currentTextField becomeFirstResponder];
            break;
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.avatarImageView.image = [info objectForKey:UIImagePickerControllerEditedImage];
    self.avatarImage = self.avatarImageView.image;
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
    //  [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
