//
//  CTSliderView.h
//  CameraGPUImage
//
//  Created by BaoNQ on 6/27/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageView.h"
#import "SNFilter.h"

@protocol HorizontalPageViewDataSource;

@interface HorizontalPageView : UIView <UIScrollViewDelegate>

@property UIScrollView *slider;
@property NSMutableArray<PageView *> *pages;

/* Data for slider*/
@property NSInteger numberOfImages;
@property NSInteger startingIndex;

/* Data source */
@property(nonatomic, weak) id<HorizontalPageViewDataSource> dataSource;

- (void)reloadData;

@end


@protocol HorizontalPageViewDataSource <NSObject>

@optional

/**
 *  Return the starting index in the image list
 */
- (NSInteger)startAtIndex:(HorizontalPageView *)horizontalPageView;

/**
 *  Return number of images in image list
 */
- (NSInteger)numberOfImages:(HorizontalPageView *)horizontalPageView;

/**
 *  Return image for page view at a given index
 */
- (SNFilter *)imageInHorizontalPageView:(HorizontalPageView *)horizontalPageView atIndex:(NSInteger)index;

@end

