//
//  EmojiCategoryCollectionViewCell.swift
//  EmojiKeyboard
//
//  Created by Quach Ha Chan Thanh on 8/14/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import UIKit

protocol EmojiCategoryCollectionViewCellDelegate: NSObjectProtocol {
    func didSelectEmoji(emoji: String)
}

public class EmojiCategoryCollectionViewCell: UICollectionViewCell, PopoverDelegate {
    
    public var emojiCategory: EmojiCategory?
    public var emojiCollectionView: UICollectionView!
    public var collectionViewLayout = UICollectionViewFlowLayout()

    weak var delegate: EmojiCategoryCollectionViewCellDelegate?
    
    // Setting
    
    var minimumPressDuration: Double = 0.06
    var minimumTimeShowPopup: Double = 0.5
    
    let options = [
        .Type(.Up),
        .CornerRadius(15),
        .SideEdge(25),
        .AnimationIn(0),
        .AnimationOut(0),
        .ArrowSize(CGSizeZero),
        .BlackOverlayColor(UIColor.clearColor()),
        .Color(customLightGrayColor),
        .ArrowSize(CGSizeMake(14.0, 12.0))
        ] as [PopoverOption]
    
    // Last Cell selected property
    private weak var lastCellPress: UICollectionViewCell? {
        willSet {
            if let newValue = newValue {
                if newValue != self.lastCellPress {
                    timerShowPopup.invalidate()
                    if let newValue = newValue as? EmojiCollectionViewCell {
                        self.willShowPopupEmoji(newValue)
                    }
                }
            } else {
                self.popoverView.dismiss()
                self.popoverView.delegate = nil
                timerShowPopup.invalidate()
            }
            
            self.lastCellPress = newValue
        }
    }
    
    override private init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    
    public func bindingUI(emojiCategory: EmojiCategory) {
        
        self.emojiCategory = emojiCategory
        
        self.initializeEmojiCollectionView()
    }
    
    var emojiItemCellSize: CGSize {
        get {
            let itemCellWidth = self.bounds.width * 0.12
            return CGSizeMake(itemCellWidth,itemCellWidth)
        }
    }
    
    private func initializeEmojiCollectionView() {
        
        if self.emojiCollectionView == nil {
            self.emojiCollectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: self.collectionViewLayout)
            self.emojiCollectionView.backgroundColor = .clearColor()
            
            self.addSubview(self.emojiCollectionView)
            
            self.emojiCollectionView.constrain(toEdgesOf: self)
            
            let emojiItemCellNib = UINib(nibName: "EmojiCollectionViewCell", bundle: nil)
            self.emojiCollectionView.registerNib(emojiItemCellNib, forCellWithReuseIdentifier: "EmojiItemCell")
            
            
            self.collectionViewLayout.itemSize = emojiItemCellSize
            self.collectionViewLayout.minimumLineSpacing = emojiMinimumLineSpacing
            self.collectionViewLayout.minimumInteritemSpacing = emojiMinimumItemSpacing
            
            self.emojiCollectionView.showsVerticalScrollIndicator = false
            self.emojiCollectionView.showsHorizontalScrollIndicator = false
            
            self.emojiCollectionView.delegate = self
            self.emojiCollectionView.dataSource = self
            
           // let holdEmojiGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.onPressLongEmoji))
//            holdEmojiGesture.minimumPressDuration = self.minimumPressDuration
            //self.emojiCollectionView.addGestureRecognizer(holdEmojiGesture)
            
            let testGesture = BLLongPressGesture(owerView: self.emojiCollectionView)
            testGesture.delegate = self
            
        }
        
        self.emojiCollectionView.reloadData()
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        
        emojiCategory = nil
        self.emojiCollectionView.reloadData()
    
    }
    
    var timerShowPopup: NSTimer = NSTimer()
    var popoverView: Popover = Popover()
    var isShowPopoverView = false
    let viewPopup = UIView()
    
    func showPopupInView(fromView: UIView, customView: UIView, size: CGSize) {
        
        popoverView.dismiss()
        popoverView.delegate = nil
        
        customView.frame.size = size
        
        popoverView = Popover(options: options, showHandler: nil, dismissHandler: nil)
        popoverView.delegate = self
        popoverView.show(customView, fromView: fromView)
    }
    
    func willShowPopupEmoji(emojiCell: UICollectionViewCell) {
        timerShowPopup = NSTimer.scheduledTimerWithTimeInterval(self.minimumTimeShowPopup, target: self, selector: #selector(didShowPupupEmoji), userInfo: ["emoji":emojiCell], repeats: false)
    }
    
    func didShowPupupEmoji() {
        if let emojiCell = self.timerShowPopup.userInfo!["emoji"] as? EmojiCollectionViewCell {
            if let popupView = emojiCell.generatePopupView() {
                self.showPopupInView(emojiCell ,customView: popupView, size: CGSizeMake(emojiItemCellSize.width * 5 + 10, emojiItemCellSize.width + 10))
            }
        }
        
    }
    
//    func onPressLongEmoji(longPressGesture: UILongPressGestureRecognizer) {
//        
//        let locationInCollectionView = longPressGesture.locationInView(longPressGesture.view)
//        
//        if let indexPath = self.emojiCollectionView.indexPathForItemAtPoint(locationInCollectionView),
//            let cell = self.emojiCollectionView.cellForItemAtIndexPath(indexPath)
//        {
//            
//            // Neu moi bat dau thi to xam va gan lastCellPress = no
//            if longPressGesture.state == .Began {
//                
//                if let lastCellPress = self.lastCellPress {
//                    lastCellPress.contentView.backgroundColor = .clearColor()
//                    self.lastCellPress = nil
//                }
//        
//                cell.contentView.backgroundColor = customLightGrayColor
//                self.lastCellPress = cell
//            } // Neu thay doi
//            else if longPressGesture.state == .Changed {
//                // Kiem tra neu khong phai la cell cu thi doi mau bthg cell cu~ va to mau` cell moi
//                if cell != self.lastCellPress {
//                    if let lastCellPress = self.lastCellPress {
//                        lastCellPress.contentView.backgroundColor = .clearColor()
//                    }
//                    cell.contentView.backgroundColor = customLightGrayColor
//                    self.lastCellPress = cell
//                }
//             // Neu touch ket thuc se goi delegate touch emoji da chon va` bo to mau` cell do , gan lastCell = nil
//            } else if longPressGesture.state == .Ended {
//                
//                if self.popoverView.isShow() {
//                    return
//                }
//                
//                if let lastCellPress = self.lastCellPress
//                {
//                    lastCellPress.contentView.backgroundColor = .clearColor()
//                    if let lastCellPress = lastCellPress as? EmojiCollectionViewCell,
//                        let emojiProtocol = lastCellPress.itemProtocol {
//                        
//                        // Call delegate touch emoji
//                        self.didSelectEmoji(emojiProtocol.titleContent)
//                    }
//                }
//                
//                self.lastCellPress = nil
//            }
//        } // Ktra neu lastCell # nil thi remove va gan lai clear color
//        else {
//            if let lastCellPress = self.lastCellPress {
//                lastCellPress.contentView.backgroundColor = .clearColor()
//            }
//            self.lastCellPress = nil
//        }
//    }
    
    func didSelectEmoji(emoji: String) {
        if let delegate = self.delegate {
            delegate.didSelectEmoji(emoji)
        }
    }
    
    public func didDismissPopupOver(popover: Popover) {
        if let cell = self.lastCellPress {
            cell.contentView.backgroundColor = .clearColor()
        }
    }
    
    
}

extension EmojiCategoryCollectionViewCell: BLLongPressGestureDelegate {
    
    public func longPressGestureOnTouchDown(view: UIView) {
        if let cell = view as? EmojiCollectionViewCell {
            if let lastCellPress = self.lastCellPress {
                lastCellPress.contentView.backgroundColor = .clearColor()
                self.lastCellPress = nil
            }
            
            cell.contentView.backgroundColor = customLightGrayColor
            self.lastCellPress = cell
            
        }
        
        
    }
    
    public func longPressGestureOnTouchUp(view: UIView) {
        if view is EmojiCollectionViewCell {
            if self.popoverView.isShow() {
                return
            }
            
            if let lastCellPress = self.lastCellPress
            {
                lastCellPress.contentView.backgroundColor = .clearColor()
                if let lastCellPress = lastCellPress as? EmojiCollectionViewCell,
                    let emojiProtocol = lastCellPress.itemProtocol {
                    
                    // Call delegate touch emoji
                    self.didSelectEmoji(emojiProtocol.titleContent)
                }
            }
            
            self.lastCellPress = nil
        }
    }
    
    public func longPressGestureOnTouchMove(view: UIView) {
        if let cell = view as? EmojiCollectionViewCell {
            // Kiem tra neu khong phai la cell cu thi doi mau bthg cell cu~ va to mau` cell moi
            if cell != self.lastCellPress {
                if let lastCellPress = self.lastCellPress {
                    lastCellPress.contentView.backgroundColor = .clearColor()
                }
                cell.contentView.backgroundColor = customLightGrayColor
                self.lastCellPress = cell
            }

        }
    }
    
    public func longPressGestureOnTouchFail(view: UIView) {
        if let lastCellPress = self.lastCellPress {
            lastCellPress.contentView.backgroundColor = .clearColor()
        }
        self.lastCellPress = nil

    }
    
}

extension EmojiCategoryCollectionViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
    
    public func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let emojiCategory = self.emojiCategory {
            return emojiCategory.emojis.count
        } else {
            return 0
        }
    }
    
    public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let emojiCell = collectionView.dequeueReusableCellWithReuseIdentifier("EmojiItemCell", forIndexPath: indexPath) as! EmojiCollectionViewCell
        let emojiProtocol = emojiCategory!.emojis[indexPath.row]
        emojiCell.bindingUI(emojiProtocol)
        emojiCell.delegate = self.delegate
        
        return emojiCell
    }
    
    public override func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
    
}





