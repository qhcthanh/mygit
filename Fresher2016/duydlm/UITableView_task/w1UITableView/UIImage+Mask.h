//
//  UIImage+Mask.h
//  w1UITableView
//
//  Created by manhduydl on 6/6/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Mask)

/**
 *  Mask image to create a circle avatar from origin avatar of contact
 *
 *  @param image The origin avatar of contact
 *  @param mask  The image is used to mask
 *
 *  @return An UIImage is a circle avatar
 */
- (UIImage *)maskWithImage:(UIImage *)mask;

@end
