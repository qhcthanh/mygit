//
//  UXTextTransitionDetail.m
//  testUIView
//
//  Created by VanDao on 6/28/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import "UXTextTransitionDetail.h"
#import "UXTextTransitionView.h"

@interface UXTextTransitionDetail(){
    NSString *transitionText;
    UIColor *textColor;
    UIFont *textFont;
}

@end

@implementation UXTextTransitionDetail

- (id)initWithTransitionView:(UXTextTransitionView *)view{
    self = [super initWithTransitionView:view];
    
    if (self){
        transitionText = view.textContent;
        textColor = view.textColor;
        textFont = view.textFont;
    }
    
    return self;
}

@end
