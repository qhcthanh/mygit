//
//  BLLightAppearance.swift
//  BanLaKey
//
//  Created by BaoNQ on 8/16/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

class BLLightAppearance: UIColor {
    
    static func lightKeyColor() -> UIColor! {
        
        struct StaticVar {
            static var lightKeyColor: UIColor!
            static var onceToken: dispatch_once_t = 0
        }
        dispatch_once(&StaticVar.onceToken, {
            if (UIDevice.currentDevice().machine().hasPrefix("iPad3,")) {
                StaticVar.lightKeyColor = UIColor.init(white: 254/255.0, alpha: 1.0)
            }
            else {
                StaticVar.lightKeyColor = UIColor.whiteColor()
            }
        })
        return StaticVar.lightKeyColor!
    }
    
    static func lightKeyShadowColor() -> UIColor! {
       
        struct StaticVar {
            static var lightKeyShadowColor: UIColor!
            static var onceToken: dispatch_once_t = 0
        }
        dispatch_once(&StaticVar.onceToken, {
            if (UIDevice.currentDevice().machine().hasPrefix("iPad3,")) {
                StaticVar.lightKeyShadowColor = UIColor.init(red: 142/255.0, green: 145/255.0, blue: 149/255.0, alpha: 1.0)
            }
            else if (UIDevice.currentDevice().machine().hasPrefix("iPhone7,2")) {
                StaticVar.lightKeyShadowColor = UIColor.init(red: 139/255.0, green: 142/255.0, blue: 146/255.0, alpha: 1.0)
            }
            else {
                StaticVar.lightKeyShadowColor = UIColor.init(red: 136/255.0, green: 138/255.0, blue: 142/255.0, alpha: 1.0)
            }
        })
        return StaticVar.lightKeyShadowColor
    }
    
    static func darkKeyColor() -> UIColor! {
        
        struct StaticVar {
            static var darkKeyColor: UIColor!
            static var onceToken: dispatch_once_t = 0
        }
        dispatch_once(&StaticVar.onceToken, {
            if (UIDevice.currentDevice().machine().hasPrefix("iPad3,")) {
                StaticVar.darkKeyColor = UIColor.init(red: 184/255.0, green: 191/255.0, blue: 202/255.0, alpha: 1.0)
            }
            else if (UIDevice.currentDevice().machine().hasPrefix("iPhone5,")) {
                StaticVar.darkKeyColor = UIColor.init(red: 174/255.0, green: 179/255.0, blue: 190/255.0, alpha: 1.0)
            }
            else {
                StaticVar.darkKeyColor = UIColor.init(red: 172/255.0, green: 179/255.0, blue: 190/255.0, alpha: 1.0)
            }
        })
        return StaticVar.darkKeyColor
    }
    
    static func darkKeyShadowColor() -> UIColor! {
        return self.lightKeyShadowColor()
    }
    
    static func darkKeyDisabledTitleColor() -> UIColor! {
        
        struct StaticVar {
            static var darkKeyDisabledTitleColor: UIColor?
            static var onceToken: dispatch_once_t = 0
        }
        dispatch_once(&StaticVar.onceToken, {
            if (UIDevice.currentDevice().machine().hasPrefix("iPhone5,")) {
                StaticVar.darkKeyDisabledTitleColor = UIColor.init(red: 118/255.0, green: 121/255.0, blue: 129/255.0, alpha: 1.0)
            }
            else if (UIDevice.currentDevice().machine().hasPrefix("iPhone6,")
                    || (UIDevice.currentDevice().machine().hasPrefix("iPad4,"))) {
                
                StaticVar.darkKeyDisabledTitleColor = UIColor.init(red: 116/255.0, green: 121/255.0, blue: 129/255.0, alpha: 1.0)
            }
            else if (UIDevice.currentDevice().machine().hasPrefix("iPhone7,2")) {
                StaticVar.darkKeyDisabledTitleColor = UIColor.init(red: 116/255.0, green: 121/255.0, blue: 127/255.0, alpha: 1.0)
            }
            else if (UIDevice.currentDevice().machine().hasPrefix("iPhone7,1")) {
                StaticVar.darkKeyDisabledTitleColor = UIColor.init(red: 118/255.0, green: 123/255.0, blue: 130/255.0, alpha: 1.0)
            } else {
                StaticVar.darkKeyDisabledTitleColor = UIColor.init(red: 125/255.0, green: 129/255.0, blue: 137/255.0, alpha: 1.0)
            }
        })
        return StaticVar.darkKeyDisabledTitleColor
    }
    
    static func blueKeyColor() -> UIColor! {
        struct StaticVar {
            static var blueKeyColor: UIColor!
            static var onceToken: dispatch_once_t = 0
        }
        dispatch_once(&StaticVar.onceToken, {
            if (UIDevice.currentDevice().machine().hasPrefix("iPad3,")) {
                StaticVar.blueKeyColor = UIColor.init(red: 9/255.0, green: 126/255.0, blue: 254/255.0, alpha: 1.0)
            }
            else {
                StaticVar.blueKeyColor = UIColor.init(red: 0/255.0, green: 122/255.0, blue: 255/255.0, alpha: 1.0)
            }
        })
        return StaticVar.blueKeyColor
    }
    
    static func blueKeyShadowColor() -> UIColor! {
        struct StaticVar {
            static var blueKeyShadowColor: UIColor!
            static var onceToken: dispatch_once_t = 0
        }
        dispatch_once(&StaticVar.onceToken, {
            if (UIDevice.currentDevice().machine().hasPrefix("iPhone5,")) {
                StaticVar.blueKeyShadowColor = UIColor.init(red: 105/255.0, green: 106/255.0, blue: 109/255.0, alpha: 1.0)
            }
            else if (UIDevice.currentDevice().machine().hasPrefix("iPhone7,2")) {
                StaticVar.blueKeyShadowColor = UIColor.init(red: 103/255.0, green: 105/255.0, blue: 108/255.0, alpha: 1.0)
            }
            else if (UIDevice.currentDevice().machine().hasPrefix("iPad3,")) {
                StaticVar.blueKeyShadowColor = UIColor.init(red: 107/255.0, green: 109/255.0, blue: 112/255.0, alpha: 1.0)
            }
            else {
                StaticVar.blueKeyShadowColor = UIColor.init(red: 104/255.0, green: 106/255.0, blue: 109/255.0, alpha: 1.0)
            }
        })
        return StaticVar.blueKeyShadowColor
    }
    
    static func blueKeyDisabledTitleColor() -> UIColor! {
        return self.darkKeyDisabledTitleColor()
    }
}
