/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

#import "UXViewController.h"

#import <WebKit/WebKit.h>

@class UXWebViewController;

@protocol UXWebViewControllerDelegate <NSObject>
@optional
- (void)webViewController:(UXWebViewController *)controller didStartLoadingURL:(NSURL *)URL;
- (void)webViewController:(UXWebViewController *)controller didFinishLoadingURL:(NSURL *)URL;
- (void)webViewController:(UXWebViewController *)controller didFailToLoadURL:(NSURL *)URL error:(NSError *)error;
- (void)webViewControllerWillDismiss:(UXWebViewController*)viewController;
@end

@interface UXWebViewController : UXViewController

#pragma mark - Public Properties
@property (nonatomic, weak) id <UXWebViewControllerDelegate> delegate;

// Depending on the version of iOS, one of these will be set
@property (nonatomic, strong) WKWebView *wkWebView;
@property (nonatomic, strong) UIWebView *uiWebView;

@property (nonatomic, weak) UINavigationBar *navigationBar;
@property (nonatomic, weak) UIView *navigationBarSuperView;
@property (nonatomic, strong) UIBarButtonItem *actionButton;
@property (nonatomic, strong) UIColor *tintColor;
@property (nonatomic, strong) UIColor *barTintColor;
@property (nonatomic, assign) BOOL actionButtonHidden;
@property (nonatomic, assign) BOOL hideBarsWithGestures;
@property (nonatomic, assign) BOOL shouldShowToolbar;
@property (nonatomic, assign) BOOL showsURLInNavigationBar;
@property (nonatomic, assign) BOOL showsPageTitleInNavigationBar;

@property (nonatomic, assign) BOOL shouldUseWebKit;
@property (nonatomic, assign) BOOL shouldApplyLinkJump;
@property (nonatomic, assign) BOOL shouldAttachShareLink;

@property (nonatomic, strong) NSArray* cookies;
@property (nonatomic, strong) NSString* URLString;
@property (nonatomic, strong) NSString* HTMLString;

@property (nonatomic, strong) NSTimer *fakeProgressTimer;

/**
 *  Allow for custom activities
 */
@property (nonatomic, strong) NSArray *customActivityItems;

#pragma mark - Public Interface
/**
 *  Load an URL request to web view
 *
 *  @param request The url request
 */
- (void)loadRequest:(NSURLRequest *)request;

/**
 *  Load an URL to web view
 *
 *  @param URL The URL request
 */
- (void)loadURL:(NSURL *)URL;

/**
 *  Load an URL to web view
 *
 *  @param URLString The URL String
 */
- (void)loadURLString:(NSString *)URLString;

/**
 *  Loads an string containing HTML to web view
 *
 *  @param HTMLString The HTML String
 */
- (void)loadHTMLString:(NSString *)HTMLString;

- (void)reloadWebView;

- (void)initWebView;

- (void)deallocAllResources;

- (void)updateWebviewTitle:(NSString*)title subTitle:(NSString*)subtitle;

@end

@interface UINavigationController(UXWebViewController)
- (UXWebViewController *)rootWebViewController;
@end

