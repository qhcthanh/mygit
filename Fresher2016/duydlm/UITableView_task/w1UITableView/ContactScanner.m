//
//  ContactScanner.m
//  w1UITableView
//
//  Created by manhduydl on 5/18/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "ContactScanner.h"
#import "Contact.h"
#import "Contants.h"

@interface ContactScanner()

@property dispatch_queue_t loadContactSerialQueue;
@property BOOL isIOS9;

@end

@implementation ContactScanner

+ (instancetype)sharedInstance {
    static ContactScanner *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ContactScanner alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    if ([super init]) {
        self.loadContactSerialQueue = dispatch_queue_create("com.duydlm.loadContactSerialQueue", NULL);
        self.isIOS9 = IS_OS_9_OR_LATER;
    }
    return self;
}

#pragma mark -

- (void)scanAllContactWithCompletionHandler:(completedLoadContactHandler)completionBlock andDispatchTo:(dispatch_queue_t)queue {
    queue = queue ? queue : dispatch_get_main_queue();
    dispatch_async(self.loadContactSerialQueue, ^{
        __block NSMutableArray *contactArray = [NSMutableArray new];
        MDAccessContactPermition permision = [self checkPermision];
        if (self.isIOS9) {
            
            if (permision == MDAccessContactPermitionDenied) {
                // denied
                dispatch_async(queue, ^{
                    NSError *error = [[NSError alloc] initWithDomain:@"com.vng.duydlm" code:MDAccessContactPermitionDenied userInfo:nil];
                    completionBlock ? completionBlock(nil, error) : nil;
                });
            } else {
                if (permision == MDAccessContactPermitionGranted) {
                    //Authorized
                    contactArray = [self getAllContactUseContactFramework];
                    dispatch_async(queue, ^{
                        completionBlock ? completionBlock(contactArray, nil) : nil;
                    });
                } else {
                    //Not determined, request permision
                    [self requestPermisionWithCompletion:^(BOOL granted, NSError *error) {
                        if(granted) {
                            //Just authorized
                            contactArray = [self getAllContactUseContactFramework];
                            dispatch_async(queue, ^{
                                completionBlock ? completionBlock(contactArray, nil) : nil;
                            });
                        } else {
                            //Just denied
                            dispatch_async(queue, ^{
                                NSError *error = [[NSError alloc] initWithDomain:@"com.vng.duydlm" code:MDAccessContactPermitionDenied userInfo:nil];
                                completionBlock ? completionBlock(nil, error) : nil;
                            });
                        }
                    }];
                }
            }
            
        } else { //ios 8 or older
            
            if (permision == MDAccessContactPermitionDenied) {
                // denied
                dispatch_async(queue, ^{
                    NSError *error = [[NSError alloc] initWithDomain:@"com.vng.duydlm" code:MDAccessContactPermitionDenied userInfo:nil];
                    completionBlock ? completionBlock(nil, error) : nil;
                });
            } else {
                if (permision == MDAccessContactPermitionGranted) {
                    //Authorized
                    contactArray = [self getAllContactUseAddressFramework];
                    dispatch_async(queue, ^{
                        completionBlock ? completionBlock(contactArray, nil) : nil;
                    });
                } else {
                    //Not determined, request permision
                    [self requestPermisionWithCompletion:^(BOOL granted, NSError *error) {
                        if (granted) {
                            //Just authorized
                            contactArray = [self getAllContactUseAddressFramework];
                            dispatch_async(queue, ^{
                                completionBlock ? completionBlock(contactArray, nil) : nil;
                            });
                        } else {
                            //Just denied
                            dispatch_async(queue, ^{
                                NSError *error = [[NSError alloc] initWithDomain:@"com.vng.duydlm" code:MDAccessContactPermitionDenied userInfo:nil];
                                completionBlock ? completionBlock(nil, error) : nil;
                            });
                        }
                    }];
                }
            }
        }
    });
    
}

- (MDAccessContactPermition)checkPermision {
    if (self.isIOS9) {
        CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
        if (status == CNAuthorizationStatusDenied || status == CNAuthorizationStatusRestricted) {
            // denied
            return MDAccessContactPermitionDenied;
        } else {
            if (status == CNAuthorizationStatusAuthorized) {
                //Authorized
                return MDAccessContactPermitionGranted;
            } else {
                //Not determined
                return MDAccessContactPermitionUnknown;
            }
        }
    } else {
        ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
        if (status == kABAuthorizationStatusDenied || status == kABAuthorizationStatusRestricted) {
            // denied
            return MDAccessContactPermitionDenied;
        } else {
            if (status == kABAuthorizationStatusAuthorized) {
                //Authorized
                return MDAccessContactPermitionGranted;
            } else {
                //Not determined, request permision
                return MDAccessContactPermitionUnknown;
            }
        }
    }
}

#pragma mark - Use Contact Framework

- (NSMutableArray *)getAllContactUseContactFramework {
    __block NSMutableArray *array = [NSMutableArray new];
    if([CNContactStore class]) {
        NSError* contactError;
        CNContactStore* addressBook = [CNContactStore new];
        
        [addressBook containersMatchingPredicate:[CNContainer predicateForContainersWithIdentifiers: @[addressBook.defaultContainerIdentifier]] error:&contactError];
        NSArray * keysToFetch = @[CNContactFamilyNameKey,
                                  CNContactGivenNameKey,
                                  CNContactIdentifierKey,
                                  CNContactImageDataAvailableKey,
                                  CNContactThumbnailImageDataKey,
                                  [CNContactFormatter descriptorForRequiredKeysForStyle:CNContactFormatterStyleFullName]];
        
        CNContactFetchRequest * request = [[CNContactFetchRequest alloc]initWithKeysToFetch:keysToFetch];
        request.sortOrder = CNContactSortOrderGivenName;
        [addressBook enumerateContactsWithFetchRequest:request
                                                 error:&contactError
                                            usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop) {
                                                UIImage *contactImage;
                                                if (contact.imageDataAvailable) {
                                                    contactImage = [UIImage imageWithData:contact.thumbnailImageData];
                                                }
                                                [array addObject:[[Contact alloc] initContactWithGivenname:contact.givenName FamilyName:contact.familyName FullName:[CNContactFormatter stringFromContact:contact style:CNContactFormatterStyleFullName]  Image:contactImage andIdentifier:contact.identifier]];
                                                
                                            }];
    }
    return array;
}

#pragma mark - Use Address framework

- (NSMutableArray *)getAllContactUseAddressFramework {
    NSMutableArray *array = [NSMutableArray new];
    CFErrorRef *error = NULL;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex numberOfPeople = ABAddressBookGetPersonCount(addressBook);
    UIImage *contactImage;
    Contact *contact;
    NSString *fullName;
    
    for(int i = 0; i < numberOfPeople; i++) {
        
        ABRecordRef person = CFArrayGetValueAtIndex( allPeople, i );
        
        NSString *identifier = [NSString stringWithFormat:@"%i", ABRecordGetRecordID(person)];
        NSString *givenName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonFirstNameProperty));
        NSString *familyName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonLastNameProperty));
        
        ABPersonCompositeNameFormat nameOrder = ABPersonGetCompositeNameFormatForRecord(person);
        switch (nameOrder) {
            case kABPersonCompositeNameFormatFirstNameFirst:
                //newContact.isGivenNameFirst = YES;
                fullName = [NSString stringWithFormat:@"%@ %@", givenName, familyName];
                break;
            case kABPersonCompositeNameFormatLastNameFirst:
                //newContact.isGivenNameFirst = NO;
                fullName = [NSString stringWithFormat:@"%@ %@", familyName, givenName];
                break;
            default:
                //newContact.isGivenNameFirst = YES;
                fullName = [NSString stringWithFormat:@"%@ %@", givenName, familyName];
                break;
        }
        
        NSData *contactImageData = (__bridge NSData *)ABPersonCopyImageDataWithFormat(person, kABPersonImageFormatThumbnail);
        contactImage = [UIImage imageWithData:contactImageData];
        
        contact = [[Contact alloc] initContactWithGivenname:givenName FamilyName:familyName FullName:fullName Image:contactImage andIdentifier:identifier];
        [array addObject:contact];
    }
    return array;
}

#pragma mark - request permision

- (void)requestPermisionWithCompletion:(completedRequestPermisionHandler)completionBlock {
    if (IS_OS_9_OR_LATER) {
        
        CNEntityType entityType = CNEntityTypeContacts;
        CNContactStore * contactStore = [[CNContactStore alloc] init];
        [contactStore requestAccessForEntityType:entityType
                               completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                   completionBlock ? completionBlock(granted, error) : nil;
                               }];
    } else {
        
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error)
                                                 {
                                                     completionBlock ? completionBlock(granted, (__bridge NSError *)error) : nil;
                                                 });
    }
}

@end
