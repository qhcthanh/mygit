/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "InterAppViewController.h"

#import <UXFramework/UXFramework.h>

@interface InterAppViewController ()<UIDocumentInteractionControllerDelegate>

@property (nonatomic, retain) UXTableViewDataSource* model;
@property (nonatomic, retain) UXTableViewAction* actions;
@property (nonatomic, copy) NSURL* fileUrl;
@property (nonatomic, retain) UIDocumentInteractionController* docController;

@end

@implementation InterAppViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.dataSource = self.model;
    self.tableView.delegate = [self.actions forwardingTo:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)cleanupDocController {
    [[NSFileManager defaultManager] removeItemAtURL:self.fileUrl error:nil];
    self.fileUrl = nil;
    self.docController = nil;
}

// This method is the short form of adding a cell object with an action.
- (id)objectWithAction:(BOOL (^)())action title:(NSString *)title subtitle:(NSString *)subtitle {
    return [self.actions attachToObject:[UXSubtitleTableViewCellObject objectWithTitle:title subtitle:subtitle]
                               tapBlock:
            ^BOOL(id object, UIViewController *controller, NSIndexPath* indexPath) {
                if (!action()) {
//                    [UXAlertView showAlertWithTitle:@"We've givin' her all she's got, cap'n!" message:@"The app you tried to open does not appear to be installed on this device." cancelButton:@"Cancel" okButton:@"OK" buttonHandler:^(UXAlertView *alert, NSInteger buttonIndex) {
//                        
//                    }];
                }
                return YES;
            }];
}

// Prepares an Instagram image for sharing and then uses the UIDocumentInteractionController
// to show a sharing controller.
- (BOOL)openInstagramImage {
    [self cleanupDocController];
    
    NSError* error = nil;
    self.fileUrl = [UXInterApp urlForInstagramImageAtFilePath:UXPathForBundleResource(nil, @"dilly.jpg")
                                                        error:&error];
    if (nil != self.fileUrl) {
        self.docController = [UIDocumentInteractionController interactionControllerWithURL:_fileUrl];
        self.docController.delegate = self;
        
        [self.docController presentOpenInMenuFromRect:CGRectZero
                                               inView:self.view
                                             animated:YES];
        return YES;
    }
    
    return NO;
}

- (id)initWithStyle:(UITableViewStyle)style {
    if ((self = [super initWithStyle:UITableViewStyleGrouped])) {
        self.titleString = @"All Actions";
        
        _actions = [[UXTableViewAction alloc] initWithTarget:self];
        
        NSArray* sectionedObjects =
        [NSArray arrayWithObjects:
         
         @"Safari",
         [self objectWithAction:^{return [UXInterApp safariWithURL:[NSURL URLWithString:@"http://zaloapp.com"]];}
                          title:@"Open a URL in Safari" subtitle:@"http://zaloapp.com"],
         
         @"Maps",
         [self objectWithAction:^{return [UXInterApp googleMapAtLocation:CLLocationCoordinate2DMake(37.37165, -121.97877)];}
                          title:@"Open a Lat/Long" subtitle:@"Trampoline dodgeball!"],
         [self objectWithAction:^{return [UXInterApp googleMapAtLocation:CLLocationCoordinate2DMake(37.37165, -121.97877)
                                                                   title:@"Trampoline Dodgeball"];}
                          title:@"Open a Lat/Long with a title" subtitle:nil],
         [self objectWithAction:^{return [UXInterApp googleMapDirectionsFromLocation:CLLocationCoordinate2DMake(37.6139, -122.4871)
                                                                          toLocation:CLLocationCoordinate2DMake(36.6039, -121.9116)];}
                          title:@"Directions" subtitle:nil],
         [self objectWithAction:^{return [UXInterApp googleMapWithQuery:@"Boudin Bakeries"];}
                          title:@"Search for Boudin Bakeries" subtitle:nil],
         
         @"Phone",
         [self objectWithAction:^{return [UXInterApp phone];} title:@"Open the app" subtitle:nil],
         [self objectWithAction:^{return [UXInterApp phoneWithNumber:@"123-500-7890"];} title:@"Call 123-500-7890" subtitle:nil],
         
         @"SMS",
         [self objectWithAction:^{return [UXInterApp sms];} title:@"Open the app" subtitle:nil],
         [self objectWithAction:^{return [UXInterApp smsWithNumber:@"123-500-7890"];} title:@"SMS 123-500-7890" subtitle:nil],
         
         @"Mail",
         [self objectWithAction:^{UXMailAppInvocation* invocation = [UXMailAppInvocation invocation];
            invocation.recipient = @"helloworld@gmail.com";
            return [UXInterApp mailWithInvocation:invocation];} title:@"Mail to helloworld@gmail.com" subtitle:nil],
         [self objectWithAction:^{UXMailAppInvocation* invocation = [UXMailAppInvocation invocation];
            invocation.recipient = @"helloworld@gmail.com";
            invocation.subject = @"Framework made me do it!";
            return [UXInterApp mailWithInvocation:invocation];} title:@"Mail with a subject" subtitle:nil],
         [self objectWithAction:^{UXMailAppInvocation* invocation = [UXMailAppInvocation invocation];
            invocation.recipient = @"helloworld@gmail.com";
            invocation.subject = @"Framework made me do it!";
            invocation.bcc = @"jverkoey+bcc@gmail.com";
            invocation.cc = @"jverkoey+cc@gmail.com";
            invocation.body = @"This will be an awesome email.";
            return [UXInterApp mailWithInvocation:invocation];} title:@"Mail with all details" subtitle:nil],
         
         @"YouTube",
         [self objectWithAction:^{return [UXInterApp youTubeWithVideoId:@"fzzjgBAaWZw"];} title:@"Ninja cat video" subtitle:nil],
         
         @"iBooks",
         [self objectWithAction:^{return [UXInterApp iBooks];} title:@"Open the app" subtitle:nil],
         
         @"App Store",
         [self objectWithAction:^{return [UXInterApp appStoreWithAppId:@"364709193"];} title:@"Buy the iBooks app" subtitle:nil],
         
         @"Facebook",
         [self objectWithAction:^{return [UXInterApp facebook];} title:@"Open the app" subtitle:nil],
         [self objectWithAction:^{return [UXInterApp facebookProfileWithId:@"122605446"];} title:@"Jeff's profile page" subtitle:nil],
         
         @"Twitter",
         [self objectWithAction:^{return [UXInterApp twitter];} title:@"Open the app" subtitle:nil],
         [self objectWithAction:^{return [UXInterApp twitterWithMessage:@"I'm playing with the Framework sample apps! http://Frameworkkit.info"];} title:@"Post a tweet" subtitle:nil],
         [self objectWithAction:^{return [UXInterApp twitterProfileForUsername:@"featherless"];} title:@"Open featherless' profile" subtitle:nil],
         
         @"Instagram",
         [self objectWithAction:^{return [UXInterApp instagram];} title:@"Open the app" subtitle:nil],
         [self objectWithAction:^{return [UXInterApp instagramCamera];} title:@"Camera" subtitle:nil],
         [self objectWithAction:^{return [UXInterApp instagramProfileForUsername:@"featherless"];} title:@"Open featherless' profile" subtitle:nil],
         [self objectWithAction:^{return [self openInstagramImage];} title:@"Open local image in Instagram" subtitle:nil],
         
         @"Custom Application",
         [self objectWithAction:^{return [UXInterApp applicationWithScheme:@"RAWR:"];} title:@"Open custom app (RAWR:)" subtitle:nil],
         [self objectWithAction:^{return [UXInterApp applicationWithScheme:@"RAWR:"
                                                             andAppStoreId:@"000000000"];} title:@"Custom app or AppStore" subtitle:nil],
         [self objectWithAction:^{return [UXInterApp applicationWithScheme:@"RAWR:"
                                                                   andPath:@"//friends/blah"];} title:@"Custom app with url" subtitle:nil],
         [self objectWithAction:^{return [UXInterApp applicationWithScheme:@"RAWR:"
                                                                appStoreId:@"000000000" 
                                                                   andPath:@"//friends/blah"];} title:@"Custom app with url or AppStore" subtitle:nil],
         
         nil];
        
        _model = [[UXTableViewDataSource alloc] initWithSectionedArray:sectionedObjects
                                                         delegate:(id)[UXTableViewCellFactory class]];
    }
    return self;
}

#pragma mark - UIDocumentInteractionControllerDelegate

- (void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller {
    [self cleanupDocController];
}

@end
