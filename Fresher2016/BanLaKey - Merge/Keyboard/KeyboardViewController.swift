//
//  KeyboardViewController.swift
//  Keyboard
//
//  Created by qhcthanh on 8/30/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import UIKit


internal enum eventType {
    case eventTypeBackspace
    case eventTypeCharacter
    case eventTypeSpace
}

var heightSearchGiphyTopBar: CGFloat  {
    get {
        return UIScreen.mainScreen().interfaceOrientation() == .Portrait ? 8 : 3
    }
}

let userDefaults = NSUserDefaults()
let kCurrentKeyboardHaveSuggesstionViewKey = "kCurrentKeyboardHaveSuggesstionViewKey"

class KeyboardViewController: UIInputViewController {
    
    // MARK: UI Properties
    
    var keyboard: BLKeyboard?
    var topExtraView: BLTopExtraView!
    var emojiView: BLEmojiView!
    var suggesstionView: BLSuggestionView!
    var giphyView: BLGiphyView!
    var searchView: BLTextField!
    
    // MARK: Configure Properties
    
    var firstRun: Bool = true
    var searchViewIsShow = false
    var onceToken: dispatch_once_t = 0
    let heightKeyboardPortrait: CGFloat = 216
    let heightKeyboardLandscape: CGFloat = 160
    var heightReduceKeyboardGiphyView: CGFloat {
        get {
            return UIScreen.mainScreen().interfaceOrientation() == .Portrait ? 50 : 35
        }
    }
    
    static weak var currentInstance: KeyboardViewController?
    var suggestionManager = SuggestionManager.shareIntance
    var textInSession = ""
    var settingsView: DefaultSettings?
    var bottomEmojiViewContraints: NSLayoutConstraint!
    
    var bottomGiphyViewContraints: NSLayoutConstraint!
    var heightGiphyViewContraints: NSLayoutConstraint!
    
    var bottomKeyboardViewContraints: NSLayoutConstraint!
    var heightKeyboardViewContraints: NSLayoutConstraint!
    
    // MARK: BLKeyboard's Properties
    
     var isHaveSuggesstionView = userDefaults.boolForKey(kCurrentKeyboardHaveSuggesstionViewKey)
    
    var widthKeyboard: CGFloat {
        get {
            return UIScreen.mainScreen().bounds.width
        }
    }
    
    /// Get current height of keyboard. Height depend on current Orizontal
    var  heightKeyboard: CGFloat {
        get {
            return  UIScreen.mainScreen().interfaceOrientation() == .Portrait ? heightKeyboardPortrait : heightKeyboardLandscape
        }
    }
    
    /// Get current Height of extra top bar view. Height depend on current Orizontal
    var heightTopExtraView: CGFloat {
        get {
            return  UIScreen.mainScreen().interfaceOrientation() == .Portrait ? 35 : 30
        } set {
            
        }
    }
    
    var heightTopSuggesstionView: CGFloat {
        get {
            return  UIScreen.mainScreen().interfaceOrientation() == .Portrait ? 35 : 30
        } set {
            
        }
    }
    
    // Total height of keyboard extension
    var heightConstraint: NSLayoutConstraint?
    
    /// Total height of keyboard extension. when set this will call update contrainst of Keyboard view
    var keyboardContrainstHeight: CGFloat {
        get {
            if let constraint = self.heightConstraint {
                return constraint.constant
            }
            else {
                return 0
            }
        }
        set {
            self.setHeight(newValue)
        }
    }
    
    // Height of top extraview constraint
    var heightTopBarConstraint: NSLayoutConstraint!
    var topExtraViewContrainstHeight: CGFloat {
        get {
            if let constraint = self.heightTopBarConstraint {
                return constraint.constant
            }
            else {
                return 0
            }
        } set {
            heightTopBarConstraint.constant = newValue
        }
    }
    
    // Height of top extraview constraint
    var heightSuggestionBarViewConstraint: NSLayoutConstraint!
    var heightSuggestionBarViewContrainstHeight: CGFloat {
        get {
            if let constraint = self.heightSuggestionBarViewConstraint {
                return constraint.constant
            }
            else {
                return 0
            }
        } set {
            heightSuggestionBarViewConstraint.constant = newValue
        }
    }
    
    // MARK: NVDVietnamesTyping
    var typingType: TypingType = TypingType.Telex
    
    // MARK: BLKeyboardDelegate's Properties
    var curRawWord = ""
    var curWord = ""
    
//     Last height will cache a last total height of keyboard the user current User.
//     Disscuss: In giphy keyboard. The keyboard height must be Keyboard + Top suggesst + topExtra height.
//     When user hide GiphyView. We will update the last height user use
    var lastHeight: CGFloat!
    
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        BLThemeManager.currentTheme.generateToJSONData()
        
        // Copy db
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        if (!fileManager.fileExistsAtPath(databasePath)) {
            if let bundlePath = NSBundle.mainBundle().pathForResource("Dictionary", ofType: "db") {
                do {
                    try fileManager.copyItemAtPath(bundlePath, toPath: databasePath)
                } catch {
                }
            }
        }
        
        
        
        // Perform custom UI setup here
        if !BLThemeManager.currentTheme.backgroundImage.isEmpty {
            let backgroundImage = UIImage(named: BLThemeManager.currentTheme.backgroundImage)
            self.view.layer.contents = backgroundImage?.CGImage
        }
        else {
            self.view.backgroundColor = BLThemeManager.currentTheme.backgroundColor
        }
        self.view.userInteractionEnabled = true
        KeyboardViewController.currentInstance = self
        
        // Setup Top ExtraView
        self.setupTopExtraView()
       self.setupSuggesstionView()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // self.keyboardContrainstHeight - heightKeyboard is delta of totoal height keyboard view and height keyboard.
        // Delta of them is origin.y of keyboard
        var currentKeyboardFrame = CGRectMake(0, abs(self.keyboardContrainstHeight - heightKeyboard) + 2, self.view.frame.width, heightKeyboard - 2)
        
        // Run the second time to update frame and constraints if need
        if (self.firstRun == false) {
            
            // If current option is GiphyKeyboard. We need sub heightSearchGiphyTopBar to keyboardFrame
            if self.topExtraView.currentOption == .GiphyKeyboard {
                currentKeyboardFrame.origin.y += heightSearchGiphyTopBar
                currentKeyboardFrame.size.height -= heightSearchGiphyTopBar
            }
            
            UIView.animateWithDuration(0.0, animations: {
                if self.topExtraView.currentOption != .GiphyKeyboard {
                    self.keyboard?.updateState(currentKeyboardFrame)
                } else {
                    
                    let frameUpdateInGiphyView = self.keyboard!.frame.width != self.view.frame.width ?
                        CGRectMake(0, currentKeyboardFrame.origin.y + self.heightReduceKeyboardGiphyView  , self.view.frame.width,currentKeyboardFrame.size.height - self.heightReduceKeyboardGiphyView) :
                        self.keyboard?.frame
                    
                    self.keyboard?.updateState(frameUpdateInGiphyView!)
                }
                
                // Just update when current keyboard option is emojikeyboard
                if let emojiView = self.emojiView, let topExtraView = self.topExtraView
                    where topExtraView.currentOption == .EmojiKeyboard {
                    emojiView.frame = self.keyboard!.frame
                }
                
                if let giphyView = self.giphyView {
                    
                    let lastHeight = giphyView.frame.size.height
                
                    // Update giphyView width and origin x. Because when rotate. Y and height emoji isn't correct in old orientation
                    
                    if giphyView.frame.size.width != self.widthKeyboard {
                        giphyView.frame.origin.y = 0
                        giphyView.frame.size.height = self.keyboardContrainstHeight
                    }
                    
                    giphyView.frame.size.width = self.widthKeyboard
                    
                    if lastHeight != giphyView.frame.size.height {
                        giphyView.giphyCollectionView.reloadData()
                    }
                
                    
                }
                
                // Update emojiView width and origin x. Because when rotate. X and width emoji isn't correct in old orientation
                if let emojiView = self.emojiView {
                    emojiView.frame.size.width = self.widthKeyboard
                    //emojiView.frame.origin.x = self.emojiView.frame.origin.x == 0 ? 0 : self.widthKeyboard
                }
                
                
                // Check current orientation update to topExtraView SuggestionBar.
                // When update complete need to reloadData
                if let topExtraView = self.topExtraView {
                    
                    let lastNumberItem = topExtraView.numberOfItemInSuggestion
                    let currentNumberOfItem = UIScreen.mainScreen().interfaceOrientation() == .Portrait ? 3 : 5
                     
                    if lastNumberItem != currentNumberOfItem {
                        self.topExtraView.numberOfItemInSuggestion = currentNumberOfItem
                        self.topExtraViewContrainstHeight = self.heightTopExtraView
                    }
                }
                
                // Update constrainst when orientation change
                if let _ = self.suggesstionView {
                    self.heightSuggestionBarViewContrainstHeight = self.heightTopSuggesstionView
                }
                
            })
        }
        
        // Run first time when launch to init keyboard
        dispatch_once(&onceToken, {
            self.firstRun = false;
            weak var weakSelf = self
            
            //self.keyboard = BLKeyboard(frame: currentKeyboardFrame, inputViewController: weakSelf!)
            self.keyboard = defaultKeyboard(currentKeyboardFrame, inputViewController: weakSelf!)
            self.view.addSubview(self.keyboard!)
            self.keyboard?.delegate = self
        })
        
        
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        // When device change orientation. The height of keyboard will change to.
        // KeyboardContrainstHeight will update by heightKeyboard and heightTopExtraView
        // heightKeyboard and heightTopExtraView will get height comfortable with current orientation
        self.keyboardContrainstHeight = heightKeyboard + heightTopExtraView + (self.topExtraView.isShowSuggesstionEmoji ? heightTopSuggesstionView : 0)
        
        if self.giphyView != nil && self.heightGiphyViewContraints != nil {
            self.heightGiphyViewContraints.constant = self.keyboardContrainstHeight
            //self.keyboard!.frame.size.height = self.heightKeyboard - (heightSearchGiphyTopBar + 20)
        }
        
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        // Add custom view sizing constraints here
        if let emojiView = self.emojiView {
            emojiView.setupConstraints()
        }
        
        if let giphyView = self.giphyView {
            giphyView.updateConstraintsIfNeeded()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Set height of keyboard when keyboard will appear
        // Height keyboard default is keyboard + top extra + top suggestion
        self.keyboardContrainstHeight = heightKeyboard + heightTopExtraView + heightTopSuggesstionView
        didTouchOnHideSuggestion(self.topExtraView, isShowUp: self.isHaveSuggesstionView)
        
        //self.setupEmojiView()
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        suggestionManager.updateDatabase(dispatch_get_main_queue()) { (error) in
            print("update db")
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        BLGiphyRequestManager.shareManager().cancelAllTask()
        YLImageDownloader.sharedDefaultImageDownloader().cancelAllDataTask()
        
        // Remove all view in keyboard viewcontroller
        if self.giphyView != nil {
            self.giphyView.giphyCollectionView.removeCollectionView()
        }
        
        self.keyboard?.removeFromSuperview()
        self.emojiView?.removeFromSuperview()
        self.giphyView?.removeFromSuperview()
        self.topExtraView?.removeFromSuperview()
        self.suggesstionView?.removeFromSuperview()
        
        self.keyboard = nil
        self.emojiView = nil
        self.giphyView = nil
        self.topExtraView = nil
        self.suggesstionView = nil
        
        // Remove word
        curWord = ""
        curRawWord = ""
        textInSession = ""
        
        // Remove all cache and manager data
        BLGIFCache.shareManager().removeAllDataInCache()
        BLDisplayGifManager.defaultManager.deleteAllImageView()
        BLGiphyManager.shareManager().removeAllDataCache()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated
    }
    
    // MARK: Private Method's
    
    /**
     Set height contrainst of total keyboard view. 
     If height contrainst not exits this will create and assgin to heightConstraint properties.
     If exists just update constrainst
     
     - parameter height: The new height contrainst to set keyboard
     */
    private func setHeight(height: CGFloat) {
        
        if self.heightConstraint == nil {
            self.heightConstraint = NSLayoutConstraint(
                item:self.view,
                attribute:NSLayoutAttribute.Height,
                relatedBy:NSLayoutRelation.Equal,
                toItem:nil,
                attribute:NSLayoutAttribute.NotAnAttribute,
                multiplier:0,
                constant:height)
            self.heightConstraint!.priority = 999
            
            self.view.addConstraint(self.heightConstraint!)
        }
        else {
            self.heightConstraint?.constant = height
        }
    }
    
    // MARK : InputTexViewController method's
    
    override func textWillChange(textInput: UITextInput?) {
        super.textWillChange(textInput)
        
    }
    
    func shouldChangeCharactersInLabel(word: String, type: eventType) {
        
    }
    
    override func textDidChange(textInput: UITextInput?) {
        super.textDidChange(textInput)
        
    }
    
    // MARK: Setup View
    
    private func setupTopExtraView() {
        
        if self.topExtraView == nil {
            // Setup Top ExtraView
            self.topExtraView = BLTopExtraView()
            self.topExtraView.translatesAutoresizingMaskIntoConstraints = false
            self.topExtraView.delegate = self
            
            self.view.addSubview(self.topExtraView)
            
            // Setup topExtraView Contrainst
            self.topExtraView.constrain(.Left, to: .Left, of: self.view)
            self.topExtraView.constrain(.Top, to: .Top, of: self.view)
            self.topExtraView.constrain(.Right, to: .Right, of: self.view)
            self.heightTopBarConstraint = self.topExtraView.constrain(.Height, to: heightTopExtraView)
            
        }
    }
    
    private func setupSuggesstionView() {
        
        if self.suggesstionView == nil {
            self.suggesstionView = BLSuggestionView()
            self.view.addSubview(self.suggesstionView)
            
            self.suggesstionView.delegate = self
            self.suggesstionView.dataSource = self
            
            self.suggesstionView.constrain(.Top, to: .Bottom, of: self.topExtraView, offsetBy: 2)
            self.heightSuggestionBarViewConstraint = self.suggesstionView.constrain(.Height, to: heightTopSuggesstionView - 2)
            self.suggesstionView.constrain(.Right, to: .Right, of: self.view, offsetBy: 2)
            self.suggesstionView.constrain(.Left, to: .Left, of: self.view, offsetBy: 2)
            self.suggesstionView.backgroundColor = .clearColor()
        }
    }
    
    private func setupEmojiView() {
        
        if self.emojiView == nil {
            let yEmojiView = self.keyboard == nil ? self.heightTopExtraView + self.heightTopSuggesstionView : self.keyboard!.frame.origin.y
            
            self.emojiView = BLEmojiView()
            
            self.emojiView.backgroundColor = BLThemeManager.currentTheme.backgroundColor
            self.emojiView.layer.contents = self.view.layer.contents
            
            self.view.addSubview(self.emojiView)
            
            self.emojiView.constrain(.Left, to: .Left, of: self.view)
            self.emojiView.constrain(.Right, to: .Right, of: self.view)
            self.emojiView.constrain(.Height,to: .Height, of: self.keyboard!)
            self.bottomEmojiViewContraints = self.emojiView.constrain(.Bottom, to: .Bottom, of: self.view, offsetBy: self.heightKeyboard)
            
            self.emojiView.delegate = self
        }
    }
    
    private func setupGiphyView() {
        
        if self.giphyView == nil {
            //  yPosition is bottom of self.heightTopExtraView
        
            self.giphyView = BLGiphyView(frame: CGRectMake(0, self.keyboardContrainstHeight,self.widthKeyboard, self.keyboardContrainstHeight))
            self.giphyView.layer.contents = self.view.layer.contents
            self.giphyView.delegate = self
            
            self.view.addSubview(self.giphyView)
            
//            self.giphyView.constrain(.Left, to: .Left, of: self.view)
//            self.giphyView.constrain(.Right, to: .Right, of: self.view)
//            self.heightGiphyViewContraints = self.giphyView.constrain(.Height,to: self.keyboardContrainstHeight)
//            self.bottomGiphyViewContraints = self.giphyView.constrain(.Bottom, to: .Bottom, of: self.view, offsetBy: self.keyboardContrainstHeight)
            
            // If suggestion emoji is hidden . Show it's and disable button
            
        }
        
        if self.topExtraView.isShowSuggesstionEmoji == false {
            self.topExtraView.didSelectHideSuggestion(UIButton())
        }
    }
    
    func toggleSettings() {
        // lazy load settings
        if self.settingsView == nil {
            if let aSettings = self.createSettings() {
                aSettings.darkMode = self.darkMode()
                
                aSettings.hidden = true
                self.view.addSubview(aSettings)
                self.settingsView = aSettings
                
                aSettings.translatesAutoresizingMaskIntoConstraints = false
                
                let widthConstraint = NSLayoutConstraint(item: aSettings, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Width, multiplier: 1, constant: 0)
                let heightConstraint = NSLayoutConstraint(item: aSettings, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Height, multiplier: 1, constant: 0)
                let centerXConstraint = NSLayoutConstraint(item: aSettings, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0)
                let centerYConstraint = NSLayoutConstraint(item: aSettings, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0)
                
                self.view.addConstraint(widthConstraint)
                self.view.addConstraint(heightConstraint)
                self.view.addConstraint(centerXConstraint)
                self.view.addConstraint(centerYConstraint)
            }
        }
        
        if let settings = self.settingsView {
            let hidden = settings.hidden
            settings.hidden = !hidden
//            self.forwardingView.hidden = hidden
//            self.forwardingView.userInteractionEnabled = !hidden
//            self.bannerView?.hidden = hidden
        }
    }
    
    // a settings view that replaces the keyboard when the settings button is pressed
    func createSettings() -> DefaultSettings? {
        // note that dark mode is not yet valid here, so we just put false for clarity
        let settingsView = DefaultSettings(darkMode: false)
        settingsView.backButton?.addTarget(self, action: #selector(KeyboardViewController.toggleSettings), forControlEvents: UIControlEvents.TouchUpInside)
        return settingsView
    }
    
    // only available after frame becomes non-zero
    func darkMode() -> Bool {
        let darkMode = { () -> Bool in
            let proxy = self.textDocumentProxy
            return proxy.keyboardAppearance == UIKeyboardAppearance.Dark
        }()
        
        return darkMode
    }
}

// MARK: BLTopExtraViewDelegate
extension KeyboardViewController: BLTopExtraViewDelegate {
    
    func didTouchOnSuggestion(topView: BLTopExtraView, suggestion: AnyObject) {
        print(suggestion)
        
        let text = suggestion as! String
        
        if (curWord != "") {
            for _ in (0..<curWord.characters.count) {
                self.textDocumentProxy.deleteBackward()
            }
        }
        
        self.textDocumentProxy.insertText(text)
        textInSession += text
        if textInSession.length < Constants.maxLenghtOfMess {
            suggestionManager.updateTree(withInput: self.textDocumentProxy.documentContextBeforeInput!)
        }
        self.textDocumentProxy.insertText(" ")
        curWord = ""
        curRawWord = ""
        
        suggestionManager.suggestWithInput(self.textDocumentProxy.documentContextBeforeInput!, numberOfSuggestion: 3, dispatchQueue: dispatch_get_main_queue()) { (result, error) in
            self.topExtraView.suggesstionView.setArraySuggest(NSMutableArray(array: result))
        }
        
        suggestionManager.suggestEmojiWithInput(self.textDocumentProxy.documentContextBeforeInput!, dispatchQueue: dispatch_get_main_queue()) { (result, error) in
            self.suggesstionView.setArraySuggest(NSMutableArray(array: result))
        }
    }
    
    func didTouchOnEmoji(topView: BLTopExtraView) {
        
        topView.emojiButton.enabled = false
        
        // Remove giphykeyboard
        if topView.currentOption == .GiphyKeyboard {
            didTouchOnGiphy(topView)
        }
        
        if self.emojiView == nil {
            self.setupEmojiView()
            self.view.layoutIfNeeded()
        } else {
            self.view.bringSubviewToFront(self.emojiView)
        }
        
        let changeActionEmoji: (optionChanged: BLKeyboardOption, yPositionChanged: CGFloat) = topView.currentOption == .NormalKeyboard ? (.EmojiKeyboard, 0) : (.NormalKeyboard, self.heightKeyboard)
        topView.currentOption = changeActionEmoji.optionChanged
        
        if let bottomEmojiViewContraints = self.bottomEmojiViewContraints {
            bottomEmojiViewContraints.constant = changeActionEmoji.yPositionChanged
        }
        
        UIView.animateWithDuration(0.3, animations: {
            
            self.view.layoutIfNeeded()
            }, completion: {
                _ in
                if changeActionEmoji.optionChanged == .NormalKeyboard {
                    self.emojiView.clipsToBounds = true
                } else {
                    self.emojiView.clipsToBounds = false
                }
                topView.emojiButton.enabled = true
        })
    }
    
    func didTouchOnGiphy(topView: BLTopExtraView) {
        
        // Remove EmojiKeyboard
        topView.giphyButton.enabled = false
        
        if topView.currentOption == .EmojiKeyboard {
            didTouchOnEmoji(topView)
        }
    
        if self.giphyView == nil {
            self.setupGiphyView()
            self.view.layoutIfNeeded()
        }
        
        let changeActionGiphy: (optionChanged: BLKeyboardOption, yPositionChanged: CGFloat) = topView.currentOption == .NormalKeyboard ? (.GiphyKeyboard, 0) : (.NormalKeyboard, self.keyboardContrainstHeight)
        topView.currentOption = changeActionGiphy.optionChanged
        
//        if let bottomGiphyViewContraints = self.bottomGiphyViewContraints {
//            bottomGiphyViewContraints.constant = changeActionGiphy.yPositionChanged
//        }
        
        
        UIView.animateWithDuration(0.3, animations: {
            self.giphyView.frame.origin.y = changeActionGiphy.yPositionChanged
            self.giphyView.frame.size.height = self.keyboardContrainstHeight
            }, completion: {
                _ in
                
                if changeActionGiphy.optionChanged == .NormalKeyboard {
                    
                    // Remove all cache and manager when turn off Giphy
                    BLGIFCache.shareManager().removeAllDataInCache()
                    BLDisplayGifManager.defaultManager.deleteAllImageView()
                    BLGiphyManager.shareManager().removeAllDataCache()
                    BLGiphyRequestManager.shareManager().cancelAllTask()
                    YLImageDownloader.sharedDefaultImageDownloader().cancelAllDataTask()
                    
                    // Remove collectionView in giphyView
                    if self.giphyView != nil {
                        self.giphyView.giphyCollectionView.removeCollectionView()
                    }
                    if let topSuggestionView = self.suggesstionView where self.isHaveSuggesstionView {
                        topSuggestionView.hidden = false
                    }
                    
                    self.giphyView.removeFromSuperview()
                    self.giphyView = nil
                    self.keyboard?.hidden = false
                } else {
                    // Set isRemoveSelf flag to false. This will accpect giphyCOllectionView reloadData
                    self.giphyView.giphyCollectionView.isRemoveSelf = false
                    self.giphyView.loadMoreRandomGiphy()
                    self.keyboard?.hidden = true
                }
                
                topView.giphyButton.enabled = true
        })
    }
    
    func didTouchOnSetting(topView: BLTopExtraView) {
        
    }
    
    func didTouchOnHideSuggestion(topView: BLTopExtraView, isShowUp: Bool) {
        
        // If show up height will have heightTopSuggesstionView
        let newHeightKeyboardContranst = isShowUp ? (heightKeyboard + heightTopExtraView + heightTopSuggesstionView) : (heightKeyboard + heightTopExtraView)
        
        // Hidden suggesstionView if showUp is flase
        self.suggesstionView.hidden = !isShowUp
        userDefaults.setBool(isShowUp, forKey: kCurrentKeyboardHaveSuggesstionViewKey)
        self.isHaveSuggesstionView = isShowUp
        topView.isShowSuggesstionEmoji = isShowUp
        
        if self.emojiView != nil {
            self.emojiView.frame.origin.y = newHeightKeyboardContranst - heightKeyboard
        }
        
        self.keyboardContrainstHeight = newHeightKeyboardContranst
    }
    
    func didDoubleTouchOnSetting(topView: BLTopExtraView) {
        self.toggleSettings()
    }
}


// MARK: BLSuggesstionViewDataSource, BLSuggesstionViewDelegate
extension KeyboardViewController: BLSuggesstionViewDataSource, BLSuggesstionViewDelegate {
    
    func numberOfSuggestion(suggesstionView: BLSuggestionView) -> Int {
        return 5
    }
    
    
    func suggesstionView(suggesstionView: BLSuggestionView, viewAtIndex index: Int) -> BLSuggestionViewCell {
        let cell = BLSuggestionViewCell()
        cell.backgroundColor = BLThemeManager.currentTheme.spaceButton
        cell.textLabel.adjustsFontSizeToFitWidth = true
        
        return cell
    }
    
    func suggesstionView(suggesstionView: BLSuggestionView, didSelectAtIndex index: Int) {
        
        if let text = suggesstionView.getSuggesstionData(AtIndex: index) as? String {
            textDocumentProxy.insertText(text)
        }
    }
}

// MARK: BLGiphyViewDelegate
extension KeyboardViewController: BLGiphyViewDelegate {
    
    func didBeginEditingText(textField: UITextField) {
        
        searchView = textField as! BLTextField
        searchViewIsShow = true
        
        if let keyboard = self.keyboard {
            
            if let topSuggestionView = self.suggesstionView {
                topSuggestionView.hidden = true
            }
            
            self.giphyView.bottomBar.hidden = true
            
            self.view.bringSubviewToFront(keyboard)
            self.keyboard!.hidden = false
            self.keyboard!.frame.origin.y = self.keyboardContrainstHeight
            self.keyboard!.frame.size.height = self.heightKeyboard
            
            self.view.layoutIfNeeded()
            
            let collectionViewFlowLayout = self.giphyView.giphyCollectionView.collectionViewLayout as! BLCollectionViewFlowLayout
            collectionViewFlowLayout.numberOfRows = 1
            
            self.giphyView.bottomGiphyCollectionViewContraint.constant =  (self.keyboard!.frame.size.height - self.heightReduceKeyboardGiphyView - self.giphyView.bottomBar.frame.height) * -1
            self.giphyView.giphyCollectionView.reloadData()
            
            UIView.animateWithDuration(0.3, animations: {
                self.giphyView.layoutIfNeeded()
                self.keyboard!.frame.origin.y = self.keyboardContrainstHeight - self.heightKeyboard + self.heightReduceKeyboardGiphyView
                self.keyboard!.frame.size.height -= self.heightReduceKeyboardGiphyView
            })
        }
    }
    
    func didEndEditingText(textField: UITextField) {
        
        if let keyboard = self.keyboard,
            let giphyView = self.giphyView
            where self.topExtraView.currentOption == .GiphyKeyboard
        {
            
            self.view.bringSubviewToFront(keyboard)
            self.keyboard!.hidden = false
            
            self.giphyView.bottomGiphyCollectionViewContraint.constant = -6
            
            UIView.animateWithDuration(0.3, animations: {
                self.keyboard!.frame.origin.y = self.keyboardContrainstHeight - self.heightReduceKeyboardGiphyView
                self.keyboard!.frame.size.height += self.heightReduceKeyboardGiphyView
                self.giphyView.layoutIfNeeded()
                }, completion: {
                    _ in
                    self.view.bringSubviewToFront(giphyView)
                    
                    if let topSuggestionView = self.suggesstionView {
                        topSuggestionView.hidden = false
                        self.view.layoutIfNeeded()
                    }
                    
                    if self.giphyView != nil {
                        self.giphyView.bottomBar.hidden = false
                        
                        let collectionViewFlowLayout = self.giphyView.giphyCollectionView.collectionViewLayout as! BLCollectionViewFlowLayout
                        collectionViewFlowLayout.numberOfRows = 2
                        
                        self.giphyView.giphyCollectionView.reloadData()
                    }
                    
                   // self.keyboard!.hidden = true
            })
        }
    }
    
    func didNeedDismissGiphyView(giphyView: BLGiphyView) {
        searchViewIsShow = false
        didTouchOnGiphy(self.topExtraView)
    }
    
}

// MARK: EmojiViewDelegate
extension KeyboardViewController: BLEmojiViewDelegate {
    
    func didSelectEmoji(emoji: String) {
        textDocumentProxy.insertText(emoji)
    }
    
}

// MARK: BLKeyboardDelegate
extension KeyboardViewController : BLKeyboardDelegate {
    
    func textProxyWillRemoveAll(textDocumentProxy: UITextDocumentProxy) {
        
        if let text = textDocumentProxy.documentContextBeforeInput {
            for _ in (0..<text.characters.count) {
                textDocumentProxy.deleteBackward()
            }
        }
        
        curWord = ""
        curRawWord = ""
    }
    
    //
    func textProxyWillChange(textDocumentProxy: UITextDocumentProxy, withCharacter: String) {
        
        if searchViewIsShow {
            searchView.addCharactor(withCharacter)
            return
        }
        
        PianoNotes.shareInstance().playSound(withCharacter)
        
        if let text = textDocumentProxy.documentContextBeforeInput {
        
            if let lastWord = text.componentsSeparatedByString(" ").last {
                if (lastWord != curWord) {
                    curWord = lastWord
                    curRawWord = lastWord
                }
            } else {
                curWord = ""
                curRawWord = ""
                
            }
        } else {
            curWord = ""
            curRawWord = ""
        }
        
        if withCharacter == " " {
            
            curWord = ""
            curRawWord = ""
            textDocumentProxy.insertText(" ")
        } else {
            curRawWord = curRawWord + withCharacter
            
            var temp: String = ""
            if (curWord.isVietnameseWord()) {
                temp = WordSplitting.init(_rawWord: curRawWord, typingType: typingType).getVietnameseWord() ?? curRawWord
            } else {
                temp = curWord + withCharacter
            }
            
            for _ in (0..<curWord.characters.count) {
                textDocumentProxy.deleteBackward()
            }
            textDocumentProxy.insertText(temp)
            textInSession += temp
            curWord = temp
        }
        
        // Suggestion
        if let content = textDocumentProxy.documentContextBeforeInput {

            suggestionManager.suggestEmojiWithInput(content, dispatchQueue: dispatch_get_main_queue(), completionHandler: { (result, error) in
                self.suggesstionView.setArraySuggest(NSMutableArray(array: result))
            })
            suggestionManager.suggestWithInput(content, numberOfSuggestion: 3, dispatchQueue: dispatch_get_main_queue()) { (result, error) in
                self.topExtraView.suggesstionView.setArraySuggest(NSMutableArray(array: result))
            }
            
            if withCharacter == " " && textInSession.length < Constants.maxLenghtOfMess {
                suggestionManager.updateTree(withInput: content)
            }
            
        }
    }
    
    func textProxyWillRemove(textDocumentProxy: UITextDocumentProxy, numberOfCharacters: Int) {
        
        if searchViewIsShow {
            searchView.deleteBackward()
            return
        }
        
        print("Number of charcter will remove: " + String(numberOfCharacters))
        
        if let text = textDocumentProxy.documentContextBeforeInput {
            
            if let lastWord = text.componentsSeparatedByString(" ").last {
                if (lastWord != curWord) {
                    curWord = lastWord
                    curRawWord = lastWord
                }
            } else {
                curWord = ""
                curRawWord = ""
            }
            
            if (curWord.isVietnameseWord()) {
                let tempRawWord = WordSplitting.init(_rawWord: curRawWord, typingType: typingType)
                let textLength = text.characters.count + (tempRawWord.count() - curWord.characters.count)
                var deleteCount = textLength > numberOfCharacters ? numberOfCharacters : textLength
                
                if (deleteCount >= tempRawWord.count()) {
                    deleteCount -= tempRawWord.count()
                    
                    for _ in (0..<tempRawWord.count()) {
                        textDocumentProxy.deleteBackward()
                    }
                    
                    for _ in (0..<deleteCount) {
                        textDocumentProxy.deleteBackward()
                    }
                    
                    if let leftText = textDocumentProxy.documentContextBeforeInput {
                        curWord = leftText.componentsSeparatedByString(" ").last!
                        curRawWord = curWord
                    } else {
                        curWord = ""
                        curRawWord = ""
                    }
                } else {
                    tempRawWord.deleteCharacters(deleteCount, typingType: typingType)
                    curRawWord = tempRawWord.rawWord!
                    
                    let tempCurWord = tempRawWord.getVietnameseWord()!
                    
                    for _ in (0..<curWord.characters.count) {
                        textDocumentProxy.deleteBackward()
                    }
                    textDocumentProxy.insertText(tempCurWord)
                    curWord = tempCurWord
                }
            } else {
                for _ in (0..<numberOfCharacters) {
                    textDocumentProxy.deleteBackward()
                }
                
                if let hasText = textDocumentProxy.documentContextBeforeInput {
                    curWord = hasText.componentsSeparatedByString(" ").last!
                    curRawWord = curWord
                } else {
                    curWord = ""
                    curRawWord = ""
                }
            }
        } else {
            
            for _ in (0..<numberOfCharacters) {
                textDocumentProxy.deleteBackward()
            }
            curWord = ""
            curRawWord = ""
        }
    }
    
    func textProxyRemoveCompleted(textDocumentProxy: UITextDocumentProxy) {
        
        if let content = self.textDocumentProxy.documentContextBeforeInput {
            
            suggestionManager.suggestEmojiWithInput(content, dispatchQueue: dispatch_get_main_queue(), completionHandler: { (result, error) in
                self.suggesstionView.setArraySuggest(NSMutableArray(array: result))
            })
            
            suggestionManager.suggestWithInput(content, numberOfSuggestion: 3, dispatchQueue: dispatch_get_main_queue()) { (result, error) in
                self.topExtraView.suggesstionView.setArraySuggest(NSMutableArray(array: result))
            }
            
        } else {
            self.suggesstionView.setArraySuggest(NSMutableArray(array: []))
            suggestionManager.suggestWithInput(randomString(1), numberOfSuggestion: 3, dispatchQueue: dispatch_get_main_queue()) { (result, error) in
                self.topExtraView.suggesstionView.setArraySuggest(NSMutableArray(array: result))
            }
        }
    }
    
    // MARK: - private method
    // Random generate a string with specify length
    private func randomString(length: Int) -> String {
        
        let allowedChars = "AĂÂBCDĐEÊGHIKLMNOÔƠPQRSTUƯVWXY"
        let allowedCharsCount = UInt32(allowedChars.characters.count)
        var randomString = ""
        
        for _ in (0..<length) {
            let randomNum = Int(arc4random_uniform(allowedCharsCount))
            let newCharacter = allowedChars[allowedChars.startIndex.advancedBy(randomNum)]
            randomString += String(newCharacter)
        }
        
        return randomString
    }

}



