//
//  BLSuggestionViewCell.swift
//  BanLaKey
//
//  Created by qhcthanh on 8/30/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import UIKit

public class BLSuggestionViewCell: UIControl {

    public var textLabel: UILabel!
    weak var suggesstionView: BLSuggestionView?
    
    convenience init() {
        self.init(frame: CGRectZero)
        
        self.textLabel = UILabel()
        self.textLabel.textAlignment = .Center
        self.textLabel.textColor = BLThemeManager.currentTheme.characterButtonTitleColor
        self.textLabel.font = UIFont(name: BLThemeManager.currentTheme.buttonTitleFont ,
                                     size: UIScreen.mainScreen().interfaceOrientation() == .Portrait ? BLThemeManager.currentTheme.buttonPhonePortraitFontSize : BLThemeManager.currentTheme.buttonPhoneLandscapeFontSize )
        
        self.addSubview(self.textLabel)
        
        self.textLabel.constrain(toEdgesOf: self)
        
        self.addTarget(self, action: #selector(didTouchInCell), forControlEvents: .TouchUpInside)
        self.addTarget(self, action: #selector(highlightButton), forControlEvents: [.TouchDown, .TouchDragInside])
        self.addTarget(self, action: #selector(unHighlightButton), forControlEvents: [.TouchUpInside, .TouchDragExit, .TouchCancel, .TouchUpOutside])
    }
    
    func didTouchInCell(sender: UIControl!) {
        
        suggesstionView?.didSelectSuggesstionCell(self)
    }
    
    func highlightButton(sender: UIControl!) {
        self.textLabel.backgroundColor = BLThemeManager.currentTheme.specialButtonHighlightedColor
    }
    
    func unHighlightButton(sender: UIControl!) {
        self.textLabel.backgroundColor = .clearColor()
    }
    
}
