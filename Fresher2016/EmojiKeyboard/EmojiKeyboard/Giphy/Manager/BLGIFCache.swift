//
//  BLGifCacheManager.swift
//  EmojiKeyboard
//
//  Created by qhcthanh on 8/25/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import UIKit
import ImageIO

public class BLGIFCache: NSObject {
    
    // MARK: Properties
    
    // Private static properties
    private static var shareCacheGIF: BLGIFCache!
    private static var managerToken: dispatch_once_t = 0
    
    // Private properties
    private var cachedGIF: Dictionary<String,BLGif>
    private var internalSerialQueue: dispatch_queue_t
    private var priorityQueue: NSMutableArray
    private var currentCacheSize: Float
    
    // Public properties
    public var cacheMode: YLCacheMode = YLCacheMode.LimitLengthAndSize
    public var cacheLength: UInt = 10
    public var cacheSize: Float = 10
    public var numberDeleteCacheItem: UInt = 1
    
    // MARK: Initialize
    
    private override init() {
        
        cachedGIF = Dictionary<String,BLGif>()
        internalSerialQueue = dispatch_queue_create("com.vng.BLGIFCacheQueue", DISPATCH_QUEUE_SERIAL)
        priorityQueue = NSMutableArray()
        currentCacheSize = 0
        
        
        super.init()
    }
    
    /**
     Share cache gif manager singletion
     
     - returns: The singleton BLGIFCache object
     */
    public class func shareManager() -> BLGIFCache {
        
        dispatch_once(&managerToken, {
            shareCacheGIF = BLGIFCache()
        })
        
        return shareCacheGIF
    }
    
    // MARK: Private method's
    
    private func updateCacheLength(length: UInt) -> Bool {
        
        if length > UInt(priorityQueue.count) {
            cacheLength = length
            return true
        }
        return false
    }
    
    private func updateCacheSize(size: Float) -> Bool {
        
        if size > currentCacheSize {
            currentCacheSize = size
            
            return true
        }
        
        return false
    }
    
    // MARK: Public method's
    
    /**
     <#Description#>
     
     - parameter data:   <#data description#>
     - parameter forKey: <#forKey description#>
     */
    public func cacheGIFData(gif: BLGif, forKey aKey: String) {
        dispatch_async(internalSerialQueue, {
            let sizeOfGIF: Float = gif.imageSize == nil ? 0 : gif.imageSize!
            var isFullCache = false
            
            // Check is full cache
            if  (Bool(self.cacheMode.rawValue | YLCacheMode.LimitLength.rawValue) &&  UInt(self.cachedGIF.count) >= self.cacheLength) ||
                (Bool(self.cacheMode.rawValue | YLCacheMode.LimitSize.rawValue)
                    && (self.currentCacheSize + sizeOfGIF) > self.cacheSize) {
                isFullCache = true
            }
            
            if !isFullCache {
                // Image cache is not full. Add this image to cache and push it to priority queue.
                
                self.cachedGIF[aKey] = gif
                self.priorityQueue.addObject(aKey)
                
            } else {
                // GIF Data cache is full.
                // Delete least recently used GIF Data until cache is available for this data.
                
                while isFullCache {
                    
                    if let firstObject = self.priorityQueue.firstObject {
                        self.currentCacheSize -= sizeOfGIF
                        
                        // Remove number cacheItem
                        for _ in 0..<self.numberDeleteCacheItem {
                            self.cachedGIF.removeValueForKey(firstObject as! String)
                            self.priorityQueue.removeObjectAtIndex(0)
                        }
                        
                        if  (Bool(self.cacheMode.rawValue | YLCacheMode.LimitLength.rawValue) &&  UInt(self.cachedGIF.count) < self.cacheLength) &&
                            (Bool(self.cacheMode.rawValue | YLCacheMode.LimitSize.rawValue)
                                && (self.currentCacheSize + sizeOfGIF) <= self.cacheSize) {
                            isFullCache = false
                        }
                        
                    }
                    
                }
                
                // Add this Gif data to cache and push it to priority queue.
                self.cachedGIF[aKey] = gif
                self.priorityQueue.addObject(aKey)
                
                
            }
            
            self.currentCacheSize += sizeOfGIF
        })
    }
    
    public func gifForKey(aKey: String) -> BLGif? {
        
        var returnData: BLGif?
        
        dispatch_sync(internalSerialQueue, {
            returnData = self.cachedGIF[aKey]
            
            if returnData != nil {
                var index = -1
                for i in 0..<self.priorityQueue.count {
                    if aKey == self.priorityQueue.objectAtIndex(i) as! String {
                        index = i
                        break
                    }
                }
                
                if index != -1 {
                    self.priorityQueue.removeObjectAtIndex(index)
                    self.priorityQueue.addObject(aKey)
                }
            }
        })

        return returnData
    }
    
    
    public func removeGIFDataForKey(aKey: String) {
        
        dispatch_async(internalSerialQueue, {
            let gif = self.cachedGIF[aKey]
            
            if let gif = gif {
                self.currentCacheSize -= gif.imageSize == nil ? 0 : gif.imageSize!
                self.cachedGIF.removeValueForKey(aKey)
                
                var index = -1
                for i in 0..<self.priorityQueue.count {
                    if aKey == self.priorityQueue.objectAtIndex(i) as! String {
                        index = i
                        break
                    }
                }
                
                if index != -1 {
                    self.priorityQueue.removeObjectAtIndex(index)
                }
            }
            
        })
    }
    
    public func removeAllObject() {
        dispatch_async(internalSerialQueue, {
            self.cachedGIF.removeAll()
            self.priorityQueue.removeAllObjects()
            self.currentCacheSize = 0
        })
    }
    
    public func numberItemInCache() -> Int {
        return cachedGIF.count
    }
}








