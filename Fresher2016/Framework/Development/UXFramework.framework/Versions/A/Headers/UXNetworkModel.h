/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "UXModel.h"
#import "UXModelDelegate.h"
#import "UXFrameworkDefines.h"

typedef NS_ENUM(NSInteger, UXModelState) {
    UXModelStateUnknown = 0,
    UXModelStateDidStartLoadFirst,
    UXModelStateDidStartLoadMore,
    UXModelStateDidStartRefresh,
    UXModelStateDidFinishLoad,
    UXModelStateDidShowModel,
    UXModelStateDidShowEmpty,
    UXModelStateDidShowError,
    UXModelStateDidStartLoading = UXModelStateDidStartLoadFirst | UXModelStateDidStartLoadMore
};

typedef NS_ENUM(NSInteger, UXSourceType) {
    UXSourceTypeUnknown = 0,
    UXSourceTypeDatabase,
    UXSourceTypeSocketStream,
    UXSourceTypeHttpRequest
};

typedef NS_ENUM(NSInteger, UXModelStyle) {
    UXModelTableViewStyleList = 0,
    UXModelTableViewStyleSection
};

@class UXSearchModel;
@class UXMutableArray;
@protocol UXNetworkModelDelegate;

/**
 * UXModel describes the state of an object that can be loaded from a remote source.
 *
 * By implementing this protocol, you can communicate to the user the state of network
 * activity in an object.
 */
@protocol UXNetworkModel <NSObject>

/**
 * An array of objects that conform to the UXModelDelegate protocol.
 */
- (NSMutableArray*)delegates;

/**
 * Indicates that the data is loaded.
 *
 * Default implementation returns NO.
 */
- (BOOL)isLoaded;

/**
 * Indicates that the data is in the process of loading.
 *
 * Default implementation returns NO.
 */
- (BOOL)isLoading;

/**
 * Indicates that the data is in the process of loading additional data.
 *
 * Default implementation returns NO.
 */
- (BOOL)isLoadingMore;

/**
 * Loads the model.
 *
 * Default implementation does nothing.
 */
- (void)load:(URLRequestCachePolicy)cachePolicy more:(BOOL)more;

/**
 * Cancel load data while typing
 */
- (void)cancel;

/**
 * Search data with text pattern
 */
- (void)search:(NSString*)text more:(BOOL)more success:(void(^)(NSMutableArray* responseObject))success failure:(void(^)(NSError* error))failure;

/**
 * Loads the model.
 *
 * Default implementation does nothing.
 */
- (void)reloadDataSource:(BOOL)loadMore success:(void(^)(NSMutableArray* responseObject, NSError* error))success failure:(void(^)(NSError* error))failure;

- (void)loadDataSource:(UXSourceType)source more:(BOOL)more success:(void(^)(id responseObject, NSError* error))success failure:(void(^)(NSError* error))failure;

/**
 * The model has some states such as unknown, loading, complete, etc,..
 */
@property (nonatomic, assign) UXModelState modelState;

/**
 * The model has a source type that determines where we're loading
 */
@property (nonatomic, assign) UXSourceType sourceType;

/**
 * Raw data model that is an instant of entity
 */
@property (nonatomic, strong) UXMutableArray* dataSet;

/**
 * The search model that contains searching objects
 */
@property (nonatomic, assign) UXSearchModel* searchModel;

@end

@interface UXNetworkModel : NSObject <UXNetworkModel> {
    UXModelState _modelState;
    UXSourceType _sourceType;
    UXMutableArray* _dataSet;
    NSMutableArray* _delegates;
    __unsafe_unretained UXSearchModel* _searchModel;
}

- (id)initWithDataSet:(UXMutableArray*)dataSet;

/**
 * Remove all entity model
 */
- (void)removeAllObjects;

/**
 * Remove all search objects
 */
- (void)removeAllSearchObjects;

/**
 * Add an entity model
 */
- (void)addObject:(id)object;

/**
 * Add mutilple objects model
 */
- (void)addObjectsFromArray:(NSArray*)objects;

/**
 * Remove an entity model
 */
- (void)removeObject:(id)object;

/**
 * Add view delegates that the model will be callback.
 */
- (void)addDelegate:(id<UXNetworkModelDelegate>)delegate;

/**
 * Remove all delgates that linked
 */
- (void)removeAllDelegates;

@end

@protocol UXNetworkModelDelegate <UXModelDelegate>

@optional

@end

