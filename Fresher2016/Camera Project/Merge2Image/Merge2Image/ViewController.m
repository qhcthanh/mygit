//
//  ViewController.m
//  Merge2Image
//
//  Created by BaoNQ on 6/17/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // Sau khi bấm nút lưu, ta sẽ được:
    //  + 1 UIImage lưu hình vừa chụp (đã blend màu)
    UIImage *inputImage = [UIImage imageNamed:@"image2.jpg"];

    //  + 1 Array chứa các sticker và textImage (UIImage).
    NSMutableArray *stickers = [[NSMutableArray alloc] init];
    [stickers addObject:[UIImage imageNamed:@"sticker.png"]];

    //  + 1 Array chứa vị trí và kích thước tương ứng của các stickers so với màn hình điện thoại.
    NSMutableArray *positions = [[NSMutableArray alloc] init];
    [positions addObject:[NSValue valueWithCGRect:CGRectMake(100, 200, 100, 100)]];
    
    UIGraphicsBeginImageContext(inputImage.size);
    //Vẽ hình vừa chụp vào context
    [inputImage drawInRect:CGRectMake(0, 0, inputImage.size.width, inputImage.size.height)];
    
    UIImage *inputImage1 = [stickers objectAtIndex:0];
    CGRect pos = [[positions objectAtIndex:0] CGRectValue];
    
    // Lấy kích thước màn hình
    float screenWidth = [UIScreen mainScreen].bounds.size.width;
    float screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    // Lấy kích thước hình vừa chụp
    float imageWidth = inputImage.size.width;
    float imageHeight = inputImage.size.height;
    
    // Tạo độ vẽ hình sẽ được tính toán dựa trên tỉ lệ của screen và image.
    [inputImage1 drawInRect:CGRectMake(pos.origin.x/screenWidth * imageWidth, pos.origin.y/screenHeight *imageHeight, pos.size.width/screenWidth *imageWidth, pos.size.height/screenHeight *imageHeight)];
    
    UIImage* screenshot = UIGraphicsGetImageFromCurrentImageContext();
    self.imageView.image = screenshot;
    
    UIGraphicsEndImageContext();
    
    
    // Lưu file hình về desktop.
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSData *myImageData = UIImagePNGRepresentation(screenshot);
    [fileManager createFileAtPath:@"/Users/admin/Desktop/myimage.png" contents:myImageData attributes:nil];
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:touch.view];
    NSLog(@"%f, %f", location.x, location.y);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
