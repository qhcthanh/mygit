//
//  NSString+encoding.h
//  FacebookContact
//
//  Created by BaoNQ on 7/20/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Encoding)

- (NSString *)URLEncoding;

@end
