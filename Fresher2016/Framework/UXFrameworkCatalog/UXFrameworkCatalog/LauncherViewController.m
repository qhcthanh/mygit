/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "LauncherViewController.h"

#import "BadgedLauncherViewObject.h"

@interface LauncherViewController () <UXLauncherViewModelDelegate>
@property (nonatomic, retain) UXLauncherViewModel* model;
@end

@implementation LauncherViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        self.title = @"Modifying";
        
        // Start by loading the UXFramwork app icon.
        NSString* imagePath = UXPathForBundleResource(nil, @"Icon.png");
        UIImage* image = [[UXImageMemoryCache sharedUXImageMemoryCache] objectWithName:imagePath];
        if (nil == image) {
            image = [UIImage imageWithContentsOfFile:imagePath];
            [[UXImageMemoryCache sharedUXImageMemoryCache] storeObject:image withName:imagePath];
        }
        
        // We can provide different launcher objects in the model to show different types of launcher
        // buttons. In this example we'll mix the default UXLauncherViewObject with
        // BadgedLauncherViewObject, which displays a launcher button with a badge number.
        NSArray* contents =
        [NSArray arrayWithObjects:
         [NSArray arrayWithObjects:
          // Shows a button with a badge showing the number 12.
          [BadgedLauncherViewObject objectWithTitle:@"Item 1" image:image badgeNumber:12],
          [UXLauncherViewObject objectWithTitle:@"Item 2" image:image],
          [UXLauncherViewObject objectWithTitle:@"Item 3" image:image],
          
          // Will not display a badge because the number is 0.
          [BadgedLauncherViewObject objectWithTitle:@"Item 4" image:image badgeNumber:0],
          
          // The badge number will become @"99+".
          [BadgedLauncherViewObject objectWithTitle:@"Item 6" image:image badgeNumber:103],
          nil],
         nil];
        
        // We'll start off with a completely empty model.
        _model = [[UXLauncherViewModel alloc] initWithArrayOfPages:contents delegate:self];
        
        // We want to add a button to the navigation bar that, when tapped, adds a new page of launcher
        // buttons to the launcher view.
        self.navigationItem.rightBarButtonItem =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                      target:self
                                                      action:@selector(didTapAddButton:)];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // iOS 7-only.    
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    self.launcherView.dataSource = self.model;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UXLauncherViewModelDelegate

- (void)launcherViewModel:(UXLauncherViewModel *)launcherViewModel
      configureButtonView:(UIView<UXLauncherButtonView> *)buttonView
          forLauncherView:(UXLauncherView *)launcherView
                pageIndex:(NSInteger)pageIndex
              buttonIndex:(NSInteger)buttonIndex
                   object:(id<UXLauncherViewObject>)object {
    // BadgedLauncherViewObject is a subclass of UXLauncherButtonView so we can still safely cast
    // without worrying about crashes.
    UXLauncherButtonView* launcherButtonView = (UXLauncherButtonView *)buttonView;
    
    launcherButtonView.label.layer.shadowColor = [UIColor blackColor].CGColor;
    launcherButtonView.label.layer.shadowOffset = CGSizeMake(0, 1);
    launcherButtonView.label.layer.shadowOpacity = 1;
    launcherButtonView.label.layer.shadowRadius = 1;
}

#pragma mark - UXLauncherDelegate

- (void)launcherView:(UXLauncherView *)launcher didSelectItemOnPage:(NSInteger)page atIndex:(NSInteger)index {
    id<UXLauncherViewObject> object = [self.model objectAtIndex:index pageIndex:page];
    
//    [UXAlertView showAlertWithTitle:@"Notice" message:[@"Did tap button with title: " stringByAppendingString:object.title] cancelButton:@"Cancel" okButton:@"OK" buttonHandler:^(UXAlertView *alert, NSInteger buttonIndex) {
//        
//    }];
}

#pragma mark - User Actions

- (void)didTapAddButton:(UIBarButtonItem *)barButtonItem {
    // When the user taps the + button we are going to create a new page filled with a random number
    // of buttons with random titles.
    
    // Start by loading the UXFramwork app icon.
    NSString* imagePath = UXPathForBundleResource(nil, @"Icon.png");
    UIImage* image = [[UXImageMemoryCache sharedUXImageMemoryCache] objectWithName:imagePath];
    if (nil == image) {
        image = [UIImage imageWithContentsOfFile:imagePath];
        [[UXImageMemoryCache sharedUXImageMemoryCache] storeObject:image withName:imagePath];
    }
    
    // Now we create a page with 1-9 randomly titled objects.
    NSInteger randomNumberOfItems = arc4random_uniform(8) + 1;
    NSMutableArray* objects = [NSMutableArray array];
    for (NSInteger ix = 0; ix < randomNumberOfItems; ++ix) {
        [objects addObject:[UXLauncherViewObject objectWithTitle:[NSString stringWithFormat:@"item %d", arc4random_uniform(1000)] image:image]];
    }
    
    // appendPage should be pretty self-explanatory.
    [self.model appendPage:objects];
    
    // Now that we've modified the model we need to reload the data for our changes to become visible.
    // The cool thing about UXFramwork' launcher view is that it only reloads the visible data, so
    // reloadData is a relatively lightweight operation, just like UITableView.
    [self.launcherView reloadData];
}

@end
