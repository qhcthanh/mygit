/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXTextField.h"

@class UXCSSRuleset;
@class UXDOM;

@interface UXTextField (UXStyleable)

/**
 * Applies the given rule set to this text field.
 *
 * This method is exposed primarily for subclasses to use when implementing the
 * applyStyleWithRuleSet: method from NIStyleable.
 */
- (void)applyUXTextFieldStyleWithRuleSet:(UXCSSRuleset *)ruleSet inDOM: (UXDOM*) dom;

/**
 * Applies the given rule set to this label.
 *
 * This method is exposed primarily for subclasses to use when implementing the
 * applyStyleWithRuleSet: method from NIStyleable. Since some of the view
 * styles (e.g. positioning) may rely on some label elements (like text), this is called
 * before the view styling is done.
 */
- (void)applyUXTextFieldStyleBeforeViewWithRuleSet:(UXCSSRuleset *)ruleSet inDOM: (UXDOM*) dom;

/**
 * Applies the given rule set to this text field but for a pseudo class. Thus it only supports the subset of
 * properties that can be set on states of the button. (There's no fancy stuff that applies the styles
 * manually on state transitions.
 *
 * Since UIView doesn't have psuedo's, we don't need the specific version for UIButton like we do with
 * applyButtonStyleWithRuleSet.
 */
- (void)applyStyleWithRuleSet:(UXCSSRuleset *)ruleSet forPseudoClass: (NSString*) pseudo inDOM: (UXDOM*) dom;


@end

