//
//  BLThemeManager.swift
//  BanLaKey
//
//  Created by BaoNQ on 8/17/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

class BLThemeManager: NSObject {
    
    static var currentTheme: BLTheme {
        get {
            return self.lightAppearanceTheme()
        }
        set {
            self.currentTheme = newValue
        }
    }
    
    static func lightAppearanceTheme() -> BLTheme {
        struct StaticVar {
            static var lightTheme: BLTheme = BLTheme()
            static var onceToken: dispatch_once_t = 0
        }
        dispatch_once(&StaticVar.onceToken, {
            StaticVar.lightTheme.backgroundColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
            StaticVar.lightTheme.backgroundImage = nil
            StaticVar.lightTheme.buttonTitleFont = "Avenir"
            StaticVar.lightTheme.buttonBorderColor = UIColor.blackColor()
            
            StaticVar.lightTheme.specialButtonTitleColor = UIColor.whiteColor()
            StaticVar.lightTheme.characterButtonTitleColor = UIColor.blackColor()
            
            StaticVar.lightTheme.specialButtonHighLightedTitleColor = UIColor.whiteColor()
            StaticVar.lightTheme.characterButtonHighLightedTitleColor = UIColor.blackColor()
            
            
            StaticVar.lightTheme.characterButtonShadowColor = UIColor.init(red: 139/255.0, green: 142/255.0, blue: 146/255.0, alpha: 1.0)
            StaticVar.lightTheme.specialButtonShadowColor = UIColor.grayColor()
            
            StaticVar.lightTheme.specialButtonHighlightedColor = UIColor.grayColor()
            StaticVar.lightTheme.characterButtonHightlightedColor = UIColor.grayColor()
            
            StaticVar.lightTheme.useSameColorForSpecialButtonHighlighted = false
            StaticVar.lightTheme.useSameColorForCharacterButtonHighlighted = false
            
            StaticVar.lightTheme.buttonShadowYOffset = 1.0
            StaticVar.lightTheme.buttonLabelYOffset = -1.5
            StaticVar.lightTheme.buttonImageYOffset = -0.5
            
            StaticVar.lightTheme.buttonPhoneBorderWidth = 1.0
            StaticVar.lightTheme.buttonPhoneShadowWidth = 1.0
            StaticVar.lightTheme.buttonPhoneCornerRadius = 4.0
            StaticVar.lightTheme.buttonPhonePopupCornerRadius = 9.0
            StaticVar.lightTheme.buttonPhonePortraitFontSize = 16.0
            StaticVar.lightTheme.buttonPhoneLandscapeFontSize = 17.0
            StaticVar.lightTheme.buttonPhonePopupPortraitFontSize = 18.0
            StaticVar.lightTheme.buttonPhonePopupLandscapeFontSize = 19.0
            
            
            StaticVar.lightTheme.buttonPadBorderWidth = 1.0
            StaticVar.lightTheme.buttonPadShadowWidth = 1.5
            StaticVar.lightTheme.buttonPadCornerRadius = 5.0
            StaticVar.lightTheme.buttonPadPopupCornerRadius = 11.0
            StaticVar.lightTheme.buttonPadPortraitFontSize = 18.0
            StaticVar.lightTheme.buttonPadLandscapeFontSize = 22.0
            StaticVar.lightTheme.buttonPadPopupPortraitFontSize = 20.0
            StaticVar.lightTheme.buttonPadPopupLandscapeFontSize = 24.0
            
            let lightColor = UIColor.init(white: 254/255.0, alpha: 1.0)
            let darkColor = UIColor.init(white: 85/255.0, alpha: 1.0)

            // Butons color
            // 1st row
            StaticVar.lightTheme.QButton = lightColor
            StaticVar.lightTheme.WButton = lightColor
            StaticVar.lightTheme.EButton = lightColor
            StaticVar.lightTheme.RButton = lightColor
            StaticVar.lightTheme.TButton = lightColor
            StaticVar.lightTheme.YButton = lightColor
            StaticVar.lightTheme.UButton = lightColor
            StaticVar.lightTheme.IButton = lightColor
            StaticVar.lightTheme.OButton = lightColor
            StaticVar.lightTheme.PButton = lightColor
            // 2nd row
            StaticVar.lightTheme.AButton = lightColor
            StaticVar.lightTheme.SButton = lightColor
            StaticVar.lightTheme.DButton = lightColor
            StaticVar.lightTheme.FButton = lightColor
            StaticVar.lightTheme.GButton = lightColor
            StaticVar.lightTheme.HButton = lightColor
            StaticVar.lightTheme.JButton = lightColor
            StaticVar.lightTheme.KButton = lightColor
            StaticVar.lightTheme.LButton = lightColor
            // 3rd row
            StaticVar.lightTheme.ZButton = lightColor
            StaticVar.lightTheme.XButton = lightColor
            StaticVar.lightTheme.CButton = lightColor
            StaticVar.lightTheme.VButton = lightColor
            StaticVar.lightTheme.BButton = lightColor
            StaticVar.lightTheme.NButton = lightColor
            StaticVar.lightTheme.MButton = lightColor

            StaticVar.lightTheme.shiftButton = darkColor
            StaticVar.lightTheme.deleteButton = darkColor
            StaticVar.lightTheme.globeButton = darkColor
            StaticVar.lightTheme.spaceButton = darkColor
            StaticVar.lightTheme.returnButton = darkColor
            StaticVar.lightTheme.keyModeButton = darkColor

            StaticVar.lightTheme.shiftDefaultButtonImage = "shift.png"
            StaticVar.lightTheme.shiftOnceButtonImage = "shift_lock.png"
            StaticVar.lightTheme.shiftForeverButtonImage = "shift_lock.png"
            
            StaticVar.lightTheme.deleteButtonImage = "delete.png"
            StaticVar.lightTheme.deleteButtonHighlightedImage = "delete.png"
            
            StaticVar.lightTheme.globeButtonImage = "globe_white.png"
            StaticVar.lightTheme.globeButtonHighlightedImage = "globe_white.png"

            StaticVar.lightTheme.spaceButtonImage = ""
            StaticVar.lightTheme.spaceButtonHighlightedImage = ""

            StaticVar.lightTheme.returnButtonImage = ""
            StaticVar.lightTheme.returnButtonHighlightedImage = ""

            StaticVar.lightTheme.keyModeButtonImage = ""
            StaticVar.lightTheme.keyModeButtonHighlightedImage = ""

            
            
            // Number keys
            StaticVar.lightTheme.zeroButton = lightColor
            StaticVar.lightTheme.oneButton = lightColor
            StaticVar.lightTheme.twoButton = lightColor
            StaticVar.lightTheme.threeButton = lightColor
            StaticVar.lightTheme.fourButton = lightColor
            StaticVar.lightTheme.fiveButton = lightColor
            StaticVar.lightTheme.sixButton = lightColor
            StaticVar.lightTheme.sevenButton = lightColor
            StaticVar.lightTheme.eightButton = lightColor
            StaticVar.lightTheme.nineButton = lightColor
            
            StaticVar.lightTheme.minusButton = lightColor
            StaticVar.lightTheme.slashButton = lightColor
            StaticVar.lightTheme.colonButton = lightColor
            StaticVar.lightTheme.semicolonButton = lightColor
            StaticVar.lightTheme.leftParenthesisButton = lightColor
            StaticVar.lightTheme.rightParenthesisButton = lightColor
            StaticVar.lightTheme.dollarSignButton = lightColor
            StaticVar.lightTheme.ampersandButton = lightColor
            StaticVar.lightTheme.atSignButton = lightColor
            StaticVar.lightTheme.quotationMarkButton = lightColor
            StaticVar.lightTheme.fullStopButton = lightColor
            StaticVar.lightTheme.commaButton = lightColor
            StaticVar.lightTheme.questionMarkButton = lightColor
            StaticVar.lightTheme.exclamationMarkButton = lightColor
            StaticVar.lightTheme.apostropheButton = lightColor
            StaticVar.lightTheme.graveAccentButton = lightColor
            
            
            // Number keys page 2
            StaticVar.lightTheme.leftSquareBracketButton = lightColor
            StaticVar.lightTheme.rightSquareBracketButton = lightColor
            StaticVar.lightTheme.leftCurlyBracketButton = lightColor
            StaticVar.lightTheme.rightCurlyBracketButton = lightColor
            StaticVar.lightTheme.sharpButton = lightColor
            StaticVar.lightTheme.percentSignButton = lightColor
            StaticVar.lightTheme.circumflexAccentButton = lightColor
            StaticVar.lightTheme.asteriskButton = lightColor
            StaticVar.lightTheme.plusSignButton = lightColor
            StaticVar.lightTheme.equalSignButton = lightColor
            StaticVar.lightTheme.lowLineButton = lightColor
            StaticVar.lightTheme.backslashButton = lightColor
            StaticVar.lightTheme.verticalBarButton = lightColor
            StaticVar.lightTheme.tildeButton = lightColor
            StaticVar.lightTheme.lessThanSignButton = lightColor
            StaticVar.lightTheme.greaterThanSignButton = lightColor
            StaticVar.lightTheme.euroSignButton = lightColor
            StaticVar.lightTheme.poundSignButton = lightColor
            StaticVar.lightTheme.yenSignButton = lightColor
            StaticVar.lightTheme.centSignButton = lightColor
            
            StaticVar.lightTheme.blackCircleButton = lightColor
            StaticVar.lightTheme.registerdSignButton = lightColor
            StaticVar.lightTheme.copyrightSignButton = lightColor
            StaticVar.lightTheme.degreeCelsiusButton = lightColor
            StaticVar.lightTheme.degreeFahrenheitButton = lightColor
        })

        return StaticVar.lightTheme
    }
    
    static func darkAppearanceTheme() -> BLTheme {
        struct StaticVar {
            static var darkTheme: BLTheme!
            static var onceToken: dispatch_once_t = 0
        }
        dispatch_once(&StaticVar.onceToken, {
            StaticVar.darkTheme.backgroundColor = UIColor.clearColor()
            StaticVar.darkTheme.backgroundImage = nil
            StaticVar.darkTheme.buttonTitleFont = "Avenir"
            StaticVar.darkTheme.buttonBorderColor = UIColor.whiteColor()
            
            StaticVar.darkTheme.specialButtonTitleColor = UIColor.whiteColor()
            StaticVar.darkTheme.characterButtonTitleColor = UIColor.whiteColor()
            
            StaticVar.darkTheme.characterButtonShadowColor = UIColor.init(red: 139/255.0, green: 142/255.0, blue: 146/255.0, alpha: 1.0)
            StaticVar.darkTheme.specialButtonShadowColor = UIColor.clearColor()

            StaticVar.darkTheme.specialButtonHighlightedColor = UIColor.grayColor()
            StaticVar.darkTheme.characterButtonHightlightedColor = UIColor.grayColor()
            
            StaticVar.darkTheme.useSameColorForSpecialButtonHighlighted = false
            StaticVar.darkTheme.useSameColorForCharacterButtonHighlighted = false
            
            
            StaticVar.darkTheme.buttonShadowYOffset = 1.0
            StaticVar.darkTheme.buttonLabelYOffset = -1.5
            StaticVar.darkTheme.buttonImageYOffset = -0.5
            
            
            StaticVar.darkTheme.buttonPhoneBorderWidth = 1.0
            StaticVar.darkTheme.buttonPhoneShadowWidth = 1.0
            StaticVar.darkTheme.buttonPhoneCornerRadius = 4.0
            StaticVar.darkTheme.buttonPhonePopupCornerRadius = 9.0
            StaticVar.darkTheme.buttonPhonePortraitFontSize = 16.0
            StaticVar.darkTheme.buttonPhoneLandscapeFontSize = 17.0
            StaticVar.darkTheme.buttonPhonePopupPortraitFontSize = 18.0
            StaticVar.darkTheme.buttonPhonePopupLandscapeFontSize = 19.0
            
            
            StaticVar.darkTheme.buttonPadBorderWidth = 1.0
            StaticVar.darkTheme.buttonPadShadowWidth = 1.5
            StaticVar.darkTheme.buttonPadCornerRadius = 5.0
            StaticVar.darkTheme.buttonPadPopupCornerRadius = 11.0
            StaticVar.darkTheme.buttonPadPortraitFontSize = 18.0
            StaticVar.darkTheme.buttonPadLandscapeFontSize = 22.0
            StaticVar.darkTheme.buttonPadPopupPortraitFontSize = 20.0
            StaticVar.darkTheme.buttonPadPopupLandscapeFontSize = 24.0
            
            let darkColor = UIColor.init(white: 85/255.0, alpha: 1.0)
            // Butons color
            // 1st row
            StaticVar.darkTheme.QButton = darkColor
            StaticVar.darkTheme.WButton = darkColor
            StaticVar.darkTheme.EButton = darkColor
            StaticVar.darkTheme.RButton = darkColor
            StaticVar.darkTheme.TButton = darkColor
            StaticVar.darkTheme.YButton = darkColor
            StaticVar.darkTheme.UButton = darkColor
            StaticVar.darkTheme.IButton = darkColor
            StaticVar.darkTheme.OButton = darkColor
            StaticVar.darkTheme.PButton = darkColor
            // 2nd row
            StaticVar.darkTheme.AButton = darkColor
            StaticVar.darkTheme.SButton = darkColor
            StaticVar.darkTheme.DButton = darkColor
            StaticVar.darkTheme.FButton = darkColor
            StaticVar.darkTheme.GButton = darkColor
            StaticVar.darkTheme.HButton = darkColor
            StaticVar.darkTheme.JButton = darkColor
            StaticVar.darkTheme.KButton = darkColor
            StaticVar.darkTheme.LButton = darkColor
            // 3rd row
            StaticVar.darkTheme.ZButton = darkColor
            StaticVar.darkTheme.XButton = darkColor
            StaticVar.darkTheme.CButton = darkColor
            StaticVar.darkTheme.VButton = darkColor
            StaticVar.darkTheme.BButton = darkColor
            StaticVar.darkTheme.NButton = darkColor
            StaticVar.darkTheme.MButton = darkColor
            
            StaticVar.darkTheme.shiftButton = darkColor
            StaticVar.darkTheme.deleteButton = darkColor
            StaticVar.darkTheme.globeButton = darkColor
            StaticVar.darkTheme.spaceButton = darkColor
            StaticVar.darkTheme.returnButton = darkColor
            StaticVar.darkTheme.keyModeButton = darkColor
            
            StaticVar.darkTheme.shiftDefaultButtonImage = "shift.png"
            StaticVar.darkTheme.shiftOnceButtonImage = "shift_lock.png"
            StaticVar.darkTheme.shiftForeverButtonImage = "shift_lock.png"
            StaticVar.darkTheme.deleteButtonImage = "delete.png"
            StaticVar.darkTheme.globeButtonImage = "globe_white.png"
            StaticVar.darkTheme.spaceButtonImage = ""
            StaticVar.darkTheme.returnButtonImage = ""
            StaticVar.darkTheme.keyModeButtonImage = ""
            
            
            // Number keys
            StaticVar.darkTheme.zeroButton = darkColor
            StaticVar.darkTheme.oneButton = darkColor
            StaticVar.darkTheme.twoButton = darkColor
            StaticVar.darkTheme.threeButton = darkColor
            StaticVar.darkTheme.fourButton = darkColor
            StaticVar.darkTheme.fiveButton = darkColor
            StaticVar.darkTheme.sixButton = darkColor
            StaticVar.darkTheme.sevenButton = darkColor
            StaticVar.darkTheme.eightButton = darkColor
            StaticVar.darkTheme.nineButton = darkColor
            
            StaticVar.darkTheme.minusButton = darkColor
            StaticVar.darkTheme.slashButton = darkColor
            StaticVar.darkTheme.colonButton = darkColor
            StaticVar.darkTheme.semicolonButton = darkColor
            StaticVar.darkTheme.leftParenthesisButton = darkColor
            StaticVar.darkTheme.rightParenthesisButton = darkColor
            StaticVar.darkTheme.dollarSignButton = darkColor
            StaticVar.darkTheme.ampersandButton = darkColor
            StaticVar.darkTheme.atSignButton = darkColor
            StaticVar.darkTheme.quotationMarkButton = darkColor
            StaticVar.darkTheme.fullStopButton = darkColor
            StaticVar.darkTheme.commaButton = darkColor
            StaticVar.darkTheme.questionMarkButton = darkColor
            StaticVar.darkTheme.exclamationMarkButton = darkColor
            StaticVar.darkTheme.apostropheButton = darkColor
            StaticVar.darkTheme.graveAccentButton = darkColor
            
            
            // Number keys page 2
            StaticVar.darkTheme.leftSquareBracketButton = darkColor
            StaticVar.darkTheme.rightSquareBracketButton = darkColor
            StaticVar.darkTheme.leftCurlyBracketButton = darkColor
            StaticVar.darkTheme.rightCurlyBracketButton = darkColor
            StaticVar.darkTheme.sharpButton = darkColor
            StaticVar.darkTheme.percentSignButton = darkColor
            StaticVar.darkTheme.circumflexAccentButton = darkColor
            StaticVar.darkTheme.asteriskButton = darkColor
            StaticVar.darkTheme.plusSignButton = darkColor
            StaticVar.darkTheme.equalSignButton = darkColor
            StaticVar.darkTheme.lowLineButton = darkColor
            StaticVar.darkTheme.backslashButton = darkColor
            StaticVar.darkTheme.verticalBarButton = darkColor
            StaticVar.darkTheme.tildeButton = darkColor
            StaticVar.darkTheme.lessThanSignButton = darkColor
            StaticVar.darkTheme.greaterThanSignButton = darkColor
            StaticVar.darkTheme.euroSignButton = darkColor
            StaticVar.darkTheme.poundSignButton = darkColor
            StaticVar.darkTheme.yenSignButton = darkColor
            StaticVar.darkTheme.centSignButton = darkColor
            
            StaticVar.darkTheme.blackCircleButton = darkColor
            StaticVar.darkTheme.registerdSignButton = darkColor
            StaticVar.darkTheme.copyrightSignButton = darkColor
            StaticVar.darkTheme.degreeCelsiusButton = darkColor
            StaticVar.darkTheme.degreeFahrenheitButton = darkColor
        })
        
        return StaticVar.darkTheme
    }
    
//    private var color: UIColor
//    private var shadowColor: UIColor
//    
//    var style: BLKeyStyle {
//        get {
//            return self.style
//        }
//        set {
//            if self.style == newValue {
//                return
//            }
//            self.style = newValue
//            self.updateState()
//        }
//    }
//    var appearance: BLKeyAppearance {
//        get {
//            return self.appearance
//        }
//        set {
//            if (self.appearance == newValue) {
//                return
//            }
//            self.appearance = newValue
//            self.updateState()
//        }
//    }
//    var cornerRadius: CGFloat {
//        get {
//            return self.cornerRadius
//        }
//        set {
//            if (self.cornerRadius == newValue) {
//                return
//            }
//            self.cornerRadius = newValue
//            self.setNeedsDisplay()
//        }
//    }
//    
//    func getTitleFont() -> UIFont! {
//        return self.label!.font
//    }
//    
//    func setTitleFont(newFont: UIFont) {
//        self.label!.font = newFont
//    }
//    
//    func updateState() {
//        switch self.appearance {
//        case BLKeyAppearance.Dark:
//            
//            switch self.style {
//            case BLKeyStyle.Light:
//                self.label!.textColor = UIColor.whiteColor()
//                
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLDarkAppearance.darkKeyColor()
//                    self.shadowColor = BLDarkAppearance.darkKeyShadowColor()
//                    
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLDarkAppearance.lightKeyColor()
//                    self.shadowColor = BLDarkAppearance.lightKeyShadowColor()
//                    
//                    break
//                }
//                
//                break
//            case BLKeyStyle.Dark:
//                self.label?.textColor = UIColor.whiteColor()
//                self.imageView?.image = self.imageView?.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
//                self.imageView?.tintColor = BLDarkAppearance.whiteColor()
//                
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLDarkAppearance.lightKeyColor()
//                    self.shadowColor = BLDarkAppearance.lightKeyShadowColor()
//                    
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLDarkAppearance.darkKeyColor()
//                    self.shadowColor = BLDarkAppearance.darkKeyShadowColor()
//                    
//                    break
//                }
//                break
//            default: // BLKeyStyle.Blue
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLDarkAppearance.lightKeyColor()
//                    self.shadowColor = BLDarkAppearance.lightKeyShadowColor()
//                    self.label?.textColor = BLDarkAppearance .blackColor()
//                    
//                    break
//                case UIControlState.Disabled:
//                    self.color = BLDarkAppearance.darkKeyColor()
//                    self.shadowColor = BLDarkAppearance.darkKeyShadowColor()
//                    self.label?.textColor = BLDarkAppearance.blueKeyDisabledTitleColor()
//                    
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLDarkAppearance.blueKeyColor()
//                    self.shadowColor = BLDarkAppearance.blueKeyShadowColor()
//                    self.label?.textColor = UIColor.whiteColor()
//                    
//                    break
//                }
//                break
//            }
//            
//            break
//        default: // BLKeyAppearance.Light
//            switch self.style {
//            case BLKeyStyle.Light:
//                self.label!.textColor = UIColor.blackColor()
//                
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLLightAppearance.darkKeyColor()
//                    self.shadowColor = BLLightAppearance.darkKeyShadowColor()
//                    
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLLightAppearance.lightKeyColor()
//                    self.shadowColor = BLLightAppearance.lightKeyShadowColor()
//                    
//                    break
//                }
//                
//                break
//            case BLKeyStyle.Dark:
//                self.label?.textColor = UIColor.blackColor()
//                
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLLightAppearance.lightKeyColor()
//                    self.shadowColor = BLLightAppearance.lightKeyShadowColor()
//                    self.imageView?.tintColor = BLLightAppearance.blackColor()
//                    
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLLightAppearance.darkKeyColor()
//                    self.shadowColor = BLLightAppearance.darkKeyShadowColor()
//                    self.imageView?.tintColor = BLLightAppearance.whiteColor()
//                    
//                    break
//                }
//                break
//            default: // BLKeyStyle.Blue
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLLightAppearance.lightKeyColor()
//                    self.shadowColor = BLLightAppearance.lightKeyShadowColor()
//                    self.label?.textColor = BLLightAppearance .blackColor()
//                    
//                    break
//                case UIControlState.Disabled:
//                    self.color = BLLightAppearance.darkKeyColor()
//                    self.shadowColor = BLLightAppearance.darkKeyShadowColor()
//                    self.label?.textColor = BLLightAppearance.blueKeyDisabledTitleColor()
//                    
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLLightAppearance.blueKeyColor()
//                    self.shadowColor = BLLightAppearance.blueKeyShadowColor()
//                    self.label?.textColor = UIColor.whiteColor()
//                    
//                    break
//                }
//                break
//            }
//            
//            break
//        }
//    }

    
}
