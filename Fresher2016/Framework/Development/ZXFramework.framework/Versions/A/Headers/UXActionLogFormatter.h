/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXLogFormatter.h"

@interface UXActionLogFormatter : NSObject <UXLogFormatter> {
}

@end
