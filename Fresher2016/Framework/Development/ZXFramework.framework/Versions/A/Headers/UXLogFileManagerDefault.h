/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXLogFileManager.h"

/**
 * Default log file manager.
 *
 * All log files are placed inside the logsDirectory.
 * If a specific logsDirectory isn't specified, the default directory is used.
 * On Mac, this is in ~/Library/Logs/<Application Name>.
 * On iPhone, this is in ~/Library/Caches/Logs.
 *
 * Log files are named "<bundle identifier> <date> <time>.log"
 * Example: com.organization.myapp 2013-12-03 17-14.log
 *
 * Archived log files are automatically deleted according to the maximumNumberOfLogFiles property.
 **/
@class UXLogFileInfo;
@protocol UXLogFileManagerDefaultDelegate;
@interface UXLogFileManagerDefault : NSObject <UXLogFileManager> {
	NSUInteger maximumNumberOfLogFiles;
	NSString* _logsDirectory;
    NSString* _filePrefix;
    
#if TARGET_OS_IPHONE
    NSString* _defaultFileProtectionLevel;
#endif
    
    BOOL _isProcessing;
    __unsafe_unretained id <UXLogFileManagerDefaultDelegate> _delegate;
}

@property (nonatomic, assign) BOOL isProcessing;
@property (nonatomic, assign) id <UXLogFileManagerDefaultDelegate> delegate;

+ (void)compressLogFile:(UXLogFileInfo*)logFile success:(void(^)(UXLogFileInfo* fileInfo))success failure:(void(^)(UXLogFileInfo* fileInfo))failure;

- (instancetype)init;
- (instancetype)initWithLogsDirectory:(NSString *)logsDirectory;
- (instancetype)initWithLogsDirectory:(NSString *)aLogsDirectory andFilePrefix:(const NSString*) prefix;
#if TARGET_OS_IPHONE
/*
 * Calling this constructor you can override the default "automagically" chosen NSFileProtection level.
 * Useful if you are writing a command line utility / CydiaSubstrate addon for iOS that has no NSBundle
 * or like SpringBoard no BackgroundModes key in the NSBundle:
 *    iPhone:~ root# cycript -p SpringBoard
 *    cy# [NSBundle mainBundle]
 *    #"NSBundle </System/Library/CoreServices/SpringBoard.app> (loaded)"
 *    cy# [[NSBundle mainBundle] objectForInfoDictionaryKey:@"UIBackgroundModes"];
 *    null
 *    cy#
 **/
- (instancetype)initWithLogsDirectory:(NSString *)logsDirectory defaultFileProtectionLevel:(NSString*)fileProtectionLevel;
#endif

/*
 * Methods to override.
 *
 * Log files are named "<bundle identifier> <date> <time>.log"
 * Example: com.organization.myapp 2013-12-03 17-14.log
 *
 * If you wish to change default filename, you can override following two methods.
 * - newLogFileName method would be called on new logfile creation.
 * - isLogFile: method would be called to filter logfiles from all other files in logsDirectory.
 *   You have to parse given filename and return YES if it is logFile.
 *
 * **NOTE**
 * newLogFileName returns filename. If appropriate file already exists, number would be added
 * to filename before extension. You have to handle this case in isLogFile: method.
 *
 * Example:
 * - newLogFileName returns "com.organization.myapp 2013-12-03.log",
 *   file "com.organization.myapp 2013-12-03.log" would be created.
 * - after some time "com.organization.myapp 2013-12-03.log" is archived
 * - newLogFileName again returns "com.organization.myapp 2013-12-03.log",
 *   file "com.organization.myapp 2013-12-03 2.log" would be created.
 * - after some time "com.organization.myapp 2013-12-03 1.log" is archived
 * - newLogFileName again returns "com.organization.myapp 2013-12-03.log",
 *   file "com.organization.myapp 2013-12-03 3.log" would be created.
 **/
- (NSString *)newLogFileName;
- (BOOL)isLogFile:(NSString *)fileName;

@end

@protocol UXLogFileManagerDefaultDelegate <NSObject>

- (void)processLogFileInfoDidSuccess:(UXLogFileInfo*)fileInfo;
- (void)processLogFileInfoDidFailure:(UXLogFileInfo*)fileInfo;

@end

