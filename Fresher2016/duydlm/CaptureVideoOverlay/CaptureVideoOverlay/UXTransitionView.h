/* Copyright (c) 2016-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

#define __PI 3.14159265359
#define kWidth [[UIScreen mainScreen] bounds].size.width
#define kHeight [[UIScreen mainScreen] bounds].size.height


@interface UXTransitionView : UIView

/**
 *  The rectange vectices of `UXTransitionView` frame after apply transformation
 */
@property (nonatomic) CGPoint vertexOne;
@property (nonatomic) CGPoint vertexTwo;
@property (nonatomic) CGPoint vertexThree;
@property (nonatomic) CGPoint vertexFour;

/**
 *  Width and height of `UXTransitionView` frame after apply transformation
 */
@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;

/**
 *  Red rectange border of `UXTransitionView' frame
 */
@property (nonatomic, strong) CAShapeLayer *borderLayer;

- (void)drawBorder;

@end
