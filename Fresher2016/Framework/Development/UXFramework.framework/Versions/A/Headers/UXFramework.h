/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

#define CONFIG_DEBUG_FRAMEWORK 0

//! Project version number for UXFramework.
FOUNDATION_EXPORT double UXFrameworkVersionNumber;

//! Project version string for UXFramework.
FOUNDATION_EXPORT const unsigned char UXFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <UXFramework/PublicHeader.h>

#if CONFIG_DEBUG_FRAMEWORK
// Base Classes
#import "UXDebug.h"
#import "UXState.h"
#import "UXAction.h"
#import "UXMetrics.h"
#import "UXDevice.h"
#import "UXBundle.h"
#import "UXRuntime.h"
#import "UXOperation.h"
#import "UXFoundation.h"
#import "UXNoneRetaining.h"
#import "UXSDKAvailability.h"
#import "UXNetworkActivity.h"
#import "UXObjectValidator.h"

// Object Classes
#import "UXMutableArray.h"
#import "UXMutableDictionary.h"

// UIView Classes
#import "UXSnapshot.h"
#import "UXTextField.h"
#import "UXBadgeView.h"
#import "UXViewRecycler.h"
#import "UXNavigationAppearance.h"
#import "UXImage+Extension.h"
#import "UXNetworkImageView.h"
#import "UITableView+Extension.h"

// Extension Classes
#import "UIView+Extension.h"
#import "NSString+Extension.h"
#import "UIButton+Extension.h"
#import "UINavigationItem+Extension.h"
#import "UIBarButtonItem+Extension.h"

// JSON Stylesheet Classes
#import "UXDefaultStyleSheet.h"
#import "UXFontDefaultStyleSheet.h"
#import "UXImageDefaultStyleSheet.h"

#import "UXJSONGlobalStyle.h"
#import "UXJSONStyleSheet.h"
#import "UXTextStyle+Extension.h"
#import "UXJSONDefaultStyleSheet.h"

// Table Model Classes
#import "UXModel.h"
#import "UXModelDelegate.h"
#import "UXNetworkModel.h"
#import "UXSearchModel.h"
#import "UXServiceModel.h"
#import "UXTableViewController.h"

#import "UXObjectAction.h"
#import "UXTableViewAction.h"
#import "UXTableViewCellModel.h"
#import "UXTableViewDataSource.h"
#import "UXTableViewDataSourceSection.h"
#import "UXTableViewCellFactory.h"
#import "UXMutableTableViewDataSource.h"
#import "UXTableViewCellCategory.h"

// Controller Classes
#import "UXViewController.h"
#import "UXCollectionViewController.h"

// Web Classes
#import "UXWebViewController.h"
#import "WebViewJavascriptBridge.h"
#import "WKWebViewJavascriptBridge.h"
#import "WebViewJavascriptBridgeBase.h"

// Attributed Label
#import "UXAttributedLabel.h"
#import "UXPlaceholderTextView.h"

// Paging View
#import "UXPagingScrollView.h"
#import "UXPagingScrollViewPage.h"
#import "UXPagingScrollView+Extension.h"

#import "UXTabBarView.h"
#import "UXTabBarControl.h"
#import "UXTabBarViewController.h"
#import "UXPageViewController.h"

// Cache
#import "UXMemoryCache.h"

// Privacy
#import "UIImage+ImageEffects.h"
#import "UXPrivacyDataSource.h"
#import "UXPrivacyTableViewCell.h"
#import "UXPrivacyViewController.h"
#import "UIViewController+Privacy.h"

// Launcher View Classes
#import "UXLauncherView.h"
#import "UXLauncherPageView.h"
#import "UXLauncherButtonView.h"
#import "UXLauncherViewModel.h"
#import "UXLauncherViewObject.h"
#import "UXLauncherViewController.h"

// Navigation Classes
#import "UXURLMap.h"
#import "UXURLObject.h"
#import "UXURLAction.h"
#import "UXNavigationManager.h"
#import "UXPopupViewController.h"
#import "UIViewController+Facebook.h"
#import "UIViewController+Extension.h"
#import "UITabBarController+Extension.h"

// Collection Model Classes
#import "UXCollectionViewDataSource.h"
#import "UXCollectionViewAction.h"
#import "UXCollectionViewCellFactory.h"
#import "UXCollectionViewDataSourceSection.h"
#import "UXMutableCollectionViewDataSource.h"

// Extension Classes
#import "UXError.h"
#import "UXInterApp.h"
#import "UIImage+Addition.h"

// Overview Classes
#import "UXDeviceInfo.h"
#import "UXOverviewPageView.h"
#import "UXOverviewGraphView.h"
#import "UXOverview.h"
#import "UXOverviewView.h"
#import "UXOverviewLogger.h"
#import "UXOverviewSwizzling.h"

// Photo Classes
#import "UXPhotoScrollView.h"
#import "UXPhotoScrubberView.h"
#import "UXPhotoAlbumScrollView.h"
#import "UXPhotoViewController.h"
#import "UXPhotoAlbumScrollViewDelegate.h"

// Alert Classes
#import "UXAlertController.h"
#import "UXMessageBarManager.h"

// Web Inapp browser
#import "UXWebViewController.h"

// UXStylesheet Classes
#import "UXColorExtension.h"
#import "UXBezierPathExtension.h"
#import "UXStyleDefines.h"
#import "UXView.h"
#import "UXButton.h"
#import "UXPopupView.h"
#import "UXButtonContent.h"
#import "UXStyleDelegate.h"

// Vendor
#import "MBProgressHUD.h"

#import "UXShape.h"
#import "UXShapeStyle.h"
#import "UXRectangleShape.h"
#import "UXSpeechBubbleShape.h"
#import "UXRoundedLeftArrowShape.h"
#import "UXRoundedRectangleShape.h"
#import "UXRoundedRightArrowShape.h"
#import "UXStyle.h"
#import "UXPartStyle.h"
#import "UXTextStyle.h"
#import "UXBoxStyle.h"
#import "UXMaskStyle.h"
#import "UXInsetStyle.h"
#import "UXBlendStyle.h"
#import "UXImageStyle.h"
#import "UXContentStyle.h"
#import "UXShadowStyle.h"
#import "UXSolidFillStyle.h"
#import "UXSolidBorderStyle.h"
#import "UXBevelBorderStyle.h"
#import "UXReflectiveFillStyle.h"
#import "UXFourBorderStyle.h"
#import "UXInnerShadowStyle.h"
#import "UXHighlightBorderStyle.h"
#import "UXLinearGradientFillStyle.h"
#import "UXLinearGradientBorderStyle.h"

#import "UXModelView.h"
#import "UXActivityView.h"

// CSSStylesheet Classes
/**
 #import "UXCSSRuleSet.h"
 #import "UXCSSParser.h"
 #import "UXDOM.h"
 #import "UXStyleable.h"
 #import "UXCSSStyleSheet.h"
 #import "UXStylesheetCache.h"
 #import "UXChameleonObserver.h"
 
 // Styleable UIKit views
 #import "UIView+UXStyleable.h"
 #import "UILabel+UXStyleable.h"
 #import "UIButton+UXStyleable.h"
 #import "UIToolbar+UXStyleable.h"
 #import "UITextField+UXStyleable.h"
 #import "UXTextField+UXStyleable.h"
 #import "UISearchBar+UXStyleable.h"
 #import "UIScrollView+UXStyleable.h"
 #import "UITableView+UXStyleable.h"
 #import "UINavigationBar+UXStyleable.h"
 */
#else

// Base Classes
#import <UXFramework/UXDebug.h>
#import <UXFramework/UXState.h>
#import <UXFramework/UXAction.h>
#import <UXFramework/UXMetrics.h>
#import <UXFramework/UXDevice.h>
#import <UXFramework/UXBundle.h>
#import <UXFramework/UXRuntime.h>
#import <UXFramework/UXOperation.h>
#import <UXFramework/UXFoundation.h>
#import <UXFramework/UXNoneRetaining.h>
#import <UXFramework/UXSDKAvailability.h>
#import <UXFramework/UXNetworkActivity.h>
#import <UXFramework/UXObjectValidator.h>

// Object Classes
#import <UXFramework/UXMutableArray.h>
#import <UXFramework/UXMutableDictionary.h>

// UIView Classes
#import <UXFramework/UXSnapshot.h>
#import <UXFramework/UXTextField.h>
#import <UXFramework/UXBadgeView.h>
#import <UXFramework/UXViewRecycler.h>
#import <UXFramework/UXNavigationAppearance.h>
#import <UXFramework/UXImage+Extension.h>
#import <UXFramework/UXNetworkImageView.h>
#import <UXFramework/UITableView+Extension.h>

// Extension Classes
#import <UXFramework/UIView+Extension.h>
#import <UXFramework/NSString+Extension.h>
#import <UXFramework/UIButton+Extension.h>
#import <UXFramework/UINavigationItem+Extension.h>
#import <UXFramework/UIBarButtonItem+Extension.h>

// JSON Stylesheet Classes
#import <UXFramework/UXDefaultStyleSheet.h>
#import <UXFramework/UXFontDefaultStyleSheet.h>
#import <UXFramework/UXImageDefaultStyleSheet.h>

#import <UXFramework/UXJSONGlobalStyle.h>
#import <UXFramework/UXJSONStyleSheet.h>
#import <UXFramework/UXTextStyle+Extension.h>
#import <UXFramework/UXJSONDefaultStyleSheet.h>

// Table Model Classes
#import <UXFramework/UXModel.h>
#import <UXFramework/UXModelDelegate.h>
#import <UXFramework/UXNetworkModel.h>
#import <UXFramework/UXSearchModel.h>
#import <UXFramework/UXServiceModel.h>
#import <UXFramework/UXTableViewController.h>

#import <UXFramework/UXObjectAction.h>
#import <UXFramework/UXTableViewAction.h>
#import <UXFramework/UXTableViewCellModel.h>
#import <UXFramework/UXTableViewDataSource.h>
#import <UXFramework/UXTableViewDataSourceSection.h>
#import <UXFramework/UXTableViewCellFactory.h>
#import <UXFramework/UXMutableTableViewDataSource.h>
#import <UXFramework/UXTableViewCellCategory.h>

// Controller Classes
#import <UXFramework/UXViewController.h>
#import <UXFramework/UXCollectionViewController.h>

// Web Classes
#import <UXFramework/UXWebViewController.h>
#import <UXFramework/WebViewJavascriptBridge.h>
#import <UXFramework/WKWebViewJavascriptBridge.h>
#import <UXFramework/WebViewJavascriptBridgeBase.h>

// Attributed Label
#import <UXFramework/UXAttributedLabel.h>
#import <UXFramework/UXPlaceholderTextView.h>

// Paging View
#import <UXFramework/UXPagingScrollView.h>
#import <UXFramework/UXPagingScrollViewPage.h>
#import <UXFramework/UXPagingScrollView+Extension.h>

#import <UXFramework/UXTabBarView.h>
#import <UXFramework/UXTabBarControl.h>
#import <UXFramework/UXTabBarViewController.h>
#import <UXFramework/UXPageViewController.h>

// Cache
#import <UXFramework/UXMemoryCache.h>

// Privacy
#import <UXFramework/UIImage+ImageEffects.h>
#import <UXFramework/UXPrivacyDataSource.h>
#import <UXFramework/UXPrivacyTableViewCell.h>
#import <UXFramework/UXPrivacyViewController.h>
#import <UXFramework/UIViewController+Privacy.h>

// Launcher View Classes
#import <UXFramework/UXLauncherView.h>
#import <UXFramework/UXLauncherPageView.h>
#import <UXFramework/UXLauncherButtonView.h>
#import <UXFramework/UXLauncherViewModel.h>
#import <UXFramework/UXLauncherViewObject.h>
#import <UXFramework/UXLauncherViewController.h>

// Navigation Classes
#import <UXFramework/UXURLMap.h>
#import <UXFramework/UXURLObject.h>
#import <UXFramework/UXURLAction.h>
#import <UXFramework/UXNavigationManager.h>
#import <UXFramework/UXPopupViewController.h>
#import <UXFramework/UIViewController+Facebook.h>
#import <UXFramework/UIViewController+Extension.h>
#import <UXFramework/UITabBarController+Extension.h>

// Collection Model Classes
#import <UXFramework/UXCollectionViewDataSource.h>
#import <UXFramework/UXCollectionViewAction.h>
#import <UXFramework/UXCollectionViewCellFactory.h>
#import <UXFramework/UXCollectionViewDataSourceSection.h>
#import <UXFramework/UXMutableCollectionViewDataSource.h>

// Extension Classes
#import <UXFramework/UXError.h>
#import <UXFramework/UXInterApp.h>
#import <UXFramework/UIImage+Addition.h>

// Overview Classes
#import <UXFramework/UXDeviceInfo.h>
#import <UXFramework/UXOverviewPageView.h>
#import <UXFramework/UXOverviewGraphView.h>
#import <UXFramework/UXOverview.h>
#import <UXFramework/UXOverviewView.h>
#import <UXFramework/UXOverviewLogger.h>
#import <UXFramework/UXOverviewSwizzling.h>

// Photo Classes
#import <UXFramework/UXPhotoScrollView.h>
#import <UXFramework/UXPhotoScrubberView.h>
#import <UXFramework/UXPhotoAlbumScrollView.h>
#import <UXFramework/UXPhotoViewController.h>
#import <UXFramework/UXPhotoAlbumScrollViewDelegate.h>

// Alert Classes
#import <UXFramework/UXAlertController.h>
#import <UXFramework/UXMessageBarManager.h>

// Web Inapp browser
#import <UXFramework/UXWebViewController.h>

// UXStylesheet Classes
#import <UXFramework/UXColorExtension.h>
#import <UXFramework/UXBezierPathExtension.h>
#import <UXFramework/UXStyleDefines.h>
#import <UXFramework/UXView.h>
#import <UXFramework/UXButton.h>
#import <UXFramework/UXPopupView.h>
#import <UXFramework/UXButtonContent.h>
#import <UXFramework/UXStyleDelegate.h>

// Vendor
#import <UXFramework/MBProgressHUD.h>

#import <UXFramework/UXShape.h>
#import <UXFramework/UXShapeStyle.h>
#import <UXFramework/UXRectangleShape.h>
#import <UXFramework/UXSpeechBubbleShape.h>
#import <UXFramework/UXRoundedLeftArrowShape.h>
#import <UXFramework/UXRoundedRectangleShape.h>
#import <UXFramework/UXRoundedRightArrowShape.h>
#import <UXFramework/UXStyle.h>
#import <UXFramework/UXPartStyle.h>
#import <UXFramework/UXTextStyle.h>
#import <UXFramework/UXBoxStyle.h>
#import <UXFramework/UXMaskStyle.h>
#import <UXFramework/UXInsetStyle.h>
#import <UXFramework/UXBlendStyle.h>
#import <UXFramework/UXImageStyle.h>
#import <UXFramework/UXContentStyle.h>
#import <UXFramework/UXShadowStyle.h>
#import <UXFramework/UXSolidFillStyle.h>
#import <UXFramework/UXSolidBorderStyle.h>
#import <UXFramework/UXBevelBorderStyle.h>
#import <UXFramework/UXReflectiveFillStyle.h>
#import <UXFramework/UXFourBorderStyle.h>
#import <UXFramework/UXInnerShadowStyle.h>
#import <UXFramework/UXHighlightBorderStyle.h>
#import <UXFramework/UXLinearGradientFillStyle.h>
#import <UXFramework/UXLinearGradientBorderStyle.h>

//Forked Classes
#import <UXFramework/UXFrameworkDefines.h>
#import <UXFramework/UXModelView.h>
#import <UXFramework/UXActivityView.h>

// CSSStylesheet Classes
/**
 #import <UXFramework/UXCSSRuleSet.h>
 #import <UXFramework/UXCSSParser.h>
 #import <UXFramework/UXDOM.h>
 #import <UXFramework/UXStyleable.h>
 #import <UXFramework/UXCSSStyleSheet.h>
 #import <UXFramework/UXStylesheetCache.h>
 #import <UXFramework/UXChameleonObserver.h>
 
 // Styleable UIKit views
 #import <UXFramework/UIView+UXStyleable.h>
 #import <UXFramework/UILabel+UXStyleable.h>
 #import <UXFramework/UIButton+UXStyleable.h>
 #import <UXFramework/UIToolbar+UXStyleable.h>
 #import <UXFramework/UITextField+UXStyleable.h>
 #import <UXFramework/UXTextField+UXStyleable.h>
 #import <UXFramework/UISearchBar+UXStyleable.h>
 #import <UXFramework/UIScrollView+UXStyleable.h>
 #import <UXFramework/UITableView+UXStyleable.h>
 #import <UXFramework/UINavigationBar+UXStyleable.h>
 */
#endif


