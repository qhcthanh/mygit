//
//  PhoneCell.h
//  ZaloContact
//
//  Created by qhcthanh on 6/3/16.
//  Copyright © 2016 qhcthanh. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kPhoneTypeMobile,
    kPhoneTypeHome,
    kPhoneTypeFax,
}PhoneType;

@interface PhoneCell : UITableViewCell {
    
    __weak IBOutlet UIImageView *phoneImageView;
    __weak IBOutlet UILabel *phoneLabel;
}

/**
 *  Clean cell when re render
 */
-(void)cleanUI;

/**
 *  Render cell with phonetype (Phone,Fax,Mobile) to phoneImageView and number phone to phoneLabel
 *
 *  @param type  PhoneType(Phone,Fax,Mobile) decision phoneImageView display
 *  @param phone string display in phoneLabel
 */
-(void)renderUI:(PhoneType)type phone:(NSString*)phone;
@end
