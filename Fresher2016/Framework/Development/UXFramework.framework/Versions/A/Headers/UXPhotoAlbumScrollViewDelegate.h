/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXPagingScrollView.h"

@class UXPhotoAlbumScrollView;

/**
 * The photo album scroll view delegate.
 *
 * @ingroup Photos-Protocols
 * @see UXPhotoAlbumScrollView
 */
@protocol UXPhotoAlbumScrollViewDelegate <UXPagingScrollViewDelegate>

@optional

#pragma mark Scrolling and Zooming /** @name [UXPhotoAlbumScrollViewDelegate] Scrolling and Zooming */

/**
 * The user double-tapped to zoom in or out.
 */
- (void)photoAlbumScrollView: (UXPhotoAlbumScrollView *)photoAlbumScrollView
                   didZoomIn: (BOOL)didZoomIn;


#pragma mark Data Availability /** @name [UXPhotoAlbumScrollViewDelegate] Data Availability */

/**
 * The next photo in the album has been loaded and is ready to be displayed.
 */
- (void)photoAlbumScrollViewDidLoadNextPhoto:(UXPhotoAlbumScrollView *)photoAlbumScrollView;

/**
 * The previous photo in the album has been loaded and is ready to be displayed.
 */
- (void)photoAlbumScrollViewDidLoadPreviousPhoto:(UXPhotoAlbumScrollView *)photoAlbumScrollView;

@end

