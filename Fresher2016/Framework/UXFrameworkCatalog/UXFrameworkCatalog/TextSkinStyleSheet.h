/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "SkinStyleSheet.h"

@interface SkinStyleSheet (TextSkinStyleSheet)

- (UIColor*)textFieldColor;

- (UIColor*)textLabelColor;

- (UIColor*)textPlaceHolderColor;

- (UIColor*)lineTextFieldColorNormal;

- (UIColor*)lineTextFieldColorSelected;

@end
