//
//  StickerPageViewController.m
//  CameraGPUImage
//
//  Created by qhcthanh on 6/16/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "StickerPageViewController.h"
#import "StickerCollectionViewController.h"


@interface StickerPageViewController() <UIPageViewControllerDelegate, UIPageViewControllerDataSource, StickerCollectionDelegate>
@end

@implementation StickerPageViewController {
    NSArray* stickerViewController;
}

-(void)viewDidLoad {

    [super viewDidLoad];
    
    self.delegate = self;
    self.dataSource = self;
    NSMutableArray* sticker01Array = [NSMutableArray new];
    
    for(int i = 0; i < 40; i++) {
        [sticker01Array addObject:[UIImage imageNamed:@"image.png"]];
    }
    
    NSMutableArray* sticker02Array = [NSMutableArray new];
    
    for(int i = 0; i < 30; i++) {
        [sticker02Array addObject:[UIImage imageNamed:@"sticker.png"]];
    }
    
    NSMutableArray* sticker03Array = [NSMutableArray new];
    
    for(int i = 0; i < 50; i++) {
        [sticker03Array addObject:[UIImage imageNamed:@"Aletter.png"]];
    }
    
    StickerCollectionViewController *sticker01 = [[StickerCollectionViewController alloc] init:sticker01Array];
    [sticker01 setSizeOfItem:CGSizeMake(100,80)];
    sticker01.stickerDelegate = self;
    StickerCollectionViewController *sticker02 = [[StickerCollectionViewController alloc] init:sticker02Array];
    [sticker02 setSizeOfItem:CGSizeMake(60,60)];
    sticker02.stickerDelegate = self;
    StickerCollectionViewController *sticker03 = [[StickerCollectionViewController alloc] init:sticker03Array];
    [sticker03 setSizeOfItem:CGSizeMake(120,80)];
    sticker03.stickerDelegate = self;
    
    stickerViewController = @[sticker01,sticker02,sticker03];
    if( _stickerDelegate && [_stickerDelegate respondsToSelector:@selector(numberOfViewController:)]) {
        [_stickerDelegate numberOfViewController:stickerViewController.count];
    }
    [self setViewControllers:@[sticker01]
                   direction:UIPageViewControllerNavigationDirectionForward
                    animated:NO completion:nil];
    
    NSLog(@"loaded!");
}

-(UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
    return stickerViewController[index];
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController
     viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger currentIndex = [stickerViewController indexOfObject:viewController];
    if( _stickerDelegate && [_stickerDelegate respondsToSelector:@selector(updateIndexViewController:)]) {
        [_stickerDelegate updateIndexViewController:currentIndex];
    }
    if (currentIndex == 0) {
        return nil;
    }
    --currentIndex;
    
    return [stickerViewController objectAtIndex:currentIndex];
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController
      viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger currentIndex = [stickerViewController indexOfObject:viewController];
    if( _stickerDelegate && [_stickerDelegate respondsToSelector:@selector(updateIndexViewController:)]) {
        [_stickerDelegate updateIndexViewController:currentIndex];
    }
    if (currentIndex == stickerViewController.count -1) {
        return nil;
    }
    ++currentIndex;
    
    return [stickerViewController objectAtIndex:currentIndex];
}

-(NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

-(NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

-(void)didSelectItem:(id)itemSelected  position:(CGPoint)touchPosition {
    if( _stickerDelegate && [_stickerDelegate respondsToSelector:@selector(didSelectItem:position:)]) {
        [_stickerDelegate didSelectItem:itemSelected position:touchPosition];
    }
}

@end
