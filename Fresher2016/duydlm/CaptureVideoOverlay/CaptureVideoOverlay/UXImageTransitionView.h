/* Copyright (c) 2016-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

#import "UXTransitionView.h"

@interface UXImageTransitionView : UXTransitionView

@property (weak, readonly, nullable) UIImage *image;
@property (nonatomic) CGRect contextRect;

/**
 *  Initializes an `UXImageTransitionView` object with the specified image at center position of object in superview
 *
 *  @param image The image that display on `UXImageTransitionView`
 *
 *  @return The newly-initialized image sticker
 */
- (id _Nullable)initWithImage:(UIImage * _Nonnull)image;

/**
 *  Initializes an `UXImageTransitionView` object with the specified image and position of object in superview
 *
 *  @param image    The image that display on `UXImageTransitionView`
 *  @param position The center position of `UXImageTransitionView` object on superview
 *
 *  @return The newly-initialized image sticker
 */
- (id _Nullable)initWithImage:(UIImage * _Nonnull)image atCenterPosition:(CGPoint)position;

@end
