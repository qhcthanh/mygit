/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#if defined(__cplusplus)
extern "C" {
#endif
    
    /**
     * CPU Tick since power up
     */
    uint64_t getUpTick();
    
    /**
     * Convert CPU Tick to Millisecond
     */
    uint64_t convertTickToMilliseconds(uint64_t tick);
    
    uint64_t uptimeInMillis();
    
#if defined(__cplusplus)
} // extern "C"
#endif

