/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXLogFormatter.h"

@interface UXStaticLogFormatter : NSObject <UXLogFormatter> {
}

@end
