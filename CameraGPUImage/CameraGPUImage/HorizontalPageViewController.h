//
//  ViewController11.h
//  CameraGPUImage
//
//  Created by BaoNQ on 6/24/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PageView.h"
#import "HorizontalPageView.h"

@interface HorizontalPageViewController : UIViewController <HorizontalPageViewDataSource>

@end
