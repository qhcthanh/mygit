/* Copyright (c) 2016-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "UXImageTransitionView.h"

#define kImageScaledSize 100
#define SCREEN_HEIGHT   [UIScreen mainScreen].bounds.size.height
#define SCREEN_WIDTH    [UIScreen mainScreen].bounds.size.width

static CGFloat targetWidth = 480.0;
static CGFloat targetHeight = 640.0;

@interface UXImageTransitionView  (){
    UIImageView *imageView;
}

@property (readwrite) UIImage *image;

@end

@implementation UXImageTransitionView

- (id _Nullable)initWithImage:(UIImage * _Nonnull)image{
    return [self initWithImage:image atCenterPosition:CGPointMake(kWidth/2, kHeight/2)];
}

- (id _Nullable)initWithImage:(UIImage * _Nonnull)image atCenterPosition:(CGPoint)position{
    
    CGFloat ratio = 640.0 / 480.0;
//    CGRect cr = CGRectMake(position.x - kImageScaledSize/2, (position.y - kImageScaledSize * ratio / 2), kImageScaledSize, kImageScaledSize * ratio);
    self = [super initWithFrame:CGRectMake(position.x - kImageScaledSize/2, (position.y - kImageScaledSize * ratio / 2), kImageScaledSize, kImageScaledSize * ratio)];
    
    if (self){

        self.opaque = NO;
        self.userInteractionEnabled = NO;
        self.multipleTouchEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        
        imageView = [[UIImageView alloc] initWithImage:image];
        imageView.frame = CGRectMake(0, 0, kImageScaledSize, kImageScaledSize * ratio);
        [self addSubview:imageView];
        _image = image;
        
        self.vertexOne = CGPointMake(position.x - kImageScaledSize / 2,  position.y - kImageScaledSize * ratio /2);
        self.vertexTwo = CGPointMake(position.x + kImageScaledSize / 2,  position.y - kImageScaledSize * ratio /2);
        self.vertexThree = CGPointMake(position.x + kImageScaledSize / 2,  position.y + kImageScaledSize * ratio /2);
        self.vertexFour = CGPointMake(position.x - kImageScaledSize / 2,  position.y + kImageScaledSize * ratio /2);
        self.width = kImageScaledSize;
        self.height = kImageScaledSize * ratio;
        self.contextRect = CGRectMake(self.vertexFour.x/SCREEN_WIDTH * targetWidth, targetHeight - (self.vertexFour.y/SCREEN_HEIGHT * targetHeight), self.width, self.height);
    }
    
    return self;
}

@end
