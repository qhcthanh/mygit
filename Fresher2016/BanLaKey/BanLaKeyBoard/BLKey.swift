//
//  BLKey.swift
//  BanLaKey
//
//  Created by BaoNQ on 8/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit


enum BLKeyType: Int {
    case CharacterKey = 0
    case SpecialKey = 1
}

class BLKey: UIControl {
    
    var background: BLKeyBackground!
    
    var displayView: ShapeView!
    var borderView: ShapeView?
    var underView: ShapeView?
    
    var shadowView: UIView!
    var shadowLayer: CALayer!
    
    var popupDirection: Direction?
    var popupShowed: Bool = false
    
    internal lazy var connector: BLKeyConnector = {
        var temporaryConnector: BLKeyConnector

        let cornerRadius: CGFloat
        let shadowWidth: CGFloat
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
            cornerRadius = BLThemeManager.currentTheme.buttonPadCornerRadius
            shadowWidth = BLThemeManager.currentTheme.buttonPadShadowWidth
        }
        else {
            cornerRadius = BLThemeManager.currentTheme.buttonPhoneCornerRadius
            shadowWidth = BLThemeManager.currentTheme.buttonPhoneShadowWidth
        }
        
        temporaryConnector = BLKeyConnector(cornerRadius: cornerRadius, underOffset: shadowWidth, start: self.background, end: self.popup, startConnectable: self.background, endConnectable: self.popup, startDirection: self.popupDirection!, endDirection: self.popupDirection!.opposite())
        
        temporaryConnector.layer.zPosition = -1
        
        return temporaryConnector
    }()
    
    internal lazy var popup: BLKeyBackground = {
        let cornerRadius: CGFloat
        let shadowWidth: CGFloat
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
            cornerRadius = BLThemeManager.currentTheme.buttonPadPopupCornerRadius
            shadowWidth = BLThemeManager.currentTheme.buttonPadShadowWidth
        }
        else {
            cornerRadius = BLThemeManager.currentTheme.buttonPhonePopupCornerRadius
            shadowWidth = BLThemeManager.currentTheme.buttonPhoneShadowWidth
        }
        
        let temporaryPopup = BLKeyBackground(cornerRadius: cornerRadius, underOffset: shadowWidth)

        return temporaryPopup
    }()
    
    private lazy var label: UILabel = {
        var temporaryLabel = UILabel()
        
        temporaryLabel.textAlignment = NSTextAlignment.Center
        temporaryLabel.baselineAdjustment = UIBaselineAdjustment.AlignCenters
        temporaryLabel.adjustsFontSizeToFitWidth = true
        temporaryLabel.minimumScaleFactor = CGFloat(0.1)
        temporaryLabel.userInteractionEnabled = false
        temporaryLabel.numberOfLines = 1
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
            if (UIScreen.mainScreen().interfaceOrientation() == BLInterfaceOrientation.Landscape) {
                temporaryLabel.font = UIFont.init(name: BLThemeManager.currentTheme.buttonTitleFont, size: BLThemeManager.currentTheme.buttonPadLandscapeFontSize)
            }
            else {
                temporaryLabel.font = UIFont.init(name: BLThemeManager.currentTheme.buttonTitleFont, size: BLThemeManager.currentTheme.buttonPadPortraitFontSize)
            }
        }
        else {
            if (UIScreen.mainScreen().interfaceOrientation() == BLInterfaceOrientation.Landscape) {
                temporaryLabel.font = UIFont.init(name: BLThemeManager.currentTheme.buttonTitleFont, size: BLThemeManager.currentTheme.buttonPhoneLandscapeFontSize)
            }
            else {
                temporaryLabel.font = UIFont.init(name: BLThemeManager.currentTheme.buttonTitleFont, size: BLThemeManager.currentTheme.buttonPhonePortraitFontSize)
            }
        }
        temporaryLabel.lineBreakMode = NSLineBreakMode.ByTruncatingMiddle
        
        return temporaryLabel
    }()
    
    private lazy var imageView: UIImageView = {
        var temporaryImageView = UIImageView.init()
        
        temporaryImageView.contentMode = UIViewContentMode.Center
        temporaryImageView.sizeToFit()
        
        // Set button image? Maybe!?
        
        self.addSubview(temporaryImageView)
        return temporaryImageView
    }()
    
    func getImage() -> UIImage? {
        return self.imageView.image
    }
    
    func setImage(newImage: UIImage?) {
        self.imageView.image = newImage
        //self.updateState()
    }
    
    var keyType: BLKeyType!
    
    var color: UIColor!
    
    func getTitle() -> String? {
        return self.label.text
    }
    
    func setTitle(newTitle: String?) {

        self.label.text = newTitle
        //self.popupLabel.text = newTitle
       // self.updateState()
    }
    
    var oldBounds: CGRect?
    override func layoutSubviews() {
        
        print("layoutSubviews")
        self.layoutPopupIfNeeded()
        
        let boundingBox = (self.popupShowed == true ? CGRectUnion(self.bounds, self.popup.frame) : self.bounds)
        
        if self.bounds.width == 0 || self.bounds.height == 0 {
            return
        }
        if oldBounds != nil && CGSizeEqualToSize(boundingBox.size, oldBounds!.size) {
            return
        }
        oldBounds = boundingBox
        
        super.layoutSubviews()
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        self.background.frame = self.bounds
        
        self.displayView.frame = boundingBox
        self.shadowView.frame = boundingBox
        self.borderView?.frame = boundingBox
        self.underView?.frame = boundingBox
        
        CATransaction.commit()
        
        self.refreshViews()
        
        self.label.frame = CGRectOffset(self.bounds, 0, BLThemeManager.currentTheme.buttonLabelYOffset)
        
        let imageRect: CGRect = CGRectMake(self.bounds.origin.x + self.bounds.size.width / 4, self.bounds.origin.y + self.bounds.size.height/4, self.bounds.size.width/2 , self.bounds.size.height/2)
        
        self.imageView.frame = CGRectOffset(imageRect, 0, BLThemeManager.currentTheme.buttonImageYOffset)
        self.imageView.contentMode = .ScaleAspectFit
    }
    
    func refreshViews() {
        print("refreshViews")
        self.refreshShapes()
        self.redrawShape()
        self.updateColors()
    }
    
    func redrawShape() {

    }
    
    func updateColors() {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        var shadowColor: UIColor
        
        if (self.keyType == BLKeyType.CharacterKey) {
            shadowColor = BLThemeManager.currentTheme.characterButtonShadowColor
        }
        else{
            shadowColor = BLThemeManager.currentTheme.specialButtonShadowColor
        }
        
        
        var backgroundColor: UIColor
        
        switch self.state {
        case UIControlState.Highlighted:
            if self.keyType == BLKeyType.SpecialKey {
                if (BLThemeManager.currentTheme.useSameColorForSpecialButtonHighlighted) {
                    backgroundColor = self.color
                }
                else {
                    backgroundColor = BLThemeManager.currentTheme.specialButtonHighlightedColor
                }
                self.label.textColor = BLThemeManager.currentTheme.specialButtonHighLightedTitleColor
            }
            else {
                if (BLThemeManager.currentTheme.useSameColorForCharacterButtonHighlighted) {
                    backgroundColor = self.color
                }
                else {
                    backgroundColor = BLThemeManager.currentTheme.characterButtonHightlightedColor
                }
                self.label.textColor = BLThemeManager.currentTheme.characterButtonHighLightedTitleColor
            }
            break
        
        default:
            if self.keyType == BLKeyType.SpecialKey {
                self.label.textColor = BLThemeManager.currentTheme.specialButtonTitleColor
            }
            else {
                self.label.textColor = BLThemeManager.currentTheme.characterButtonTitleColor
            }
            backgroundColor = self.color
            
            break
        }
        
        self.displayView.fillColor = backgroundColor
        
        self.underView?.fillColor = shadowColor
        
        self.borderView?.strokeColor = shadowColor
        
        if self.popupShowed {
          //  self.popupLabel.textColor = self.label.textColor
        }

//        if self.popupShowed {
//            self.displayView.fillColor = self.popupColor
//        }
        
        CATransaction.commit()
    }

    func refreshShapes() {
        
        print("refeshShpaes()")
        self.background.setNeedsLayout()
        
        self.background.layoutIfNeeded()
        if self.popupShowed {
            self.popup.layoutIfNeeded()
            self.connector.layoutIfNeeded()
        }
        
        let testPath = UIBezierPath()
        let edgePath = UIBezierPath()
        
        let unitSquare = CGRectMake(0, 0, 1, 1)
        
        // TODO: withUnder
        let addCurves = { (fromShape: BLKeyBackground?, toPath: UIBezierPath, toEdgePaths: UIBezierPath) -> Void in
            if let shape = fromShape {
                let path = shape.fillPath
                
                let translatedUnitSquare = self.displayView.convertRect(unitSquare, fromView: shape)
                
                let transformFromShapeToView = CGAffineTransformMakeTranslation(translatedUnitSquare.origin.x, translatedUnitSquare.origin.y)
                path?.applyTransform(transformFromShapeToView)
                
                if path != nil {
                    toPath.appendPath(path!)
                }
                
                if let edgePaths = shape.edgePaths {
                    
                    for (_, anEdgePath) in edgePaths.enumerate() {
                        let editablePath = anEdgePath
                        
                        editablePath.applyTransform(transformFromShapeToView)
                        toEdgePaths.appendPath(editablePath)
                    }
                }
            }
        }
        
        if self.popupShowed {
            addCurves(self.popup, testPath, edgePath)
            addCurves(self.connector, testPath, edgePath)
        }
        
        let shadowPath = UIBezierPath(CGPath: testPath.CGPath)
        
        addCurves(self.background, testPath, edgePath)
        
        let underPath = self.background.underPath
        let translatedUnitSquare = self.displayView.convertRect(unitSquare, fromView: self.background)
        let transformFromShapeToView = CGAffineTransformMakeTranslation(translatedUnitSquare.origin.x, translatedUnitSquare.origin.y)
        
        underPath?.applyTransform(transformFromShapeToView)
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        if self.popupShowed {
            self.shadowLayer.shadowPath = shadowPath.CGPath
        }
        
        self.underView?.curve = underPath
        self.displayView.curve = testPath
        self.borderView?.curve = edgePath
        
        if let borderLayer = self.borderView?.layer as? CAShapeLayer {
            borderLayer.strokeColor = UIColor.greenColor().CGColor
        }

        CATransaction.commit()
    }
    
    var popupSubview: UIView?
    let labelContentOffset: CGFloat = 4.0
    let popupSubviewContentOffset: CGFloat = 7.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
//    convenience init() {
//        self.init(frame: CGRectZero)
//    }
    
    internal func keyInitializer(title: String, popupSubview: UIView? = nil, originalPopupFrame: CGRect = CGRectZero, image: UIImage? = nil, keyType: BLKeyType = .CharacterKey, backgroundColor: UIColor = UIColor.whiteColor()) {
        
        self.color = backgroundColor
        self.keyType = keyType
        self.popupSubview = popupSubview
        self.popupDirection = nil
        
        let cornerRadius: CGFloat
        let shadowWidth: CGFloat
        let borderWidth: CGFloat
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
            cornerRadius = BLThemeManager.currentTheme.buttonPadCornerRadius
            shadowWidth = BLThemeManager.currentTheme.buttonPadShadowWidth
            borderWidth = BLThemeManager.currentTheme.buttonPadBorderWidth
        }
        else {
            cornerRadius = BLThemeManager.currentTheme.buttonPhoneCornerRadius
            shadowWidth = BLThemeManager.currentTheme.buttonPhoneShadowWidth
            borderWidth = BLThemeManager.currentTheme.buttonPhoneBorderWidth
        }
        
        self.background = BLKeyBackground.init(cornerRadius: cornerRadius, underOffset: shadowWidth)
        
        self.displayView = ShapeView()
        self.underView = ShapeView()
        self.borderView = ShapeView()
        
        self.shadowLayer = CAShapeLayer()
        self.shadowView = UIView()
        
        
        if (popupSubview != nil) {
            
            self.updatePopupFrame(originalPopupFrame)
            
            self.popupSubview = popupSubview
            self.popup.addSubview(popupSubview!)
        }
        
        self.addSubview(self.shadowView)
        self.shadowView.layer.addSublayer(self.shadowLayer)
        
        self.addSubview(self.displayView)
        if let underView = self.underView {
            self.addSubview(underView)
        }
        if let borderView = self.borderView {
            self.addSubview(borderView)
        }
        
        self.addSubview(self.background)
        self.background.addSubview(self.label)
        
        self.displayView.opaque = false
        self.underView?.opaque = false
        self.borderView?.opaque = false
        
        self.shadowLayer.shadowOpacity = Float(0.2)
        self.shadowLayer.shadowRadius = 4
        self.shadowLayer.shadowOffset = CGSizeMake(0, 3)
        
        self.borderView?.lineWidth = CGFloat(borderWidth)
        self.borderView?.fillColor = UIColor.clearColor()
        
        self.backgroundColor = UIColor.clearColor()
        
        self.setTitle(title)
        if (image != nil) {
            self.setImage(image)
        }
    }
    
    convenience init(frame: CGRect, title: String, image: UIImage? = nil, keyType: BLKeyType = .CharacterKey, backgroundColor: UIColor = UIColor.whiteColor()) {
        
        self.init(frame: frame)
        
        self.keyInitializer(title, image: image, keyType: keyType, backgroundColor: backgroundColor)
    }
    
    convenience init(frame: CGRect, title: String, originalPopupFrame: CGRect, image: UIImage? = nil, keyType: BLKeyType = .CharacterKey, backgroundColor: UIColor = UIColor.whiteColor()) {
        
        self.init(frame: frame)
        
        let popupView = self.popupSubviewInitializer(title, extendedCharacters: [])
        
        self.keyInitializer(title, popupSubview: popupView, originalPopupFrame: originalPopupFrame, image: image, keyType: keyType, backgroundColor: backgroundColor)
    }
    
    convenience init(frame: CGRect, title: String, popupSubview: UIView!, originalPopupFrame: CGRect, image: UIImage? = nil, keyType: BLKeyType = .CharacterKey, backgroundColor: UIColor = UIColor.whiteColor()) {
        
        self.init(frame: frame)
        
        self.keyInitializer(title, popupSubview: popupSubview, originalPopupFrame: originalPopupFrame, image: image, keyType: keyType, backgroundColor: backgroundColor)
    }
    
    internal func popupSubviewInitializer(title: String, extendedCharacters:[String]!) -> UIView! {
        let popupView = UIView()
        let mutableExtendedCharacters = [title] + extendedCharacters
        
        //Generate popup subview
        
        //self.popup.frame = CGRectMake(-13, -61 , 52, 55)
        //popupView.frame = CGRectMake(0, 0, self.popup.bounds.width * CGFloat(mutableExtendedCharacters.count), self.popup.bounds.height)
        
        popupView.frame = CGRectMake(0, 0, 0, 0)
        
        var index: Int = 0
        for character in mutableExtendedCharacters {
            let temporaryLabel = UILabel()
            
            temporaryLabel.textAlignment = self.label.textAlignment
            temporaryLabel.baselineAdjustment = self.label.baselineAdjustment
            temporaryLabel.adjustsFontSizeToFitWidth = self.label.adjustsFontSizeToFitWidth
            temporaryLabel.minimumScaleFactor = CGFloat(0.1)
            temporaryLabel.userInteractionEnabled = false
            temporaryLabel.numberOfLines = 1
            
            var fontSize: CGFloat
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
                if (UIScreen.mainScreen().interfaceOrientation() == BLInterfaceOrientation.Landscape) {
                    fontSize = BLThemeManager.currentTheme.buttonPadPopupLandscapeFontSize
                    temporaryLabel.font = UIFont.init(name: BLThemeManager.currentTheme.buttonTitleFont, size: fontSize)
                }
                else {
                    fontSize = BLThemeManager.currentTheme.buttonPadPopupPortraitFontSize
                    temporaryLabel.font = UIFont.init(name: BLThemeManager.currentTheme.buttonTitleFont, size: fontSize)
                }
            }
            else {
                if (UIScreen.mainScreen().interfaceOrientation() == BLInterfaceOrientation.Landscape) {
                    fontSize = BLThemeManager.currentTheme.buttonPhonePopupLandscapeFontSize
                    temporaryLabel.font = UIFont.init(name: BLThemeManager.currentTheme.buttonTitleFont, size: fontSize)
                }
                else {
                    fontSize = BLThemeManager.currentTheme.buttonPhonePopupPortraitFontSize
                    temporaryLabel.font = UIFont.init(name: BLThemeManager.currentTheme.buttonTitleFont, size: fontSize)
                }
            }
            temporaryLabel.text = character
            temporaryLabel.lineBreakMode = self.label.lineBreakMode;
            temporaryLabel.sizeToFit()
            
            temporaryLabel.frame = CGRectMake(popupView.frame.width + popupSubviewContentOffset, popupSubviewContentOffset, temporaryLabel.frame.width + 2 * labelContentOffset, fontSize + 2 * labelContentOffset)
            
            popupView.addSubview(temporaryLabel)
            
            popupView.frame = CGRectMake(0, 0, popupView.frame.width + temporaryLabel.frame.width + popupSubviewContentOffset, 2 * popupSubviewContentOffset + temporaryLabel.frame.height)
            //temporaryLabel.backgroundColor = UIColor.blueColor()
            index += 1
        }
        popupView.frame = CGRectMake(0, 0, popupView.frame.width + popupSubviewContentOffset, popupView.frame.height)
        //popupView.backgroundColor = UIColor.redColor()
        return popupView
    }
    
    convenience init(frame: CGRect, title: String, extendedCharacters:[String]!, originalPopupFrame: CGRect, image: UIImage? = nil, keyType: BLKeyType = .CharacterKey, backgroundColor: UIColor = UIColor.whiteColor()) {
        
        self.init(frame: frame)
        
        let popupView = self.popupSubviewInitializer(title, extendedCharacters: extendedCharacters)
        
        self.keyInitializer(title, popupSubview: popupView, originalPopupFrame: originalPopupFrame, image: image, keyType: keyType, backgroundColor: backgroundColor)
    }
    
    func updatePopupFrame(originalPopupFrame: CGRect) {
        let popupWidth = max(self.popupSubview!.frame.width, originalPopupFrame.size.width)
        
        self.popup.frame = CGRectMake(originalPopupFrame.origin.x, originalPopupFrame.origin.y, popupWidth, max(self.popupSubview!.frame.height, originalPopupFrame.size.height))
        
        if (popupWidth == originalPopupFrame.size.width) {
            self.popupSubview!.frame = CGRectMake((self.popup.frame.width - self.popupSubview!.frame.width)/2, (self.popup.frame.height - self.popupSubview!.frame.height)/2, self.popupSubview!.frame.width, self.popupSubview!.frame.height)
        }
    }
    
    func layoutPopupIfNeeded() {
        if self.popupShowed == true && self.popupDirection == nil {
            print("layoutPopupIfNeeded() if")
            self.shadowView.hidden = false
            self.borderView?.hidden = false
            
            // Configure popup
            self.popupDirection = Direction.Up

            self.background.attach(self.popupDirection)
            self.popup.attach(self.popupDirection!.opposite())
            
            
            let kv = self.background
            let p = self.popup
            
            let cornerRadius: CGFloat
            let shadowWidth: CGFloat
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
                cornerRadius = BLThemeManager.currentTheme.buttonPadPopupCornerRadius
                shadowWidth = BLThemeManager.currentTheme.buttonPadShadowWidth
            }
            else {
                cornerRadius = BLThemeManager.currentTheme.buttonPhonePopupCornerRadius
                shadowWidth = BLThemeManager.currentTheme.buttonPhoneShadowWidth
            }
            print(self.popup.frame)
            print(self.frame)
            self.connector = BLKeyConnector(cornerRadius: cornerRadius, underOffset: shadowWidth, start: kv, end: p, startConnectable: kv, endConnectable: p, startDirection: self.popupDirection!, endDirection: self.popupDirection!.opposite())
            self.addSubview(self.connector)
            
            self.willShowPopup(self.popupDirection!)
        }
        else {
            print("layoutPopupIfNeeded() else")

            self.shadowView.hidden = true
            self.borderView?.hidden = true
        }
    }

    func showPopup() {
        if self.popupShowed == false {
            print(self.background.frame)
            print("showpopup()")
            self.layer.zPosition = 1000
            
            //self.popup.addSubview(self.popupLabel)
            self.addSubview(self.popup)
            
            self.popupShowed = true;
            self.label.hidden = self.popupShowed

        }
    }
    
    func hidePopup() {
        if self.popupShowed == true {
            
            self.connector.removeFromSuperview()
            
            self.popup.removeFromSuperview()
            
            self.popupShowed = false
            self.label.hidden = self.popupShowed
            self.background.attach(nil)
            
            self.layer.zPosition = 0
            
            self.popupDirection = nil
        }
    }
    
    func willShowPopup(direction: Direction) {
        if self.popupShowed {
            let actualSuperview = (self.superview!.superview != nil ? self.superview!.superview! : self.superview)
            
            var localFrame = actualSuperview!.convertRect(self.popup.frame, fromView: self.popup.superview)
            
            if localFrame.origin.y < 3 {
                localFrame.origin.y = 3
                
                self.background.attached = Direction.Down
                self.connector.startDir = Direction.Down
                self.background.hideDirectionIsOpposite = true
            }
            else {
                self.background.hideDirectionIsOpposite = false
            }
            
            if localFrame.origin.x < 3 {
                localFrame.origin.x = self.frame.origin.x
            }
            
            if localFrame.origin.x + localFrame.width > superview!.bounds.width - 3 {
                localFrame.origin.x = self.frame.origin.x + self.frame.width - localFrame.width
            }
            
            self.popup.frame = actualSuperview!.convertRect(localFrame, toView: self.popup.superview)
        }
    }

    
    func updateState() {
        // Update state
        self.setNeedsDisplay()
    }
    
    override var selected: Bool {
//        get {
//            self.updateState()
//            return self.selected
//        }
        willSet {
            self.updateState()
        }
    }
    
//    override func drawRect(rect: CGRect) {
//        super.drawRect(rect)
//        
//        var shadowColor: UIColor
//        
//        if (self.keyType == BLKeyType.CharacterKey) {
//            shadowColor = BLThemeManager.currentTheme.characterButtonShadowColor
//        }
//        else{
//            shadowColor = BLThemeManager.currentTheme.specialButtonShadowColor
//        }
//        
//        
//        var backgroundColor: UIColor
//        
//        switch self.state {
//        case UIControlState.Highlighted:
//            if self.keyType == BLKeyType.SpecialKey {
//                if (BLThemeManager.currentTheme.useSameColorForSpecialButtonHighlighted) {
//                    backgroundColor = self.color
//                }
//                else {
//                    backgroundColor = BLThemeManager.currentTheme.specialButtonHighlightedColor
//                }
//                self.label.textColor = BLThemeManager.currentTheme.specialButtonHighLightedTitleColor
//            }
//            else {
//                if (BLThemeManager.currentTheme.useSameColorForCharacterButtonHighlighted) {
//                    backgroundColor = self.color
//                }
//                else {
//                    backgroundColor = BLThemeManager.currentTheme.characterButtonHightlightedColor
//                }
//                self.label.textColor = BLThemeManager.currentTheme.characterButtonHighLightedTitleColor
//            }
//            
//            break
//        default:
//            if self.keyType == BLKeyType.SpecialKey {
//                self.label.textColor = BLThemeManager.currentTheme.specialButtonTitleColor
//            }
//            else {
//                self.label.textColor = BLThemeManager.currentTheme.characterButtonTitleColor
//            }
//            backgroundColor = self.color
//            
//            break
//        }
//        
//        self.drawKeyRect(rect, color: backgroundColor, shadowColor: shadowColor)
//    }

//    func drawKeyRect(rect: CGRect, color: UIColor, shadowColor: UIColor) {
//        let shadowRect: CGRect = CGRectOffset(CGRectInset(rect, 0, BLThemeManager.currentTheme.buttonShadowYOffset), 0, BLThemeManager.currentTheme.buttonShadowYOffset)
//        let shadowPath: UIBezierPath = UIBezierPath.init()
//        
//        let cornerRadius: CGFloat
//        
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
//            cornerRadius = BLThemeManager.currentTheme.buttonPadCornerRadius
//        }
//        else {
//            cornerRadius = BLThemeManager.currentTheme.buttonPhoneCornerRadius
//        }
//        
//        // Bottom-left 1
//        shadowPath.moveToPoint(CGPointMake(0.0, shadowRect.size.height - cornerRadius))
//        // Bottom - left 2
//        shadowPath.addCurveToPoint(CGPointMake(cornerRadius, shadowRect.size.height),
//                                   controlPoint1: CGPointMake(0.0, shadowRect.size.height - cornerRadius/2),
//                                   controlPoint2: CGPointMake(cornerRadius/2, shadowRect.size.height))
//        
//        // Bottom-right 1
//        shadowPath.addLineToPoint(CGPointMake(shadowRect.size.width - cornerRadius, shadowRect.size.height))
//        // Bottom-right 2
//        shadowPath.addCurveToPoint(CGPointMake(shadowRect.size.width, shadowRect.size.height - cornerRadius),
//                                   controlPoint1: CGPointMake(shadowRect.size.width - cornerRadius/2, shadowRect.size.height),
//                                   controlPoint2: CGPointMake(shadowRect.size.width, shadowRect.size.height - cornerRadius/2))
//        
//        // Top-right 1
//        shadowPath.addLineToPoint(CGPointMake(shadowRect.size.width, shadowRect.size.height - cornerRadius - BLThemeManager.currentTheme.buttonShadowYOffset))
//        // Top-right 2
//        shadowPath.addCurveToPoint(CGPointMake(shadowRect.size.width - cornerRadius, shadowRect.size.height - BLThemeManager.currentTheme.buttonShadowYOffset),
//                                   controlPoint1: CGPointMake(shadowRect.size.width, shadowRect.size.height - BLThemeManager.currentTheme.buttonShadowYOffset - cornerRadius/2),
//                                   controlPoint2: CGPointMake(shadowRect.size.width - cornerRadius/2, shadowRect.size.height - BLThemeManager.currentTheme.buttonShadowYOffset))
//        
//        // Top-left 1
//        shadowPath.addLineToPoint(CGPointMake(cornerRadius, shadowRect.size.height - BLThemeManager.currentTheme.buttonShadowYOffset))
//        // Top-left 2
//        shadowPath.addCurveToPoint(CGPointMake(0.0, shadowRect.size.height - cornerRadius - BLThemeManager.currentTheme.buttonShadowYOffset),
//                                   controlPoint1: CGPointMake(cornerRadius/2, shadowRect.size.height - BLThemeManager.currentTheme.buttonShadowYOffset),
//                                   controlPoint2: CGPointMake(0.0, shadowRect.size.height - cornerRadius/2 - BLThemeManager.currentTheme.buttonShadowYOffset))
//        
//        // Bottom-left 1
//        shadowPath.addLineToPoint(CGPointMake(0.0, shadowRect.size.height - cornerRadius))
//        
//        shadowPath.closePath()
//        shadowPath.applyTransform(CGAffineTransformMakeTranslation(0, BLThemeManager.currentTheme.buttonShadowYOffset * 2))
//        shadowColor.setFill()
//        shadowPath.fill()
//        
//        let keyRect: CGRect = CGRectOffset(CGRectInset(rect, 0, BLThemeManager.currentTheme.buttonShadowYOffset/2), 0, -BLThemeManager.currentTheme.buttonShadowYOffset/2)
//        let keyPath: UIBezierPath = UIBezierPath.init(roundedRect: keyRect, cornerRadius: cornerRadius)
//        
//        color.setFill()
//        keyPath.fill()
//    }
    
//    override func systemLayoutSizeFittingSize(targetSize: CGSize) -> CGSize {
//        return UILayoutFittingExpandedSize
//    }
//    
////    override func layoutSubviews() {
////        super.layoutSubviews()
////        
////        self.label.frame = CGRectOffset(self.bounds, 0, BLThemeManager.currentTheme.buttonLabelYOffset)
////        
////        let imageRect: CGRect = CGRectMake(self.bounds.origin.x + self.bounds.size.width / 4, self.bounds.origin.y + self.bounds.size.height/4, self.bounds.size.width/2 , self.bounds.size.height/2)
////        
////        self.imageView.frame = CGRectOffset(imageRect, 0, BLThemeManager.currentTheme.buttonImageYOffset)
////        self.imageView.contentMode = .ScaleAspectFit
////        
////        self.setNeedsDisplay()
////    }
//    
//    override func sizeThatFits(size: CGSize) -> CGSize {
//        let rect: CGSize = CGSizeMake(max(self.intrinsicContentSize().width, size.width), max(self.intrinsicContentSize().height, size.height))
//        
//        return rect
//    }
//    
//    override func intrinsicContentSize() -> CGSize {
//        return CGSizeMake(20.0, 20.0)
//    }
//    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

