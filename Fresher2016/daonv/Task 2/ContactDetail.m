//
//  ContactLoadDetail.m
//  ZaloContactInviting
//
//  Created by VanDao on 5/26/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import "ContactDetail.h"

@implementation ContactDetail

- (id)init{
    self = [super init];
    if (self){
        self.phoneNumbers = [[NSMutableArray alloc]init];
    }
    
    return self;
}

- (NSString * _Nonnull)getFullName{
    
    NSString *fullName = @"";
    
    // Check if person don't have given name
    if ([_givenName length] > 0){
        fullName = [fullName stringByAppendingString:_givenName];
        fullName = [fullName stringByAppendingString:@" "];
    }
    
    // Check if person don't have family name
    if ([_familyName length] > 0){
        fullName = [fullName stringByAppendingString:_familyName];
    }
    
    if ([fullName isEqualToString:@""])
        return @" ";
    else
        return fullName;
}

@end
