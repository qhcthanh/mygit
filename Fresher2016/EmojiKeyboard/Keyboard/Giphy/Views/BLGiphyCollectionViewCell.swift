//
//  BLGiphyCollectionViewCell.swift
//  EmojiKeyboard
//
//  Created by Quach Ha Chan Thanh on 8/23/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import UIKit

@objc public protocol BLGiphyCollectionViewCellDelegate {
    
    func didSelectViewCell(viewCell: BLGiphyCollectionViewCell)
    
    func didReviewViewCell(viewCell: BLGiphyCollectionViewCell)
    
    func didEndReviewViewCell(viewCell: BLGiphyCollectionViewCell)
}

public class BLGiphyCollectionViewCell: UICollectionViewCell {
    
    public var gifView: BLGifView!
    public var indicatorView: UIActivityIndicatorView!
    
    public var showAnimated: Bool = true
    public var isDisplay: Bool = true
    public weak var giphyRendition: GiphyRendition?
    public weak var delegate: BLGiphyCollectionViewCellDelegate?
    
    public convenience init() {
        self.init()
        
        self.setupView()
    }
    
    /**
     Setup subview in BLGiphyCollectionViewCell
     */
    func setupView() {
        // GifView
        self.gifView = BLGifView(frame: CGRectZero)
        self.gifView.hidden = true
        self.gifView.userInteractionEnabled = true
        
        self.contentView.addSubview(self.gifView)
        
        // Setup Gesture
        self.setupGestureGifView()
        
        // Initial indicatorView
        self.indicatorView = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
//        self.indicatorView.hidesWhenStopped = true
        
        self.contentView.addSubview(self.indicatorView)
        
        // Setup Constraint
        self.gifView.constrain(toEdgesOf: self.contentView)
        self.indicatorView.constrain(toEdgesOf: self.contentView)
        
        // SetupView
        self.clipsToBounds = true
    }
    
    /**
     Setup Gesture to gifView. The gesture will catch the select Gif and review-endreview Gif
     */
    private func setupGestureGifView() {
        
        let selectGesture = UILongPressGestureRecognizer(target:self , action:  #selector(self.onTapGiphyGesture(_:)))
        selectGesture.minimumPressDuration = 0.1
        self.gifView.addGestureRecognizer(selectGesture)
        
        let reviewGesture = UILongPressGestureRecognizer(target:self , action:  #selector(self.onLongTapGiphyGestrue(_:)))
        reviewGesture.minimumPressDuration = 1
        self.gifView.addGestureRecognizer(reviewGesture)
        
        // The selectGesture will be received when reviewGesture fail 
        selectGesture.requireGestureRecognizerToFail(reviewGesture)
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupView()
    }
    
    /**
     Binding UI with GifRendition
     
     - parameter giphyRendition: The GiphyRendition to binding with imageGif
     */
    func bindingUI(giphyRendition: GiphyRendition) {
        if gifView == nil {
            self.setupView()
        }
        
        self.giphyRendition = giphyRendition

        self.indicatorView.startAnimating()
        
        self.loadThumbGIF()
    }
    
    public func loadThumbGIF() {
        if let giphyRendition = self.giphyRendition {
            let image = BLGIFCache.shareManager().thumbForKey(giphyRendition.getID())
            self.gifView.image = image
        }
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        
        if gifView != nil {
            self.gifView.cleanGIF()
            self.gifView.image = nil
        }
    }
    
    
    public func onVisableCell() {
        
        if let giphyRendition = self.giphyRendition {
            
            self.gifView.stopDisplay()
            self.loadThumbGIF()
            self.gifView.setGifImage(NSURL(string:giphyRendition.url)!, gifIdentifier: giphyRendition.getID(), gifPlaceholder: nil, manager: BLDisplayGifManager.defaultManager, loopCount: -1, success: {
                identifier, gif in
                
                if identifier == self.giphyRendition?.getID() {
                    dispatch_async(dispatch_get_main_queue(), {
                        if let gif = gif {
                            self.gifView.hidden = false
                            self.indicatorView.stopAnimating()
                            self.indicatorView.hidden = true
                            
                            self.gifView.setGifImage(gif)
                        }
                    })
                }
            })
        }
    }
    
    func onInvisableCell() {
        if gifView != nil {
            self.gifView.cleanGIF()
            self.gifView.cancelImageDownloadTask()
            
            self.loadThumbGIF()
        }
    }
    
    func onTapGiphyGesture(gesture: UILongPressGestureRecognizer) {
        
        if gesture.state == .Began {
            if let delegate = self.delegate {
                delegate.didSelectViewCell(self)
                
            }
        }
        
    }
    
    func onLongTapGiphyGestrue(gesture: UILongPressGestureRecognizer) {
        
        if let delegate = self.delegate {
            if gesture.state == .Began {
                
                delegate.didReviewViewCell(self)
            } else if gesture.state == .Ended {
                
                delegate.didEndReviewViewCell(self)
            }
        }
    }
    
}
