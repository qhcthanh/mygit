/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */


#import "AppDelegate.h"

#import <UXFramework/UXFramework.h>

#import "SkinStyleSheetManager.h"
#import "BadgeViewController.h"
#import "StoreKitViewController.h"
#import "CatalogTableViewController.h"
#import "IBAttributedLabelViewController.h"
#import "CustomNibCollectionModelViewController.h"

const int kClientVersionInfo = 1.0;

#define kIBAttributedLabelViewController @"ux://IBAttributedLabelViewController"
#define kCustomNibCollectionModelViewController @"ux://CustomNibCollectionModelViewController"

#define kCatalogTableViewController @"ux://CatalogTableViewController"

#define kBadgeViewController @"ux://BadgeViewController"
#define kBadgeViewController2 @"ux://BadgeViewController/(initWithBool:)"

#define USE_NAVIGATION_MANAGER 1

int UXLogLevel = 1;

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [[SkinStyleSheetManager manager] loadSkin];
    
#if USE_NAVIGATION_MANAGER
    //[UXOverview applicationDidFinishLaunching];
    self.window = [UIWindow new];
    [self.window makeKeyAndVisible];
    self.window.frame = [[UIScreen mainScreen] bounds];
    
    UXNavigationManager* navigator = [UXNavigationManager navigator];
    navigator.window = self.window;
    UXURLMap* map = navigator.URLMap;
    
    [map from:@"*" toViewController:[UXWebViewController class]];
    [map from:kCatalogTableViewController toViewController:[CatalogTableViewController class]];
    [map from:kBadgeViewController toViewController:[BadgeViewController class]];
    [map from:kBadgeViewController2 toModalViewController:[BadgeViewController class]];
    [map from:kIBAttributedLabelViewController toViewController:[IBAttributedLabelViewController class]];
    [map from:kCustomNibCollectionModelViewController toViewController:[CustomNibCollectionModelViewController class]];
    //
    // Open Category
    [navigator openURLAction:[UXURLAction actionWithURLPath:kCatalogTableViewController]];
    
    //[UXOverview addOverviewToWindow:self.window];
#else
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    StoreKitViewController* storeKitViewController = [[StoreKitViewController alloc] init];
    self.window.rootViewController = [[UXNavigationController alloc] initWithRootViewController:storeKitViewController];
    [self.window makeKeyAndVisible];
    
    [[UXStoreKit sharedKit] startProductRequest];
    [[NSNotificationCenter defaultCenter] addObserverForName:kUXStoreKitProductsAvailableNotification
                                                      object:nil
                                                       queue:[[NSOperationQueue alloc] init]
                                                  usingBlock:^(NSNotification *note) {
                                                      
                                                      NSLog(@"Products available: %@", [[UXStoreKit sharedKit] availableProducts]);
                                                  }];
#endif

    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
