//
//  BLEmojiCategory.swift
//  BanLaKey
//
//  Created by Quach Ha Chan Thanh on 9/4/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import Foundation

public class BLEmojiCategory: NSObject {
    
    var category: String!
    var emojis: [BLEmojiInfo]!
    var priority: Int!
    var represent: String!
    
    lazy var emojiDefault: String = {
        return self.emojis.first!.char
    }()
    
    init(category: String, jsonData: NSDictionary) {
        super.init()
        
        self.emojis = [BLEmojiInfo]()
        self.category = category
        
        if let priority = jsonData["priority"] as? Int,
            let represent = jsonData["represent"] as? String,
            let arrayData = jsonData["emojis"] as? NSArray
        {
            self.priority = priority
            self.represent = represent
            
            for emojiDictionary in arrayData {
                if let emojiDictionary = emojiDictionary as? NSDictionary,
                    let name = emojiDictionary["name"] as? String,
                    let char = emojiDictionary["char"] as? String,
                    let keywords = emojiDictionary["keywords"] as? [String]
                {
                    let emoji = BLEmojiInfo(name: name,keywords: keywords, char: char)
                    self.emojis.append(emoji)
                }
            }
            emojis.sortInPlace({
                if $0.char < $1.char {
                    return false
                }
                return true
            })
            
        }
        
        
    }
    
}
