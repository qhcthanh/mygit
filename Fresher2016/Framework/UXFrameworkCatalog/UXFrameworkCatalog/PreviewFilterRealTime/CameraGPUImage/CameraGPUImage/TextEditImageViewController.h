//
//  TextEditImageViewController.h
//  CameraGPUImage
//
//  Created by qhcthanh on 6/16/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PreviewImageViewController.h"
#import "TextStickerNode.h"



@protocol TextEditControlDelegate <NSObject>

@optional
-(void) didFinishEditText:(TextStickerNode* _Nonnull)newSticker;
@end

@interface TextEditImageViewController : UIViewController
-(void)beginEditText;
-(void)endEditText;

@property (nonnull) PreviewImageViewController* previewVC;
@property (nullable, nonatomic) id<TextEditControlDelegate> textDelegate;
@property (weak, nonatomic, nullable) IBOutlet UITextView *textEditImageView;
@property (weak, nullable, nonatomic) TextStickerNode* currentTextSticker;

@end
