/* Copyright (c) 2016-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

#import "UXTransitionView.h"

@interface UXTextTransitionView : UXTransitionView

@property(weak, readonly, nullable) NSString *textContent;
@property(weak, readonly, nullable) UIFont *textFont;
@property(readonly) CGFloat colorValue;
/**
 *  Initializes an `UXTextTransitionView` object with the specified content at center position in superview
 *
 *  @param content The content that displayed on `UXTextTransitonView`
 *
 *  @return The newly-initialized text sticker
 */
- (id _Nullable)initWithText:(NSString * _Nonnull)text colorValue:(CGFloat)colorValue font:(UIFont * _Nullable)font;

/**
 *  Initializes an `UXTextTransitionView` object with the specified content and position in superview
 *
 *  @param content  The content that displayed on `UXTextTransitonView`
 *  @param position The center position of `UXTextTransitionView` object on superview
 *
 *  @return The newly-initialzed text sticker
 */
- (id _Nullable)initWithText:(NSString * _Nonnull)text colorValue:(CGFloat)colorValue font:(UIFont * _Nullable)font position:(CGPoint)position;

/**
 *  Change content of `UXTextTransitionView`
 *
 *  @param content The content of `UXTextTransitionView'
 *  @param color   The color of content
 *  @param font    The font of content
 */
- (void)updateText:(NSString * _Nullable)text colorValue:(CGFloat)colorValue font:(UIFont * _Nullable)font;

/**
 *  Update new frame in content label
 *
 *  @param frame will update
 */
-(void)updateFrameContentLabel:(CGRect)frame;
@end
