/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@interface NSData (Security)

- (NSString*)stringToHexBytes;
- (NSString*)stringWithHexBytes;

- (NSData*)encryptString:(NSString*)plaintext withKey:(NSString*)key withIV:(NSString*)iv;
- (NSString*)decryptData:(NSData*)ciphertext withKey:(NSString*)key withIV:(NSString*)iv;

- (NSData*)AES128EncryptWithKey:(NSString *)key withIV:(NSString*)iv;
- (NSData*)AES128DecryptWithKey:(NSString *)key withIV:(NSString*)iv;

- (NSData*)base64DecodeString:(NSString*)string;
- (NSString*)base64EncodeData:(NSData*)dataToConvert;

- (NSData *)XOREncryptWithKeyString:(NSString* )key;
- (NSData *)XORDecryptWithKeyString:(NSString* )key;

- (NSData *)XOREncryptWithKeyData:(NSData* )data;
- (NSData *)XORDecryptWithKeyData:(NSData* )data;

@end
