/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

@interface UXModelView : UIView

- (instancetype)initWithFrame:(CGRect)frame withImage:(UIImage*)image  withTitle:(NSString*)title withSubTitle:(NSString*)subTitle withRetryButton:(NSString*)buttonTitle withCompletionHandler:(void(^)())completion;

- (void)updateTitle:(NSString*)title withSubTitle:(NSString*)subTitle;

@end
