/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <UXFramework/UXFramework.h>

@class UXBadgeView;

/**
 * A subclass of NILauncherViewObject that provides badging support.
 *
 * Adding this type of object to a UXLauncherViewModel will create a BadgedLauncherButtonView view.
 */

@interface BadgedLauncherViewObject : UXLauncherViewObject
@property (nonatomic, assign) NSInteger badgeNumber;
- (id)initWithTitle:(NSString *)title image:(UIImage *)image badgeNumber:(NSInteger)badgeNumber;
+ (id)objectWithTitle:(NSString *)title image:(UIImage *)image badgeNumber:(NSInteger)badgeNumber;
@end

/**
 * A launcher button view that displays a badge number.
 *
 * The badge is hidden if the number is 0.
 * The badge displays 99+ if the number is greater than 99.
 */
@interface BadgedLauncherButtonView : UXLauncherButtonView
@property (nonatomic, retain) UXBadgeView* badgeView;
@end
