//
//  ZaloContactTests.m
//  ZaloContactTests
//
//  Created by qhcthanh on 6/4/16.
//  Copyright © 2016 qhcthanh. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CTAddressBook.h"
#import "CTContactManager.h"

@interface ZaloContactTests : XCTestCase

@end

@implementation ZaloContactTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}


-(void)testLoadAllContactWithMultithread {
    int numberThread = 20;
    dispatch_group_t loadContactGroup = dispatch_group_create();
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    for(int i = 0 ;i<numberThread;i++) {
        dispatch_group_enter(loadContactGroup);
        dispatch_async(queue, ^{
            [[CTContactManager sharedManager] getAllContactOfDevice:^(NSMutableArray* contactArray, CTError* error) {
                if (error != nil)
                    NSLog(@"%u\n",*error);
                else {
                    NSLog(@"Load comple with: %lu contact",(unsigned long)contactArray.count);
                }
                dispatch_group_leave(loadContactGroup);
            } queue:queue];
        });
    }
    
    dispatch_group_wait(loadContactGroup, DISPATCH_TIME_FOREVER);
    NSLog(@"\n---------------Load Complete");
    
}

-(void)testCacheImage {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_group_t loadContactGroup = dispatch_group_create();
    dispatch_group_enter(loadContactGroup);
    __block NSMutableArray* contacts = nil;
    
    [[CTContactManager sharedManager] getAllContactOfDevice:^(NSMutableArray* contactArray, CTError* error) {
        if (error != nil)
            NSLog(@"%u\n",*error);
        else {
            NSLog(@"Load comple with: %lu contact",(unsigned long)contactArray.count);
            dispatch_async(dispatch_queue_create("LoadImage", DISPATCH_QUEUE_CONCURRENT), ^{
                for(int i = 0 ;i< contactArray.count; i++) {
                    [contactArray[i] getContactImage:nil];
                }
            });
            contacts = contactArray;
        }
        dispatch_group_leave(loadContactGroup);
    } queue:queue];
    
    dispatch_group_wait(loadContactGroup, DISPATCH_TIME_FOREVER);
    NSLog(@"\n---------------Load Complete");
    int countImage = 0;
    for(int i = 0 ;i<contacts.count;i++) {
        CTAddressBook* contact = contacts[i];
        // has image
        if([[CTCacheImageManager shareManager] getImageInCacheWithIdentifield: contact.mIdentifield]) {
            countImage++;
        } else {
            //NSLog(@"Hasn't Image in Identifile: %@",contact.mIdentifield);
        }
    }
    NSLog(@"-------Has %d image",countImage);
    
}



@end
