/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@class QoSSimplePing;
typedef void (^SimplePingWithSuccessBlock)(QoSSimplePing* simplePing, id responseObject);
typedef void (^SimplePingWithFailureBlock)(QoSSimplePing* simplePing, NSError *error);

@interface QoSSimplePing : NSObject {
}

@property (nonatomic, copy) SimplePingWithSuccessBlock simplePingWithSuccessBlock;
@property (nonatomic, copy) SimplePingWithFailureBlock simplePingWithFailureBlock;

- (void)pingGoogleWithSuccess:(void(^)(QoSSimplePing* simplePing, id responseObject))success failure:(void(^)(QoSSimplePing* simplePing, NSError* error))failure;

@end
