/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

#import "UXPrivacyDataSource.h"

/**
 *  Dismiss completion block
 */
typedef void (^UXDismissCompletionHandler)(BOOL cancel);
typedef void (^UXPresentCompletionHandler)();

@interface UXPrivacyViewController : UIViewController

+ (instancetype)controllerWithPrivacyType:(UXPrivacyType)type;

/**
 *  The privacy type of controller
 */
@property (nonatomic, readonly) UXPrivacyType privacyType;

/**
 *  Set the status bar style
 */
@property (nonatomic, assign) UIStatusBarStyle statusBarStyle;

/**
 *  Set if the controller can rotate
 */
@property (nonatomic, assign) BOOL canRotate;

/**
 *  The close button
 */
@property (nonatomic, readonly) UIButton *closeButton;

/**
 *  The snapshot of the window
 */
@property (nonatomic, strong) UIImage *snapshot;

/**
 *  Personal App Icon name for DBPrivacyTypeNotifications
 */
@property (nonatomic, strong) NSString *appIcon;

/**
 *  The dismiss completion block
 */
@property (nonatomic, copy) UXDismissCompletionHandler completionHandler;

@end
