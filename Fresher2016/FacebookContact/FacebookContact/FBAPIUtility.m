//
//  FBRequestManager.m
//  FacebookContact
//
//  Created by BaoNQ on 7/14/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import "FBAPIUtility.h"
#import "NSString+encoding.h"

#define kBackgroundSessionConfigurationID @"backgroundSessionConfiguration"
#define kRootURL @"https://graph.facebook.com/v2.7/"
#define kTokenKey @"EAACEdEose0cBACkbGJow8JnMN6UEZBJmvAFoJ4bjtQIqVSE0U74IT26IRT4JvYaJZBJPDblVko2TQj9kYA16C0ZCaZA4RGxQuOlqzKqQzYUodZCHh8klLEipZCGZCZAfWa4tZCerBcZB3FSh60d0zMUX6Cs6W3e9IVxZAfenffAzdLaNQZDZD"


@interface FBAPIUtility ()

@property (readwrite, nonatomic, strong) FBSessionManager *sessionManager;

@end

@implementation FBAPIUtility

- (id)init {
    self = [super init];
    if (!self)
        return nil;
    // Create a session manager use defaultSessionConfiguration.
    self.sessionManager = [[FBSessionManager alloc] init];
    return self;
}

- (id)initWithSessionManager:(FBSessionManager *)sessionManager {
    self = [super init];
    if (!self)
        return nil;
    self.sessionManager = sessionManager;
    return self;
}

- (INTEGER_T)requestGraphDataWithGraphPath:(NSString *)graphPath parameters:(NSDictionary *)params completionHandler:(RequestGraphDataWithGraphPathCompletionHandler)completionBlock {
    
    NSString *key = params.allKeys[0];
    NSString *urlString = [NSString stringWithFormat:@"%@%@?access_token=%@&%@=%@", kRootURL, graphPath, kTokenKey, key, [[params objectForKey:key] URLEncoding]];
    
    NSURL *url = [NSURL URLWithString:urlString];
    return [self requestDataWithURL:url completionHandler:^(NSData *result, NSURLResponse *response, NSError *error) {
        if (completionBlock) {
            completionBlock(result, response, error);
        }
    }];
}

- (INTEGER_T)requestDataWithURL:(NSURL *)url completionHandler:(RequestDataWithURLCompletionHandler)completionBlock {
    
    return [self.sessionManager startDataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (completionBlock) {
            completionBlock(data, response, error);
        }
    }];
}

+ (BOOL)isUserLoginFacebook {
    if([FBSDKAccessToken currentAccessToken]) {
        return true;
    }
    
    return false;
}

+ (NSString*)getCurrentAccessTokenKey {
    if([FBAPIUtility isUserLoginFacebook]) {
        return [FBSDKAccessToken currentAccessToken].tokenString;
    }
    
    return nil;
}

+ (void)logInFacebookWithReadPermissions:(NSArray *)permissions
              fromViewController:(UIViewController *)fromViewController
                                 handler:(LoginFacebookCompletionHanlder)handler {
    FBSDKLoginManager* loginManager = [FBSDKLoginManager new];
    [loginManager logInWithReadPermissions:permissions fromViewController:fromViewController handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        handler(result, error);
    }];
}

+ (void)logOutFacebook {
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logOut];
}


@end
