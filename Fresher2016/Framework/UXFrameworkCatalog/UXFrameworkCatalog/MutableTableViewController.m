/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "MutableTableViewController.h"

@interface MutableTableViewController ()<UXMutableTableViewDataSourceDelegate>
@property (nonatomic, strong) UXMutableTableViewDataSource* model;
@property (nonatomic, strong) UXMutableTableViewDataSource* actions;
@property (nonatomic, strong) NSIndexPath* indexPathForDeletion;
@end

@implementation MutableTableViewController

- (id)initWithStyle:(UITableViewStyle)style {
    if ((self = [super initWithStyle:UITableViewStyleGrouped])) {//UITableViewStyleGrouped
        self.titleString = @"Mutable Models";
        
        // In order to be able to modify a model we must create an instance of UXMutableTableViewDataSource.
        // This object differs from NITableViewModel in that it exposes methods for modifying the
        // contents of the model, similarly to the differences between NSArray and NSMutableArray.
        _model = [[UXMutableTableViewDataSource alloc] initWithDelegate:self];
        
        // We are going to show how to recompile the section index so we provide the settings here.
        [_model setSectionIndexType:UXTableViewDataSourceSectionIndexDynamic
                        showsSearch:NO
                       showsSummary:NO];
        
        // By tapping this button we'll add a new section to the model.
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(didAddMoreObject:)];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = _model;
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
//    return UXIsSupportedOrientation(toInterfaceOrientation);
//}

- (NSString *)randomName {
    NSMutableString *name = [[NSMutableString alloc] init];
    for (NSInteger ix = 0; ix < arc4random_uniform(10) + 5; ++ix) {
        [name appendFormat:@"%c", arc4random_uniform('z'-'a')+'a'];
    }
    return [name capitalizedString];
}

- (void)didAddMoreObject:(UIBarButtonItem *)buttonItem {
    // We first create a new section in the model.
    NSIndexSet* indexSet = [self.model addSectionWithTitle:[self randomName]];
    
    // Then we create an array of objects that we want to add to this section.
    NSMutableArray *objects = [NSMutableArray array];
    for (NSInteger ix = 0; ix < arc4random_uniform(10) + 1; ++ix) {
        [objects addObject:[UXTitleTableViewCellObject objectWithTitle:[self randomName]]];
    }
    
    // The result of adding these objects is an array of index paths that can be used to ensure the
    // visibility of the new objects.
    NSArray* indexPaths = [self.model addObjectsFromArray:objects];
    
    // Now that we've modified the model, we want to recompile the section index before notifying the
    // table view of changes to the model.
    [self.model updateSectionIndex];
    
    // Tell the table view that we've added a new section and that it should use the default
    // animation.
    [self.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
    
    // Scroll the table view such that the last object is in view.
    [self.tableView scrollToRowAtIndexPath:indexPaths.lastObject atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

#pragma mark - UITableViewDelegate

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

#pragma mark - UXMutableTableViewDataSourceDelegate

- (BOOL)tableViewModel:(UXMutableTableViewDataSource *)tableViewModel
         canEditObject:(id)object
           atIndexPath:(NSIndexPath *)indexPath
           inTableView:(UITableView *)tableView {
    // We want every cell to be editable.
    return YES;
}

- (BOOL)tableViewModel:(UXMutableTableViewDataSource *)tableViewModel
    shouldDeleteObject:(id)object
           atIndexPath:(NSIndexPath *)indexPath
           inTableView:(UITableView *)tableView {
    // We're going to store the index path that wants to be deleted so that we can delete the correct
    // cell after the alert view has been dismissed.
    self.indexPathForDeletion = indexPath;
    
    // Rather than allow the model to simply delete the object, we're going to present a prompt that
    // confirms with the user that they want to delete this object.
//    [UXAlertView showAlertWithTitle:@"Confirm" message:@"Are you that sure you want to delete this cell?" cancelButton:@"No" okButton:@"Yes" buttonHandler:^(UXAlertView *alert, NSInteger buttonIndex) {
//        
//        if (alert.cancelButtonIndex != buttonIndex) {
//            // If the user hits "OK" then let's delete the object from the model.
//            NSArray *indexPaths = [self.model removeObjectAtIndexPath:self.indexPathForDeletion];
//            
//            // And then notify the table view of the deletion.
//            [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
//            
//            // Now that we've deleted the object we no longer need this index path.
//            self.indexPathForDeletion = nil;
//        }
//    }];
    return NO;
}

- (UITableViewCell *)tableViewCellObject:(UXTableViewDataSource *)tableViewModel
                        cellForTableView:(UITableView *)tableView
                             atIndexPath:(NSIndexPath *)indexPath
                              withObject:(id)object {
    return [UXTableViewCellFactory tableViewCellObject:tableViewModel cellForTableView:tableView atIndexPath:indexPath withObject:object];
}


@end
