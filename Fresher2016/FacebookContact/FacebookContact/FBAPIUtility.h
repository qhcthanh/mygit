//
//  FBRequestManager.h
//  FacebookContact
//
//  Created by BaoNQ on 7/14/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "FBSessionManager.h"

@import FBSDKLoginKit;
@import FBSDKCoreKit;

#define kFBGraphRequestBaseURL @"https://graph.facebook.com/v2.6/"

typedef void(^RequestGraphDataWithGraphPathCompletionHandler)(NSData *data, NSURLResponse *response, NSError *error);
typedef void(^RequestDataWithURLCompletionHandler)(NSData *data, NSURLResponse *response, NSError *error);
typedef void(^LoginFacebookCompletionHanlder)(FBSDKLoginManagerLoginResult *result, NSError *error);

@interface FBAPIUtility : NSObject

@property (readonly, nonatomic, strong) FBSessionManager *sessionManager;


/**
 *  Init with a given session manager.
 */
- (id)initWithSessionManager:(FBSessionManager *)sessionManager;

/**
 *  Initializes a new request that use Facebook token.
    This function will be called asynchronize with a completion block.
 *
 *  @param graphPath            The graph path (e.g., "me").
 *  @param params               The optional parameters dictionary.
 *  @param completionBlock      A RequestGraphDataWithGraphPathCompletionHandler block.
 *
 *  @return This request will be add to a dictionary with this identifier. The dictionary is managed by FBSessionManager.
 */
- (INTEGER_T)requestGraphDataWithGraphPath:(NSString *)graphPath parameters:(NSDictionary *)params completionHandler:(RequestGraphDataWithGraphPathCompletionHandler)completionBlock;

/**
 *  Initializes a new request with a given URL.
    This function will be called asynchronize with a completion block.
 *
 *  @param url                  URL
 *  @param completionBlock A    RequestDataWithURLCompletionHandler block.
 *
 *  @return This request will be add to a dictionary with this identifier. The dictionary is managed by FBSessionManager.
 */
- (INTEGER_T)requestDataWithURL:(NSURL *)url completionHandler:(RequestDataWithURLCompletionHandler)completionBlock;

/**
 *  This function will check the user has been login Facebook
 *
 *  @return If user has been login will return true else return false
 */
+ (BOOL)isUserLoginFacebook;

/**
 *  Get current user access token key Facebook
 *
 *  @return A access token key. If user is not login will return nil
 */
+ (NSString*)getCurrentAccessTokenKey;

/**
 *  Logs the user in or authorizes additional permissions.
 *
 *  @param permissions        the optional array of permissions. Note this is converted to NSSet and is only
  an NSArray for the convenience of literal syntax.
 *  @param fromViewController the view controller to present from. If nil, the topmost view controller will be
 automatically determined as best as possible.
 *  @param handler            the callback.
 *  Discussion Use this method when asking for read permissions. You should only ask for permissions when they
    are needed and explain the value to the user. You can inspect the result.declinedPermissions to also
    provide more information to the user if they decline permissions.
 
    This method will present UI the user. You typically should check if `[FBSDKAccessToken currentAccessToken]`
    already contains the permissions you need before asking to reduce unnecessary app switching. For example,
    you could make that check at viewDidLoad.
 */
+ (void)logInFacebookWithReadPermissions:(NSArray *)permissions
                     fromViewController:(UIViewController *)fromViewController
                                handler:(LoginFacebookCompletionHanlder)handler;

/**
 *  Logs the user out
 *  discussion This calls [FBSDKAccessToken setCurrentAccessToken:nil] and [FBSDKProfile setCurrentProfile:nil].
 */
+ (void)logOutFacebook;
@end










