//
//  CameraZaloSessionView.m
//  CameraWithAVFoundation
//
//  Created by qhcthanh on 6/9/16.
//  Copyright © 2016 Gabriel Alvarado. All rights reserved.
//

#import "CameraZaloSessionView.h"

@interface CameraZaloSessionView () <CaptureSessionManagerDelegate> {
    
}

@end

@implementation CameraZaloSessionView {

}


-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    return self;
}

-(void)composeInterface {

    //Define adaptable sizing variables for UI elements to the right device family (iPhone or iPad)
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        //Declare the sizing of the UI elements for iPad
        shutterButtonSize = CGSizeMake(self.bounds.size.width * 0.1,
                                       self.bounds.size.width * 0.1);
        topBarSize = CGSizeMake(self.frame.size.width,
                                self.frame.size.height * 0.06);
        bottomBarSize = CGSizeMake(self.frame.size.width,
                                   self.frame.size.height * 0.2);
        barButtonItemSize = CGSizeMake(UISCREEN_BOUND_SIZE_HEIGHT * 0.04,
                                       UISCREEN_BOUND_SIZE_HEIGHT * 0.04);
    } else
    {
        //Declare the sizing of the UI elements for iPhone
        shutterButtonSize = CGSizeMake(self.bounds.size.width * 0.21, self.bounds.size.width * 0.21);
        topBarSize = CGSizeMake(self.frame.size.width,
                                UISCREEN_BOUND_SIZE_HEIGHT * 0.07);
        bottomBarSize = CGSizeMake(self.frame.size.width,
                                   UISCREEN_BOUND_SIZE_HEIGHT * 0.15);
        barButtonItemSize = CGSizeMake(UISCREEN_BOUND_SIZE_HEIGHT * 0.05,
                                       UISCREEN_BOUND_SIZE_HEIGHT * 0.05);
    }
    
    //Create the bottom bar and add the button to it
    _bottomBarView = [UIView new];
    if (_bottomBarView) {
        _bottomBarView.frame = (CGRect){0,self.frame.size.height - bottomBarSize.height,bottomBarSize};
        _bottomBarView.backgroundColor = defaultBarColor;
        [self addSubview:_bottomBarView];
        
        //Create shutter button
        _cameraShutter = [CameraShutterButton new];
        
        if (captureManager) {
            
            //Button Visual attribution
            _cameraShutter.frame = (CGRect){0,0, shutterButtonSize};
            _cameraShutter.center = CGPointMake(_bottomBarView.center.x,
                                                _bottomBarView.frame.size.height/2);
            _cameraShutter.tag = ShutterButtonTag;
            _cameraShutter.backgroundColor = [UIColor clearColor];
            
            [_bottomBarView addSubview:_cameraShutter];
        }
        //Add the camera dismiss button
        _cameraDismiss = [CameraDismissButton new];
        if (_cameraDismiss) {
            _cameraDismiss.frame = (CGRect){0,0, barButtonItemSize};
            bottomLeftPosition = CGPointMake(_bottomBarView.center.x * 0.20,
                                             _cameraShutter.center.y); // default left bottom position
            _cameraDismiss.center = bottomLeftPosition;
            _cameraDismiss.tag = DismissButtonTag;
            [_bottomBarView addSubview:_cameraDismiss];
        }
        
        //Add the camera toggle button
        _cameraToggle = [CameraToggleButton new];
        if (_cameraToggle) {
            _cameraToggle.frame = (CGRect){0,0, barButtonItemSize};
            bottomRightPosition = CGPointMake(_bottomBarView.center.x * 1.80,
                                              _cameraShutter.center.y); // default right bottom position
            _cameraToggle.center = bottomRightPosition;
            _cameraToggle.tag = ToggleButtonTag;
            [_bottomBarView addSubview:_cameraToggle];
        }
        
        for (UIButton *button in _bottomBarView.subviews) {
            button.backgroundColor = [UIColor clearColor];
            [button addTarget:self action:@selector(inputManager:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    _topBarView = [UIView new];
    if (_topBarView) {
        
        //Setup visual attribution for bar
        _topBarView.frame  = (CGRect){0,0, topBarSize};
        _topBarView.backgroundColor = defaultBarColor;
        [self addSubview:_topBarView];
        
        //Add the flash button
        _cameraFlash = [CameraFlashButton new];
        if (_cameraFlash) {
            _cameraFlash.frame = (CGRect){0,0, barButtonItemSize};
            topLeftPosition = CGPointMake(_topBarView.center.x * 0.2, _topBarView.center.y); // default top left position
            _cameraFlash.center = topLeftPosition;
            _cameraFlash.tag = FlashButtonTag;
            [_topBarView addSubview:_cameraFlash];
        }
        
        _cameraRecord = [CameraShutterButton new];
        
        if (_cameraRecord) {
            _cameraRecord.frame = (CGRect){0,0,barButtonItemSize};
            _cameraRecord.center = _topBarView.center;
            _cameraRecord.tag = RecordButtonTag;
            [_cameraRecord setImage:[UIImage imageNamed:@"Play"] forState: UIControlStateNormal];
            [_topBarView addSubview:_cameraRecord];
        }
        
        //Attribute and configure all buttons in the bar's subview
        for (UIButton *button in _topBarView.subviews) {
            button.backgroundColor = [UIColor clearColor];
            [button addTarget:self action:@selector(inputManager:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    //Create the focus reticule UIView
    focalReticule = [CameraFocalReticule new];
    
    if (focalReticule) {
        
        focalReticule.frame = (CGRect){0,0, 60, 60};
        focalReticule.backgroundColor = [UIColor clearColor];
        focalReticule.hidden = YES;
        [self addSubview:focalReticule];
    }
    
    //Create the gesture recognizer for the focus tap
//    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]
//                                                          initWithTarget:self action:@selector(focusGesture:)];
//    singleTapGestureRecognizer.cancelsTouchesInView = false;
//    if (singleTapGestureRecognizer) [self addGestureRecognizer:singleTapGestureRecognizer];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    NSLog(@"Touch began");
    UITouch *touch = [[touches allObjects] firstObject];
    if (touch) {
        CGPoint location = [touch locationInView:self];
        [self focusAtPoint:location completionHandler:^{
            [self animateFocusReticuleToPoint:location];
        }];
    }
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    NSLog(@"Touch end");
}

- (void)focusGesture:(id)sender {
    
    if ([sender isKindOfClass:[UITapGestureRecognizer class]]) {
        UITapGestureRecognizer *tap = sender;
        if (tap.state == UIGestureRecognizerStateRecognized) {
            CGPoint location = [sender locationInView:self];
            
            [self focusAtPoint:location completionHandler:^{
                [self animateFocusReticuleToPoint:location];
            }];
        }
    }
}

-(void)inputManager:(id)sender {
    
    //If animation is in progress, ignore input
    if (animationInProgress) return;
    
    //If sender does not inherit from 'UIButton', return
    if (![sender isKindOfClass:[UIButton class]]) return;
    
    //Input manager switch
    switch ([(UIButton *)sender tag]) {
        case ShutterButtonTag:  [self onTapShutterButton];  return;
        case ToggleButtonTag:   [self onTapToggleButton];   return;
        case FlashButtonTag:    [self onTapFlashButton];    return;
        case DismissButtonTag:  [self onTapDismissButton];  return;
        case RecordButtonTag: [self onTapRecordButton]; return;
    }
}

-(void)onTapRecordButton {
    if (![captureManager isRecording]) {
        [self startRecording];
        
        [_cameraRecord setImage:[UIImage imageNamed:@"Stop"] forState: UIControlStateNormal];
    } else {
        [self stopRecording];
        [_cameraRecord setImage:[UIImage imageNamed:@"Play"] forState: UIControlStateNormal];
    }
}

-(void)cameraSessionManagerDidCaptureImage {
    [super cameraSessionManagerDidCaptureImage];
}

-(void)cameraSessionManagerFailedToCaptureImage {
    [super cameraSessionManagerFailedToCaptureImage];
}

-(void)cameraSessionManagerDidReportAvailability:(BOOL)deviceAvailability forCameraType:(CameraType)cameraType {
    [super cameraSessionManagerDidReportAvailability:deviceAvailability forCameraType:cameraType];
}

-(void)cameraSessionManagerDidReportDeviceStatistics:(CameraStatistics)deviceStatistics {
    // Get type
    [super cameraSessionManagerDidReportDeviceStatistics:deviceStatistics];
}

@end
