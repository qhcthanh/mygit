//
//  FBImageDownloader.h
//  FacebookContact
//
//  Created by BaoNQ on 7/15/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FBSessionManager.h"
#import <UIKit/UIKit.h>

//@import FBSDKLoginKit;
//@import FBSDKCoreKit;

typedef void(^DownloadImageWithURLCompletionHandler)(id imageIdentifier, UIImage *image, NSURLResponse * response, NSError * error);

@interface FBImageDownloader : NSObject

@property (readonly, nonatomic, strong) FBSessionManager *sessionManager;

/**
 *  Init with a given session manager.
 */
- (id)initWithSessionManager:(FBSessionManager *)sessionManager;

/**
 *  Download image with a given URL
    This function will be called asynchronize with a completion block.
 *
 *  @param url                  The URL to be retrieved.
 *  @param imageIdentifier      The downloaded image will be add to FBImageCacheManager by this identifier.
 *  @param completionBlock      A DownloadImageWithURLCompletionHandler block.
 *
 *  @return This task will be add to a dictionary with this identifier. The dictionary is managed by FBSessionManager.
 */
- (INTEGER_T)downloadImageWithURL:(NSURL *)url identifier:(id)imageIdentifier completionHandler:(DownloadImageWithURLCompletionHandler)completionBlock;

@end
