//
//  BLGiphyTopBarView.swift
//  EmojiKeyboard
//
//  Created by Quach Ha Chan Thanh on 8/23/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import UIKit

public class BLGiphyTopBarView: UIView {
    
    var searchView: UITextField!
    var currentSearchText: String?
    var keyboardButton: UIButton!
    
    public convenience init() {
        self.init(frame: CGRectZero)
        
        setupView()
    }
    
    func setupView() {
        
        // Setup Search Text Field
        self.searchView = UITextField()
        self.addSubview(searchView)
        
        self.searchView.layer.cornerRadius = 5
        self.searchView.layer.borderColor = UIColor.whiteColor().CGColor
        self.searchView.layer.borderWidth = 1.5
        
        self.searchView.textColor = UIColor.whiteColor()
        self.searchView.attributedPlaceholder = NSAttributedString(string: "Search text in here", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
        self.searchView.textAlignment = .Center
        
        // Setup change KeyboardButton
        self.keyboardButton = UIButton.init(type: .System)
        self.addSubview(keyboardButton)
        self.keyboardButton.setTitle("Aa", forState: .Normal)
        self.keyboardButton.tintColor = .whiteColor()
        self.keyboardButton.titleLabel?.adjustsFontSizeToFitWidth
        
        self.setupConstraints()
    }
    
    func setupConstraints() {
        
        // self.searchView constraint
        self.searchView.constrain(.Left, to: .Left, of: self, offsetBy: 5)
        self.searchView.constrain(.Top, to: . Top, of: self, offsetBy: 2)
        self.searchView.constrain(.Bottom, to: . Bottom, of: self, offsetBy: 2)
        
        // self.keyboard constraint
        self.keyboardButton.constrain(.Top, to: . Top, of: self.searchView, offsetBy: 2)
        self.keyboardButton.constrain(.Bottom, to: . Bottom, of: self.searchView, offsetBy: 2)
        self.keyboardButton.constrain(.Right, to: .Right, of: self, offsetBy: -5)
        self.keyboardButton.constrain(.Width, to: 30.0)
        
        // Relationship view
        self.searchView.constrain(.Right, to: . Left, of: self.keyboardButton, offsetBy: -5)
    }
}
