/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@class UXMutableURLRequest;
@protocol UXURLDataResponse <NSObject>
@required

/**
 * Processes the data from a successful request and determines if it is valid.
 *
 * If the data is not valid, return an error. The data will not be cached if there is an error.
 *
 * @param  request    The request this response is bound to.
 * @param  response   The response object, useful for getting the status code.
 * @param  data       The data received from the UXMutableURLRequest.
 * @return NSError if there was an error parsing the data. nil otherwise.
 *
 * @required
 */
- (NSError*)request:(UXMutableURLRequest*)request processResponse:(id)response data:(id)data;

@optional
/**
 * Processes the data from a unsuccessful request to construct a custom NSError object.
 *
 * @param  request    The request this response is bound to.
 * @param  response   The response object, useful for getting the status code.
 * @param  data       The data received from the TTURLRequest.
 * @return NSError to construct for this response.
 *
 * @optional
 */
- (NSError*)request:(UXMutableURLRequest*)request processErrorResponse:(id)response
               data:(id)data;

@end
