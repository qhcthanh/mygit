//
//  SNTextField+KeyboardEvent.m
//  CameraGPUImage
//
//  Created by BaoNQ on 8/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "SNTextField+KeyboardEvent.h"

@implementation SNTextField (KeyboardEvent)

- (void)keyboardWillShow:(NSNotification *)notification {
    [self updatePosition:notification];
}

- (void)keyboardTypeChanged:(NSNotification *)notification {
    [self updatePosition:notification];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    self.frame = CGRectMake(self.frame.origin.x, self.location.y, self.frame.size.width, self.frame.size.height);
}

- (void)updatePosition:(NSNotification *)notification {
    
    id userInfo = notification.userInfo[UIKeyboardFrameEndUserInfoKey];
    
    if (userInfo) {
        CGRect keyboardSize = [userInfo CGRectValue];
        
        self.frame = CGRectMake(self.frame.origin.x, (self.heightOfScreen - keyboardSize.size.height - self.frame.size.height), self.frame.size.width, self.frame.size.height);
    }
}

@end
