/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>


@interface UXLogMessage : NSObject {
@public
	int logLevel;
	int logFlag;
	int logContext;
	NSString *logMsg;
	NSDate *timestamp;
	const char *file;
	const char *function;
	int lineNumber;
	mach_port_t machThreadID;
    char *queueLabel;
	NSString *threadName;
	
	// The private variables below are only calculated if needed.
	// You should use the public methods to access this information.	
@private
	NSString *threadID;
	NSString *fileName;
	NSString *methodName;
}

/**
 * The initializer is somewhat reserved for internal use.
 * However, if you find need to manually create logMessage objects,
 * there is one thing you should be aware of.
 * The initializer expects the file and function parameters to be string literals.
 * That is, it expects the given strings to exist for the duration of the object's lifetime,
 * and it expects the given strings to be immutable.
 * In other words, it does not copy these strings, it simply points to them.
 */
- (id)initWithLogMsg:(NSString *)logMsg
               level:(int)logLevel
                flag:(int)logFlag
             context:(int)logContext
                file:(const char *)file
            function:(const char *)function
                line:(int)line;

/**
 * Returns the threadID as it appears in NSLog.
 * That is, it is a hexadecimal value which is calculated from the machThreadID.
 */
- (NSString *)threadID;

/**
 * Convenience method to get just the file name, as the file variable is generally the full file path.
 * This method does not include the file extension, which is generally unwanted for logging purposes.
 */
- (NSString *)fileName;

/**
 * Returns the function variable in NSString form.
 */
- (NSString *)methodName;

@end
