//
//  SuggestionManager.swift
//  testSuggestion
//
//  Created by manhduydl on 8/22/16.
//  Copyright © 2016 admin. All rights reserved.
//

import Foundation

enum trieTree : Int {
    case letterTree = 1
    case relativeLetterTree = 2
    case phraseTree = 3
}

public class SuggestionManager {
    
    // Singleton intance
    static let shareIntance = SuggestionManager()
    
    // Internal Concurent Queue
    var internalConcurentQueue : dispatch_queue_t
    var internalSerialQueue : dispatch_queue_t
    
    // Root node of trees
    var rootNodeOfLetterTree : LetterNode
    var rootNodeOfRelativeLetterTree : LetterNode
    var rootNodeOfPhraseTree : LetterNode
    
    init() {
        internalConcurentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
        internalSerialQueue = dispatch_queue_create("com.vng.banlakey", DISPATCH_QUEUE_SERIAL)
        rootNodeOfLetterTree = LetterNode.init()
        rootNodeOfRelativeLetterTree = LetterNode.init()
        rootNodeOfPhraseTree = LetterNode.init()
        
        setupRelativeLetterTree(dispatch_get_main_queue()) { (rootNodeOfRelativeLetterTree, error) in
            if error == nil {
                print("setup relative letter tree completed")
            }
        }
    }
    
    deinit {
        print("deinit suggestion")
    }
    
// MARK: - Setup Tree
    
    /**
     Build trees to set up suggestion function
     
     - parameter userDefaultKey:    Key of NSUserDefault to get database
     - parameter dispatchQueue:     Queue to dispatch when completion
     - parameter completionHandler: Closure to perform when completion
     */
    private func setupAllTree(userDefaultKey : String, dispatchQueue : dispatch_queue_t, completionHandler : (rootNodeDictionary : Dictionary<String,LetterNode>, error : NSError?) -> Void) {
        
        dispatch_async(internalConcurentQueue) {
            let defaults = NSUserDefaults.init(suiteName: userDefaultKey)
            
            let letterJsonString = defaults?.stringForKey(Constants.letterJsonStringKey)
            let relativeLetterJsonString = defaults?.stringForKey(Constants.relativeLetterJsonStringKey)
            let phraseJsonString = defaults?.stringForKey(Constants.phraseJsonStringKey)
            
            self.rootNodeOfLetterTree = self.buildLetterTreeFromJsonString(letterJsonString!)
            self.rootNodeOfRelativeLetterTree = self.buildRelativeLetterTreeFromJsonString(relativeLetterJsonString!)
            self.rootNodeOfPhraseTree = self.buildPhraseTreeFromJsonString(phraseJsonString!)
            
            let rootNodeDictionary : [String:LetterNode] = [
                Constants.letterKey : self.rootNodeOfLetterTree,
                Constants.relativeLetterKey : self.rootNodeOfRelativeLetterTree,
                Constants.phraseKey : self.rootNodeOfPhraseTree
            ]
            
            dispatch_async(dispatchQueue, {
                completionHandler(rootNodeDictionary : rootNodeDictionary, error : nil)
            })
        }
    }
    
    /**
     Build relative letter tree from file
     
     - parameter dispatchQueue:     Queue to dispatch when completion
     - parameter completionHandler: Closure to perform when completion
     */
    func setupRelativeLetterTree(dispatchQueue : dispatch_queue_t, completionHandler : (rootNodeOfRelativeLetterTree : LetterNode, error : NSError?) -> Void) {
        
        dispatch_async(internalConcurentQueue) {
        
            let path = NSBundle.mainBundle().pathForResource("relativeLetterJson", ofType: "txt")
            var error : NSError? = nil
            do {
                let relativeLetterJsonString = try String(contentsOfFile:path!, encoding: NSUTF8StringEncoding)
                self.rootNodeOfRelativeLetterTree = self.buildRelativeLetterTreeFromJsonString(relativeLetterJsonString)
                
            } catch let err as NSError {
                error = err
            }
            
            dispatch_async(dispatchQueue, {
                completionHandler(rootNodeOfRelativeLetterTree : self.rootNodeOfRelativeLetterTree, error : error)
            })
        }
    }
    
// MARK: - Database Method
    
    /**
     Check tree overload, optimize tree and update sqlite db
     
     - parameter dispatchQueue:     queue to dispatch when completion
     - parameter completionHandler: closure to perform when completion
     */
    func updateDatabase(dispatchQueue : dispatch_queue_t, completionHandler : (NSError?) -> Void) {
        
        // Check overload of letter tree and phrase tree. If overloaded, remove nodes (Number of node to remove save in Contants)
        dispatch_sync(internalSerialQueue) {
            
            if self.rootNodeOfLetterTree.getAllEndNode().count > Constants.maxNumberNodeOfTree {
                self.optimizeLetterTree(self.rootNodeOfLetterTree.getAllEndNode().count)
            }
            if self.rootNodeOfPhraseTree.getAllEndNode().count > Constants.maxNumberNodeOfTree {
                self.optimizePhraseTree(self.rootNodeOfPhraseTree.getAllEndNode().count)
            }
        }
        
        // Call method update sqlite db
        var singleWordList : [BLSingleWord] = []
        for node in self.rootNodeOfLetterTree.getAllEndNode() {
            singleWordList.append(BLSingleWord(_word: node.key, _score: node.score))
        }
        
        var multiWordList : [BLMultiWord] = []
        for node in self.rootNodeOfPhraseTree.getAllEndNode() {
            if let multiWord = BLMultiWord.init(phrase: node.key, _score: node.score) {
                multiWordList.append(multiWord)
            }
        }
        
        BLDBManager.shareIntance.updateObjects(BLSingleWord.classForCoder(), objects: singleWordList)
        BLDBManager.shareIntance.updateObjects(BLMultiWord.classForCoder(), objects: multiWordList)

        completionHandler(nil)
    }
    
// MARK: - Build Tree
    
    /**
     Build Letter tree from json string in nsuserdefaul of container app and dispatch to dispatchQueue with completionHandler closure
     
     - parameter letterJsonString:  json string to build tree
     - parameter dispatchQueue:     queue to dispatch result
     - parameter completionHandler: closure to perform when tree is builded
     */
    func buildLetterTreeFromJsonString(letterJsonString : String, dispatchQueue : dispatch_queue_t, completionHandler : (rootNode : LetterNode, error : NSError?) -> Void) {
        
        dispatch_async(internalConcurentQueue) {
            let rootNode = self.buildLetterTreeFromJsonString(letterJsonString)
            
            dispatch_async(dispatchQueue, {
                completionHandler(rootNode: rootNode, error: nil)
            })
        }
    }
    
    /**
     Build Letter tree from json string in syncronize
     
     - parameter letterJsonString: Json string to build tree
     
     - returns: Return rootNode of tree
     */
    func buildLetterTreeFromJsonString(letterJsonString : String) -> LetterNode {
        
        let words = self.convertStringToDictionary(letterJsonString)
        let rootNode = LetterNode.init()
        
        for word : String in (words?.keys)! {
            var current = rootNode
            while (word.length != current.level) {
                let searchKey: String = word.substringToIndex(current.level + 1)
                current = current.addChild(withKey : searchKey)
            }
            
            current.isEnd = true
            current.count = words![word]?.valueForKey("count") as! Int
            current.time = words![word]?.valueForKey("time") as! String
        }
        
        
        print("build letter tree completed", rootNode.getAllEndNode().count)
        return rootNode
    }
    
    /**
     Build Phrase tree from json string in nsuserdefaul of container app
     
     - parameter letterJsonString:  json string to build tree
     - parameter dispatchQueue:     queue to dispatch result
     - parameter completionHandler: closure to perform when tree is builded
     */
    func buildPhraseTreeFromJsonString(phraseJsonString : String, dispatchQueue : dispatch_queue_t, completionHandler : (rootNode : LetterNode, error : NSError?) -> Void) {
        
        dispatch_async(internalConcurentQueue) {
            let rootNode = self.buildPhraseTreeFromJsonString(phraseJsonString)
            
            dispatch_async(dispatchQueue, {
                completionHandler(rootNode: rootNode, error: nil)
            })
        }
    }
    
    /**
     Build phrase tree from json String in syncronize
     
     - parameter phraseJsonString: Json string to build tree
     
     - returns: Return rootNode of tree
     */
    func buildPhraseTreeFromJsonString(phraseJsonString : String) -> LetterNode {
        
        let phrases = self.convertStringToDictionary(phraseJsonString)
        let rootNode = LetterNode.init()
        
        for phrase : String in (phrases?.keys)! {
            
            var current = rootNode
            var words = phrase.words()
            
            while (words.count != current.level) {
                //                let searchKey = words[current.level]
                let searchKey : String
                if current.level == 0 {
                    searchKey = words[current.level]
                } else {
                    searchKey = phrase
                }
                current = current.addChild(withKey : searchKey)
            }
            
            current.isEnd = true
            current.count = (phrases![phrase]?.valueForKey("count"))! as! Int
            current.time = (phrases![phrase]?.valueForKey("time"))! as! String
        }
        
        
        //print(phrases?.count)
        print("build phrase tree completed", (phrases?.count)!)
        return rootNode
    }
    
    /**
     Build Relative Letter tree from json string in nsuserdefaul of container app.
     
     - parameter letterJsonString:  json string to build tree
     - parameter dispatchQueue:     queue to dispatch result
     - parameter completionHandler: closure to perform when tree is builded
     */
    func buildRelativeLetterTreeFromJsonString(relativeJsonString : String, dispatchQueue : dispatch_queue_t, completionHandler : (rootNode : LetterNode, error : NSError?) -> Void) {
        
        dispatch_async(internalConcurentQueue) {
            let rootNode = self.buildRelativeLetterTreeFromJsonString(relativeJsonString)
            
            dispatch_async(dispatchQueue, {
                completionHandler(rootNode: rootNode, error: nil)
            })
        }
        
    }
    
    /**
     Build relative letter tree from json string in syncronize
     
     - parameter relativeJsonString: Json string to build tree
     
     - returns: Return rootNode of tree
     */
    func buildRelativeLetterTreeFromJsonString(relativeJsonString : String) -> LetterNode {
        
        let relativeLetter = self.convertStringToDictionary(relativeJsonString)
        let rootNode = LetterNode.init()
        
        for phrase : String in (relativeLetter?.keys)! {
            var current = rootNode
            var words = phrase.words()
            
            while (words.count != current.level) {
                //              let searchKey = words[current.level]
                let searchKey : String
                if current.level == 0 {
                    searchKey = words[current.level]
                } else {
                    searchKey = phrase
                }
                current = current.addChild(withKey : searchKey)
            }
            
            current.isEnd = true
        }
        
        print("build relative letter tree completed", (relativeLetter?.count)!)
        return rootNode
    }
    
// MARK: - Public Suggestion Method
    
    /**
     Return a list suggestion of word or phrase
     
     - parameter input:              input is all text in a session of keyboard
     - parameter numberOfSuggestion: number of suggestion will return
     - parameter dispatchQueue:      queue will dispatch when completion
     - parameter completionHandler:  closure will perform when completion
     */
    func suggestWithInput(input : String, numberOfSuggestion : Int, dispatchQueue : dispatch_queue_t, completionHandler : (Array<String>,NSError?) ->Void) {

        dispatch_async(internalSerialQueue) {
            var result : Array<String> = Array<String>()
            
            if (input.characters.last == " " && input.words().count > 0) {
                
                let sentence = input.substringToIndex((input.endIndex.predecessor()));
                result = self.suggestPhraseWithWord(sentence.lastWord()!.lowercaseString, andNumberOfSuggestion: numberOfSuggestion)
            } else {
                
                var prefix : String
                if ((input.characters.contains(" ")) == true && input.words().count > 0) {
                    var words = input.words()
                    prefix = words[(words.endIndex) - 1]
                    result = self.suggestWordWithPrefix(prefix.lowercaseString, andNumberOfSuggestion: numberOfSuggestion)
                    
                } else {
                    prefix = input
                    result = self.suggestWordWithPrefix(prefix.lowercaseString, andNumberOfSuggestion: numberOfSuggestion)
                }
                
                if prefix.isCapitalizedWord() {
                    for i in 0..<result.count {
                        result[i] = result[i].capitalizedString
                    }
                }
            }
            
            dispatch_async(dispatchQueue) {
                completionHandler(result, nil)
            }
        }
    }
    
    func suggestWhenSwipe(word: String) -> [String] {
        var result: [String] = []
        
        dispatch_sync(internalSerialQueue) {
            
            let list = BLDBManager.shareIntance.fetchObjects(BLSingleWord.classForCoder(), matching: "asciiWord MATCH '\(word.arrayCharacters().joinWithSeparator("*"))'", number: 4)
            
            for singleWord in list {
                result.append((singleWord as! BLSingleWord).word)
            }
        }

        return result
    }
    
    /**
     Return words are suggested, from prefix input, in special queue
     
     - parameter prefix:             prefix of word
     - parameter numberOfSuggestion: number of word will return
     - parameter dispatchQueue:      queue to return
     - parameter completionHandler:  closure perform when completion
     */
    func suggestWordWithPrefix(prefix : String, numberOfSuggestion : Int?, dispatchQueue : dispatch_queue_t, completionHandler : (Array<String>,NSError?) ->Void) {
        
        dispatch_async(internalConcurentQueue) {
            
            var result : [String] = []
            if let numberOfWord = numberOfSuggestion {
                
                // Return a number of word
                result = self.suggestWordWithPrefix(prefix, andNumberOfSuggestion: numberOfWord)
            } else {
                
                // Return all word are suggeted
                result = self.suggestAllWordWithPrefix(prefix)
            }
            
            dispatch_async(dispatchQueue) {
                completionHandler(result, nil)
            }
        }
    }
    
    /**
     Suggest all word with prefix input in syncronize
     
     - parameter prefix: Prefix to suggest word
     
     - returns: Return array of all word
     */
    func suggestAllWordWithPrefix(prefix : String) -> Array<String> {
        
        var result = Array<LetterNode>()
        var suggestionWords : [String] = []
        
        self.findWord(withPrefix: prefix, result: &result)
        self.findRelativeWord(withPrefix: prefix, result: &result)
        
        if result.count == 0 {
            suggestionWords.append(prefix)
            return suggestionWords
        }
        
        result.sortInPlace({ (node1:LetterNode, node2:LetterNode) -> Bool in
            node1.score >= node2.score
        })
        for node in result {
            suggestionWords.append(node.key)
        }
        
        return suggestionWords
    }
    
    /**
     Suggest a number of word with prefix in syncronize
     
     - parameter prefix: Prefix to suggest word
     - parameter number: Number of word will return
     
     - returns: Return array of suggested word
     */
    func suggestWordWithPrefix(prefix : String, andNumberOfSuggestion number : Int ) -> Array<String> {
        
        var result = Array<LetterNode>()
        var suggestionWords : [String] = []
        
        self.findWord(withPrefix: prefix, result: &result)
        if result.count < number {
            self.findRelativeWord(withPrefix: prefix, result: &result)
        }
        
        if result.count >= number {
            result.sortInPlace({ (node1:LetterNode, node2:LetterNode) -> Bool in
                node1.count >= node2.count
            })
            for i in 0..<number  {
                suggestionWords.append(result[i].key)
            }
            
        } else {
            
            // Check if prefix has sign
            var hasSign : Bool
            if prefix.isVietNamString() {
                hasSign = true
            } else {
                hasSign = false
            }
            
            // Get word from sqlite database when word in tree is not enough
            let temptArray = BLDBManager.shareIntance.fetchObjects(BLSingleWord.classForCoder(), matching: "\(hasSign ? "word" : "asciiWord") MATCH '\(prefix)*'", number: number)
            //let temptArray = []
            
            if result.count == 0 {
                for object in temptArray {
                    suggestionWords.append((object as! BLSingleWord).word)
                }
            } else {
            
                for node in result {
                    suggestionWords.append(node.key)
                }
                
                for object in temptArray {
                    let word = (object as! BLSingleWord).word
                    if !suggestionWords.contains(word) {
                        suggestionWords.append(word)
                        if suggestionWords.count == number {
                            break
                        }
                    }
                }
            }
        }
        
        return suggestionWords
    }
    
    /**
     Suggest all phrase with first word
     
     - parameter word: First word to suggest phrase
     
     - returns: Array of all phrase suggest
     */
    func suggestPhraseWithWord(word : String) -> Array<String> {
        var result = Array<String>()
        if let brandToFind = rootNodeOfPhraseTree.childrentNode[word] {
            
            // Sort dictionary and return array tuble
            let array = brandToFind.childrentNode.sort({ (i1, i2) -> Bool in
                if i1.1.score > i2.1.score {
                    return true
                }
                return false
            })
            
            for (key, _) in array {
                result.append(key.words()[1])
            }
            
        } else {
            result.append(word)
        }
        
        return result
    }
    
    /**
     Suggest a number of phrase with first word
     
     - parameter word:   First word to suggest phrase
     - parameter number: Number of phrase will return
     
     - returns: Array of phrase
     */
    func suggestPhraseWithWord(word : String, andNumberOfSuggestion number : Int) -> Array<String> {
        var result = Array<String>()
        
        if let brandToFind = rootNodeOfPhraseTree.childrentNode[word] {
            
            if brandToFind.childrentNode.count >= number {
                // Sort dictionary and return array tuble
                let array = brandToFind.childrentNode.sort({ (i1, i2) -> Bool in
                    if i1.1.score > i2.1.score {
                        return true
                    }
                    return false
                })
                
                for (key, _) in array {
                    result.append(key.words()[1])
                    if result.count == Constants.numberOfSuggestion {
                        return result
                    }
                }
            } else {
                for (key, _) in brandToFind.childrentNode {
                    result.append(key.words()[1])
                }
            }
        }
        
        if result.count < number {
            
            // Search in sqlite database
            let tempArray = BLDBManager.shareIntance.fetchObjects(BLMultiWord.classForCoder(), matching: "firstWord MATCH '\(word)'", number: number)
            //let tempArray = []
            
            for object in tempArray {
                let word = (object as! BLMultiWord).secondWord
                if !result.contains(word) {
                    result.append(word)
                    if result.count == number {
                        break
                    }
                }
            }
        }
        
        return result
    }
    
// MARK: - Improve Suggestion
    
    /**
     Learn new word or increase count of exist word when user typing
     
     - parameter content: All text before when user type a space (content except space)
     */
    func updateTree(withInput input : String?) {
        
        if let content = input {
            if (content.characters.count > 1) && (content.characterAtIndex(content.length - 2).isUnicodeCharacter()) {
                if let lastWord = content.lastWord() {
                    updateTreeWithWord(lastWord)
                }
                if let lastPhrase = content.lastPhrase() {
                    updateTreeWithPhrase(lastPhrase)
                }
            }
        }
    }
    
    /**
     Update tree with word. If new word is create node, else is increase count of word
     
     - parameter word: Word to update tree
     */
    func updateTreeWithWord(word : String) {
        
        if word.length < Constants.maxLenghtOfWord {
            dispatch_async(internalSerialQueue) {
                print("learn word: ", word)
                
                let findWord = self.existionOfWord(word)
                
                if findWord.found == true {
                    findWord.node?.count += 1
                } else {
                    // Fetch from sqlite db and update to tree
                    let temp = BLDBManager.shareIntance.fetchObjects(BLSingleWord.classForCoder(), matching: "word MATCH '\(word)'", number: 1)
                    let singleWord: BLSingleWord? = temp.count > 0 ? (temp.last as! BLSingleWord) : nil
                    
                    var current = self.rootNodeOfLetterTree
                    while (word.length != current.level) {
                        let searchKey: String = word.substringToIndex(current.level + 1)
                        current = current.addChild(withKey : searchKey.lowercaseString)
                    }
                    
                    current.isEnd = true
                    if current.score < Int.max && current.level > 1 {
                        if singleWord == nil {
                            current.score = 1
                        } else {
                            current.score = (singleWord?.score)! + 1
                        }
                    }
                }
            }
        }
    }
    
    /**
     Update tree with phrase. If new phrase is create node, else is increase count of word
     
     - parameter phrase: Phrase to update tree
     */
    func updateTreeWithPhrase(phrase : String) {
        
        if phrase.length < Constants.maxLenghtOfPhrase {
            dispatch_async(internalSerialQueue) {
                print("learn phrase: ", phrase)
                
                let findPhrase = self.existionOfPhrase(phrase)
                
                if findPhrase.found == true {
                    findPhrase.node?.score += 1
                } else {
                    
                    // Fetch from sqlite db and save to tree
                    let splitWord: [String] = phrase.words()
                    let temp = BLDBManager.shareIntance.fetchObjects(BLMultiWord.classForCoder(), matching: "firstWord = '\(splitWord[0])' AND secondWord = '\(splitWord[1])'", number: 1)
                    let multiWord: BLMultiWord? = temp.count > 0 ? (temp.last as! BLMultiWord) : nil
                    
                    var current = self.rootNodeOfPhraseTree
                    var words = phrase.words()
                    
                    while (words.count != current.level) {
                        let searchKey : String
                        if current.level == 0 {
                            searchKey = words[current.level]
                        } else {
                            searchKey = phrase
                        }
                        current = current.addChild(withKey : searchKey.lowercaseString)
                    }
                    
                    current.isEnd = true
                    if  current.score < Int.max {
                        if multiWord == nil {
                            current.score = 1
                        } else {
                            current.score = (multiWord?.score)! + 1
                        }
                    }
                    
                }
            }
        }
    }
    
// MARK: - Optimize Tree
    
    /**
     Optimize tree by removing node of it
     
     - parameter tree:             Kind of trie tree
     - parameter numberNodeRemove: Number of node will remove
     */
    func optimizeTree(tree : trieTree, numberNodeRemove : Int) {
        
        switch tree {
        case trieTree.letterTree :
            optimizeLetterTree(numberNodeRemove)
            break
            
        case trieTree.phraseTree :
            optimizePhraseTree(numberNodeRemove)
            break
            
        default :
            print("Dont have trie tree match")
        }
    }
    
    /**
     Optimize memory with removing a number of node in letter tree
     
     - parameter rootNode:         Root node of tree will optimize
     - parameter numberNodeRemove: Number of node will remove
     */
    func optimizeLetterTree(numberNodeRemove : Int) {
        
        print("optimize letter tree")
        var listNode = self.rootNodeOfLetterTree.getAllEndNode()
        
        listNode.sortInPlace { (node1, node2) -> Bool in
            node1.score < node2.score
        }
        
        for i in 0..<numberNodeRemove {
            self.removeWord(listNode[i].key)
        }
        
    }
    
    /**
     Optimize memory with removing a number of node in phrase tree
     
     - parameter rootNode:         Root node of tree will optimize
     - parameter numberNodeRemove: Number of node will remove
     */
    func optimizePhraseTree(numberNodeRemove : Int) {
        
        print("optimize phrase tree")
        var listNode = self.rootNodeOfPhraseTree.getAllEndNode()
        
        listNode.sortInPlace { (node1, node2) -> Bool in
            node1.score < node2.score
        }
        
        for i in 0..<numberNodeRemove {
            self.removePhrase(listNode[i].key)
        }
    }
    
// MARK: - Private Method
    
    // Converse Json String to Dictionary
    private func convertStringToDictionary(text: String) -> [String:NSDictionary]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:NSDictionary]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    // Check existion of word in letter tree
     func existionOfWord(word : String) -> (node : LetterNode?, found : Bool) {
        var currentNode = rootNodeOfLetterTree
        
        while (word.length != currentNode.level) {
            let searchKey : String = word.substringToIndex(currentNode.level + 1)
            if currentNode.find(searchKey).found {
                currentNode = currentNode.find(searchKey).node!
            } else {
                return (nil, false)
            }
        }
        
        return (currentNode, currentNode.isEnd)
    }
    
    // Check existion of phrase in phrase tree
     func existionOfPhrase(phrase : String) -> (node : LetterNode?, found : Bool) {
        let firstWord = phrase.words()[0]
        
        if let brandToFind = rootNodeOfPhraseTree.childrentNode[firstWord] {
            if brandToFind.childrentNode[phrase] != nil {
                return (brandToFind.childrentNode[phrase], true)
            } else {
                return (nil, false)
            }
        } else {
            return (nil, false)
        }
    }
    
    // Remove word from letter tree
    private func removeWord(word : String) -> Bool {
        
        if self.existionOfWord(word).found == false {
            return false
        }
        
        var currentNode = rootNodeOfLetterTree
        while word.length != currentNode.level {
            let searchKey = word.substringToIndex(currentNode.level + 1)
            currentNode = currentNode.childrentNode[searchKey]!
        }
        
        if currentNode.childrentNode.count > 0 {
            currentNode.setEndNode(false)
            
        } else {
            var key = currentNode.key
            while (currentNode.childrentNode.count == 0 &&  !currentNode.isRoot()) {
                currentNode = currentNode.parentNode!
                currentNode.childrentNode[key]!.parentNode = nil
                //currentNode.childrentNode[key]!.key = ""
                currentNode.childrentNode.removeValueForKey(key)
                key = currentNode.key
            }
        }
        
        return true
    }
    
    // Remove phrase from phrase tree
    private func removePhrase(phrase : String) -> Bool {
        
        if self.existionOfPhrase(phrase).found == false {
            return false
        }
        
        var currentNode = rootNodeOfPhraseTree.childrentNode[phrase.words()[0]]!
        currentNode = currentNode.childrentNode[phrase]!
        
        var key = currentNode.key
        while (currentNode.childrentNode.count == 0 &&  !currentNode.isRoot()) {
            currentNode = currentNode.parentNode!
            currentNode.childrentNode[key]!.parentNode = nil
            currentNode.childrentNode[key]!.key = ""
            currentNode.childrentNode.removeValueForKey(key)
            key = currentNode.key
        }
        
        return true
    }
    
    // Find word with prefix
    private func findWord(withPrefix prefix : String, inout result : Array<LetterNode>) {
        let branchToUse = self.findBranchToSearch(fromPrefix: prefix)
        
        if branchToUse.level == 0 {
            //result.append(prefix)
            return
        }
        if branchToUse.isEnd == true {
            result.append(branchToUse)
        }
        
        self.findWord(fromBranch: branchToUse, result: &result)
    }
    
    // Find word from branch
    private func findWord(fromBranch branch : LetterNode, inout result : Array<LetterNode>) {
        var remainNode = Array<LetterNode>()
        
        for (_, node) in branch.childrentNode {
            if node.isEnd == true {
                result.append(node)
                if node.childrentNode.count > 0 {
                    remainNode.append(node)
                }
            } else {
                remainNode.append(node)
            }
        }
        
        for node in remainNode {
            self.findWord(fromBranch: node, result: &result)
        }
    }
    
    // Find branch to begin search from
    private func findBranchToSearch(fromPrefix prefix : String) -> LetterNode {
        var currentNode = rootNodeOfLetterTree
        
        while prefix.length != currentNode.level {
            let searchKey = prefix.substringToIndex(currentNode.level + 1)
            
            if let temptNode = currentNode.childrentNode[searchKey] {
                currentNode = temptNode
            } else {
                return rootNodeOfLetterTree
            }
        }
        
        return currentNode
    }
    
    // find relative word with prefix
    private func findRelativeWord(withPrefix prefix : String, inout result : Array<LetterNode>) {
        for i in (prefix.length - 1).stride(to: -1, by: -1) {
            //print(i)
            let key : String = prefix.characterAtIndex(i)
            var searchKey : String
            
            if let temptNode = self.rootNodeOfRelativeLetterTree.childrentNode[key] {
                for (letter , _) in temptNode.childrentNode {
                    if prefix.length >= 2 {
                        searchKey = prefix.replace(withCharacter: letter.words()[1], atIndex: i)
                    } else {
                        searchKey = letter.words()[1]
                    }
                    self.findWord(withPrefix: searchKey, result: &result)
                }
            }
        }
    }
    
    // Random generate a string with specify length
    private func randomString(length: Int) -> String {
        
        let allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        let allowedCharsCount = UInt32(allowedChars.characters.count)
        var randomString = ""
        
        for _ in (0..<length) {
            let randomNum = Int(arc4random_uniform(allowedCharsCount))
            let newCharacter = allowedChars[allowedChars.startIndex.advancedBy(randomNum)]
            randomString += String(newCharacter)
        }
        
        return randomString
    }
}