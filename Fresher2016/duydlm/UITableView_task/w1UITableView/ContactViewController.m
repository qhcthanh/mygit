//
//  ContactViewController.m
//  w1UITableView
//
//  Created by manhduydl on 5/18/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "ContactViewController.h"
#import "ContactScanner.h"
#import "ContactTableViewCell.h"
#import "Contants.h"
#import "Contact.h"
#import "AddContactViewController.h"
#import "UIImage+Mask.h"

@interface ContactViewController () <AddContactDelegate>

@property (nonatomic) NSMutableDictionary *contactDictionary;
@property (nonatomic) NSMutableArray *contactSectionTitleArray;
@property (nonatomic) NSMutableArray *selectedContactArray;
@property (strong, nonatomic) UISearchController *searchController;
@property (nonatomic) NSMutableArray *contactArray;
@property (nonatomic) NSMutableArray *filteredContactArray;
@property (nonatomic) BOOL isSearchTextNull;
@property (nonatomic) BOOL isSearching;
@property (nonatomic) UIImage *accessoryImage;
@property (nonatomic) UIView *blurView;

@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (IS_OS_9_OR_LATER) {
        //Notification when contact change ios 9 and later
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contactsDidChange:) name:CNContactStoreDidChangeNotification object:nil];
    } else {
        //Notification when addressBook change ios 8 and older
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        ABAddressBookRegisterExternalChangeCallback(addressBook, addressBookChanged, (__bridge void *)(self));
    }
    
    //config search bar
    [self.searchBar setBackgroundImage:[[UIImage alloc] init]];

    //new variable
    self.contactDictionary = [NSMutableDictionary new];
    self.selectedContactArray = [NSMutableArray new];
    
    //config collection view
    self.hightOfCollectionViewConstraint.constant = 0;
    self.automaticallyAdjustsScrollViewInsets = false;
    [self.selectedContactCollectionView setContentInset:UIEdgeInsetsMake(10, 5, 0, 0)];
    
    // config table view
    self.contactTableView.allowsMultipleSelectionDuringEditing = true;
    self.isSearchTextNull = true;
    self.isSearching = false;
    
    [self getAllContacts];
    
    self.searchBar.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getAllContacts {
    // get contacts
    ContactScanner *contactScanner = [ContactScanner sharedInstance];
    [contactScanner scanAllContactWithCompletionHandler:^(NSArray<Contact *> * _Nullable contacts, NSError * _Nullable error) {
        if (error == nil) {
            self.contactArray = [NSMutableArray arrayWithArray:contacts];
            [self createDictionaryContact];
            self.contactSectionTitleArray = [[[self.contactDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] mutableCopy];
            [self.contactTableView reloadData];
        } else {
            if (error.code == MDAccessContactPermitionDenied) {
                // show AlertView
                UIAlertView *cantLoadContactAlert = [[UIAlertView alloc] initWithTitle: @"Cannot Load Contact" message: @"You must give the app permission to add the contact first." delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
                [cantLoadContactAlert show];
            }
        }
    } andDispatchTo:dispatch_get_main_queue()];
}

/**
 *  Create contactDictionary from contactArray to group contact with first letter of name
 */
- (void)createDictionaryContact {
    [self.contactDictionary removeAllObjects];
    for (Contact *contact in self.contactArray) {
        NSString *firstLetter = [[[contact fullName] substringToIndex:1] uppercaseString];
        unichar firstChar = [firstLetter characterAtIndex:0];
        
        if (firstChar >= 'A' && firstChar <= 'Z') {
            if ([self.contactDictionary objectForKey:firstLetter] == nil) {
                NSMutableArray *temptArray = [[NSMutableArray alloc] initWithObjects:contact, nil];
                [self.contactDictionary setObject:temptArray forKey:firstLetter];
            } else {
                [[self.contactDictionary objectForKey:firstLetter] addObject:contact];
            }
        } else {
            if ([self.contactDictionary objectForKey:@"#"] == nil) {
                NSMutableArray *temptArray = [[NSMutableArray alloc] initWithObjects:contact, nil];
                [self.contactDictionary setObject:temptArray forKey:@"#"];
            } else {
                [[self.contactDictionary objectForKey:@"#"] addObject:contact];
            }
        }

    }
}

#pragma mark - NSNotification

- (void)contactsDidChange:(NSNotification *)notification {
    [self getAllContacts];
}

void addressBookChanged(ABAddressBookRef addressBook, CFDictionaryRef info, void *context)
{
    ContactViewController *contactViewController = (__bridge ContactViewController *)(context);
    [contactViewController getAllContacts];
}

#pragma mark - UITableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([self isSearching] && ![self isSearchTextNull]) {
        return 1;
    }
    return [self.contactSectionTitleArray count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ([self isSearching] && ![self isSearchTextNull]) {
        return nil;
    }
    return [self.contactSectionTitleArray objectAtIndex:section];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if ([self isSearching] && ![self isSearchTextNull]) {
        return nil;
    }
    NSMutableArray *indexList = [NSMutableArray arrayWithCapacity:[self.contactSectionTitleArray count]+1];
    [indexList addObject:UITableViewIndexSearch];
    for (NSString *item in self.contactSectionTitleArray)
        [indexList addObject:[item substringToIndex:1]];
    return indexList;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    if (index == 0)
    {
        [tableView setContentOffset:CGPointZero animated:NO];
        return NSNotFound;
    }
    return index - 1;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    if ([view isMemberOfClass:[UITableViewHeaderFooterView class]]) {
        ((UITableViewHeaderFooterView *)view).backgroundView.backgroundColor = [UIColor colorWithRed:224.0/255 green:224.0/255 blue:224.0/255 alpha:0.7];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self isSearching] && ![self isSearchTextNull]) {
        if ([self.filteredContactArray count] == 0) {
            [self.contactTableView setHidden:YES];
        }
        [self.contactTableView setHidden:NO];
        return [self.filteredContactArray count];
    }
    [self.contactTableView setHidden:NO];
    NSString *sectionTitle = [self.contactSectionTitleArray objectAtIndex:section];
    return [[self.contactDictionary objectForKey:sectionTitle] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CellIdentifier";
    
    ContactTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[ContactTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    return cell;
}

#pragma mark - UITableViewDelegate

// Binding data to cell
- (void)tableView:(UITableView *)tableView willDisplayCell:(ContactTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    id<ContactProtocol> contact;
    // Configure the cell...
    if ([self isSearching] && ![self isSearchTextNull]) {
        contact = [self.filteredContactArray objectAtIndex:indexPath.row];
        cell.fullNameLabel.text = contact.fullName;
        [cell.avatarImageView setImageForContact:contact];
    } else {
        NSString *sectionTitle = [self.contactSectionTitleArray objectAtIndex:indexPath.section];
        NSArray *contactArray = [self.contactDictionary objectForKey:sectionTitle];
        contact = [contactArray objectAtIndex:indexPath.row];
        cell.fullNameLabel.text = contact.fullName;
        [cell.avatarImageView setImageForContact:contact];
    }
    
    //config accessory when select cell
    if ([self.selectedContactArray containsObject:contact]) {
        self.accessoryImage = [UIImage imageNamed:@"accessoryImageChecked"];
    } else {
        self.accessoryImage = [UIImage imageNamed:@"accessoryImageUncheck"];
    }
    [cell.accessoryImageView setImage:self.accessoryImage];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.searchBar resignFirstResponder];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    ContactTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSString *sectionTitle = [self.contactSectionTitleArray objectAtIndex:indexPath.section];
    NSArray *contactArray = [self.contactDictionary objectForKey:sectionTitle];
    Contact *contact;
    
    if ([self isSearching] && !self.isSearchTextNull) {
        contact = [self.filteredContactArray objectAtIndex:indexPath.row];
    } else {
        contact = [contactArray objectAtIndex:indexPath.row];
    }
    
    if ([self.selectedContactArray containsObject:contact]) {
        NSInteger indexRemovedItem = [self.selectedContactArray indexOfObject:contact];
        [self.selectedContactArray removeObject:contact];
        self.accessoryImage = [UIImage imageNamed:@"accessoryImageUncheck"];
        contact.indexPathOfContact = nil;
        
        // adjust layout of UICollectionView
        [self adjustLayoutOfCollectionView];
        //remove item from CollectionView
        NSArray* indexPathArray = [[NSArray alloc] initWithObjects: [NSIndexPath indexPathForItem:indexRemovedItem inSection:0]  , nil];
        [self.selectedContactCollectionView deleteItemsAtIndexPaths:indexPathArray];
        
    } else {
        
        [self.selectedContactArray addObject:contact];
        self.accessoryImage = [UIImage imageNamed:@"accessoryImageChecked"];
        contact.indexPathOfContact = indexPath;

        NSInteger indexInsertedItem = self.selectedContactArray.count == 0 ? 0 : self.selectedContactArray.count - 1;
        [self.selectedContactCollectionView performBatchUpdates:^{
            // insert item to CollectionView
            NSArray* indexPathArray = [[NSArray alloc] initWithObjects: [NSIndexPath indexPathForItem:indexInsertedItem inSection:0]  , nil];
            [self.selectedContactCollectionView insertItemsAtIndexPaths:indexPathArray];
        } completion:^(BOOL finished) {
            // adjust layout of UICollectionView
            [self adjustLayoutOfCollectionView];
            // scroll to item just insert
            [self.selectedContactCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:indexInsertedItem inSection:0]
                                                       atScrollPosition:UICollectionViewScrollPositionRight
                                                               animated:YES];
        }];
    }
    [cell.accessoryImageView setImage:self.accessoryImage];
    self.countSelectedContact.text = [[NSNumber numberWithInteger:[self.selectedContactArray count]] stringValue];
}

- (void)adjustLayoutOfCollectionView {
    if (([self.selectedContactArray count] == 1) && (self.hightOfCollectionViewConstraint.constant == 0)) {
        self.hightOfCollectionViewConstraint.constant = 60;
        [UIView animateWithDuration:.3
                         animations:^{
                             [self.view layoutIfNeeded];
                         }];
        
    } else {
        if ([self.selectedContactArray count] == 0) {
            self.hightOfCollectionViewConstraint.constant = 0;
            [UIView animateWithDuration:.3
                             animations:^{
                                 [self.view layoutIfNeeded];
                             }];
            
        }
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSString *sectionTitle = [self.contactSectionTitleArray objectAtIndex:indexPath.section];
        NSArray *contactArray = [self.contactDictionary objectForKey:sectionTitle];
        Contact *contact;
        
        if ([self isSearching] && !self.isSearchTextNull) {
            [self.filteredContactArray removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
        } else {
            contact = [contactArray objectAtIndex:indexPath.row];
            // update tableview
            [tableView beginUpdates];
            [[self.contactDictionary objectForKey:sectionTitle] removeObject:contact];
            [self.contactArray removeObject:contact];
            if ([[self.contactDictionary objectForKey:sectionTitle] count] > 0)
            {
                // Section is not yet empty, so delete only the current row.
                [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                 withRowAnimation:UITableViewRowAnimationFade];
            }
            else
            {
                // Section is now completely empty, so delete the entire section.
                [self.contactSectionTitleArray removeObject:sectionTitle];
                [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                         withRowAnimation:UITableViewRowAnimationFade];
            }
            [tableView endUpdates];
        }
        
        // update collectionview
        if ([self.selectedContactArray containsObject:contact]) {
            NSInteger indexRemovedItem = [self.selectedContactArray indexOfObject:contact];
            [self.selectedContactArray removeObject:contact];
            self.accessoryImage = [UIImage imageNamed:@"accessoryImageUncheck"];
            contact.indexPathOfContact = nil;
            
            // adjust layout of UICollectionView
            [self adjustLayoutOfCollectionView];
            //remove item from CollectionView
            NSArray* indexPathArray = [[NSArray alloc] initWithObjects: [NSIndexPath indexPathForItem:indexRemovedItem inSection:0]  , nil];
            [self.selectedContactCollectionView deleteItemsAtIndexPaths:indexPathArray];
            
        }
    }
}

#pragma mark - UISearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.isSearching = true;
    if ([searchText isEqualToString:@""]) {
        self.isSearchTextNull = true;
        [self.contactTableView reloadData];
    } else {
        self.isSearchTextNull = false;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.fullName contains[c] %@", searchText];
        self.filteredContactArray = [[self.contactArray filteredArrayUsingPredicate:predicate] mutableCopy];
        
        if ([self.filteredContactArray count] == 0) {
            [self.contactTableView setHidden:YES];
        } else {
            [self.contactTableView reloadData];
        }
    }
}

#pragma mark - UICollectionView Datasource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.selectedContactArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CollectionViewCellIdentifier";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Contact *contact = [self.selectedContactArray objectAtIndex:indexPath.row];
    MDContactImageView *selectedContactImageView = (MDContactImageView *)[cell viewWithTag:1001];
    [selectedContactImageView setImageForContact:contact];
    
    return cell;
}

#pragma mark - UICollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    MDContactImageView *selectedContactImageView = (MDContactImageView *)[cell viewWithTag:1001];
    
    if (![self.blurView isDescendantOfView:selectedContactImageView]) {
        self.blurView = [[UIView alloc] initWithFrame:selectedContactImageView.frame];
        self.blurView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
        self.blurView.layer.cornerRadius = 25;
        self.blurView.layer.masksToBounds = YES;
        
        [selectedContactImageView addSubview:self.blurView];
    }
    
    Contact *contact = [self.selectedContactArray objectAtIndex:indexPath.row];
    [self.contactTableView selectRowAtIndexPath:contact.indexPathOfContact animated:YES scrollPosition:UITableViewScrollPositionTop];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.blurView removeFromSuperview];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10.0;
}

#pragma mark - AddNewContact Delegate

- (void)addNewContact:(Contact *)newContact {
    [self.contactArray addObject:newContact];
    [self.contactDictionary removeAllObjects];
    [self createDictionaryContact];
    self.contactSectionTitleArray = [[[self.contactDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] mutableCopy];
    [self.contactTableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"prepareForSegue: %@", segue.identifier);
    if ([segue.identifier isEqualToString:@"addContactSegue"]) {
        AddContactViewController *destinationViewController = segue.destinationViewController;
        destinationViewController.delegate = self;
    }
}

@end
