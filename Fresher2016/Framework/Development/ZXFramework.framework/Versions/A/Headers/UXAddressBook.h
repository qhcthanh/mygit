/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Alexey Belkevich, 2014
 * Updated by ThemND VNG Coporation, 2015
 * Change Log: Integrate Contact Framework that supports the iOS9
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXTypes.h"

@class UXContact;
@interface UXAddressBook : NSObject

@property (nonatomic, assign) UXContactField fieldsMask;
@property (nonatomic, copy) UXContactFilterBlock filterBlock;
@property (nonatomic, strong) NSArray *sortDescriptors;

/**
 *  Get address book access status
 *
 *  @return An address book access status type
 */
+ (UXAddressBookAccessStatus)addressBookAccessStatus;

/**
 *  Request access address book on main queue
 *
 *  @param completionBlock A block object to be executed when the request address book finishes successfully.
 */
+ (void)requestAccess:(void (^)(BOOL granted, NSError * error))completionBlock;

/**
 *  Request access address book on a dispatch queue
 *
 *  @param queue           The background queue executes request access contact phone book
 *  @param completionBlock A block object to be executed when the request address book finishes successfully.
 */
+ (void)requestAccessOnQueue:(dispatch_queue_t)queue
                  completion:(void (^)(BOOL granted, NSError * error))completionBlock;

/**
 *  Load Contact on phone book on main queue
 *
 *  @param completionBlock A block object to be executed when loading finishes successfully.
 */
- (void)loadContacts:(void (^)(NSArray *contacts, NSError *error))completionBlock;

/**
 *  Load Contact on phone book on a dispatch queue
 *
 *  @param queue           The background queue executes loading contact phone book
 *  @param completionBlock A block object to be executed when loading finishes successfully.
 */
- (void)loadContactsOnQueue:(dispatch_queue_t)queue
                 completion:(void (^)(NSArray *contacts, NSError *error))completionBlock;
/**
 *  Observe address book changes
 *
 *  @param callback A block object to be executed when having contact changes
 */
- (void)startObserveChangesWithCallback:(void (^)())callback;

/**
 *  Stop observing address changes
 */
- (void)stopObserveChanges;

/**
 *  Get Contact with a record id
 *
 *  @param recordID An integer number that indentifies a contact on the address book.
 *
 *  @return return A contact if the contact is exist.
 */
- (UXContact *)getContactByRecordID:(NSNumber *)recordID;

@end
