//
//  PhoneCell.m
//  ZaloContact
//
//  Created by qhcthanh on 6/3/16.
//  Copyright © 2016 qhcthanh. All rights reserved.
//

#import "PhoneCell.h"

@implementation PhoneCell

-(void)awakeFromNib {
    [super awakeFromNib];
}

-(void)cleanUI {
    phoneImageView.image = nil;
    phoneLabel.text = nil;
}

-(void)renderUI:(PhoneType)type phone:(NSString*)phone {
    UIImage* image = nil;
    switch (type) {
        case kPhoneTypeFax:
            image = [UIImage imageNamed:@"Fax-50"];
            break;
        case kPhoneTypeHome:
            image = [UIImage imageNamed:@"Home-50"];
            break;
        case kPhoneTypeMobile:
            image = [UIImage imageNamed:@"Phone-50"];
            break;
        default:
            break;
    }
    phoneImageView.image = image;
    phoneLabel.text = phone;
}

@end
