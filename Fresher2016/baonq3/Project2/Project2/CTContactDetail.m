//
//  CTContactDetail.m
//  Project2
//
//  Created by BaoNQ on 5/23/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "CTContactDetail.h"

@interface CTContactDetail () {
}
@end

@implementation CTContactDetail

- (id)init {
    self = [super init];
    if (self == nil)
        return nil;
    self.phoneNumbers = [[NSMutableArray alloc] init];
    return self;
}

- (void)getProfileImageWithCompletionHandler:(GetProfileImageCompletionHandler)completionHandler dispatchTo:(dispatch_queue_t)returnedQueue {
    dispatch_queue_t dispatchQueue = (returnedQueue != nil) ? returnedQueue : dispatch_get_main_queue();
    ContactImageCaching *cachedImage = [ContactImageCaching sharedContactImageCaching];
    
    __block UIImage *profileImage = [cachedImage imageForKey:self.contactIdentifier];
    if (profileImage != nil) {
        if (completionHandler != nil) {
            dispatch_async(dispatchQueue, ^{
                completionHandler(profileImage, nil);
            });
        }
        return;
    }
    dispatch_async([CTContactDetail getInternalQueue], ^{
        //Get 2 first characters of firstname and lastname.
        NSString *textForImage;
        if ([self.firstName length] > 0 && [self.lastName length] > 0)
            textForImage = [NSString stringWithFormat:@"%@%@", [self.firstName substringToIndex:1], [self.lastName substringToIndex:1]];
        else {
            if ([self.lastName length] > 0)
                textForImage = [NSString stringWithFormat:@"%@", [self.lastName substringToIndex:1]];
            else if ([self.firstName length] > 0)
                textForImage = [NSString stringWithFormat:@"%@", [self.firstName substringToIndex:1]];
            else
                textForImage = @"#";
        }
        
        //Create default color for profile picture
        NSMutableArray *colors = [[NSMutableArray alloc] init];
        [colors addObject:[UIColor colorWithRed:112.0/255.0 green:207.0/255.0 blue:155.0/255.0 alpha:1.0]];
        [colors addObject:[UIColor colorWithRed:212.0/255.0 green:137.0/255.0 blue:127.0/255.0 alpha:1.0]];
        [colors addObject:[UIColor colorWithRed:154.0/255.0 green:185.0/255.0 blue:221.0/255.0 alpha:1.0]];
        [colors addObject:[UIColor colorWithRed:161.0/255.0 green:165.0/255.0 blue:179.0/255.0 alpha:1.0]];
        
        NSUInteger i = rand() % 4;
        
        // Create image with given text.
        profileImage = [UIImage imageWithText:textForImage imageSize:CGSizeMake(70, 70) textColor:[UIColor whiteColor] backgroundColor:[colors objectAtIndex:i]];
        
        // Mask with circle image.
        UIImage *mask = [UIImage imageNamed:@"mask4.png"];
        profileImage = [profileImage maskImageWith:mask];
        
        // Add masked image to cache
        [cachedImage cacheImage:profileImage forKey:self.contactIdentifier];
        
        if (completionHandler != nil) {
            dispatch_async(dispatchQueue, ^{
                completionHandler(profileImage, nil);
            });
        }
    });
}

+ (dispatch_queue_t)getInternalQueue {
    static dispatch_once_t onceToken;
    static dispatch_queue_t internalQueue;
    dispatch_once(&onceToken, ^{
        internalQueue = dispatch_queue_create("com.vng.loadProfileImageQueue", DISPATCH_QUEUE_CONCURRENT);
    });
    return internalQueue;
}

@end
