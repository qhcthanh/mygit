//
//  LetterNode.swift
//  testSuggestion
//
//  Created by manhduydl on 8/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

import Foundation


public class LetterNode {
    
    var key : String!
    var parentNode : LetterNode?
    var childrentNode : [String : LetterNode]
    var isEnd : Bool
    var level : Int
    var score : Int
    var count : Int
    var time : String
    
    init(key : String) {
        self.key = key
        self.childrentNode = [:]
        self.isEnd = false
        self.level = 0
        self.score = 0
        self.count = 0
        self.time = ""
    }
    
    convenience init() {
        self.init(key : "-")
    }
    
    func numberChild() -> Int {
        return self.childrentNode.count
    }
    
    func isEndNode() -> Bool {
        return self.isEnd
    }
    
    func setEndNode(bool : Bool) {
        self.isEnd = bool
    }
    
    func isRoot() -> Bool {
        return self.key == "-" && self.parentNode == nil
    }
    
    func find(key : String) -> (node : LetterNode?, found : Bool) {
        if let child = self.childrentNode[key] {
            return (child, true)
        }
        return (nil, false)
    }
    
    func addChild(withKey key : String!) -> LetterNode {
        var childNode : LetterNode?
        
        if let temptNode = self.childrentNode[key] {
            childNode = temptNode
        } else {
            childNode = LetterNode.init(key: key)
            childNode?.level = self.level + 1
            childNode?.parentNode = self
            childNode?.time = NSDate().description
            self.childrentNode[key] = childNode
        }

        return childNode!
    }
    
    func addChild(withKey key : String!) -> (node : LetterNode, isNewNode : Bool) {
        var childNode : LetterNode?
        var isNewNode : Bool
        
        if let temptNode = self.childrentNode[key] {
            childNode = temptNode
            isNewNode = false
        } else {
            childNode = LetterNode.init(key: key)
            childNode?.level = self.level + 1
            childNode?.parentNode = self
            //childNode?.time = NSDate().description
            self.childrentNode[key] = childNode
            isNewNode = true
        }
        
        return (childNode!, isNewNode)
    }
    
    func addChild(withKey key : String, initTime time : String, numberOfUse count : Int) -> LetterNode {
        var childNode : LetterNode?
        
        if let temptNode = self.childrentNode[key] {
            childNode = temptNode
        } else {
            childNode = LetterNode.init(key: key)
            childNode?.level = self.level + 1
            childNode?.parentNode = self
            childNode?.time = time
            childNode?.count = count
            self.childrentNode[key] = childNode
        }
        
        return childNode!
    }
    
    func findChild(withKey key : String!) -> LetterNode? {
        if let temptNode = self.childrentNode[key] {
            return temptNode
        }
        return nil
    }

    func printNode(var indent: String, leaf: Bool) {
        
        print(indent, terminator: "")
        if leaf {
            print("\\-", terminator: "")
            indent += " "
        } else {
            print("|-", terminator: "")
            indent += "| "
        }
        
        print(self.key)
        
        var i = 0
        for (_, node) in self.childrentNode {
            node.printNode(indent, leaf: i == self.childrentNode.count-1)
            i+=1
        }
        
    }
}
