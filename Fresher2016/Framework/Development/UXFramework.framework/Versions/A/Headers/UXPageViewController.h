/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXViewController.h"

typedef void(^UXDidChangedPageBlock)(NSInteger currentPage, NSString *title);

@class UXPageTitleView;
@interface UXPageViewController : UXViewController

/**
 *  Container display title collection
 */
@property (nonatomic, strong) UIView *centerContainerView;
@property (nonatomic, strong) UIScrollView *paggingScrollView;

/**
 *  Display title collection container
 */
@property (nonatomic, strong) UXPageTitleView *pageTitleView;

/**
 *  Change the page callback
 */
@property (nonatomic, copy) UXDidChangedPageBlock didChangedPageCompleted;

/**
 *  For Cocotouch design of iOS, UITabbarController, most do support 5 controllers, the excess will be more to replace, you can see five more normal memory management scheme, so here is promoting the use of maximum 5 controllers
 */
@property (nonatomic, strong) NSArray *viewControllers;

/**
 *  Initialization with the left sliding menu of Viewer Controller
 *
 *  @param leftViewController  Target the left of the menu controller object
 *
 *  @return Returns Viewer controller object
 */
- (instancetype)initWithLeftViewController:(UIViewController *)leftViewController;

/**
 *  Initialization with the right of the sliding menu of Viewer Controller
 *
 *  @param rightViewController Target the right of the menu controller object
 *
 *  @return Returns Viewer controller object
 */
- (instancetype)initWithRightViewController:(UIViewController *)rightViewController;

/**
 *  Initialize with sliding around the Viewer menu controller
 *
 *  @param leftViewController  Target left menu controller object
 *  @param rightViewController Target right menu controller object
 *
 *  @return Returns Viewer controller object
 */
- (instancetype)initWithLeftViewController:(UIViewController *)leftViewController rightViewController:(UIViewController *)rightViewController;

/**
 *  Get the current page number
 *
 *  @return Return the current page number
 */
- (NSInteger)getCurrentPageIndex;

/**
 *  Set the current page to the page you want
 *
 *  @param currentPage Target Page
 *  @param animated    Whether the animation settings
 */
- (void)setCurrentPage:(NSInteger)currentPage animated:(BOOL)animated;

/**
 *  Set controller data source, carried reload method
 */
- (void)reloadData;


@end
