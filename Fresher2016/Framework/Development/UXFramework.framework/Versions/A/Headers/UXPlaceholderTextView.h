/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@interface UXPlaceholderTextView : UITextView

@property (strong, nonatomic) NSString *placeholder;
@property (nonatomic, strong) UIColor *placeholderColor;
@property (nonatomic, retain) NSUndoManager *undoTextManager;

- (void)updateShouldDrawPlaceholder;

- (void)removeUndoTextmanager;

@end
