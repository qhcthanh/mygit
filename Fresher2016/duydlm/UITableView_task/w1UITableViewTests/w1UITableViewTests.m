//
//  w1UITableViewTests.m
//  w1UITableViewTests
//
//  Created by manhduydl on 5/18/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ContactScanner.h"

@interface w1UITableViewTests : XCTestCase

@end

@implementation w1UITableViewTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

//- (void)testExample {
//    // This is an example of a functional test case.
//    // Use XCTAssert and related functions to verify your tests produce the correct results.
//}
//
//- (void)testPerformanceExample {
//    // This is an example of a performance test case.
//    [self measureBlock:^{
//        // Put the code you want to measure the time of here.
//    }];
//}

- (void)testScanAllContactWithNilCompletionBlock {
    ContactScanner *contactScanner = [ContactScanner sharedInstance];
    [contactScanner scanAllContactWithCompletionHandler:nil andDispatchTo:dispatch_get_main_queue()];
}

- (void)testScanAllContactWithNilQueue {
    ContactScanner *contactScanner = [ContactScanner sharedInstance];
    [contactScanner scanAllContactWithCompletionHandler:^(NSArray<Contact *> * _Nullable contacts, NSError * _Nullable error) {
    } andDispatchTo:nil];
}

- (void)testScanAllContactWithNilQueueNilCompletionBlock {
    ContactScanner *contactScanner = [ContactScanner sharedInstance];
    [contactScanner scanAllContactWithCompletionHandler:nil andDispatchTo:nil];
}

- (void)testloadContactInMultiThread {
    ContactScanner *contactScanner = [ContactScanner sharedInstance];
    int threads = 10;
    dispatch_group_t loadContactGroup = dispatch_group_create();
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    for(int i = 0;i < threads; i++) {
        dispatch_group_enter(loadContactGroup);
        dispatch_async(queue, ^{
            [contactScanner scanAllContactWithCompletionHandler:^(NSArray<Contact *> * _Nullable contacts, NSError * _Nullable error) {
                if (error != nil)
                    NSLog(@"%ld\n", error.code);
                else {
                    NSLog(@"Contacts size: %lu",(unsigned long)contacts.count);
                }
                dispatch_group_leave(loadContactGroup);
            } andDispatchTo:queue];
        });
    }
    dispatch_group_wait(loadContactGroup, DISPATCH_TIME_FOREVER);
}

@end
