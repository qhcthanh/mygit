//
//  SNSticker+NSCopying.m
//  CameraGPUImage
//
//  Created by BaoNQ on 8/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "SNSticker+NSCopying.h"

@implementation SNSticker (NSCopying)

- (id)copyWithZone:(NSZone *)zone {
    return [[SNSticker alloc] initWithFrame:self.frame image:self.image atZPosition:0];
}

@end
