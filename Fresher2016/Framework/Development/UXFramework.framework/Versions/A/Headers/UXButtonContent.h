/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@class UXStyle;
@class UXButton;
@interface UXButtonContent : NSObject {
}

@property (nonatomic, copy)   NSString* title;
@property (nonatomic, copy)   NSString* imageURL;
@property (nonatomic, strong) UIImage*  image;
@property (nonatomic, strong) UXStyle*  style;
@property (nonatomic, assign) UXButton* button;

- (id)initWithButton:(UXButton*)button;

- (void)reload;
- (void)stopLoading;

@end
