//
//  BLGiphyBottomBarView.swift
//  EmojiKeyboard
//
//  Created by Quach Ha Chan Thanh on 8/23/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import UIKit

@objc public enum BLGiphyBottomOptionItem: Int {
    case Home
    case MyFavorite
    case Recent
    case Trend
}

@objc public protocol BLGiphyBottomBarViewDelegate: NSObjectProtocol {
    optional func didSelectItem(itemOption: BLGiphyBottomOptionItem)
}

public class BLGiphyBottomBarView: UIView {
    
    // MARK: UI Properties
    public let globalButton = UIButton.init(type: .System)
    public let backspaceButton = UIButton.init(type: .System)
    public var currentSelectedControl: BLGiphyButtonControl?
    
    public var homeButtonView: BLGiphyButtonControl!
    public var myButtonView: BLGiphyButtonControl!
    public var recentButtonView: BLGiphyButtonControl!
    public var trendButtonView: BLGiphyButtonControl!
    
    public weak var delegate: BLGiphyBottomBarViewDelegate?
    
    
    public convenience init() {
        self.init(frame: CGRectZero)
        
        setupView()
    }
    
    /**
     Inititialize subview's
     */
    public func setupView() {
        
        self.addSubview(globalButton)
        self.addSubview(backspaceButton)
        
        globalButton.setImage(UIImage(named: "Globe.png"), forState: .Normal)
        globalButton.imageView?.contentMode = .ScaleAspectFit
        globalButton.tintColor = .whiteColor()
        globalButton.imageEdgeInsets = UIEdgeInsetsMake(5, 0, 5, 0)
        
        backspaceButton.imageView?.contentMode = .ScaleAspectFit
        backspaceButton.setImage(UIImage(named: "Backspace.png"), forState: .Normal)
        backspaceButton.tintColor = .whiteColor()
        backspaceButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        
        self.createPageButton()
        self.updateConstraint()
        
    }
    
    /**
     Update contrainsts subView's need to call when reload UI
     */
    func updateConstraint() {

        globalButton.constrain(.Top, to: . Top, of: self)
        globalButton.constrain(.Bottom, to: .Bottom, of: self, offsetBy: -3)
        globalButton.constrain(.Left, to: . Left, of: self)
        globalButton.constrain(.Width, being: .Equal, to: .Width, of: self, multipliedBy: 1/6)
        
        backspaceButton.constrain(.Top, to: . Top, of: globalButton)
        backspaceButton.constrain(.Bottom, to: .Bottom, of: globalButton)
        backspaceButton.constrain(.Right, to: . Right, of: self)
        backspaceButton.constrain(.Width, to: .Width, of: globalButton)
        
        self.homeButtonView.constrain(.Top, to: . Top, of: globalButton)
        self.homeButtonView.constrain(.Bottom, to: .Bottom, of: globalButton)
        self.homeButtonView.constrain(.Left, to: . Right, of: globalButton)
        self.homeButtonView.constrain(.Width, to: .Width, of: globalButton)
        
        self.myButtonView.constrain(.Top, to: . Top, of: globalButton)
        self.myButtonView.constrain(.Bottom, to: .Bottom, of: globalButton)
        self.myButtonView.constrain(.Left, to: . Right, of: self.homeButtonView)
        self.myButtonView.constrain(.Width, to: .Width, of: globalButton)
        
        self.trendButtonView.constrain(.Top, to: . Top, of: globalButton)
        self.trendButtonView.constrain(.Bottom, to: .Bottom, of: globalButton)
        self.trendButtonView.constrain(.Left, to: . Right, of: self.recentButtonView)
        self.trendButtonView.constrain(.Width, to: .Width, of: globalButton)
    }
    
    /**
     Create option button's
     */
    private func createPageButton() {
        // self.homeButtonView
        self.homeButtonView = BLGiphyButtonControl(image: UIImage(named: "Home"), selectedImage: UIImage(named: "Home Filled"), select: true)
        self.addSubview(self.homeButtonView)

        
        self.homeButtonView.onSelected()
        self.currentSelectedControl = self.homeButtonView
        self.homeButtonView.addTarget(self, action: #selector(onTouchUpInsideActiveControl), forControlEvents: .TouchUpInside)
        
        // self.myButtonView
        self.myButtonView = BLGiphyButtonControl(image: UIImage(named: "Happy"), selectedImage: UIImage(named: "Happy Filled"))
        self.addSubview(self.myButtonView)
        
        self.myButtonView.addTarget(self, action: #selector(onTouchUpInsideActiveControl), forControlEvents: .TouchUpInside)
        
        // self.recentButtonView
        self.recentButtonView = BLGiphyButtonControl(image: UIImage(named: "Time"), selectedImage: UIImage(named: "Time Filled"))
        self.addSubview(self.recentButtonView)
        
        self.recentButtonView.constrain(.Top, to: . Top, of: globalButton)
        self.recentButtonView.constrain(.Bottom, to: .Bottom, of: globalButton)
        self.recentButtonView.constrain(.Left, to: . Right, of: self.myButtonView)
        self.recentButtonView.constrain(.Width, to: .Width, of: globalButton)
        
        self.recentButtonView.addTarget(self, action: #selector(onTouchUpInsideActiveControl), forControlEvents: .TouchUpInside)
        
        // self.trendButtonView
        self.trendButtonView = BLGiphyButtonControl(image: UIImage(named: "Line Chart"), selectedImage: UIImage(named: "Line Chart Filled"))
        self.addSubview(self.trendButtonView)
        
        self.trendButtonView.addTarget(self, action: #selector(onTouchUpInsideActiveControl), forControlEvents: .TouchUpInside)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        self.self.homeButtonView.updateConstraints()
    }
    
    @objc func onTouchUpInsideActiveControl(sender: BLGiphyButtonControl!) {

        if self.currentSelectedControl != sender {
            if let currentSelectedControl = self.currentSelectedControl {
                currentSelectedControl.onDeselect()
            }
            self.currentSelectedControl = sender
            sender.onSelected()
            
            if let delegate = self.delegate, let selectSelector = delegate.didSelectItem {
                switch sender {
                case self.homeButtonView:
                    selectSelector(.Home)
                    break
                case self.myButtonView:
                    selectSelector(.MyFavorite)
                    break
                case self.recentButtonView:
                    selectSelector(.Recent)
                    break
                case self.trendButtonView:
                    selectSelector(.Trend)
                    break
                default:
                    break
                }
            }
           
        }
    }
    
    /**
     Call when user select option button manually. This function will call touchUpInside target in this option button
     
     - parameter option: The option to select
     */
    public func selectItemOption(option: BLGiphyBottomOptionItem) {
        var buttonSelect: BLGiphyButtonControl?
        
        switch option {
            case .Home:
                buttonSelect = homeButtonView
            case .MyFavorite:
                buttonSelect = myButtonView
            case .Trend:
                buttonSelect = trendButtonView
            case .Recent:
                buttonSelect = recentButtonView
        }
        
        if let buttonSelect = buttonSelect {
            self.onTouchUpInsideActiveControl(buttonSelect)
        }
    }
}

