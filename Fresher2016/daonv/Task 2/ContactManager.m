//
//  ContactLoadManager.m
//  ZaloContactInviting
//
//  Created by VanDao on 5/26/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import "ContactManager.h"

@interface ContactManager(){
    dispatch_queue_t serialQueue;
}

@end

@implementation ContactManager
- (id)init{
    self = [super init];
    
    if (self){
        serialQueue = dispatch_queue_create("load Contact Queue", NULL);
    }
    
    return self;
}

+ (instancetype)sharedManager{
    static ContactManager *sharedContactManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedContactManager = [[self alloc] init];
    });
    
    return sharedContactManager;
}

+ (int)isIOS9Device{
    // Get version of device
    static int isIOS9 = -1;
    if (isIOS9 == -1){
        isIOS9 = ([[[UIDevice currentDevice] systemVersion] compare:@"9.0" options:NSNumericSearch] != NSOrderedAscending) ? 1 : 0;
    }
    
    return isIOS9;
}

- (void)requestPermissionAccessWithCallbackBlock: (_Nullable getPermissionBlock)callbackBlock onQueue:(_Nullable dispatch_queue_t)callbackQueue{
    dispatch_async(serialQueue, ^{
        __block ContactAuthorizationStatus state;
        
        if ([ContactManager isIOS9Device]){
            // iOS 9.0 or later
            if ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts] == CNAuthorizationStatusNotDetermined){
                CNContactStore *contactStore = [[CNContactStore alloc]init];
                
                // Request permission
                [contactStore requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error){
                    if (granted){
                        // Have permission
                        if (callbackBlock){
                            state = kPermissionAuthorized;
                            dispatch_queue_t queue = callbackQueue ? callbackQueue : dispatch_get_main_queue();
                            
                            dispatch_async(queue, ^{
                                callbackBlock(state);
                            });
                        }
                    }else{
                        //Don't have permission
                        if (callbackBlock){
                            state = kPermissionDenied;
                            dispatch_queue_t queue = callbackQueue ? callbackQueue : dispatch_get_main_queue();
                            
                            dispatch_async(queue, ^{
                                callbackBlock(state);
                            });
                        }
                    }
                }];
            } else {
                if ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts] == CNAuthorizationStatusAuthorized)
                    state = kPermissionAuthorized;
                else if ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts] == CNAuthorizationStatusDenied)
                    state = kPermissionDenied;
                else if ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts] == CNAuthorizationStatusRestricted)
                    state = kPermissionRetricted;
                
                if (callbackBlock){
                    dispatch_queue_t queue = callbackQueue ? callbackQueue : dispatch_get_main_queue();
                    
                    dispatch_async(queue, ^{
                        callbackBlock(state);
                    });
                }
            }
        }
        else{
            // iOS older 9.0
            if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined){
                
                // Request permission
                ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error){
                    if (granted){
                        // Have permission
                        if (callbackBlock){
                            state = kPermissionAuthorized;
                            dispatch_queue_t queue = callbackQueue ? callbackQueue : dispatch_get_main_queue();
                            
                            dispatch_async(queue, ^{
                                callbackBlock(state);
                            });
                        }
                    } else {
                        // Don't have permission
                        if (callbackBlock){
                            state = kPermissionDenied;
                            dispatch_queue_t queue = callbackQueue ? callbackQueue : dispatch_get_main_queue();
                            
                            dispatch_async(queue, ^{
                                callbackBlock(state);
                            });
                        }
                    }
                });
            } else {
                if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized)
                    state = kPermissionAuthorized;
                else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied)
                    state = kPermissionDenied;
                else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted)
                    state = kPermissionRetricted;
                
                if (callbackBlock){
                    dispatch_queue_t queue = callbackQueue ? callbackQueue : dispatch_get_main_queue();
                    
                    dispatch_async(queue, ^{
                        callbackBlock(state);
                    });
                }
            }
        }
    });
}

- (void)getAllContactWithCallbackBlock:(_Nullable loadContactBlock)callbackBlock onQueue: (_Nullable dispatch_queue_t) callbackQueue{
    // Get permission
    [self requestPermissionAccessWithCallbackBlock:^(ContactAuthorizationStatus state){
        if (state != kPermissionAuthorized){
            // If dont' have permission
            if (callbackBlock){
                // If have callback block
                dispatch_queue_t queue = callbackQueue ? callbackQueue : dispatch_get_main_queue();
                dispatch_async(queue, ^{
                    callbackBlock(nil, [NSError errorWithDomain:@"" code:state userInfo:nil]);
                });
            }
        }
        else {
            // If have permission
            // Load all contact
            dispatch_async(serialQueue, ^{
                // Get contact list
                NSMutableArray<ContactDetail *> *contactList;
                
                if ([ContactManager isIOS9Device]){
                    // iOS 9.0 or later
                    contactList = [self getAllContactUsingContactFrameWork];
                }
                else{
                    // iOS older 9.0
                    contactList = [self getAllContactUsingABAddressBookFrameWork];
                }
                
                if (callbackBlock){
                    dispatch_queue_t queue = callbackQueue ? callbackQueue : dispatch_get_main_queue();
                    dispatch_async(queue, ^{
                        callbackBlock(contactList, nil);
                    });
                }
            });
        }
    } onQueue:callbackQueue];
}

- (NSMutableArray *)getAllContactUsingContactFrameWork{
    //iOS 9.0 or later
    NSMutableArray<ContactDetail *> *contactList = [[NSMutableArray alloc]init];

    if([CNContactStore class]) {
        NSError* contactError;
        CNContactStore* addressBook = [CNContactStore new];
        
        [addressBook containersMatchingPredicate:[CNContainer predicateForContainersWithIdentifiers: @[addressBook.defaultContainerIdentifier]] error:&contactError];
        NSArray * keysToFetch = @[CNContactFamilyNameKey,
                                  CNContactGivenNameKey,
                                  CNContactIdentifierKey,
                                  CNContactImageDataKey,
                                  CNContactPhoneNumbersKey];
        
        CNContactFetchRequest * request = [[CNContactFetchRequest alloc]initWithKeysToFetch:keysToFetch];
        [addressBook enumerateContactsWithFetchRequest:request error:&contactError usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop){
            ContactDetail *newContact = [[ContactDetail alloc]init];
            
            newContact.identifier = contact.identifier;
            newContact.givenName = contact.givenName;
            newContact.familyName = contact.familyName;
            // Get all phone numbers
            for (CNLabeledValue *label in contact.phoneNumbers){
                NSString *phoneNumber = [label.value stringValue];
                if (phoneNumber)
                    [newContact.phoneNumbers addObject: phoneNumber];
            }
            // Get image
            newContact.image = [UIImage imageWithData:contact.imageData];
            
            [contactList addObject:newContact];
        }];
    }
    
    return contactList;
}

- (NSMutableArray *)getAllContactUsingABAddressBookFrameWork{
    // iOS older 9.0
    NSMutableArray<ContactDetail *> *contactList = [[NSMutableArray alloc]init];
    
    CFErrorRef *error = NULL;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex numberOfPeople = ABAddressBookGetPersonCount(addressBook);
    
    // Convert the contact with ABRecordRef type to ContactLoadDetail type
    CFIndex num = numberOfPeople;
    for(CFIndex i = 0; i < num; i++) {
        ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
        ContactDetail *newContact = [[ContactDetail alloc]init];
        
        newContact.identifier = [NSString stringWithFormat:@"%i", ABRecordGetRecordID(person)];
        newContact.givenName = CFBridgingRelease(ABRecordCopyValue(person, kABPersonFirstNameProperty));
        newContact.familyName = CFBridgingRelease(ABRecordCopyValue(person, kABPersonLastNameProperty));
        // Get all phone number
        ABMultiValueRef multi = ABRecordCopyValue(person, kABPersonPhoneProperty);
        CFIndex count = ABMultiValueGetCount(multi);
        for (CFIndex j = 0; j < count; j++) {
            NSString* phone = CFBridgingRelease(ABMultiValueCopyValueAtIndex(multi, j));
            [newContact.phoneNumbers addObject:phone];
        }
        CFRelease(multi);
        // Get image
        newContact.image = [UIImage imageWithData: CFBridgingRelease(ABPersonCopyImageData(person))];
        
        [contactList addObject:newContact];
    }
    
    CFRelease(addressBook);
    CFRelease(allPeople);
    
    return contactList;
}

@end
