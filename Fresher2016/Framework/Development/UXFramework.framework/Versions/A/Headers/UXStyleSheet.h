/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class UXStyle;
@interface UXStyleSheet : NSObject {

	NSMutableDictionary* _styles;
}

+ (UXStyleSheet*)globalStyleSheet;
+ (void)setGlobalStyleSheet:(UXStyleSheet*)styleSheet;

- (UXStyle*)styleWithSelector:(NSString*)selector;
- (UXStyle*)styleWithSelector:(NSString*)selector forState:(UIControlState)state;

- (void)freeMemory;

- (UIFont*)styledSystemFontOfSize:(CGFloat)fontSize;
- (UIFont*)boldStyledSystemFontOfSize:(CGFloat)fontSize;
- (UIFont*)italicStyledSystemFontOfSize:(CGFloat)fontSize;

@end
