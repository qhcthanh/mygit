//
//  BLDBManager.swift
//  KeyboardDatabaseHaha
//
//  Created by VanDao on 8/31/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

import Foundation

let databasePath: String! = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.CachesDirectory, NSSearchPathDomainMask.UserDomainMask, true).first!.stringByAppendingString("/Dictionary.db")

class BLDBManager : NSObject {
    
    // Singleton type
    static let shareIntance = BLDBManager()
    
    var propertyCache: [String : [String]]! = [:]
    var internalSerialQueue: dispatch_queue_t = dispatch_queue_create("com.daonv.BLDBManager.internalSerialQueue", DISPATCH_QUEUE_SERIAL)
    
    /**
     Create a table for model object to singleton database
     
     - parameter _class: The class that want to create table in database
     */
    func createTableForClass(_class: AnyClass) {
        
        dispatch_async(internalSerialQueue) { 
            var sqlString: String! = nil
            
            switch String(_class) {
                
            case String(BLSingleWord):
                sqlString = BLSingleWord.getSQLCommandForCreateTable()
            case String(BLMultiWord):
                sqlString = BLMultiWord.getSQLCommandForCreateTable()
            default:
                break
            }
            
            if let sql = sqlString {
                sharedDatabase().executeStatements(sql)
            }
        }
    }
    
    /**
     Fetch number of objects with highest score in database with specifier matching
     
     - parameter _class:   The class model want to fetch
     - parameter matching: The matching in where sql command
     - parameter number:   Number of objects want to fetch
     
     - returns: <#return value description#>
     */
    func fetchObjects(_class: AnyClass, matching: String, number: Int) -> [AnyObject] {
        
        NSDate.startCount()
        
        var result: [AnyObject]! = []

        dispatch_sync(internalSerialQueue) {
            
            let sql: String = "SELECT * FROM \(String(_class)) WHERE \(matching) ORDER BY score DESC LIMIT \(number)"
            
            sharedDatabase().executeStatements(sql) {(res) -> Int32 in
                
                switch String(_class) {
                    
                case String(BLSingleWord):
                    result.append(BLSingleWord.init(dictionary: res))
                case String(BLMultiWord):
                    result.append(BLMultiWord.init(dictionary: res))
                default:
                    break
                }
                
                return 0
            }
        }
        
        print("Hello: \(NSDate.endCount())")
        
        return result
    }
    
    /**
     Fetch number of object with highest score in database with specifier matching and return in callback block
     
     - parameter _class:     The class model want to fetch
     - parameter matching:   The matching in where sql command
     - parameter number:     Number of objects want to fetch
     - parameter completion: The callback block that contain list of result object
     */
    func fetchObjects(_class: AnyClass, matching: String, number: Int, completion: (res: [BLObject]) -> Void)  {
        
        dispatch_sync(internalSerialQueue) {
            
            var result: [BLObject]! = []
            let sql: String = "SELECT * FROM \(String(_class)) WHERE \(matching) ORDER BY score DESC LIMIT \(number)"
            
            sharedDatabase().executeStatements(sql) {(res) -> Int32 in
                
                switch String(_class) {
                    
                case String(BLSingleWord.classForCoder()):
                    result.append(BLSingleWord.init(dictionary: res))
                case String(BLMultiWord.classForCoder()):
                    result.append(BLMultiWord.init(dictionary: res))
                default:
                    break
                }
                
                return 0
            }
            
            completion(res: result)
        }
    }
    
    /**
     Update list of objects with specifier class model
     
     - parameter _class:  The class model want to update
     - parameter objects: The list of objects want to update
     */
    func updateObjects(_class: AnyClass, objects: [BLObject]) {
        
        dispatch_sync(internalSerialQueue) {
            
            if let sqlCommand = self.createUpdateCommand(_class, objects: objects) {
                sharedDatabase().executeStatements(sqlCommand)
            }
        }
    }
    
    /**
     Create update sql command with specifier class model and list of objects
     
     - parameter _class:  The class model want to update
     - parameter objects: The list of objects want to update
     
     - returns: The update sql command
     */
    private func createUpdateCommand(_class: AnyClass, objects: [BLObject]) -> String? {
        
        if (!(objects.count > 0)) {
            return nil
        }
        
        let propertyNames = propertyNamesForClass(_class)
        var sql: String = "INSERT OR REPLACE INTO \(String(_class))(\(propertyNames.joinWithSeparator(", "))) VALUES "
        
        for index in (objects.startIndex..<objects.endIndex) {
            
            var propertyValues = [String]()
            let object = objects[index]
            for property in propertyNames {
                propertyValues.append(String("'\(object.valueForKey(property)!)'"))
            }
            sql += "(\(propertyValues.joinWithSeparator(", ")))" + ((index.advancedBy(1) == objects.endIndex) ? ";" : ",")
        }
        
        return sql
    }
    
    /**
     Fetch and cache properties of class model that exists in table database
     
     - parameter _class: The class model
     
     - returns: The list of property of class model that exist in table database
     */
    private func propertyNamesForClass(_class: AnyClass) -> Array<String> {
        
        let className = String(_class)
        var propertyNames: [String]! = propertyCache[className]
        
        if propertyNames == nil {
            propertyNames = [];
            
            // retrieve the properties via the class_copyPropertyList function
            var count: UInt32 = 0;
            let properties = class_copyPropertyList(_class, &count);
            
            // iterate each objc_property_t struct
            for i: UInt32 in 0 ..< count {
                let property = properties[Int(i)];
                
                // retrieve the property name by calling property_getName function
                let cname = property_getName(property);
                
                // covert the c string into a Swift string
                let name = String.fromCString(cname);
                propertyNames.append(name!);
            }
            
            // release objc_property_t structs
            propertyCache[className] = propertyNames
            free(properties);
        }
        
        return propertyNames;
    }
}

/**
 The share database singleton used in BLDBManager
 
 - returns: The singleton `FMDatabase` object
 */
private func sharedDatabase() -> FMDatabase {
    
    var database:FMDatabase?
    
    var tokenOnce: dispatch_once_t = 0
    dispatch_once(&tokenOnce) {
        database = FMDatabase.init(path: databasePath)
        database!.openWithFlags(6)
    }
    
    return database!
}