/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "AFURLResponseSerialization.h"

@interface AFFileRequestOperation : NSOperation

- (instancetype)initWithURL:(NSURL*)url;

@property (readwrite, nonatomic, strong) NSURL* URL;

/**
 *  The dispatch queue for `completionBlock`. If `NULL` (default), the main queue is used.
 */
@property (nonatomic, strong) dispatch_queue_t completionQueue;

/**
 *  The dispatch group for `completionBlock`. If `NULL` (default), a private dispatch group is used.
 */
@property (nonatomic, strong) dispatch_group_t completionGroup;

@property (nonatomic, strong) AFImageResponseSerializer <AFURLResponseSerialization> * responseSerializer;

- (void)setCompletionBlockWithSuccess:(void (^)(AFFileRequestOperation *operation, id responseObject))success
                              failure:(void (^)(AFFileRequestOperation *operation, NSError *error))failure;

- (AFFileRequestOperation *)fileRequestOperationWithRequest:(NSURL *)url
                                                    success:(void (^)(AFFileRequestOperation *operation, id responseObject))success
                                                    failure:(void (^)(AFFileRequestOperation *operation, NSError *error))failure;

@end
