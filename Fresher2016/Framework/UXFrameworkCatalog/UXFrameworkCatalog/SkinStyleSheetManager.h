/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@class SkinStyleSheet;

@interface SkinStyleSheetManager : NSObject {
    SkinStyleSheet* skinStyleSheet;
}

+ (SkinStyleSheetManager*)manager;

- (void)loadSkin;

@end
