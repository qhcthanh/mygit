/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#define AF_DEFAULT_CUSTOM_IMAGE_CACHE 1

#define AF_IMAGE_CACHING_DEFAULT        @"default_"
#define AF_IMAGE_CACHING_AVATAR         @"avatar_"
#define AF_IMAGE_CACHING_TEMPORARY      @"temporary_"
#define AF_IMAGE_CACHING_THUMBNAIL      @"thumbnail_"
#define AF_IMAGE_CACHING_LARGEPHOTO     @"largephoto_"

// Default cache directory name
#define AF_DEFAULT_CACHE_DIRECTORY @"uxthemes"

// Schedule invalidate timer on local
#define AF_DEFAULT_INVALIDATE_TIMER @"afinvalidationtimer"

// The maximum number of pixels being stored in the cache
#define AF_DEFAULT_CACHE_SIZE_MAX 1024L*1024L*10L

// The maximum number of pixels this cache may store
#define AF_DEFAULT_CACHE_SIZE_MIN 1024L*1024L*3L

// The maximum number of pixels being stored in the cache
#define AF_CLIP_CACHE_SIZE_MAX 1024L*1024L*5L

// The maximum number of pixels this cache may store
#define AF_CLIP_CACHE_SIZE_MIN 1024L*1024L*3L

// Invalidation time for cleaning up files on local
#define AF_DEFAULT_CACHE_INVALIDATION_AGE   (60*60*24*7) // 7days

// Default expiration age of cache item
#define AF_DEFAULT_CACHE_EXPIRATION_AGE_LIMIT   (60*60*24) // 1 day

// Never expire cache for special item such as avatar
#define AF_DEFAULT_CACHE_EXPIRATION_AGE_NEVER   (INT_MAX) // Never

// The cached item will be expired, apply for redirect item
#define AF_DEFAULT_CACHE_EXPIRATION_AGE_TEMPORARY 30 // 30 seconds

@interface AFMemoryCache : NSCache

/**
 * Initializes a newly allocated cache with the given capacity.
 *
 * @returns An in-memory cache initialized with the given capacity.
 */
- (id)initWithCapacity:(NSUInteger)capacity;

- (NSUInteger)count;

/**
 * Stores an object in the cache.
 *
 * The object will be stored without an expiration date. The object will stay in the cache until
 * it's bumped out due to the cache's memory limit.
 *
 * @param object  The object being stored in the cache.
 * @param name    The name used as a key to store this object.
 */
- (void)storeObject:(id)object withName:(NSString *)name;

/**
 * Stores an object in the cache with an expiration date.
 *
 * If an object is stored with an expiration date that has already passed then the object will
 * not be stored in the cache and any existing object will be removed. The rationale behind this
 * is that the object would be removed from the cache the next time it was accessed anyway.
 *
 * @param object          The object being stored in the cache.
 * @param name            The name used as a key to store this object.
 * @param expirationDate  A date after which this object is no longer valid in the cache.
 */
- (void)storeObject:(id)object withName:(NSString *)name expiresAfter:(NSDate *)expirationDate;

/**
 * Removes an object from the cache with the given name.
 *
 * @param name The name used as a key to store this object.
 */
- (void)removeObjectWithName:(NSString *)name;


- (void)removeObjectWithURL:(NSString *)URL;

/**
 * Removes all objects from the cache with a given prefix.
 *
 * This method requires a scan of the cache entries.
 *
 * @param prefix Any object name that has this prefix will be removed from the cache.
 */
- (void)removeAllObjectsWithPrefix:(NSString *)prefix;

/**
 * Removes all objects from the cache, regardless of expiration dates.
 *
 * This will completely clear out the cache and all objects in the cache will be released.
 *
 */
- (void)removeAllObjects;

/**
 * Retrieves an object from the cache.
 *
 * If the object has expired then the object will be removed from the cache and nil will be
 * returned.
 *
 * @returns The object stored in the cache. The object is retained and autoreleased to
 *               ensure that it survives this run loop if you then remove it from the cache.
 */
- (id)objectWithName:(NSString *)name;

/**
 * Returns a Boolean value that indicates whether an object with the given name is present
 * in the cache.
 *
 * Does not update the access time of the object.
 *
 * If the object has expired then the object will be removed from the cache and NO will be
 * returned.
 *
 * @returns YES if an object with the given name is present in the cache and has not expired,
 *               otherwise NO.
 */
- (BOOL)containsObjectWithName:(NSString *)name;

/**
 * Returns the date that the object with the given name was last accessed.
 *
 * Does not update the access time of the object.
 *
 * If the object has expired then the object will be removed from the cache and nil will be
 * returned.
 *
 * @returns The last access date of the object if it exists and has not expired, nil
 *               otherwise.
 */
- (NSDate *)dateOfLastAccessWithName:(NSString *)name;

/**
 * Retrieve the name of the object that was least recently used.
 *
 * This will not update the access time of the object.
 *
 * If the cache is empty, returns nil.
 *
 */
- (NSString *)nameOfLeastRecentlyUsedObject;

/**
 * Retrieve the key with the most fresh access.
 *
 * This will not update the access time of the object.
 *
 * If the cache is empty, returns nil.
 *
 */
- (NSString *)nameOfMostRecentlyUsedObject;

/**
 * Removes all expired objects from the cache.
 *
 * Subclasses may add additional functionality to this implementation.
 * Subclasses should call super in order to prune expired objects.
 *
 * This will be called when <code>UIApplicationDidReceiveMemoryWarningNotification</code>
 * is posted.
 *
 */
- (void)reduceMemoryUsage;

// Subclassing
/**
 * An object is about to be stored in the cache.
 *
 * @param object          The object that is about to be stored in the cache.
 * @param name            The cache name for the object.
 * @param previousObject  The object previously stored in the cache. This may be the
 *                             same as object.
 * @returns YES If object is allowed to be stored in the cache.
 */
- (BOOL)shouldSetObject:(id)object withName:(NSString *)name previousObject:(id)previousObject;



/**
 * An object has been stored in the cache.
 *
 * @param object          The object that was stored in the cache.
 * @param name            The cache name for the object.
 */
- (void)didSetObject:(id)object withName:(NSString *)name;

/**
 * An object is about to be removed from the cache.
 *
 * @param object  The object about to removed from the cache.
 * @param name    The cache name for the object about to be removed.
 */
- (void)willRemoveObject:(id)object withName:(NSString *)name;

/**
 * This method is deprecated. Please use shouldSetObject:withName:previousObject: instead.
 *
 */
- (BOOL)willSetObject:(id)object withName:(NSString *)name previousObject:(id)previousObject;

@end

/**
 * An in-memory cache for storing images with caps on the total number of pixels.
 *
 * When reduceMemoryUsage is called, the least recently used images are removed from the cache
 * until the numberOfPixels is below maxNumberOfPixelsUnderStress.
 *
 * When an image is added to the cache that causes the memory usage to pass the max, the
 * least recently used images are removed from the cache until the numberOfPixels is below
 * maxNumberOfPixels.
 *
 * By default the image memory cache has no limit to its pixel count. You must explicitly
 * set this value in your application.
 *
 * @attention If the cache is too small to fit the newly added image, then all images
 *                 will end up being removed including the one being added.
 *
 */
@interface AFImageMemoryCache : AFMemoryCache

/**
 * Singleton interfaces
 */
+ (NSOperationQueue *)af_sharedImageLoaderOperationQueue;
+ (AFImageMemoryCache *)sharedAFImageMemoryCache;
+ (AFImageMemoryCache *)sharedAFClipMemoryCache;

+ (UIImage*)afSmallCircledImage;
+ (UIImage*)afLargeCircledImage;

/**
 * Adjust cache size based on memory avaiable
 */
- (void)invalidCacheSize;

/**
 *  Cleanup unnecessary cache to avoid memory warning
 */
- (void)reduceCacheVolume;

/**
 * Get an image on memory cache by URL
 */
- (UIImage*)imageForURL:(NSString*)URL;

/**
 Get an image on memory cache by URL and prefix
 */
- (UIImage*)imageForURL:(NSString*)URL withPrefix:(NSString*)prefix;

/**
 Load an image on both memory and disk
 */
- (UIImage*)imageForURL:(NSString*)URL onDisk:(BOOL)disk;

/**
 * Load an image on both memory and disk with a prefix
 */
- (UIImage*)imageForURL:(NSString*)URL onDisk:(BOOL)disk withPrefix:(NSString*)prefix;

/**
 * Get cached path from an URL
 */

- (NSString*)cachedFilePathForURL:(NSString*)URL;

/**
 * Get data raw from an URL
 */
- (NSData*)dataForURL:(NSString*)URL;

/**
 * Put an image into memory cache
 */
- (void)storeImage:(UIImage*)image forURL:(NSString*)URL;

/**
 * Put an image into memory cache with a prefix
 */
- (void)storeImage:(UIImage*)image forURL:(NSString*)URL withPrefix:(NSString*)prefix expiresAfter:(NSDate*)expiresDate;

/**
 * Save image data into a file on local
 */
- (void)storeData:(NSData*)data forURL:(NSString*)URL;

/**
 * Update expiration age ragarding redirected item
 */
- (void)updateExpirationAgeForURL:(NSString*)URL;

/**
 * Returns the total number of pixels being stored in the cache.
 *
 * @returns The total number of pixels being stored in the cache.
 */
@property (nonatomic, readonly) unsigned long long numberOfPixels;

/**
 * The maximum number of pixels this cache may ever store.
 *
 * Defaults to 0, which is special cased to represent an unlimited number of pixels.
 *
 * @returns The maximum number of pixels this cache may ever store.
 */
@property (nonatomic)           unsigned long long maxNumberOfPixels;             // Default: 0 (unlimited)

/**
 * The maximum number of pixels this cache may store after a call to reduceMemoryUsage.
 *
 * Defaults to 0, which is special cased to represent an unlimited number of pixels.
 *
 * @returns The maximum number of pixels this cache may store after a call
 *               to reduceMemoryUsage.
 */
@property (nonatomic)           unsigned long long maxNumberOfPixelsUnderStress;  // Default: 0 (unlimited)

@property (nonatomic)           unsigned long long maxNumberOfPixelCacheAllowed;  // Default: 0 (unlimited)

@property (nonatomic, strong) NSString*  cachePath;

/**
 *  Help to download data with an URL
 *
 *  @param URLString         The URL should be downloaded
 *  @param shouldCache       Automatically cache the image on memory
 *  @param progressHandler   Calculate progresss based on percent of downloaded bytes per total bytes
 *  @param completionHandler The callback function that return an UIImage object
 */
- (void)downloadDataWithURL:(NSString*)URLString withCompletionHandler:(void(^)(NSData* data))completionHandler withProgressHandler:(void (^)(CGFloat progress))progressHandler;

/**
 *  Help to download an image with an URL and put result to global cache if need
 *
 *  @param URLString         The URL should be downloaded
 *  @param shouldCache       Automatically cache the image on memory
 *  @param progressHandler   Calculate progresss based on percent of downloaded bytes per total bytes
 *  @param completionHandler The callback function that return an UIImage object
 */
- (void)downloadImageWithURL:(NSString*)URLString shouldCacheOnMemory:(BOOL)shouldCache withCompletionHandler:(void(^)(UIImage* downloadedImage))completionHandler withProgressHandler:(void (^)(CGFloat progress))progressHandler;

@end
