/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXStyle.h"

@interface UXReflectiveFillStyle : UXStyle {
}

@property (nonatomic, retain) UIColor* color;
@property (nonatomic, assign) BOOL     withBottomHighlight;

+ (UXReflectiveFillStyle*)styleWithColor:(UIColor*)color next:(UXStyle*)next;
+ (UXReflectiveFillStyle*)styleWithColor:(UIColor*)color
                     withBottomHighlight:(BOOL)withBottomHighlight next:(UXStyle*)next;

@end
