//
//  FriendsFacebookViewController.h
//  FacebookContact
//
//  Created by qhcthanh on 7/14/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsFacebookViewController : UIViewController


/**
 *  Current facebook friend inofo array
 */
@property (weak, atomic) NSMutableArray* friendArray;

/**
 *  Next cursor URL of page friend data in Facebook Graph API
 */
@property (strong, atomic) NSURL *nextFriendsURLRequest;

@end
