/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, UXActivityAnimationType) {
    UXActivityAnimationTypeDefault,
    UXActivityAnimationTypeMusic,
    UXActivityAnimationTypeSkype,
};

@interface UXActivityView : UIView

@property (nonatomic, assign) UXActivityAnimationType type;
@property (nonatomic, strong) UIColor *backgroundTintColor;
@property (nonatomic, strong) UIColor *loadingTintColor;
@property (nonatomic, assign) CGFloat size;

@property (nonatomic, readonly) BOOL animating;

/**
 *  Initialize an activity view
 *
 *  @param type  The activity type of view
 *  @param title The title of loading activity
 *
 *  @return An activity object
 */
- (id)initWithType:(UXActivityAnimationType)type title:(NSString*)title;

/**
 *  Initialize an activity view
 *
 *  @param type      The activity type of view
 *  @param tintColor The tint color of view
 *  @param title     title The title of loading activity
 *
 *  @return An activity object
 */
- (id)initWithType:(UXActivityAnimationType)type
  loadingTintColor:(UIColor *)tintColor title:(NSString*)title;

/**
 *  Initialize an activity view
 *
 *  @param type      The activity type of view
 *  @param tintColor The tint color of view
 *  @param size      The size of view
 *  @param title     title The title of loading activity
 *
 *  @return An activity object
 */
- (id)initWithType:(UXActivityAnimationType)type
  loadingTintColor:(UIColor *)tintColor size:(CGFloat)size title:(NSString*)title;

/**
 *  Start animation
 */
- (void)startAnimating;

/**
 *  Stop animation
 */
- (void)stopAnimating;

@end
