/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXNetworkModel.h"

@class UXMutableArray;
@interface UXServiceModel : UXNetworkModel

@property(nonatomic, assign) BOOL searchFinish;

@end
