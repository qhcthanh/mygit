/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "TargetConditionals.h"

#if TARGET_OS_IPHONE
    #import <Foundation/Foundation.h>

#else
    #import <Foundation/Foundation.h>
    #import <Cocoa/Cocoa.h>

    #ifndef __MAC_10_10
        #error "UXStoreKit is only supported on OS X 10.10 or later."
    #endif
#endif

/**
 * This notification is posted when UXStoreKit completes initialization sequence successfully
 */
extern NSString *const kUXStoreKitProductsAvailableNotification;

/**
 * This notification is posted when UXStoreKit completes initialization sequence unsuccessfully
 */
extern NSString *const kUXStoreKitProductsUnavailableNotification;

/**
 *  This notification is posted when UXStoreKit completes purchase of a product
 */
extern NSString *const kUXStoreKitProductPurchasedNotification;

/**
 *  This notification is posted when UXStoreKit fails to complete the purchase of a product
 */
extern NSString *const kUXStoreKitProductPurchaseFailedNotification;

/**
 *  This notification is posted when UXStoreKit has a purchase deferred for approval
 *
 *  @discussion
 *  This occurs when a device has parental controls for in-App Purchases enabled.
 *   iOS will present a prompt for parental approval, either on the current device or
 *   on the parent's device. Update your UI to reflect the deferred status, and wait
 *   to be notified of the completed or failed purchase.
 *  @availability iOS 8.0 or later
 */
extern NSString *const kUXStoreKitProductPurchaseDeferredNotification NS_AVAILABLE(10_10, 8_0);

/**
 *  This notification is posted when UXStoreKit completes restoring purchases
 */
extern NSString *const kUXStoreKitRestoredPurchasesNotification;

/**
 *  This notification is posted when UXStoreKit fails to restore purchases
 */
extern NSString *const kUXStoreKitRestoringPurchasesFailedNotification;

/**
 *  This notification is posted when UXStoreKit fails to validate receipts
 */
extern NSString *const kUXStoreKitReceiptValidationFailedNotification;

/**
 *  This notification is posted when UXStoreKit detects expiration of a auto-renewing subscription
 */
extern NSString *const kUXStoreKitSubscriptionExpiredNotification;

/**
 *  This notification is posted when UXStoreKit downloads a hosted content
 */
extern NSString *const kUXStoreKitDownloadProgressNotification;

/**
 *  This notification is posted when UXStoreKit completes downloading a hosted content
 */
extern NSString *const kUXStoreKitDownloadCompletedNotification;

/**
 *  This notification is posted when UXStoreKit has been closed in-app purchase services
 */
extern NSString *const kUXStoreKitInAppPurchaseUnavailableNotification;

/**
 *  The singleton class that takes care of In App Purchasing
 *  @discussion
 *  UXStoreKit provides three basic functionalities, namely, managing In App Purchases,
 *  remembering purchases for you and also provides a basic virtual currency manager
 */
@interface UXStoreKit : NSObject

/**
 *  Property that stores the list of available In App purchaseable products
 *  @discussion
 *	This property is initialized after the call to startProductRequest completes
 *  If the startProductRequest fails, this property will be nil
 */
@property (nonatomic, strong) NSArray *availableProducts;

/**
 *  Accessor for the shared singleton object
 *  @discussion
 *      Use this to access the only object of UXStoreKit
 *  @return A Store kis instance
 */
+ (UXStoreKit *)sharedKit;

/**
 *  Initializes UXStoreKit singleton by making the product request using StoreKit's SKProductRequest
 *  @discussion
 *	This method should be called in your application:didFinishLaunchingWithOptions: method
 *  If this method fails, UXStoreKit will not work
 *  Most common reason for this method to fail is Internet connection being offline
 *  It's your responsibility to call startProductRequest if the Internet connection comes online
 *  and the previous call to startProductRequest failed (availableProducts.count == 0).
 */
- (void)startProductRequest;

/**
 *  Initializes UXStoreKit singleton by making the product request using StoreKit's SKProductRequest
 *
 *  @discussion
 *	This method is normally called after fetching a list of products from your server.
 *  If all your products are known before hand,
 *  fill them in UXStoreKitConfigs.plist and use -startProductRequest
 *  @param items The item list of products on server
 */
- (void)startProductRequestWithProductIdentifiers:(NSArray*) items;

/**
 *  Restores In App Purchases made on other devices
 *  @discussion
 *	This method restores your user's In App Purchases made on other devices.
 */
- (void)restorePurchases;

/**
 *  Refreshes the App Store receipt and prompts the user to authenticate.
 *  @discussion
 *	This method can generate a reciept while debugging your application. In a production
 *  environment this should only be used in an appropriate context because it will present
 *  an App Store login alert to the user (without explanation).
 */
- (void)refreshAppStoreReceipt;

/**
 *  Initiates payment request for a In App Purchasable Product
 *
 *  @discussion
 *	This method starts a payment request to App Store.
 *  Purchased products are notified through NSNotifications
 *  Observe kUXStoreKitProductPurchasedNotification to get notified when the purchase completes
 *  @param productId The productId that will be buy
 */
- (void)initiatePaymentRequestForProductWithIdentifier:(NSString *)productId;

/**
 *  Checks whether the app version the user purchased is older than the required version
 *
 *  @discussion
 *	This method checks against the local store maintained by UXStoreKit if the product was previously purchased
 *  This method can be used for Consumables/Non-Consumables/Auto-renewing subscriptions
 *  Observe kUXStoreKitProductPurchasedNotification to get notified when the purchase completes
 *  @param requiredVersion The required version should be purchased
 *
 *  @return YES if the user have already purchased product version
 */
- (BOOL)purchasedAppBeforeVersion:(NSString *)requiredVersion;

/**
 *  Checks whether the product identified by the given productId is purchased previously
 *
 *	This method checks against the local store maintained by UXStoreKit if the product was previously purchased
 *  This method can be used for Consumables/Non-Consumables/Auto-renewing subscriptions
 *  Observe kUXStoreKitProductPurchasedNotification to get notified when the purchase completes
 *  @param productId The required version should be purchased
 *
 *  @return YES if the product is purchased
 */
- (BOOL)isProductPurchased:(NSString *)productId;

/**
 *  Checks the expiry date for the product identified by the given productId
 *
 *  @discussion
 *	This method checks against the local store maintained by UXStoreKit for expiry date of a given product
 *  This method can be used for Consumables/Non-Consumables/Auto-renewing subscriptions
 *  Expiry date for Consumables/Non-Consumables is always [NSNull null]
 *  Expiry date for Auto-renewing subscriptions is fetched from receipt validation server and remembered by UXStoreKit
 *  Expiry date for Auto-renewing subscriptions will be [NSNull null] for a subscription that was just purchased
 *  UXStoreKit automatically takes care of updating expiry date when a auto-renewing subscription renews
 *  Observe kUXStoreKitProductPurchasedNotification to get notified when the purchase completes
 *  Observe kUXStoreKitSubscriptionExpiredNotification to get notified when a auto-renewing subscription expires and the
 *  user has stopped the subscription
 *  @param productId The product should be checked
 *
 *  @return expiry date of product
 */
- (NSDate *)expiryDateForProduct:(NSString *)productId;

/**
 *  This method returns the available credits (managed by UXStoreKit) for a given consumable
 *
 *  @discussion
 *	UXStoreKit provides a basic virtual currency manager for your consumables
 *  This method returns the available credits for a consumable
 *  A consumable ID is different from its product id, and it is configured in UXStoreKitConfigs.plist file
 *  Observe kUXStoreKitProductPurchasedNotification to get notified when the purchase of the consumable completes
 *  @param consumableID The cusumableId that is configured in UXStoreKitConfigs.plist file
 *
 *  @return Number of credits for given consumable
 */
- (NSNumber *)availableCreditsForConsumable:(NSString *)consumableID;

/**
 *  This method updates the available credits (managed by UXStoreKit) for a given consumable
 *
 *  @discussion
 *	UXStoreKit provides a basic virtual currency manager for your consumables
 *  This method should be called if the user consumes a consumable credit
 *  Observe kUXStoreKitProductPurchasedNotification to get notified when the purchase of the consumable completes
 *
 *  @param creditCountToConsume The credit count to consume
 *  @param consumableId         A consumable ID is different from its product id, and it is configured in UXStoreKitConfigs.plist file
 *
 *  @return Number of creadit to consume
 */
- (NSNumber *)consumeCredits:(NSNumber *)creditCountToConsume identifiedByConsumableIdentifier:(NSString *)consumableId;

/**
 *  This method sets the default credits (managed by UXStoreKit) for a given consumable
 *
 *  @discussion
 *	UXStoreKit provides a basic virtual currency manager for your consumables
 *  This method should be called if you provide free credits to start with
 *  Observe kUXStoreKitProductPurchasedNotification to get notified when the purchase of the consumable completes
 *  @param creditCount  The number of credits
 *  @param consumableId A consumable ID is different from its product id, and it is configured in UXStoreKitConfigs.plist file
 */
- (void)setDefaultCredits:(NSNumber *)creditCount forConsumableIdentifier:(NSString *)consumableId;

@end
