//
//  Catboard.swift
//  TransliteratingKeyboard
//
//  Created by Alexei Baboulevitch on 9/24/14.
//  Copyright (c) 2014 Alexei Baboulevitch ("Archagon"). All rights reserved.
//

import UIKit

/*
 This is the demo keyboard. If you're implementing your own keyboard, simply follow the example here and then
 set the name of your KeyboardViewController subclass in the Info.plist file.
 */

let kCatTypeEnabled = "kCatTypeEnabled"

class Catboard: KeyboardViewController {
    
    let takeDebugScreenshot: Bool = false
    var suggestionManager : SuggestionManager
    var banner : CatboardBanner? = nil
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        NSUserDefaults.standardUserDefaults().registerDefaults([kCatTypeEnabled: true])
        //
//        print("Anh".isCapitalizedWord())
//        print("aNH".isCapitalizedWord())
        print("anh yeu Em".lastPhrase())
//        print("Ăn".isCapitalizedWord())
        
        
        
        
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        //if (!fileManager.fileExistsAtPath(multiwordDataPath)) {
            if let bundlePath = NSBundle.mainBundle().pathForResource("MultiWordDB", ofType: "db") {
                do {
                    try fileManager.copyItemAtPath(bundlePath, toPath: multiwordDataPath)
                    print(multiwordDataPath)
                } catch {
                }
            }
        //}
       // if (!fileManager.fileExistsAtPath(singleWordDatabasePath)) {
            if let bundlePath = NSBundle.mainBundle().pathForResource("SingleWordDB", ofType: "db") {
                do {
                    try fileManager.copyItemAtPath(bundlePath, toPath: singleWordDatabasePath)
                    print(singleWordDatabasePath)
                } catch {
                }
            }
        //}
        
//        var letterJsonString = ""
//        var relativeLetterJsonString = ""
//        var phraseJsonString = ""
//        let path1 = NSBundle.mainBundle().pathForResource("letterJson", ofType: "txt")
//        let path2 = NSBundle.mainBundle().pathForResource("relativeLetterJson", ofType: "txt")
//        let path3 = NSBundle.mainBundle().pathForResource("phraseJson2", ofType: "txt")
//        
//        do {
//            letterJsonString = try String(contentsOfFile:path1!, encoding: NSUTF8StringEncoding)
//            relativeLetterJsonString = try String(contentsOfFile:path2!, encoding: NSUTF8StringEncoding)
//            phraseJsonString = try String(contentsOfFile:path3!, encoding: NSUTF8StringEncoding)
//        } catch _ as NSError {
//            
//        }
//        
//        let defaults = NSUserDefaults.init(suiteName: Constants.userDefaultKey)
//        defaults!.setObject(letterJsonString, forKey: Constants.letterJsonStringKey)
//        defaults!.setObject(relativeLetterJsonString, forKey: Constants.relativeLetterJsonStringKey)
//        defaults!.setObject(phraseJsonString, forKey: Constants.phraseJsonStringKey)
        
        self.suggestionManager = SuggestionManager.shareIntance
        
        //        suggestionManager.setup(Constants.userDefaultKey, dispatchQueue: dispatch_get_main_queue()) { (rootNodeDictionary, error) in
        //            print("setup completed")
        //            //let letterRootNode = rootNodeDictionary[Constants.letterKey]
        //            //suggestionManager.optimizeLetterTree(2700)
        //            print(self.suggestionManager.rootNodeOfLetterTree.getAllEndNode().count)
        //        }
        
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)

        banner = self.bannerView as? CatboardBanner
        
        banner?.catLabel.userInteractionEnabled = true
        banner?.catLabel1.userInteractionEnabled = true
        banner?.catLabel2.userInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(Catboard.labelTapFunction(_:)))
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(Catboard.labelTapFunction(_:)))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(Catboard.labelTapFunction(_:)))
        
        banner?.catLabel.addGestureRecognizer(tap)
        banner?.catLabel1.addGestureRecognizer(tap1)
        banner?.catLabel2.addGestureRecognizer(tap2)
    }
    
    @objc private func labelTapFunction(sender:UITapGestureRecognizer) {
        if var content = self.textDocumentProxy.documentContextBeforeInput {
            
            if (sender.view as! UILabel).text != "" {
                let text = (sender.view as! UILabel).text
                
                //suggestionManager.updateTreeWithWord(text!)
                
                if content.characters.last == " " {
                    self.textDocumentProxy.insertText(text! + " ")
                } else {
                    for _ in 0..<content.lastWord()!.characters.count {
                        content = content.substringToIndex(content.endIndex.predecessor())
                        self.textDocumentProxy.deleteBackward()
                    }
                    self.textDocumentProxy.insertText(text! + " ")
                }

                self.updateTree(documentContext: self.textDocumentProxy.documentContextBeforeInput!)
                
                suggestionManager.suggestWithInput(self.textDocumentProxy.documentContextBeforeInput!, numberOfSuggestion: 3, dispatchQueue: dispatch_get_main_queue()) { (result, error) in
                    self.updateSuggestionText(result)
                }
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func keyPressed(key: Key) {
        
        NSDate.startCount()
        
        let textDocumentProxy = self.textDocumentProxy
        
        let keyOutput = key.outputForCase(self.shiftState.uppercase())
        
        if let content = textDocumentProxy.documentContextBeforeInput {
            
            //print("before:" + content)
            suggestionManager.suggestWithInput(content + keyOutput, numberOfSuggestion: 3, dispatchQueue: dispatch_get_main_queue()) { (result, error) in
                self.updateSuggestionText(result)
            }
            if keyOutput == " " {
                self.updateTree(documentContext: content)
            }
            
        } else {
            
            suggestionManager.suggestWithInput(keyOutput, numberOfSuggestion: 3, dispatchQueue: dispatch_get_main_queue()) { (result, error) in
                self.updateSuggestionText(result)
            }
        }
        
        textDocumentProxy.insertText(keyOutput)
        
        print("Sugesstion time: " + String(NSDate.endCount()))
    }
    
    func updateTree(documentContext content : String) {
        
        if let lastWord = content.lastWord() {
            if content.characterAtIndex(content.length - 2).isUnicodeCharacter() {
                suggestionManager.updateTreeWithWord(lastWord)
            }
        }
        if let lastPhrase = content.lastPhrase() {
            suggestionManager.updateTreeWithPhrase(lastPhrase)
        }
    }
    
    func updateSuggestionText(var textArray : [String]) {
        
        banner!.catLabel.text = " "
        banner!.catLabel1.text = " "
        banner!.catLabel2.text = " "
        
        if textArray.count < 3 {
            for _ in 0..<3 - textArray.count {
                textArray.append(" ")
            }
        }
        
        banner!.catLabel.text = textArray[0]
        banner!.catLabel1.text = textArray[1]
        banner!.catLabel2.text = textArray[2]
        
        banner!.catLabel.sizeToFit()
        banner!.catLabel1.sizeToFit()
        banner!.catLabel2.sizeToFit()
    }
    
    override func backspaceDown(sender: KeyboardKey) {
        super.backspaceDown(sender)
    }
    
    override func backspaceUp(sender: KeyboardKey) {
        super.backspaceUp(sender)
        
        if let content = self.textDocumentProxy.documentContextBeforeInput {
            suggestionManager.suggestWithInput(content, numberOfSuggestion: 3, dispatchQueue: dispatch_get_main_queue()) { (result, error) in
                self.updateSuggestionText(result)
            }
        } else {
            suggestionManager.suggestWithInput("", numberOfSuggestion: 3, dispatchQueue: dispatch_get_main_queue()) { (result, error) in
                self.updateSuggestionText(result)
            }
        }
    }
    
    override func backspaceRepeatCallback() {
        super.backspaceRepeatCallback()
    }
    
    override func setupKeys() {
        super.setupKeys()
        
        if takeDebugScreenshot {
            if self.layout == nil {
                return
            }
            
            for page in keyboard.pages {
                for rowKeys in page.rows {
                    for key in rowKeys {
                        if let keyView = self.layout!.viewForKey(key) {
                            keyView.addTarget(self, action: #selector(Catboard.takeScreenshotDelay), forControlEvents: .TouchDown)
                        }
                    }
                }
            }
        }
    }
    
    override func createBanner() -> ExtraView? {
        return CatboardBanner(globalColors: self.dynamicType.globalColors, darkMode: false, solidColorMode: self.solidColorMode())
    }
    
    func takeScreenshotDelay() {
        NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(Catboard.takeScreenshot), userInfo: nil, repeats: false)
    }
    
    func takeScreenshot() {
        if !CGRectIsEmpty(self.view.bounds) {
            UIDevice.currentDevice().beginGeneratingDeviceOrientationNotifications()
            
            let oldViewColor = self.view.backgroundColor
            self.view.backgroundColor = UIColor(hue: (216/360.0), saturation: 0.05, brightness: 0.86, alpha: 1)
            
            let rect = self.view.bounds
            UIGraphicsBeginImageContextWithOptions(rect.size, true, 0)
            var context = UIGraphicsGetCurrentContext()
            self.view.drawViewHierarchyInRect(self.view.bounds, afterScreenUpdates: true)
            let capturedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            let name = (self.interfaceOrientation.isPortrait ? "Screenshot-Portrait" : "Screenshot-Landscape")
            let imagePath = "/Users/archagon/Documents/Programming/OSX/RussianPhoneticKeyboard/External/tasty-imitation-keyboard/\(name).png"
            
            if let pngRep = UIImagePNGRepresentation(capturedImage) {
                pngRep.writeToFile(imagePath, atomically: true)
            }
            
            self.view.backgroundColor = oldViewColor
        }
    }
    override func viewDidAppear(animated: Bool) {
        super.viewWillAppear(false)
        print("view did appear")
    }
    
    override func viewWillDisappear(animated: Bool) {
        
//        suggestionManager.rootNodeOfLetterTree.clear()
//        suggestionManager.rootNodeOfPhraseTree.clear()
//        suggestionManager.rootNodeOfRelativeLetterTree.clear()
        
        suggestionManager.updateDatabase(withUserDefaultKey: Constants.userDefaultKey, dispatchQueue: dispatch_get_main_queue()) { (defaults, error) in
            print("db update")
        }
        //suggestionManager.clearMem()
//        if let content = self.textDocumentProxy.documentContextBeforeInput {
//            print("view will disappear", content)
//        }
        
    }
}

func randomCat() -> String {
    let cats = "🐱😺😸😹😽😻😿😾😼🙀"
    
    let numCats = cats.characters.count
    let randomCat = arc4random() % UInt32(numCats)
    
    let index = cats.startIndex.advancedBy(Int(randomCat))
    let character = cats[index]
    
    return String(character)
}
