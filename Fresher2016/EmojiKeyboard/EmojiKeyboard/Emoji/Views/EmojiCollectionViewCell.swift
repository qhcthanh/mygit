//
//  EmojiCollectionViewCell.swift
//  EmojiKeyboard
//
//  Created by Quach Ha Chan Thanh on 8/14/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import UIKit

let imageViewDetailTag = 1001

var fontFitSizeDictionary = Dictionary<CGFloat, UIFont>()

public class EmojiCollectionViewCell: UICollectionViewCell {
    
    static private var detailEmojiView: (UIView) -> (UIImageView) =  {
         view in
        let size = view.bounds.width * 0.3
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Up")
        imageView.tag = imageViewDetailTag
        
        
        view.addSubview(imageView)
        
        imageView.constrain(to: CGSizeMake(size, size))
        imageView.constrain(.Right, to: .Right, of: view)
        imageView.constrain(.Bottom, to: .Bottom, of: view, offsetBy: -5)
        
        return imageView
    }
    
    @IBOutlet weak var emojiLabel: UILabel!
    
    weak public var itemProtocol: SimpleModelProtocol?
    weak var delegate: EmojiCategoryCollectionViewCellDelegate?
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        

    }
    
    public func bindingUI(itemProtocol: SimpleModelProtocol) {

        self.itemProtocol = itemProtocol
        
        emojiLabel.text = itemProtocol.titleContent
       
        if let font = fontFitSizeDictionary[self.bounds.width] {
            emojiLabel.font = font
        } else {
            let font = emojiLabel.fitFontForSize(self.bounds.size)
            emojiLabel.font = font
            fontFitSizeDictionary[self.bounds.size.width] = font
        }
        
        
        if let _ = itemProtocol.details {
            EmojiCollectionViewCell.detailEmojiView(self)
        }

    }
    
    override public func prepareForReuse() {
        super.prepareForReuse()
        
        // Clean UI
        emojiLabel.text = ""
        self.viewWithTag(imageViewDetailTag)?.removeFromSuperview()
        
        itemProtocol = nil
        
    }
    
    public func generatePopupView() -> UIView? {
        if let emojiSkins = itemProtocol?.details {
            let popupView = UIView()
            popupView.backgroundColor = customLightGrayColor
            var index: CGFloat = 0
            
            for emoji in emojiSkins {
                let emojiCustomLabel = UILabel()
                emojiCustomLabel.text = emoji
                
                // Set similar font of emojiLabel and size
                emojiCustomLabel.font = self.emojiLabel.font
                emojiCustomLabel.frame.size = self.bounds.size
                emojiCustomLabel.textAlignment = .Center
                
                // Set
                emojiCustomLabel.frame.origin.x = 5 + (index * emojiCustomLabel.frame.size.width)
                emojiCustomLabel.frame.origin.y = 5
                
                // 
                emojiCustomLabel.userInteractionEnabled = true
                let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(onTapEmojiGesture))
                longPressGesture.minimumPressDuration = 0.06
                emojiCustomLabel.addGestureRecognizer(longPressGesture)
                
                // Update index
                index += 1
                
                popupView.addSubview(emojiCustomLabel)
            }
            
            return popupView
        }
        
        return nil
    }
    
    func onTapEmojiGesture(gesture: UILongPressGestureRecognizer) {
        if let view = gesture.view,
            let emojiCustomLabel = view as? UILabel {
            
            if gesture.state == .Began {
                emojiCustomLabel.backgroundColor = customDarkGrayColor
            } else if gesture.state != . Changed {
                if let emoji = emojiCustomLabel.text, let delegate = self.delegate {
                    delegate.didSelectEmoji(emoji)

                    emojiCustomLabel.backgroundColor = .clearColor()
                }
            }
        }
    }
    
}






