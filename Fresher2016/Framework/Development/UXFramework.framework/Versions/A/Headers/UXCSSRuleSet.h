/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

/**
 * Currently, for size units we only support pixels (resolution independent)
 * and percentage (of superview)
 */
typedef enum {
    CSS_PIXEL_UNIT,
    CSS_PERCENTAGE_UNIT,
    CSS_AUTO_UNIT
} UXCSSUnitType;

/**
 * Width, height, top, left, right, bottom can be expressed in various units.
 */
typedef struct {
    UXCSSUnitType type;
    CGFloat value;
} UXCSSUnit;

typedef enum {
    UXCSSButtonAdjustNone = 0,
    UXCSSButtonAdjustHighlighted = 1,
    UXCSSButtonAdjustDisabled = 2
} UXCSSButtonAdjust;

/**
 * A simple translator from raw CSS rulesets to Objective-C values.
 *
 * @ingroup UXCSS
 *
 * Objective-C values are created on-demand and cached. These ruleset objects are cached
 * by NIStylesheet for a given CSS scope. When a memory warning is received, all ruleset objects
 * are removed from every stylesheet.
 */
@interface UXCSSRuleset : NSObject {
@private
    NSMutableDictionary* _ruleset;
    
    UIColor* _textColor;
    UIColor* _highlightedTextColor;
    NSTextAlignment _textAlignment;
    UIFont* _font;
    UIColor* _textShadowColor;
    CGSize _textShadowOffset;
    NSLineBreakMode _lineBreakMode;
    NSInteger _numberOfLines;
    CGFloat _minimumFontSize;
    BOOL _adjustsFontSize;
    UIBaselineAdjustment _baselineAdjustment;
    CGFloat _opacity;
    UIColor* _backgroundColor;
    NSString* _backgroundImage;
    UIEdgeInsets _backgroundStretchInsets;
    NSString* _image;
    CGFloat _borderRadius;
    UIColor *_borderColor;
    CGFloat _borderWidth;
    UIColor *_tintColor;
    UIActivityIndicatorViewStyle _activityIndicatorStyle;
    UIViewAutoresizing _autoresizing;
    UITableViewCellSeparatorStyle _tableViewCellSeparatorStyle;
    UIScrollViewIndicatorStyle _scrollViewIndicatorStyle;
    NSTextAlignment _frameHorizontalAlign;
    UIViewContentMode _frameVerticalAlign;
    UIControlContentVerticalAlignment _verticalAlign;
    UIControlContentHorizontalAlignment _horizontalAlign;
    BOOL _visible;
    UXCSSButtonAdjust _buttonAdjust;
    UIEdgeInsets _titleInsets;
    UIEdgeInsets _contentInsets;
    UIEdgeInsets _imageInsets;
    NSString *_textKey;
    NSString* _relativeToId;
    UXCSSUnit _marginTop;
    UXCSSUnit _marginLeft;
    UXCSSUnit _marginRight;
    UXCSSUnit _marginBottom;
//    UXCSSUnit _verticalPadding;
//    UXCSSUnit _horizontalPadding;
    
    UXCSSUnit _width;
    UXCSSUnit _height;
    UXCSSUnit _top;
    UXCSSUnit _bottom;
    UXCSSUnit _left;
    UXCSSUnit _right;
    UXCSSUnit _minHeight;
    UXCSSUnit _minWidth;
    UXCSSUnit _maxHeight;
    UXCSSUnit _maxWidth;
    
    union {
        struct {
            int TextColor : 1;
            int HighlightedTextColor: 1;
            int TextAlignment : 1;
            int Font : 1;
            int TextShadowColor : 1;
            int TextShadowOffset : 1;
            int LineBreakMode : 1;
            int NumberOfLines : 1;
            int MinimumFontSize : 1;
            int AdjustsFontSize : 1;
            int BaselineAdjustment : 1;
            int Opacity : 1;
            int BackgroundColor : 1;
            int BackgroundImage: 1;
            int BackgroundStretchInsets: 1;
            //16
            int Image: 1;
            int BorderRadius : 1;
            int BorderColor : 1;
            int BorderWidth : 1;
            int TintColor : 1;
            int ActivityIndicatorStyle : 1;
            int Autoresizing : 1;
            int TableViewCellSeparatorStyle : 1;
            int ScrollViewIndicatorStyle : 1;
            int VerticalAlign: 1;
            int HorizontalAlign: 1;
            int Width : 1;
            int Height : 1;
            int Top : 1;
            int Bottom : 1;
            int Left : 1;
            // 32
            int Right : 1;
            int FrameHorizontalAlign: 1;
            int FrameVerticalAlign: 1;
            int Visible: 1;
            int TitleInsets: 1;
            int ContentInsets: 1;
            int ImageInsets: 1;
            int RelativeToId: 1;
            int MarginTop: 1;
            int MarginLeft: 1;
            int MarginRight: 1;
            int MarginBottom: 1;
            int MinWidth: 1;
            int MinHeight: 1;
            int MaxWidth: 1;
            int MaxHeight: 1;
            // 48
            int TextKey: 1;
            int ButtonAdjust: 1;
            int HorizontalPadding: 1;
            int VerticalPadding: 1;
        } cached;
        int64_t _data;
    } _is;
}

- (void)addEntriesFromDictionary:(NSDictionary *)dictionary;
- (id)cssRuleForKey: (NSString*)key;

- (BOOL)hasTextColor;
- (UIColor *)textColor; // color

- (BOOL)hasHighlightedTextColor;
- (UIColor *)highlightedTextColor;

- (BOOL)hasTextAlignment;
- (NSTextAlignment)textAlignment; // text-align

- (BOOL)hasFont;
- (UIFont *)font; // font, font-family, font-size, font-style, font-weight

- (BOOL)hasTextShadowColor;
- (UIColor *)textShadowColor; // text-shadow

- (BOOL)hasTextShadowOffset;
- (CGSize)textShadowOffset; // text-shadow

- (BOOL)hasLineBreakMode;
- (NSLineBreakMode)lineBreakMode; // -ios-line-break-mode

- (BOOL)hasNumberOfLines;
- (NSInteger)numberOfLines; // -ios-number-of-lines

- (BOOL)hasMinimumFontSize;
- (CGFloat)minimumFontSize; // -ios-minimum-font-size

- (BOOL)hasAdjustsFontSize;
- (BOOL)adjustsFontSize; // -ios-adjusts-font-size

- (BOOL)hasBaselineAdjustment;
- (UIBaselineAdjustment)baselineAdjustment; // -ios-baseline-adjustment

- (BOOL)hasOpacity;
- (CGFloat)opacity; // opacity

- (BOOL)hasBackgroundColor;
- (UIColor *)backgroundColor; // background-color

- (BOOL)hasBackgroundImage;
- (NSString*)backgroundImage; // background-image

- (BOOL)hasBackgroundStretchInsets;
- (UIEdgeInsets)backgroundStretchInsets; // -mobile-background-stretch

- (BOOL)hasImage;
- (NSString*)image; // -mobile-image

- (BOOL)hasBorderRadius;
- (CGFloat)borderRadius; // border-radius

- (BOOL)hasBorderColor;
- (UIColor *)borderColor; // border, border-color

- (BOOL)hasBorderWidth;
- (CGFloat)borderWidth; // border, border-width

- (BOOL)hasWidth;
- (UXCSSUnit)width; // width

- (BOOL)hasHeight;
- (UXCSSUnit)height; // height

- (BOOL)hasTop;
- (UXCSSUnit)top; // top

- (BOOL)hasBottom;
- (UXCSSUnit)bottom; // bottom

- (BOOL)hasLeft;
- (UXCSSUnit)left; // left

- (BOOL)hasRight;
- (UXCSSUnit)right; // right

- (BOOL)hasMinWidth;
- (UXCSSUnit)minWidth; // min-width

- (BOOL)hasMinHeight;
- (UXCSSUnit)minHeight; // min-height

- (BOOL)hasMaxWidth;
- (UXCSSUnit)maxWidth; // max-width

- (BOOL)hasMaxHeight;
- (UXCSSUnit)maxHeight; // max-height

- (BOOL)hasVerticalAlign;
- (UIControlContentVerticalAlignment)verticalAlign; // -mobile-content-valign

- (BOOL)hasHorizontalAlign;
- (UIControlContentHorizontalAlignment)horizontalAlign; // -mobile-content-halign

- (BOOL)hasFrameHorizontalAlign;
- (NSTextAlignment)frameHorizontalAlign; // -mobile-halign

- (BOOL)hasFrameVerticalAlign;
- (UIViewContentMode)frameVerticalAlign; // -mobile-valign

- (BOOL)hasTintColor;
- (UIColor *)tintColor; // -ios-tint-color

- (BOOL)hasActivityIndicatorStyle;
- (UIActivityIndicatorViewStyle)activityIndicatorStyle; // -ios-activity-indicator-style

- (BOOL)hasAutoresizing;
- (UIViewAutoresizing)autoresizing; // -ios-autoresizing

- (BOOL)hasTableViewCellSeparatorStyle;
- (UITableViewCellSeparatorStyle)tableViewCellSeparatorStyle; // -ios-table-view-cell-separator-style

- (BOOL)hasScrollViewIndicatorStyle;
- (UIScrollViewIndicatorStyle)scrollViewIndicatorStyle; // -ios-scroll-view-indicator-style

- (BOOL)hasVisible;
- (BOOL)visible; // visibility

- (BOOL)hasButtonAdjust;
- (UXCSSButtonAdjust)buttonAdjust; // -ios-button-adjust

- (BOOL)hasTitleInsets;
- (UIEdgeInsets)titleInsets; // -mobile-title-insets

- (BOOL)hasContentInsets;
- (UIEdgeInsets)contentInsets; // -mobile-content-insets

- (BOOL)hasImageInsets;
- (UIEdgeInsets)imageInsets; // -mobile-image-insets

- (BOOL)hasRelativeToId;
- (NSString*)relativeToId; // -mobile-relative

- (BOOL)hasMarginTop;
- (UXCSSUnit)marginTop; // margin-top

- (BOOL)hasMarginBottom;
- (UXCSSUnit)marginBottom; // margin-bottom

- (BOOL)hasMarginLeft;
- (UXCSSUnit)marginLeft; // margin-left

- (BOOL)hasMarginRight;
- (UXCSSUnit)marginRight; // margin-bottom

- (BOOL)hasTextKey;
- (NSString*)textKey; // -mobile-text-key

- (BOOL) hasHorizontalPadding;
- (UXCSSUnit) horizontalPadding; // padding or -mobile-hPadding

- (BOOL) hasVerticalPadding;
- (UXCSSUnit) verticalPadding; // padding or -mobile-vPadding
@end

/**
 * Adds a raw CSS ruleset to this ruleset object.
 *
 * @fn UXCSSRuleset::addEntriesFromDictionary:
 */

/**
 * Returns YES if the ruleset has a 'color' property.
 *
 * @fn UXCSSRuleset::hasTextColor
 */

/**
 * Returns the text color.
 *
 * @fn UXCSSRuleset::textColor
 */

/**
 * Returns YES if the ruleset has a 'text-align' property.
 *
 * @fn UXCSSRuleset::hasTextAlignment
 */

/**
 * Returns the text alignment.
 *
 * @fn UXCSSRuleset::textAlignment
 */

/**
 * Returns YES if the ruleset has a value for any of the following properties:
 * font, font-family, font-size, font-style, font-weight.
 *
 * Note: You can't specify bold or italic with a font-family due to the way fonts are
 * constructed. You also can't specify a font that is both bold and italic. In order to do
 * either of these things you must specify the font-family that corresponds to the bold or italic
 * version of your font.
 *
 * @fn UXCSSRuleset::hasFont
 */

/**
 * Returns the font.
 *
 * @fn UXCSSRuleset::font
 */

/**
 * Returns YES if the ruleset has a 'text-shadow' property.
 *
 * @fn UXCSSRuleset::hasTextShadowColor
 */

/**
 * Returns the text shadow color.
 *
 * @fn UXCSSRuleset::textShadowColor
 */

/**
 * Returns YES if the ruleset has a 'text-shadow' property.
 *
 * @fn UXCSSRuleset::hasTextShadowOffset
 */

/**
 * Returns the text shadow offset.
 *
 * @fn UXCSSRuleset::textShadowOffset
 */

/**
 * Returns YES if the ruleset has an '-ios-line-break-mode' property.
 *
 * @fn UXCSSRuleset::hasLineBreakMode
 */

/**
 * Returns the line break mode.
 *
 * @fn UXCSSRuleset::lineBreakMode
 */

/**
 * Returns YES if the ruleset has an '-ios-number-of-lines' property.
 *
 * @fn UXCSSRuleset::hasNumberOfLines
 */

/**
 * Returns the number of lines.
 *
 * @fn UXCSSRuleset::numberOfLines
 */

/**
 * Returns YES if the ruleset has an '-ios-minimum-font-size' property.
 *
 * @fn UXCSSRuleset::hasMinimumFontSize
 */

/**
 * Returns the minimum font size.
 *
 * @fn UXCSSRuleset::minimumFontSize
 */

/**
 * Returns YES if the ruleset has an '-ios-adjusts-font-size' property.
 *
 * @fn UXCSSRuleset::hasAdjustsFontSize
 */

/**
 * Returns the adjustsFontSize value.
 *
 * @fn UXCSSRuleset::adjustsFontSize
 */

/**
 * Returns YES if the ruleset has an '-ios-baseline-adjustment' property.
 *
 * @fn UXCSSRuleset::hasBaselineAdjustment
 */

/**
 * Returns the baseline adjustment.
 *
 * @fn UXCSSRuleset::baselineAdjustment
 */

/**
 * Returns YES if the ruleset has an 'opacity' property.
 *
 * @fn UXCSSRuleset::hasOpacity
 */

/**
 * Returns the opacity.
 *
 * @fn UXCSSRuleset::opacity
 */

/**
 * Returns YES if the ruleset has a 'background-color' property.
 *
 * @fn UXCSSRuleset::hasBackgroundColor
 */

/**
 * Returns the background color.
 *
 * @fn UXCSSRuleset::backgroundColor
 */

/**
 * Returns YES if the ruleset has a 'border-radius' property.
 *
 * @fn UXCSSRuleset::hasBorderRadius
 */

/**
 * Returns the border radius.
 *
 * @fn UXCSSRuleset::borderRadius
 */

/**
 * Returns YES if the ruleset has a 'border' or 'border-color' property.
 *
 * @fn UXCSSRuleset::hasBorderColor
 */

/**
 * Returns the border color.
 *
 * @fn UXCSSRuleset::borderColor
 */

/**
 * Returns YES if the ruleset has a 'border' or 'border-width' property.
 *
 * @fn UXCSSRuleset::hasBorderWidth
 */

/**
 * Returns the border width.
 *
 * @fn UXCSSRuleset::borderWidth
 */

/**
 * Returns YES if the ruleset has an '-ios-tint-color' property.
 *
 * @fn UXCSSRuleset::hasTintColor
 */

/**
 * Returns the tint color.
 *
 * @fn UXCSSRuleset::tintColor
 */

/**
 * Returns YES if the ruleset has a 'width' property.
 *
 * @fn UXCSSRuleset::hasWidth
 */

/**
 * Returns the width.
 *
 * @fn UXCSSRuleset::width
 */

/**
 * When relativeToId is set, a view will be positioned using margin-* directives relative to the view
 * identified by relativeToId. You can use id notation, e.g. #MyButton, or a few selectors:
 * .next, .prev, .first and .last which find the obviously named siblings. Note that the mechanics or
 * margin are not the same as CSS, which is of course a flow layout. So you cannot, for example,
 * combine margin-top and margin-bottom as only margin-top will be executed.
 *
 * Relative positioning also requires that you're careful about the order in which you register views
 * in the engine (for now), since we will evaluate the rules immediately. TODO add some simple dependency
 * management to make sure we've run the right views first.
 *
 * @fn UXCSSRuleset::relativeToId
 */

/**
 * In combination with relativeToId, the margin fields control how a view is positioned relative to another.
 * margin-top: 0 means the top of this view will be aligned to the bottom of the view identified by relativeToId.
 * A positive number will move this further down, and a negative number further up. A *percentage* will operate
 * off the height of relativeToId and modify the position relative to margin-top:0. So -100% means "align top".
 * A value of auto means we will align the center y of relativeToId with the center y of this view.
 *
 * @fn UXCSSRuleset::margin-top
 */

/**
 * In combination with relativeToId, the margin fields control how a view is positioned relative to another.
 * margin-bottom: 0 means the bottom of this view will be aligned to the bottom of the view identified by relativeToId.
 * A positive number will move this further down, and a negative number further up. A *percentage* will operate
 * off the height of relativeToId and modify the position relative to margin-bottom:0. So -100% means line up the bottom
 * of this view with the top of relativeToId.
 * A value of auto means we will align the center y of relativeToId with the center y of this view.
 *
 * @fn UXCSSRuleset::margin-bottom
 */

/**
 * In combination with relativeToId, the margin fields control how a view is positioned relative to another.
 * margin-left: 0 means the left of this view will be aligned to the right of the view identified by relativeToId.
 * A positive number will move this further right, and a negative number further left. A *percentage* will operate
 * off the width of relativeToId and modify the position relative to margin-left:0. So -100% means line up the left
 * of this view with the left of relativeToId.
 * A value of auto means we will align the center x of relativeToId with the center x of this view.
 *
 * @fn UXCSSRuleset::margin-left
 */

/**
 * In combination with relativeToId, the margin fields control how a view is positioned relative to another.
 * margin-right: 0 means the right of this view will be aligned to the right of the view identified by relativeToId.
 * A positive number will move this further right, and a negative number further left. A *percentage* will operate
 * off the width of relativeToId and modify the position relative to margin-left:0. So -100% means line up the right
 * of this view with the left of relativeToId.
 * A value of auto means we will align the center x of relativeToId with the center x of this view.
 *
 * @fn UXCSSRuleset::margin-right
 */

/**
 * Return the rule values for a particular key, such as margin-top or width. Exposing this allows you, among
 * other things, use the CSS to hold variable information that has an effect on the layout of the views that
 * cannot be expressed as a style - such as padding.
 *
 * @fn UXCSSRuleset::cssRuleForKey
 */

/**
 * For views that support sizeToFit, padding will add a value to the computed size
 *
 * @fn UXCSSRuleset::horizontalPadding
 */

/**
 * For views that support sizeToFit, padding will add a value to the computed size
 *
 * @fn UXCSSRuleset::verticalPadding
 */

