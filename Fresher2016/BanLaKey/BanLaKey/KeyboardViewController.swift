//
//  KeyboardViewController.swift
//  BanLaKeyBoard
//
//  Created by BaoNQ on 8/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

class KeyboardViewController: UIInputViewController {

    var keyboard: BLKeyboard?
    var firstRun: Bool = true
    var onceToken: dispatch_once_t = 0
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        // Add custom view sizing constraints here
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if (self.firstRun == false) {
            UIView.animateWithDuration(0.0, animations: {
                self.keyboard?.updateState(self.view.frame)
            })
        }

        dispatch_once(&onceToken, {
            self.firstRun = false;
            self.keyboard = BLKeyboard(frame: self.view.frame, textProxy: self.textDocumentProxy)
            self.view.addSubview(self.keyboard!)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Perform custom UI setup here
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated
    }

    override func textWillChange(textInput: UITextInput?) {
        // The app is about to change the document's contents. Perform any preparation here.
    }

    override func textDidChange(textInput: UITextInput?) {
        // The app has just changed the document's contents, the document context has been updated.
    
        var textColor: UIColor
        let proxy = self.textDocumentProxy
        if proxy.keyboardAppearance == UIKeyboardAppearance.Dark {
            textColor = UIColor.whiteColor()
        } else {
            textColor = UIColor.blackColor()
        }
       // self.nextKeyboardButton.setTitleColor(textColor, forState: .Normal)
    }

}
