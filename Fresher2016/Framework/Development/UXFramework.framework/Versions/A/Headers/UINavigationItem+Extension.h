/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@interface UINavigationItem (Extension)

/**
 *  Add a bar button item on top-left corner
 *
 *  @param leftBarButtonItem A bar button item object
 */
- (void)addLeftBarButtonItem:(UIBarButtonItem *)leftBarButtonItem;

/**
 *  Add a bar button item on top-right corner
 *
 *  @param rightBarButtonItem A bar button item object
 */
- (void)addRightBarButtonItem:(UIBarButtonItem *)rightBarButtonItem;

/**
 *  Update title of left bar button
 *
 *  @param title The title you wanted to change
 */
- (void)updateLeftBarButtonItem:(NSString*)title;

/**
 *  Update title of right bar button
 *
 *  @param title The title you wanted to change
 */
- (void)updateRightBarButtonItem:(NSString*)title;

/**
 *  Update both title and icon images of left button item
 *
 *  @param title        The title you wanted to change
 *  @param normalIcon   The image path is used for default state
 *  @param selectedIcon The image path is used for selected state
 */
- (void)updateLeftBarButtonItem:(NSString*)title withIconNormal:(NSString*)normalIcon withIconSelected:(NSString*)selectedIcon;

/**
 *  Update both title and icon images of right button item
 *
 *  @param title        The title you wanted to change
 *  @param normalIcon   The image path is used for default state
 *  @param selectedIcon The image path is used for selected state
 */
- (void)updateRightBarButtonItem:(NSString*)title withIconNormal:(NSString*)normalIcon withIconSelected:(NSString*)selectedIcon;

@end
