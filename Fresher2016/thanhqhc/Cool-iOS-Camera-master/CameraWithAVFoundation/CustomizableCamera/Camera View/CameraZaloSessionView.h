//
//  CameraZaloSessionView.h
//  CameraWithAVFoundation
//
//  Created by qhcthanh on 6/9/16.
//  Copyright © 2016 Gabriel Alvarado. All rights reserved.
//

#import "CameraSessionView.h"
#import "UIKit/UIKit.h"
@interface CameraZaloSessionView : CameraSessionView


@property (nonnull, nonatomic, strong) UIView *topBarView;
@property (nonnull, nonatomic, strong) CameraFlashButton *cameraFlash;

@property (nonnull, nonatomic, strong) UIView *bottomBarView;
@property (nonnull, nonatomic, strong) CameraDismissButton *cameraDismiss;
@property (nonnull, nonatomic, strong) CameraShutterButton *cameraShutter;
@property (nonnull, nonatomic, strong) CameraToggleButton *cameraToggle;
@property (nonnull, nonatomic, strong) CameraShutterButton *cameraRecord;

@end
