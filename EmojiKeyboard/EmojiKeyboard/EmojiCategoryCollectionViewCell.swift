//
//  EmojiCategoryCollectionViewCell.swift
//  EmojiKeyboard
//
//  Created by Quach Ha Chan Thanh on 8/14/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import UIKit

protocol EmojiCategoryCollectionViewCellDelegate: NSObjectProtocol {
    func didSelectEmojiProtocol(emojiProtocol: SimpleModelProtocol)
}

public class EmojiCategoryCollectionViewCell: UICollectionViewCell {
    
    public var emojiCategory: EmojiCategory?
    public var emojiCollectionView: UICollectionView!
    public var collectionViewLayout = UICollectionViewFlowLayout()
    
    weak var delegate: EmojiCategoryCollectionViewCellDelegate?
    
    private weak var lastCellPress: UICollectionViewCell? {
        willSet {
            if let newValue = newValue {
                if newValue != self.lastCellPress {
                    timerShowPopup.invalidate()
                    if let newValue = newValue as? EmojiCollectionViewCell {
                        self.willShowPopupEmoji(newValue)
                    }
                }
            } else {
                timerShowPopup.invalidate()
            }
            
            self.lastCellPress = newValue
        }
    }
    
    override private init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    
    public func bindingUI(emojiCategory: EmojiCategory) {
        
        self.emojiCategory = emojiCategory
        
        self.initializeEmojiCollectionView()
    }
    
    private func initializeEmojiCollectionView() {
        if self.emojiCollectionView == nil {
            self.emojiCollectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: self.collectionViewLayout)
            self.emojiCollectionView.backgroundColor = .clearColor()
            
            self.addSubview(self.emojiCollectionView)
            
            self.emojiCollectionView.constrain(toEdgesOf: self)
            
            let emojiItemCellNib = UINib(nibName: "EmojiCollectionViewCell", bundle: nil)
            self.emojiCollectionView.registerNib(emojiItemCellNib, forCellWithReuseIdentifier: "EmojiItemCell")
            
            self.collectionViewLayout.itemSize = CGSizeMake(25, 25)
            self.collectionViewLayout.minimumLineSpacing = emojiMinimumLineSpacing
            self.collectionViewLayout.minimumInteritemSpacing = emojiMinimumItemSpacing
            
            self.emojiCollectionView.showsVerticalScrollIndicator = false
            self.emojiCollectionView.showsHorizontalScrollIndicator = false
            
            self.emojiCollectionView.delegate = self
            self.emojiCollectionView.dataSource = self
            
            let holdEmojiGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.onPressLongEmoji))
            holdEmojiGesture.minimumPressDuration = 0.1
            self.emojiCollectionView.addGestureRecognizer(holdEmojiGesture)

        }
        
        self.emojiCollectionView.reloadData()
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        
        emojiCategory = nil
        self.emojiCollectionView.reloadData()
    
    }
    
    var timerShowPopup: NSTimer = NSTimer()
    
    func willShowPopupEmoji(emojiCell: UICollectionViewCell) {
        timerShowPopup = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(didShowPupupEmoji), userInfo: ["emoji":emojiCell], repeats: false)
    }
    
    func didShowPupupEmoji() {
        if let emojiCell = self.timerShowPopup.userInfo!["emoji"] as? EmojiCollectionViewCell {
            showPopupInView(emojiCell,customView: viewPopup, size: CGSizeMake(20, 40))
        }
        
    }
    
    func onPressLongEmoji(longPressGesture: UILongPressGestureRecognizer) {
        
        let locationInCollectionView = longPressGesture.locationInView(longPressGesture.view)
        
        if let indexPath = self.emojiCollectionView.indexPathForItemAtPoint(locationInCollectionView),
            let cell = self.emojiCollectionView.cellForItemAtIndexPath(indexPath)
        {
            
            // Neu moi bat dau thi to xam va gan lastCellPress = no
            if longPressGesture.state == .Began {
                cell.contentView.backgroundColor = .grayColor()
                self.lastCellPress = cell
            } // Neu thay doi
            else if longPressGesture.state == .Changed {
                // Kiem tra neu khong phai la cell cu thi doi mau bthg cell cu~ va to mau` cell moi
                if cell != self.lastCellPress {
                    if let lastCellPress = self.lastCellPress {
                        lastCellPress.contentView.backgroundColor = .clearColor()
                    }
                    cell.contentView.backgroundColor = .grayColor()
                    self.lastCellPress = cell
                }
             // Neu touch ket thuc se goi delegate touch emoji da chon va` bo to mau` cell do , gan lastCell = nil
            } else if longPressGesture.state == .Ended {
                
                if let lastCellPress = self.lastCellPress
                {
                    lastCellPress.contentView.backgroundColor = .clearColor()
                    if let lastCellPress = lastCellPress as? EmojiCollectionViewCell,
                        let emojiProtocol = lastCellPress.itemProtocol {
                        
                        // Call delegate touch emoji
                        self.didSelectEmojiProtocol(emojiProtocol)
                    }
                }
                
                self.lastCellPress = nil
            }
        } // Ktra neu lastCell # nil thi remove va gan lai clear color
        else {
            if let lastCellPress = self.lastCellPress {
                lastCellPress.contentView.backgroundColor = .clearColor()
            }
            self.lastCellPress = nil
        }
    }
    
    func didSelectEmojiProtocol(emojiProtocol: SimpleModelProtocol) {
        if let delegate = self.delegate {
            delegate.didSelectEmojiProtocol(emojiProtocol)
        }
    }
    
}

extension EmojiCategoryCollectionViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
    
    public func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let emojiCategory = self.emojiCategory {
            return emojiCategory.emojis.count
        } else {
            return 0
        }
    }
    
    public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let emojiCell = collectionView.dequeueReusableCellWithReuseIdentifier("EmojiItemCell", forIndexPath: indexPath) as! EmojiCollectionViewCell
        let emojiModel = emojiCategory!.emojis[indexPath.row]
        emojiCell.bindingUI(emojiModel)
        
        return emojiCell
    }
    
    public override func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
}





