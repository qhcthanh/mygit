/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXAction.h"

/**
 * The UXCollectionViewActions class provides an interface for attaching actions to objects in a
 * UXCollectionViewDataSource.
 *
 * <h2>Basic Use</h2>
 *
 * UXCollectionViewDataSource and UXCollectionViewActions cooperate to solve two related tasks: data
 * representation and user actions, respectively. A UXCollectionViewDataSource is composed of objects and
 * UXCollectionViewActions maintains a mapping of actions to these objects. The object's attached
 * actions are executed when the user interacts with the cell representing an object.
 *
 * <h2>Delegate Forwarding</h2>
 *
 * Your delegate implementation can call the listed collectionView: methods in order for the
 * collection view to respond to user actions. Notably shouldHighlightItemAtIndexPath: allows
 * cells to be highlighted only if the cell's object has an attached action.
 * didSelectItemAtIndexPath: will execute the object's attached tap actions.
 *
 * If you use the delegate forwarders your collection view's data source must be an instance of
 * UXCollectionViewDataSource.
 *
 * @ingroup CollectionViewTools
 */
@interface UXCollectionViewActions : UXAction

/**
 * Asks the receiver whether the object at the given index path is actionable.
 *
 * collectionView.dataSource must be a UXCollectionViewDataSource.
 *
 *    @returns YES if the object at the given index path is actionable.
 *    @fn UXCollectionViewActions::collectionView:shouldHighlightItemAtIndexPath:
 */
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath;

/**
 * Asks the receiver to perform the tap action for an object at the given indexPath.
 *
 * collectionView.dataSource must be a UXCollectionViewDataSource.
 *
 *    @fn UXCollectionViewActions::collectionView:didSelectItemAtIndexPath:
 */
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;

@end


