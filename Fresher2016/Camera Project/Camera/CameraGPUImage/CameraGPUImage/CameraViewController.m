//
//  CameraViewController.m
//  ZaloCameraCustom
//
//  Created by qhcthanh on 6/14/16.
//  Copyright © 2016 qhcthanh. All rights reserved.
//

#import "CameraViewController.h"
#import "PreviewImageViewController.h"
#import "GPUStickerView.h"
#import "PreviewFilterRealTimeView.h"
#import "UXOverlayView.h"
#import "UXImageTransitionView.h"
#import "UXTransitionView.h"
#import "UXTextTransitionView.h"
#import "UXTransitionDetail.h"
#import "UXTextTransitionDetail.h"
#import "UXImageTransitionDetail.h"
#import <AssetsLibrary/ALAssetsLibrary.h>
#import "MDFrameDrawer.h"
#import "MDCameraManager.h"
#import "SVProgressHUD.h"
#import <Photos/Photos.h>

#define kWidth [[UIScreen mainScreen] bounds].size.width
#define kHeight [[UIScreen mainScreen] bounds].size.height

@interface CameraViewController () <UIGestureRecognizerDelegate, PreviewFilterRealTimeViewDelegate, UXOverlayViewDelegate, UIAlertViewDelegate> {
    
    __weak IBOutlet UIButton *trashButton;
    __weak IBOutlet NSLayoutConstraint *heightTrashButtonContrainst;
    __weak IBOutlet NSLayoutConstraint *widthTrashButtonContrainst;
    UXOverlayView *stickerOverlayView;
    BOOL isInTrash;
    NSURL *outUrl;
    
    MDCameraManager *cameraManager;
    MDFrameDrawer *frameDrawer;

}
@end

@implementation CameraViewController {
    GPUImageOutput<GPUImageInput> *filter;
    GPUImageMovieWriter *movieWriter;
    GPUImageStillCamera *stillCamera;
    //GPUImageVideoCamera *videoCamera;
    

    NSMutableArray<Class> *_filters;
    NSUInteger _curFilter;
    NSURL *outputFileURL;
    BOOL isRecording;
    BOOL isStartRecord;
    BOOL isFlashOn;
    BOOL isNewFilter;
    
    //Sticker view
    NSMutableArray<UIImage *> *imageList;
    PreviewFilterRealTimeView* previewFilter;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    // Set up filters;
    isRecording = YES;
    isFlashOn = NO;
    _curFilter = 0;
    _filters = [[NSMutableArray alloc] init];
   
    [_filters addObject:[IFNormalFilter class]];
    [_filters addObject:[IF1977Filter class]];
    [_filters addObject:[IFBrannanFilter class]];
    [_filters addObject:[IFEarlybirdFilter class]];
    [_filters addObject:[IFInkwellFilter class]];
    [_filters addObject:[IFLomofiFilter class]];
    [_filters addObject:[IFLordKelvinFilter class]];
    [_filters addObject:[IFNashvilleFilter class]];
    [_filters addObject:[IFValenciaFilter class]];
    

    [_filters addObject:[IFWaldenFilter class]];
    [_filters addObject:[IFXproIIFilter class]];
    [_filters addObject:[IFAmaroFilter class]];
    [_filters addObject:[IFHefeFilter class]];
    [_filters addObject:[IFHudsonFilter class]];
    [_filters addObject:[IFRiseFilter class]];
    [_filters addObject:[IFSierraFilter class]];
    [_filters addObject:[IFSutroFilter class]];
    [_filters addObject:[IFToasterFilter class]];

    //set private property
    _lastScale = 1;
    
    //init filter
    filter = [[_filters objectAtIndex:0] new];
    
    // Add swipe gsture in camera view
//    UISwipeGestureRecognizer* swipeLeftCameraViewGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeFilterGesture:)];
//    swipeLeftCameraViewGesture.direction = UISwipeGestureRecognizerDirectionLeft;
//    
//    UISwipeGestureRecognizer* swipeRightCameraViewGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeFilterGesture:)];
//    swipeRightCameraViewGesture.direction = UISwipeGestureRecognizerDirectionRight;
//    
//    [_cameraView addGestureRecognizer: swipeRightCameraViewGesture];
//    [_cameraView addGestureRecognizer: swipeLeftCameraViewGesture];
    
    
    // add swipe gesture in mode view
    UISwipeGestureRecognizer* swipeLeftModeViewGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeModeGesture:)];
    swipeLeftModeViewGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    
    UISwipeGestureRecognizer* swipeRightModeViewGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeModeGesture:)];
    swipeRightModeViewGesture.direction = UISwipeGestureRecognizerDirectionRight;
    
    [_modeView addGestureRecognizer: swipeRightModeViewGesture];
    [_modeView addGestureRecognizer: swipeLeftModeViewGesture];
    
    //Add tap gesture in label mode
    UITapGestureRecognizer* photoTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeModePhoto)];
    [photoTapGesture setNumberOfTapsRequired:1];
    UITapGestureRecognizer* videoTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeModeVideo)];
    [videoTapGesture setNumberOfTapsRequired:1];
    [_photoModeLabel addGestureRecognizer:photoTapGesture];
    [_videoModeLabel addGestureRecognizer:videoTapGesture];
    
    //Add pinch gesture in cameraview
//    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
//    pinchGesture.delegate = self;
//    [_cameraView addGestureRecognizer:pinchGesture];
    
    // init stillCamera
    stillCamera = [[GPUImageStillCamera alloc] initWithSessionPreset:AVCaptureSessionPreset640x480 cameraPosition:AVCaptureDevicePositionBack];
    stillCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
    
    [self.view bringSubviewToFront:self.topBar];
    [self.view bringSubviewToFront:self.bottomBar];
    
    imageList = [[NSMutableArray alloc] init];
    [imageList addObject:[UIImage imageNamed:@"image.png"]];
    [imageList addObject:[UIImage imageNamed:@"sticker.png"]];
    
    previewFilter = [[PreviewFilterRealTimeView alloc]
                     initWithCamera: stillCamera
                     frame:
                     CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height - 90 - 50) filters:_filters];
    [self.view addSubview:previewFilter];
    
    //setup camera
    [self setupCamera];
    
    stickerOverlayView = [[UXOverlayView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    [self.view addSubview:stickerOverlayView];
    [self.view bringSubviewToFront:stickerOverlayView];
    stickerOverlayView.touchDelegate = self;
    stickerOverlayView.userInteractionEnabled = YES;
    stickerOverlayView.nextView = nil;
    
    [self.view bringSubviewToFront:_topBar];
    [self.view bringSubviewToFront:_bottomBar];
    
    previewFilter.delegate = self;
}

-(void)setupCamera {
    isNewFilter = YES;
    // create video cameraManager
    cameraManager = [[MDCameraManager alloc] initWithStillCamera:stillCamera filter:filter];
    cameraManager.shouldCaptureAudio = NO;
    
    // context initialization - block (we dont want to overload class in this example)
    void (^contextInitialization)(CGContextRef context, CGSize size) = ^(CGContextRef context, CGSize size) {
        CGContextClearRect(context, CGRectMake(0, 0, size.width, size.height));
    };
    
    // create overlay
    frameDrawer = [[MDFrameDrawer alloc] initWithSize:CGSizeMake(480, 640)
                               contextInitailizeBlock:contextInitialization];
    
    // setup composer, preview and cameraManager all together
    
    //[cameraManager.composer addTarget:previewFilter.currentFilterView];
    
    [cameraManager setOverlay:frameDrawer];
    
    [cameraManager startCameraCapture];

}

- (void)updateContextUpdateBlock {
    __block BOOL _isStartRecord = isStartRecord;
    __block BOOL _isNewFilter = isNewFilter;
    
    //init weak reference stickerOverlayView to avoid retain cycle
    UXOverlayView *weakStickerOverlayView = stickerOverlayView;
    
    frameDrawer.contextUpdateBlock = ^BOOL(CGContextRef context, CGSize size, CMTime time) {
        if (_isStartRecord) {
            CGContextClearRect(context, CGRectMake(0, 0, size.width, size.height));
            CGFloat scale = size.width / stickerOverlayView.frame.size.width;
          
            if (_isNewFilter) {
                //weakStickerOverlayView.layer.transform = CATransform3DMakeScale(.45, .45, 1);
                CGContextScaleCTM(context, scale, scale);
                
                CGAffineTransform transform = CGAffineTransformIdentity;
                transform = CGAffineTransformScale(transform, 1.0f, -1.0f);
                transform = CGAffineTransformTranslate(transform, 0.0f, (-size.height/scale - kHeight)/2.0);
                CGContextConcatCTM(context, transform);
                _isNewFilter = NO;
            }
            
            [weakStickerOverlayView.layer renderInContext:context];
            _isStartRecord = NO;
        }
        
        return YES;
    };
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Remove old filter
    //[stillCamera removeTarget:filter];
    [stickerOverlayView setUserInteractionEnabled:YES];

    // start/resume camera
    [cameraManager startCameraCapture];
    [cameraManager resumeCameraCapture];
    
    [self changeModeCamera];
}

- (void)viewWillDisappear:(BOOL)animated{
    
}

-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    NSLog(@"CAMERA VIEW CONTROLLER: dealloc");
    [filter removeAllTargets];
    [stillCamera removeAllTargets];
    
    stillCamera = nil;
    previewFilter.delegate = nil;
    previewFilter = nil;
}

#pragma mark -
#pragma mark - Handler camera capture

- (void)startCameraCapture
{
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:@"file.mov"];
    outUrl = [NSURL fileURLWithPath:path];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    [fm removeItemAtPath:path error:nil];
    
    NSLog(@"Recording ...");
    
    [cameraManager startCameraRecordingWithURL:outUrl size:CGSizeMake(kWidth, kHeight)];
}

- (void)stopCameraCapture
{
    if(!cameraManager.isRecording) {
        return;
    }
    [cameraManager pauseCameraCapture];
    NSURL *movieUrl = outUrl;
    
    [cameraManager stopCameraRecordingWithCompetionHandler:^(){
        
        dispatch_async(dispatch_get_main_queue(), ^(){
            NSLog(@"Recorded :/");
            // start process hud
            [SVProgressHUD showWithStatus:@"Exporting..."];
            
            // Save to the album
            __block PHObjectPlaceholder *placeholder;
            
            [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                PHAssetChangeRequest* createAssetRequest = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:movieUrl];
                placeholder = [createAssetRequest placeholderForCreatedAsset];
                
            } completionHandler:^(BOOL success, NSError *error) {
                if (success)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // dismiss process hud
                        [SVProgressHUD dismiss];
                        
                        // show alert
                        [self showAlertWithMessage:@"Saved To Photo Album" withTitle:@"Video Saved"];
                        
                        // show preview video
                        AVPlayer* avPlayer = [AVPlayer playerWithURL:movieUrl];
                        PreviewVideoViewController* previewVC = [PreviewVideoViewController new];
                        previewVC.avPlayer = avPlayer;
                        [self presentViewController:previewVC animated:true completion:nil];
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // dismis process hud
                        [SVProgressHUD dismiss];
                        
                        // show alert
                        [self showAlertWithMessage:@"Video Saving Failed" withTitle:@"Error"];
                    });
                    
                }
            }];
        });
    }];
}

// get ViewController in top
- (UIViewController*) topMostController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

// show alert dialog
-(void)showAlertWithMessage:(NSString*)message withTitle:(NSString *)title{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [cameraManager resumeCameraCapture];
        }]];
        
        [[self topMostController] presentViewController:alertController animated:YES completion:^{
        }];
    });
}

//MARK: Change mode camera
-(void)changeModeCamera {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, false), ^{
        if (stillCamera && _leadingModeViewConstraint.constant == LEADING_PHOTO_MODEVIEW_CONTRAINST) {
            if (isFlashOn == YES) {
                [stillCamera.inputCamera lockForConfiguration:nil];
                
                [stillCamera.inputCamera setTorchMode:AVCaptureTorchModeOff];
                [stillCamera.inputCamera setFlashMode:AVCaptureFlashModeOn];
                
                [stillCamera.inputCamera unlockForConfiguration];
            }
            
        } else if (stillCamera && _leadingModeViewConstraint.constant == LEADING_VIDEO_MODEVIEW_CONTRAINST) {
            if (isFlashOn == YES) {
                [stillCamera.inputCamera lockForConfiguration:nil];
                
                [stillCamera.inputCamera setFlashMode:AVCaptureFlashModeOff];
                [stillCamera.inputCamera setTorchMode:AVCaptureTorchModeOn];
                
                [stillCamera.inputCamera unlockForConfiguration];
            }
        }
    });
}

-(void)changeModePhoto {
    //previewFilter.currentFilterView.fillMode = kGPUImageFillModePreserveAspectRatioAndFill;
    _photoModeLabel.textColor = [UIColor yellowColor];
    _videoModeLabel.textColor = [UIColor whiteColor];
    [_shutterButton setImage: [UIImage imageNamed:@"Circle Thin-96"] forState:UIControlStateNormal];
    _leadingModeViewConstraint.constant = LEADING_PHOTO_MODEVIEW_CONTRAINST;
    _timeVideoLabel.hidden = true;
    [_countTimer invalidate];
    [movieWriter finishRecording];
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    [self changeModeCamera];
}

-(void)changeModeVideo {
    //previewFilter.currentFilterView.fillMode = kGPUImageFillModeStretch;
    _photoModeLabel.textColor = [UIColor whiteColor];
    _videoModeLabel.textColor = [UIColor yellowColor];
    [_shutterButton setImage: [UIImage imageNamed:@"Record"] forState:UIControlStateNormal];
    _leadingModeViewConstraint.constant = LEADING_VIDEO_MODEVIEW_CONTRAINST;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    [self changeModeCamera];
}

//MARK: UIGestureRecognizer
-(void)swipeModeGesture:(UISwipeGestureRecognizer*)swipeGesture {
    if (swipeGesture.direction == UISwipeGestureRecognizerDirectionRight) {
        [self changeModePhoto];
    } else {
        [self changeModeVideo];
    }
}

//-(void)swipeFilterGesture:(UISwipeGestureRecognizer*)swipeGesture {
//    if (swipeGesture.direction == UISwipeGestureRecognizerDirectionRight) {
//        // swipe right
//        if (_curFilter == 0)
//            return;
//        _curFilter--;
//        NSLog(@"swipe right");
//    } else {
//        // swipe left
//        if (_curFilter == _filters.count - 1)
//            return;
//        _curFilter++;
//        NSLog(@"swipe left");
//    }
//    [stillCamera removeTarget:filter];
//    
//    if (movieWriter)
//        [filter removeTarget:movieWriter];
//    
//    filter = [[[_filters objectAtIndex:_curFilter] alloc] init];
//    
//    [filter addTarget:self.cameraView];
//    
//    if (movieWriter)
//        [filter addTarget:movieWriter];
//    
//    [stillCamera addTarget:filter];
//  
//}
//
//- (void)handlePinchGesture:(UIPinchGestureRecognizer *)gestureRecognizer {
//    
//        if([gestureRecognizer state] == UIGestureRecognizerStateBegan) {
//            // Reset the last scale, necessary if there are multiple objects with different scales
//            _lastScale = [gestureRecognizer scale];
//        }
//    
//        if ([gestureRecognizer state] == UIGestureRecognizerStateBegan ||
//            [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
//    
//            CGFloat currentScale = [[[gestureRecognizer view].layer valueForKeyPath:@"transform.scale"] floatValue];
//    
//            // Constants to adjust the max/min values of zoom
//            const CGFloat kMaxScale = 5.0;
//            const CGFloat kMinScale = 1.0;
//    
//            CGFloat newScale = 1 -  (_lastScale - [gestureRecognizer scale]);
//            newScale = MIN(newScale, kMaxScale / currentScale);
//            newScale = MAX(newScale, kMinScale / currentScale);
//            CGAffineTransform transform = CGAffineTransformScale([[gestureRecognizer view] transform], newScale, newScale);
//            [gestureRecognizer view].transform = transform;
//    
//    
//            _lastScale = [gestureRecognizer scale];  // Store the previous scale factor for the next pinch gesture call
//        }
//}

//MARK: UIButton action
- (IBAction)onTapFlashAction:(id)sender {
    NSString* imageFlashName = @"Flash Off";
    NSInteger mode = AVCaptureTorchModeOff;
    
    if(isFlashOn == NO) {
        mode = AVCaptureTorchModeOn;
        imageFlashName = @"Flash On";
        isFlashOn = YES;
    }
    else {
       isFlashOn = NO;
    }
    
    [_flashButton setImage:[UIImage imageNamed:imageFlashName] forState: UIControlStateNormal];
    
    [stillCamera.inputCamera lockForConfiguration:nil];
    if(stillCamera && _leadingModeViewConstraint.constant == LEADING_PHOTO_MODEVIEW_CONTRAINST) {
        [stillCamera.inputCamera setFlashMode:mode];
    }
    else {
        if (stillCamera && _leadingModeViewConstraint.constant == LEADING_VIDEO_MODEVIEW_CONTRAINST) {
            [stillCamera.inputCamera setTorchMode:mode];
        }
    }
    [stillCamera.inputCamera unlockForConfiguration];
    
}

- (IBAction)onTapBackAction:(id)sender {
    [stillCamera removeAllTargets];
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)onTapRotateAction:(id)sender {
    //[_cameraView onTapToggleButton];
    [stillCamera rotateCamera];
    
}

- (IBAction)onTapShutterAction:(id)sender {
    if (_leadingModeViewConstraint.constant == LEADING_PHOTO_MODEVIEW_CONTRAINST) {
        static BOOL canCapture = YES;
        if (!canCapture) {
            return;
        }

        [stillCamera addTarget:filter];
        canCapture = NO;
        
        // Handler process image
        [stillCamera capturePhotoAsImageProcessedUpToFilter:filter withCompletionHandler:^(UIImage *processedImage, NSError *error) {
            canCapture = YES;
            // Present to preview image controller
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle: nil];
            PreviewImageViewController *showImageVC = (PreviewImageViewController*)[mainStoryboard
                                                               instantiateViewControllerWithIdentifier: @"PreviewImageViewController"];
            showImageVC.previewImage = processedImage;
            showImageVC.indexFilter = _curFilter;
            showImageVC.filterList = _filters;
            showImageVC.stickers = [stickerOverlayView getStickerList];
            
            //[stillCamera stopCameraCapture];
            
            [self presentViewController:showImageVC animated:true completion:nil];
        }];
    }
    else {
//        if (isRecording) {
//            //Setup const
//            isRecording = NO;
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, false), ^{
//                [filter removeTarget:movieWriter];
//                // create url save temp video
//                NSString* outputFileName = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Movie.m4v"];
//                // If a file already exists, AVAssetWriter won't let you record new frames, so delete the old movie
//                unlink([outputFileName UTF8String]);
//                outputFileURL = [NSURL fileURLWithPath:outputFileName];
//                //setup movie writer
//                
//                movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:outputFileURL size:CGSizeMake(480.0, 640.0)];
//                stillCamera.audioEncodingTarget = movieWriter;
//                //videoCamera.audioEncodingTarget = movieWriter;
//                if (isFlashOn) {
//                        [stillCamera.inputCamera lockForConfiguration:nil];
//                        [stillCamera.inputCamera setTorchMode:isFlashOn];
//                        [stillCamera.inputCamera unlockForConfiguration];
//                }
//                
//                movieWriter.encodingLiveVideo = YES;
//                [stillCamera addTarget:filter];
//                [filter addTarget:movieWriter];
//                
//                //Start recording
//                [self didStartRecordingVideo];
//            });
//        } else {
//            [self didFinishRecordingVideo];
//            
//        }
        
        if(cameraManager.isRecording) {
            [self stopCameraCapture];
        } else {
            isStartRecord = YES;
            [stickerOverlayView setUserInteractionEnabled:NO];
            [self updateContextUpdateBlock];
            isNewFilter = NO;
            [self startCameraCapture];
        }
    }
}



-(NSString*)getImagePathName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *directory = [paths objectAtIndex:0];
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    NSString* uniqueString = [NSString stringWithFormat:@"%f",timeInSeconds];
    uniqueString = [uniqueString stringByReplacingOccurrencesOfString:@"." withString:@""];
    return [NSString stringWithFormat:@"%@/Image%@.jpg", directory ,uniqueString ];
}

- (IBAction)trashAction:(id)sender {
    [stickerOverlayView removeAllStickers];
}

- (IBAction)textAction:(id)sender {
    
}

- (IBAction)stickerAction:(id)sender {
    UIImage *image = [UIImage imageNamed:@"sticker.png"];
    CGPoint touchPosition =  CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    UXImageTransitionView *newSticker = [[UXImageTransitionView alloc] initWithImage:image atCenterPosition:touchPosition];
    [stickerOverlayView addSticker:[[UXImageTransitionDetail alloc] initWithTransitionView:newSticker]];
    //NSLog(@"number of sticker: %lu", [[stickerOverlayView getStickerList] count]);
    //stickerOverlayView.transform = CGAffineTransformMakeScale(1.0, -1.0);
    
}

- (IBAction)previewFilterAction:(id)sender {
    if (previewFilter.canTouch) {
        if (previewFilter && !previewFilter.isShowMenu) {
            [UIView animateWithDuration:0.3 animations:^{
                _topBar.alpha = 1;
                _bottomBar.alpha = 1;
                _shutterButton.alpha = 0;
                _modeView.alpha = 0;
                _backButton.alpha = 0;
            }];
            
            [self.view bringSubviewToFront:previewFilter];
            [self.view bringSubviewToFront:_bottomBar];
            [self.view bringSubviewToFront:_topBar];
            [previewFilter showFilterMenu];
        } else {
            [self.view bringSubviewToFront:stickerOverlayView];
            [self.view bringSubviewToFront:_bottomBar];
            [self.view bringSubviewToFront:_topBar];
            [previewFilter hideMenuFilter];
            [self hidePrevieFilter];
        }    
    }
}

//MARK: Touch delegate
-(void)didTouchEndSticker:(id)sticker position:(CGPoint)position {
    if (CGRectContainsPoint(trashButton.frame, position)){
        [stickerOverlayView removeSticker:sticker];
        
        heightTrashButtonContrainst.constant = 0;
        widthTrashButtonContrainst.constant = 0;
        isInTrash = NO;
    }
}

-(void)didTouchMoveSticker:(id)sticker position:(CGPoint)position {
    if (CGRectContainsPoint(trashButton.frame, position) && !isInTrash){
        ((UXTransitionDetail *)sticker).transitionView.alpha = 0.4;
        
        heightTrashButtonContrainst.constant = 20;
        widthTrashButtonContrainst.constant = 13;
        isInTrash = YES;
    }
    else if (!CGRectContainsPoint(trashButton.frame, position) && isInTrash){
        ((UXTransitionDetail *)sticker).transitionView.alpha = 1;
        
        heightTrashButtonContrainst.constant = 0;
        widthTrashButtonContrainst.constant = 0;
        isInTrash = NO;
    }
}

//MARK: PreiviewFilterRealTimeViewDelegate
-(void)didSelectFilter:(id)filterT {
    filter = [[filterT alloc] init];
    [self setupCamera];
    
    [self.view bringSubviewToFront:stickerOverlayView];
    [self.view bringSubviewToFront:_bottomBar];
    [self.view bringSubviewToFront:_topBar];
    previewFilter.isShowMenu = false;
    [self hidePrevieFilter];
}

//MARK: private function
-(void)hidePrevieFilter {
    if (previewFilter.superview == self.view) {
        [UIView animateWithDuration:0.3 animations:^{
            _topBar.alpha = 0.5;
            _bottomBar.alpha = 0.5;
            _shutterButton.alpha = 1;
            _modeView.alpha = 1;
            _backButton.alpha = 1;
        }];
    }
}


//-(void)didStartRecordingVideo {
//    if(_countTimer) {
//        _timeCount = 0;
//        [_countTimer invalidate];
//    }
//    dispatch_async(dispatch_get_main_queue(), ^{
//        _modeView.hidden = true;
//    });
//    
//    
//    [movieWriter startRecording];
//    if (![NSThread isMainThread]) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            _timeVideoLabel.hidden = false;
//            _countTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateCountdown) userInfo:nil repeats: YES];
//        });
//    } else {
//        _timeVideoLabel.hidden = false;
//        _countTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateCountdown) userInfo:nil repeats: YES];
//    }
//    
//}
//
//-(void)didFinishRecordingVideo {
//    _timeVideoLabel.hidden = true;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        _modeView.hidden = false;
//    });
//    [_modeView setNeedsDisplay];
//    [_countTimer invalidate];
//    _timeVideoLabel.text = DEFAULT_VIDEO_TIME;
//    stillCamera.audioEncodingTarget = nil;
//    //videoCamera.audioEncodingTarget = nil;
//    [movieWriter finishRecording];
//    //UISaveVideoAtPathToSavedPhotosAlbum([NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Movie.m4v"] , nil, nil, nil);
//    //[stillCamera stopCameraCapture];
//    //[videoCamera stopCameraCapture];
//    NSLog(@"finishRecording");
//    
//    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
//    if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputFileURL])
//    {
//        [library writeVideoAtPathToSavedPhotosAlbum:outputFileURL completionBlock:^(NSURL *assetURL, NSError *error)
//         {
//             dispatch_async(dispatch_get_main_queue(), ^{
//                 
//                 if (error) {
//                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Video Saving Failed"
//                                                                    delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                     [alert show];
//                     
//                 } else {
//                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video Saved" message:@"Saved To Photo Album"
//                                                                    delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                     [alert show];
//                     AVPlayer* avPlayer = [AVPlayer playerWithURL:outputFileURL];
//                     PreviewVideoViewController* previewVC = [PreviewVideoViewController new];
//                     previewVC.avPlayer = avPlayer;
//                     
//                     [self presentViewController:previewVC animated:true completion:nil];
//                     isRecording = YES;
//                 }
//             });
//         }];
//    }
//}
//
//-(void) updateCountdown
//{
//    _timeCount++;
//    NSDate *now = [NSDate dateWithTimeIntervalSince1970:_timeCount];
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    NSDateComponents *componentMint = [calendar components:NSCalendarUnitMinute fromDate:now];
//    NSDateComponents *componentSec = [calendar components:NSCalendarUnitSecond fromDate:now];
//    
//    NSString* minute = [NSString stringWithFormat:@"%02ld",(long)(componentMint.minute)];
//    NSString* second = [NSString stringWithFormat:@"%02ld",(long)(componentSec.second)];
//    
//    _timeVideoLabel.text = [NSString stringWithFormat:@"%@:%@",minute,second];
//    
//    if (_timeCount == 15) {
//        [_countTimer invalidate];
//        [self didFinishRecordingVideo];
//    }
//    
//}

@end
