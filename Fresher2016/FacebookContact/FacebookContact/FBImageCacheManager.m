//
//  FBImageCaching.m
//  FacebookContact
//
//  Created by BaoNQ on 7/14/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import "FBImageCacheManager.h"

@interface FBImageCacheManager ()

@property NSMutableDictionary *cachedImage;
@property dispatch_queue_t internalSerialQueue;
@property NSMutableArray *priorityQueue;
@property NSInteger currentSize;

@end

@implementation FBImageCacheManager

//Singleton Method
+ (id)sharedImageCaching {
    
    static FBImageCacheManager *singletonCachedImage = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        singletonCachedImage = [[self alloc] init];
    });
    
    return singletonCachedImage;
}

- (id)init {
    
    self = [super init];
    if (!self)
        return nil;
    
    _cachedImage = [[NSMutableDictionary alloc] init];
    _internalSerialQueue = dispatch_queue_create("com.vng.FBImageCachingQueue", DISPATCH_QUEUE_SERIAL);
    _cacheLength = 20;
    _cacheSize = 300000; // 2Mb
    _currentSize = 0;
    _cacheMode = kFBCacheModeLimitLengthAndSize;
    
    _priorityQueue = [[NSMutableArray alloc]init];
    
    return self;
}

- (BOOL)resetLengthOfCache:(NSInteger)length {
    
    if (length > [_priorityQueue count])
    {
        _cacheLength = length;
        return YES;
    } else
        return NO;
}

- (BOOL)resetSizeOfCache:(NSInteger)size {
    
    if (size > _currentSize)
    {
        _currentSize = size;
        return YES;
    } else
        return NO;
}

- (void)setCacheMode:(FBCacheMode)cacheMode {
    
    _cacheMode = cacheMode;
}

- (void)cacheImage:(UIImage *)image forKey:(NSString *)aKey {
    
    if (image == nil || aKey == nil)
        return;
    if ([_cachedImage objectForKey:aKey] != nil)
        return;

    NSInteger sizeOfImage = CGImageGetBytesPerRow(image.CGImage) * CGImageGetHeight(image.CGImage);
    BOOL isFullCache = NO;
    if (((_cacheMode | kFBCacheModeLimitLength) && [_cachedImage count] >= _cacheLength) ||
        ((_cacheMode | kFBCacheModeLimitSize) && (_currentSize + sizeOfImage) > _cacheSize))
        
        isFullCache = YES;

    if (!isFullCache){
        
        dispatch_async(_internalSerialQueue, ^{
            [_cachedImage setObject:image forKey:aKey];
            [_priorityQueue addObject:aKey];
        });
        
    } else {
        
        if (_cacheMode == kFBCacheModeLimitSize || _cacheMode == kFBCacheModeLimitLengthAndSize){
            while (isFullCache){
                
                UIImage *imageLestRecentlyUsed = [_cachedImage objectForKey:[_priorityQueue firstObject]];
                _currentSize -= CGImageGetBytesPerRow(imageLestRecentlyUsed.CGImage) * CGImageGetHeight(imageLestRecentlyUsed.CGImage);
                [_cachedImage removeObjectForKey:[_priorityQueue firstObject]];
                [_priorityQueue removeObjectAtIndex:0];
                
                if (((_cacheMode | kFBCacheModeLimitLength) && [_cachedImage count] < _cacheLength) ||
                    ((_cacheMode | kFBCacheModeLimitSize) && (_currentSize + sizeOfImage) <= _cacheSize))
                    isFullCache = NO;
            }
        }
        
        [_cachedImage setObject:image forKey:aKey];
        [_priorityQueue addObject:aKey];
    }
    _currentSize += sizeOfImage;
}

- (UIImage *)imageForKey:(NSString *)aKey {
    
    __block UIImage *returnedImage;
    
    dispatch_sync(_internalSerialQueue, ^{
        
        returnedImage = [_cachedImage objectForKey:aKey];
        if (returnedImage){
            NSInteger index = -1;
            for (NSInteger i = 0; i < [_priorityQueue count]; i++){
                if ([aKey isEqualToString:[_priorityQueue objectAtIndex:i]])
                {
                    index = i;
                    break;
                }
            }
            if (index != -1) {
                [_priorityQueue removeObjectAtIndex:index];
                [_priorityQueue addObject:aKey];
            }
            NSLog(@"Current size: %ld", _currentSize);
            NSLog(@"Current length: %ld", [_priorityQueue count]);
        }
    });
    
    return returnedImage;
}

- (void)removeImageForKey:(NSString *)aKey {
    
    dispatch_async(_internalSerialQueue, ^{
        
        UIImage *image = [_cachedImage objectForKey:aKey];
        _currentSize -= CGImageGetBytesPerRow(image.CGImage) * CGImageGetHeight(image.CGImage);
        [_cachedImage removeObjectForKey:aKey];
        
        NSInteger index = -1;
        for (NSInteger i = 0; i < [_priorityQueue count]; i++){
            if ([aKey isEqualToString:[_priorityQueue objectAtIndex:i]])
            {
                index = i;
                break;
            }
        }
        if (index != -1)
            [_priorityQueue removeObjectAtIndex:index];
    });
}

- (void)removeAllObjects{
    
    dispatch_async(_internalSerialQueue, ^{
        
        [_cachedImage removeAllObjects];
        [_priorityQueue removeAllObjects];
        _currentSize = 0;
    });
}

@end
