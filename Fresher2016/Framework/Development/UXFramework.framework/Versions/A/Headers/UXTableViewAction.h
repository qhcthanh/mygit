/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXAction.h"

/**
 * The UXTableViewAction class provides an interface for attaching actions to objects in a
 * UXTableViewDataSource.
 *
 * <h2>Basic Use</h2>
 *
 * UXTableViewDataSource and UXTableViewAction cooperate to solve two related tasks: data
 * representation and user actions, respectively. A UXTableViewDataSource is composed of objects and
 * UXTableViewAction maintains a mapping of actions to these objects. The object's attached actions
 * are executed when the user interacts with the cell representing an object.
 *
 * <h3>Delegate Forwarding</h3>
 *
 * UXTableViewAction will apply the correct accessoryType and selectionStyle values to the cell
 * when the cell is displayed using a mechanism known as <i>delegate chaining</i>. This effect is
 * achieved by invoking @link UXTableViewAction::forwardingTo: forwardingTo:@endlink on the
 * UXTableViewAction instance and providing the appropriate object to forward to (generally
 * @c self).
 *
 @code
 tableView.delegate = [self.actions forwardingTo:self];
 @endcode
 *
 * The dataSource property of the table view must be an instance of UXTableViewDataSource.
 *
 * @ingroup ModelTools
 */
@interface UXTableViewAction : UXAction <UITableViewDelegate>

#pragma mark Forwarding

/**
 * Sets the delegate that table view methods should be forwarded to.
 *
 * This method allows you to insert the actions into the call chain for the table view's
 * delegate methods.
 *
 * Example:
 *
 // Let the actions handle delegate methods and then forward them to whatever delegate was
 // already assigned.
    self.tableView.delegate = [self.actions forwardingTo:self.tableView.delegate];
 *
 * @param forwardDelegate The delegate to forward invocations to.
 * @returns self so that this method can be chained.
 */

- (id<UITableViewDelegate>)forwardingTo:(id<UITableViewDelegate>)forwardDelegate;

/**
 * Removes the delegate from the forwarding chain.
 *
 * If a forwared delegate is about to be released but this object may live on, you must remove the
 * forwarding in order to avoid invalid access errors at runtime.
 *
 * @param forwardDelegate The delegate to stop forwarding invocations to.
 */
- (void)removeForwarding:(id<UITableViewDelegate>)forwardDelegate;

#pragma mark Object state

/**
 * Returns the accessory type this actions object will apply to a cell for the
 * given object when it is displayed.
 *
 * @param object The object to determine the accessory type for.
 * @returns the accessory type this object's cell will have.
 */
- (UITableViewCellAccessoryType)accessoryTypeForObject:(id)object;

/**
 * Returns the cell selection style this actions object will apply to a cell
 * for the given object when it is displayed.
 *
 * @param object The object to determine the selection style for.
 * @returns the selection style this object's cell will have.
 */
- (UITableViewCellSelectionStyle)selectionStyleForObject:(id)object;

#pragma mark Configurable Properties
/**
 * The cell selection style that will be applied to the cell when it is displayed using
 * delegate forwarding.
 *
 * By default this is UITableViewCellSelectionStyleBlue.
 *
 */
@property (nonatomic, assign) UITableViewCellSelectionStyle tableViewCellSelectionStyle;

@end
