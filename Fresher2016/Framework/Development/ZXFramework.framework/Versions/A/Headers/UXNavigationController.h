/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>


/**
 * UINavigationController subclass that adds completion block support to view controller transitions.
 * This class includes four items of interest:
 
 * 1. The NSObjectProtocol method, 'respondsToSelector' is being used to pass information about more than one object's implementation details.
 * 2. The NSObject method, 'forwardInvocation:', is being used to pass message handling to another object.
 * 3. The delegate accessor has been overwritten for purposes of "misdirection".
 * 4. A private dispatch queue, group, and semaphore are being used to schedule blocks on the execution of methods over which we have no control
 * Sample
 * - (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated completion:(void (^)(UINavigationController *navigationController, UIViewController *viewController))completion;
 */


@interface UXNavigationController : UINavigationController

/**
 * Object conforming to the UINavigationControllerDelegate protocol. This accessor should be used en lieu of the UINavigationController delegate property.
 
 * @Note Both 'setDelegate:' and 'setExternalDelegate:' will set the 'externalDelegate' property.
 * The 'delegate' property will always return the navigation controller.
 */
@property (nonatomic, readwrite, weak) id <UINavigationControllerDelegate> externalDelegate;

/** 
 * Performs a standard push with the added benefit of calling a completion block
 */
- (void)pushViewController:(UIViewController *)viewController
                  animated:(BOOL)animated
                completion:(void (^)(UINavigationController *navigationController, UIViewController *viewController))completion;
@end
