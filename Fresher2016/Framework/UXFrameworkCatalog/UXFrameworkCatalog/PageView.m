/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */


#import "PageView.h"

@implementation PageView

@synthesize pageIndex = _pageIndex;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithFrame:CGRectZero])) {
        _label = [[UILabel alloc] initWithFrame:self.bounds];
        _label.autoresizingMask = UIViewAutoresizingFlexibleDimensions;
        _label.font = [UIFont systemFontOfSize:26];
        _label.textAlignment = NSTextAlignmentCenter;
        _label.backgroundColor = [UIColor clearColor];
        
        [self addSubview:_label];
        
        self.reuseIdentifier = reuseIdentifier;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    return [self initWithReuseIdentifier:nil];
}

- (void)setPageIndex:(NSInteger)pageIndex {
    _pageIndex = pageIndex;
    
    self.label.text = [NSString stringWithFormat:@"This is page %ld", (long)pageIndex];
    
    UIColor* bgColor;
    UIColor* textColor;
    // Change the background and text color depending on the index.
    switch (pageIndex % 4) {
        case 0:
            bgColor = [UIColor redColor];
            textColor = [UIColor whiteColor];
            break;
        case 1:
            bgColor = [UIColor blueColor];
            textColor = [UIColor whiteColor];
            break;
        case 2:
            bgColor = [UIColor yellowColor];
            textColor = [UIColor blackColor];
            break;
        case 3:
            bgColor = [UIColor greenColor];
            textColor = [UIColor blackColor];
            break;
    }
    
    self.backgroundColor = bgColor;
    self.label.textColor = textColor;
    
    [self setNeedsLayout];
}

- (void)prepareForReuse {
    self.label.text = nil;
}

- (void)setReuseIdentifier:(NSString *)reuseIdentifier {
    [super setRestorationIdentifier:reuseIdentifier];
}

@end
