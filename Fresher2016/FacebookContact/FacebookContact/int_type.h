//
//  int_type.h
//  FacebookContact
//
//  Created by BaoNQ on 7/25/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#ifndef int_type_h
#define int_type_h

#ifdef __LP64__
#define __64bit__         1
typedef	long long		INTEGER_T;
#else
#define __64bit__         0
typedef	int             INTEGER_T;
#endif

#endif /* int_type_h */
