/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXStyle.h"

@interface UXBlendStyle : UXStyle {
}

@property (nonatomic) CGBlendMode blendMode;

+ (UXBlendStyle*)styleWithBlend:(CGBlendMode)blendMode next:(UXStyle*)next;

@end
