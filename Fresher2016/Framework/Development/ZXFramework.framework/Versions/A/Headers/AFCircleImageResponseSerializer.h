/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "AFURLResponseSerialization.h"

@interface AFCircleImageResponseSerializer : AFImageResponseSerializer


/**
 Whether to automatically mask response image after decoding image
 */
@property (nonatomic, assign) BOOL automaticallyMaskSmallImage;

/**
 Whether to automatically mask response image after decoding image
 */
@property (nonatomic, assign) BOOL automaticallyMaskLargeImage;

@end
