//
//  CustomCollectionViewCell.h
//  ZaloContactInviting
//
//  Created by Nguyen Van Dao on 5/29/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
