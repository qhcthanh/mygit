/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXTableViewCellFactory.h"

@class UXNetworkImageView;

extern const CGFloat separatorWidth;
extern const CGSize tableImageSize;
extern const CGSize tablePaddingSize;

typedef CGFloat (^UXTableViewCellDrawRectBlock)(CGRect rect, id object, UITableViewCell* cell);

/**
 * An object that will draw the contents of the cell using a provided block.
 *
 */
@interface UXDrawRectBlockCellObject : UXTableViewCellObject
// Designated initializer.
- (id)initWithBlock:(UXTableViewCellDrawRectBlock)block object:(id)object;
+ (id)objectWithBlock:(UXTableViewCellDrawRectBlock)block object:(id)object;
@property (nonatomic, copy) UXTableViewCellDrawRectBlock block;
@property (nonatomic, strong) id object;
@end

/**
 * An object for displaying a single-line title in a table view cell.
 *
 * This object maps by default to a UXTextTableViewCell and displays the title with a
 * UITableViewCellStyleDefault cell. You can customize the cell class using the
 * UXTableViewCellObject methods.
 *
 */
@interface UXTitleTableViewCellObject : UXTableViewCellObject
// Designated initializer.
- (id)initWithTitle:(NSString *)title image:(UIImage *)image imageURL:(NSString*)imageURL;
- (id)initWithTitle:(NSString *)title image:(UIImage *)image;
- (id)initWithTitle:(NSString *)title;

/**
 * Initializes the UXTitleTableViewCellObject with the given title, image, cellClass, and userInfo.
 *
 * This is the designated initializer. Use of this initializer allows for customization of the
 * associated cell class for this object.
 *
 */
- (id)initWithTitle:(NSString *)title image:(UIImage *)image imageURL:(NSString*)imageURL  cellClass:(Class)cellClass userInfo:(id)userInfo;
- (id)initWithTitle:(NSString *)title image:(UIImage *)image cellClass:(Class)cellClass userInfo:(id)userInfo;

+ (id)objectWithTitle:(NSString *)title image:(UIImage *)image imageURL:(NSString*)imageURL;
/**
 * Initializes the UXTitleTableViewCellObject with UXTextTableViewCell as the cell class and the given title text and image.
 *
 * @returns Autoreleased instance of UXTitleTableViewCellObject.
 */
+ (id)objectWithTitle:(NSString *)title image:(UIImage *)image;

/**
 * Initializes the UXTitleTableViewCellObject with UXTextTableViewCell as the cell class and the given title text.
 *
* @returns Autoreleased instance of UXTitleTableViewCellObject.
 */
+ (id)objectWithTitle:(NSString *)title;

/**
 * The text to be displayed in the cell.
 *
 */
@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* imageURL;
@property (nonatomic, strong) UIImage* image;
@end

/**
 * An object for displaying two lines of text in a table view cell.
 *
 * This object maps by default to a UXTextTableViewCell and displays the title with a
 * UITableViewCellStyleSubtitle cell. You can customize the cell class using the
 * UXTableViewCellObject methods.
 *
 */
@interface UXSubtitleTableViewCellObject : UXTitleTableViewCellObject
// Designated initializer.
- (id)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle image:(UIImage *)image  imageURL:(NSString*)imageURL;
- (id)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle image:(UIImage *)image;

/**
 * Initializes the UXTableViewCellObject with UXTextTableViewCell as the cell class and the given title and subtitle
 * text.
 *
 * @returns Autoreleased instance of UXSubtitleTableViewCellObject.
 */
- (id)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle;

/**
 * Initializes the UXSubtitleTableViewCellObject with the given title, subtitle, image, cellClass, and
 * userInfo.
 *
 * This is the designated initializer. Use of this initializer allows for customization of the
 * associated cell class for this object.
 *
 */
- (id)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle image:(UIImage *)image cellClass:(Class)cellClass userInfo:(id)userInfo;
+ (id)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle imageURL:(NSString*)imageURL image:(UIImage *)image cellClass:(Class)cellClass userInfo:(id)userInfo;

+ (id)objectWithTitle:(NSString *)title subtitle:(NSString *)subtitle image:(UIImage *)image  imageURL:(NSString*)imageURL;
+ (id)objectWithTitle:(NSString *)title subtitle:(NSString *)subtitle image:(UIImage *)image;
+ (id)objectWithTitle:(NSString *)title subtitle:(NSString *)subtitle;

/**
 * The text to be displayed in the subtitle portion of the cell.
 *
 */
@property (nonatomic, copy) NSString* subtitle;

/**
 * The type of UITableViewCell to instantiate.
 *
 * By default this is UITableViewCellStyleSubtitle.
 *
 */

@property (nonatomic, assign) UITableViewCellStyle cellStyle;
@end


@interface UXMoreTableViewCellObject : UXTitleTableViewCellObject

@property (nonatomic, assign) BOOL animating;

@end

/**
 * A general-purpose cell for displaying text.
 *
 * When given a UXTitleTableViewCellObject, will set the textLabel's text with the title.
 * When given a UXSubtitleTableViewCellObject, will also set the detailTextLabel's text with the subtitle.
 *
 */
@interface UXTextTableViewCell : UITableViewCell <UXTableViewCellDelegate>

@property (nonatomic, strong) UXTableViewCellObject* cellObject;
@end

/**
 * A cell that renders its contents using a block.
 *
 */
@interface UXDrawRectBlockCell : UITableViewCell <UXTableViewCellDelegate>
@property (nonatomic, strong) UIView* blockView;
@end

@interface UXMoreTableViewCell : UXTextTableViewCell
@end
