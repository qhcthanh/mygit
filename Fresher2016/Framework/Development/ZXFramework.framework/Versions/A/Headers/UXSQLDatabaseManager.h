/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@class UXSQLDatabaseRequest;
@interface UXSQLDatabaseManager : NSObject

+ (UXSQLDatabaseManager*)manager;

/**
 Excute a UXSQLDatabaseRequest with a 'EXECUTE' or 'UPDATE' database request.
 
 @param request The request will be excuted.
 @param success A block object to be executed when the request operation finishes successfully. This block has no return value and takes two arguments: the request operation, and the response object created by the client response serializer.
 @param failure A block object to be executed when the request operation finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data. This block has no return value and takes a two arguments: the request operation and the error describing the network or parsing error that occurred.
 */
- (void)EXECUTE:(UXSQLDatabaseRequest*)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

- (void)UPDATE:(UXSQLDatabaseRequest*)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

/**
 Excute a UXSocketStreamRequest with a 'EXECUTE' or 'UPDATE' database request.
 
 @param request The request will be excuted.
 @param success A block object to be executed when the request operation finishes successfully. This block has no return value and takes two arguments: the request operation, and the response object created by the client response serializer.
 @param failure A block object to be executed when the request operation finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data. This block has no return value and takes a two arguments: the request operation and the error describing the network or parsing error that occurred.
 @param dispatchQueue A dispatch queue for the `completionBlock` of request operations. If `NULL` (default), the main queue is used.
 */
- (void)EXECUTE:(UXSQLDatabaseRequest*)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure completionQueue:(dispatch_queue_t) dispatchQueue;
- (void)UPDATE:(UXSQLDatabaseRequest*)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure completionQueue:(dispatch_queue_t) dispatchQueue;

@end
