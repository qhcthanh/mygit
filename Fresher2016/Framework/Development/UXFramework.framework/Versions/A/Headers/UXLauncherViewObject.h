/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXLauncherViewModel.h"

/**
 * An implementation of the UXLauncherViewObject protocol.
 *
 */
@interface UXLauncherViewObject : NSObject <UXLauncherViewObject, NSCoding>

// Designated initializer.
/**
 * Initializes a newly allocated launcher view object with a given title and image.
 *
 * This is the designated initializer.
 *
 */
- (id)initWithTitle:(NSString *)title image:(UIImage *)image;

/**
 * Allocates and returns an autoreleased instance of a launcher view object.
 *
 */
+ (id)objectWithTitle:(NSString *)title image:(UIImage *)image;

@end





