//
//  String+EmojiSkin.swift
//  EmojiKeyboard
//
//  Created by qhcthanh on 8/15/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import Foundation

extension String {
    var emojiSkinToneModifiers: [String] {
        return [ "🏻", "🏼", "🏽", "🏾", "🏿" ]
    }
    
    var emojiVisibleLength: Int {
        var count = 0
        enumerateSubstringsInRange(startIndex..<endIndex, options: .ByComposedCharacterSequences) {
            _ in count += 1
        }
        return count
    }
    
    var emojiUnmodified: String {
        if self.characters.count == 0 {
            return ""
        }
        
        let range:Range<String.Index> = self.startIndex..<self.startIndex.advancedBy(1)
        return self[range]
    }
    
    var canHaveSkinToneModifier: Bool {
        if self.characters.count == 0 {
            return false
        }
        
        let modified = self.emojiUnmodified + self.emojiSkinToneModifiers[0]
        return modified.emojiVisibleLength == 1
    }
}