//
//  AppDelegate.h
//  CameraGPUImage
//
//  Created by BaoNQ on 6/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

