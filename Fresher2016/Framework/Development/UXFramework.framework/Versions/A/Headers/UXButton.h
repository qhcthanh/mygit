/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXStyleDelegate.h"

@class UXStyle;
@protocol UXStyleDelegate;
@protocol UXImageViewDelegate;

@interface UXButton : UIControl <UXStyleDelegate> {
    NSMutableDictionary*  _content;
    UIFont*               _font;
}

@property (nonatomic, retain) UIFont* font;
@property (nonatomic, assign) BOOL    isVertical;

// This must be set before you call setImage:
@property (nonatomic, assign) id<UXImageViewDelegate> imageDelegate;

+ (UXButton*)buttonWithStyle:(NSString*)selector;
+ (UXButton*)buttonWithStyle:(NSString*)selector title:(NSString*)title;

- (NSString*)titleForState:(UIControlState)state;
- (void)setTitle:(NSString*)title forState:(UIControlState)state;

- (NSString*)imageForState:(UIControlState)state;
- (void)setImage:(NSString*)imageURL forState:(UIControlState)state;

- (UXStyle*)styleForState:(UIControlState)state;
- (void)setStyle:(UXStyle*)style forState:(UIControlState)state;

/**
 * Sets the styles for all control states using a single style selector.
 *
 * The method for the selector must accept a single argument for the control state.  It will
 * be called to return a style for each of the different control states.
 */
- (void)setStylesWithSelector:(NSString*)selector;

- (void)suspendLoadingImages:(BOOL)suspended;

- (CGRect)rectForImage;

@end

