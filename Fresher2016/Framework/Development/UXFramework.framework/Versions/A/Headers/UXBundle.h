/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#if defined __cplusplus
extern "C" {
#endif
    
    /**
     * For creating standard system paths.
     *
     * @ingroup UXFrameworkCore
     * @defgroup Paths Paths
     * @{
     */
    
    /**
     * Create a path with the given bundle and the relative path appended.
     *
     * @param bundle        The bundle to append relativePath to. If nil, [NSBundle mainBundle]
     *                           will be used.
     * @param relativePath  The relative path to append to the bundle's path.
     *
     * @returns The bundle path concatenated with the given relative path.
     */
    NSString* UXPathForBundleResource(NSBundle* bundle, NSString* relativePath);
    
    /**
     * Create a path with the documents directory and the relative path appended.
     *
     * @returns The documents path concatenated with the given relative path.
     */
    NSString* UXPathForDocumentsResource(NSString* relativePath);
    
    /**
     * Create a path with the Library directory and the relative path appended.
     *
     * @returns The Library path concatenated with the given relative path.
     */
    NSString* UXPathForLibraryResource(NSString* relativePath);
    
    /**
     * Create a path with the caches directory and the relative path appended.
     *
     * @returns The caches path concatenated with the given relative path.
     */
    NSString* UXPathForCachesResource(NSString* relativePath);
    
    BOOL UXisBundleURL(NSString* URL);
    BOOL UXisDocumentURL(NSString* URL);
    NSString* doubleImageURLPath(NSString* urlPath);
#if defined __cplusplus
};
#endif

