/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

/**
 * @code
 * error = [NSError errorWithDomain: UXFrameworkErrorDomain
 *                             code: UXImageTooSmall
 *                         userInfo: [NSDictionary dictionaryWithObject: image
 *                                                               forKey: NIImageErrorKey]];
 */
@interface UXError : NSObject

@end
/**
 * For defining various error types used throughout the UXFramework.
 */

/** The framework error domain. */
extern NSString* const UXErrorDomain;

/** The key used for images in the error's userInfo. */
extern NSString* const UXImageErrorKey;

/** NSError codes in UXErrorDomain. */
typedef enum {
    /** The image is too small to be used. */
    UXImageTooSmall = 1,
} UXErrorDomainCode;

