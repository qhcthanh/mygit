//
//  PreviewImageViewController.m
//
//
//  Created by qhcthanh on 6/14/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "PreviewImageViewController.h"
#import "StickerPageViewController.h"
#import "TextEditImageViewController.h"
#import "PickEffectViewController.h"

@interface PreviewImageViewController () <UIGestureRecognizerDelegate, StickerPageControllerDelegate, TextEditControlDelegate, TouchStickerViewDelegate, PickEffectViewControllerDelegate> {
    GPUImagePicture *stillImage;
    GPUImageOutput<GPUImageInput> *filter;
    GPUImageView *gpuImageView;
    __weak IBOutlet UIView *stickerView;
    __weak IBOutlet UIPageControl *stickerPageControl;
    __weak IBOutlet UIView *textEditView;
    TextEditImageViewController* textEditController ;
    __weak IBOutlet UIButton *trashButton;
    __weak IBOutlet UIButton *hideButton;
    __weak IBOutlet NSLayoutConstraint *heightTrashButtonContrainst;
    __weak IBOutlet UIView *effectView;
}
@end

@implementation PreviewImageViewController

-(void)viewDidLoad {
    
    
    //self.imageview.image = _previewImage;
    gpuImageView = [[GPUImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:gpuImageView];
    
    [self.view insertSubview:self.bottomBar aboveSubview:gpuImageView];
    [self.view insertSubview:self.topBar aboveSubview:gpuImageView];
    [self.view insertSubview:stickerView aboveSubview:gpuImageView];
    [self.view insertSubview:textEditView aboveSubview:gpuImageView];
    
    [self.view insertSubview:_saveStateView aboveSubview:gpuImageView];
    [self.view insertSubview:stickerPageControl aboveSubview:stickerView];
    [self.view insertSubview:effectView aboveSubview:gpuImageView];
    
    // Add swipe gsture in camera view
    UISwipeGestureRecognizer* swipeLeftCameraViewGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeFilterGesture:)];
    swipeLeftCameraViewGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    
    UISwipeGestureRecognizer* swipeRightCameraViewGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeFilterGesture:)];
    swipeRightCameraViewGesture.direction = UISwipeGestureRecognizerDirectionRight;
    
    [gpuImageView addGestureRecognizer: swipeRightCameraViewGesture];
    [gpuImageView addGestureRecognizer: swipeLeftCameraViewGesture];
    
    /*
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(50, 50, 50, 50)];
    [backBtn setTitle:@"Back" forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:backBtn];
     */
    
    //[self.view sendSubviewToBack:self.buttomBar];
    
    stillImage = [[GPUImagePicture alloc] initWithImage:self.previewImage];
    filter = [[[self.filterList objectAtIndex:self.indexFilter] alloc] init];

    [stillImage addTarget:filter];
    [filter addTarget:gpuImageView];
    
    [stillImage processImage];
    
    //Sticker setup
    self.gpuStickerView = [[GPUStickerView alloc] initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    [self.gpuStickerView setOpaque:NO];
    [self.view addSubview:self.gpuStickerView];
    
    [self.view bringSubviewToFront:self.gpuStickerView];
    [self.view bringSubviewToFront:self.topBar];
    [self.view bringSubviewToFront:self.bottomBar];
    [self.view bringSubviewToFront:effectView];
    
    self.gpuStickerView.userInteractionEnabled = YES;
    self.gpuStickerView.multipleTouchEnabled = YES;
    self.gpuStickerView.backgroundColor = [UIColor clearColor];
    
    self.gpuStickerView.touchDelegate = self;
}

- (void)viewWillDisappear:(BOOL)animated{
    [stillImage removeTarget:filter];
    [self.gpuStickerView removeAllSticker];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if ( touch.view == _bottomBar || touch.view == _topBar) {
        [self.gpuStickerView touchesBegan:touches withEvent:event];
    }
    //[self.nextResponder touchesBegan:touches withEvent:event];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if ( touch.view == _bottomBar || touch.view == _topBar) {
        [self.gpuStickerView touchesEnded:touches withEvent:event];
    }
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if ( touch.view == _bottomBar || touch.view == _topBar) {
        [self.gpuStickerView touchesMoved:touches withEvent:event];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"%@",segue.identifier);
    if ([segue.identifier isEqualToString: @"StickerPageControllerSegue"]) {
        UIViewController* dest = [segue destinationViewController];
        if ([dest isKindOfClass:[StickerPageViewController class]]) {
            StickerPageViewController* stickPageController = (StickerPageViewController*)dest;
            stickPageController.stickerDelegate = self;
        }
    }
    else if ([segue.identifier isEqualToString: @"TextEditSegue"]) {
        UIViewController* dest = [segue destinationViewController];
        if ([dest isKindOfClass:[TextEditImageViewController class]]) {
            textEditController = (TextEditImageViewController*)dest;
            textEditController.previewVC = self;
            textEditController.textDelegate = self;
        }
    }
    else if ([segue.identifier isEqualToString: @"PickEffectSegue"]) {
        UIViewController* dest = [segue destinationViewController];
        if ([dest isKindOfClass:[PickEffectViewController class]]) {
            PickEffectViewController* effectVC = (PickEffectViewController*)dest;
            effectVC.effectDelegate = self;
            effectVC.filterList = self.filterList;
        }
    }
    
}

-(void)swipeFilterGesture:(UISwipeGestureRecognizer*)swipeGesture {
    if (swipeGesture.direction == UISwipeGestureRecognizerDirectionRight) {
        // swipe right
        if (self.indexFilter == 0)
            return;
        self.indexFilter--;
        NSLog(@"swipe right");
    } else {
        // swipe left
        if (self.indexFilter == self.filterList.count - 1)
            return;
        self.indexFilter++;
        NSLog(@"swipe left");
    }
    [stillImage removeTarget:filter];
    
    filter = [[[self.filterList objectAtIndex:self.indexFilter] alloc] init];
    [filter addTarget:gpuImageView];
    
    [stillImage addTarget:filter];
}

//MARK: Button action
- (IBAction)backAction:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)saveAction:(id)sender {
    [self.view setUserInteractionEnabled:false];
    [stillImage processImageUpToFilter:filter withCompletionHandler:^(UIImage* processImage){
        [self.view setUserInteractionEnabled:true];
        if (processImage) {
            NSMutableArray *stickers = [[NSMutableArray alloc]init];
            for (StickerNode *sticker in [self.gpuStickerView getStickerList])
                [stickers addObject:[sticker getImage]];
            
            UIImage *finalImage = [self mergeSticker:stickers ToImage:processImage];
            
            UIImageWriteToSavedPhotosAlbum(finalImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
            dispatch_async(dispatch_get_main_queue(), ^{
                _saveStateView.hidden = false;
                [self performSelector:@selector(hiddenSaveStateView) withObject:nil afterDelay:1];
            });
        } else {
            
        }
    }];
}

- (UIImage *)mergeSticker:(NSMutableArray *)stickers ToImage:(UIImage *)inputImage{
    //  + 1 Array chứa vị trí và kích thước tương ứng của các stickers so với màn hình điện thoại.
    UIGraphicsBeginImageContext(inputImage.size);
    //Vẽ hình vừa chụp vào context
    [inputImage drawInRect:CGRectMake(0, 0, inputImage.size.width, inputImage.size.height)];
    
    for (NSInteger index = [stickers count] - 1; index >= 0; index --)
    {
        UIImage *inputImage1 = [stickers objectAtIndex:index];
        [inputImage1 drawInRect:CGRectMake(0, 0, inputImage.size.width, inputImage.size.height)];
    }
    
    UIImage* screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return screenshot;
}

- (IBAction)showOverlayAction:(id)sender {
    textEditView.hidden = true;
    effectView.hidden = true;
    [self showEditTextView];
    stickerView.hidden = !stickerView.hidden;
    stickerPageControl.hidden = stickerView.hidden;
    [self showStickerView];
}

- (IBAction)textShowAction:(id)sender {
    stickerView.hidden = true;
     stickerPageControl.hidden = stickerView.hidden;
    effectView.hidden = true;
    self.gpuStickerView.hidden = stickerView.hidden;
    [self showStickerView];
    
    textEditView.hidden = !textEditView.hidden;
    [self showEditTextView];
}

- (IBAction)trashAction:(id)sender {
    
}

- (IBAction)onHideAllSticker:(id)sender {
    stickerView.hidden = true;
    stickerPageControl.hidden = stickerView.hidden;
    textEditView.hidden = true;
    self.topBar.alpha = 0.5;
    self.gpuStickerView.hidden = false;
    self.bottomBar.hidden = false;
    [textEditController endEditText];
    hideButton.hidden = true;
    effectView.hidden = true;
}



-(void)showEditTextView {
    if (textEditView.hidden) {
        self.topBar.alpha = 0.5;
        self.gpuStickerView.hidden = false;
        self.bottomBar.hidden = false;
        [textEditController endEditText];
        hideButton.hidden = true;
        
    } else {
        self.topBar.alpha = 0.8;
        self.bottomBar.hidden = true;
        self.gpuStickerView.hidden = true;
        [textEditController beginEditText];
        hideButton.hidden = false;
    }
}

-(void)showStickerView {
    
    if (stickerView.hidden) {
        hideButton.hidden = true;
        self.topBar.alpha = 0.5;
        self.bottomBar.hidden = false;
        self.gpuStickerView.hidden = false;
    } else {
        self.topBar.alpha = effectView.hidden = true;;
        self.bottomBar.hidden = true;
        self.gpuStickerView.hidden = true;
        hideButton.hidden = false;
    }
}

- (IBAction)showEffectPickerView:(id)sender {
    
    // show
    if (effectView.hidden) {
        stickerView.hidden = true;
        stickerPageControl.hidden = stickerView.hidden;
        textEditView.hidden = true;
        [textEditController endEditText];
        hideButton.hidden = true;
        effectView.hidden = !effectView.hidden;
        self.topBar.alpha = 0.5;
        hideButton.hidden = false;
    } // hide
    else {
        [self onHideAllSticker:nil];
    }
    
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    //Show error alert if image could not be saved
    if (error) [[[UIAlertView alloc] initWithTitle:@"Error!" message:@"Image couldn't be saved" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    else {
        _saveStateView.hidden = false;
        [self performSelector:@selector(hiddenSaveStateView) withObject:nil afterDelay:2];
    }
}

-(void)hiddenSaveStateView {
    _saveStateView.hidden = true;
}


//MARK: StickerPageViewController Delegate
-(void)updateIndexViewController:(NSInteger)index {
    stickerPageControl.currentPage = index;
}

-(void)numberOfViewController:(NSInteger)numberPages {
    stickerPageControl.numberOfPages = numberPages;
}

-(void)didSelectItem:(id)item position:(CGPoint)touchPosition {
    if ([item isKindOfClass:[UIImage class]]) {
        [self showOverlayAction:nil];
        UIImage* image = (UIImage*)item;
        StickerNode *newSticker = [[StickerNode alloc] initWithFrame:self.gpuStickerView.frame];
        [newSticker addStickerWithImage:image];
        [self.gpuStickerView addSticker:newSticker];
        [newSticker render];
    }
}

//MARK: Edit Text Delegate

-(void)didFinishEditText:(TextStickerNode *)newSticker {
    [self.gpuStickerView addSticker:newSticker];
    [newSticker render];
}

//MARK: Touch StickView
-(void)didTouchUpInsideTextSticker:(TextStickerNode *)textSticker {
    //NSLog(@"%@",textSticker.content);
    textEditController.currentTextSticker = textSticker;
    textEditController.textEditImageView.text = textSticker.content;
    textEditController.textEditImageView.textColor = textSticker.color;
    [self textShowAction:nil];
}

-(void)didTouchEndSticker:(id)sticker position:(CGPoint)position {
    bool inRect = CGRectContainsPoint(trashButton.frame, position);
    if( inRect ) {
        if ([sticker isKindOfClass:[StickerNode class]]) {
            StickerNode* stickerT = (StickerNode*)sticker;
            [self.gpuStickerView removeSticker:stickerT];
        }
    }
    heightTrashButtonContrainst.constant = 0;
}

-(void)didTouchMoveEndSticker:(id)sticker position:(CGPoint)position {
    bool inRect = CGRectContainsPoint(trashButton.frame, position);
    if( inRect ) {
        heightTrashButtonContrainst.constant = 15;
        return;
    }
    heightTrashButtonContrainst.constant = 0;
}

//MARK: PickEffectViewControllerDelegate
-(void)didSelectFilter:(GPUImageOutput<GPUImageInput> *)filterT {
    [stillImage removeTarget:filter];
    
    filter = filterT;
    [filter addTarget:gpuImageView];
    
    [stillImage addTarget:filter];
}
@end
