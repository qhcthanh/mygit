//
//  CustomMutableArray.m
//  Task1_GCD
//
//  Created by VanDao on 5/31/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import "STMutableArray.h"

@interface STMutableArray(){
    NSMutableArray *internalMutableArray;
    dispatch_queue_t concurrentQueue;
}
@end

@implementation STMutableArray

- (id)init{
    self = [super init];
    if (self){
        internalMutableArray = [[NSMutableArray alloc]init];
        concurrentQueue = dispatch_queue_create("Concurrent Queue CustomMutableArray", DISPATCH_QUEUE_CONCURRENT);
    }
    
    return self;
}

- (void)insertObject:(id) anObject
             atIndex:(NSUInteger)index{
    dispatch_barrier_async(concurrentQueue, ^{
        if (index >= [internalMutableArray count])
            return;
        [internalMutableArray insertObject:anObject atIndex:index];
    });
}

- (void)removeObjectAtIndex:(NSUInteger)index{
    dispatch_barrier_async(concurrentQueue, ^{
        if (index >= [internalMutableArray count])
            return;
        [internalMutableArray removeObjectAtIndex:index];
    });
}

- (void)addObject:(id)anObject{
    dispatch_barrier_async(concurrentQueue, ^{
        [internalMutableArray addObject:anObject];
    });
}

-(NSUInteger)count {
    __block NSUInteger count;
    dispatch_sync(concurrentQueue, ^{
        count = [internalMutableArray count];
    });
    return count;
}

- (id)objectAtIndex:(NSUInteger)index {
    __block id obj;
    dispatch_sync(concurrentQueue, ^{
        if (index >= [internalMutableArray count])
            return;
        obj = [internalMutableArray objectAtIndex:index];
    });
    return obj;
}

- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject {
    dispatch_barrier_async(concurrentQueue, ^{
        if (index >= [internalMutableArray count])
            return;
        [internalMutableArray replaceObjectAtIndex:index withObject:anObject];
    });
}

- (void)removeAllObjects{
    dispatch_barrier_async(concurrentQueue, ^{
        if ([internalMutableArray count] == 0)
            return;
        [internalMutableArray removeAllObjects];
    });
}

@end
