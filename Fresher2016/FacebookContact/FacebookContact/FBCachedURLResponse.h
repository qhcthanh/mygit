//
//  FBCachedURLResponse.h
//  FacebookContact
//
//  Created by VanDao on 7/25/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#define kTimeToLive 1 * 24 * 60 * 60 //1 day

@interface FBCachedURLResponse : NSCachedURLResponse

@property (readonly, nonatomic) NSDate *lastFocusDate;

/**
 *  Update lastFocusDate with current date
 */
- (void)updateFocusDate;

/**
 *  Check if lastFocusDate is out of date
 *
 *  @return YES if FBCacheURLResponse is out of date, otherwise, return NO.
 */
- (BOOL)isResponseOutOfDate;

/**
 *  Check if lastForcusDate is out of date
 *
 *  @param date The expired time of FBCacheURLResponse
 *
 *  @return YES if FBCacheURLResponse is out of date, otherwise, return NO
 */
- (BOOL)isResponseOutOfDateWithExpiredDate:(NSDate *)date;

@end
