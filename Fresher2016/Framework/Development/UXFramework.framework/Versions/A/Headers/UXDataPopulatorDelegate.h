/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@protocol UXDataPopulatorDelegate
@optional

/**
 * When the Data Populator can't automatically convert some specific type. He will call this method
 * and let you extend the class converting you specific type.
 * @param firstObject is original object that we need to convert.
 * @param firstObjectClass is the class of the original object.
 * @param convertToClass is the class that we need to receive whe converted.
 * @return Should return converted object or <tt>nil</tt> if can't convert.
 */
-(id)tryToConvert:(id)object ofClass:(Class)objectClass toClass:(Class)convertToClass;

@end
