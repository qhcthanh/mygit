/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "StyledViewController.h"

#import <UXFramework/UXFramework.h>

#import "StyledScrollView.h"

@interface StyledViewController () {
    StyledScrollView* scrollView;
}

@end

@implementation StyledViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.titleString = @"Styled";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    scrollView = [[StyledScrollView alloc] initWithFrame:self.view.bounds];
    scrollView.autoresizesSubviews = YES;
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    scrollView.backgroundColor = RGBCOLOR(216, 221, 231);
    self.view = scrollView;
        
    UIColor* black = RGBCOLOR(158, 163, 172);
    UIColor* blue = RGBCOLOR(191, 197, 208);
    UIColor* darkBlue = RGBCOLOR(109, 132, 162);
    
    NSArray* styles = [NSArray arrayWithObjects:
                       // Rectangle
                       [UXSolidFillStyle styleWithColor:[UIColor whiteColor] next:
                        [UXSolidBorderStyle styleWithColor:black width:1 next:nil]],
                       
                       // Rounded rectangle
                       [UXShapeStyle styleWithShape:[UXRoundedRectangleShape shapeWithRadius:10] next:
                        [UXSolidFillStyle styleWithColor:[UIColor whiteColor] next:
                         [UXSolidBorderStyle styleWithColor:black width:1 next:nil]]],
                       
                       // Gradient border
                       [UXShapeStyle styleWithShape:[UXRoundedRectangleShape shapeWithRadius:10] next:
                        [UXSolidFillStyle styleWithColor:[UIColor whiteColor] next:
                         [UXLinearGradientBorderStyle styleWithColor1:RGBCOLOR(0, 0, 0)
                                                               color2:RGBCOLOR(216, 221, 231) width:2 next:nil]]],
                       
                       // Rounded left arrow
                       [UXShapeStyle styleWithShape:[UXRoundedLeftArrowShape shapeWithRadius:5] next:
                        [UXSolidFillStyle styleWithColor:[UIColor whiteColor] next:
                         [UXSolidBorderStyle styleWithColor:black width:1 next:nil]]],
                       
                       // Partially rounded rectangle
                       [UXShapeStyle styleWithShape:
                        [UXRoundedRectangleShape shapeWithTopLeft:0 topRight:0 bottomRight:10 bottomLeft:10] next:
                        [UXSolidFillStyle styleWithColor:[UIColor whiteColor] next:
                         [UXSolidBorderStyle styleWithColor:black width:1 next:nil]]],
                       
                       // SpeechBubble with pointer left of the centre on the top edge
                       // Locations for top edge are 45 on the left, 90 in the centre, 134.999 on the right
                       [UXShapeStyle styleWithShape:[UXSpeechBubbleShape shapeWithRadius:5
                                                                           pointLocation:60
                                                                              pointAngle:90
                                                                               pointSize:CGSizeMake(20,10)] next:
                        [UXSolidFillStyle styleWithColor:[UIColor whiteColor] next:
                         [UXSolidBorderStyle styleWithColor:black width:1 next:nil]]],
                       
//                       // SpeechBubble with pointer on the extreme left on the bottom edge
//                       // Locations for bottom edge are 225 on the left, 270 in the centre, 314.999 on the left
//                       [UXShapeStyle styleWithShape:[UXSpeechBubbleShape shapeWithRadius:5
//                                                                           pointLocation:314
//                                                                              pointAngle:270
//                                                                               pointSize:CGSizeMake(20,10)] next:
//                        [UXSolidFillStyle styleWithColor:[UIColor whiteColor] next:
//                         [UXSolidBorderStyle styleWithColor:black width:1 next:nil]]],
//                       
//                       // SpeechBubble with pointer on the bottom of the left edge
//                       // Locations for left edge are 315 on the bottom, 0 in the centre, 44.999 on top
//                       [UXShapeStyle styleWithShape:[UXSpeechBubbleShape shapeWithRadius:5
//                                                                           pointLocation:315
//                                                                              pointAngle:0
//                                                                               pointSize:CGSizeMake(10,20)] next:
//                        [UXSolidFillStyle styleWithColor:[UIColor whiteColor] next:
//                         [UXSolidBorderStyle styleWithColor:black width:1 next:nil]]],
                       
                       // SpeechBubble with pointer on the centre of the left edge
                       // Locations for left edge are 315 on the bottom, 0 in the centre, 44.999 on top
                       [UXShapeStyle styleWithShape:[UXSpeechBubbleShape shapeWithRadius:5 pointLocation:0
                                                                              pointAngle:0
                                                                               pointSize:CGSizeMake(20,10)] next:
                        [UXSolidFillStyle styleWithColor:[UIColor whiteColor] next:
                         [UXSolidBorderStyle styleWithColor:black width:1 next:nil]]],
                       
                       // SpeechBubble with pointer on the bottom of the right hand edge
                       // Locations for right edge are 135 on top, 180 in the middle, 314.999 on the bottom
                       [UXShapeStyle styleWithShape:[UXSpeechBubbleShape shapeWithRadius:5 pointLocation:224
                                                                              pointAngle:180
                                                                               pointSize:CGSizeMake(15,15)] next:
                        [UXSolidFillStyle styleWithColor:[UIColor whiteColor] next:
                         [UXSolidBorderStyle styleWithColor:black width:1 next:nil]]],
                       
                       // Drop shadow
                       [UXShapeStyle styleWithShape:[UXRoundedRectangleShape shapeWithRadius:10] next:
                        [UXShadowStyle styleWithColor:RGBACOLOR(0,0,0,0.5) blur:5 offset:CGSizeMake(2, 2) next:
                         [UXInsetStyle styleWithInset:UIEdgeInsetsMake(0.25, 0.25, 0.25, 0.25) next:
                          [UXSolidFillStyle styleWithColor:[UIColor whiteColor] next:
                           [UXInsetStyle styleWithInset:UIEdgeInsetsMake(-0.25, -0.25, -0.25, -0.25) next:
                            [UXSolidBorderStyle styleWithColor:black width:1 next:nil]]]]]],
                       
                       // Inner shadow
                       [UXShapeStyle styleWithShape:[UXRoundedRectangleShape shapeWithRadius:10] next:
                        [UXSolidFillStyle styleWithColor:[UIColor whiteColor] next:
                         [UXInnerShadowStyle styleWithColor:RGBACOLOR(0,0,0,0.5) blur:6 offset:CGSizeMake(1, 1) next:
                          [UXSolidBorderStyle styleWithColor:black width:1 next:nil]]]],
                       
                       // Chiseled button
                       [UXShapeStyle styleWithShape:[UXRoundedRectangleShape shapeWithRadius:10] next:
                        [UXShadowStyle styleWithColor:RGBACOLOR(255,255,255,0.9) blur:1 offset:CGSizeMake(0, 1) next:
                         [UXLinearGradientFillStyle styleWithColor1:RGBCOLOR(255, 255, 255)
                                                             color2:RGBCOLOR(216, 221, 231) next:
                          [UXSolidBorderStyle styleWithColor:blue width:1 next:nil]]]],
                       
                       // Embossed button
                       [UXShapeStyle styleWithShape:[UXRoundedRectangleShape shapeWithRadius:10] next:
                        [UXLinearGradientFillStyle styleWithColor1:RGBCOLOR(255, 255, 255)
                                                            color2:RGBCOLOR(216, 221, 231) next:
                         [UXFourBorderStyle styleWithTop:blue right:black bottom:black left:blue width:1 next:nil]]],
                       
                       // Toolbar button
                       [UXShapeStyle styleWithShape:[UXRoundedRectangleShape shapeWithRadius:4.5] next:
                        [UXShadowStyle styleWithColor:RGBCOLOR(255,255,255) blur:1 offset:CGSizeMake(0, 1) next:
                         [UXReflectiveFillStyle styleWithColor:darkBlue next:
                          [UXBevelBorderStyle styleWithHighlight:[darkBlue shadow]
                                                          shadow:[darkBlue multiplyHue:1 saturation:0.5 value:0.5]
                                                           width:1 lightSource:270 next:
                           [UXInsetStyle styleWithInset:UIEdgeInsetsMake(0, -1, 0, -1) next:
                            [UXBevelBorderStyle styleWithHighlight:nil shadow:RGBACOLOR(0,0,0,0.15)
                                                             width:1 lightSource:270 next:nil]]]]]],
                       
                       // Back button
                       [UXShapeStyle styleWithShape:[UXRoundedLeftArrowShape shapeWithRadius:4.5] next:
                        [UXShadowStyle styleWithColor:RGBCOLOR(255,255,255) blur:1 offset:CGSizeMake(0, 1) next:
                         [UXReflectiveFillStyle styleWithColor:darkBlue next:
                          [UXBevelBorderStyle styleWithHighlight:[darkBlue shadow]
                                                          shadow:[darkBlue multiplyHue:1 saturation:0.5 value:0.5]
                                                           width:1 lightSource:270 next:
                           [UXInsetStyle styleWithInset:UIEdgeInsetsMake(0, -1, 0, -1) next:
                            [UXBevelBorderStyle styleWithHighlight:nil shadow:RGBACOLOR(0,0,0,0.15)
                                                             width:1 lightSource:270 next:nil]]]]]],
                       
                       // Badge
                       [UXShapeStyle styleWithShape:[UXRoundedRectangleShape shapeWithRadius:4.5] next:
                        [UXInsetStyle styleWithInset:UIEdgeInsetsMake(1.5, 1.5, 1.5, 1.5) next:
                         [UXShadowStyle styleWithColor:RGBACOLOR(0,0,0,0.8) blur:3 offset:CGSizeMake(0, 5) next:
                          [UXReflectiveFillStyle styleWithColor:[UIColor redColor] next:
                           [UXInsetStyle styleWithInset:UIEdgeInsetsMake(-1.5, -1.5, -1.5, -1.5) next:
                            [UXSolidBorderStyle styleWithColor:[UIColor whiteColor] width:3 next:nil]]]]]],
                       
                       // Mask
//                       [UXMaskStyle styleWithMask:AFIMAGE(@"bundle://mask.png") next:
//                        [UXLinearGradientFillStyle styleWithColor1:RGBCOLOR(0, 180, 231)
//                                                            color2:RGBCOLOR(0, 0, 255) next:nil]],
                       
                       // simple bottom only border
                       [UXShapeStyle styleWithShape:[UXRectangleShape shape] next:
                        [UXSolidFillStyle styleWithColor:RGBCOLOR(255, 255, 255) next:
                         [UXFourBorderStyle styleWithTop:nil right:nil bottom:black left:nil width:5 next:nil]]],
                       
                       nil];
    
    CGFloat padding = 10.0f;
    CGFloat viewWidth = scrollView.frame.size.width/2 - padding*2;
    CGFloat viewHeight = 44;
    
    CGFloat x = padding;
    CGFloat y = padding;
    for (UXStyle* style in styles) {
        if (x + viewWidth >= scrollView.frame.size.width) {
            x = padding;
            y += viewHeight + padding;
        }
        
        CGRect frame = CGRectMake(x, y, viewWidth, viewHeight);
        UXView* view = [[UXView alloc] initWithFrame:frame];
        view.backgroundColor = scrollView.backgroundColor;
        view.style = style;
        [scrollView addSubview:view];
        
        x += frame.size.width + padding;
    }
    
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, y + viewHeight + padding);
    
    styles = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    scrollView = nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
