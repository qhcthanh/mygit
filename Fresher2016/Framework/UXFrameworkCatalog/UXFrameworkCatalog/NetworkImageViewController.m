/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "NetworkImageViewController.h"

#import <UXFramework/UXFramework.h>


@interface NetworkImageViewController ()
@property (nonatomic, copy) NSArray* networkImageViews;
@end

@implementation NetworkImageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        self.titleString = @"UXImageView";
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // iOS 7-only.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    // A UXNetworkImageView is a subclass of UIImageView. We can provide an image to the initializer
    // and it will be displayed until the network image is loaded. In this example we won't set an
    // initial image.
    AFImageView* imageView = [[AFImageView alloc] initWithFrame:CGRectMake(20, 20, 200, 200)];
    imageView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    imageView.afImageCacheType = kAFImageCacheTypeThumbnail;
    imageView.backgroundColor = [UIColor greenColor];
    
    // We can set the path to this image view's image on the network now that the various presentation
    // options have been set. As discussed above, the network image view will use the contentMode and
    // bounds to crop and/or resize the downloaded image to fit the dimensions perfectly.
    [imageView setImageWithURL:[NSURL URLWithString:@"http://farm5.staticflickr.com/4016/4441744445_97cfbf4519_b_d.jpg"]];
    
    [self.view addSubview:imageView];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // The views are no longer needed.
    self.networkImageViews = nil;
    
    // Reset the maximum number of concurrent operations.
    //[[UXFramework networkOperationQueue] setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
