//
//  BLKeyLayout.swift
//  BanLaKey
//
//  Created by BaoNQ on 8/26/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

class BLKeyLayout: NSObject {
    
    var keyboardMetrics: KeyboardMetrics!
    
    let edgeMargin: CGFloat = 5.0
    
    let bottomMargin: CGFloat = 5.0
    
    var keyboardWidth: CGFloat = 0
    
    var keyboardHeight: CGFloat = 0
    
    init(keyboardWidth: CGFloat, keyboardHeight: CGFloat) {
        
        super.init()
        
        self.keyboardWidth = keyboardWidth
        self.keyboardHeight = keyboardHeight
        
        #if isIPad
            self.keyboardMetrics = generatePadKeyboardMetrics(keyboardWidth, keyboardHeight)
        #else
            self.keyboardMetrics = generatePhoneKeyboardMetrics(keyboardWidth, keyboardHeight)
        #endif
    }
    
    func updateKeyboardMetrics(keyboardWidth: CGFloat, keyboardHeight: CGFloat) {
        
        self.keyboardWidth = keyboardWidth
        self.keyboardHeight = keyboardHeight
        
        #if isIPad
            self.keyboardMetrics = generatePadKeyboardMetrics(keyboardWidth, keyboardHeight)
        #else
            self.keyboardMetrics = generatePhoneKeyboardMetrics(keyboardWidth, keyboardHeight)
        #endif
    }
    
    var popupFrame: CGRect {
        get {
            return CGRectMake((self.keyboardMetrics!.letterKeyWidth - self.keyboardMetrics!.popupWidth)/2.0,
                              -self.keyboardMetrics!.popupHeight - self.keyboardMetrics!.popupConnectorHeight,
                              self.keyboardMetrics!.popupWidth,
                              self.keyboardMetrics!.popupHeight)
        }
    }
    
    var keyModeButton: CGRect {
        get {
            return CGRectMake(self.edgeMargin,
                              self.keyboardHeight - self.bottomMargin - self.keyboardMetrics!.keyHeight,
                              self.keyboardMetrics!.nextKeyboardButtonWidth,
                              self.keyboardMetrics!.keyHeight)
        }
    }
    
    var returnButton: CGRect {
        get {
            return CGRectMake(self.keyboardWidth - self.edgeMargin - self.keyboardMetrics!.returnButtonWidth,
                              self.keyboardHeight - self.bottomMargin - self.keyboardMetrics!.keyHeight,
                              self.keyboardMetrics!.returnButtonWidth,
                              self.keyboardMetrics!.keyHeight)
        }
    }
    
    var spaceButton: CGRect {
        get {
            return CGRectMake(self.edgeMargin + 2 * (self.keyboardMetrics!.nextKeyboardButtonWidth + self.keyboardMetrics!.columnMargin),
                              self.keyboardHeight - self.bottomMargin - self.keyboardMetrics!.keyHeight,
                              self.keyboardWidth - (self.edgeMargin + 2 * (self.keyboardMetrics!.nextKeyboardButtonWidth + self.keyboardMetrics!.columnMargin) + self.keyboardMetrics!.columnMargin + self.keyboardMetrics!.returnButtonWidth + self.edgeMargin),
                              self.keyboardMetrics!.keyHeight)
        }
    }
    
    var deleteButton: CGRect {
        get {
            return CGRectMake(self.keyboardWidth - self.edgeMargin - self.keyboardMetrics!.deleteButtonWidth,
                              self.keyboardHeight - (self.bottomMargin + self.keyboardMetrics!.keyHeight + self.keyboardMetrics!.rowMargin + self.keyboardMetrics!.keyHeight),
                              self.keyboardMetrics!.deleteButtonWidth,
                              self.keyboardMetrics!.keyHeight)
        }
    }
    
    var leftShiftButton: CGRect {
        get {
            return CGRectMake(self.edgeMargin,
                              self.keyboardHeight - (self.bottomMargin + self.keyboardMetrics!.keyHeight + self.keyboardMetrics!.rowMargin + self.keyboardMetrics!.keyHeight),
                              self.keyboardMetrics!.shiftButtonWidth,
                              self.keyboardMetrics!.keyHeight)
        }
    }
    
    var globeButton: CGRect {
        get {
            return CGRectMake(self.edgeMargin + self.keyboardMetrics!.nextKeyboardButtonWidth + self.keyboardMetrics!.columnMargin,
                              self.keyboardHeight - self.bottomMargin - self.keyboardMetrics!.keyHeight,
                              self.keyboardMetrics!.nextKeyboardButtonWidth,
                              self.keyboardMetrics!.keyHeight)
        }
    }
    
    // handles the layout for the keyboard, including key spacing and arrangement
    func layoutKeyboard(keyboard: BLKeyboard, currentPage: Int) {
        
        var rowIndex: Int = 0
        var keyIndex: Int = 0
        var pageIndex: Int = 0
        
        // Update frame for special keys
        keyboard.returnButton?.frame = self.returnButton
        keyboard.deleteButton?.frame = self.deleteButton
        keyboard.globeButton?.frame = self.globeButton
        keyboard.spaceButton?.frame = self.spaceButton
        keyboard.keyModeButton?.frame = self.keyModeButton
        keyboard.shiftButton?.frame = self.leftShiftButton
        
        for page in keyboard.pages {
            
            let numberOfRows = page.rows.count
            
            rowIndex = 0
            for row in page.rows {
                
                var startingPosition: CGFloat = 0
                
                if row.count % 2 == 0 {
                    startingPosition = (keyboardWidth + keyboardMetrics.columnMargin)/2
                }
                else {
                    startingPosition = (keyboardWidth - keyboardMetrics.letterKeyWidth)/2
                }
                
                let keyHorizontalSpacing = (keyboardMetrics.letterKeyWidth + keyboardMetrics.columnMargin)
                let keyVerticalSpacing = keyboardMetrics.rowMargin + keyboardMetrics.keyHeight
                
                startingPosition -= CGFloat(Int(row.count/2)) * keyHorizontalSpacing
                
                keyIndex = 0
                for key in row {
                    
                    // Update key frame
                    key.frame = CGRectMake(startingPosition + CGFloat(keyIndex) * keyHorizontalSpacing,
                                           (keyboardHeight - bottomMargin - keyboardMetrics.keyHeight + CGFloat(rowIndex - numberOfRows) * keyVerticalSpacing),
                                           keyboardMetrics.letterKeyWidth,
                                           keyboardMetrics.keyHeight)
                    
                    // Update popup frame
                    key.updatePopupFrame(self.popupFrame)
                    
                    if (pageIndex == currentPage) {
                        key.hidden = false
                    }
                    else {
                        key.hidden = true
                    }
                    
                    keyIndex += 1
                }
                
                rowIndex += 1
            }
            pageIndex += 1
        }
    }
}
