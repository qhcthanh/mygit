//
//  ContactProtocol.h
//  w1UITableView
//
//  Created by manhduydl on 7/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ContactProtocol <NSObject>

@property (nonatomic, copy) NSString *fullName;
@property (nonatomic, copy) UIImage *avatarImage;

@end
