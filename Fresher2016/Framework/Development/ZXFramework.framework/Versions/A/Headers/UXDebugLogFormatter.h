/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXLogFormatter.h"

@interface UXDebugLogFormatter : NSObject <UXLogFormatter> {
    NSDateFormatter *dateFormatter;
}

@end
