/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXStyle.h"

@interface UXSolidBorderStyle : UXStyle {
}

@property (nonatomic, retain) UIColor*  color;
@property (nonatomic)         CGFloat   width;

+ (UXSolidBorderStyle*)styleWithColor:(UIColor*)color width:(CGFloat)width next:(UXStyle*)next;


@end
