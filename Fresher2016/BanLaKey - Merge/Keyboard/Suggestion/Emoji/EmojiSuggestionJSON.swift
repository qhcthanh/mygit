//
//  EmojiSuggestionJSON.swift
//  EmotionDetect
//
//  Created by VanDao on 8/29/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

import Foundation

// MARK: - HEADER
// MARK: -

// MARK: Emoji content
// Emoji content
enum EmojiContent: String {
    case fun = "fun"
    case sadness = "sadness"
    case hatred = "hatred"
    case lovely = "lovely"
    case party = "party"
    case music = "music"
    case sport = "sport"
    case study = "study"
    case relax = "relax"
    case denial = "denial"
    case goodbye = "goodbye"
    case question = "question"
    case normal = "normal"
    case none = "none"
}

let emojiContents : [EmojiContent] = [EmojiContent.fun, EmojiContent.sadness, EmojiContent.hatred, EmojiContent.lovely, EmojiContent.party, EmojiContent.music, EmojiContent.sport, EmojiContent.study, EmojiContent.relax, EmojiContent.denial, EmojiContent.goodbye, EmojiContent.normal, EmojiContent.none]

// MARK: Word description
// Word description

enum WordDescription: String {
    case emotion = "emotion"
    case negative = "negative"
    case dismiss = "dissmiss"
    case interrogative = "interrogativeWord"
    case none = "none"
}

let wordDescription: [WordDescription]! = [WordDescription.emotion, WordDescription.negative, WordDescription.dismiss, WordDescription.none]

// MARK: - BODY
// MARK: -

// MARK: Emoji descriptions
// MARK: Emoji descriptions
let emojiList: String! = "List"
let wordCategories:  String! = "Words"
let emojiDescriptions: [EmojiContent : [String : [String]]]! = [
    EmojiContent.fun : [
        wordCategories : ["vui", "hehe", "haha", "hihi", "sướng", "buồn cười", "cười"],
        emojiList : ["😊","🙂","😄","😁"]
    ],
    EmojiContent.sadness : [
        wordCategories : ["buồn", "chán", "huhu", "hichic", "nản", "bùn", "haizz"],
        emojiList : ["😔","😢","😞","😭"]
    ],
    EmojiContent.hatred : [
        wordCategories : ["ghét", "cút", "gừ", "giận", "tức", "gét", "ghét"],
        emojiList : ["😒","😑","😔","😡"]
    ],
    EmojiContent.lovely :[
        wordCategories : ["yêu", "iu", "tình yêu", "dễ thương", "thương", "thích", "chụt chụt", "người yêu", "người iu", "love"],
        emojiList : ["😍","💕","❤️","😘"]
    ],
    EmojiContent.party : [
        wordCategories : ["chơi", "nhậu", "quẩy", "bung lụa", "chai bia", "uống bia", "két bia", "rượu", "bia"],
        emojiList : ["🎉","🍻","👌","👍"]
    ],
    EmojiContent.music : [
        wordCategories : ["hát", "karaoke", "hát karaoke", "ca nhạc", "ca sĩ", "bài hát", "văn nghệ"],
        emojiList : ["🎧","🎶","🎵","🎼"]
    ],
    EmojiContent.sport : [
        wordCategories : ["đá banh", "thể thao", "thể dục", "chơi game", "liên minh", "dota", "quán net", "tiệm net"],
        emojiList : ["💪","🎮","🏋","⚽"]
    ],
    EmojiContent.study : [
        wordCategories : ["học", "tới lớp", "ôn bài", "bài tập", "bài tập", "soạn bài", "học bài", "ôn thi", "ôn tập", "thi", "thi cử", "đi thi"],
        emojiList : ["📚","📖","📕","📝"]
    ],
    EmojiContent.relax : [
        wordCategories : ["nghĩ ngơi", "giải lao", "thư giãn", "nằm nghĩ", "nghỉ mệt"],
        emojiList : ["😴","🛀","😌","💆"]
    ],
    EmojiContent.denial : [
        wordCategories : ["từ chối", "không được", "không đi", "éo", "dẹp", "hog đi", "hog thể", "hog làm", "hog được", "không thể", "không làm"],
        emojiList : ["🙅", "😵", "😔","😒"]
    ],
    EmojiContent.goodbye : [
        wordCategories : ["tạm biệt", "bye", "pipi", "biến", "pp", "bb"],
        emojiList : ["👋","🙋","😢","😙"]
    ],
    EmojiContent.question : [
        wordCategories : [],
        emojiList : ["🤔"]
    ],
    EmojiContent.normal : [
        wordCategories : [],
        emojiList : ["😐","😶", "😏"]
    ],
    EmojiContent.none : [
        wordCategories : [],
        emojiList : []
    ]
]

// MARK: Negative words
let negativeWords: [String]! = ["không", "ko", "hông", "khó", "hog", "k", "hong"]

// MARK: Interrogative words
let interrogativeWords: [String]! = ["hả", "à", "chưa", "sao"]

// MARK: Dismiss words
let dismissWords: [String]! = ["nhưng", "tuy nhiên", "bởi vì"]

// MARK: Revert emoji content table
let revertEmojiContentTable: [EmojiContent: [EmojiContent]]! = [
    EmojiContent.fun : [EmojiContent.sadness],
    EmojiContent.sadness : [EmojiContent.fun],
    EmojiContent.hatred : [EmojiContent.fun],
    EmojiContent.lovely : [EmojiContent.denial],
    EmojiContent.party : [EmojiContent.denial],
    EmojiContent.sport : [EmojiContent.denial],
    EmojiContent.study : [EmojiContent.denial],
    EmojiContent.relax : [EmojiContent.denial],
    EmojiContent.denial : [EmojiContent.denial],
    EmojiContent.goodbye : [EmojiContent.sadness],
    EmojiContent.question : [EmojiContent.question],
    EmojiContent.none : [EmojiContent.none],
    EmojiContent.normal : [EmojiContent.hatred],
]
