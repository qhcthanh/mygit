//
//  MDFrameDrawer.h
//  CaptureVideoOverlay
//
//  Created by manhduydl on 7/30/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "GPUImage.h"

@interface MDFrameDrawer : GPUImageOutput
{
    GPUPixelFormat pixelFormat;
    GPUPixelType   pixelType;
    CGSize uploadedImageSize;
    BOOL hasProcessedData;
    
    dispatch_semaphore_t dataUpdateSemaphore;
}

@property (nonatomic, copy) BOOL (^contextUpdateBlock)(CGContextRef context, CGSize size, CMTime time);

// initialization and teardown
-(id) initWithSize:(CGSize) size;
-(id) initWithSize:(CGSize) size contextInitailizeBlock:(void(^)(CGContextRef context, CGSize size)) contextInitializeBlock;

// data processing
-(void) processData;
-(CGSize) outputImageSize;

@end