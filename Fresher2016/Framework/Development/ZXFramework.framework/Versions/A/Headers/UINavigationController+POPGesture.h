/* Copyright (c) 2015-Zalo, VNG Corp.
 * Copyright (c) 2015-2016 FD Inc.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

/**
 *  This class extends UINavigationController's swipe-to-pop behavior in iOS 7+ by supporting fullscreen pan gesture. 
 *  Instead of screen edge, you can now swipe from any place on the screen and the onboard interactive pop transition works seamlessly. 
 *  Adding the implementation file of this category to your target will automatically patch UINavigationController with this feature.
 */
@interface UINavigationController (POPGesture)

// The gesture recognizer that actually handles interactive pop.
@property (nonatomic, strong, readonly) UIPanGestureRecognizer *ux_fullscreenPopGestureRecognizer;

// A view controller is able to control navigation bar's appearance by itself, rather than a global way, checking "ux_prefersNavigationBarHidden" property. Default to YES, disable it if you don't want so.
@property (nonatomic, assign) BOOL ux_viewControllerBasedNavigationBarAppearanceEnabled;


@end

/**
 *  Allows any view controller to disable interactive pop gesture, which might be necessary when the view controller itself handles pan gesture in some cases.
 */
@interface UIViewController (POPGesture)

// Whether the interactive pop gesture is disabled when contained in a navigation stack.
@property (nonatomic, assign) BOOL ux_interactivePopDisabled;

// Indicate this view controller prefers its navigation bar hidden or not, checked when view controller based navigation bar's appearance is enabled. Default to NO, bars are more likely to show.
@property (nonatomic, assign) BOOL ux_prefersNavigationBarHidden;

@end
