/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXStyle.h"
#import "UXStyleDefines.h"

@interface UXBoxStyle : UXStyle {
}

@property (nonatomic) UIEdgeInsets margin;
@property (nonatomic) UIEdgeInsets padding;
@property (nonatomic) CGSize minSize;
@property (nonatomic) UXPosition position;

+ (UXBoxStyle*)styleWithMargin:(UIEdgeInsets)margin next:(UXStyle*)next;
+ (UXBoxStyle*)styleWithPadding:(UIEdgeInsets)padding next:(UXStyle*)next;
+ (UXBoxStyle*)styleWithFloats:(UXPosition)position next:(UXStyle*)next;
+ (UXBoxStyle*)styleWithMargin:(UIEdgeInsets)margin padding:(UIEdgeInsets)padding
						  next:(UXStyle*)next;
+ (UXBoxStyle*)styleWithMargin:(UIEdgeInsets)margin padding:(UIEdgeInsets)padding
					   minSize:(CGSize)minSize position:(UXPosition)position next:(UXStyle*)next;

@end
