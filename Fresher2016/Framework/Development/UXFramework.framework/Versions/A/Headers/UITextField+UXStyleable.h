/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@class UXCSSRuleset;
@class UXDOM;

@interface UITextField (UXStyleable)

/**
 * Applies the given rule set to this text field.
 *
 * This method is exposed primarily for subclasses to use when implementing the
 * applyStyleWithRuleSet: method from UXStyleable.
 */
- (void)applyTextFieldStyleWithRuleSet:(UXCSSRuleset *)ruleSet inDOM: (UXDOM*) dom;

/**
 * Applies the given rule set to this label.
 *
 * This method is exposed primarily for subclasses to use when implementing the
 * applyStyleWithRuleSet: method from UXStyleable. Since some of the view
 * styles (e.g. positioning) may rely on some label elements (like text), this is called
 * before the view styling is done.
 */
- (void)applyTextFieldStyleBeforeViewWithRuleSet:(UXCSSRuleset *)ruleSet inDOM: (UXDOM*) dom;

/**
 * Tells the CSS engine a set of pseudo classes that apply to views of this class.
 * In the case of UITextField, this is :empty.
 * In CSS, you specify these with selectors like UITextField:empty.
 *
 * Make sure to include the leading colon.
 */
- (NSArray*) pseudoClasses;

/**
 * Applies the given rule set to this text field but for a pseudo class. Thus it only supports the subset of
 * properties that can be set on states of the button. (There's no fancy stuff that applies the styles
 * manually on state transitions.
 *
 * Since UIView doesn't have psuedo's, we don't need the specific version for UITextField like we do with
 * applyTextFieldStyleWithRuleSet.
 */
- (void)applyStyleWithRuleSet:(UXCSSRuleset *)ruleSet forPseudoClass: (NSString*) pseudo inDOM: (UXDOM*) dom;

@end

