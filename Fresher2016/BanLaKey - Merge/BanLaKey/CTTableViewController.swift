//
//  CTTableViewController.swift
//  BanLaKey
//
//  Created by Quach Ha Chan Thanh on 9/7/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import Foundation
import UIKit

public class CTTableViewController: UIViewController {
    
    
    var dataSources = [CTCategoryInfo]() {
        didSet {
            if self.tableView != nil {
                self.tableView.dataSources = dataSources
                self.tableView.reloadData()
            }
        }
    }
    
    @IBOutlet weak var tableView: CTTableView!
    
    convenience init() {
        self.init(nibName: nil,bundle: nil)
        
    }
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        if self.tableView == nil {
            self.tableView = CTTableView()
            self.view.addSubview(self.tableView)
        }
        
        self.tableView.dataSources = self.dataSources
        self.tableView.reloadData()
    
    }
    
    public override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        /*if let navigationController = self.navigationController where navigationController.navigationBarHidden == false {
            self.tableView.frame = CGRectMake(0, 44, CGRectGetMaxX(self.view.frame), CGRectGetMaxY(self.view.frame) - 44)
        } else {
            self.tableView.frame = CGRectMake(0, 0, CGRectGetMaxX(self.view.frame), CGRectGetMaxY(self.view.frame))
        }*/
        self.navigationController?.navigationBarHidden = false
        self.tableView.contentInset = UIEdgeInsetsMake(8, 0, 8, 0)
        
    
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}








