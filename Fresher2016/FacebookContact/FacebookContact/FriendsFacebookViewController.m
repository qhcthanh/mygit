//
//  FriendsFacebookViewController.m
//  FacebookContact
//
//  Created by qhcthanh on 7/14/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import "FriendsFacebookViewController.h"
#import "FriendCollectionViewCell.h"
#import "FBUserManager.h"
#import "FBImageDownloader.h"
#import "FBAPIUtility.h"
#import "UserInfoViewController.h"
#import "FBSessionManager.h"
#import "UserInfoViewController.h"
#import "FBCacheManager.h"

@interface FriendsFacebookViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *friendCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *birthDayLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userAvatarImageView;
@property (strong, nonatomic) UIActivityIndicatorView *indicatorView;
@property (assign) BOOL isRefreshing;

@end

@implementation FriendsFacebookViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    FBUserInfo* currentUser = [[FBUserManager shareManager] getCurrentUser];
    if(currentUser) {
        _nameLabel.text = currentUser.userName;
        _birthDayLabel.text = currentUser.birthDay;
        UIImage* picture = [[FBCacheManager sharedCache] imageForKey:currentUser.userID];
        
        if(picture) {
            _userAvatarImageView.image = picture;
        } else {
            FBImageDownloader* imageDowloader = [[FBImageDownloader alloc] initWithSessionManager:[FBSessionManager sharedDefaultSessionManager]];
            NSURL* urlPicture = [NSURL URLWithString:currentUser.pictureURL];
            
            [imageDowloader downloadImageWithURL:urlPicture  identifier:currentUser.userID completionHandler:^(id imageIdentifier, UIImage *image, NSURLResponse *response, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (imageIdentifier == currentUser.userID)
                        _userAvatarImageView.image = picture;
                });
            }];
        }
    }
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [_indicatorView startAnimating];
    _isRefreshing = YES;
}

- (void)viewDidAppear:(BOOL)animated{
    
}

- (IBAction)showDetailAction:(id)sender {
    UserInfoViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([UserInfoViewController class])];
    vc.userInfo = [[FBUserManager shareManager] getCurrentUser];
    
    [self.navigationController pushViewController:vc animated:true];
}


- (void)refreshAction {
    [_indicatorView stopAnimating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
   
}

- (IBAction)logOutAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - UICollectionView DataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if(_isRefreshing) {
        return 2;
    }
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 1)
        return 1;
    else
        return _friendArray.count;
}

- (void)loadMore {
    if (_nextFriendsURLRequest && _isRefreshing) {
        _isRefreshing = NO;
        FBSessionManager *session = [[FBSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        [session startDataTaskWithURL:_nextFriendsURLRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            NSArray* dataArray = [result valueForKey:@"data"];
            NSMutableArray *updateArray = [[NSMutableArray alloc]init];
            
            for( NSDictionary* fr in dataArray) {
                FBUserInfo* friendInfo = [[FBUserInfo alloc] initWithDictionary:fr];
                [updateArray addObject:[NSIndexPath indexPathForItem:[_friendArray count] inSection:0]];
                [_friendArray addObject:friendInfo];
            }
            
            _nextFriendsURLRequest = [NSURL URLWithString:[[result valueForKey:@"paging"] valueForKey:@"next"]];

            if (_nextFriendsURLRequest){  
                _isRefreshing = YES;
            } else {
                _isRefreshing = NO;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_friendCollectionView reloadData];
            });
        }];
    }
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 1 && _isRefreshing) {
        //load more
        if (_isRefreshing == YES){
            [self loadMore];
           
        }
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FriendCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FriendCell" forIndexPath:indexPath];
    
    if (indexPath.section == 1) {
        [cell hideAllUIInCell];
        CGFloat color = 233.0f/255.0f;
        
        _indicatorView.frame = CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height);
        _indicatorView.backgroundColor = [UIColor colorWithRed:color green:color blue:color alpha:1.0f];
        [_indicatorView startAnimating];
        _indicatorView.tag = kIndicatorViewTag;
        
        [cell.contentView addSubview:_indicatorView];
    } else {
        [cell bindingUIWithUserInfo:[_friendArray objectAtIndex:indexPath.row]];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    FriendCollectionViewCell* cell = (FriendCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    UserInfoViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserInfoViewController"];
    
    vc.userInfo = cell.friend;
    [self.navigationController pushViewController:vc animated:true];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(CGRectGetMaxX(self.view.frame), 60);
}

@end
