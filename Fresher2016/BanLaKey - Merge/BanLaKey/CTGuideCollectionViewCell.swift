//
//  CTGuideCollectionViewCell.swift
//  BanLaKey
//
//  Created by qhcthanh on 9/8/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import UIKit

class CTGuideCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var detailButton: UIButton!
    @IBOutlet weak var previewView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
}
