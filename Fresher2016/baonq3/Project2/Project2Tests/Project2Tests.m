//
//  Project2Tests.m
//  Project2Tests
//
//  Created by BaoNQ on 6/3/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CTContactStore.h"
#import "ContactImageCaching.h"

@interface Project2Tests : XCTestCase

@end

@implementation Project2Tests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

/**
 *  Test multiple writing on cachedimage
 */
- (void)testMultipleWritingCachedImage {
    ContactImageCaching *cachedImage = [ContactImageCaching sharedContactImageCaching];
    dispatch_queue_t queue = dispatch_queue_create("com.vng.testQueue", DISPATCH_QUEUE_CONCURRENT);
    
    __block NSInteger count = 0;
    dispatch_apply(10000, queue, ^(size_t index){
        UIImage *image = [UIImage imageNamed:@"mask4.png"];
        [cachedImage cacheImage:image forKey:@"ABC"];
        count++;
        NSLog(@"%li", (long)count);
    });
    while (true) {
        if (count == 10000)
            
            break;
    }
    NSLog(@"Done");

}

/**
 *  Test multiple reading on cached images.
 */
- (void)testMultiReadingCachedImage {
    ContactImageCaching *cachedImage = [ContactImageCaching sharedContactImageCaching];
    //add 100000 images to cache
    for (int i = 0; i < 10000; ++i) {
        UIImage *image = [UIImage imageNamed:@"mask4.png"];
        [cachedImage cacheImage:image forKey:@"ABC"];
    }
    dispatch_queue_t queue = dispatch_queue_create("com.vng.testQueue", DISPATCH_QUEUE_CONCURRENT);
    __block NSInteger count = 0;
    
    dispatch_apply(10000, queue, ^(size_t index){
        [cachedImage imageForKey:@"ABC"];
        count++;
        //NSLog(@"%@", image);
        //NSLog(@"%li", (long)count);
    });

    while (true) {
        if (count == 10000)
            
            break;
    }
    NSLog(@"Done");
}

/**
 *  Test add nil object to cache with a valid key
 */
- (void)testAddNilObjectToCache {
    ContactImageCaching *cachedImage = [ContactImageCaching sharedContactImageCaching];
    [cachedImage cacheImage:nil forKey:@"ABC"];
}

/**
 *  Test add object to cache with nil key
 */
- (void)testAddObjectToCacheWithNilKey {
    ContactImageCaching *cachedImage = [ContactImageCaching sharedContactImageCaching];
    UIImage *image = [UIImage imageNamed:@"mask4.png"];

    [cachedImage cacheImage:image forKey:nil];
}

/**
 *  Test get image from cache with nil key
 */
- (void)testGetObjectFromCacheWithNilKey {
    ContactImageCaching *cachedImage = [ContactImageCaching sharedContactImageCaching];
    NSLog(@"%@", [cachedImage imageForKey:nil]);
}

@end
