/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXAbstractLogger.h"

/** This class provides a logger for Terminal output or Xcode console output,
 * depending on where you are running your code.
 * 
 * As described in the "Getting Started" page,
 * the traditional NSLog() function directs it's output to two places:
 * 
 * - Apple System Log (so it shows up in Console.app)
 * - StdErr (if stderr is a TTY, so log statements show up in Xcode console)
 * 
 * To duplicate NSLog() functionality you can simply add this logger and an asl logger.
 * However, if you instead choose to use file logging (for faster performance),
 * you may choose to use only a file logger and a tty logger.
 */

@interface UXTerminalLogger : UXAbstractLogger {
	
	BOOL isaTTY;
	
	char *app; // Not null terminated
	char *pid; // Not null terminated
	
	size_t appLen;
	size_t pidLen;
	
	NSDateFormatter *dateFormatter;
}

+ (UXTerminalLogger*)sharedInstance;

@end
