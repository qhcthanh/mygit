/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <CoreLocation/CoreLocation.h>

@class UXMailAppInvocation;

/**
 * An interface for interacting with other apps installed on the device.
 *
 */
@interface UXInterApp : NSObject

#pragma mark Chrome vs Safari

+ (void)setPreferGoogleChrome:(BOOL)preferGoogleChromeOverSafari;
+ (BOOL)preferGoogleChrome;
+ (BOOL)openPreferredBrowserWithURL:(NSURL *)url;

#pragma mark Safari
/**
 * Opens the given URL in Safari.
 */
+ (BOOL)safariWithURL:(NSURL *)url;

#pragma mark Google Chrome
/**
 * Returns YES if the Google Chrome application is installed.
 */
+ (BOOL)googleChromeIsInstalled;

/**
 * Opens the given URL in Google Chrome if installed on the device.
 */
+ (BOOL)googleChromeWithURL:(NSURL *)url;

/**
 * The Google Chrome App Store ID.
 */
+ (NSString *)googleChromeAppStoreId;

#pragma mark Google Maps
+ (BOOL)googleMapsIsInstalled;
+ (BOOL)googleMaps;
+ (NSString *)googleMapsAppStoreId;

/**
 * Opens Google Maps at the given location.
 */
+ (BOOL)googleMapAtLocation:(CLLocationCoordinate2D)location;

/**
 * Opens Google Maps at the given location with a title.
 */
+ (BOOL)googleMapAtLocation:(CLLocationCoordinate2D)location title:(NSString *)title;

/**
 * Opens Google Maps with directions from one location to another.
 */
+ (BOOL)googleMapDirectionsFromLocation:(CLLocationCoordinate2D)fromLocation toLocation:(CLLocationCoordinate2D)toLocation;

// directionsMode can be nil. @"driving", @"transit", or @"walking".
+ (BOOL)googleMapDirectionsFromLocation:(CLLocationCoordinate2D)fromLocation toLocation:(CLLocationCoordinate2D)toLocation withMode:(NSString*)directionsMode;
+ (BOOL)googleMapDirectionsFromSourceAddress:(NSString *)srcAddr toDestAddress:(NSString *)destAddr withMode:(NSString *)directionsMode;

/**
 * These just use the user's current location (even if your application doesn't have locations services on, the google maps site/app MIGHT
 */
+ (BOOL)googleMapDirectionsToDestAddress:(NSString *)destAddr withMode:(NSString *)directionsMode;

+ (BOOL)googleMapDirectionsToLocation:(CLLocationCoordinate2D)toLocation withMode:(NSString *)directionsMode;

/**
 * Opens Google Maps with a generic query.
 */
+ (BOOL)googleMapWithQuery:(NSString *)query;

#pragma mark Phone

/**
 * Opens the phone app.
 */
+ (BOOL)phone;

/**
 * Make a phone call with the given number.
 */
+ (BOOL)phoneWithNumber:(NSString *)phoneNumber;

#pragma mark SMS
/**
 * Opens the phone app.
 */
+ (BOOL)sms;

/**
 * Opens mail with the given invocation properties.
 */
+ (BOOL)smsWithNumber:(NSString *)phoneNumber;

#pragma mark Mail
/**
 * Start texting the given number.
 */
+ (BOOL)mailWithInvocation:(UXMailAppInvocation *)invocation;

#pragma mark YouTube
/**
 * Opens the YouTube video with the given video id.
 */
+ (BOOL)youTubeWithVideoId:(NSString *)videoId;

#pragma mark App Store
/**
 * Opens the App Store page for the app with the given ID.
 */
+ (BOOL)appStoreWithAppId:(NSString *)appId;

/**
 * Opens the "Gift this app" App Store page for the app with the given ID.
 */
+ (BOOL)appStoreGiftWithAppId:(NSString *)appId;

/**
 * Opens the "Write a review" App Store page for the app with the given ID.
 */
+ (BOOL)appStoreReviewWithAppId:(NSString *)appId;

#pragma mark iBooks
/**
 * Returns YES if the iBooks application is installed.
 */
+ (BOOL)iBooksIsInstalled;

/**
 * Opens the iBooks application. If the iBooks application is not installed, will open the
 * App Store to the iBooks download page.
 */
+ (BOOL)iBooks;

/**
 * The iBooks App Store ID.
 */
+ (NSString *)iBooksAppStoreId;

#pragma mark Facebook
/**
 * Returns YES if the Facebook application is installed.
 */
+ (BOOL)facebookIsInstalled;

/**
 * Opens the Facebook application. If the Facebook application is not installed, will open the
 * App Store to the Facebook download page.
 */
+ (BOOL)facebook;

/**
 * Opens the Facebook profile with the given id.
 */
+ (BOOL)facebookProfileWithId:(NSString *)profileId;

/**
 * The Facebook App Store ID.
 */
+ (NSString *)facebookAppStoreId;

#pragma mark Twitter
/**
 * Returns YES if the Twitter application is installed.
 */
+ (BOOL)twitterIsInstalled;

/**
 * Opens the Twitter application. If the Twitter application is not installed, will open the
 * App Store to the Twitter download page.
 */
+ (BOOL)twitter;

/**
 * Begins composing a message.
 */
+ (BOOL)twitterWithMessage:(NSString *)message;

/**
 * Opens the profile for the given username.
 */
+ (BOOL)twitterProfileForUsername:(NSString *)username;
+ (NSString *)twitterAppStoreId;

#pragma mark Instagram
/**
 * Returns YES if the Instagram application is installed.
 */
+ (BOOL)instagramIsInstalled;

/**
 * Opens the Instagram application. If the Instagram application is not installed, will open the
 * App Store to the Instagram download page.
 */
+ (BOOL)instagram;

/**
 * Opens the Instagram camera.
 */
+ (BOOL)instagramCamera;

/**
 * Opens the profile for the given username.
 */
+ (BOOL)instagramProfileForUsername:(NSString *)username;

/**
 * Copies an image to a temporary path suitable for use with a UIDocumentInteractionController in
 * order to open the image in Instagram.
 *
 * The image at filePath must be at least 612x612 and preferably square. If the image
 * is smaller than 612x612 then this method will fail.
 */
+ (NSURL *)urlForInstagramImageAtFilePath:(NSString *)filePath error:(NSError **)error;

/**
 * The Instagram App Store ID.
 */
+ (NSString *)instagramAppStoreId;

#pragma mark Custom Application
/**
 * Returns YES if the supplied application is installed.
 */
+ (BOOL)applicationIsInstalledWithScheme:(NSString *)applicationScheme;

/**
 * Opens the supplied application.
 */
+ (BOOL)applicationWithScheme:(NSString *)applicationScheme;

/**
 * Opens the supplied application. If the supplied application is not installed, will open the
 * App Store to the specified ID download page.
 */
+ (BOOL)applicationWithScheme:(NSString *)applicationScheme andAppStoreId:(NSString *)appStoreId;

/**
 * Opens the supplied application.
 */
+ (BOOL)applicationWithScheme:(NSString *)applicationScheme andPath:(NSString *)path;

/**
 * Opens the supplied application, to the specified path. If the supplied application is not installed, will open the
 * App Store to the download page for the specified AppStoreId.
 */
+ (BOOL)applicationWithScheme:(NSString *)applicationScheme appStoreId:(NSString *)appStoreId andPath:(NSString *)path;

@end

@interface UXMailAppInvocation : NSObject {
@private
    NSString* _recipient;
    NSString* _cc;
    NSString* _bcc;
    NSString* _subject;
    NSString* _body;
}

@property (nonatomic, copy) NSString* recipient;
@property (nonatomic, copy) NSString* cc;
@property (nonatomic, copy) NSString* bcc;
@property (nonatomic, copy) NSString* subject;
@property (nonatomic, copy) NSString* body;

/**
 * Returns an autoreleased invocation object.
 */
+ (id)invocation;

@end


