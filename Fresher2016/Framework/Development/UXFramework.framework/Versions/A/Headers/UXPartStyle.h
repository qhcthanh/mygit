/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "UXStyle.h"

@class UXStyleContext;
@interface UXPartStyle : UXStyle {
}

@property (nonatomic, copy) NSString* name;
@property (nonatomic, retain) UXStyle* style;

+ (UXPartStyle*)styleWithName:(NSString*)name style:(UXStyle*)style next:(UXStyle*)next;

- (void)drawPart:(UXStyleContext*)context;

@end
