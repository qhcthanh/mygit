//
//  Memory_ManagementTests.m
//  Memory ManagementTests
//
//  Created by Developer on 5/13/16.
//  Copyright © 2016 VNG. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <Foundation/Foundation.h>
#import "SafeThreadMutableArray.h"

@interface Memory_ManagementTests : XCTestCase

@end

@implementation Memory_ManagementTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    //NSLog(@"temp Test");
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    NSLog(@"temp Test");
    
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}
/**
 *  Bộ test khởi tạo NSMutableArray và 2 queue để lần lượt thêm 100000 phần tử vào array cũng như queue.
 *  Mong muốn là crash
 */
-(void) testAddObjectWithNSMutableArray {
    dispatch_queue_t queueConcurrent = dispatch_queue_create("objectAtIndex Queue", DISPATCH_QUEUE_CONCURRENT);
     dispatch_queue_t queueConcurrent2 = dispatch_queue_create("objectAtIndex Queue", DISPATCH_QUEUE_CONCURRENT);
    NSMutableArray *arrayTestCase = [NSMutableArray new];
    
    //Tạo 100k task addObject vào queue 1 và queue 2
    for (int i = 0; i <100000; i++) {
        dispatch_async(queueConcurrent, ^{
            [arrayTestCase addObject: [NSString stringWithFormat:@"%d",i]];
        });
        dispatch_async(queueConcurrent2, ^{
            [arrayTestCase addObject: [NSString stringWithFormat:@"%d",i]];
        });
    }
}

/**
 *  Bộ test khởi tạo SafeThreadMutableArray và 2 queue để lần lượt thêm 100000 phần tử vào array cũng như queue.
 *  Mong muốn là pass
 */
-(void) testAddObjectWithSafeThreadMutableArray {
    dispatch_queue_t queueConcurrent = dispatch_queue_create("objectAtIndex Queue", DISPATCH_QUEUE_CONCURRENT);
    dispatch_queue_t queueConcurrent2 = dispatch_queue_create("objectAtIndex Queue", DISPATCH_QUEUE_CONCURRENT);
    SafeThreadMutableArray *arrayTestCase = [SafeThreadMutableArray new];
    
    //Tạo 100k task addObject vào queue 1 và queue 2
    for (int i = 0; i <100000; i++) {
        dispatch_async(queueConcurrent, ^{
            [arrayTestCase addObject: [NSString stringWithFormat:@"%d",i]];
        });
        dispatch_async(queueConcurrent2, ^{
            [arrayTestCase addObject: [NSString stringWithFormat:@"%d",i]];
        });
    }
}

/**
 *  Bộ test khởi tạo NSMutableArray và insert 10000 phần tử bằng các thêm các câu lệnh insert vào queueConcurent đồn thời thêm 100000 phẩn tử vào main thread với khối async và tiếp tục tạo ra 100k task insertObject vào queue.
 *   Mong muốn là crash
 */
-(void) testInsertObjectWithNSMutableArray {
    dispatch_queue_t queueConcurrent = dispatch_queue_create("objectAtIndex Queue", DISPATCH_QUEUE_CONCURRENT);
    NSMutableArray *arrayTestCase = [NSMutableArray new];
    
    // Tạo 100k task insertObject thêm vào queueConcurrent.
    for (int i = 0; i <100000; i++) {
        dispatch_async(queueConcurrent, ^{
            [arrayTestCase insertObject:[NSString stringWithFormat:@"%d",i] atIndex:0];
        });
    }
    
    // Tạo 100k task addObject thêm vào main queue bất đồng bộ
    dispatch_async(dispatch_get_main_queue(), ^{
        for (int i = 0; i <100000; i++) {
            [arrayTestCase addObject:[NSNumber numberWithInt:i]];
        }
    });
    
    // Tạo 100k task insertObject thêm vào queueConcurrent.
    for (int i = 0; i <100000; i++) {
        dispatch_async(queueConcurrent, ^{
            [arrayTestCase insertObject:[NSString stringWithFormat:@"%d",i] atIndex:0];
        });
    }
}

/**
 *  Bộ test khởi tạo SafeThreadMutableArray và insert 10000 phần tử bằng các thêm các câu lệnh insert vào queueConcurent đồn thời thêm 100000 phẩn tử vào main thread với khối async và tiếp tục tạo ra 100k task insertObject vào queue.
 *   Mong muốn là pass
 */
-(void) testInsertObjectWithSafeThreadMutableArray {
    dispatch_queue_t queueConcurrent = dispatch_queue_create("objectAtIndex Queue", DISPATCH_QUEUE_CONCURRENT);
    SafeThreadMutableArray *arrayTestCase = [SafeThreadMutableArray new];
    
    // Tạo 100k task insertObject thêm vào queueConcurrent.
    for (int i = 0; i <100000; i++) {
        dispatch_async(queueConcurrent, ^{
            [arrayTestCase insertObject:[NSString stringWithFormat:@"%d",i] atIndex:0];
        });
    }
    
    // Tạo 100k task addObject thêm vào main queue bất đồng bộ
    dispatch_async(dispatch_get_main_queue(), ^{
        for (int i = 0; i <100000; i++) {
            [arrayTestCase addObject:[NSNumber numberWithInt:i]];
        }
    });
    
    // Tạo 100k task insertObject thêm vào queueConcurrent.
    for (int i = 0; i <100000; i++) {
        dispatch_async(queueConcurrent, ^{
            [arrayTestCase insertObject:[NSString stringWithFormat:@"%d",i] atIndex:0];
        });
    }
}

@end
