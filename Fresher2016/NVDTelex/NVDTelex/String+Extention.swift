//
//  String+Extention.swift
//  BanLaKey
//
//  Created by manhduydl on 8/31/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import Foundation

extension String {
    
    // Return String length
    var length: Int {
        return self.characters.count
    }
    
    // Substring to special index
    func substringToIndex(to: Int) -> String {
        return self.substringToIndex(self.startIndex.advancedBy(to))
    }
    
    /**
     Replace one character in String with another character at specify index
     
     - parameter character: charactor will replace
     - parameter atIndex:   index to replace
     
     - returns: return String after replace
     */
    func replace(withCharacter character : String, atIndex : Int) -> String {
        let s1 = self.substringToIndex(self.startIndex.advancedBy(atIndex))
        let s2 = self.substringFromIndex(self.startIndex.advancedBy(atIndex+1))
        
        //let te = s1 + character + s2
        return s1 + character + s2
    }
    
    /**
     Separate sentence to word array
     
     - returns: return word array
     */
    func words() -> [String] {
        let r = self.startIndex..<self.endIndex
        var words = [String]()
        
        self.enumerateSubstringsInRange(r, options: NSStringEnumerationOptions.ByWords) { (substring, _, _, _) -> () in
            words.append(substring!)
        }
        
        return words
    }
    
    /**
     Get last word of sentence
     
     - returns: return last word of sentence
     */
    func lastWord() -> String? {
        let words = self.words()
        
        if words.count > 0 {
            let index = words.endIndex - 1
            let lastWord = words[index]
            
            return lastWord
        }
        return nil
    }
    
    /**
     Get phrase with 2 word from String
     
     - returns: Return phrase array
     */
    func phrases() -> [String] {
        let words = self.words()
        var phrases = [String]()
        
        if words.count < 2 {
            return phrases
        }
        
        for i in 0..<words.count-1 {
            phrases.append(words[i] + " " + words[i+1])
        }
        
        return phrases
    }
    
    /**
     Get last phrase with 2 word from string
     
     - returns: Return phrase
     */
    func lastPhrase() -> String? {
        var words = self.words()
        
        if words.count > 1 {
            return words[words.count - 2] + " " + words[words.count - 1]
        }
        
        return nil
    }
    
    /**
     Get character at specify index
     
     - parameter index: index to get character
     
     - returns: return character like String
     */
    func characterAtIndex(index : Int) -> String {
        return self.substringWithRange(self.startIndex.advancedBy(index)..<self.startIndex.advancedBy(index+1))
    }
    
    /**
     Check charater as string is a unicode charater?
     
     - returns: Bool result
     */
    func isUnicodeCharacter() -> Bool {
        
        if self.length > 1 {
            return false
        }
        
        let s = self.unicodeScalars
        let uni = s[s.startIndex]
        
        let letter = NSCharacterSet.letterCharacterSet()
        return letter.longCharacterIsMember(uni.value)
    }
    
    /**
     Check first word of string is capitalized
     
     - returns: Bool result
     */
    func isCapitalizedWord() -> Bool {
        
        let s = unicodeScalars
        let uni = s[s.startIndex]
        
        let letter = NSCharacterSet.uppercaseLetterCharacterSet()
        return letter.longCharacterIsMember(uni.value)
    }
    
    func isVietNamString() -> Bool {
        for c in characters {
            if let _ = mapVADictionary[String(c)] {
                return true
            }
        }
        return false
    }
}

let mapVADictionary = [
    "đ": "d",
    //a
    "á": "a",
    "à": "a",
    "ả": "a",
    "ã": "a",
    "ạ": "a",
    // ă
    "ă": "a",
    "ắ": "a",
    "ằ": "a",
    "ặ": "a",
    "ẳ": "a",
    "ẵ": "a",
    //â
    "â": "a",
    "ấ": "a",
    "ầ": "a",
    "ẩ": "a",
    "ẫ": "a",
    "ậ": "a",
    // ê
    "ê": "e",
    "ế": "e",
    "ề": "e",
    "ể": "e",
    "ễ": "e",
    "ệ": "e",
    // e
    "é": "e",
    "è": "e",
    "ẻ": "e",
    "ẽ": "e",
    "ẹ": "e",
    // u
    "ú": "u",
    "ù": "u",
    "ủ": "u",
    "ũ": "u",
    "ụ": "u",
    // ư
    "ư": "u",
    "ứ": "u",
    "ừ": "u",
    "ử": "u",
    "ữ": "u",
    "ự": "u",
    // i
    "í": "i",
    "ì": "i",
    "ỉ": "i",
    "ĩ": "i",
    "ị": "i",
    //o
    "ó": "o",
    "ò": "o",
    "ỏ": "o",
    "õ": "o",
    "ọ": "o",
    //ô
    "ô": "o",
    "ố": "o",
    "ồ": "o",
    "ổ": "o",
    "ỗ": "o",
    "ộ": "o",
    
    //ơ
    "ơ": "o",
    "ớ": "o",
    "ờ": "o",
    "ở": "o",
    "ỡ": "o",
    "ợ": "o",
    
    //y
    "ý": "y",
    "ỳ": "y",
    "ỷ": "y",
    "ỹ": "y",
    "ỵ": "y",
]

