/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@class Reachability;
@protocol UXMobileNetworkNotifyDelegate;
@interface UXMobileNetworkNotify : NSObject {
}

- (void)start;
- (void)stop;
- (BOOL)isConnection;

@property (nonatomic, assign, readonly) BOOL hostActive;
@property (nonatomic, assign, readonly) BOOL internetActive;
@property (nonatomic, assign, readonly) NSInteger lastNetworkStatus;

- (void)subcribleNotify:(id<UXMobileNetworkNotifyDelegate>)delegate;
- (void)unSubcribleNotify:(id<UXMobileNetworkNotifyDelegate>)delegate;

+ (UXMobileNetworkNotify*)manager;

@end

@protocol UXMobileNetworkNotifyDelegate <NSObject>

- (void)networkDidChangedStatus:(NSInteger)status;

@end

