/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXStyle.h"

@interface UXTextStyle : UXStyle {
}

@property (nonatomic, retain) UIFont*					font;
@property (nonatomic, retain) UIColor*					color;
@property (nonatomic, retain) UIColor*					shadowColor;
@property (nonatomic) CGFloat							minimumFontSize;
@property (nonatomic) CGSize							shadowOffset;
@property (nonatomic) NSInteger							numberOfLines;
@property (nonatomic) NSTextAlignment					textAlignment;
@property (nonatomic) NSLineBreakMode					lineBreakMode;
@property (nonatomic) UIControlContentVerticalAlignment verticalAlignment;

+ (UXTextStyle*)styleWithFont:(UIFont*)font next:(UXStyle*)next;
+ (UXTextStyle*)styleWithColor:(UIColor*)color next:(UXStyle*)next;
+ (UXTextStyle*)styleWithFont:(UIFont*)font color:(UIColor*)color next:(UXStyle*)next;
+ (UXTextStyle*)styleWithBadge:(UIFont*)font color:(UIColor*)color next:(UXStyle*)next;
+ (UXTextStyle*)styleWithFont:(UIFont*)font color:(UIColor*)color
                textAlignment:(NSTextAlignment)textAlignment next:(UXStyle*)next;
+ (UXTextStyle*)styleWithFont:(UIFont*)font color:(UIColor*)color
				  shadowColor:(UIColor*)shadowColor shadowOffset:(CGSize)shadowOffset
						 next:(UXStyle*)next;
+ (UXTextStyle*)styleWithFont:(UIFont*)font color:(UIColor*)color
			  minimumFontSize:(CGFloat)minimumFontSize
				  shadowColor:(UIColor*)shadowColor shadowOffset:(CGSize)shadowOffset
						 next:(UXStyle*)next;
+ (UXTextStyle*)styleWithFont:(UIFont*)font color:(UIColor*)color
			  minimumFontSize:(CGFloat)minimumFontSize
				  shadowColor:(UIColor*)shadowColor shadowOffset:(CGSize)shadowOffset
                textAlignment:(NSTextAlignment)textAlignment
			verticalAlignment:(UIControlContentVerticalAlignment)verticalAlignment
                lineBreakMode:(NSLineBreakMode)lineBreakMode numberOfLines:(NSInteger)numberOfLines
						 next:(UXStyle*)next;


@end
