//
//  UXTransitionDetail.m
//  testUIView
//
//  Created by VanDao on 6/28/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import "UXTransitionDetail.h"
#import "UXTransitionView.h"
#import "UXImageTransitionView.h"
#import "UXTextTransitionView.h"

@interface UXTransitionDetail() {
@protected
    CGPoint centerPosition;
    CGPoint translation;
    CGFloat scale;
    CGFloat rotation;
}

@end

@implementation UXTransitionDetail

- (id)initWithTransitionView:(id)view{
    self = [super init];
    
    if (self){
        self.transitionView = view;
        self.idView = view;
        translation = CGPointMake(0.0f, 0.0f);
        scale = 1.0f;
        rotation = 0.0f;
        
        centerPosition = _transitionView.center;
    }
    
    return self;
}

- (void)setTransformStickerWithTranslate:(CGPoint)initTranslate scale:(CGFloat)initScale rotate:(CGFloat)initRotate{
    translation = initTranslate;
    scale = initScale;
    rotation = initRotate;
    
    [self updateTransformStickerWithTranslate:CGPointMake(0, 0) scale:1.0f rotate:0.0f];
}

- (void)updateTransformStickerWithTranslate:(CGPoint)newTranslate scale:(CGFloat)newScale rotate:(CGFloat)newRotate{
    [self updateTransformStickerWithTranslate:newTranslate scale:newScale rotate:newRotate atAnchorPoint:self.transitionView.center];
}

- (void)updateTransformStickerWithTranslate:(CGPoint)newTranslate scale:(CGFloat)newScale rotate:(CGFloat)newRotate atAnchorPoint:(CGPoint)anchorPoint{
    // Update transformation value
    scale *= newScale;
    translation.x += newTranslate.x;
    translation.y += newTranslate.y;
    rotation += newRotate;
    if (rotation > __PI)
        rotation -= 2 * __PI;
    else if (rotation < -__PI)
        rotation += 2 * __PI;
    
    // Transform transition view
    CGAffineTransform transformView =  CGAffineTransformIdentity;
    transformView = CGAffineTransformRotate(transformView, rotation);
    transformView = CGAffineTransformScale(transformView, scale, scale);
    self.transitionView.transform = transformView;
    
    // Create the affine transform
    CGAffineTransform transformVertices =  CGAffineTransformIdentity;
    transformVertices = CGAffineTransformTranslate(CGAffineTransformIdentity, self.transitionView.center.x, self.transitionView.center.y);
    transformVertices = CGAffineTransformRotate(transformVertices, newRotate);
    transformVertices = CGAffineTransformScale(transformVertices, newScale, newScale);
    transformVertices = CGAffineTransformTranslate(transformVertices, -self.transitionView.center.x, -self.transitionView.center.y);
    
    // Apply transformation for vetices
    self.transitionView.vertexOne = CGPointApplyAffineTransform(self.transitionView.vertexOne, transformVertices);
    self.transitionView.vertexTwo = CGPointApplyAffineTransform(self.transitionView.vertexTwo, transformVertices);
    self.transitionView.vertexThree = CGPointApplyAffineTransform(self.transitionView.vertexThree, transformVertices);
    self.transitionView.vertexFour = CGPointApplyAffineTransform(self.transitionView.vertexFour, transformVertices);
    CGPoint newAnchorPoint = CGPointApplyAffineTransform(anchorPoint, transformVertices);
    
    // Translation
    newTranslate.x -= (newAnchorPoint.x - anchorPoint.x);
    newTranslate.y -= (newAnchorPoint.y - anchorPoint.y);
    self.transitionView.center = CGPointMake(self.transitionView.center.x + newTranslate.x, self.transitionView.center.y + newTranslate.y);
    self.transitionView.vertexOne = CGPointApplyAffineTransform(self.transitionView.vertexOne, CGAffineTransformMakeTranslation(newTranslate.x, newTranslate.y));
    self.transitionView.vertexTwo = CGPointApplyAffineTransform(self.transitionView.vertexTwo, CGAffineTransformMakeTranslation(newTranslate.x, newTranslate.y));
    self.transitionView.vertexThree = CGPointApplyAffineTransform(self.transitionView.vertexThree, CGAffineTransformMakeTranslation(newTranslate.x, newTranslate.y));
    self.transitionView.vertexFour = CGPointApplyAffineTransform(self.transitionView.vertexFour, CGAffineTransformMakeTranslation(newTranslate.x, newTranslate.y));
    
    //[self.transitionView drawBorder];
}

- (UIImage *)getImage {
    // override at subclass
    
    return nil;
}


@end
