/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@class UXFileLogger;
@protocol UXLogFileManagerDefaultDelegate;
@interface UXStaticLogManager : NSObject {
}

@property (nonatomic, assign) NSUInteger userId;
@property (nonatomic, assign) NSInteger logStaticType;
@property (nonatomic, assign) NSInteger logStaticInterval;

@property (nonatomic, assign) uint64_t didEnterBackgroundTime;
@property (nonatomic, assign) uint64_t didEnterForegroundTime;

+ (UXStaticLogManager*)getInstance;
+ (UXStaticLogManager*)createInstanceOfType:(UInt16)logStaticType withInterval:(UInt32)logStaticInterval withCallback:(id <UXLogFileManagerDefaultDelegate>)delegate;

- (void)logAction:(NSTimeInterval)time
      withCommand:(NSInteger)command
   withSubCommand:(NSInteger)subCommand
      withResCode:(NSInteger)resCode
   withExcuteTime:(NSTimeInterval)excuteTime
       withParams:(NSString*)params;

- (void)addLogEntity:(NSInteger)command
      withSubCommand:(NSInteger)subCommand
    withFailedStatus:(BOOL)failed
withTotalOfTimeRequest:(NSTimeInterval)totalTime
          withParams:(NSString*)params;

@end
