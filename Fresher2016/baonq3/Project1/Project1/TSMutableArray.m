//
//  TSMutableArray
//  Project1
//
//  Created by BaoNQ on 5/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "TSMutableArray.h"

@interface TSMutableArray(){
    
    NSMutableArray* _internalArray;
    dispatch_queue_t _internalQueue;
}
@end

@implementation TSMutableArray

- (id)init {
    self = [super init];
    if (!self)
        return nil;
    _internalArray = [[NSMutableArray alloc] init];
    _internalQueue = dispatch_queue_create("com.ThreadSafe.Project", DISPATCH_QUEUE_CONCURRENT);
    return self;
}

/**
 *  Lấy ra object tại vị trí index
 */
- (id)objectAtIndex:(NSUInteger)index {
    __block id result;
    dispatch_sync(_internalQueue, ^{
        result = [_internalArray objectAtIndex:index];
    });
    return result;
}

/**
 *  Thêm object vào bằng hàm addObject
 */
- (void)addObject:(id)anObject {
    dispatch_barrier_async(_internalQueue, ^{
        [_internalArray addObject:anObject];
    });
}

/**
 *  Lấy số lượng phần tử
 */
- (NSUInteger)count {
    return [_internalArray count];
}

/**
 *  Thêm object vào vị trí index
 */
- (void)insertObject:(id)anObject atIndex:(NSUInteger)index {
    dispatch_barrier_async(_internalQueue, ^{
        [_internalArray insertObject:anObject atIndex:index];
    });
}

/**
 *  Xoá phần tử cuối
 */
- (void)removeLastObject {
    dispatch_barrier_async(_internalQueue, ^{
        [_internalArray removeLastObject];
    });
}

/**
 *  Xoá phần tử tại vị trí index
 */
- (void)removeObjectAtIndex:(NSUInteger)index {
    dispatch_barrier_async(_internalQueue, ^{
        [_internalArray removeObjectAtIndex:index];
    });
}

/**
 *  Thay đổi giá trị của 1 phần tử tại vị trí index
 */
- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject {
    dispatch_barrier_async(_internalQueue, ^{
        [_internalArray replaceObjectAtIndex:index withObject:anObject];
    });
}

@end
