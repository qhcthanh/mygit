/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#if defined(__cplusplus)
extern "C" {
#endif

    NSInteger objectToInterger(id object);
    
    long long objectToLong(id object);
    
    NSTimeInterval objectToTimeInterval(id object) ;
    
    NSString* objectToString(id object);
    
    NSString* nullObjectToEmtpyString(NSString* object);
    
    NSString* nullObjectToSpaceString(NSString* object);
    
    CGFloat objectToFloat(id object);
    
#if defined(__cplusplus)
} // extern "C"
#endif
