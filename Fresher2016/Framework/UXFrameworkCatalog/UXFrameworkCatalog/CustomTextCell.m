/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "CustomTextCell.h"

@implementation CustomTextCellObject

+ (instancetype)objectWithTitle:(NSString *)title {
  CustomTextCellObject* object = [[[self class] alloc] init];
  object.title = title;
  return object;
}

#pragma mark - UXCollectionViewNibCellObject

- (UINib *)collectionViewCellNib {
  return [UINib nibWithNibName:NSStringFromClass([CustomTextCell class]) bundle:nil];
}

@end

@implementation CustomTextCell

#pragma mark - UXCollectionViewCell

- (BOOL)shouldUpdateCellWithObject:(CustomTextCellObject *)object {
  self.label.text = object.title;
  return YES;
}

@end
