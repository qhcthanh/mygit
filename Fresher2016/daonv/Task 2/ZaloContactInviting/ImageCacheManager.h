//
//  ImageContactLoadManager.h
//  ZaloContactInviting
//
//  Created by VanDao on 5/26/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface ImageCacheManager : NSObject
/**
 *  Push an image to cache with key is identifier of person
 *
 *  @param identifier identifier of person
 *  @param image      image of person
 */
- (void)setImage: (UIImage * _Nonnull)image withID:(NSString * _Nonnull)identifier;

/**
 *  Get an image from cache by identifier of person
 *
 *  @param identifier identifier of person
 *
 *  @return image of person from cache
 */
- (UIImage * _Nullable)getImageWithID: (NSString * _Nonnull)identifier;

/**
 *  Remove an image from cache
 *
 *  @param identifier identifier of person
 */
- (void)removeImageWithID: (NSString * _Nonnull)identifier;

/**
 *  Remove all image from cache
 */
- (void)cleanCache;

@end
