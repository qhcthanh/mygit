    //
//  TextEditImageViewController.m
//  CameraGPUImage
//
//  Created by qhcthanh on 6/16/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "TextEditImageViewController.h"
#import "CLColorPickerView.h"
#import "FontCell.h"
@interface TextEditImageViewController () <UITextViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

@end

@implementation TextEditImageViewController {
    CGSize keyboardSize;
    __weak IBOutlet UIView *menuView;
    __weak IBOutlet NSLayoutConstraint *heightEditTextConstraint;
    __weak IBOutlet NSLayoutConstraint *bottomEditTextConstraint;
    __weak IBOutlet NSLayoutConstraint *leadingFontButtonContrainst;
    __weak IBOutlet UIButton *fontButton;
    NSMutableArray* fontNameArray;
    NSMutableArray* fontSpecificArray;
    UISlider *_colorSlider;
    __weak IBOutlet UICollectionView *fontCollectionView;
    __weak IBOutlet UICollectionView *fontSpecificCollectionView;

}

-(void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    bottomEditTextConstraint.constant = self.view.frame.size.height * 0.8;
    //[_textEditImageView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    
  //  UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideEditTextView)];
    //[self.view addGestureRecognizer:tapGesture];
    
    fontNameArray = [[NSMutableArray alloc] init];
    for (NSString *familyName in [UIFont familyNames]){
    //    NSLog(@"Family name: %@", familyName);
        for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName]) {
            //NSLog(@"--Font name: %@", fontName);
            [fontNameArray addObject:fontName];
        }
    }
    fontCollectionView.allowsSelection = true;
    [fontCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]
                                     animated:true scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
    [self setMenu];
}

-(void)hideEditTextView {
    if (_previewVC) {
        [self exportTextViewToImage];
        [_previewVC textShowAction:nil];
    }
}

-(void)viewDidAppear:(BOOL)animated {
  //  [textEditImageView becomeFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated {
    [self.view endEditing:true];
}

-(void)beginEditText {
    [_textEditImageView becomeFirstResponder];
}

-(void)endEditText {
    bottomEditTextConstraint.constant = self.view.frame.size.height * 0.8;
    [self.view endEditing:true];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSRange resultRange = [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet] options:NSBackwardsSearch];
    if ([text length] == 1 && resultRange.location != NSNotFound) {
        [self endEditText];
        [self hideEditTextView];
        return NO;
    }
    
    static NSUInteger maxNumberOfLines = 4;
    NSUInteger numLines = textView.contentSize.height/textView.font.lineHeight;
    CGFloat newHeight = 70 + (numLines-1)*(textView.font.lineHeight);
//    if (numLines > maxNumberOfLines)
//    {
//        textView.text = [textView.text substringToIndex:textView.text.length - 1];
//        return NO;
//    }
    
    NSString *newText = [textView.text stringByReplacingCharactersInRange:range withString:text];
    CGFloat textWidth = CGRectGetWidth(UIEdgeInsetsInsetRect(textView.frame, textView.textContainerInset));
    CGSize sizeTextT = [newText sizeWithAttributes:@{NSFontAttributeName:_textEditImageView.font}];
    
    textWidth = sizeTextT.width + 55;
    
    NSInteger numberOfLines = textWidth / textView.contentSize.width;
    newHeight = 70 + (numberOfLines)*(textView.font.lineHeight);
    if (newHeight != heightEditTextConstraint.constant) {
        heightEditTextConstraint.constant = newHeight;
        [_textEditImageView layoutIfNeeded];
        [textView setContentOffset:CGPointZero];
    }

    return numberOfLines <= maxNumberOfLines;
}



-(CGSize)sizeOfString:(NSString *)string constrainedToWidth:(double)width font:(UIFont *)font
{
    return  [string boundingRectWithSize:CGSizeMake(width, DBL_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size;
}

-(void)textViewDidChange:(UITextView *)textView {
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"%@",segue.identifier);
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)fontAction:(id)sender {
    CGFloat constant = 0;
    if (leadingFontButtonContrainst.constant == 0) {
        constant = self.view.frame.size.width - fontButton.frame.size.width - 25;
        [fontButton setImage:[UIImage imageNamed:@"Back-50"] forState: UIControlStateNormal];
    } else {
        [fontButton setImage:[UIImage imageNamed:@"Font"] forState: UIControlStateNormal];
    }
    
    if (constant != leadingFontButtonContrainst.constant) {
        leadingFontButtonContrainst.constant = constant;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
    
}

-(void)keyboardWillShow:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    bottomEditTextConstraint.constant = keyboardSize.height;
    [UIView animateWithDuration:1.5 animations:^{
        [self.view layoutIfNeeded];
    }];
    
}

-(void)keyboardWillHide:(NSNotification*)aNotification{
    
}


-(void)exportTextViewToImage {
    UIImage* image = nil;
    static float scaledRatio = 10;

    [self.view layoutIfNeeded];

    int numberOfRows = (_textEditImageView.contentSize.height - _textEditImageView.textContainerInset.top - _textEditImageView.textContainerInset.bottom) / _textEditImageView.font.lineHeight;
    //CGFloat textWidth = CGRectGetWidth(UIEdgeInsetsInsetRect(_textEditImageView.frame, _textEditImageView.textContainerInset));
    //textWidth -= 2.0 * _textEditImageView.textContainer.lineFragmentPadding;
    
    //CGSize boundingRect = [self sizeOfString:_textEditImageView.text constrainedToWidth:textWidth font:_textEditImageView.font];
    // numberOfRows = boundingRect.height / _textEditImageView.font.lineHeight;
    
    CGSize newContentSize;
    float heSo = 10;
    CGSize textSize = [_textEditImageView.text sizeWithAttributes:@{NSFontAttributeName:_textEditImageView.font}];
    
    if (numberOfRows == 1) {
        newContentSize = CGSizeMake(textSize.width * scaledRatio + heSo, textSize.height * scaledRatio);
    }
    else
        newContentSize = CGSizeMake(_textEditImageView.contentSize.width * scaledRatio - heSo * scaledRatio, textSize.height * scaledRatio * numberOfRows);
    
    UIGraphicsBeginImageContext(newContentSize);
    {
        UIFont *savedFont = _textEditImageView.font;
        CGPoint savedContentOffset = _textEditImageView.contentOffset;
        CGRect savedFrame = _textEditImageView.frame;
        
        _textEditImageView.font = [savedFont fontWithSize:savedFont.pointSize * scaledRatio];
        _textEditImageView.frame = CGRectMake(0, 0, newContentSize.width, newContentSize.height);
        [_textEditImageView setContentOffset:CGPointMake(0, 0)];
        [_textEditImageView.layer renderInContext: UIGraphicsGetCurrentContext()];
        image = UIGraphicsGetImageFromCurrentImageContext();
        
        _textEditImageView.contentOffset = savedContentOffset;
        _textEditImageView.frame = savedFrame;
        _textEditImageView.font = savedFont;
    }
    UIGraphicsEndImageContext();
    
    if (image != nil) {

        if (_textDelegate && [_textDelegate respondsToSelector:@selector(didFinishEditText:)]) {
            TextStickerNode *newSticker  = _currentTextSticker;
            if (!newSticker) {
                newSticker = [[TextStickerNode alloc] initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
            }
            newSticker.content = _textEditImageView.text;
            newSticker.color = _textEditImageView.textColor;
            [newSticker addStickerWithImage:image];
            [_textDelegate didFinishEditText:newSticker];
            _currentTextSticker = nil;
        }
    }
}

- (UIImage*)colorSliderBackground
{
    CGSize size = _colorSlider.frame.size;
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGRect frame = CGRectMake(5, (size.height-10)/2, size.width-10, 5);
    CGPathRef path = [UIBezierPath bezierPathWithRoundedRect:frame cornerRadius:5].CGPath;
    CGContextAddPath(context, path);
    CGContextClip(context);
    
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGFloat components[] = {
        0.0f, 0.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 0.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 1.0f, 1.0f,
        0.0f, 0.0f, 1.0f, 1.0f
    };
    
    size_t count = sizeof(components)/ (sizeof(CGFloat)* 4);
    CGFloat locations[] = {0.0f, 0.9/3.0, 1/3.0, 1.5/3.0, 2/3.0, 2.5/3.0, 1.0};
    
    CGPoint startPoint = CGPointMake(5, 0);
    CGPoint endPoint = CGPointMake(size.width-5, 0);
    
    CGGradientRef gradientRef = CGGradientCreateWithColorComponents(colorSpaceRef, components, locations, count);
    
    CGContextDrawLinearGradient(context, gradientRef, startPoint, endPoint, kCGGradientDrawsAfterEndLocation);
    
    UIImage *tmp = UIGraphicsGetImageFromCurrentImageContext();
    
    CGGradientRelease(gradientRef);
    CGColorSpaceRelease(colorSpaceRef);
    
    UIGraphicsEndImageContext();
    
    return tmp;
}

- (void)setMenu
{
    // _colorSlider = [self defaultSliderWithWidth:_menuView.width - W - 20];
    float w, h;
    w = menuView.frame.size.height - 20;
    h = menuView.frame.size.width - 20;
    _colorSlider = [[UISlider alloc] initWithFrame:CGRectMake(-(w - menuView.frame.size.width)/2, menuView.frame.size.height/2 - h/2, w, h)];
    
    _colorSlider.maximumTrackTintColor = [UIColor clearColor];
    _colorSlider.minimumTrackTintColor = [UIColor clearColor];
    [_colorSlider setMaximumTrackImage:[UIImage new] forState:UIControlStateNormal];
    [_colorSlider setMinimumTrackImage:[UIImage new] forState:UIControlStateNormal];
    [_colorSlider setThumbTintColor:[UIColor whiteColor]];
    
    
    
    [_colorSlider addTarget:self action:@selector(colorSliderDidChange:) forControlEvents:UIControlEventValueChanged];
    _colorSlider.backgroundColor = [UIColor colorWithPatternImage:[self colorSliderBackground]];
    _colorSlider.value = 0.25;
    _colorSlider.transform = CGAffineTransformMakeRotation(-M_PI_2);
    
    [menuView addSubview:_colorSlider];
    
    [self colorSliderDidChange:_colorSlider];
}

- (void)colorSliderDidChange:(UISlider*)sender
{
    UIColor *color = [self colorForValue:_colorSlider.value];
    _colorSlider.thumbTintColor = color;
    _textEditImageView.textColor = color;
}

- (UIColor*)colorForValue:(CGFloat)value
{
    if(value<1/3.0){
        return [UIColor colorWithWhite:value/0.3 alpha:1];
    }
    return [UIColor colorWithHue:((value-1/3.0)/0.7)*2/3.0 saturation:1 brightness:1 alpha:1];
}

//MARK: UICollectionViewDelegate + UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == fontSpecificCollectionView) {
        return fontSpecificArray.count;
    }
    return [UIFont familyNames].count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FontCell* cell = (FontCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"FontCell" forIndexPath:indexPath];
    [cell cleanUI];
    if (collectionView == fontSpecificCollectionView) {
        NSString* fontSpecific = [fontSpecificArray objectAtIndex:indexPath.row];
        [cell renderUI: fontSpecific];
    } else {
        [cell renderUI:[[UIFont familyNames] objectAtIndex:indexPath.row]];
    }
    return cell;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString* fontName = @"";
    if (collectionView == fontCollectionView) {
        NSArray<NSString*> *arrayT = [UIFont fontNamesForFamilyName:[[UIFont familyNames] objectAtIndex:indexPath.row]];
        fontSpecificArray = [[NSMutableArray alloc] initWithArray:arrayT];
        [fontSpecificCollectionView reloadData];
        fontName = [[UIFont familyNames] objectAtIndex:indexPath.row];
    } else {
        fontName = [fontSpecificArray objectAtIndex:indexPath.row];
    }
    
    [_textEditImageView setFont:[UIFont fontWithName:fontName size:40]];
}


@end
