/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXTextStyle.h"

@interface UXTextStyle (Extension)

+ (UXTextStyle*)styleWithCssSelector:(NSString*)selector
                                next:(UXStyle*)next;

+ (UXTextStyle*)styleWithCssSelector:(NSString*)selector
                     minimumFontSize:(CGFloat)minimumFontSize
                                next:(UXStyle*)next;

+ (UXTextStyle*)styleWithCssSelector:(NSString*)selector
                     minimumFontSize:(CGFloat)minimumFontSize
                       textAlignment:(NSTextAlignment)textAlignment
                   verticalAlignment:(UIControlContentVerticalAlignment)verticalAlignment
                       lineBreakMode:(NSLineBreakMode)lineBreakMode
                       numberOfLines:(NSInteger)numberOfLines
                                next:(UXStyle*)next;


+ (UXTextStyle*)styleWithCssSelector:(NSString*)selector
                            forState:(UIControlState)state
                                next:(UXStyle*)next;

+ (UXTextStyle*)styleWithCssSelector:(NSString*)selector
                            forState:(UIControlState)state
                     minimumFontSize:(CGFloat)minimumFontSize
                                next:(UXStyle*)next;

+ (UXTextStyle*)styleWithCssSelector:(NSString*)selector
                            forState:(UIControlState)state
                     minimumFontSize:(CGFloat)minimumFontSize
                       textAlignment:(NSTextAlignment)textAlignment
                   verticalAlignment:(UIControlContentVerticalAlignment)verticalAlignment
                       lineBreakMode:(NSLineBreakMode)lineBreakMode
                       numberOfLines:(NSInteger)numberOfLines
                                next:(UXStyle*)next;

@end
