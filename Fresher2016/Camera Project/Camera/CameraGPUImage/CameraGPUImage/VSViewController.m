//
//  VSViewController.m
//  CameraGPUImage
//
//  Created by manhduydl on 8/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "VSViewController.h"
#import <Accelerate/Accelerate.h>
#import "OVLViewController.h"
#import "OVLPlaneShaders.h"
#import "OVLScript.h"
#import "OVLTexture.h"
#import "OVLFilter.h"
#import "OVLShaderManager.h"
#import "PreviewVideoViewController.h"
#import "VSPreviewImage.h"

@interface VSViewController() {
    
    __weak IBOutlet GLKView *_glkView;
    OVLShaderManager *shaderManager;
    
    float _curRed;
    BOOL _increasing;
    NSMutableArray *scripts;
    int scriptIndex;
    BOOL isFlashOn;
    
    OVLPlaneShaders* _shader;
    OVLScript* _script;
    CGSize _size;
    BOOL _fInitializingShader;
    CGFloat _clipRatio;

    CVOpenGLESTextureCacheRef _textureCache;
    CVOpenGLESTextureRef _videoTexture;
    
    BOOL _fFirstFrame;
    CMTime _timeStamp, _startTime;
    BOOL _fUpdated;
    NSInteger _duration;
    // Fast buffer
    CVPixelBufferRef _renderPixelBuffer;
    CVOpenGLESTextureRef _renderTexture;
    // External display support
    //GLuint _frameBufferEx;
    
    // FFT
    FFTSetup _fftSetup;
    DSPSplitComplex _fftA;
    CMItemCount _fftSize;
}

@property (nonatomic, strong) MDCaptureManager *captureManager;

@end

@implementation VSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Initialize the view's layer
    _glkView.contentScaleFactor = [UIScreen mainScreen].scale;
    CAEAGLLayer* eaglLayer = (CAEAGLLayer*)_glkView.layer;
    eaglLayer.opaque = YES;
    eaglLayer.contentsScale = _glkView.contentScaleFactor;
    
    // Initialize the context
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    _glkView.context = self.context;
    if (!_glkView.context || ![EAGLContext setCurrentContext:_glkView.context]) {
        NSLog(@"Failed to initialize or set current OpenGL context");
        exit(1);
    }
    
    self.maxDuration = kCMTimeIndefinite;
    
    _fHD = NO;
    _captureManager.fps = 24;
    _captureManager.fFrontCamera = NO;
    _captureManager.isRecording = NO;
    _fPhotoRatio = NO;
    _timeLabel.hidden = YES;
    
    shaderManager = [OVLShaderManager sharedInstance];
    scripts = [NSMutableArray new];
    scriptIndex = 0;
    isFlashOn = NO;
    
    [self setupScript];
    
    [self setupSwipeGesture];
    
    [self loadScript:[scripts objectAtIndex:scriptIndex]];
    
    _captureManager = [[MDCaptureManager alloc] initFromViewController:self];
    _captureManager.delegate = self;

}

- (void)setupScript {
    NSArray *scriptNames = @[@"myscript", @"hawaii", @"freeza", @"matrix1",
                             @"pixelize", @"motionblur", @"red", @"gradientmap",
                             @"colorsketch", @"delicious", @"emboss", @"yeswecan"];
    
    for (NSString *scriptName in scriptNames) {
        NSError *jsonError;
        NSString *path = [[NSBundle mainBundle] pathForResource:scriptName ofType:@"vsscript"];
        NSData *data = [NSData dataWithContentsOfFile:path];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
        
        if (jsonError == nil) {
            OVLScript *script = [[OVLScript alloc] initWithDictionary:json];
            [scripts addObject:script];
        }
    }
}

- (void)setupSwipeGesture {
    
    // Add swipe gesture for glkView
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeFilterGesture:)];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeFilterGesture:)];
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [_glkView addGestureRecognizer:swipeLeft];
    [_glkView addGestureRecognizer:swipeRight];
    
    // add swipe gesture in mode view
    UISwipeGestureRecognizer* swipeLeftModeViewGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeModeGesture:)];
    swipeLeftModeViewGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    UISwipeGestureRecognizer* swipeRightModeViewGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeModeGesture:)];
    swipeRightModeViewGesture.direction = UISwipeGestureRecognizerDirectionRight;
    [_modeView addGestureRecognizer: swipeRightModeViewGesture];
    [_modeView addGestureRecognizer: swipeLeftModeViewGesture];
    
    //Add tap gesture in label mode
    UITapGestureRecognizer* photoTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeModePhoto)];
    [photoTapGesture setNumberOfTapsRequired:1];
    UITapGestureRecognizer* videoTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeModeVideo)];
    [videoTapGesture setNumberOfTapsRequired:1];
    [_photoModeLabel addGestureRecognizer:photoTapGesture];
    [_videoModeLabel addGestureRecognizer:videoTapGesture];
}

#pragma mark -
#pragma mark - handle swipe gesture

- (void)swipeFilterGesture:(UISwipeGestureRecognizer *)swipe {
    int delta = (swipe.direction == UISwipeGestureRecognizerDirectionLeft) ? 1 : -1;
    scriptIndex = (scriptIndex + delta + (int)[scripts count]) % (int)[scripts count];
    
    // Switch script
    [self switchScript:[scripts objectAtIndex:scriptIndex]];
}

- (void)swipeModeGesture:(UISwipeGestureRecognizer*)swipeGesture {
    if (swipeGesture.direction == UISwipeGestureRecognizerDirectionRight) {
        [self changeModePhoto];
    } else {
        [self changeModeVideo];
    }
}

-(void)changeModePhoto {
    _timeLabel.hidden = YES;
    _photoModeLabel.textColor = [UIColor yellowColor];
    _videoModeLabel.textColor = [UIColor whiteColor];
    [_shutterButton setImage: [UIImage imageNamed:@"Circle Thin-96"] forState:UIControlStateNormal];
    _leadingModeViewConstraint.constant = LEADING_PHOTO_MODEVIEW_CONTRAINST;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    [self changeModeFlashCamera];
}

-(void)changeModeVideo {
    _timeLabel.hidden = NO;
    _photoModeLabel.textColor = [UIColor whiteColor];
    _videoModeLabel.textColor = [UIColor yellowColor];
    [_shutterButton setImage: [UIImage imageNamed:@"button_video_normal@2x"] forState:UIControlStateNormal];
    _leadingModeViewConstraint.constant = LEADING_VIDEO_MODEVIEW_CONTRAINST;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    [self changeModeFlashCamera];
}

-(void)changeModeFlashCamera {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, false), ^{
        if (_leadingModeViewConstraint.constant == LEADING_PHOTO_MODEVIEW_CONTRAINST) {
            if (isFlashOn == YES) {
                [_camera lockForConfiguration:nil];
                
                [_camera setTorchMode:AVCaptureTorchModeOff];
                [_camera setFlashMode:AVCaptureFlashModeOn];
                
                [_camera unlockForConfiguration];
            }
            
        } else if (_leadingModeViewConstraint.constant == LEADING_VIDEO_MODEVIEW_CONTRAINST) {
            if (isFlashOn == YES) {
                [_camera lockForConfiguration:nil];
                
                [_camera setFlashMode:AVCaptureFlashModeOff];
                [_camera setTorchMode:AVCaptureTorchModeOn];
                
                [_camera unlockForConfiguration];
            }
        }
    });
}

#pragma mark -
#pragma mark - Handle UIButton

- (IBAction)onTapFlashAction:(id)sender {
    NSString* imageFlashName = @"Flash Off";
    NSInteger mode = AVCaptureTorchModeOff;
    
    if(isFlashOn == NO) {
        mode = AVCaptureTorchModeOn;
        imageFlashName = @"Flash On";
        isFlashOn = YES;
    }
    else {
        isFlashOn = NO;
    }
    
    [_flashButton setImage:[UIImage imageNamed:imageFlashName] forState: UIControlStateNormal];
    
    [_camera lockForConfiguration:nil];
    if(_camera && _leadingModeViewConstraint.constant == LEADING_PHOTO_MODEVIEW_CONTRAINST) {
        [_camera setFlashMode:mode];
    }
    else {
        if (_camera && _leadingModeViewConstraint.constant == LEADING_VIDEO_MODEVIEW_CONTRAINST) {
            [_camera setTorchMode:mode];
        }
    }
    [_camera unlockForConfiguration];
    
}

- (IBAction)onTapBackAction:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)onTapRotateAction:(id)sender {
    [self resetShader];
    [_captureManager rotateCamera];
    _flashButton.enabled = _captureManager.fFrontCamera ? NO : YES;
}

- (IBAction)onTapShutterAction:(id)sender {
    
    if (_leadingModeViewConstraint.constant == LEADING_PHOTO_MODEVIEW_CONTRAINST) {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        VSPreviewImage *vsPreviewImage = (VSPreviewImage *)[mainStoryboard
                                                            instantiateViewControllerWithIdentifier: @"VSPreviewImage"];
        UIImage *image = [_glkView snapshot];
        vsPreviewImage.imagePreview = image;
        [self presentViewController:vsPreviewImage animated:YES completion:nil];
    } else {
        if (!_captureManager.isRecording) {
            [self _startRecording];
        } else {
            [self _stopRecording];
        }
        [_shutterButton setImage: [UIImage imageNamed: _captureManager.isRecording ? @"button_video_pressed" : @"button_video_normal"] forState:UIControlStateNormal];
    }
}

#pragma mark -
#pragma mark - GLKViewDelegate

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect {
    
    if (!_shader) {
        // We don't want to do anything if the shader is not initialized yet.
        return;
    }
    
//    if (_assetReader && _captureManager.isRecording) {
//        BOOL fSkipShader = [self _readAssetBuffer];
//        if (fSkipShader) {
//            return;
//        }
//    }
    
    //NSLog(@"OVLVL drawInRect shading");
    [_shader process];
    
    [view bindDrawable];
    glClearColor(0.333, 0.333, 0.333, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    [_shader render];
    
    if (_captureManager.isRecording) {
        [self _writeToBuffer];
    }
    
//    if (self.renderbufferEx) {
//        if (!_frameBufferEx) {
//            glGenFramebuffers(1, &_frameBufferEx);
//        }
//        glBindFramebuffer(GL_FRAMEBUFFER, _frameBufferEx);
//        glClearColor(0.333, 0.333, 0.333, 1.0);
//        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//        glBindRenderbuffer(GL_RENDERBUFFER, self.renderbufferEx);
//        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
//                                  GL_RENDERBUFFER, self.renderbufferEx);
//        //[_shader renderWithProgram:_programHandleEx texture:_uTextureEx];
//        [_shader render];
//        [_glkView.context presentRenderbuffer:GL_RENDERBUFFER];
//        [view bindDrawable];
//    }
}

#pragma mark - didOutputSampleBuffer

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    if (captureOutput == _captureManager.videoOutput) {
        CVImageBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
        _timeStamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
        size_t width = CVPixelBufferGetWidth(pixelBuffer);
        size_t height = CVPixelBufferGetHeight(pixelBuffer);
        if (!_shader && _script && !_fInitializingShader) {
            _fInitializingShader = YES;
            [self _setInitialSize:CGSizeMake(width, height)];
        }
        
        [self _cleanUpTextures];
        
        // Create a live binding between the captured pixelBuffer and an openGL texture
        CVReturn err = CVOpenGLESTextureCacheCreateTextureFromImage(
                                                                    kCFAllocatorDefault,    // allocator
                                                                    _textureCache,     // texture cache
                                                                    pixelBuffer,            // source Image
                                                                    NULL,                   // texture attributes
                                                                    GL_TEXTURE_2D,          // target
                                                                    GL_RGBA,                // internal format
                                                                    (int)width,             // width
                                                                    (int)height,            // height
                                                                    GL_BGRA,                // format
                                                                    GL_UNSIGNED_BYTE,       // type
                                                                    0,                      // planeIndex
                                                                    &_videoTexture);        // texture out
        if (err) {
            NSLog(@"OVLVC Error at CVOpenGLESTextureCacheCreateTextureFromImage %d", err);
        }
        
        [_shader setSourceTexture:CVOpenGLESTextureGetName(_videoTexture)];
        _fUpdated = YES;
        
    } else
        if (captureOutput == _captureManager.audioOutput) {
            if (_shader.fProcessAudio) {
                // http://stackoverflow.com/questions/14088290/passing-avcaptureaudiodataoutput-data-into-vdsp-accelerate-framework/14101541#14101541
                // Pitch detection
                // http://stackoverflow.com/questions/7181630/fft-on-iphone-to-ignore-background-noise-and-find-lower-pitches?lq=1
                // https://github.com/irtemed88/PitchDetector/blob/master/RIOInterface.mm
                // get a pointer to the audio bytes
                CMItemCount numSamples = CMSampleBufferGetNumSamples(sampleBuffer);
                CMBlockBufferRef audioBuffer = CMSampleBufferGetDataBuffer(sampleBuffer);
                size_t lengthAtOffset;
                size_t totalLength;
                char *samples;
                CMBlockBufferGetDataPointer(audioBuffer, 0, &lengthAtOffset, &totalLength, &samples);
                
                CMAudioFormatDescriptionRef format = CMSampleBufferGetFormatDescription(sampleBuffer);
                const AudioStreamBasicDescription *desc = CMAudioFormatDescriptionGetStreamBasicDescription(format);
                if (desc->mFormatID == kAudioFormatLinearPCM) {
                    if (desc->mChannelsPerFrame == 1 && desc->mBitsPerChannel == 16) {
                        /*
                         NSInteger total = 0;
                         short* values = (short*)samples;
                         for (int i=0; i<numSamples; i++) {
                         total += abs(values[i]);
                         }
                         _shader.audioVolume = (float)total/(float)numSamples/(float)0x4000;
                         */
                        // Convert it to float vector
                        if (_fftSize ==0) {
                            _fftA.realp = malloc(numSamples * sizeof(float));
                            _fftA.imagp = malloc(numSamples * sizeof(float));
                            _fftSize = numSamples;
                        } else if (_fftSize < numSamples) {
                            _fftA.realp = realloc(_fftA.realp, numSamples * sizeof(float));
                            _fftA.imagp = realloc(_fftA.imagp, numSamples * sizeof(float));
                            _fftSize = numSamples;
                        }
                        vDSP_vflt16((short *)samples, 1, _fftA.realp, 1, numSamples);
                        
                        float scale = 1.0 / 0x7fff;
                        vDSP_vsmul(_fftA.realp, 1, &scale, _fftA.realp, 1, numSamples);
                        float maxValue;
                        vDSP_maxv(_fftA.realp, 1, &maxValue, numSamples);
                        _shader.audioVolume = maxValue;
                        
                        //NSLog(@"OVLVC s1=%.2f, %.2f, %.2f, %.2f, %.2f, %.2f", _fftA.realp[0], _fftA.realp[1], _fftA.realp[2], _fftA.realp[3], _fftA.realp[4], _fftA.realp[5]);
                        float a = 0.0;
                        vDSP_vfill(&a, _fftA.imagp, 1, numSamples);
                        //NSLog(@"OVLVC s2=%.2f, %.2f, %.2f, %.2f, %.2f, %.2f", _fftA.imagp[0], _fftA.imagp[1], _fftA.imagp[2], _fftA.imagp[3], _fftA.imagp[4], _fftA.imagp[5]);
                        vDSP_Length log2n = log2(numSamples);
                        vDSP_fft_zrip(_fftSetup, &_fftA, 1, log2n, FFT_FORWARD);
                        
                        /*
                         scale = 1.0 / (2 * numSamples);
                         vDSP_vsmul(_fftA.realp, 1, &scale, _fftA.realp, 1, numSamples/2);
                         vDSP_vsmul(_fftA.imagp, 1, &scale, _fftA.imagp, 1, numSamples/2);
                         */
                        
                        //NSLog(@"OVLVC s3=%.2f, %.2f, %.2f, %.2f, %.2f, %.2f", _fftA.realp[0], _fftA.realp[1], _fftA.realp[2], _fftA.realp[3], _fftA.realp[4], _fftA.realp[5]);
                        maxValue = 0.0;
                        int maxIndex = -1;
                        for (int i=0; i < numSamples/2; i++) {
                            float value = _fftA.realp[i] * _fftA.realp[i] + _fftA.imagp[i] * _fftA.imagp[i];
                            if (value > maxValue) {
                                maxValue = value;
                                maxIndex = i;
                            }
                        }
                        NSLog(@"OVLVC maxIndex=%d", maxIndex);
                    } else {
                        // handle other cases as required
                    }
                }
            }
            
            if (_captureManager.isRecording && _captureManager.audioInput && !_fFirstFrame) {
                //NSLog(@"OGL _audioOutput");
                [_captureManager.audioInput appendSampleBuffer:sampleBuffer];
            }
        }
}


#pragma mark -

- (void)switchScript:(OVLScript*)script {
    _shader = nil;
    _script = script;
    [_script compile];
}

- (void)loadScript:(OVLScript *)script {
    _script = script;
    [_script compile];
    
#if 0
    GLint cTextures = 0;
    glGetIntegerv(GL_MAX_TEXTURE_UNITS, &cTextures);
    NSLog(@"OVC cTextures=%d", cTextures);
#endif
    
    // Initialize the view's properties
    _glkView.drawableDepthFormat = GLKViewDrawableDepthFormatNone;
    //_glkView.drawableMultisample = GLKViewDrawableMultisample4X;
    _glkView.drawableColorFormat = GLKViewDrawableColorFormatRGB565;
    
    CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, NULL, _glkView.context, NULL, &_textureCache);
    
}

#pragma mark -
#pragma mark - private method

- (void)_setInitialSize:(CGSize)size {
    _size = size;
    if (!self.fHD) {
        // NOTE: Using floor add a green line at the bottom
        _size.width = ceil(480.0 * _size.width / _size.height);
        _size.height = 480.0;
    }
    _clipRatio = 1.0;
    if (self.fPhotoRatio) {
        CGFloat width = _size.height / 3.0 * 4.0;
        if (_size.width > width + 1.0) {
            _clipRatio = _size.width / width;
            _size.width = width;
            NSLog(@"OVL capture size adjusted to %f,%f", _size.width, _size.height);
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        _fInitializingShader = NO;
        [self _initShader:_size];
    });
}

- (void)_initShader:(CGSize)size {
    NSLog(@"OVC _initShader: %.0f,%.0f", size.width, size.height);
    
    NSArray* nodes = _script.nodes;
    if (_clipRatio > 1.0) {
        //NSLog(@"OVC _clipRatio is %f", _clipRatio);
        NSDictionary* extra = @{
                                @"pipeline":@[
                                        @{ @"filter":@"stretch", @"attr":@{
                                                   @"ratio":@[@1.0, [NSNumber numberWithFloat:_clipRatio]] } },
                                        ]
                                };
        OVLScript* scriptExtra = [[OVLScript alloc] initWithDictionary:extra];
        [scriptExtra compile];
        NSMutableArray* nodesNew = [NSMutableArray arrayWithArray:scriptExtra.nodes];
        [nodesNew addObjectsFromArray:nodes];
        nodes = nodesNew;
    }
    if (self.fWatermark) {
        NSDictionary* extra = @{
                                @"pipeline":@[
                                        @{ @"filter":@"watermark",
                                           /*@"extra": @{
                                            @"orientation":@YES,
                                            },*/
                                           @"attr":@{
                                                   @"scale":[NSNumber numberWithFloat:50.0/480.0 /*size.height*/],
                                                   @"ratio":@0.7,
                                                   //@"position":@[@0.5, @0.5]
                                                   }
                                           }
                                        ]
                                };
        OVLScript* scriptExtra = [[OVLScript alloc] initWithDictionary:extra];
        [scriptExtra compile];
        NSMutableArray* nodesNew = [NSMutableArray arrayWithArray:nodes];
        [nodesNew addObjectsFromArray:scriptExtra.nodes];
        nodes = nodesNew;
    }
    if (self.imageTexture) {
        //NSLog(@"OVLVC imageOrientation %d", self.imageTexture.imageOrientation);
        NSDictionary* extra = @{
                                @"pipeline":@[
                                        @{ @"source":@"texture" },
                                        @{ @"filter":@"rotation" },
                                        @{ @"filter":@"stretch" },
                                        @{ @"filter":@"timedzoom" },
                                        ]
                                };
        OVLScript* scriptExtra = [[OVLScript alloc] initWithDictionary:extra];
        OVLTexture* nodeTexture = [scriptExtra.nodes objectAtIndex:0];
        if ([nodeTexture isKindOfClass:[OVLTexture class]]) {
            // taking the orientation out
            UIGraphicsBeginImageContextWithOptions(self.imageTexture.size, NO, self.imageTexture.scale);
            [self.imageTexture drawInRect:(CGRect){0, 0, self.imageTexture.size}];
            nodeTexture.imageTexture = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        OVLFilter* nodeRotation = [scriptExtra.nodes objectAtIndex:1];
        OVLFilter* nodeStretch = [scriptExtra.nodes objectAtIndex:2];
        float angle = 0.0;
        //CGImageRef imageRef = self.imageTexture.CGImage;
        //CGSize size = { CGImageGetWidth(imageRef), CGImageGetHeight(imageRef) };
        CGSize size = nodeTexture.imageTexture.size;
        CGSize sizeOut = { 9.0, 16.0 };
        float ratioX = size.width / sizeOut.width;
        float ratioY = size.height / sizeOut.height;
        switch ([UIDevice currentDevice].orientation) {
            case UIDeviceOrientationPortrait:
                angle = M_PI_2;
                break;
            case UIDeviceOrientationPortraitUpsideDown:
                angle = -M_PI_2;
                break;
            case UIDeviceOrientationLandscapeRight:
                angle = M_PI;
                // fall through
            default:
                ratioX = size.height / sizeOut.width;
                ratioY = size.width / sizeOut.height;
                break;
        }
        if (ratioX < ratioY) {
            ratioY = ratioY / ratioX;
            ratioX = 1.0;
        } else {
            ratioX = ratioX / ratioY;
            ratioY = 1.0;
        }
        [nodeRotation setAttr:[NSNumber numberWithFloat:angle] forName:@"angle"];
        [nodeStretch setAttr:@[[NSNumber numberWithFloat:ratioX], [NSNumber numberWithFloat:ratioY]] forName:@"ratio"];
        
        [scriptExtra compile];
        NSMutableArray* nodesNew = [NSMutableArray arrayWithArray:scriptExtra.nodes];
        [nodesNew addObjectsFromArray:nodes];
        nodes = nodesNew;
    }
    
    if (nodes.count == 0) {
        NSLog(@"OVC _initShader: ### special case, empty pipeline");
        NSDictionary* extra = @{
                                @"pipeline":@[
                                        @{ @"filter":@"simple" }
                                        ]
                                };
        OVLScript* scriptExtra = [[OVLScript alloc] initWithDictionary:extra];
        [scriptExtra compile];
        nodes = scriptExtra.nodes;
    }
    
    _shader = [[OVLPlaneShaders alloc] initWithSize:size withNodeList:nodes viewSize:_glkView.bounds.size landscape:self.fLandscape];
    
    // Set the initial projection to all the shaders
    GLKMatrix4 matrix = GLKMatrix4MakeOrtho(0.0, 1.0, 1.0, 0.0, 1.0, 100.0);
    [_shader setProjection:&matrix];
    
    if (_renderPixelBuffer) {
        NSLog(@"OVLVC _initShader calling setRenderTexture");
        [_shader setRenderTexture:_renderTexture];
    }
}

- (void)_cleanUpTextures {
    if (_videoTexture) {
        CFRelease(_videoTexture);
        _videoTexture = NULL;
    }

    CVOpenGLESTextureCacheFlush(_textureCache, 0);
}


-(void) _writeToBuffer {
    if (_fUpdated && _captureManager.videoInput.readyForMoreMediaData) {
        glFlush(); // Making it sure that GPU won't update the texture particially
        
        _fUpdated = NO;
        CMTime timeStamp = _timeStamp;
        if (_fFirstFrame) {
            _fFirstFrame = NO;
            [_captureManager.videoWriter startSessionAtSourceTime:_timeStamp];
            _startTime = _timeStamp;
            _duration = -1;
        } else if (self.speed > 0) {
            CMTime delta = CMTimeSubtract(_timeStamp, _startTime);
            delta.value /= self.speed;
            timeStamp = CMTimeAdd(_startTime, delta);
        }

        [_captureManager.adaptor appendPixelBuffer:_renderPixelBuffer withPresentationTime:timeStamp];

        self.timeRecorded = CMTimeSubtract(_timeStamp, _startTime);
        NSInteger d = (NSInteger)(self.timeRecorded.value / self.timeRecorded.timescale); // interested in only sec.
        if (d > _duration) {
            _duration = d;
            [self updateTimeLabel];
        }
        
        if (CMTimeCompare(self.timeRecorded, _maxDuration) != -1) {
            NSLog(@"OVL stopping at %lld, %d", self.timeRecorded.value, self.timeRecorded.timescale);
            [self _stopRecording];
        }
    }
}

- (void)updateTimeLabel {
    int sec = _duration % 60;
    int min = (_duration / 60) % 60;
    int hour = _duration / 3600;
    //self.labelTime.text = NSString(format:"%02d:%02d:%02d", hour, min, sec) as String
    _timeLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d", hour, min, sec];
}

- (BOOL)isReadyToRecord {
    return (_size.width > 0);
}

- (void)_startRecording {
    if (![self isReadyToRecord]) {
        NSLog(@"OVLVC _size is not yet initialized.");
        return;
    }
    _captureManager.isRecording = YES;
    [_shader startRecording];
    
    _captureManager.videoInputSize = _size;
    [_captureManager startRecording];
    
    _fFirstFrame = YES;
    _duration = 0;
    
    if (_renderPixelBuffer == NULL) {
        NSLog(@"OGLVC creating a new _renderTexture");
        CVReturn status = CVPixelBufferPoolCreatePixelBuffer(NULL, [_captureManager.adaptor pixelBufferPool], &_renderPixelBuffer);
        if ((_renderPixelBuffer == NULL) || (status != kCVReturnSuccess)) {
            NSLog(@"OVLVC can't create pixel buffer %d", status);
            return;
        }
        
        // Create a live binding between _renderPixelBuffer and an openGL texture
        CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                     _textureCache,
                                                     _renderPixelBuffer,
                                                     NULL, // texture attributes
                                                     GL_TEXTURE_2D,
                                                     GL_RGBA, // opengl format
                                                     (int)_size.width,
                                                     (int)_size.height,
                                                     GL_BGRA, // native iOS format
                                                     GL_UNSIGNED_BYTE,
                                                     0,
                                                     &_renderTexture);
        
        [_shader setRenderTexture:_renderTexture];
    }
}


- (void)_stopRecording {
    _captureManager.isRecording = NO;
    [_captureManager stopRecording];
}

#pragma mark - MDCaptureManager delegate

- (void)didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL error:(NSError *)error {
    _timeLabel.text = @"00:00:00";
    _shutterButton.enabled = NO;
    [shaderManager saveMovieToPhotoAlbumAsync:outputFileURL callback:^(NSURL *assetURL) {
        _shutterButton.enabled = YES;
        // show preview video
        AVPlayer* avPlayer = [AVPlayer playerWithURL:outputFileURL];
        PreviewVideoViewController* previewVC = [PreviewVideoViewController new];
        previewVC.avPlayer = avPlayer;
        [self presentViewController:previewVC animated:true completion:nil];
    }];
}

#pragma mark -
#pragma mark - teardown method

- (void)resetShader {
    [_shader cleanupNodelist];
    _shader = nil;
    [self _tearDownRenderTarget];
}

- (void)_tearDownRenderTarget {
    if (_renderPixelBuffer) {
        CVPixelBufferRelease(_renderPixelBuffer);
        _renderPixelBuffer = NULL;
        if (_renderTexture) {
            CFRelease(_renderTexture);
            _renderTexture = NULL;
        }
    }
}

- (void)_tearDownAVCapture {
    [self _cleanUpTextures];
    if (_textureCache) {
        CFRelease(_textureCache);
        _textureCache = nil;
    }
}

- (void)dealloc {
    [self _tearDownAVCapture];
    [self _tearDownRenderTarget];
//    if (_frameBufferEx) {
//        glDeleteFramebuffers(1, &_frameBufferEx);
//    }
    vDSP_destroy_fftsetup(_fftSetup);
    if (_fftSize > 0) {
        free(_fftA.realp);
        free(_fftA.imagp);
    }
}

@end

