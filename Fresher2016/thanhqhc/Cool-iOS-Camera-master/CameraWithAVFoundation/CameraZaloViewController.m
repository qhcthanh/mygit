//
//  CameraZaloViewController.m
//  CameraWithAVFoundation
//
//  Created by qhcthanh on 6/10/16.
//  Copyright © 2016 Gabriel Alvarado. All rights reserved.
//

#import "CameraZaloViewController.h"
#import "CameraSessionView.h"
#import "ShowImageCaptureViewController.h"
@interface CameraZaloViewController () <CACameraSessionDelegate>

@end

@implementation CameraZaloViewController {
    
}

-(void)viewDidLoad {
    _cameraView.delegate = self;
}

- (IBAction)onTapFlashAction:(id)sender {
    [_cameraView onTapFlashButton];
}

- (IBAction)onTapBackAction:(id)sender {
    //[_cameraView onTapDismissButton];
}

- (IBAction)onTapRotateAction:(id)sender {
    [_cameraView onTapToggleButton];
}

- (IBAction)onTapShutterAction:(id)sender {
    [_cameraView onTapShutterButton];
}

-(void)didCaptureImageWithData:(NSData *)imageData {
    NSLog(@"Success");
}

-(void)didCaptureImage:(UIImage *)image {
    NSLog(@"Success");
    ShowImageCaptureViewController* showImageVC = [ShowImageCaptureViewController new];
    showImageVC.captureImage = image;
    
    [self presentViewController:showImageVC animated:true completion:nil];
}

-(void)didStartRecordingVideo:(NSURL *)outputFileURL {
    
}

-(void)didFinishRecordingVideo:(NSURL *)outputFileURL error:(NSError *)error cleanUp:(dispatch_block_t)cleanUpBlock {
    
}


@end
