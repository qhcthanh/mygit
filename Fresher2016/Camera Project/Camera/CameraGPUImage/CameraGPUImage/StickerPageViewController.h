//
//  StickerPageViewController.h
//  CameraGPUImage
//
//  Created by qhcthanh on 6/16/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StickerPageControllerDelegate <NSObject>

@required
-(void)updateIndexViewController:(NSInteger)index;
-(void)numberOfViewController:(NSInteger)numberPages;
@optional
-(void)didSelectItem:(id _Nonnull)item position:(CGPoint)touchPosition;
@end

@interface StickerPageViewController : UIPageViewController {
    
}
@property (nullable, nonatomic, weak) id <StickerPageControllerDelegate> stickerDelegate;
//@property (nonatomic, weak, nullable)
@end
