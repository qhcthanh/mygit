//
//  NSDate+Extension.swift
//  NVDTelex
//
//  Created by VanDao on 8/24/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

import Foundation

var currentTime: NSTimeInterval!

public extension NSDate {
    
    class func startCount() -> NSTimeInterval {
        currentTime = NSDate().timeIntervalSince1970
        return currentTime
    }
    
    class func endCount() -> NSTimeInterval {
        return NSDate().timeIntervalSince1970 - currentTime
    }
    
}