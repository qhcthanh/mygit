//
//  FBUserManager.m
//  FacebookContact
//
//  Created by qhcthanh on 7/14/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import "FBUserManager.h"

@interface FBUserManager()

@property (nonatomic) FBUserInfo* currentUser;

@end

@implementation FBUserManager


+ (id)shareManager {
    static FBUserManager* singletionInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        singletionInstance = [self new];
    });
    
    return singletionInstance;
}

- (void)setCurrentUser:(FBUserInfo *)currentUser {
    _currentUser = currentUser;
}

- (FBUserInfo*)getCurrentUser {
    return _currentUser;
}

@end
