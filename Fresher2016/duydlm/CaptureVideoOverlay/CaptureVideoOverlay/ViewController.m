//
//  ViewController.m
//  CaptureVideoOverlay
//
//  Created by manhduydl on 7/30/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "ViewController.h"
#import "MDFrameDrawer.h"
#import "MDCameraManager.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "UXImageTransitionView.h"
#import "PreviewVideoViewController.h"
#import "SVProgressHUD.h"
#import "MDActionType.h"

#define kImageScaledSize 100

@interface ViewController () {
    GPUImageView *cameraPreview;
    MDCameraManager *cameraManager;
    MDFrameDrawer *frameDrawer;
    float screenWidth;
    float screenHeight;
    MDActionType currentActionType;
    UXImageTransitionView *stickerView;
    
    NSMutableArray *stickerList;
    NSMutableArray<UITouch *> *touchList;
    NSMutableArray *beginPointList;
    NSMutableArray *movePointList;
    
    id focusTranstionView;
    id focusChangeTranstionView;
    BOOL movedSticker;
    __block BOOL isStartMoveSticker;
    CGPoint movePoint;

    NSURL *outUrl;
}

@property (nonatomic, weak) IBOutlet UIButton *recordButton;

@end

@implementation ViewController

//
static CGFloat targetWidth = 480.0;
static CGFloat targetHeight = 640.0;

static NSUInteger videoDurationInSec = 240; // 4min+


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //
    currentActionType = MDNodAction;
    touchList = [[NSMutableArray alloc]init];
    beginPointList = [[NSMutableArray alloc]init];
    movePointList = [[NSMutableArray alloc]init];
    stickerList = [[NSMutableArray alloc]init];
    isStartMoveSticker = YES;
    
    //get screen size
    screenWidth = [UIScreen mainScreen].bounds.size.width;
    screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    // create camera preview
    [self createCameraPreview];
    
    // init capture session and pass it to preview
    [self initCameraCapture];
    
    // add tap gesture to Self
    //UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    //[self.view addGestureRecognizer:singleFingerTap];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    for (UITouch *touch in [touches allObjects])
    {
        // add new touch to list
        [touchList addObject:touch];
        [beginPointList addObject:[NSValue valueWithCGPoint:[touch locationInView:self.view]]];
        [movePointList addObject:[NSValue valueWithCGPoint:[touch locationInView:self.view]]];
        
        CGPoint touchPosition = [[movePointList objectAtIndex:0] CGPointValue];
        
        if (!focusTranstionView){
            for (id sticker in stickerList){
                UXImageTransitionView *curSticker = sticker;
                if ([self checkTransitionView:curSticker containsPoint:touchPosition]){
                    focusTranstionView = sticker;
                    
                   
                    
                    id firstSticker = focusTranstionView;
                    [stickerList removeObject:focusTranstionView];
                    [stickerList insertObject:firstSticker atIndex:0];
                    
                    //[self bringSubviewToFront:curSticker.transitionView];
                    isStartMoveSticker = YES;
                    movedSticker = NO;
                    
                    break;
                }
            }
        }
    }
    

}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    // Update move point list
    for (UITouch *touch in [touches allObjects])
    {
        NSInteger index = [touchList indexOfObject:touch];
        [movePointList replaceObjectAtIndex:index withObject:[NSValue valueWithCGPoint:[touch locationInView:self.view]]];
    }
    
    
    
    // Check if having sticker focussed
    if (focusTranstionView) {
        movedSticker = YES;
        movePoint = [[movePointList objectAtIndex:0] CGPointValue];
        currentActionType = MDChangePositonSticker;
        [self updateContextUpdateBlock];
        
//        //stickerView = [[UXImageTransitionView alloc] initWithImage:[UIImage imageNamed:@"image copy.png"] atCenterPosition:movePoint];
//        UXImageTransitionView *newWeakSticker = [[UXImageTransitionView alloc] initWithImage:[UIImage imageNamed:@"image copy.png"] atCenterPosition:movePoint];;
//        //__block UXImageTransitionView *newWeakStickerView;
//        UXImageTransitionView *oldWeakSticker = focusTranstionView;
//        
//        //NSArray *stickerArray = [NSArray arrayWithArray:stickerList];
//        //BOOL isRecording = cameraManager.isRecording;
//        BOOL weakIsStartMoveSticker = isStartMoveSticker;
        
//        frameDrawer.contextUpdateBlock = ^BOOL(CGContextRef context, CGSize size, CMTime time) {
//            //newWeakStickerView = [[UXImageTransitionView alloc] initWithImage:[UIImage imageNamed:@"image copy.png"] atCenterPosition:movePoint];
//            
//            //CGContextClearRect(context, CGRectMake(0, 0, size.width, size.height));
//            
//            if (weakIsStartMoveSticker) {
//                _curRect = oldWeakSticker.contextRect;
//                isStartMoveSticker = NO;
//            }
//            CGContextClearRect(context, CGRectInset(_curRect, -3, -3));
//            
//            _curRect = newWeakSticker.contextRect;
//            CGContextDrawImage(context, _curRect, newWeakSticker.image.CGImage);
//
////            for (int i=1; i<[stickerArray count]; i++) {
////                UXImageTransitionView *tempt = [stickerArray objectAtIndex:i];
////                CGContextDrawImage(context, tempt.contextRect , tempt.image.CGImage);
////            }
//            return YES;
//        };
//        focusChangeTranstionView = newWeakSticker;
        
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    if (focusTranstionView && movedSticker && focusChangeTranstionView) {
        [stickerList removeObjectAtIndex:0];
        [stickerList insertObject:focusChangeTranstionView atIndex:0];
        NSLog(@"end %f %f", ((UXImageTransitionView*)focusChangeTranstionView).contextRect.origin.x, ((UXImageTransitionView*)focusChangeTranstionView).contextRect.origin.y);
    }
    
    for (UITouch *touch in [touches allObjects]) {
        NSInteger index = [touchList indexOfObject:touch];
        
        [touchList removeObjectAtIndex:index];
        [beginPointList removeObjectAtIndex:index];
        [movePointList removeObjectAtIndex:index];
    }
    if ([beginPointList count] == 0) {
        focusTranstionView = nil;
        isStartMoveSticker = YES;
        currentActionType = MDNodAction;
        [self updateContextUpdateBlock];
    }
}


#pragma mark -
#pragma mark - Button Event Handler

- (IBAction)addStickerButtonClick:(id)sender {
    stickerView = [[UXImageTransitionView alloc] initWithImage:[UIImage imageNamed:@"image copy.png"] atCenterPosition:CGPointMake(320/2, 480/2)];
    [stickerList addObject:stickerView];
    currentActionType = MDAddSticker;
    [self updateContextUpdateBlock];
}

- (IBAction)clearButtonClick:(id)sender {
    [stickerList removeAllObjects];
    currentActionType = MDClearAllSticker;
    [self updateContextUpdateBlock];
}

- (IBAction)recordButtonClick:(id)sender {
    if(cameraManager.isRecording) {
        [self stopCameraCapture];
    } else {
        [self startCameraCapture];
    }
    currentActionType = MDRecordAction;
    [self updateContextUpdateBlock];
}

#pragma mark -
#pragma mark - Hide Status Bar

- (BOOL)prefersStatusBarHidden{
    return YES;
}

#pragma mark -
#pragma mark - Update ContextUpdateBlock

- (void)updateContextUpdateBlock {
    __block int valueOfActionType = currentActionType;
    BOOL isRecording = cameraManager.isRecording;
    UXImageTransitionView *weakStickerView = stickerView;
    
    static CGRect _curRect;
    UXImageTransitionView *newWeakSticker = [[UXImageTransitionView alloc] initWithImage:[UIImage imageNamed:@"image copy.png"] atCenterPosition:movePoint];
    focusChangeTranstionView = newWeakSticker;
    UXImageTransitionView *oldWeakSticker = focusTranstionView;
    BOOL weakIsStartMoveSticker = isStartMoveSticker;

    frameDrawer.contextUpdateBlock = ^BOOL(CGContextRef context, CGSize size, CMTime time) {
        switch (valueOfActionType) {
            case MDNodAction:
               // NSLog(@"No Action");
                break;
            case MDRecordAction:
                break;
            case MDClearAllSticker:
            {
                // clear all sticker
                CGContextClearRect(context, CGRectMake(0, 0, size.width, size.height));
                valueOfActionType = MDNodAction;
            }
                break;
            case MDAddSticker:
            {
                //CGContextClearRect(context, CGRectMake(0, 0, size.width, size.height));
             
//                CGRect rect = CGRectMake(weakStickerView.frame.origin.x/screenWidth * targetWidth, targetHeight - (weakStickerView.frame.origin.y/screenHeight * targetHeight) - weakStickerView.frame.size.height, weakStickerView.frame.size.width/screenWidth * targetWidth, weakStickerView.frame.size.height/screenHeight * targetHeight);
                CGContextDrawImage(context, weakStickerView.contextRect, weakStickerView.image.CGImage);
                valueOfActionType = MDNodAction;
            }
                break;
            case MDChangePositonSticker:
            {
                if (weakIsStartMoveSticker) {
                    _curRect = oldWeakSticker.contextRect;
                    isStartMoveSticker = NO;
                    NSLog(@"ba dao: %f %f %f %f", _curRect.origin.x, _curRect.origin.y, _curRect.size.width, _curRect.size.height);
                }
                CGContextClearRect(context, CGRectInset(_curRect, -10, -10));
                
                _curRect = newWeakSticker.contextRect;
                CGContextDrawImage(context, _curRect, newWeakSticker.image.CGImage);

                valueOfActionType = MDNodAction;
                
            }
                break;
            default:
                break;
        }
        if (isRecording) {
            // update record time
            NSString *chars = @"-\\|/";
            CGFloat secondsf = (CGFloat)time.value / (CGFloat)time.timescale;
            NSUInteger seconds = (int)roundf(secondsf);
            NSUInteger loc = (int)roundf(secondsf * 10) % (int)chars.length;
            NSString *s = [chars substringWithRange:NSMakeRange(loc,1)];
            
            CGContextClearRect(context, CGRectMake(40, 40, 120, 40));
            CGContextClearRect(context, CGRectMake(40, 40, 120, 40));
            CGContextSetRGBFillColor(context, 0, 0, 0, 0.6);
            CGContextFillRect(context, CGRectMake(40, 40, 120, 40));
            
            s = [NSString stringWithFormat:@"%@ - %02d:%02d",s,(int)(seconds / 60),(int)(seconds % 60)];
            
            CGContextSetRGBFillColor(context, 1, 1, 1, 1);
            CGContextShowTextAtPoint(context, 50, 50, [s UTF8String], s.length);
        }
        return YES;
    };

}

#pragma mark -
#pragma mark - Initialize camera preview view

- (void)createCameraPreview
{
    //CGRect rect = CGRectMake(0, 0, screen.size.height, screen.size.width);
    CGRect rect = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    CGAffineTransform T = CGAffineTransformIdentity;
    
    T = CGAffineTransformTranslate(T, -rect.size.width * 0.5, -rect.size.height * 0.5);
    T = CGAffineTransformRotate(T, M_PI_2);
    T = CGAffineTransformTranslate(T, rect.size.width * 0.5, -rect.size.height * 0.5);
    
    cameraPreview = [[GPUImageView alloc] initWithFrame:rect];
    //cameraPreview.transform = T;
    //cameraPreview.fillMode = kGPUImageFillModePreserveAspectRatioAndFill;
    
    [self.view insertSubview:cameraPreview atIndex:0];
}

#pragma mark -
#pragma mark - Initialize camera capture

- (void)initCameraCapture
{
    // create video cameraManager
    cameraManager = [[MDCameraManager alloc] initWithSessionPreset:AVCaptureSessionPreset640x480 cameraPosition:AVCaptureDevicePositionBack];
    cameraManager.shouldCaptureAudio = NO;
    cameraManager.camera.outputImageOrientation = UIInterfaceOrientationPortrait;
    
//    stickerView = [[UXImageTransitionView alloc] initWithImage:[UIImage imageNamed:@"image copy.png"] atCenterPosition:CGPointMake(320/2, 480/2)];
//    UXImageTransitionView *weakStickerView = stickerView;
    
    // context initialization - block (we dont want to overload class in this example)
    void (^contextInitialization)(CGContextRef context, CGSize size) = ^(CGContextRef context, CGSize size) {
        CGContextClearRect(context, CGRectMake(0, 0, size.width, size.height));
        
        NSString *fontName = @"Courier-Bold";
        CGContextSelectFont(context, [fontName UTF8String], 18, kCGEncodingMacRoman);
        
//        CGContextClearRect(context, CGRectMake(0, 0, size.width, size.height));
//        CGRect rect = CGRectMake(weakStickerView.vertexFour.x/screenWidth * targetWidth, targetHeight - (weakStickerView.vertexFour.y/screenHeight * targetHeight) , weakStickerView.frame.size.width, weakStickerView.frame.size.height);
//        NSLog(@"%f", weakStickerView.vertexFour.x);
        //CGContextDrawImage(context, weakStickerView.contextRect, weakStickerView.image.CGImage);
    };
    
    // create overlay
    frameDrawer = [[MDFrameDrawer alloc] initWithSize:CGSizeMake(480, 640)
                               contextInitailizeBlock:contextInitialization];
    
    
    // update ContextUpdateBlock
    [self updateContextUpdateBlock];
    
    // setup composer, preview and cameraManager all together
    [cameraManager.composer addTarget:cameraPreview];
    
    [cameraManager setOverlay:frameDrawer];
    
    [cameraManager startCameraCapture];
}

#pragma mark -
#pragma mark - Handler camera capture

- (void)startCameraCapture
{
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:@"file.mov"];
    outUrl = [NSURL fileURLWithPath:path];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    [fm removeItemAtPath:path error:nil];
    
    NSLog(@"Recording ...");
    
    [cameraManager startCameraRecordingWithURL:outUrl size:CGSizeMake(480, 640)];
    
    __weak ViewController *weakSelf = self;
    
    int64_t stopDelay = (int64_t)(videoDurationInSec * NSEC_PER_SEC);
    dispatch_time_t autoStopTime = dispatch_time(DISPATCH_TIME_NOW, stopDelay);
    
    dispatch_after(autoStopTime, dispatch_get_main_queue(), ^{
        [weakSelf stopCameraCapture];
    });
    
    [self.recordButton setTitle:@"STOP" forState:UIControlStateNormal];
}

- (void)stopCameraCapture
{
    if(!cameraManager.isRecording) {
        return;
    }
    
    NSURL *movieUrl = outUrl;
    
    __weak ViewController *weakSelf = self;
    
    [cameraManager stopCameraRecordingWithCompetionHandler:^(){
        
        dispatch_async(dispatch_get_main_queue(), ^(){
            NSLog(@"Recorded :/");
            [SVProgressHUD showWithStatus:@"Exporting..."];
            
            [weakSelf.recordButton setTitle:@"RECORD" forState:UIControlStateNormal];
            
            // Save to the album
            __block PHObjectPlaceholder *placeholder;
            
            [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                PHAssetChangeRequest* createAssetRequest = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:movieUrl];
                placeholder = [createAssetRequest placeholderForCreatedAsset];
                
            } completionHandler:^(BOOL success, NSError *error) {
                if (success)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video Saved" message:@"Saved To Photo Album"
                                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
                        AVPlayer* avPlayer = [AVPlayer playerWithURL:movieUrl];
                        PreviewVideoViewController* previewVC = [PreviewVideoViewController new];
                        previewVC.avPlayer = avPlayer;
                        
                        [self presentViewController:previewVC animated:true completion:nil];
                    });
                 
                }
                else
                {
                    [SVProgressHUD dismiss];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Video Saving Failed"
                                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
                    });
                   
                }
            }];
            //UISaveVideoAtPathToSavedPhotosAlbum(outputFileName, nil, nil, nil);
          
            
        });
    }];
}

#pragma mark -
#pragma mark - Handle dark side

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    NSLog(@"Memory Warning!!!");
}

- (void)dealloc
{
    NSLog(@"Dealloc");
}

- (BOOL)checkTransitionView:(UXTransitionView *)sticker containsPoint:(CGPoint)point{
    CGPoint contextPoint = [self convertToContextPointFromViewPoint:point];
    CGPoint contextVertexOne = [self convertToContextPointFromViewPoint:sticker.vertexOne];
    CGPoint contextVertexTwo = [self convertToContextPointFromViewPoint:sticker.vertexTwo];
    CGPoint contextVertexThree = [self convertToContextPointFromViewPoint:sticker.vertexThree];
    CGPoint contextVertexFour = [self convertToContextPointFromViewPoint:sticker.vertexFour];
    
    NSMutableArray *linearEquation1 = [self linearEquation:contextVertexOne :contextVertexTwo];
    NSMutableArray *linearEquation2 = [self linearEquation:contextVertexTwo :contextVertexThree];
    NSMutableArray *linearEquation3 = [self linearEquation:contextVertexThree :contextVertexFour];
    NSMutableArray *linearEquation4 = [self linearEquation:contextVertexFour :contextVertexOne];
    
    CGPoint origin = CGPointMake((contextVertexOne.x + contextVertexThree.x)/2, (contextVertexOne.y + contextVertexThree.y)/2);
    
    if (![self is2PointsLocatedOnTheSameSide:contextPoint :origin withLinearEquation:linearEquation1])
        return NO;
    if (![self is2PointsLocatedOnTheSameSide:contextPoint :origin withLinearEquation:linearEquation2])
        return NO;
    if (![self is2PointsLocatedOnTheSameSide:contextPoint :origin withLinearEquation:linearEquation3])
        return NO;
    if (![self is2PointsLocatedOnTheSameSide:contextPoint :origin withLinearEquation:linearEquation4])
        return NO;
    
    return YES;
}

- (NSMutableArray * _Nonnull)linearEquation:(CGPoint)point1 :(CGPoint)point2 {
    
    CGPoint u = CGPointMake(point2.x - point1.x, point2.y - point1.y);
    float a = -u.y;
    float b = u.x;
    float c = -a*point1.x - b*point1.y;
    
    NSMutableArray *result = [[NSMutableArray alloc] init];
    [result addObject:[NSNumber numberWithFloat:a]];
    [result addObject:[NSNumber numberWithFloat:b]];
    [result addObject:[NSNumber numberWithFloat:c]];
    
    return result;
    
}

- (BOOL)is2PointsLocatedOnTheSameSide:(CGPoint)point1 :(CGPoint)point2 withLinearEquation:(NSMutableArray * _Nonnull)linearEquation {
    
    float a = [[linearEquation objectAtIndex:0] floatValue];
    float b = [[linearEquation objectAtIndex:1] floatValue];
    float c = [[linearEquation objectAtIndex:2] floatValue];
    
    CGFloat value1 = (a * point1.x + b * point1.y + c);
    CGFloat value2 = (a * point2.x + b * point2.y + c);
    
    if (value1 * value2 >= 0) {
        return YES;
    }
    
    return NO;
}

- (CGPoint)convertToContextPointFromViewPoint:(CGPoint)viewPoint {
    return CGPointMake(viewPoint.x/320 * 480, 640 - (viewPoint.y/480 *640));
}

@end

