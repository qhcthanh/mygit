/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>

#import "UXPreprocessor.h"

#if defined __cplusplus
extern "C" {
#endif
    
    /**
     * Calculates the ideal dimensions of an attributed string fitting a given size.
     *
     * This calculation is performed off the raw attributed string so this calculation may differ
     * slightly from UXAttributedLabel's use of it due to lack of link and image attributes.
     *
     * This method is used in UXAttributedLabel to calculate its size after all additional
     * styling attributes have been set.
     */
    CGSize UXSizeOfAttributedStringConstrainedToSize(NSAttributedString* attributedString, CGSize size, NSInteger numberOfLines);
    
#if defined __cplusplus
};
#endif

// Vertical alignments for UXAttributedLabel.
typedef NS_ENUM(NSInteger, UXVerticalTextAlignment) {
    UXVerticalTextAlignmentTop = 0,
    UXVerticalTextAlignmentMiddle,
    UXVerticalTextAlignmentBottom,
} ;

extern NSString* const UXAttributedLabelLinkAttributeName; // Value is an NSTextCheckingResult.

@protocol UXAttributedLabelDelegate;

/**
 * The UXAttributedLabel class provides support for displaying rich text with selectable links and
 * embedded images.
 *
 * Differences between UILabel and UXAttributedLabel:
 *
 * - @c NSLineBreakByTruncatingHead and @c NSLineBreakByTruncatingMiddle only apply to single
 *   lines and will not wrap the label regardless of the @c numberOfLines property. To wrap lines
 *   with any of these line break modes you must explicitly add newline characters to the string.
 * - When you assign an NSString to the text property the attributed label will create an
 *   attributed string that inherits all of the label's current styles.
 * - Text is aligned vertically to the top of the bounds by default rather than centered. You can
 *   change this behavior using @link UXAttributedLabel::verticalTextAlignment verticalTextAlignment@endlink.
 * - CoreText fills the frame with glyphs until they no longer fit. This is an important difference
 *   from UILabel because it means that CoreText will not add any glyphs that won't fit in the
 *   frame, while UILabel does. This can result in empty UXAttributedLabels if your frame is too
 *   small where UILabel would draw clipped text. It is recommended that you use sizeToFit to get
 *   the correct dimensions of the attributed label before setting the frame.
 *
 * UXAttributedLabel implements the UIAccessibilityContainer methods to expose each link as an
 * accessibility item.
 *
 */
@interface UXAttributedLabel : UILabel

// Please use attributedText instead. MAINTENANCE: Remove by Feb 28, 2014.
@property (nonatomic, copy) NSAttributedString* attributedString __UX_DEPRECATED_METHOD;

@property (nonatomic) BOOL                autoDetectLinks;    // Default: NO
@property (nonatomic) NSTextCheckingType  dataDetectorTypes;  // Default: NSTextCheckingTypeLink
@property (nonatomic) BOOL                deferLinkDetection; // Default: NO

- (void)addLink:(NSURL *)urlLink range:(NSRange)range;
- (void)removeAllExplicitLinks; // Removes all links that were added by addLink:range:. Does not remove autodetected links.

@property (nonatomic, strong) UIColor*      linkColor;                      // Default: self.tintColor (iOS 7) or [UIColor blueColor] (iOS 6)
@property (nonatomic, strong) UIColor*      highlightedLinkBackgroundColor; // Default: [UIColor colorWithWhite:0.5 alpha:0.5
@property (nonatomic)         BOOL          linksHaveUnderlines;            // Default: NO
@property (nonatomic, copy)   NSDictionary* attributesForLinks;             // Default: nil
@property (nonatomic, copy)   NSDictionary* attributesForHighlightedLink;   // Default: nil
@property (nonatomic)         CGFloat       lineHeight;

@property (nonatomic)         UXVerticalTextAlignment   verticalTextAlignment;  // Default: UXVerticalTextAlignmentTop
@property (nonatomic)         CTUnderlineStyle          underlineStyle;
@property (nonatomic)         CTUnderlineStyleModifiers underlineStyleModifier;
@property (nonatomic)         CGFloat                   shadowBlur;             // Default: 0
@property (nonatomic)         CGFloat                   strokeWidth;
@property (nonatomic, strong) UIColor*                  strokeColor;
@property (nonatomic)         CGFloat                   textKern;

@property (nonatomic, copy) NSString* tailTruncationString;

- (void)setFont:(UIFont *)font            range:(NSRange)range;
- (void)setStrokeColor:(UIColor *)color   range:(NSRange)range;
- (void)setStrokeWidth:(CGFloat)width     range:(NSRange)range;
- (void)setTextColor:(UIColor *)textColor range:(NSRange)range;
- (void)setTextKern:(CGFloat)kern         range:(NSRange)range;
- (void)setUnderlineStyle:(CTUnderlineStyle)style modifier:(CTUnderlineStyleModifiers)modifier range:(NSRange)range;

- (void)insertImage:(UIImage *)image atIndex:(NSInteger)index;
- (void)insertImage:(UIImage *)image atIndex:(NSInteger)index margins:(UIEdgeInsets)margins;
- (void)insertImage:(UIImage *)image atIndex:(NSInteger)index margins:(UIEdgeInsets)margins verticalTextAlignment:(UXVerticalTextAlignment)verticalTextAlignment;

- (void)invalidateAccessibleElements;

@property (nonatomic, weak) IBOutlet id<UXAttributedLabelDelegate> delegate;
@end

/**
 * The methods declared by the UXAttributedLabelDelegate protocol allow the adopting delegate to
 * respond to messages from the UXAttributedLabel class and thus respond to selections.
 *
 */
@protocol UXAttributedLabelDelegate <NSObject>
@optional

/** @name Managing Selections */

/**
 * Informs the receiver that a data detector result has been selected.
 *
 * @param attributedLabel An attributed label informing the receiver of the selection.
 * @param result The data detector result that was selected.
 * @param point The point within @c attributedLabel where the result was tapped.
 */
- (void)attributedLabel:(UXAttributedLabel *)attributedLabel didSelectTextCheckingResult:(NSTextCheckingResult *)result atPoint:(CGPoint)point;

/**
 * Asks the receiver whether an action sheet should be displayed at the given point.
 *
 * If this method is not implemented by the receiver then @c actionSheet will always be displayed.
 *
 * @c actionSheet will be populated with actions that match the data type that was selected. For
 * example, a link will have the actions "Open in Safari" and "Copy URL". A phone number will have
 * @"Call" and "Copy Phone Number".
 *
 * @param attributedLabel An attributed label asking the delegate whether to display the action
 *                             sheet.
 * @param actionSheet The action sheet that will be displayed if YES is returned.
 * @param result The data detector result that was selected.
 * @param point The point within @c attributedLabel where the result was tapped.
 * @returns YES if @c actionSheet should be displayed. NO if @c actionSheet should not be
 *               displayed.
 */
- (BOOL)attributedLabel:(UXAttributedLabel *)attributedLabel shouldPresentActionSheet:(UIActionSheet *)actionSheet withTextCheckingResult:(NSTextCheckingResult *)result atPoint:(CGPoint)point;

@end

/** @name Accessing the Text Attributes */

/**
 * This method is now deprecated and will eventually be removed, please use attributedText instead.
 *
 * @fn UXAttributedLabel::attributedString
 */

/** @name Accessing and Detecting Links */

/**
 * A Booelan value indicating whether to automatically detect links in the string.
 *
 * By default this is disabled.
 *
 * Link detection is deferred until the label is displayed for the first time. If the text changes
 * then all of the links will be cleared and re-detected when the label displays again.
 *
 * Note that link detection is an expensive operation. If you are planning to use attributed labels
 * in table views or similar high-performance situations then you should consider enabling defered
 * link detection by setting @link UXAttributedLabel::deferLinkDetection deferLinkDetection@endlink
 * to YES.
 *
 * @sa UXAttributedLabel::dataDetectorTypes
 * @sa UXAttributedLabel::deferLinkDetection
 * @fn UXAttributedLabel::autoDetectLinks
 */

/**
 * A Boolean value indicating whether to defer link detection to a separate thread.
 *
 * By default this is disabled.
 *
 * When defering is enabled, link detection will be performed on a separate thread. This will cause
 * your label to appear without any links briefly before being redrawn with the detected links.
 * This offloads the data detection to a separate thread so that your labels can be displayed
 * faster.
 *
 * @fn UXAttributedLabel::deferLinkDetection
 */

/**
 * The types of data that will be detected when
 * @link UXAttributedLabel::autoDetectLinks autoDetectLinks@endlink is enabled.
 *
 * By default this is NSTextCheckingTypeLink. <a href="https://developer.apple.com/library/mac/#documentation/AppKit/Reference/NSTextCheckingResult_Class/Reference/Reference.html#//apple_ref/doc/uid/TP40008798-CH1-DontLinkElementID_50">All available data detector types</a>.
 *
 * @fn UXAttributedLabel::dataDetectorTypes
 */

/**
 * Adds a link to a URL at a given range.
 *
 * Adding any links will immediately enable user interaction on this label. Explicitly added
 * links are removed whenever the text changes.
 *
 * @fn UXAttributedLabel::addLink:range:
 */

/**
 * Removes all explicit links from the label.
 *
 * If you wish to remove automatically-detected links, set autoDetectLinks to NO.
 *
 * @fn UXAttributedLabel::removeAllExplicitLinks
 */

/** @name Accessing Link Display Styles */

/**
 * The text color of detected links.
 *
 * The default color is [UIColor blueColor] on pre-iOS 7 devices or self.tintColor on iOS 7 devices.
 * If linkColor is assigned nil then links will not be given any special color. Use
 * attributesForLinks to specify alternative styling.
 *
 *  @image html UXAttributedLabelLinkAttributes.png "Link attributes"
 *
 * @fn UXAttributedLabel::linkColor
 */

/**
 * The background color of the link's selection frame when the user is touching the link.
 *
 * The default is [UIColor colorWithWhite:0.5 alpha:0.5].
 *
 * If you do not want links to be highlighted when touched, set this to nil.
 *
 *  @image html UXAttributedLabelLinkAttributes.png "Link attributes"
 *
 * @fn UXAttributedLabel::highlightedLinkBackgroundColor
 */

/**
 * A Boolean value indicating whether links should have underlines.
 *
 * By default this is disabled.
 *
 * This affects all links in the label.
 *
 * @fn UXAttributedLabel::linksHaveUnderlines
 */

/**
 * A dictionary of CoreText attributes to apply to links.
 *
 * This dictionary must contain CoreText attributes. These attributes are applied after the color
 * and underline styles have been applied to the link.
 *
 * @fn UXAttributedLabel::attributesForLinks
 */

/**
 * A dictionary of CoreText attributes to apply to the highlighted link.
 *
 * This dictionary must contain CoreText attributes. These attributes are applied after
 * attributesForLinks have been applied to the highlighted link.
 *
 * @fn UXAttributedLabel::attributesForHighlightedLink
 */

/** @name Modifying Rich Text Styles for All Text */

/**
 * The vertical alignment of the text within the label's bounds.
 *
 * The default is @c UXVerticalTextAlignmentTop. This is for performance reasons because the other
 * modes require more computation. Aligning to the top is generally what you want anyway.
 *
 * @c UXVerticalTextAlignmentBottom will align the text to the bottom of the bounds, while
 * @c UXVerticalTextAlignmentMiddle will center the text vertically.
 *
 * @fn UXAttributedLabel::verticalTextAlignment
 */

/**
 * The underline style for the entire label.
 *
 * By default this is @c kCTUnderlineStyleNone.
 *
 * <a href="https://developer.apple.com/library/mac/#documentation/Carbon/Reference/CoreText_StringAttributes_Ref/Reference/reference.html#//apple_ref/c/tdef/CTUnderlineStyle">View all available styles</a>.
 *
 * @fn UXAttributedLabel::underlineStyle
 */

/**
 * The underline style modifier for the entire label.
 *
 * By default this is @c kCTUnderlinePatternSolid.
 *
 * <a href="https://developer.apple.com/library/mac/#documentation/Carbon/Reference/CoreText_StringAttributes_Ref/Reference/reference.html#//apple_ref/c/tdef/CTUnderlineStyleModifiers">View all available style
 * modifiers</a>.
 *
 * @fn UXAttributedLabel::underlineStyleModifier
 */

/**
 * A non-negative number specifying the amount of blur to apply to the label's text shadow.
 *
 * By default this is zero.
 *
 * @fn UXAttributedLabel::shadowBlur
 */

/**
 * Sets the stroke width for the text.
 *
 * By default this is zero.
 *
 * Positive numbers will draw the stroke. Negative numbers will draw the stroke and fill.
 *
 * @fn UXAttributedLabel::strokeWidth
 */

/**
 * Sets the stroke color for the text.
 *
 * By default this is nil.
 *
 * @fn UXAttributedLabel::strokeColor
 */

/**
 * Sets the line height for the text.
 *
 * By default this is zero.
 *
 * Setting this value to zero will make the label use the default line height for the text's font.
 *
 * @fn UXAttributedLabel::lineHeight
 */

/**
 * Sets the kern for the text.
 *
 * By default this is zero.
 *
 * The text kern indicates how many points each character should be shifted from its default offset.
 * A positive kern indicates a shift farther away. A negative kern indicates a shift closer.
 *
 * @fn UXAttributedLabel::textKern
 */

/** @name Modifying Tail Truncation Properties */

/**
 * The string to display for tail truncation.
 *
 * By default this is nil and the default ellipses character, \u2026, is used.
 *
 * @fn UXAttributedLabel::tailTruncationString
 */

/** @name Modifying Rich Text Styles in Ranges */

/**
 * Sets the text color for text in a given range.
 *
 * @fn UXAttributedLabel::setTextColor:range:
 */

/**
 * Sets the font for text in a given range.
 *
 * @fn UXAttributedLabel::setFont:range:
 */

/**
 * Sets the underline style and modifier for text in a given range.
 *
 * <a href="https://developer.apple.com/library/mac/#documentation/Carbon/Reference/CoreText_StringAttributes_Ref/Reference/reference.html#//apple_ref/c/tdef/CTUnderlineStyle">View all available styles</a>.
 *
 * <a href="https://developer.apple.com/library/mac/#documentation/Carbon/Reference/CoreText_StringAttributes_Ref/Reference/reference.html#//apple_ref/c/tdef/CTUnderlineStyleModifiers">View all available style
 * modifiers</a>.
 *
 * @fn UXAttributedLabel::setUnderlineStyle:modifier:range:
 */

/**
 * Sets the stroke width for text in a given range.
 *
 * Positive numbers will draw the stroke. Negative numbers will draw the stroke and fill.
 *
 * @fn UXAttributedLabel::setStrokeWidth:range:
 */

/**
 * Sets the stroke color for text in a given range.
 *
 * @fn UXAttributedLabel::setStrokeColor:range:
 */

/**
 * Sets the kern for text in a given range.
 *
 * The text kern indicates how many points each character should be shifted from its default offset.
 * A positive kern indicates a shift farther away. A negative kern indicates a shift closer.
 *
 * @fn UXAttributedLabel::setTextKern:range:
 */

/** @name Adding Inline Images */

/**
 * Inserts the given image inline at the given index in the receiver's text.
 *
 * The image will have no margins.
 * The image's vertical text alignment will be UXVerticalTextAlignmentBottom.
 *
 * @param image The image to add to the receiver.
 * @param index The index into the receiver's text at which to insert the image.
 * @fn UXAttributedLabel::insertImage:atIndex:
 */

/**
 * Inserts the given image inline at the given index in the receiver's text.
 *
 * The image's vertical text alignment will be UXVerticalTextAlignmentBottom.
 *
 * @param image The image to add to the receiver.
 * @param index The index into the receiver's text at which to insert the image.
 * @param margins The space around the image on all sides in points.
 * @fn UXAttributedLabel::insertImage:atIndex:margins:
 */

/**
 * Inserts the given image inline at the given index in the receiver's text.
 *
 * @attention
 *      Images do not currently support UXVerticalTextAlignmentTop and the receiver will fire
 *      multiple debug assertions if you attempt to use it.
 *
 * @param image The image to add to the receiver.
 * @param index The index into the receiver's text at which to insert the image.
 * @param margins The space around the image on all sides in points.
 * @param verticalTextAlignment The position of the text relative to the image.
 * @fn UXAttributedLabel::insertImage:atIndex:margins:verticalTextAlignment:
 */

/** @name Accessibility */

/**
 * Invalidates this label's accessible elements.
 *
 * When a label is contained within another view and that parent view moves, the label will not be
 * informed of this change and any existing accessibility elements will still point to the old
 * screen location. If this happens you must call -invalidateAccessibleElements in order to force
 * the label to refresh its accessibile elements.
 *
 * @fn UXAttributedLabel::invalidateAccessibleElements
 */

/** @name Accessing the Delegate */

/**
 * The delegate of the attributed-label object.
 *
 * The delegate must adopt the UXAttributedLabelDelegate protocol. The UXAttributedLabel class,
 * which does not strong the delegate, invokes each protocol method the delegate implements.
 *
 * @fn UXAttributedLabel::delegate
 */

