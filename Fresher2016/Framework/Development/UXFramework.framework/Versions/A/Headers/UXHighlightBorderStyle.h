/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXStyle.h"

@interface UXHighlightBorderStyle : UXStyle {
}

@property (nonatomic, retain) UIColor*  color;
@property (nonatomic, retain) UIColor*  highlightColor;
@property (nonatomic)         CGFloat   width;

+ (UXHighlightBorderStyle*)styleWithColor:(UIColor*)color highlightColor:(UIColor*)highlightColor
                                    width:(CGFloat)width next:(UXStyle*)next;
@end
