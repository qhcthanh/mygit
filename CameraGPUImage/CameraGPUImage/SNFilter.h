//
//  SNFilter.h
//  CameraGPUImage
//
//  Created by BaoNQ on 8/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SNSticker.h"

@interface SNFilter : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSMutableArray<SNSticker *> *stickers;


+ (NSMutableArray<SNFilter *> *)generateFilters:(UIImage *)originalImage filterNames:(NSArray<NSString *> *)filters;

+ (NSArray *)filterNameList;

+ (BOOL)isFilterExisting:(NSString *)filterName;

- (id)initWithImage:(UIImage *)originalImage;

- (void)addSticker:(SNSticker *)sticker;

- (void)applyFilter:(NSString *)filterName;

@end
