/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol UXOverviewGraphViewDataSource;

/**
 * A graph view.
 *
 * @ingroup Overview-Pages
 */
@interface UXOverviewGraphView : UIView

/**
 * The data source for this graph view.
 */
@property (nonatomic, weak) id<UXOverviewGraphViewDataSource> dataSource;

@end

/**
 * The data source for UXOverviewGraphView.
 *
 * @ingroup Overview-Pages
 */
@protocol UXOverviewGraphViewDataSource <NSObject>

@required

/**
 * Fetches the total range of all x values for this graph.
 */
- (CGFloat)graphViewXRange:(UXOverviewGraphView *)graphView;

/**
 * Fetches the total range of all y values for this graph.
 */
- (CGFloat)graphViewYRange:(UXOverviewGraphView *)graphView;

/**
 * The data source should reset its iterator for fetching points in the graph.
 */
- (void)resetPointIterator;

/**
 * Fetches the next point in the graph to plot.
 */
- (BOOL)nextPointInGraphView: (UXOverviewGraphView *)graphView
                       point: (CGPoint *)point;

/**
 * The data source should reset its iterator for fetching events in the graph.
 */
- (void)resetEventIterator;

/**
 * Fetches the next event in the graph to plot.
 */
- (BOOL)nextEventInGraphView: (UXOverviewGraphView *)graphView
                      xValue: (CGFloat *)xValue
                       color: (UIColor **)color;

@end
