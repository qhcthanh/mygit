//
//  NSCachedURLResponse+Extension.m
//  FacebookContact
//
//  Created by VanDao on 7/25/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import "NSCachedURLResponse+Extension.h"

@implementation NSCachedURLResponse (UIImage)

- (UIImage *)imageData {
    return [UIImage imageWithData:self.data];
}

@end
