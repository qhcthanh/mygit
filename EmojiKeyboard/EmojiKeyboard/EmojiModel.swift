//
//  EmojiModel.swift
//  EmojiModel
//
//  Created by Quach Ha Chan Thanh on 8/13/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import Foundation
import UIKit

// Simple Model has only titleContent
public protocol SimpleModelProtocol: NSObjectProtocol {
    var titleContent: String {get}
}

// Image + Title Model
public protocol ImageModelProtocol: SimpleModelProtocol {
    var imageContent: UIImage {get}
}


public class EmojiModel: NSObject, SimpleModelProtocol {
    
    var keywords: [String]!
    var char: String!
    var name: String!
    
    var emojiSkins: [String]?
    
    init(name: String, keywords: [String], char: String) {
        super.init()
        
        self.name = name
        self.keywords = keywords
        self.char = char
        
        if !self.keywords.contains("face") {
            if char.canHaveSkinToneModifier {
                // Ensure the unmodified Emoji is being used
                let unmodified = char.emojiUnmodified

                for modifier in char.emojiSkinToneModifiers {
                    let newEmoji = unmodified + modifier
                    
                    // add emoji to list
                    if let _ = self.emojiSkins {
                        self.emojiSkins!.append(newEmoji)
                    } else {
                        self.emojiSkins = [String]([newEmoji])
                    }
                }
            }
        }

    }
    
    init(jsonData: NSDictionary) {
        
    }
    
    public var titleContent: String {
        return char
    }
    
}

public class EmojiCategory: NSObject {
    
    var category: String!
    var emojis: [EmojiModel]!
    var priority: Int!
    var represent: String!
    
    lazy var emojiDefault: String = {
        return self.emojis.first!.char
    }()
    
    init(category: String, jsonData: NSDictionary) {
        super.init()
        
        self.emojis = [EmojiModel]()
        self.category = category
        
        if let priority = jsonData["priority"] as? Int,
            let represent = jsonData["represent"] as? String,
            let arrayData = jsonData["emojis"] as? NSArray
        {
            self.priority = priority
            self.represent = represent
            
            for emojiDictionary in arrayData {
                if let emojiDictionary = emojiDictionary as? NSDictionary,
                    let name = emojiDictionary["name"] as? String,
                    let char = emojiDictionary["char"] as? String,
                    let keywords = emojiDictionary["keywords"] as? [String]
                {
                    let emoji = EmojiModel(name: name,keywords: keywords, char: char)
                    self.emojis.append(emoji)
                }
            }
        }
        
        
    }
    
}

public class EmojiManager: NSObject {
    
    private static var instance: EmojiManager?
    
    var emojiCategorys: [EmojiCategory]!
    
    private override init() {
        super.init()
        
        let emojiAllDictionary = readEmojiJsonCustom(emojiResourcePath)
        
        self.emojiCategorys = [EmojiCategory]()
        
        for key in emojiAllDictionary.allKeys {
            let category = key as! String
            let emojiCategory = EmojiCategory(category: category, jsonData: emojiAllDictionary[category] as! NSDictionary)
            
            self.emojiCategorys.append(emojiCategory)
        }
        
        self.emojiCategorys.sortInPlace({
            if $0.priority < $1.priority {
                return true
            }
            return false
        })
    }
    
    class func shareInstance() -> EmojiManager {
        var emojiManagerDispatchOne: dispatch_once_t  = 0
        
        dispatch_once(&emojiManagerDispatchOne, {
            if instance == nil {
                instance = EmojiManager()
            }
        })
        
        return instance!
    }
    
}

func writeEmojiJsonFromBase () {
    if let path = NSBundle.mainBundle().pathForResource("emojis", ofType: "json"),
        let data = NSData(contentsOfFile: path),
        let dictionaryEmojis = try? NSJSONSerialization.JSONObjectWithData(data, options: []) as! NSDictionary
    {
        //   print(dictionaryEmojis)
        var emojiDictionary = Dictionary<String,AnyObject>()
        
        for key in dictionaryEmojis.allKeys {
            if let emoji = dictionaryEmojis.objectForKey(key) as? Dictionary<String,AnyObject> {

                if let char = emoji["char"] as? String,
                    let keywords = emoji["keywords"] as? [String] {
                    let emojiData = [
                        "name": key,
                        "keywords": keywords,
                        "char": char
                    ]
                    
                    let category = emoji["category"] as! String
            
                    if let emojiCategory = emojiDictionary[category] as? NSDictionary
                    {
                        let emojiCategory = NSMutableDictionary(dictionary: emojiCategory)
                        if let emojiArray = emojiCategory["emojis"] as? NSMutableArray {
                            emojiArray.addObject(emojiData)
                            emojiCategory["emojis"] = emojiArray
                            emojiDictionary[category] = emojiCategory
                        }
                    } else {
                        let emojis = NSMutableArray()
                        emojis.addObject(emojiData)
                        emojiDictionary[category] = [
                            "emojis": emojis,
                            "priority": 1,
                            "represent": "a"
                        ]
                    }
                }
            }
        }
        
        //print(emojiDictionary)
        
        if let jsonData = try? NSJSONSerialization.dataWithJSONObject(emojiDictionary, options: .PrettyPrinted) {
            if let json = String(data: jsonData, encoding: NSUTF8StringEncoding) {
                print(json)
                try! json.writeToFile("/Users/thanhqhc/Desktop/emojiBL.json", atomically: false, encoding: NSUTF8StringEncoding)
            }
            
        }
        
        
    }
}

let emojiResourcePath = NSBundle.mainBundle().pathForResource("emojiBL", ofType: "json")!
func readEmojiJsonCustom(path: String) -> NSDictionary {
    let data = NSData(contentsOfFile: path)
    let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves)
    
    return json as! NSDictionary
}




