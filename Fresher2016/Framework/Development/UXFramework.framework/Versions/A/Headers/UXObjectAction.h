/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXAction.h"

@interface UXObjectAction : NSObject

@property (nonatomic, copy) UXActionBlock tapAction;
@property (nonatomic, copy) UXActionBlock detailAction;
@property (nonatomic, copy) UXActionBlock navigateAction;

@property (nonatomic) SEL tapSelector;
@property (nonatomic) SEL detailSelector;
@property (nonatomic) SEL navigateSelector;

@end

@interface UXAction ()

@property (nonatomic, weak) id target;

- (UXObjectAction *)actionForObjectOrClassOfObject:(id<NSObject>)object;

@end
