/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXDefaultStyleSheet.h"

const extern NSString* kRelativeCSSPath;

@class UXJSONStyleSheet;
@interface UXJSONDefaultStyleSheet : UXDefaultStyleSheet {
@protected
    UXJSONStyleSheet* _styleSheet;
    NSString* _themeFolderPath;
}

@property (nonatomic, readonly) NSString* themeFolderPath;
@property (nonatomic, readonly) UXJSONStyleSheet* styleSheet;

+ (UXJSONDefaultStyleSheet*)globalJSONDefaultStyleSheet;

- (BOOL)addStyleSheetFromDisk:(NSString*)filename;

- (NSString*)findFilePathByUrl:(NSString*)url;
- (NSString*)findDefaultPathByUrl:(NSString*)url;

@end
