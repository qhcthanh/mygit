//
//  EmojiView.swift
//  EmojiKeyboard
//
//  Created by Quach Ha Chan Thanh on 8/13/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import Foundation
import UIKit

private let emojiMinimumLineSpacing: CGFloat = 5
private let emojiMinimumItemSpacing: CGFloat = 5
private let kBLEmojiItemCell = "kBLEmojiItemCell"

@objc public protocol BLEmojiViewDelegate: NSObjectProtocol {

    optional func didTouchReturn() // The delegate called when use tap in retrun button
    
    optional func didTouchBackspace() // The delegate called when use tap in backspace button
    
    func didSelectEmoji(emoji: String) // The delegate called when use tap in emoji
}

public class BLEmojiView: UIView, EmojiCollectionViewCellDelegate, PopoverDelegate {
    
    // MARK: UI Properties
    public var titleLabel: UILabel!
    public var emojiCollectionView: UICollectionView!
    public var emojiCategoryCollectionView: UICollectionView!
    
    public var emojiCollectionViewLayout = UICollectionViewFlowLayout()
    public var emojiCategoryCollectionViewLayout = UICollectionViewFlowLayout()
    
    // MARK: Public Properties
    public private(set) var emojiManager = BLEmojiManager.shareInstance()
    weak public var delegate: BLEmojiViewDelegate?
    
    // Setting
    public var minimumPressDuration: Double = 0.06
    public var minimumTimeShowPopup: Double = 0.5
    public var spacingLeftSectionEmojiCategory: CGFloat = 5
    public var spacingRightSectionEmojiCategory: CGFloat = 35
    
    var timerShowPopup: NSTimer = NSTimer()
    
    // Last Cell selected property
    private weak var lastCellPress: UICollectionViewCell? {
        willSet {
            if let newValue = newValue {
                if newValue != self.lastCellPress {
                    timerShowPopup.invalidate()
                    if let newValue = newValue as? BLEmojiCollectionViewCell {
                        self.willShowPopupEmoji(newValue)
                    }
                }
            } else {
                if lastCellPress != nil {
                    self.lastCellPress?.contentView.backgroundColor = .clearColor()
                }
                
                self.popupView.hidePopup()
                self.popupView.removeFromSuperview()
                
                timerShowPopup.invalidate()
            }
            

        }
    }
    
    lazy var popupView: BLKey = {
        
        var emojiPopupFrame: CGRect
        
        #if isIPad
            emojiPopupFrame = calculatePhonePopupFrame(self.emojiItemCellSize, self.frame.width, self.frame.height)
        #else
            emojiPopupFrame = calculatePhonePopupFrame(self.emojiItemCellSize, self.frame.width, self.frame.height)
        #endif
        
        var temporaryPopupView = BLKey(frame: CGRectMake(0, 0, self.emojiItemCellSize.width, self.emojiItemCellSize.height), title: " ", originalPopupFrame: emojiPopupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: .whiteColor())
        
        
        return temporaryPopupView
    }()
    
    // Private properties
    private var selectCricleView: UIView!
    
    var emojiItemCellSize: CGSize {
        get {
            let itemCellWidth = min(self.bounds.width, self.bounds.height) * 0.18
            return CGSizeMake(itemCellWidth,itemCellWidth)
        }
    }
    
    // MARK: Initialize
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setup()
    }
    
    
    private func setup() {
        self.titleLabel = UILabel()
        self.titleLabel.font = UIFont(name: BLThemeManager.currentTheme.buttonTitleFont,
                                      size: UIScreen.mainScreen().interfaceOrientation() == .Portrait ? BLThemeManager.currentTheme.buttonPhonePortraitFontSize : BLThemeManager.currentTheme.buttonPhoneLandscapeFontSize)
        
        self.selectCricleView = UIView(frame: CGRectMake(0,0,28,28))
        self.selectCricleView.layer.cornerRadius = 12
        self.selectCricleView.backgroundColor = BLThemeManager.currentTheme.characterButtonHightlightedColor.colorWithAlphaComponent(0.9)
        
        self.emojiCollectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: self.emojiCollectionViewLayout)
        self.emojiCategoryCollectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: self.emojiCategoryCollectionViewLayout)
        
        self.clipsToBounds = false
        self.emojiCollectionView.clipsToBounds = false
        
        // Add subview
        self.addSubview(titleLabel)
        self.addSubview(emojiCollectionView)
        self.addSubview(emojiCategoryCollectionView)
        
        // Update UI
        self.updateUI()
        self.setupEmojiCollectionView()
        self.setupEmojiCategoryCollectionView()
        
        self.setupConstraints()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(didChangedDeviceOrientation), name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    func setupConstraints() {
        
        // Set contrain to titleView (height = 0.1 * superView, and in top left superview)
        self.titleLabel.constrain(.Height, being: .Equal, to: .Height, of: self, multipliedBy: 0.1)
        self.titleLabel.constrain(.Left, to: .Left, of: self, offsetBy: 5)
        self.titleLabel.constrain(.Top, to: .Top, of: self)
        
        // Set collectionView is between title and return button
        self.emojiCategoryCollectionView.constrain(.Left, to: .Left, of: self)
        self.emojiCategoryCollectionView.constrain(.Right, to: .Right, of: self, offsetBy: 5)
        self.emojiCategoryCollectionView.constrain(.Height, being: .Equal, to: .Height, of: self, multipliedBy: 0.125)
        self.emojiCategoryCollectionView.constrain(.Bottom, to: .Bottom, of: self)
        
        // Set collectionView is between title and return button
        self.emojiCollectionView.constrain(.Right, to: .Right, of: self)
        self.emojiCollectionView.constrain(.Left, to: .Left, of: self)
        self.emojiCollectionView.constrain(.Top, to: .Bottom, of: self.titleLabel)
        self.emojiCollectionView.constrain(.Bottom, to: .Top, of: self.emojiCategoryCollectionView, offsetBy: 5)
        
    }
    
    func didChangedDeviceOrientation() {
        
        self.emojiCollectionView.reloadData()
        self.emojiCategoryCollectionView.reloadData()
    }
    
    private func updateUI() {
        
        // Set first title of emoji category
        if let emojiCategory = self.emojiManager.emojiCategorys.first {
            self.titleLabel.text = emojiCategory.category
        }
        
        // Set UI titleButton
        self.titleLabel.textColor = BLThemeManager.currentTheme.characterButtonTitleColor
        
        // Set UI EmojiCollectionView + EmojiCategory
        self.emojiCollectionView.backgroundColor = .clearColor()
        self.emojiCategoryCollectionView.backgroundColor = .clearColor()
           
    }
    
    private func setupEmojiCollectionView() {
        
        //self.emojiCollectionView.registerClass(EmojiCategoryCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: "EmojiCategoryCell")
        let emojiItemCellNib = UINib(nibName: "EmojiCollectionViewCell", bundle: nil)
        self.emojiCollectionView.registerNib(emojiItemCellNib, forCellWithReuseIdentifier: kBLEmojiItemCell)
        
        self.emojiCollectionViewLayout.itemSize = CGSizeMake(self.bounds.width, self.bounds.height - 5)
        self.emojiCollectionViewLayout.minimumLineSpacing = 10
        self.emojiCollectionViewLayout.minimumInteritemSpacing = 0
        self.emojiCollectionViewLayout.scrollDirection = .Horizontal
        
        let testGesture = BLLongPressGesture(owerView: self.emojiCollectionView)
        testGesture.delegate = self
        
      //  self.emojiCollectionView.pagingEnabled = true
        self.emojiCollectionView.showsVerticalScrollIndicator = false
        self.emojiCollectionView.showsHorizontalScrollIndicator = false
        
        self.emojiCollectionView.delegate = self
        self.emojiCollectionView.dataSource = self
    
    }
    
    private func setupEmojiCategoryCollectionView() {
        
        let emojiItemCellNib = UINib(nibName: "EmojiCollectionViewCell", bundle: nil)
        self.emojiCategoryCollectionView.registerNib(emojiItemCellNib, forCellWithReuseIdentifier: kBLEmojiItemCell)
        
        self.emojiCategoryCollectionViewLayout.itemSize = CGSizeMake(25,25)
        self.emojiCategoryCollectionViewLayout.minimumLineSpacing = 8
        self.emojiCategoryCollectionViewLayout.minimumInteritemSpacing = 5
        self.emojiCategoryCollectionViewLayout.scrollDirection = .Horizontal
        
        self.emojiCategoryCollectionView.showsVerticalScrollIndicator = false
        self.emojiCategoryCollectionView.showsHorizontalScrollIndicator = false
        
        self.emojiCategoryCollectionView.delegate = self
        self.emojiCategoryCollectionView.dataSource = self
        
    }
    
    // MARK: Button Action's
    
    func didSelectEmoji(emoji: String) {
        
        if let delegate = self.delegate {
            delegate.didSelectEmoji(emoji)
        }
    }
    
    // MARK: Popup handler
    
    func willShowPopupEmoji(emojiCell: UICollectionViewCell) {
        
        timerShowPopup = NSTimer.scheduledTimerWithTimeInterval(self.minimumTimeShowPopup, target: self, selector: #selector(didShowPupupEmoji), userInfo: ["emoji":emojiCell], repeats: false)
    }
    
    func didShowPupupEmoji() {
        
        // Get emojiCell in timer
        if let emojiCell = self.timerShowPopup.userInfo!["emoji"] as? BLEmojiCollectionViewCell {
            
            if (self.popupView.popupShowed) {
                self.popupView.hidePopup()
            }
            
            // If details nil. Will use empty Array to create Popup. Because create popup should have details array
            let details = emojiCell.itemProtocol?.details == nil ? [] : emojiCell.itemProtocol?.details
            
            // create subView
            let subView = self.popupView.popupSubviewInitializer(emojiCell.itemProtocol!.titleContent, extendedCharacters: details)
            
            self.popupView.setPopupView(subView)
            
            var emojiPopupFrame: CGRect
            
            #if isIPad
                emojiPopupFrame = calculatePadPopupFrame(self.emojiItemCellSize, self.frame.width, self.frame.height)
            #else
                emojiPopupFrame = calculatePhonePopupFrame(self.emojiItemCellSize, self.frame.width, self.frame.height)
            #endif
            
            // Recalculate popup
            self.popupView.updatePopupFrame(emojiPopupFrame)
            self.popupView.clipsToBounds = false
            
            if self.popupView.superview == nil {
                self.emojiCollectionView.addSubview(self.popupView)
            }
            self.popupView.frame.origin = emojiCell.frame.origin
           
            self.popupView.showPopup()
            
        }
        
    }
}

// MARK: UICollectionViewDelegate, UICollectionViewDataSource
extension BLEmojiView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        if collectionView != self.emojiCollectionView {
            return 1
        }
        
        return emojiManager.emojiCategorys.count
    }
    
    public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView != self.emojiCollectionView {
            return emojiManager.emojiCategorys.count
        }
        
        return emojiManager.emojiCategorys[section].emojis.count
    }
    
    public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let emojiCell = collectionView.dequeueReusableCellWithReuseIdentifier(kBLEmojiItemCell, forIndexPath: indexPath) as! BLEmojiCollectionViewCell
        
        if collectionView == emojiCollectionView {

            let emojiProtocol = emojiManager.emojiCategorys[indexPath.section].emojis[indexPath.row]
            emojiCell.bindingUI(emojiProtocol)
            emojiCell.delegate = self
            
            
        } else {
            
            let representCategory = emojiManager.emojiCategorys[indexPath.row].represent
            emojiCell.emojiLabel.adjustsFontSizeToFitWidth = true
            emojiCell.emojiLabel.text = representCategory
            
            if indexPath.row == 0 {
                emojiCell.backgroundView = self.selectCricleView
            }
            
        }
        
        return emojiCell
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        if collectionView == self.emojiCollectionView {
            return self.emojiItemCellSize
        }
        return CGSizeMake(collectionView.frame.height - 2, collectionView.frame.height - 2)
    }

    public func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
        self.titleLabel.text = self.emojiManager.emojiCategorys[indexPath.section].category
        self.highlightEmojiCategoryCell(NSIndexPath(forItem: indexPath.section, inSection: 0))
    }

    public func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: NSInteger) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0,spacingLeftSectionEmojiCategory, 0, spacingRightSectionEmojiCategory)
    }
    
    public func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if collectionView == emojiCollectionView {
            
        } else {
            //self.highlightEmojiCategoryCell(indexPath)
            self.emojiCollectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: 0, inSection: indexPath.row), atScrollPosition: .Left, animated: false)
        }
    }
    
    private func highlightEmojiCategoryCell(indexPath: NSIndexPath) {
        if let cell = emojiCategoryCollectionView.cellForItemAtIndexPath(indexPath) {
            if let _ = cell.backgroundView {
                cell.backgroundView = nil
            }
            if cell.backgroundView != self.selectCricleView {
                cell.backgroundView = self.selectCricleView
            }
        }
    }
    
}

// MARK: BLLongPressGestureDelegate
extension BLEmojiView: BLLongPressGestureDelegate {
    
    public func longPressGestureOnTouchDown(view: UIView) {
        
        if let cell = view as? BLEmojiCollectionViewCell {
            if let lastCellPress = self.lastCellPress {
                lastCellPress.contentView.backgroundColor = .clearColor()
                self.lastCellPress = nil
            }
            
            cell.contentView.backgroundColor = BLThemeManager.currentTheme.characterButtonHightlightedColor
            self.lastCellPress = cell
            
        }
        
        
    }
    
    public func longPressGestureOnTouchUp(view: UIView?) {
        
        if view is BLEmojiCollectionViewCell {
            if self.popupView.popupShowed {
                self.lastCellPress = nil
                //return
            }
            
            if let lastCellPress = self.lastCellPress
            {
                lastCellPress.contentView.backgroundColor = .clearColor()
                if let lastCellPress = lastCellPress as? BLEmojiCollectionViewCell,
                    let emojiProtocol = lastCellPress.itemProtocol {
                    
                    // Call delegate touch emoji
                    self.didSelectEmoji(emojiProtocol.titleContent)
                }
            }
        }

        self.lastCellPress = nil
    }
    
    public func longPressGestureOnTouchMove(view: UIView?) {
        
        if let cell = view as? BLEmojiCollectionViewCell {
            // Kiem tra neu khong phai la cell cu thi doi mau bthg cell cu~ va to mau` cell moi
            if cell != self.lastCellPress {
                if let lastCellPress = self.lastCellPress {
                    lastCellPress.contentView.backgroundColor = .clearColor()
                }
                cell.contentView.backgroundColor = BLThemeManager.currentTheme.characterButtonHightlightedColor
                self.lastCellPress = cell
            }
        }
    }
    
    public func longPressGestureOnTouchFail(view: UIView?) {
        if let lastCellPress = self.lastCellPress {
            lastCellPress.contentView.backgroundColor = .clearColor()
        }
        
        self.lastCellPress = nil
        
    }
    
}






