//
//  Extention.swift
//  testSuggestion
//
//  Created by manhduydl on 8/18/16.
//  Copyright © 2016 admin. All rights reserved.
//

import Foundation

let mapVADictionary = [
    "đ": "d",
    //a
    "á": "a",
    "à": "a",
    "ả": "a",
    "ã": "a",
    "ạ": "a",
    // ă
    "ă": "a",
    "ắ": "a",
    "ằ": "a",
    "ặ": "a",
    "ẳ": "a",
    "ẵ": "a",
    //â
    "â": "a",
    "ấ": "a",
    "ầ": "a",
    "ẩ": "a",
    "ẫ": "a",
    "ậ": "a",
    // ê
    "ê": "e",
    "ế": "e",
    "ề": "e",
    "ể": "e",
    "ễ": "e",
    "ệ": "e",
    // e
    "é": "e",
    "è": "e",
    "ẻ": "e",
    "ẽ": "e",
    "ẹ": "e",
    // u
    "ú": "u",
    "ù": "u",
    "ủ": "u",
    "ũ": "u",
    "ụ": "u",
    // ư
    "ư": "u",
    "ứ": "u",
    "ừ": "u",
    "ử": "u",
    "ữ": "u",
    "ự": "u",
    // i
    "í": "i",
    "ì": "i",
    "ỉ": "i",
    "ĩ": "i",
    "ị": "i",
    //o
    "ó": "o",
    "ò": "o",
    "ỏ": "o",
    "õ": "o",
    "ọ": "o",
    //ô
    "ô": "o",
    "ố": "o",
    "ồ": "o",
    "ổ": "o",
    "ỗ": "o",
    "ộ": "o",
    
    //ơ
    "ơ": "o",
    "ớ": "o",
    "ờ": "o",
    "ở": "o",
    "ỡ": "o",
    "ợ": "o",
    
    //y
    "ý": "y",
    "ỳ": "y",
    "ỷ": "y",
    "ỹ": "y",
    "ỵ": "y",
]

extension NSDate {
    func yearsFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Year, fromDate: date, toDate: self, options: []).year
    }
    func monthsFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Month, fromDate: date, toDate: self, options: []).month
    }
    func weeksFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.WeekOfYear, fromDate: date, toDate: self, options: []).weekOfYear
    }
    func daysFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Day, fromDate: date, toDate: self, options: []).day
    }
    func hoursFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Hour, fromDate: date, toDate: self, options: []).hour
    }
    func minutesFrom(date: NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Minute, fromDate: date, toDate: self, options: []).minute
    }
    func secondsFrom(date: NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Second, fromDate: date, toDate: self, options: []).second
    }
    func offsetFrom(date: NSDate) -> String {
        if yearsFrom(date)   > 0 { return "\(yearsFrom(date))y"   }
        if monthsFrom(date)  > 0 { return "\(monthsFrom(date))M"  }
        if weeksFrom(date)   > 0 { return "\(weeksFrom(date))w"   }
        if daysFrom(date)    > 0 { return "\(daysFrom(date))d"    }
        if hoursFrom(date)   > 0 { return "\(hoursFrom(date))h"   }
        if minutesFrom(date) > 0 { return "\(minutesFrom(date))m" }
        if secondsFrom(date) > 0 { return "\(secondsFrom(date))s" }
        return ""
    }
}

// MARK: - Extention String
extension String {
    
    // Return String length
    var length: Int {
        return self.characters.count
    }
    
    // Substring to special index
    func substringToIndex(to: Int) -> String {
        return self.substringToIndex(self.startIndex.advancedBy(to))
    }
    
    /**
     Replace one character in String with another character at specify index
     
     - parameter character: charactor will replace
     - parameter atIndex:   index to replace
     
     - returns: return String after replace
     */
    func replace(withCharacter character : String, atIndex : Int) -> String {
        let s1 = self.substringToIndex(self.startIndex.advancedBy(atIndex))
        let s2 = self.substringFromIndex(self.startIndex.advancedBy(atIndex+1))
        
        //let te = s1 + character + s2
        return s1 + character + s2
    }
    
    /**
     Separate sentence to word array
     
     - returns: return word array
     */
    func words() -> [String] {
        let r = self.startIndex..<self.endIndex
        var words = [String]()
        
        self.enumerateSubstringsInRange(r, options: NSStringEnumerationOptions.ByWords) { (substring, _, _, _) -> () in
            words.append(substring!)
        }
        
        return words
    }
    
    /**
     Get last word of sentence
     
     - returns: return last word of sentence
     */
    func lastWord() -> String {
        let words = self.words()
        
        let index = words.endIndex - 1
        let lastWord = words[index]
        
        return lastWord
    }
    
    /**
     Get phrase with 2 word from String
     
     - returns: Return phrase array
     */
    func phrases() -> [String] {
        let words = self.words()
        var phrases = [String]()
        
        if words.count < 2 {
            return phrases
        }
        
        for i in 0..<words.count-1 {
            phrases.append(words[i] + " " + words[i+1])
        }
        
        return phrases
    }
    
    /**
     Get character at specify index
     
     - parameter index: index to get character
     
     - returns: return character like String
     */
    func characterAtIndex(index : Int) -> String {
        return self.substringWithRange(self.startIndex.advancedBy(index)..<self.startIndex.advancedBy(index+1))
    }
    
    /**
     Check charater as string is a unicode charater?
     
     - returns: Bool result
     */
    func isUnicodeCharacter() -> Bool {
        
        if self.length > 1 {
            return false
        }
        
        let s = self.unicodeScalars
        let uni = s[s.startIndex]
        
        let letter = NSCharacterSet.letterCharacterSet()
        return letter.longCharacterIsMember(uni.value)
    }
    
    func toASCII() -> String {
        var result : String = ""
        
        for c in characters {
            if let asciiChar = mapVADictionary[String(c)] {
                result += asciiChar
            } else {
                result.append(c)
            }
        }
        return result
    }
    
}

// MARK: - Extention LetterNode
extension LetterNode {
    
    /**
     Converse LetterNode to Json String
     
     - returns: return Json String
     */
    func toJsonString() -> String {
        var listNodes : [LetterNode] = [LetterNode]()
        self.getAllEndNodeFromNode(self, list: &listNodes)
        
        var dict : [String : NSDictionary] = [:]
        for item in listNodes {
//            let tDict = ["time" : NSDate().description, "count" : 0]
            let tDict = ["time" : item.time, "count" : item.count]
            let key : String = item.key
            dict[key] = tDict
        }
   
        var jsonString = ""
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(dict, options: NSJSONWritingOptions.PrettyPrinted)
            jsonString = String(data: jsonData, encoding: NSUTF8StringEncoding)!
        } catch let error as NSError {
            print(error)
        }
        
        return jsonString
    }
    
    /**
     Get all EndNode from RootNode
     
     - parameter rootNode: rootNode to find EndNode
     - parameter list:     list EndNode to return
     */
    private func getAllEndNodeFromNode(node : LetterNode, inout list : [LetterNode]) {
        for (_, v1) in node.childrentNode {
            if v1.isEnd == true {
                list.append(v1)
            }
            self.getAllEndNodeFromNode(v1, list: &list)
        }
    }
    
    func getAllEndNode() -> [LetterNode] {
        var listNode : [LetterNode] = [LetterNode]()
        self.getAllEndNodeFromNode(self, list: &listNode)
        return listNode
    }
}

extension Character {
    
    func isUnicodeCharacter() -> Bool {
        let s = String(self).unicodeScalars
        let uni = s[s.startIndex]
        
        let letter = NSCharacterSet.letterCharacterSet()
        return letter.longCharacterIsMember(uni.value)
    }
}