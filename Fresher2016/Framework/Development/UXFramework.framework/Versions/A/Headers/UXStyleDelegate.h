/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class UXStyle;
@class UXStyleContext;

@protocol UXStyleDelegate <NSObject>

@optional

- (NSString*)textForLayerWithStyle:(UXStyle*)style;

- (UIImage*)imageForLayerWithStyle:(UXStyle*)style;

- (void)drawLayer:(UXStyleContext*)context withStyle:(UXStyle*)style;

@end
