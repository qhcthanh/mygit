//
//  AddContactViewController.h
//  ZaloContact
//
//  Created by qhcthanh on 6/2/16.
//  Copyright © 2016 qhcthanh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhoneCell.h"
@interface AddContactViewController : UIViewController<UITextFieldDelegate, UIImagePickerControllerDelegate, UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate>

@end
