/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class UXStyleContext;

const extern NSInteger kDefaultLightSource;

@interface UXStyle : NSObject {
}

@property (nonatomic, retain) UXStyle* next;

- (id)initWithNext:(UXStyle*)next;

- (UXStyle*)next:(UXStyle*)next;

- (void)draw:(UXStyleContext*)context;

- (UIEdgeInsets)addToInsets:(UIEdgeInsets)insets forSize:(CGSize)size;
- (CGSize)addToSize:(CGSize)size context:(UXStyleContext*)context;

- (void)addStyle:(UXStyle*)style;

- (id)firstStyleOfClass:(Class)cls;
- (id)styleForPart:(NSString*)name;

@end
