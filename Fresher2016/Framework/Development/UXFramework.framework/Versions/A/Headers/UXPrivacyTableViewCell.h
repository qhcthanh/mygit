/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

extern NSString *const kUXPrivateCellIdentifier;

@interface UXPrivacyTableViewCell : UITableViewCell

/**
 *  Set the cell values
 *
 *  @param icon The icon
 *  @param text The description text
 *  @param row  The row number
 */
- (void)setIcon:(NSString *)icon text:(NSString *)text row:(NSInteger)row;

@end
