/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXStyle.h"

@interface UXLinearGradientBorderStyle : UXStyle {
}

@property (nonatomic, retain) UIColor*  color1;
@property (nonatomic, retain) UIColor*  color2;
@property (nonatomic)         CGFloat   location1;
@property (nonatomic)         CGFloat   location2;
@property (nonatomic)         CGFloat   width;

+ (UXLinearGradientBorderStyle*)styleWithColor1:(UIColor*)color1 color2:(UIColor*)color2
                                          width:(CGFloat)width next:(UXStyle*)next;
+ (UXLinearGradientBorderStyle*)styleWithColor1:(UIColor*)color1 location1:(CGFloat)location1
                                         color2:(UIColor*)color2 location2:(CGFloat)location2
                                          width:(CGFloat)width next:(UXStyle*)next;
@end
