/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXPreprocessor.h"

// Methods that are meant to be subclassed.
@interface UXPagingScrollView (Extension)

// Meant to be subclassed. Default implementations are stubs.
/**
 * Called before the page is about to be shown and after its frame has been set.
 *
 * Meant to be subclassed. By default this method does nothing.
 *
 */
- (void)willDisplayPage:(UIView<UXPagingScrollViewPage> *)pageView;

/**
 * Called immediately after the page is removed from the paging scroll view.
 *
 * Meant to be subclassed. By default this method does nothing.
 *
 */
- (void)didRecyclePage:(UIView<UXPagingScrollViewPage> *)pageView;

/**
 * Called immediately after the data source has been queried for its number of
 * pages.
 *
 * Meant to be subclassed. By default this method does nothing.
 *
 */
- (void)didReloadNumberOfPages;

/**
 * Called when the visible page has changed.
 *
 * Meant to be subclassed. By default this method does nothing.
 *
 */
- (void)didChangeCenterPageIndexFrom:(NSInteger)from to:(NSInteger)to;

/**
 * Called when a page needs to be loaded before it is displayed.
 *
 * By default this method asks the data source for the page at the given index.
 * A subclass may chose to modify the page index using a transformation method
 * before calling super.
 *
 */
- (UIView<UXPagingScrollViewPage> *)loadPageAtIndex:(NSInteger)pageIndex;

#pragma mark Accessing Child Views

- (UIScrollView *)scrollView;
- (NSMutableSet *)visiblePages; // Set of UIView<UXPagingScrollViewPage>*

@end

// Methods that are not meant to be subclassed.
@interface UXPagingScrollView (ProtectedMethods)

/**
 * Sets the centerPageIndex ivar without side effects.
 *
 */
- (void)setCenterPageIndexIvar:(NSInteger)centerPageIndex;

/**
 * Recycles the page at the given index.
 *
 */
- (void)recyclePageAtIndex:(NSInteger)pageIndex;

/**
 * Displays the page at the given index.
 *
 */
- (void)displayPageAtIndex:(NSInteger)pageIndex;

/**
 * Returns the page's scrollable dimension.
 *
 * This is the width of the paging scroll view for horizontal scroll views, or
 * the height of the paging scroll view for vertical scroll views.
 *
 */
- (CGFloat)pageScrollableDimension;

/**
 * Updates the frames of all visible pages based on their page indices.
 *
 */
- (void)layoutVisiblePages;

@end

// Deprecated methods formerly used by subclasses.
// This category will be removed on February 28, 2014.
@interface UXPagingScrollView (DeprecatedSubclassingMethods)

// Use -[UXPagingScrollView scrollView] instead.
- (UIScrollView *)pagingScrollView __UX_DEPRECATED_METHOD;

@end
