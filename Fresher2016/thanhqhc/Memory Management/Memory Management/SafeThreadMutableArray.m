//
//  NSMutableCustom2.m
//  Memory Management
//
//  Created by Developer on 5/13/16.
//  Copyright © 2016 VNG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SafeThreadMutableArray.h"

@interface SafeThreadMutableArray() {
    NSMutableArray *data;
    dispatch_queue_t dataQueue;
}

@end

@implementation SafeThreadMutableArray

/**
 *  Khởi tạo SafeThreadMutable khới tạo data = NSMuatble, dataQueue = concurrent queue
 *
 *  @return instance
 */
- (instancetype)init {
    if (self = [super init]) {
        data = [[NSMutableArray alloc] init];
        dataQueue = dispatch_queue_create("SafeArray", DISPATCH_QUEUE_CONCURRENT);
    }
    return self;
}

/**
 *  Block obj và lấy đối tượng thứ index
 *
 *  @param index thứ tự phần tử cần lấy
 *
 *  @return phần tử lấy được
 */
- (id)objectAtIndex:(NSUInteger)index {
    __block id obj;
    dispatch_sync(dataQueue, ^{
        obj = [data objectAtIndex:index];
    });
    return obj;
}

/**
 *  Lấy ra chiều dài của data array sử dụng block reslut và sync để lấy dữ liệu
 *
 *  @return Chiều dài của data array
 */
- (NSUInteger)lengthOfArray {
    __block NSUInteger result = 0;
    dispatch_sync(dataQueue, ^{
        result = [data count];
    });
    return result;
}

/**
 *  Thêm phần tử vào NSMutableArray data với vị trí cho trước sử dụng barrier để ngăn chăn data racing
 *
 *  @param obj   đối tượng cần thêm
 *  @param index thêm vào vị trí
 */
- (void)insertObject:(id)obj atIndex:(NSUInteger)index {
    dispatch_barrier_async(dataQueue, ^{
        [data insertObject:obj atIndex:index];
    });
}

/**
 *  Thêm phần tử vào NSMutableArray data sử dụng barrier để ngăn chăn tranh chấp
 *
 *  @param obj đối tượng cần thêm
 */
- (void)addObject:(id)obj {
    dispatch_barrier_async(dataQueue, ^{
        [data addObject:obj];
    });
}

/**
 *  Xoá một phần tử với index cho trước sử dụng barrier để ngăn chăn tranh chấp
 *
 *  @param index vị trí xoá
 */
- (void)removeObjectAtIndex:(NSUInteger)index {
    dispatch_barrier_async(dataQueue, ^{
        [data removeObjectAtIndex:index];
    });
}

@end


