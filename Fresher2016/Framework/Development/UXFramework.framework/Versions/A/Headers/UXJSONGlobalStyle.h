/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXJSONRuleSet.h"
#import "UXJSONStyleSheet.h"
#import "UXJSONDefaultStyleSheet.h"

// CSS Style helpers

#define UXJSONSTYLESHEET ([[UXJSONDefaultStyleSheet globalJSONDefaultStyleSheet] styleSheet])

#define UXJSON_object(_SELECTOR) \
                ([UXJSONSTYLESHEET objectWithCssSelector:_SELECTOR])

#define UXJSON_color(_SELECTOR, _STATE) \
                ([UXJSONSTYLESHEET colorWithCssSelector:_SELECTOR forState:_STATE])

#define UXJSON_opacity(_SELECTOR, _STATE) \
                ([UXJSONSTYLESHEET opacityWithCssSelector:_SELECTOR forState:_STATE])

#define UXJSON_backgroundColor(_SELECTOR, _STATE) \
                ([UXJSONSTYLESHEET backgroundColorWithCssSelector:_SELECTOR forState:_STATE])

#define UXJSON_borderColor(_SELECTOR, _STATE) \
                ([UXJSONSTYLESHEET borderColorWithCssSelector:_SELECTOR forState:_STATE])

#define UXJSON_backgroundImage(_SELECTOR, _STATE) \
                ([UXJSONSTYLESHEET backgroundImageWithCssSelector:_SELECTOR forState:_STATE])

#define UXJSON_font(_SELECTOR, _STATE) \
					([UXJSONSTYLESHEET fontWithCssSelector:_SELECTOR forState:_STATE])

#define UXJSON_shadowColor(_SELECTOR, _STATE) \
						([UXJSONSTYLESHEET textShadowColorWithCssSelector:_SELECTOR forState:_STATE])

#define UXJSON_shadowOffset(_SELECTOR, _STATE) \
							([UXJSONSTYLESHEET textShadowOffsetWithCssSelector:_SELECTOR forState:_STATE])

#define UXJSON_shadowRadius(_SELECTOR, _STATE) \
							([UXJSONSTYLESHEET textShadowRadiusWithCssSelector:_SELECTOR forState:_STATE])

// _VARNAME must be one of: color, backgroundColor, font, shadowColor, shadowOffset, shadowRadius
#define UXJSONSTATE(_SELECTOR, _VARNAME, _STATE) \
					UXJSON_##_VARNAME(_SELECTOR, _STATE)

#define UXJSONOBJECT(_SELECTOR, _VARNAME) \
                        UXJSON_##_VARNAME(_SELECTOR)
/**
 * Retrieve a Full CSS Rule (UXJSONRuleSet) for specified selector.
 */
#define UXJSONRule(selector) (UXJSONRuleSet*)[UXJSONSTYLESHEET css:selector]

/**
 * Retrieve an value for a property of an Rule Set (UXJSONRuleSet) for specified selector.
 */
#define UXJSON(selector,property) [UXJSONRule(selector) property]
