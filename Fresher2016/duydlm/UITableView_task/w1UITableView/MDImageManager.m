//
//  MDImageManager.m
//  w1UITableView
//
//  Created by manhduydl on 5/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MDImageManager.h"
#import "UIImage+Mask.h"
#import "UIImage+Letters.h"

@interface MDImageManager()

@property (nonatomic) NSMutableDictionary *imageCache;
@property dispatch_queue_t internalSerialQueue;

@end

@implementation MDImageManager

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id instance;
    dispatch_once(&once, ^{
        instance = [self new];
    });
    return instance;
}

- (id)init {
    if ((self = [super init])) {
        self.imageCache = [NSMutableDictionary new];
        self.internalSerialQueue = dispatch_queue_create("com.vng.duydlm", DISPATCH_QUEUE_SERIAL);
    }
    return self;
}

- (UIImage *)loadImageOfContact:(Contact *)contact {
    UIImage *contactImage;
    if (contact.avatarImage != nil) {
        contactImage = [contact.avatarImage maskWithImage:[UIImage imageNamed:@"maskImage"]];
        dispatch_async(self.internalSerialQueue, ^{
            [self.imageCache setValue:contactImage forKey:contact.identifier];
        });
        return contactImage;
    } else {
        contactImage = [UIImage createImageWithLetters:contact.symbolOfName];
        dispatch_async(self.internalSerialQueue, ^{
            [self.imageCache setValue:contactImage forKey:contact.identifier];
        });
        return contactImage;
    }
}

- (UIImage *)getImageFromCache:(Contact *)contact {
    __block UIImage *contactImage;
    dispatch_sync(self.internalSerialQueue, ^{
        contactImage = [self.imageCache objectForKey:contact.identifier];
    });
    if (contactImage != nil) {
        return contactImage;
    }
    return nil;
}

@end
