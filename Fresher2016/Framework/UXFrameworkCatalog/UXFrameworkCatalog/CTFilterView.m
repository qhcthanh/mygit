//
//  CTFilterView.m
//  CameraGPUImage
//
//  Created by BaoNQ on 6/27/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "CTFilterView.h"

@implementation CTFilterView

@synthesize pageIndex = _pageIndex;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithFrame:CGRectZero])) {
        self.reuseIdentifier = reuseIdentifier;
    }
    return self;
}


- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self == nil)
        return nil;
    
    self.contentMode = UIViewContentModeScaleAspectFit;
    self.clipsToBounds = YES;
    
    return self;
}

- (void)mask:(CGRect)maskRect {
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, nil, maskRect);
    maskLayer.path = path;
    self.layer.mask = maskLayer;
}

- (void)updateMask:(CGRect)maskRect newXPosition:(CGFloat)nX {
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    CGMutablePathRef path = CGPathCreateMutable();
    CGRect rect = maskRect;
    
    rect.origin.x = nX;
    CGPathAddRect(path, nil, rect);
    maskLayer.path = path;
    self.layer.mask = maskLayer;
}

- (void)setPageIndex:(NSInteger)pageIndex {
    _pageIndex = pageIndex;
}

- (void)prepareForReuse {
    self.image = nil;
}

- (void)setReuseIdentifier:(NSString *)reuseIdentifier {
    [super setRestorationIdentifier:reuseIdentifier];
}

@end
