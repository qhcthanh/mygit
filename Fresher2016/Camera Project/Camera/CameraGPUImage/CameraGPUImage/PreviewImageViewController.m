//
//  PreviewImageViewController.m
//
//
//  Created by qhcthanh on 6/14/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "PreviewImageViewController.h"
#import "StickerPageViewController.h"
#import "TextEditImageViewController.h"
#import "PickEffectViewController.h"
#import "UXOverlayView.h"
#import "HorizontalImageView.h"
#import "UXImageTransitionView.h"
#import "UXTransitionView.h"
#import "UXTextTransitionView.h"
#import "UXTransitionDetail.h"
#import "UXTextTransitionDetail.h"
#import "UXImageTransitionDetail.h"
#import "SVProgressHUD.h"
#import "UIImage+Utility.h"

#define kWidth [[UIScreen mainScreen] bounds].size.width
#define kHeight [[UIScreen mainScreen] bounds].size.height

@interface PreviewImageViewController () <UIGestureRecognizerDelegate, StickerPageControllerDelegate, TextEditControlDelegate, TouchStickerViewDelegate, PickEffectViewControllerDelegate,UXOverlayViewDelegate, HorizontalImageViewDataSource> {
    GPUImagePicture *stillImage;
    GPUImageOutput<GPUImageInput> *filter;
    GPUImageView *gpuImageView;
    __weak IBOutlet UIView *stickerView;
    __weak IBOutlet UIPageControl *stickerPageControl;
    __weak IBOutlet UIView *textEditView;
    TextEditImageViewController* textEditController ;
    __weak IBOutlet UIButton *trashButton;
    __weak IBOutlet UIButton *hideButton;
    __weak IBOutlet NSLayoutConstraint *heightTrashButtonContrainst;
    __weak IBOutlet UIView *effectView;
    __weak IBOutlet NSLayoutConstraint *widthTrashButtonContrainst;
    
    //Map
    UXOverlayView *stickerOverlayView;
    BOOL isInTrash;
    NSMutableArray *_images;
    HorizontalImageView *sliderView;
}
@end

@implementation PreviewImageViewController

-(void)viewDidLoad {
    
    
    //self.imageview.image = _previewImage;
    self.previewImage = [self.previewImage aspectFill:CGSizeMake(kWidth, kHeight)];
    gpuImageView = [[GPUImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    //gpuImageView.fillMode = kGPUImageFillModePreserveAspectRatioAndFill;
    [self.view addSubview:gpuImageView];
    
    [self.view insertSubview:self.bottomBar aboveSubview:gpuImageView];
    [self.view insertSubview:self.topBar aboveSubview:gpuImageView];
    [self.view insertSubview:stickerView aboveSubview:gpuImageView];
    [self.view insertSubview:textEditView aboveSubview:gpuImageView];
    
    [self.view insertSubview:_saveStateView aboveSubview:gpuImageView];
    [self.view insertSubview:stickerPageControl aboveSubview:stickerView];
    [self.view insertSubview:effectView aboveSubview:gpuImageView];
    
    // Add swipe gsture in camera view
//    UISwipeGestureRecognizer* swipeLeftCameraViewGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeFilterGesture:)];
//    swipeLeftCameraViewGesture.direction = UISwipeGestureRecognizerDirectionLeft;
//    
//    UISwipeGestureRecognizer* swipeRightCameraViewGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeFilterGesture:)];
//    swipeRightCameraViewGesture.direction = UISwipeGestureRecognizerDirectionRight;
//    
//    [gpuImageView addGestureRecognizer: swipeRightCameraViewGesture];
//    [gpuImageView addGestureRecognizer: swipeLeftCameraViewGesture];
    
    // 27 -> 35
    stillImage = [[GPUImagePicture alloc] initWithImage:self.previewImage];
    
    filter = [[[self.filterList objectAtIndex:self.indexFilter] alloc] init];

    [stillImage addTarget:filter];
    [filter addTarget:gpuImageView];
    
    [stillImage processImage];
    
    //Sticker setup
    //35.8 -> 45.5
    [self initScrollViewFilter];
    
    //45.5 -> 46.2
    [self initUXOverlay];
    
}

-(void)initUXOverlay {
//    stickerOverlayView = [[UXOverlayView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    stickerOverlayView = [[UXOverlayView alloc]initWithFrame:gpuImageView.frame];
    [self.view addSubview:stickerOverlayView];

    [self.view bringSubviewToFront:self.topBar];
    [self.view bringSubviewToFront:self.bottomBar];
    [self.view bringSubviewToFront:effectView];
    [self.view bringSubviewToFront:stickerView];
    [self.view bringSubviewToFront:textEditView];
    
    
    stickerOverlayView.touchDelegate = self;
    stickerOverlayView.userInteractionEnabled = YES;
    stickerOverlayView.nextView = sliderView;
    
    for (UXTransitionDetail *item in self.stickers) {
        [stickerOverlayView addSticker:item];
    }
}

-(void)initScrollViewFilter {
    //Scroll view
    sliderView = [[HorizontalImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    sliderView.dataSource = self;
    //Create data
    _images = [[NSMutableArray alloc] init];
    
    // load images
    NSInteger n = MIN(3, _filterList.count);
    // load 3 first image
    // It allocates 10mb in 3 loops.
    for(int i = 0 ;i < n; i++) {
        //35.9 -> 43.8 (for the first loop, and in the other loop, it just rises less than 0.5mb for IFImageFilter allocation)
        //Preview image will be cached in GPUImagePicture.
        GPUImagePicture* tempImage = [[GPUImagePicture alloc] initWithImage:self.previewImage];
        
        IFImageFilter* filterT = [[[self.filterList objectAtIndex:i] alloc] init];
        [tempImage addTarget:filterT];
        [filterT useNextFrameForImageCapture];
        [tempImage processImage];
        
        UIImage *currentFilteredVideoFrame = [filterT imageFromCurrentFramebuffer];
        [_images addObject:currentFilteredVideoFrame];
        NSLog(@"ContentSize: %i", i);
    }
    
    // load 3 to n-1 image
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        for (NSInteger i = n; i < _filterList.count; i++) {
            // Because the image was cached, hence it does not need to allocate new memory for preview image.
            // It just allocate memory for new IFImageFilter.
            GPUImagePicture* tempImage = [[GPUImagePicture alloc] initWithImage:self.previewImage];
            
            IFImageFilter* filterT = [[[self.filterList objectAtIndex:i] alloc] init];
            [tempImage addTarget:filterT];
            [filterT useNextFrameForImageCapture];
            [tempImage processImage];
            
            UIImage *currentFilteredVideoFrame = [filterT imageFromCurrentFramebuffer];
            [_images addObject:currentFilteredVideoFrame];
            
            // update content size
            CGSize oldSize = sliderView.slider.contentSize;
            [sliderView.slider setContentSize:CGSizeMake(oldSize.width + sliderView.slider.frame.size.width, oldSize.height)];
            NSLog(@"ContentSize: %li", (long)i);
        }
    });
    
    [self.view addSubview:sliderView];
    [self.view bringSubviewToFront:sliderView];
    [sliderView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated{
    [stillImage removeTarget:filter];
    [self.gpuStickerView removeAllSticker];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if ( touch.view == _bottomBar || touch.view == _topBar) {
        [self.gpuStickerView touchesBegan:touches withEvent:event];
    }
    //[self.nextResponder touchesBegan:touches withEvent:event];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if ( touch.view == _bottomBar || touch.view == _topBar) {
        [self.gpuStickerView touchesEnded:touches withEvent:event];
    }
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if ( touch.view == _bottomBar || touch.view == _topBar) {
        [self.gpuStickerView touchesMoved:touches withEvent:event];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"%@",segue.identifier);
    if ([segue.identifier isEqualToString: @"StickerPageControllerSegue"]) {
        UIViewController* dest = [segue destinationViewController];
        if ([dest isKindOfClass:[StickerPageViewController class]]) {
            StickerPageViewController* stickPageController = (StickerPageViewController*)dest;
            stickPageController.stickerDelegate = self;
        }
    }
    else if ([segue.identifier isEqualToString: @"TextEditSegue"]) {
        UIViewController* dest = [segue destinationViewController];
        if ([dest isKindOfClass:[TextEditImageViewController class]]) {
            textEditController = (TextEditImageViewController*)dest;
            textEditController.previewVC = self;
            textEditController.textDelegate = self;
        }
    }
    else if ([segue.identifier isEqualToString: @"PickEffectSegue"]) {
        UIViewController* dest = [segue destinationViewController];
        if ([dest isKindOfClass:[PickEffectViewController class]]) {
            PickEffectViewController* effectVC = (PickEffectViewController*)dest;
            effectVC.effectDelegate = self;
            effectVC.filterList = self.filterList;
        }
    }
    
}

//-(void)swipeFilterGesture:(UISwipeGestureRecognizer*)swipeGesture {
//    if (swipeGesture.direction == UISwipeGestureRecognizerDirectionRight) {
//        // swipe right
//        if (self.indexFilter == 0)
//            return;
//        self.indexFilter--;
//        NSLog(@"swipe right");
//    } else {
//        // swipe left
//        if (self.indexFilter == self.filterList.count - 1)
//            return;
//        self.indexFilter++;
//        NSLog(@"swipe left");
//    }
//    [stillImage removeTarget:filter];
//    
//    filter = [[[self.filterList objectAtIndex:self.indexFilter] alloc] init];
//    [filter addTarget:gpuImageView];
//    
//    [stillImage addTarget:filter];
//}

//MARK: Button action
- (IBAction)backAction:(id)sender {
    [self dismissViewControllerAnimated:true completion:^{
        [self.stickers removeAllObjects];
    }];
}

- (IBAction)saveAction:(id)sender {
    [SVProgressHUD showWithStatus:@"Saving..."];
    [self.view setUserInteractionEnabled:false];
    
    UIImage *processImage = [_images objectAtIndex:sliderView.curImageIndex];
    UIImage *finalImage = [self buildImage:processImage];
    
    UIImageWriteToSavedPhotosAlbum(finalImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);

    /*
    [stillImage processImageUpToFilter:filter withCompletionHandler:^(UIImage* processImage){
        [self.view setUserInteractionEnabled:true];
        if (processImage) {
            NSMutableArray *stickers = [[NSMutableArray alloc]init];
            for (UXTransitionDetail *sticker in [stickerOverlayView getStickerList])
                [stickers addObject:sticker];
            
            UIImage *finalImage = [self mergeSticker:stickers ToImage:processImage];
//            UIImage *finalImage = [self buildImage:processImage];
            
            UIImageWriteToSavedPhotosAlbum(processImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
            dispatch_async(dispatch_get_main_queue(), ^{
                _saveStateView.hidden = false;
                [self performSelector:@selector(hiddenSaveStateView) withObject:nil afterDelay:1];
            });
        } else {
            
        }
    }];
     */
}

- (UIImage *)mergeSticker:(NSMutableArray *)stickers ToImage:(UIImage *)inputImage{
    //  + 1 Array chứa vị trí và kích thước tương ứng của các stickers so với màn hình điện thoại.
    UIGraphicsBeginImageContext(inputImage.size);
    //Vẽ hình vừa chụp vào context
    [inputImage drawInRect:CGRectMake(0, 0, inputImage.size.width, inputImage.size.height)];
    
    // Lấy kích thước hình vừa chụp
    float imageWidth = inputImage.size.width;
    float imageHeight = inputImage.size.height;
    
    for (NSInteger index = [stickers count] - 1; index >= 0; index --)
    {
        UIImage *stickerImage = [[stickers objectAtIndex:index] getImage];
        CGRect pos = [[stickers objectAtIndex:index] transitionView].frame;
        [stickerImage drawInRect:CGRectMake(pos.origin.x/kWidth * imageWidth, pos.origin.y/kHeight * imageHeight, pos.size.width/kWidth * imageWidth, pos.size.height/kHeight * imageHeight)];
    }
    
    UIImage* screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return screenshot;

}

- (UIImage*)buildImage:(UIImage*)image
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    
    [image drawAtPoint:CGPointZero];
    
    CGFloat scale = image.size.width / stickerOverlayView.frame.size.width;
//    for (UIView* temp in [stickerOverlayView subviews]) {
//        NSLog(@"%@",[temp description]);
//    }
    CGContextScaleCTM(UIGraphicsGetCurrentContext(), scale, scale);
    [stickerOverlayView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *tmp = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return tmp;
}

- (IBAction)showOverlayAction:(id)sender {
    textEditView.hidden = true;
    effectView.hidden = true;
    [self showEditTextView];
    stickerView.hidden = !stickerView.hidden;
    stickerPageControl.hidden = stickerView.hidden;
    [self showStickerView];
}

- (IBAction)textShowAction:(id)sender {
    stickerView.hidden = true;
     stickerPageControl.hidden = stickerView.hidden;
    effectView.hidden = true;
    self.gpuStickerView.hidden = stickerView.hidden;
    [self showStickerView];
    
    textEditView.hidden = !textEditView.hidden;
    [self showEditTextView];
}

- (IBAction)trashAction:(id)sender {
    
}

- (IBAction)onHideAllSticker:(id)sender {
    stickerView.hidden = true;
    stickerPageControl.hidden = stickerView.hidden;
    textEditView.hidden = true;
    self.topBar.alpha = 0.5;
    self.gpuStickerView.hidden = false;
    self.bottomBar.hidden = false;
    [textEditController endEditText];
    hideButton.hidden = true;
    effectView.hidden = true;
}



-(void)showEditTextView {
    if (textEditView.hidden) {
        self.topBar.alpha = 0.5;
        self.gpuStickerView.hidden = false;
        self.bottomBar.hidden = false;
        [textEditController endEditText];
        hideButton.hidden = true;
        
    } else {
        self.topBar.alpha = 0.8;
        self.bottomBar.hidden = true;
        self.gpuStickerView.hidden = true;
        [textEditController beginEditText];
        hideButton.hidden = false;
    }
}

-(void)showStickerView {
    
    if (stickerView.hidden) {
        hideButton.hidden = true;
        self.topBar.alpha = 0.5;
        self.bottomBar.hidden = false;
        self.gpuStickerView.hidden = false;
    } else {
        self.topBar.alpha = effectView.hidden = true;;
        self.bottomBar.hidden = true;
        self.gpuStickerView.hidden = true;
        hideButton.hidden = false;
    }
}

- (IBAction)showEffectPickerView:(id)sender {
    
    // show
    if (effectView.hidden) {
        stickerView.hidden = true;
        stickerPageControl.hidden = stickerView.hidden;
        textEditView.hidden = true;
        [textEditController endEditText];
        hideButton.hidden = true;
        effectView.hidden = !effectView.hidden;
        self.topBar.alpha = 0.5;
        hideButton.hidden = false;
    } // hide
    else {
        [self onHideAllSticker:nil];
    }
    
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    //Show error alert if image could not be saved
    if (error)
        [[[UIAlertView alloc] initWithTitle:@"Error!" message:@"Image couldn't be saved" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    else {
        [SVProgressHUD dismiss];
        [[[UIAlertView alloc] initWithTitle:@"Completed!" message:@"Image is saved" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        [self.view setUserInteractionEnabled:YES];
//        _saveStateView.hidden = false;
//        [self performSelector:@selector(hiddenSaveStateView) withObject:nil afterDelay:2];
    }
}

-(void)hiddenSaveStateView {
    _saveStateView.hidden = true;
}


//MARK: StickerPageViewController Delegate
-(void)updateIndexViewController:(NSInteger)index {
    stickerPageControl.currentPage = index;
}

-(void)numberOfViewController:(NSInteger)numberPages {
    stickerPageControl.numberOfPages = numberPages;
}

-(void)didSelectItem:(id)item position:(CGPoint)touchPosition {
    if ([item isKindOfClass:[UIImage class]]) {
        [self showOverlayAction:nil];
        UIImage* image = (UIImage*)item;
        
        UXImageTransitionView *newSticker = [[UXImageTransitionView alloc] initWithImage:image atCenterPosition:touchPosition];
        [stickerOverlayView addSticker:[[UXImageTransitionDetail alloc] initWithTransitionView:newSticker]];
    }
}

//MARK: Edit Text Delegate

-(void)didFinishEditText:(UXTextTransitionDetail *)newSticker {
    [stickerOverlayView addSticker:newSticker];
}

//MARK: Touch StickView
-(void)didTouchUpInsideTextSticker:(UXTextTransitionDetail *)textSticker {
    //NSLog(@"%@",textSticker.content);
    textEditController.currentTextSticker = textSticker;
    [self textShowAction:nil];
}

-(void)didTouchEndSticker:(id)sticker position:(CGPoint)position {
    /*
    bool inRect = CGRectContainsPoint(trashButton.frame, position);
    if( inRect ) {
        if ([sticker isKindOfClass:[UXTransitionDetail class]]) {
            UXTransitionDetail* stickerT = (UXTransitionDetail*)sticker;
            [stickerOverlayView removeSticker:stickerT];
        }
    }
    heightTrashButtonContrainst.constant = 0; */
    if (CGRectContainsPoint(trashButton.frame, position)){
        [stickerOverlayView removeSticker:sticker];
        
        heightTrashButtonContrainst.constant = 0;
        widthTrashButtonContrainst.constant = 0;
        isInTrash = NO;
    }
}

-(void)didTouchMoveSticker:(id)sticker position:(CGPoint)position {
    /*
    bool inRect = CGRectContainsPoint(trashButton.frame, position);
    if( inRect ) {
        heightTrashButtonContrainst.constant = 15;
        return;
    }
    heightTrashButtonContrainst.constant = 0; */
    if (CGRectContainsPoint(trashButton.frame, position) && !isInTrash){
        // [sticker setBlurImage];
        ((UXTransitionDetail *)sticker).transitionView.alpha = 0.4;
        
        heightTrashButtonContrainst.constant = 20;
        widthTrashButtonContrainst.constant = 13;
        isInTrash = YES;
    }
    else if (!CGRectContainsPoint(trashButton.frame, position) && isInTrash){
        // [sticker setNormalImage];
        ((UXTransitionDetail *)sticker).transitionView.alpha = 1;
        
       
        heightTrashButtonContrainst.constant = 0;
        widthTrashButtonContrainst.constant = 0;
        isInTrash = NO;
    }
}

//MARK: PickEffectViewControllerDelegate
-(void)didSelectFilter:(GPUImageOutput<GPUImageInput> *)filterT {
    [stillImage removeTarget:filter];
    
    filter = filterT;
    [filter addTarget:gpuImageView];
    
    [stillImage addTarget:filter];
}

//MARK: - HorizontalImageView DataSource
- (NSInteger)numberOfImages:(HorizontalImageView *)horizontalImageView {
    return _filterList.count;
}

- (UIImage *)imageInHorizontalImageView:(HorizontalImageView *)horizontalImageView atIndex:(NSInteger)index {
    return _images[index];
}

@end
