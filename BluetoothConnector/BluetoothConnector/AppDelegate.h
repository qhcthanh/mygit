//
//  AppDelegate.h
//  BluetoothConnector
//
//  Created by qhcthanh on 9/9/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

