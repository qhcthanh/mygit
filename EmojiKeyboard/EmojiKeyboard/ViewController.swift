//
//  ViewController.swift
//  EmojiKeyboard
//
//  Created by Quach Ha Chan Thanh on 8/13/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import UIKit

class ViewController: UIViewController, EmojiViewDelegate {

    @IBOutlet weak var textLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let emojiManager = EmojiManager.shareInstance()
        print(emojiManager)
        
        let emojiView = EmojiView(frame: CGRectMake(0,100,self.view.frame.size.width, 217))
        emojiView.backgroundColor = UIColor(red: 233.0/255, green: 233.0/255, blue: 233.0/255, alpha: 1)
        self.view.addSubview(emojiView)
        emojiView.constrain(.Height, to: 217)
        emojiView.delegate = self
        
        emojiView.constrain(.Left, to: .Left, of: self.view)
        emojiView.constrain(.Right, to: .Right, of: self.view)
        emojiView.constrain(.Bottom, to: .Bottom, of: self.view)
        
        
        //self.view.addSubview(popupButtom)
    }
    
    func onPopupTap(sender: UIButton) {
        showPopupInView(sender,customView: viewPopup, size: CGSizeMake(20, 40))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didSelectEmoji(emoji: String) {
       self.textLabel.text! += emoji
    }
    
}


internal let sizeOfScreen = CGSizeMake(360, 480)

let options = [
    .Type(.Up),
    .CornerRadius(10),
    .SideEdge(20),
    .AnimationIn(0),
    .AnimationOut(0),
    .ArrowSize(CGSizeZero),
    .BlackOverlayColor(UIColor.clearColor()),
    .Color(UIColor.grayColor()),
    .ArrowSize(CGSizeMake(16.0, 10.0))
    ] as [PopoverOption]

let viewPopup = UIView()
func showPopupInView(sender: UIView, customView: UIView, size: CGSize) {
    
    viewPopup.backgroundColor = .grayColor()
    viewPopup.frame.size = sender.frame.size
    
    let popover = Popover(options: options, showHandler: nil, dismissHandler: nil)
    popover.show(viewPopup, fromView: sender)
}






