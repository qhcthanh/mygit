//
//  UIImage+Letters.h
//  w1UITableView
//
//  Created by manhduydl on 6/6/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Letters)

/**
 *  Create circle UIImage with letters in center
 *
 *  @param displayLetters Letters in center of UIImage
 *
 *  @return An UIImage is created
 */
+ (UIImage *)createImageWithLetters:(NSString *)displayLetters;

@end
