//
//  Importer.swift
//  testSuggestion
//
//  Created by manhduydl on 8/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

import Foundation

public class Importer {
    
    var words : Array<String>?
    var internalQueue : dispatch_queue_t
    
    init() {
        words = Array<String>()
        internalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
    }
    /**
     Build Trie tree from file
     
     - parameter fileName:          Name of file to build
     - parameter dispatchQueue:     Queue to dispatch result
     - parameter completionHandler: Closure to handle completion
     */
    func buildTreeFromFile(fileName : String, dispatchQueue : dispatch_queue_t, completionHandler : (rootNode : LetterNode, error : NSError?) -> Void) {

        dispatch_async(internalQueue) { 
            
            self.words = self.arrayFromContentsOfFileWithName(fileName)
            let rootNode = LetterNode.init()
            
            for word : String in self.words! {
                var current = rootNode
                while (word.length != current.level) {
                    let searchKey: String = word.substringToIndex(current.level + 1)
                    current = current.addChild(withKey : searchKey)
                }
                current.isEnd = true
            }
            
            dispatch_async(dispatchQueue, {
                completionHandler(rootNode: rootNode, error: nil)
            })
        }
    }
    
    func buildPhraseTreeFromFile(fileName : String, dispatchQueue : dispatch_queue_t, completionHandler : (rootNode : LetterNode, error : NSError?) -> Void) {
        
        dispatch_async(internalQueue) {

            let phrases = self.arrayFromContentsOfFileWithName(fileName)
            let rootNode = LetterNode.init()
            
            for phrase : String in phrases! {
                var current = rootNode
                var words = phrase.words()
                
                while (words.count != current.level) {
                    let searchKey : String
                    if current.level == 0 {
                        searchKey = words[current.level]
                    } else {
                        searchKey = phrase
                    }
                    current = current.addChild(withKey : searchKey)
                }
                
                current.isEnd = true
            }
            
            dispatch_async(dispatchQueue, {
                completionHandler(rootNode: rootNode, error: nil)
            })
        }
    }
    
    /**
     Parse data from file to array
     
     - parameter fileName: Name of file to parse
     
     - returns: Array contain (word : string) from file
     */
    func arrayFromContentsOfFileWithName(fileName: String) -> [String]? {
        guard let path = NSBundle.mainBundle().pathForResource(fileName, ofType: "txt") else {
            return nil
        }
        
        do {
            let content = try String(contentsOfFile:path, encoding: NSUTF8StringEncoding)
            return content.componentsSeparatedByString("\n")
        } catch _ as NSError {
            return nil
        }
    }
}