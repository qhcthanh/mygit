//
//  TestViewController.m
//  CaptureVideoOverlay
//
//  Created by manhduydl on 8/1/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "TestViewController.h"
#import "GPUImage.h"

@interface TestViewController () {
    GPUImageVideoCamera *videoCamera;
}

@end

@implementation TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    GPUImageView *filterView = [[GPUImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPreset640x480 cameraPosition:AVCaptureDevicePositionBack];
    videoCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
    
    GPUImageOverlayBlendFilter *filter = [[GPUImageOverlayBlendFilter alloc] init];
    GPUImageTransformFilter *transformFilter = [[GPUImageTransformFilter alloc]init];
    
    [videoCamera addTarget:filter];
    [transformFilter addTarget:filter];
    
    // setup overlay image
    UIImage *inputImage = [UIImage imageNamed:@"image copy.png"];
    GPUImagePicture *sourcePicture = [[GPUImagePicture alloc] initWithImage:inputImage smoothlyScaleOutput:YES];
    
    // determine the necessary scaling to keep image at actual size
    CGFloat tx = inputImage.size.width / 480.0;     // 480/640: based on video camera preset
    CGFloat ty = inputImage.size.height / 640.0;
    
    // apply transform to filter
    CGAffineTransform t = CGAffineTransformMakeScale(tx, ty);
    [(GPUImageTransformFilter *)transformFilter setAffineTransform:t];
    
    //
    [sourcePicture addTarget:filter];
    //[sourcePicture addTarget:transformFilter];
    [sourcePicture processImage];
    
    [filter addTarget:filterView];
    [videoCamera startCameraCapture];
    
    [self.view addSubview:filterView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
