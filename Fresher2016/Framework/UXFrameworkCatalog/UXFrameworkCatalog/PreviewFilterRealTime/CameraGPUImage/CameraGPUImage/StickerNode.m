//
//  StickerNode.m
//  CameraGPUImage
//
//  Created by VanDao on 6/18/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "StickerNode.h"

#define kDefaultWidth 150

@implementation StickerNode {
    dispatch_queue_t serialQueue;
    CAShapeLayer *layer;

    GPUImageTransformFilter *transformFilter;
    GPUImageCropFilter *cropFilter;
}

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if (self){
        self.userInteractionEnabled = YES;
        self.multipleTouchEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        self.fillMode = kGPUImageFillModePreserveAspectRatioAndFill;
        serialQueue = dispatch_queue_create("com.daonv.serialqueue.StickerNode", NULL);
    }
    
    return self;
}

- (UIImage *)getImage{
    [cropFilter useNextFrameForImageCapture];
    [texture processImage];
    UIImage *image = [cropFilter imageFromCurrentFramebuffer];
    
    return image;
}

- (NSArray *)getVerticesArray{
    return vertices;
}

- (void)addTarget:(id<GPUImageInput>)newTarget{
    [cropFilter addTarget:newTarget];
}

- (void)replaceStickerWithImage:(UIImage *)newImageSource{
    if (!texture)
        return;
    
    CGFloat deltaX = (newImageSource.size.width - texture.outputImageSize.width);
    CGFloat deltaY = (newImageSource.size.height - texture.outputImageSize.height);
    
    
    texture = [[GPUImagePicture alloc]initWithImage:newImageSource smoothlyScaleOutput:YES removePremultiplication:YES];
}

- (void)addStickerWithImage:(UIImage *)newImageSource{
    texture = [[GPUImagePicture alloc]initWithImage:newImageSource smoothlyScaleOutput:NO removePremultiplication:NO];
    transformFilter = [[GPUImageTransformFilter alloc]init];
    transformFilter.ignoreAspectRatio = NO;
    transformFilter.affineTransform = CGAffineTransformIdentity;
    cropFilter = [[GPUImageCropFilter alloc]init];
    
    [texture addTarget:transformFilter];
    [transformFilter addTarget:cropFilter];
    [cropFilter addTarget:self];

    if (newImageSource.size.height/newImageSource.size.width <= kHeight/kWidth){
        // Crop
         CGFloat width = texture.outputImageSize.width / (texture.outputImageSize.height / kHeight);
        [cropFilter setCropRegion:CGRectMake((width - kWidth)/2 / width, 0, 1 - (width - kWidth) / width, 1)];
        
        // Get vertices
        CGFloat ratio = newImageSource.size.height / newImageSource.size.width;
        
        CGPoint one = CGPointMake((kWidth - kHeight/ratio)/2, 0);
        CGPoint two = CGPointMake((kWidth + kHeight/ratio)/2, 0);
        CGPoint three = CGPointMake((kWidth + kHeight/ratio)/2, kHeight);
        CGPoint four = CGPointMake((kWidth - kHeight/ratio)/2, kHeight);
        vertices = [[NSMutableArray alloc]initWithObjects: [NSValue valueWithCGPoint:one], [NSValue valueWithCGPoint:two], [NSValue valueWithCGPoint:three], [NSValue valueWithCGPoint:four], nil];
    } else {
        // Crop
        CGFloat height = texture.outputImageSize.height / (texture.outputImageSize.width / kWidth);
        [cropFilter setCropRegion:CGRectMake(0, (height - kHeight)/2 /height, 1, 1 - (height - kHeight)/height)];
        
        // Get vertices
        CGFloat ratio = newImageSource.size.width / newImageSource.size.height;
        
        CGPoint one = CGPointMake(0, (kHeight - kWidth/ratio)/2);
        CGPoint two = CGPointMake(kWidth, (kHeight - kWidth/ratio)/2);
        CGPoint three = CGPointMake(kWidth, (kHeight + kWidth/ratio)/2);
        CGPoint four = CGPointMake(0, (kHeight + kWidth/ratio)/2);
        vertices = [[NSMutableArray alloc]initWithObjects: [NSValue valueWithCGPoint:one], [NSValue valueWithCGPoint:two], [NSValue valueWithCGPoint:three], [NSValue valueWithCGPoint:four], nil];
    }

    rotate = 0.0f;
    translate = CGPointMake(0.0f, 0.0f);
    scale = 0.3f;
    [self updateVerticesWithTranslate:translate scale:scale rotate:rotate];
    [self render];
}

- (void)setTransformWithTranslate:(CGPoint)newTranslate scale:(CGFloat)newScale rotate:(CGFloat)newRotate{
    if (texture.outputImageSize.height/texture.outputImageSize.width <= kHeight / kWidth){
        translate.x += 2 * newTranslate.x / (kHeight / (transformFilter.sizeOfFBO.height / transformFilter.sizeOfFBO.width));
        translate.y += 2 * (transformFilter.sizeOfFBO.height / transformFilter.sizeOfFBO.width) * newTranslate.y / kHeight;
    } else {
        translate.x += 2 * newTranslate.x / kWidth;
        translate.y += 2 * newTranslate.y * (transformFilter.sizeOfFBO.height / transformFilter.sizeOfFBO.width) / (transformFilter.sizeOfFBO.height * kWidth / transformFilter.sizeOfFBO.width);
    }
    
    scale *= newScale;
    rotate += newRotate;
    if (rotate > 2 * __PI)
        rotate -= 2 * __PI;
    else if (rotate < -2 * __PI)
        rotate += 2 * __PI;
    
    //Update frame
    [self updateVerticesWithTranslate:newTranslate scale:newScale rotate:newRotate];
}

- (void)updateVerticesWithTranslate:(CGPoint)newTranslate scale:(CGFloat)newScale rotate:(CGFloat)newRotate{
    dispatch_async(serialQueue, ^{
        CGPoint one = [[vertices objectAtIndex:0] CGPointValue];
        CGPoint two = [[vertices objectAtIndex:1] CGPointValue];
        CGPoint three = [[vertices objectAtIndex:2] CGPointValue];
        CGPoint four = [[vertices objectAtIndex:3] CGPointValue];
        
        // Translate
        CGFloat deltaX = newTranslate.x;
        CGFloat deltaY = newTranslate.y;//2 * (transformFilter.sizeOfFBO.height / transformFilter.sizeOfFBO.width) * newTranslate.y;
        one.x += deltaX;
        one.y += deltaY ;
        two.x += deltaX;
        two.y += deltaY ;
        three.x += deltaX;
        three.y += deltaY ;
        four.x += deltaX;
        four.y += deltaY ;
        
        // Scale
        CGPoint midPoint = CGPointMake((one.x + three.x) / 2, (one.y + three.y) / 2);
        one.x -= (one.x - midPoint.x) * (1 - newScale);
        one.y -= (one.y - midPoint.y) * (1 - newScale);
        two.x -= (two.x - midPoint.x) * (1 - newScale);
        two.y -= (two.y - midPoint.y) * (1 - newScale);
        three.x -= (three.x - midPoint.x) * (1 - newScale);
        three.y -= (three.y - midPoint.y) * (1 - newScale);
        four.x -= (four.x - midPoint.x) * (1 - newScale);
        four.y -= (four.y - midPoint.y) * (1 - newScale);
        
         //Rotate
        CGAffineTransform transform = CGAffineTransformTranslate(CGAffineTransformIdentity, midPoint.x, midPoint.y);
        transform = CGAffineTransformRotate(transform, newRotate);
        transform = CGAffineTransformTranslate(transform, -midPoint.x, -midPoint.y);

        one = CGPointApplyAffineTransform(one, transform);
        two = CGPointApplyAffineTransform(two, transform);
        three = CGPointApplyAffineTransform(three, transform);
        four = CGPointApplyAffineTransform(four, transform);
         
        [vertices removeAllObjects];
        vertices = [[NSMutableArray alloc]initWithObjects: [NSValue valueWithCGPoint:one], [NSValue valueWithCGPoint:two], [NSValue valueWithCGPoint:three], [NSValue valueWithCGPoint:four], nil];
        [self drawRectHahaha];
    });
}

- (void)removeLayer{
    [layer removeFromSuperlayer];
}

- (void)drawRectHahaha{
    if (layer)
        [layer removeFromSuperlayer];
    CGPoint one = [[vertices objectAtIndex:0] CGPointValue];
    CGPoint two = [[vertices objectAtIndex:1] CGPointValue];
    CGPoint three = [[vertices objectAtIndex:2] CGPointValue];
    CGPoint four = [[vertices objectAtIndex:3] CGPointValue];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:one];
    [path addLineToPoint:two];
    [path moveToPoint:two];
    [path addLineToPoint: three];
    [path moveToPoint:three];
    [path addLineToPoint:four];
    [path moveToPoint:four];
    [path addLineToPoint:one];
    
    layer = [CAShapeLayer layer];
    layer.path = [path CGPath];
    layer.strokeColor = [[UIColor redColor]CGColor];
    layer.lineWidth = 2.0;
    layer.fillColor = [[UIColor clearColor]CGColor];
    
    [self.superview.layer addSublayer:layer];
}

- (void)render{
    CGAffineTransform transfrom = CGAffineTransformIdentity;
    transfrom = CGAffineTransformTranslate(transfrom, translate.x, translate.y);
    transfrom = CGAffineTransformRotate(transfrom, rotate);
    transfrom = CGAffineTransformScale(transfrom, scale, scale);
    
    transformFilter.affineTransform = transfrom;
    
    [texture processImage];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [super drawRect:rect];
}

@end
