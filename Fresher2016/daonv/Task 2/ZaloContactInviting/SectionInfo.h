//
//  SectionInfo.h
//  ZaloContactInviting
//
//  Created by VanDao on 6/6/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SectionInfo : NSObject

@property(strong, nonatomic) NSString * _Nonnull name; //Name of section
@property(readwrite, nonatomic) NSInteger indexOfSection; //index of section
@property(readwrite, nonatomic) NSInteger indexOfFirstRow;// index of first row in contact list
@property(readwrite, nonatomic) NSInteger count; //number of row in section

+ (SectionInfo * _Nonnull)sectionWithLetter:(NSString * _Nonnull)name indexOfSection:(NSInteger)sectionIndex indexOfFirstRow:(NSInteger)firstRowIndex count:(NSInteger)count;

@end
