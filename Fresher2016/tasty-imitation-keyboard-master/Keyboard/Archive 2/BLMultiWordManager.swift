//
//  BLDatabaseManager.swift
//  KeyboardDatabase
//
//  Created by VanDao on 8/25/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

import Foundation

// MARK: constant variable
let multiwordDataPath: String! = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.CachesDirectory, NSSearchPathDomainMask.UserDomainMask, true).first!.stringByAppendingString("/MultiWordDB.db")
private var internalQueue: dispatch_queue_t = dispatch_queue_create("com.multiworddatabase.internalSerialQueue", DISPATCH_QUEUE_SERIAL)

// MARK: - BLDatabaseManager class

public class BLMultiWordManager: NSObject {
    
    class func insertToDictionary(newWords newWords: [NSString], completion: (error: Bool) -> Void) {
        
        dispatch_async(internalQueue) {
            
            NSDate.startCount()
            
            createTableFromDictionary(table)

            let result: Bool = sharedDatabase().executeCommand(createInsertCommand(newWords))

            print("Multi insert: " + String(NSDate.endCount()))
            
            completion(error: !result)
        }
    }
    
    class func updateToDictionary(newWords newWords: [NSString], completion: (error: Bool) -> Void) {
        dispatch_async(internalQueue) {
            
            NSDate.startCount()

            let result: Bool = sharedDatabase().executeCommand(createUpdateCommand(newWords))
            
            print("Multi update: " + String(NSDate.endCount()))

            completion(error: !result)
        }
    }
    
    class func createIndexForDictionary(withCompletion completion: (error: Bool) -> Void) {
        dispatch_async(internalQueue) {
            
            NSDate.startCount()
            
            let result: Bool = sharedDatabase().executeCommand(createIndexCommand())
            
            print("Multi indices: " + String(NSDate.endCount()))
            
            completion(error: !result)
        }
    }
    
    class func fetchHighestScoreWords(firstWord: String, number: Int, completion: (res: [String]) -> Void) {
        dispatch_async(internalQueue) {
            
            NSDate.startCount()

            let result: YLSQLResult = sharedDatabase().executeQueryCommand(createQueryCommand(firstWord, numbers: number))
          
            print("Multi fetch: " + String(NSDate.endCount()))

            completion(res: result.rowsForColumnName("WORD") as! [String])
        }
    }
}

// MARK: - Private function

private var indices: [String]! = []

private let tableName: String! = "Dictionary"

private let id: String! = "ID"
private let firstWord: String! = "FIRSTWORD"
private let secondWord: String! = "SECONDWORD"
private let score: String! = "SCORE"
private let table: [String : String] = [
    id : "INTEGER",
    firstWord : "TEXT",
    secondWord : "TEXT",
    score : "INTEGER"
]

private func createTableFromDictionary(dictionary: Dictionary<String, String>) -> Bool {
    let sqlString: String! = "CREATE TABLE IF NOT EXISTS \(tableName)(" +
        "\(id) \(table[id]! as String) PRIMARY KEY AUTOINCREMENT," +
        "\(firstWord)      \(table[firstWord]!)    NOT NULL," +
        "\(secondWord)     \(table[secondWord]!)     NOT NULL," +
        "\(score)          \(table[score]!));";
    
    let sqlCommand: YLSQLCommand = YLSQLCommand.init(string: sqlString)
    let result: Bool = sharedDatabase().executeCommand(sqlCommand)
    
    return result
}

private func createQueryCommand(_firstWord: String, numbers: Int) -> YLSQLCommand {
    
    let sqlString = "SELECT (\(firstWord) || ' ' || \(secondWord)) AS WORD FROM \(tableName) WHERE \(firstWord) = '\(_firstWord)' ORDER BY \(score) DESC LIMIT \(numbers)"
    
    return YLSQLCommand.init(string: sqlString)
}

private func createUpdateCommand(words: [NSString]) -> YLSQLCommand {
    
    var sqlString: String! = ""
    
    for index in (0..<words.count) {
        
        let separateWord: [String] = words[index].componentsSeparatedByString(" ")
        sqlString = sqlString + "UPDATE \(tableName) SET \(score) = \(score) + 1 WHERE \(firstWord) = '\(separateWord[0])' AND \(secondWord) = '\(separateWord[1])';"
    }
    
    return YLSQLCommand.init(string: sqlString)
}

private func createIndexCommand() -> YLSQLCommand {
    
    var sqlString: String! = ""
    
    for index in (0..<indices.count) {
        
        sqlString = sqlString + "CREATE INDEX IF NOT EXISTS idx_\(indices[index]) ON \(tableName) WHERE \(firstWord) = '\(indices[index])';"
    }
    
    return YLSQLCommand.init(string: sqlString)
}

private func createInsertCommand(words: [NSString]) -> YLSQLCommand {
    
    var sqlString: String! = "INSERT OR REPLACE INTO \(tableName) (\(firstWord), \(secondWord), \(score)) VALUES "
    
    for index in (0..<words.count) {
        
        let separateWord: [String] = words[index].componentsSeparatedByString(" ")
        sqlString = sqlString +  "('\(separateWord[0])', '\(separateWord[1])', 0)"
        sqlString = sqlString + ((index == words.count - 1) ? ";" : ",")
        
        // create index
        if !indices.contains(separateWord[0]) {
            indices.append(separateWord[0])
        }
    }
    
    return YLSQLCommand.init(string: sqlString)
}

private func sharedDatabase() -> YLSQLDatabase {
    
    var database:YLSQLDatabase?
    
    var tokenOnce: dispatch_once_t = 0
    dispatch_once(&tokenOnce) {
        database = YLSQLDatabase.init(path: multiwordDataPath)
        database!.openDatabaseWithOption(DatabaseOpenOption.init(rawValue: 6)!)
    }
    
    return database!
}