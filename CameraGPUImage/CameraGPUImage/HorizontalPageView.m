//
//  CTSliderView.m
//  CameraGPUImage
//
//  Created by BaoNQ on 6/27/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "HorizontalPageView.h"

@implementation HorizontalPageView {

    NSInteger _movedPage;
    BOOL _movedPageInitialized;
    
    CGPoint _lastContentOffset;
    NSInteger _curImageIndex;
    NSInteger _numberOfPages;
    
    int count;
    BOOL swipedLeftInPreviousSwipe;
    BOOL _preparedForPageView;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self == nil)
        return nil;
    
    count = 0;
    _movedPageInitialized = NO;
    swipedLeftInPreviousSwipe = YES;
    _preparedForPageView = NO;
    
    self.startingIndex = 0;
    self.slider = [[UIScrollView alloc] initWithFrame:frame];
    
    self.slider.delegate = self;
    self.slider.pagingEnabled = true;
    self.slider.bounces = false;
    self.slider.showsHorizontalScrollIndicator = false;
    self.slider.showsVerticalScrollIndicator = false;
    self.slider.layer.zPosition = 1;
    
    [self addSubview:self.slider];
    
    self.pages = [[NSMutableArray alloc] init];
    
    return self;
}

- (void)reloadData {
    
    self.startingIndex = [self.dataSource startAtIndex:self];
    self.numberOfImages = [self.dataSource numberOfImages:self];
    
    _curImageIndex = self.startingIndex;
    _lastContentOffset = CGPointMake(self.startingIndex * self.frame.size.width, 0);
    
    // Load data
    self.slider.contentSize = CGSizeMake(self.frame.size.width * self.numberOfImages, self.frame.size.height);
    
    _numberOfPages = MIN(3, self.numberOfImages);
    // We just use maximum 3 pages to present the image list.
    for (int i = 0; i < _numberOfPages; ++i) {
        PageView *page = [[PageView alloc] initWithFrame:self.frame];
        
        SNFilter *filter = [self.dataSource imageInHorizontalPageView:self atIndex:i];
        page.image = filter.image;
        
        page.layer.zPosition = 0;
        [page updateMask:page.frame newXPosition:[self positionOfPageAtIndex:(i - self.startingIndex)]];
        
        [self.pages addObject:page];
        [self addSubview:page];
    }
    
    for (int i = 0; i < _numberOfImages; ++i) {
        
        SNFilter *filter = [self.dataSource imageInHorizontalPageView:self atIndex:i];

        for (SNSticker *sticker in filter.stickers) {
            NSLog(@"Addsticker");
            sticker.frame = CGRectMake(sticker.frame.origin.x + [self positionOfPageAtIndex:i], sticker.frame.origin.y, sticker.frame.size.width, sticker.frame.size.height);
            NSLog(@"%f, %f, %f, %f", sticker.frame.origin.x, sticker.frame.origin.y, sticker.frame.size.width, sticker.frame.size.height);
            NSLog(@"%@", sticker);
            [self.slider addSubview:sticker];
        }
    }

    //Scroll the view to the starting offset
    [self.slider scrollRectToVisible:CGRectMake([self positionOfPageAtIndex:self.startingIndex], 0, self.frame.size.width, self.frame.size.height) animated:false];
}

- (CGFloat) positionOfPageAtIndex:(NSInteger)index {
    return self.frame.size.width * (float)(index);
}

- (NSInteger) previousPage:(NSInteger)curPage {
    // Get the previous page.
    // Ex: 2 -> 1, 1 -> 0, 0 -> 2
    NSInteger newPage = curPage - 1;
    if (newPage < 0)
        newPage = 2;
    return newPage;
}

//UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    // Update new position for 3 pages depending on current content offset of scroll view.
    for (int i = 0; i < _numberOfPages; ++i) {
        int newXpos = ([self positionOfPageAtIndex:(i)] - scrollView.contentOffset.x);
        newXpos = newXpos % (int)(3 * self.frame.size.width);
        
        if (abs(newXpos) > 2 * self.frame.size.width) {
            newXpos = 3 * self.frame.size.width - abs(newXpos);
        }

        [self.pages[i] updateMask:self.pages[i].frame newXPosition:newXpos];
    }
    
    // If a page has been completely swiped
    if (fabs(_lastContentOffset.x - scrollView.contentOffset.x) >= self.frame.size.width) {

        BOOL flag = NO;
        
        // Swiped to right
        if (_lastContentOffset.x - scrollView.contentOffset.x > 0) {
            
            if (!_movedPageInitialized) {
                _movedPage = 3;
                swipedLeftInPreviousSwipe = NO;
                _movedPageInitialized = YES;
            }
            
            _lastContentOffset.x -= self.frame.size.width;
            _curImageIndex--;
            
            if (swipedLeftInPreviousSwipe) {
                swipedLeftInPreviousSwipe = NO;
            }
            else {
                _movedPage = [self previousPage:_movedPage];
            }
            
        }
        // Swiped to left
        else {
            _lastContentOffset.x += self.frame.size.width;
            
            if (!_movedPageInitialized) {
                _movedPage = -1;
                swipedLeftInPreviousSwipe = YES;
                _movedPageInitialized = YES;
            }
            
            _curImageIndex++;
            flag = YES;
            
            if (!swipedLeftInPreviousSwipe) {
                swipedLeftInPreviousSwipe = YES;
            }
            else {
                _movedPage = (++_movedPage)%3;
            }
        }

        // If a page has been moved.
        if (_movedPage >= 0) {
            NSInteger nextImageIndex = _curImageIndex;
            if (flag)
                nextImageIndex++;
            else
                nextImageIndex--;
            
            // Set new image for this page.
            if (nextImageIndex >= 0 && nextImageIndex < self.numberOfImages) {
                SNFilter *filter = [self.dataSource imageInHorizontalPageView:self atIndex:nextImageIndex];

                self.pages[_movedPage].image = filter.image;
            }
        }
        
        
        if (scrollView.contentOffset.x == [self positionOfPageAtIndex:(0)]) {
            
            for (NSInteger i = 0; i < _numberOfImages - 2; ++i) {
                [scrollView scrollRectToVisible:CGRectMake(scrollView.contentOffset.x + self.frame.size.width, 0, self.frame.size.width, self.frame.size.height) animated:false];
            }
        }
        else if (scrollView.contentOffset.x == [self positionOfPageAtIndex:self.numberOfImages - 1]) {
            
            for (NSInteger i = _numberOfImages - 1; i > 1; --i) {
                [scrollView scrollRectToVisible:CGRectMake(scrollView.contentOffset.x - self.frame.size.width, 0, self.frame.size.width, self.frame.size.height) animated:false];
            }
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {

}


@end
