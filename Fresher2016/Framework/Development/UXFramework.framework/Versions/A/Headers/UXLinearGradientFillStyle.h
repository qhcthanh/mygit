/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXStyle.h"

@interface UXLinearGradientFillStyle : UXStyle {
}

@property (nonatomic, retain) UIColor* color1;
@property (nonatomic, retain) UIColor* color2;

+ (UXLinearGradientFillStyle*)styleWithColor1:(UIColor*)color1 color2:(UIColor*)color2
                                         next:(UXStyle*)next;

@end
