//
//  ContactLoadDetail.h
//  ZaloContactInviting
//
//  Created by VanDao on 5/26/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface ContactDetail : NSObject

@property (strong, nonatomic) NSString * _Nonnull identifier;
@property (strong, nonatomic) NSString * _Nullable givenName;
@property (strong, nonatomic) NSString * _Nullable familyName;
@property (strong, nonatomic) UIImage * _Nullable image;
@property (strong, nonatomic) NSMutableArray<NSString *> * _Nullable phoneNumbers;

/**
 *  Get full name of person by given name + family name
 *
 *  @return NSString contain full name of person
 */
- (NSString * _Nonnull)getFullName;

@end
