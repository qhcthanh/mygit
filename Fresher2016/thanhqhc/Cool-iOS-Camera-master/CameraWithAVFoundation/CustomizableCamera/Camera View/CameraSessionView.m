//
//  CACameraSessionDelegate.h
//
//  Created by Christopher Cohen & Gabriel Alvarado on 1/23/15.
//  Copyright (c) 2015 Gabriel Alvarado. All rights reserved.
//

#import "CameraSessionView.h"



#import "Constants.h"

@interface CameraSessionView () <CaptureSessionManagerDelegate>
{

}

//Temporary/Diagnostic properties
@property (nonatomic, strong) UILabel *ISOLabel, *apertureLabel, *shutterSpeedLabel;

@end

@implementation CameraSessionView

-(void)drawRect:(CGRect)rect {
    if (self) {
        animationInProgress = NO;
        [self setupCaptureManager:RearFacingCamera];
        cameraBeingUsed = RearFacingCamera;
        [self composeInterface];
        
        [[captureManager captureSession] startRunning];
    }
}

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    defaultBarColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    topLeftPosition = CGPointZero;
    bottomLeftPosition = CGPointZero;
    bottomRightPosition = CGPointZero;
    
//    if (self) {
//        animationInProgress = NO;
//        [self setupCaptureManager:RearFacingCamera];
//        cameraBeingUsed = RearFacingCamera;
//        [self composeInterface];
//        
//        [[captureManager captureSession] startRunning];
//    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    return self;
}

#pragma mark - Setup

-(void)setupCaptureManager:(CameraType)camera {
    
    // remove existing input
    AVCaptureInput* currentCameraInput = [self->captureManager.captureSession.inputs objectAtIndex:0];
    [self->captureManager.captureSession removeInput:currentCameraInput];
    
    captureManager = nil;
    
    //Create and configure 'CaptureSessionManager' object
    captureManager = [CaptureSessionManager new];
    
    // indicate that some changes will be made to the session
    [self->captureManager.captureSession beginConfiguration];
    
    if (captureManager) {
        
        //Configure
        [captureManager setDelegate:self];
        [captureManager initiateCaptureSessionForCamera:camera];
        [captureManager addStillImageOutput];
        [captureManager addMovieCameraFileOutput];
        [captureManager addVideoPreviewLayer];
        [self->captureManager.captureSession commitConfiguration];
        
        //Preview Layer setup
        CGRect layerRect = self.layer.bounds;
        [captureManager.previewLayer setBounds:layerRect];
        [captureManager.previewLayer setPosition:CGPointMake(CGRectGetMidX(layerRect),CGRectGetMidY(layerRect))];
        
        //Apply animation effect to the camera's preview layer
        CATransition *applicationLoadViewIn =[CATransition animation];
        [applicationLoadViewIn setDuration:0.6];
        [applicationLoadViewIn setType:kCATransitionReveal];
        [applicationLoadViewIn setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
        [captureManager.previewLayer addAnimation:applicationLoadViewIn forKey:kCATransitionReveal];
        
        //Add to self.view's layer
        [self.layer addSublayer:captureManager.previewLayer];
    }
}

-(void)composeInterface {
    
    //Define adaptable sizing variables for UI elements to the right device family (iPhone or iPad)
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        //Declare the sizing of the UI elements for iPad
        shutterButtonSize = CGSizeMake(self.bounds.size.width * 0.1,
                                       self.bounds.size.width * 0.1);
        topBarSize = CGSizeMake(self.frame.size.width,
                                       self.frame.size.height * 0.06);
        bottomBarSize = CGSizeMake(self.frame.size.width,
                                   self.frame.size.height * 0.2);
        barButtonItemSize = CGSizeMake(UISCREEN_BOUND_SIZE_HEIGHT * 0.04,
                                       UISCREEN_BOUND_SIZE_HEIGHT * 0.04);
    } else
    {
        //Declare the sizing of the UI elements for iPhone
        shutterButtonSize = CGSizeMake(self.bounds.size.width * 0.21, self.bounds.size.width * 0.21);
        topBarSize = CGSizeMake(self.frame.size.width,
                                UISCREEN_BOUND_SIZE_HEIGHT * 0.07);
        bottomBarSize = CGSizeMake(self.frame.size.width,
                                   UISCREEN_BOUND_SIZE_HEIGHT * 0.15);
        barButtonItemSize = CGSizeMake(UISCREEN_BOUND_SIZE_HEIGHT * 0.05,
                                       UISCREEN_BOUND_SIZE_HEIGHT * 0.05);
    }
}

#pragma mark - User Interaction

- (void)onTapShutterButton {
    
    //Animate shutter release
    [self animateShutterRelease];
    
    //Capture image from camera
    [captureManager captureStillImage];
    //[captureManager startRecording];
    
}

- (void)startRecording {
    [captureManager startRecording];
}

-(void)stopRecording {
    [captureManager stopRecording];
}

- (void)onTapFlashButton {
    BOOL enable = !self->captureManager.isTorchEnabled;
    self->captureManager.enableTorch = enable;
}

- (void)onTapToggleButton {
    
    if (cameraBeingUsed == RearFacingCamera) {
        [self setupCaptureManager:FrontFacingCamera];
        cameraBeingUsed = FrontFacingCamera;
        [self composeInterface];
        [[captureManager captureSession] startRunning];
       // _cameraFlash.hidden = YES;
    } else {
        [self setupCaptureManager:RearFacingCamera];
        cameraBeingUsed = RearFacingCamera;
        [self composeInterface];
        [[captureManager captureSession] startRunning];
       // _cameraFlash.hidden = NO;
    }
}

- (void)onTapDismissButton {
    [UIView animateWithDuration:0.3 animations:^{
        self.center = CGPointMake(self.center.x, self.center.y*3);
    } completion:^(BOOL finished) {
        [captureManager stop];
        [self removeFromSuperview];
    }];
    
}

#pragma mark - Animation

- (void)animateShutterRelease {
    
    animationInProgress = YES; //Disables input manager
    
    [UIView animateWithDuration:.1 animations:^{
       // _cameraShutter.transform = CGAffineTransformMakeScale(1.25, 1.25);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:.1 animations:^{
     //       _cameraShutter.transform = CGAffineTransformMakeScale(1, 1);
        } completion:^(BOOL finished) {
            
            animationInProgress = NO; //Enables input manager
        }];
    }];
}

- (void)animateFocusReticuleToPoint:(CGPoint)targetPoint
{
    animationInProgress = YES; //Disables input manager
    
    [focalReticule setCenter:targetPoint];
    focalReticule.alpha = 0.0;
    focalReticule.hidden = NO;
    
    [UIView animateWithDuration:0.4 animations:^{
         focalReticule.alpha = 1.0;
     } completion:^(BOOL finished) {
         [UIView animateWithDuration:0.4 animations:^{
              focalReticule.alpha = 0.0;
          }completion:^(BOOL finished) {
              
              animationInProgress = NO; //Enables input manager
          }];
     }];
}

#pragma mark - Camera Session Manager Delegate Methods

-(void)cameraSessionManagerDidCaptureImage
{
    if (self.delegate)
    {
        if ([self.delegate respondsToSelector:@selector(didCaptureImage:)])
            [self.delegate didCaptureImage:[captureManager stillImage]];
        
        if ([self.delegate respondsToSelector:@selector(didCaptureImageWithData:)])
            [self.delegate didCaptureImageWithData:[captureManager stillImageData]];
    }
}

-(void)cameraSessionManagerFailedToCaptureImage {
}

-(void)cameraSessionManagerDidReportAvailability:(BOOL)deviceAvailability forCameraType:(CameraType)cameraType {
}

-(void)cameraSessionManagerDidReportDeviceStatistics:(CameraStatistics)deviceStatistics {
    // Get type
}

-(void)cameraSessionMaagerDidStartRecording:(NSURL *)outputFileURL {
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(didStartRecordingVideo:)]) {
            [self. delegate didStartRecordingVideo:outputFileURL];
        }
    }
}

-(void)cameraSessionMaagerDidFinsishRecording:(NSURL *)outputFileURL error:(NSError *)error cleanUp:(dispatch_block_t)cleanUpBlock {
    
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(didFinishRecordingVideo:error:cleanUp:)]) {
            [self. delegate didFinishRecordingVideo:outputFileURL error:error cleanUp:cleanUpBlock];
        }
    }
    
    
}

#pragma mark - Helper Methods

- (void)focusAtPoint:(CGPoint)point completionHandler:(void(^)())completionHandler
{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];;
    CGPoint pointOfInterest = CGPointZero;
    CGSize frameSize = self.bounds.size;
    pointOfInterest = CGPointMake(point.y / frameSize.height, 1.f - (point.x / frameSize.width));
    
    if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:AVCaptureFocusModeAutoFocus]) {
        
        //Lock camera for configuration if possible
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            
            if ([device isWhiteBalanceModeSupported:AVCaptureWhiteBalanceModeAutoWhiteBalance]) {
                [device setWhiteBalanceMode:AVCaptureWhiteBalanceModeAutoWhiteBalance];
            }
            
            if ([device isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus]) {
                [device setFocusMode:AVCaptureFocusModeAutoFocus];
                [device setFocusPointOfInterest:pointOfInterest];
            }
            
            if([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure]) {
                [device setExposurePointOfInterest:pointOfInterest];
                [device setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
            }
            
            [device unlockForConfiguration];
            
            completionHandler();
        }
    }
    else { completionHandler(); }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

#pragma mark - API Functions

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
