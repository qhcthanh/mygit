//
//  MDContactImageView.h
//  w1UITableView
//
//  Created by manhduydl on 5/23/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact.h"

@interface MDContactImageView : UIImageView

/**
 *  set image for imageView correspond with contact
 *
 *  @param contact The contact need set image
 *  
 *  Note: perform in global queue
 */
- (void)setImageForContact:(Contact *)contact;

@end
