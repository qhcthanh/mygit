/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

#define kApnsTokenKey @"kApnsTokenKey"

#define kNotificationActionViewText         @"kNotificationActionViewText"
#define kNotificationActionReplyText        @"kNotificationActionReplyText"
#define kNotificationActionVoipAccept       @"kNotificationActionVoipAccept"
#define kNotificationActionVoipDecline      @"kNotificationActionVoipDecline"
#define kNotificationCategoryVoipIdentify   @"kNotificationCategoryVoipIdentify"
#define kNotificationCategoryReplyIndentify @"kNotificationCategoryReplyIndentify"

@class APNSHandler;

@protocol APNSHandlerDelegate <NSObject>
@optional

/**
 *  Will be called only once in an app install
 *
 *  @param handler                Identify handler
 *  @param didPrompt              If YES: user never see the prompt from system before. If NO: user used to make a decision, so no UI was displayed. Good time to display a custom popup to guide user to enable in SETTINGS
 *  @param canDisplayNotification If YES: user already tap on either Don't Allow or Allow button. If NO: user never sees the system Alert View before, it's a good time to display your own custom UI first then call registerNotificationSettings
 */
- (void)APNSHandler:(APNSHandler*)handler didAskForNotificationSettingsWithPrompt:(BOOL)didPrompt canDisplayNotification:(BOOL)canDisplayNotification;

/**
 *  Received device token from Apple
 *
 *  @param handler     Identify handler
 *  @param deviceToken The device token string
 */
- (void)APNSHandler:(APNSHandler*)handler didRegisterForRemoteNotificationsWithDeviceToken:(NSString*)deviceToken;

/**
 *  Probably changed manually by user. Called when app becomes active if there's any change.
 *
 *  @param handler                Identify handler
 *  @param notificationTypes      UIRemoteNotificationType
 *  @param canDisplayNotification If YES: user already tap on either Don't Allow or Allow button. If NO: user never sees the system Alert View before, it's a good time to display your own custom UI first then call registerNotificationSettings
 */
- (void)APNSHandler:(APNSHandler*)handler notificationTypeUpdated:(UIRemoteNotificationType)notificationTypes canDisplayNotification:(BOOL)canDisplayNotification;

/**
 *  Fail to register remote notification because of internet connectivity or denial
 *
 *  @param handler Identify handler
 *  @param error   An error returned
 */
- (void)APNSHandler:(APNSHandler*)handler didFailToRegisterForRemoteNotificationsWithError:(NSError *)error;
@end

@interface APNSHandler : NSObject

+ (APNSHandler*)sharedAPNSHandler;

/**
 *  If YES: it either means you can fire a LOCAL notification; can register REMOTE notification; and can receive and display PUSH notification from server.
 *  If NO: UIUserNotificationTypeNone
 */
@property (nonatomic) BOOL canDisplayNotification;

@property (nonatomic) BOOL alreadyTurnOnNotification;

/**
 *  Return the last deviceToken, can be new or cached since the last app launch
 */
@property (nonatomic, strong) NSString *deviceToken;

/**
 *  Default: YES. If your app doesn't have Push Notification or you want to control when to call, set this flag to NO.
 */
@property (nonatomic) BOOL shouldAutoRegisterRemoteNotification;

@property (nonatomic, weak) id<APNSHandlerDelegate>delegate;

/**
 *  Start listening to significant events
 */
- (void)setup;

/**
 *  Call to UIApplication registerUserNotificationSettings but start a timer (as a trick) to measure whether a system alert was shown
 *
 *  @param notificationSettings UIUserNotificationSettings
 */
- (void)registerUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings;

/**
 *  Call to UIApplication registerForRemoteNotifications
 */
- (void)registerForRemoteNotifications;

/**
 *  Call to UIApplication registerForRemoteNotifications if did not register before
 */
- (void)registerForRemoteNotificationsIfNecessary;

/**
 *  Please put in didRegisterUserNotificationSettings
 *
 *  @param application          UIApplication
 *  @param notificationSettings UIUserNotificationSettings
 */
- (void)handleApplication:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings;

/**
 *  Please put in didRegisterForRemoteNotificationsWithDeviceToken
 *
 *  @param application UIApplication
 *  @param deviceToken NSData
 */

- (void)handleApplication:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;

/**
 *  Please put in didFailToRegisterForRemoteNotificationsWithError
 *
 *  @param application UIApplication
 *  @param error       NSError
 */
- (void)handleApplication:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error;

@end
