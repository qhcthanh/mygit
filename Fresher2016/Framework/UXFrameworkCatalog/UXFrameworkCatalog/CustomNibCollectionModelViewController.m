/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "CustomNibCollectionModelViewController.h"

#import "CustomTextCell.h"

//
// What's going on in this file:
//
// This is a demo of creating a UXCollectionViewDataSource and filling it with a list of objects that
// will be displayed in a collection view.
//
// You will find the following UXFramwork features used:
//

@interface CustomNibCollectionModelViewController ()
@property (nonatomic, strong) UXCollectionViewDataSource* dataSource;
@end

@implementation CustomNibCollectionModelViewController

- (id)initWithCollectionViewLayout:(UICollectionViewLayout *)layout {
  UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
  flowLayout.itemSize = CGSizeMake(200, 200);
  if ((self = [super initWithCollectionViewLayout:flowLayout])) {
    self.titleString = @"Nibs";

    NSArray* collectionContents =
    @[
      [CustomTextCellObject objectWithTitle:@"Title 1"],
      [CustomTextCellObject objectWithTitle:@"Title 2"],
    ];

    _dataSource = [[UXCollectionViewDataSource alloc] initWithListArray:collectionContents
                                                     delegate:(id)[UXCollectionViewCellFactory class]];
  }
  return self;
}

- (id)init {
  return [self initWithCollectionViewLayout:[[UICollectionViewLayout alloc] init]];
}

- (void)viewDidLoad {
  [super viewDidLoad];

  self.collectionView.dataSource = _dataSource;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
  return UXIsSupportedOrientation(toInterfaceOrientation);
}

@end
