//
//  AddContactController.h
//  Project2
//
//  Created by BaoNQ on 6/2/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CTContactDetail.h"
#import "ContactImageCaching.h"

@protocol ContactDelegate <NSObject>

/**
 *  Send new contact to tableview controller
 *
 *  @param newContact New contact
 *  @param newImage   New image
 */
- (void)sendNewContactToTableViewController:(CTContactDetail *)newContact withImage:(UIImage *)newImage;

@end

@interface AddContactController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, weak) id<ContactDelegate>delegate;

@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;

//Actions
- (IBAction)lastNameChanged:(id)sender;
- (IBAction)firstNameChanged:(id)sender;
- (IBAction)phoneChanged:(id)sender;
- (IBAction)doneButtonClicked:(id)sender;
- (IBAction)cancelButtonClicked:(id)sender;

@end
