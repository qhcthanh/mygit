/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

/**
 * Returns the true status bar height when the Overview is active.
 *
 * @ingroup Overview-Tools
 *
 * The Overview swizzles the methods used by NIStatusBarHeight.
 */
CGFloat UXOverviewStatusBarHeight();

/**
 * Swizzles all the necessary methods to get the Overview working.
 *
 * @ingroup Overview-Tools
 */
void UXOverviewSwizzleMethods();

