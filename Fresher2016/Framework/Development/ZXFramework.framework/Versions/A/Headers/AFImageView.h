/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, AFImageCacheType) {
    kAFImageCacheTypeDefault = 0,
    kAFImageCacheTypeAvatar,
    kAFImageCacheTypeTempAvatar,
    kAFImageCacheTypeThumbnail,
    kAFImageCacheTypeLargePhoto
};

#define AF_IMAGE_CACHING_DEFAULT        @"default_"
#define AF_IMAGE_CACHING_AVATAR         @"avatar_"
#define AF_IMAGE_CACHING_TEMPORARY      @"temporary_"
#define AF_IMAGE_CACHING_THUMBNAIL      @"thumbnail_"
#define AF_IMAGE_CACHING_LARGEPHOTO     @"largephoto_"

@interface AFImageView : UIImageView

- (void)unsetImage;
- (NSString*)getPrefix;

/**
 Determine image cache type such as avatar, thumbnail and photo
 */
@property (nonatomic, assign) AFImageCacheType afImageCacheType;
@property (nonatomic, assign) BOOL automaticallyAnimation;
@property (nonatomic, assign) BOOL automaticallyCircleImage;
@property (nonatomic, assign) BOOL hasURLString;

@end
