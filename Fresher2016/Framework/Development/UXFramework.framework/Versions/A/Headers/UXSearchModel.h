/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXNetworkModel.h"

#define kSearchCriteriaPattern @" :;?,.@#%^&*()!~+_=-/\\[]{}<>/|"

@class UXMutableArray;
@interface UXSearchModel : UXNetworkModel {
    UXMutableArray* _searchSet;
}

/**
 * Notify to view after finshing search
 */
@property (nonatomic, assign) BOOL finish;

/**
 * Raw data model that contains search results
 */
@property(nonatomic, strong) UXMutableArray* searchSet;

/**
 * Search time interval
 */
@property(nonatomic, assign) NSTimeInterval timeInterval;

/**
 * Add an entity model
 */
- (void)addSearchObject:(id)object;

/**
 * Remove an entity model
 */
- (void)removeSearchObject:(id)object;

/**
 * Search entity model with text pattern
 * The result will be stored into searchSet object
 */
- (void)fakeSearch:(NSString*)text more:(BOOL)more success:(void(^)(NSMutableArray* responseObject))success failure:(void(^)(NSError* error))failure;

/**
 * Return YES if found an item in dataSet object
 */
- (BOOL)findWithPattern:(NSString*)patern byText:(NSString*)text;

- (double)computeSimScore:(NSArray*)keywordArray withTextArray:(NSArray*)textArray UX_DEPRECATED_ATTRIBUTE("This method is not maintained anymore!");

- (double)caculateByComparisionString:(NSString*)first withString:(NSString*)second
                              atFirst:(NSUInteger)i atSecond:(NSUInteger)j UX_DEPRECATED_ATTRIBUTE("This method is not maintained anymore!");

@end
