/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXStyleDelegate.h"

@class UXStyle;
@class UXLayout;

/**
 * A UIView with an attached style and layout that are used to render and
 * layout the view, respectively. Style and layout are optional.
 */
@interface UXView : UIView <UXStyleDelegate> {
    UXStyle*  _style;
    UXLayout* _layout;
}

@property (nonatomic, retain) UXStyle*  style;
@property (nonatomic, retain) UXLayout* layout;

- (void)drawContent:(CGRect)rect;

@end

