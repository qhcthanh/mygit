/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#if NS_BLOCKS_AVAILABLE
typedef UITableViewCell* (^UXTableViewDataSourceCellForIndexPathBlock)(UITableView* tableView, NSIndexPath* indexPath, id object);
#endif // #if NS_BLOCKS_AVAILABLE

@class UXNetworkModel;
@protocol UXTableViewDataSourceDelegate;

/**
 * Forked table model with previous framework
 */
@protocol UXModelTableViewDataSourceDelegate <UITableViewDataSource, UISearchDisplayDelegate>

@optional

- (NSIndexPath*)tableView:(UITableView*)tableView willUpdateObject:(id)object
              atIndexPath:(NSIndexPath*)indexPath;

- (NSIndexPath*)tableView:(UITableView*)tableView willInsertObject:(id)object
              atIndexPath:(NSIndexPath*)indexPath;

- (NSIndexPath*)tableView:(UITableView*)tableView willRemoveObject:(id)object
              atIndexPath:(NSIndexPath*)indexPath;

- (NSString*)titleForLoading:(BOOL)reloading;

- (UIImage*)imageForEmpty;

- (NSString*)titleForEmpty;

- (NSString*)subtitleForEmpty;

- (UIImage*)imageForError:(NSError*)error;

- (NSString*)titleForError:(NSError*)error;

- (NSString*)subtitleForError:(NSError*)error;

- (NSString*)titleForRetryButton:(NSError*)error;

@end

#pragma mark Sectioned Array Objects

// Classes used when creating UXTableViewDataSources.
@class UXTableViewDataSourceFooter;  // Provides the information for a footer.

typedef NS_ENUM(NSInteger, UXTableViewDataSourceSectionIndex) {
    UXTableViewDataSourceSectionIndexNone, // Displays no section index.
    UXTableViewDataSourceSectionIndexDynamic, // Generates a section index from the first letters of the section titles.
    UXTableViewDataSourceSectionIndexAlphabetical, // Generates an alphabetical section index.
};

/**
 * A non-mutable table view model that complies to the UITableViewDataSource protocol.
 *
 * This model allows you to easily create a data source for a UITableView without having to
 * implement the UITableViewDataSource methods in your UITableViewController.
 *
 * This base class is non-mutable, much like an NSArray. You must initialize this model with
 * the contents when you create it.
 *
 * @ingroup TableViewDataSources
 */
@interface UXTableViewDataSource : NSObject <UXModelTableViewDataSourceDelegate>

#pragma mark Creating Table View Models

// Designated initializer.
/**
 * Initializes a newly allocated static model with the given delegate and empty contents.
 *
 * This method can be used to create an empty model.
 *
 */
- (id)initWithDelegate:(id<UXTableViewDataSourceDelegate>)delegate;

/**
 * Initializes a newly allocated static model with the contents of a list array.
 *
 * A list array is a one-dimensional array that defines a flat list of rows. There will be
 * no sectioning of contents in any way.
 *
 * <h3>Example</h3>
 *
 * @code
 * NSArray* contents =
 * [NSArray arrayWithObjects:
 *  [NSDictionary dictionaryWithObject:@"Row 1" forKey:@"title"],
 *  [NSDictionary dictionaryWithObject:@"Row 2" forKey:@"title"],
 *  [NSDictionary dictionaryWithObject:@"Row 3" forKey:@"title"],
 *  nil];
 * [[UXStaticTableViewModel alloc] initWithListArray:contents delegate:self];
 * @endcode
 *
 */
- (id)initWithListArray:(NSArray *)sectionedArray delegate:(id<UXTableViewDataSourceDelegate>)delegate;
// Each NSString in the array starts a new section. Any other object is a new row (with exception of certain model-specific objects).

/**
 * Initializes a newly allocated static model with the contents of a sectioned array.
 *
 * A sectioned array is a one-dimensional array that defines a list of sections and each
 * section's contents. Each NSString begins a new section and any other object defines a
 * row for the current section.
 *
 * <h3>Example</h3>
 *
 * @code
 * NSArray* contents =
 * [NSArray arrayWithObjects:
 *  @"Section 1",
 *  [NSDictionary dictionaryWithObject:@"Row 1" forKey:@"title"],
 *  [NSDictionary dictionaryWithObject:@"Row 2" forKey:@"title"],
 *  @"Section 2",
 *  // This section is empty.
 *  @"Section 3",
 *  [NSDictionary dictionaryWithObject:@"Row 3" forKey:@"title"],
 *  [UXTableViewDataSourceFooter footerWithTitle:@"Footer"],
 *  nil];
 * [[UXStaticTableViewModel alloc] initWithSectionedArray:contents delegate:self];
 * @endcode
 *
 */
- (id)initWithSectionedArray:(NSArray *)sectionedArray delegate:(id<UXTableViewDataSourceDelegate>)delegate;

#pragma mark Accessing Objects
/**
 * Returns the object at the given index path.
 *
 * If no object exists at the given index path (an invalid index path, for example) then nil
 * will be returned.
 *
 */
- (id)objectAtIndexPath:(NSIndexPath *)indexPath;

// This method is not appropriate for performance critical codepaths.
/**
 * Returns the index path of the given object within the model.
 *
 * If the model does not contain the object then nil will be returned.
 *
 */
- (NSIndexPath *)indexPathForObject:(id)object;

- (void)compileSectionIndex;

#pragma mark Configuration

// Immediately compiles the section index.
/**
 * Configures the model's section index properties.
 *
 * Calling this method will compile the section index depending on the index type chosen.
 *
 * @param sectionIndexType The type of section index to display.
 * @param showsSearch      Whether or not to show the search icon at the top of the index.
 * @param showsSummary     Whether or not to show the summary icon at the bottom of the index.
 */
- (void)setSectionIndexType:(UXTableViewDataSourceSectionIndex)sectionIndexType showsSearch:(BOOL)showsSearch showsSummary:(BOOL)showsSummary;

/**
 * The section index type.
 *
 * You will likely use UXTableViewDataSourceSectionIndexAlphabetical in practice.
 *
 * UXTableViewDataSourceSectionIndexNone by default.
 *
 */
@property (nonatomic, readonly, assign) UXTableViewDataSourceSectionIndex sectionIndexType;

/**
 * Whether or not the search symbol will be shown in the section index.
 *
 * NO by default.
 *
 */
@property (nonatomic, readonly, assign) BOOL sectionIndexShowsSearch;

/**
 * Whether or not the summary symbol will be shown in the section index.
 *
 * NO by default.
 *
 */
@property (nonatomic, readonly, assign) BOOL sectionIndexShowsSummary;

#pragma mark Creating Table View Cells
@property (nonatomic, strong) NSArray* sections; // Array of UXTableViewDataSourceSection
@property (nonatomic, strong) UXNetworkModel* model;
/**
 * A delegate used to fetch table view cells for the data source.
 *
 */
@property (nonatomic, weak) id<UXTableViewDataSourceDelegate> delegate;

#if NS_BLOCKS_AVAILABLE
// If both the delegate and this block are provided, cells returned by this block will be used
// and the delegate will not be called.
/**
 * A block used to create a UITableViewCell for a given object.
 *
 */
@property (nonatomic, copy) UXTableViewDataSourceCellForIndexPathBlock createCellBlock;
#endif // #if NS_BLOCKS_AVAILABLE

@end

/**
 * A protocol for UXTableViewDataSource to fetch rows to be displayed for the table view.
 *
 * @ingroup TableViewDataSources
 */
@protocol UXTableViewDataSourceDelegate <NSObject>

@required

/**
 * Fetches a table view cell at a given index path with a given object.
 *
 * The implementation of this method will generally use object to customize the cell.
 */
- (UITableViewCell *)tableViewCellObject: (UXTableViewDataSource *)dataSource
                        cellForTableView: (UITableView *)tableView
                             atIndexPath: (NSIndexPath *)indexPath
                              withObject: (id)object;

@end

/**
 * An object used in sectioned arrays to denote a section footer title.
 *
 * Meant to be used in a sectioned array for UXTableViewDataSource.
 *
 * <h3>Example</h3>
 *
 * @code
 *  [UXTableViewDataSourceFooter footerWithTitle:@"Footer"]
 * @endcode
 */
@interface UXTableViewDataSourceFooter : NSObject

+ (id)footerWithTitle:(NSString *)title;
- (id)initWithTitle:(NSString *)title;

@property (nonatomic, copy) NSString* title;

@end
