/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#define __UX_DEPRECATED_METHOD __attribute__((deprecated))

/**
 * Force a category to be loaded when an app starts up.
 *
 * Add this macro before each category implementation, so we don't have to use
 * -all_load or -force_load to load object files from static libraries that only contain
 * categories and no classes.
 * See http://developer.apple.com/library/mac/#qa/qa2006/qa1490.html for more info.
 */
#define UX_FIX_CATEGORY_BUG(name) @interface UX_FIX_CATEGORY_BUG_##name : NSObject @end \
@implementation UX_FIX_CATEGORY_BUG_##name @end

/**
 * Creates an opaque UIColor object from a byte-value color definition.
 */
#define UXRGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]

/**
 * Creates a UIColor object from a byte-value color definition and alpha transparency.
 */
#define UXRGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]

#if __has_feature(objc_arc)
    #define UX_RELEASE_SAFELY(__POINTER) { if (__POINTER) { __POINTER = nil; }}
#else
    #define UX_RELEASE_SAFELY(__POINTER) { if (__POINTER) { [__POINTER release]; __POINTER = nil; }}
#endif

#define UX_INVALIDATE_TIMER(__TIMER) { if (__TIMER ) { [__TIMER invalidate]; __TIMER = nil; }}

// Release a CoreFoundation object safely.
#define UX_RELEASE_CF_SAFELY(__REF) { if (nil != (__REF)) { CFRelease(__REF); __REF = nil; } }

// The general purpose console logger.
#if defined(DEBUG)
    #define UX_CONSOLE_LOGGER(arguments, ...)  NSLog(@"%s(%d): " arguments, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define UX_CONSOLE_LOGGER(arguments, ...) {}
#endif

#define UX_SEPERATOR_HEIGHT UXSeparatorHeight()

#define LOCALIZE_STRING(string) [NSBundle getLocalizedString:string withComment:string]
#define UX_LOCALIZE_STRING(string) LOCALIZE_STRING(string)

/**
 * System Versioning Preprocessor Macros
 */
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


