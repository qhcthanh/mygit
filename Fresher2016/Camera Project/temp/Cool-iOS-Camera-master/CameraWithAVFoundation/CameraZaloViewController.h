//
//  CameraZaloViewController.h
//  CameraWithAVFoundation
//
//  Created by qhcthanh on 6/10/16.
//  Copyright © 2016 Gabriel Alvarado. All rights reserved.
//

#import "CameraSessionView.h"

@interface CameraZaloViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *flashButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *cameraRotateButton;
@property (weak, nonatomic) IBOutlet CameraSessionView *cameraView;
@end
