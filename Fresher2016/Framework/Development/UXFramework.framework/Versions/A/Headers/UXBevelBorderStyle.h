/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXStyle.h"

@interface UXBevelBorderStyle : UXStyle {
}

@property (nonatomic, retain) UIColor*  highlight;
@property (nonatomic, retain) UIColor*  shadow;
@property (nonatomic)         CGFloat   width;
@property (nonatomic)         NSInteger lightSource;

+ (UXBevelBorderStyle*)styleWithColor:(UIColor*)color width:(CGFloat)width next:(UXStyle*)next;

+ (UXBevelBorderStyle*)styleWithHighlight:(UIColor*)highlight
                                   shadow:(UIColor*)shadow
                                    width:(CGFloat)width
                              lightSource:(NSInteger)lightSource
                                     next:(UXStyle*)next;

@end
