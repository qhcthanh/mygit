/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */


#import "BadgedLauncherViewObject.h"


@implementation BadgedLauncherViewObject 

- (id)initWithTitle:(NSString *)title image:(UIImage *)image badgeNumber:(NSInteger)badgeNumber {
    if ((self = [super initWithTitle:title image:image])) {
        _badgeNumber = badgeNumber;
    }
    return self;
}

+ (id)objectWithTitle:(NSString *)title image:(UIImage *)image badgeNumber:(NSInteger)badgeNumber {
    return [[self alloc] initWithTitle:title image:image badgeNumber:badgeNumber];
}

- (Class)buttonViewClass {
    return [BadgedLauncherButtonView class];
}

@end

@implementation BadgedLauncherButtonView


- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithReuseIdentifier:reuseIdentifier])) {
        _badgeView = [[UXBadgeView alloc] initWithFrame:CGRectZero];
        _badgeView.backgroundColor = [UIColor clearColor];
        _badgeView.hidden = YES;
        
        // This ensures that the badge will not eat taps.
        _badgeView.userInteractionEnabled = NO;
        
        [self addSubview:_badgeView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.badgeView sizeToFit];
    // Find the image's top right corner.
    CGPoint imageTopRight = CGPointMake(floorf((self.button.frame.size.width
                                                + [self.button imageForState:UIControlStateNormal].size.width)
                                               / 2.f),
                                        floorf((self.button.frame.size.height
                                                - [self.button imageForState:UIControlStateNormal].size.height)
                                               / 2.f));
    
    // Center the badge on the image's top right corner.
    self.badgeView.frame = CGRectMake(floorf(imageTopRight.x - self.badgeView.frame.size.width / 2.f),
                                      floorf(imageTopRight.y - self.badgeView.frame.size.height / 3.f),
                                      self.badgeView.frame.size.width, self.badgeView.frame.size.height);
}

- (void)shouldUpdateViewWithObject:(BadgedLauncherViewObject *)object {
    [super shouldUpdateViewWithObject:object];
    
    NSInteger badgeNumber = UXBoundi(object.badgeNumber, 0, 100);
    if (object.badgeNumber > 0) {
        if (badgeNumber < 100) {
            self.badgeView.text = [NSString stringWithFormat:@"%ld", (long)badgeNumber];
        } else {
            self.badgeView.text = @"99+";
        }
        self.badgeView.hidden = NO;
        
    } else {
        self.badgeView.hidden = YES;
    }
}


@end
