//
//  BanlaKeyJSON.swift
//  NVDTelex
//
//  Created by VanDao on 8/18/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

import Foundation

// MARK: - HEADER 
// MARK: -

// MARK: Typing type

public enum TypingType : String {
    case Telex = "Telex"
    case VNI = "VNI"
}


// MARK: Tone mark type

public enum ToneMarkType: String {
    case HighRising = "highRising"
    case LowFalling = "lowFalling"
    case MidFalling = "midFalling"
    case GlottalizedRising = "glottalizedRising"
    case GlottalizedFalling = "glottalizedFalling"
    case MidLevel = "midLevel"
    case None = ""
}


// MARK: Letter mark type

public enum LetterMarkType: String {
    case Horn = "horn"
    case Breve = "breve"
    case AWithCircumflex = "a-circumflex"
    case OWithCircumflex = "o-circumflex"
    case EWithCircumflex = "e-circumflex"
    case Stroke = "stroke"
    case None = ""
}


// MARK: Available part
let list: String! = "List"

public enum AvailablePart: String {
    case BeginConsonant = "AvailableBeginConsonant"
    case Vowel = "AvailableVowels"
    case EndConsonant = "AvailableEndConsonant"
    case ToneMark = "AvailableToneMark"
}

// MARK: Begin consonant types

public enum BeginConsonantType: String {
    case One = "TypeOne"
    case Two = "TypeTwo"
    case Three = "TypeThree"
    case Four = "TypeFour"
    case Five = "TypeFive"
    case Six = "TypeSix"
    case None = ""
}

// MARK: Vowel types

public enum VowelType: String {
    case One = "TypeOne"
    case Two = "TypeTwo"
    case Three = "TypeThree"      // u, o, uo
    case Four = "TypeFour"      // ie
    case Five = "TypeFive"      // oe
    case Six = "TypeSix"      // ye, uye
    case None = ""
}

// MARK: End consonant types

public enum EndConsonantType: String {
    case One = "TypeOne"    // c, t, p, ch
    case Two = "TypeTwo"    // m, n, ng, nh
    case None = ""
}

// MARK: - Enum attributes lists
let beginConsonantTypes: [BeginConsonantType] = [BeginConsonantType.One, BeginConsonantType.Two, BeginConsonantType.Three, BeginConsonantType.Four, BeginConsonantType.Five, BeginConsonantType.Six, BeginConsonantType.None]

let vowelTypes: [VowelType] = [VowelType.One, VowelType.Two, VowelType.Three, VowelType.Four, VowelType.Five, VowelType.Six, VowelType.None]

let endConsonantTypes: [EndConsonantType] = [EndConsonantType.One, EndConsonantType.Two, EndConsonantType.None]

let toneMarkTypes: [ToneMarkType] = [ToneMarkType.HighRising, ToneMarkType.LowFalling, ToneMarkType.MidFalling, ToneMarkType.GlottalizedRising, ToneMarkType.GlottalizedFalling, ToneMarkType.MidLevel, ToneMarkType.None]

let letterMarkTypes: [LetterMarkType] = [LetterMarkType.Horn, LetterMarkType.Breve, LetterMarkType.AWithCircumflex, LetterMarkType.EWithCircumflex, LetterMarkType.OWithCircumflex, LetterMarkType.Stroke, LetterMarkType.None]



// MARK: - BODY
// MAKR: -

// MARK: - Begin consonants
let beginConsonants:[BeginConsonantType : Dictionary<String, Array<String>>] = [
    BeginConsonantType.One : [
        list : ["b", "d", "đ", "g", "h", "l", "m", "n","p", "r", "s", "t", "v", "x", "z", "ch", "tr", "gh", "th", "ng", "nh", "kh", "ph", "ngh"],
        AvailablePart.Vowel.rawValue : ["a", "e", "eu", "i", "u", "o", "ai", "au", "ao", "ay","eo", "ia", "iu", "ie", "oa", "oe", "ua", "uo", "ui", "uy", "oi", "ieu", "oeo", "uoi", "oay", "oai", "uou", "uya", "uye", ""]
    ],
    BeginConsonantType.Two : [
        list : ["k"],
        AvailablePart.Vowel.rawValue : ["e", "eu", "i", "ia", "ai", "ieu"]
    ],
    BeginConsonantType.Three : [
        list : ["q"],
        AvailablePart.Vowel.rawValue : [],
    ],
    BeginConsonantType.Four : [
        list : ["c"],
        AvailablePart.Vowel.rawValue : ["a", "u", "o", "ai", "au", "ao", "ay", "ua", "uo", "ui", "oi", "uoi"]
    ],
    BeginConsonantType.Five : [
        list : ["gi"],
        AvailablePart.Vowel.rawValue : ["a", "e", "eu", "u", "o", "ai", "au", "ao", "ay", "eo", "oa", "oe", "ua", "uo", "ui", "uy", "oi", "uoi", "oay", "oai", "uou", "uya", "uye"]
    ],
    BeginConsonantType.Six : [
        list : ["qu"],
        AvailablePart.Vowel.rawValue : ["a", "e", "o", "ai", "au", "ao", "ay", "eo"]
    ],
    BeginConsonantType.None : [
        list : [""],
        AvailablePart.Vowel.rawValue : ["a", "e", "eu", "i", "u", "o", "y", "ai", "au", "ao", "ay","eo", "ia", "iu", "ie", "oa", "oe", "ua", "uo", "ui", "uy", "oi", "oeo", "ieu", "uoi", "oay", "oai", "uou", "uya", "uye", "ye", "yeu"]
    ]
]

// MARK: Vowels
let vowels: [VowelType : Dictionary<String, Array<String>>] = [
    VowelType.One : [
        list : ["y", "ai", "au", "ao", "ay", "eo", "eu", "ia", "iu", "ua", "ui" , "uy", "oi", "yeu", "ieu", "uoi", "oay", "oai", "oeo", "uou", "uya"],
        AvailablePart.EndConsonant.rawValue : [""],
    ],
    VowelType.Two : [
        list : ["a", "e", "i", "oa"],
        AvailablePart.EndConsonant.rawValue : ["c", "m", "n", "t", "p", "ng", "ch", "nh", ""]
    ],
    VowelType.Three : [
        list : ["u", "o", "uo"],
        AvailablePart.EndConsonant.rawValue : ["c", "m", "n", "t", "p", "ng", ""]
    ],
    VowelType.Four : [
        list : ["ie"],
        AvailablePart.EndConsonant.rawValue : ["c", "m","n", "t", "ng"],
    ],
    VowelType.Five : [
        list : ["oe"],
        AvailablePart.EndConsonant.rawValue : ["n", "t", ""]
    ],
    VowelType.Six : [
        list : ["ye", "uye"],
        AvailablePart.EndConsonant.rawValue : ["t", "n"]
    ],
    VowelType.None : [
        list : [""],
        AvailablePart.EndConsonant.rawValue : [""]
    ]
]

// MARK: End consonants
let endConsonants: [EndConsonantType : Dictionary<String, Array<String>>] = [
    EndConsonantType.One : [
        list : ["c", "t", "p", "ch"],
        AvailablePart.ToneMark.rawValue : [ToneMarkType.HighRising.rawValue, ToneMarkType.GlottalizedFalling.rawValue, ToneMarkType.None.rawValue]
    ],
    EndConsonantType.Two : [
        list : ["m", "n", "ng", "nh", ""],
        AvailablePart.ToneMark.rawValue  : [ToneMarkType.HighRising.rawValue, ToneMarkType.LowFalling.rawValue, ToneMarkType.MidFalling.rawValue, ToneMarkType.GlottalizedRising.rawValue, ToneMarkType.GlottalizedFalling.rawValue, ToneMarkType.MidLevel.rawValue, ToneMarkType.None.rawValue]
    ],
    EndConsonantType.None : [
        list : ["c", "m", "n", "t", "p", "ch", "ng", "nh", ""],
        AvailablePart.ToneMark.rawValue  : [ToneMarkType.HighRising.rawValue, ToneMarkType.LowFalling.rawValue, ToneMarkType.MidFalling.rawValue, ToneMarkType.GlottalizedRising.rawValue, ToneMarkType.GlottalizedFalling.rawValue, ToneMarkType.MidLevel.rawValue, ToneMarkType.None.rawValue]
    ]
]

// MARK: Tone marks
let toneMarks: [ToneMarkType : Dictionary<String, Array<String>>] = [
    ToneMarkType.HighRising : [
        TypingType.Telex.rawValue : ["s"],
        TypingType.VNI.rawValue : ["1"],
    ],
    ToneMarkType.LowFalling : [
        TypingType.Telex.rawValue : ["f"],
        TypingType.VNI.rawValue : ["2"],
    ],
    ToneMarkType.MidFalling : [
        TypingType.Telex.rawValue : ["r"],
        TypingType.VNI.rawValue : ["3"],
    ],
    ToneMarkType.GlottalizedRising : [
        TypingType.Telex.rawValue : ["x"],
        TypingType.VNI.rawValue : ["4"],
    ],
    ToneMarkType.GlottalizedFalling : [
        TypingType.Telex.rawValue : ["j"],
        TypingType.VNI.rawValue : ["5"],
    ],
    ToneMarkType.MidLevel : [
        TypingType.Telex.rawValue : ["z"],
        TypingType.VNI.rawValue : ["0"],
    ],
    ToneMarkType.None : [
        TypingType.Telex.rawValue : [""],
        TypingType.VNI.rawValue : [""],
    ]
]

// MARK: Letter marks
let letterMarks: [LetterMarkType : Dictionary<String, Array<String>>] = [
    LetterMarkType.Horn : [
        TypingType.Telex.rawValue : ["w"],
        TypingType.VNI.rawValue : ["8"],
        AvailablePart.Vowel.rawValue : ["a", "oa"]
    ],
    LetterMarkType.Breve : [
        TypingType.Telex.rawValue : ["w"],
        TypingType.VNI.rawValue : ["7"],
        AvailablePart.Vowel.rawValue : ["u", "o", "oa", "ui", "uo", "oi", "uoi", "ua"]
    ],
    LetterMarkType.AWithCircumflex : [
        TypingType.Telex.rawValue : ["a"],
        TypingType.VNI.rawValue : ["6"],
        AvailablePart.Vowel.rawValue : ["a", "au", "ay", "ua", "uay"]
    ],
    LetterMarkType.EWithCircumflex : [
        TypingType.Telex.rawValue : ["e"],
        TypingType.VNI.rawValue : ["6"],
        AvailablePart.Vowel.rawValue : ["e", "eu", "ye", "ie","ue","yeu", "ieu", "uye"]
    ],
    LetterMarkType.OWithCircumflex : [
        TypingType.Telex.rawValue : ["o"],
        TypingType.VNI.rawValue : ["6"],
        AvailablePart.Vowel.rawValue : ["o", "uo", "oi", "uoi"]
    ],
    LetterMarkType.Stroke: [
        TypingType.Telex.rawValue : ["d"],
        TypingType.VNI.rawValue : ["9"],
        AvailablePart.Vowel.rawValue : [],
        AvailablePart.BeginConsonant.rawValue : ["d"]
    ],
    LetterMarkType.None : [
        TypingType.Telex.rawValue : [""],
        TypingType.VNI.rawValue : [""],
        AvailablePart.Vowel.rawValue : ["a", "e", "i", "u", "o", "y", "ai", "au", "ao", "ay", "eo", "eu", "ia", "iu", "ua", "ui", "uy", "oa", "oe", "oeo", "oi", "oay", "oai", "oeo", "uya", "uye", "yeu"]
    ]
]

// MARK: - Word transformation table
let wordTransformTable: [String : Dictionary<String, String>] = [
    ToneMarkType.HighRising.rawValue: [
        "a" : "á",
        "ă" : "ắ",
        "â" : "ấ",
        "e" : "é",
        "ê" : "ế",
        "i" : "í",
        "o" : "ó",
        "ô" : "ố",
        "ơ" : "ớ",
        "u" : "ú",
        "ư" : "ứ",
        "y" : "ý",
        "A" : "Á",
        "Ă" : "Ắ",
        "Â" : "Ấ",
        "E" : "É",
        "Ê" : "Ế",
        "I" : "Í",
        "O" : "Ó",
        "Ô" : "Ố",
        "Ơ" : "Ớ",
        "U" : "Ú",
        "Ư" : "Ứ",
        "Y" : "Ý"
    ],
    ToneMarkType.LowFalling.rawValue: [
        "a" : "à",
        "ă" : "ằ",
        "â" : "ầ",
        "e" : "è",
        "ê" : "ề",
        "i" : "ì",
        "o" : "ò",
        "ô" : "ồ",
        "ơ" : "ờ",
        "u" : "ù",
        "ư" : "ừ",
        "y" : "ỳ",
        "A" : "À",
        "Ă" : "Ằ",
        "Â" : "Ầ",
        "E" : "È",
        "Ê" : "Ề",
        "I" : "Ì",
        "O" : "Ò",
        "Ô" : "Ồ",
        "Ơ" : "Ờ",
        "U" : "Ù",
        "Ư" : "Ừ",
        "Y" : "Ỳ"
    ],
    ToneMarkType.MidFalling.rawValue: [
        "a" : "ả",
        "ă" : "ẳ",
        "â" : "ẩ",
        "e" : "ẻ",
        "ê" : "ể",
        "i" : "ỉ",
        "o" : "ỏ",
        "ô" : "ổ",
        "ơ" : "ở",
        "u" : "ủ",
        "ư" : "ử",
        "y" : "ỷ",
        "A" : "Ả",
        "Ă" : "Ẳ",
        "Â" : "Ẩ",
        "E" : "Ẻ",
        "Ê" : "Ể",
        "I" : "Ỉ",
        "O" : "Ỏ",
        "Ô" : "Ổ",
        "Ơ" : "Ở",
        "U" : "Ủ",
        "Ư" : "Ử",
        "Y" : "Ỷ"
    ],
    ToneMarkType.GlottalizedRising.rawValue: [
        "a" : "ã",
        "ă" : "ẵ",
        "â" : "ẫ",
        "e" : "ẽ",
        "ê" : "ễ",
        "i" : "ĩ",
        "o" : "õ",
        "ô" : "ỗ",
        "ơ" : "ỡ",
        "u" : "ũ",
        "ư" : "ữ",
        "y" : "ỹ",
        "A" : "Ã",
        "Ă" : "Ẵ",
        "Â" : "Ẫ",
        "E" : "Ẽ",
        "Ê" : "Ễ",
        "I" : "Ĩ",
        "O" : "Õ",
        "Ô" : "Ỗ",
        "Ơ" : "Ỡ",
        "U" : "Ũ",
        "Ư" : "Ữ",
        "Y" : "Ỹ"
    ],
    ToneMarkType.GlottalizedFalling.rawValue: [
        "a" : "ạ",
        "ă" : "ặ",
        "â" : "ậ",
        "e" : "ẹ",
        "ê" : "ệ",
        "i" : "ị",
        "o" : "ọ",
        "ô" : "ộ",
        "ơ" : "ợ",
        "u" : "ụ",
        "ư" : "ự",
        "y" : "ỵ",
        "A" : "Ạ",
        "Ă" : "Ặ",
        "Â" : "Ậ",
        "E" : "Ẹ",
        "Ê" : "Ệ",
        "I" : "Ị",
        "O" : "Ọ",
        "Ô" : "Ộ",
        "Ơ" : "Ợ",
        "U" : "Ụ",
        "Ư" : "Ự",
        "Y" : "Ỵ"
    ],
    ToneMarkType.MidLevel.rawValue: [
        "a" : "a",
        "ă" : "ă",
        "â" : "â",
        "e" : "e",
        "ê" : "ê",
        "i" : "i",
        "o" : "o",
        "ô" : "ô",
        "ơ" : "ơ",
        "u" : "u",
        "ư" : "ư",
        "y" : "y",
        "A" : "A",
        "Ă" : "Ă",
        "Â" : "Â",
        "E" : "E",
        "Ê" : "Ê",
        "I" : "I",
        "O" : "O",
        "Ô" : "Ô",
        "Ơ" : "Ơ",
        "U" : "U",
        "Ư" : "Ư",
        "Y" : "Y"
    ],
    LetterMarkType.Horn.rawValue: [
        "a" : "ă",
        "A" : "Ă"
    ],
    LetterMarkType.Breve.rawValue: [
        "u" : "ư",
        "o" : "ơ",
        "U" : "Ư",
        "O" : "Ơ",
    ],
    LetterMarkType.AWithCircumflex.rawValue: [
        "a" : "â",
        "A" : "Â",
    ],
    LetterMarkType.EWithCircumflex.rawValue: [
        "e" : "ê",
        "E" : "Ê",
    ],
    LetterMarkType.OWithCircumflex.rawValue: [
        "o" : "ô",
        "O" : "Ô",
    ],
    LetterMarkType.Stroke.rawValue: [
        "d" : "đ",
        "D" : "Đ"
    ],
    "": [:]
]


// MARK: HashTable map Vietnamese to rawWord

let VNsTransformTable : [String : [String : String]] = [
    "đ": [
        TypingType.Telex.rawValue : "dd",
        TypingType.VNI.rawValue : "d9"
    ],
    
    //a
    "á": [
        TypingType.Telex.rawValue : "as",
        TypingType.VNI.rawValue : "a1"
    ],
    "à": [
        TypingType.Telex.rawValue : "af",
        TypingType.VNI.rawValue : "a2"
    ],
    "ả": [
        TypingType.Telex.rawValue : "ar",
        TypingType.VNI.rawValue : "a3"
    ],
    "ã": [
        TypingType.Telex.rawValue : "ax",
        TypingType.VNI.rawValue : "a4"
    ],
    "ạ": [
        TypingType.Telex.rawValue : "aj",
        TypingType.VNI.rawValue : "a5"
    ],
    // ă
    "ă": [
        TypingType.Telex.rawValue : "aw",
        TypingType.VNI.rawValue : "a8"
    ],
    "ắ": [
        TypingType.Telex.rawValue : "aws",
        TypingType.VNI.rawValue : "a81"
    ],
    "ằ": [
        TypingType.Telex.rawValue : "awf",
        TypingType.VNI.rawValue : "a82"
    ],
    "ẳ": [
        TypingType.Telex.rawValue : "awr",
        TypingType.VNI.rawValue : "a83"
    ],
    "ẵ": [
        TypingType.Telex.rawValue : "awx",
        TypingType.VNI.rawValue : "a84"
    ],
    "ặ": [
        TypingType.Telex.rawValue : "awj",
        TypingType.VNI.rawValue : "a85"
    ],
    //â
    "â": [
        TypingType.Telex.rawValue : "aa",
        TypingType.VNI.rawValue : "a6"
    ],
    "ấ": [
        TypingType.Telex.rawValue : "aas",
        TypingType.VNI.rawValue : "a61"
    ],
    "ầ": [
        TypingType.Telex.rawValue : "aaf",
        TypingType.VNI.rawValue : "a62"
    ],
    "ẩ": [
        TypingType.Telex.rawValue : "aar",
        TypingType.VNI.rawValue : "a63"
    ],
    "ẫ": [
        TypingType.Telex.rawValue : "aax",
        TypingType.VNI.rawValue : "a64"
    ],
    "ậ": [
        TypingType.Telex.rawValue : "aaj",
        TypingType.VNI.rawValue : "a65"
    ],
    // ê
    "ê": [
        TypingType.Telex.rawValue : "ee",
        TypingType.VNI.rawValue : "e6"
    ],
    "ế": [
        TypingType.Telex.rawValue : "ees",
        TypingType.VNI.rawValue : "e61"
    ],
    "ề": [
        TypingType.Telex.rawValue : "eef",
        TypingType.VNI.rawValue : "e62"
    ],
    "ể": [
        TypingType.Telex.rawValue : "eer",
        TypingType.VNI.rawValue : "e63"
    ],
    "ễ": [
        TypingType.Telex.rawValue : "eex",
        TypingType.VNI.rawValue : "e64"
    ],
    "ệ": [
        TypingType.Telex.rawValue : "eej",
        TypingType.VNI.rawValue : "e65"
    ],
    // e
    "é": [
        TypingType.Telex.rawValue : "es",
        TypingType.VNI.rawValue : "e1"
    ],
    "è": [
        TypingType.Telex.rawValue : "ef",
        TypingType.VNI.rawValue : "e2"
    ],
    "ẻ": [
        TypingType.Telex.rawValue : "er",
        TypingType.VNI.rawValue : "e3"
    ],
    "ẽ": [
        TypingType.Telex.rawValue : "ex",
        TypingType.VNI.rawValue : "e4"
    ],
    "ẹ": [
        TypingType.Telex.rawValue : "ej",
        TypingType.VNI.rawValue : "e5"
    ],
    // u
    "ú": [
        TypingType.Telex.rawValue : "us",
        TypingType.VNI.rawValue : "u1"
    ],
    "ù": [
        TypingType.Telex.rawValue : "uf",
        TypingType.VNI.rawValue : "u2"
    ],
    "ủ": [
        TypingType.Telex.rawValue : "ur",
        TypingType.VNI.rawValue : "u3"
    ],
    "ũ": [
        TypingType.Telex.rawValue : "ux",
        TypingType.VNI.rawValue : "u4"
    ],
    "ụ": [
        TypingType.Telex.rawValue : "uj",
        TypingType.VNI.rawValue : "u5"
    ],
    // ư
    "ư": [
        TypingType.Telex.rawValue : "uw",
        TypingType.VNI.rawValue : "u7"
    ],
    "ứ": [
        TypingType.Telex.rawValue : "uws",
        TypingType.VNI.rawValue : "u71"
    ],
    "ừ": [
        TypingType.Telex.rawValue : "uwf",
        TypingType.VNI.rawValue : "u72"
    ],
    "ử": [
        TypingType.Telex.rawValue : "uwr",
        TypingType.VNI.rawValue : "u73"
    ],
    "ữ": [
        TypingType.Telex.rawValue : "uwx",
        TypingType.VNI.rawValue : "u74"
    ],
    "ự": [
        TypingType.Telex.rawValue : "uwj",
        TypingType.VNI.rawValue : "u75"
    ],
    // i
    "í": [
        TypingType.Telex.rawValue : "is",
        TypingType.VNI.rawValue : "i1"
    ],
    "ì": [
        TypingType.Telex.rawValue : "if",
        TypingType.VNI.rawValue : "i2"
    ],
    "ỉ": [
        TypingType.Telex.rawValue : "ir",
        TypingType.VNI.rawValue : "i3"
    ],
    "ĩ": [
        TypingType.Telex.rawValue : "ix",
        TypingType.VNI.rawValue : "i4"
    ],
    "ị": [
        TypingType.Telex.rawValue : "ij",
        TypingType.VNI.rawValue : "i5"
    ],
    //o
    "ó": [
        TypingType.Telex.rawValue : "os",
        TypingType.VNI.rawValue : "o1"
    ],
    "ò": [
        TypingType.Telex.rawValue : "of",
        TypingType.VNI.rawValue : "o2"
    ],
    "ỏ": [
        TypingType.Telex.rawValue : "or",
        TypingType.VNI.rawValue : "o3"
    ],
    "õ": [
        TypingType.Telex.rawValue : "ox",
        TypingType.VNI.rawValue : "o4"
    ],
    "ọ": [
        TypingType.Telex.rawValue : "oj",
        TypingType.VNI.rawValue : "o5"
    ],
    //ô
    "ô": [
        TypingType.Telex.rawValue : "oo",
        TypingType.VNI.rawValue : "o6"
    ],
    "ố": [
        TypingType.Telex.rawValue : "oos",
        TypingType.VNI.rawValue : "o61"
    ],
    "ồ": [
        TypingType.Telex.rawValue : "oof",
        TypingType.VNI.rawValue : "o62"
    ],
    "ổ": [
        TypingType.Telex.rawValue : "oor",
        TypingType.VNI.rawValue : "o63"
    ],
    "ỗ": [
        TypingType.Telex.rawValue : "oox",
        TypingType.VNI.rawValue : "o64"
    ],
    "ộ": [
        TypingType.Telex.rawValue : "ooj",
        TypingType.VNI.rawValue : "o65"
    ],
    
    //ơ
    "ơ": [
        TypingType.Telex.rawValue : "ow",
        TypingType.VNI.rawValue : "o7"
    ],
    "ớ": [
        TypingType.Telex.rawValue : "ows",
        TypingType.VNI.rawValue : "o61"
    ],
    "ờ": [
        TypingType.Telex.rawValue : "owf",
        TypingType.VNI.rawValue : "o62"
    ],
    "ở": [
        TypingType.Telex.rawValue : "owr",
        TypingType.VNI.rawValue : "o63"
    ],
    "ỡ": [
        TypingType.Telex.rawValue : "owx",
        TypingType.VNI.rawValue : "o64"
    ],
    "ợ": [
        TypingType.Telex.rawValue : "owj",
        TypingType.VNI.rawValue : "o65"
    ],
    
    //y
    "ý": [
        TypingType.Telex.rawValue : "ys",
        TypingType.VNI.rawValue : "y1"
    ],
    "ỳ": [
        TypingType.Telex.rawValue : "yf",
        TypingType.VNI.rawValue : "y2"
    ],
    "ỷ": [
        TypingType.Telex.rawValue : "yr",
        TypingType.VNI.rawValue : "y3"
    ],
    "ỹ": [
        TypingType.Telex.rawValue : "yx",
        TypingType.VNI.rawValue : "y4"
    ],
    "ỵ": [
        TypingType.Telex.rawValue : "yj",
        TypingType.VNI.rawValue : "y5"
    ],
]

