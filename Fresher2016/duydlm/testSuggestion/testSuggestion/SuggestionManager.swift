//
//  SuggestionManager.swift
//  testSuggestion
//
//  Created by manhduydl on 8/22/16.
//  Copyright © 2016 admin. All rights reserved.
//

import Foundation

public class SuggestionManager {
    
    // Singleton intance
    static let shareIntance = SuggestionManager()
    
    // Internal Concurent Queue
    var internalConcurentQueue : dispatch_queue_t
    var internalSerialQueue : dispatch_queue_t
    
    // Root node of trees
    var rootNodeOfLetterTree : LetterNode
    var rootNodeOfRelativeLetterTree : LetterNode
    var rootNodeOfPhraseTree : LetterNode
    var rootNodeOfCommonPhraseTree : LetterNode
    
    init() {
        internalConcurentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
        internalSerialQueue = dispatch_queue_create("com.vng.banlakey", DISPATCH_QUEUE_SERIAL)
        rootNodeOfLetterTree = LetterNode.init()
        rootNodeOfRelativeLetterTree = LetterNode.init()
        rootNodeOfPhraseTree = LetterNode.init()
        rootNodeOfCommonPhraseTree = LetterNode.init()
    }
    
// MARK: - Setup all tree
    
    /**
     Build trees to set up suggestion function
     
     - parameter userDefaultKey:    Key of NSUserDefault to get database
     - parameter dispatchQueue:     Queue to dispatch when completion
     - parameter completionHandler: Closure to perform when completion
     */
    func setup(userDefaultKey : String, dispatchQueue : dispatch_queue_t, completionHandler : (rootNodeDictionary : Dictionary<String,LetterNode>, error : NSError?) -> Void) {
        
        dispatch_async(internalConcurentQueue) {
            let defaults = NSUserDefaults.init(suiteName: userDefaultKey)
            
            let letterJsonString = defaults?.stringForKey(Constants.letterJsonStringKey)
            let relativeLetterJsonString = defaults?.stringForKey(Constants.relativeLetterJsonStringKey)
            let phraseJsonString = defaults?.stringForKey(Constants.phraseJsonStringKey)
            
            self.rootNodeOfLetterTree = self.buildLetterTreeFromJsonString(letterJsonString!)
            self.rootNodeOfRelativeLetterTree = self.buildRelativeLetterTreeFromJsonString(relativeLetterJsonString!)
            self.rootNodeOfPhraseTree = self.buildPhraseTreeFromJsonString(phraseJsonString!)
            
            //self.optimizePhraseTree()
            
            let rootNodeDictionary : [String:LetterNode] = [
                Constants.letterKey : self.rootNodeOfLetterTree,
                Constants.relativeLetterKey : self.rootNodeOfRelativeLetterTree,
                Constants.phraseKey : self.rootNodeOfPhraseTree
            ]
            
            dispatch_async(dispatchQueue, { 
                completionHandler(rootNodeDictionary : rootNodeDictionary, error : nil)
            })
        }
    }

// MARK: - Database method
    
    /**
     Save trees to nsuserdefault as jsonString
     
     - parameter defaultKey:        key to determine NSUserDefaul
     - parameter dispatchQueue:     queue to dispatch when completion
     - parameter completionHandler: closure to perform when completion
     */
    func updateDatabase(withUserDefaultKey defaultKey : String, dispatchQueue : dispatch_queue_t, completionHandler : (NSUserDefaults, NSError?) -> Void) {
        
        dispatch_async(internalConcurentQueue) {
            let defaults = NSUserDefaults.init(suiteName: defaultKey)
            
            let letterJsonString = self.rootNodeOfLetterTree.toJsonString()
            let relativeLetterJsonString = self.rootNodeOfRelativeLetterTree.toJsonString()
            let phraseJsonString = self.rootNodeOfPhraseTree.toJsonString()
            
            //print(self.rootNodeOfPhraseTree.childrentNode.count)
            
            defaults?.setObject(letterJsonString, forKey: Constants.letterJsonStringKey)
            defaults?.setObject(relativeLetterJsonString, forKey: Constants.relativeLetterJsonStringKey)
            defaults?.setObject(phraseJsonString, forKey: Constants.phraseJsonStringKey)
            
            dispatch_async(dispatchQueue, { 
                completionHandler(defaults!, nil)
            })
        }
    }
    
// MARK: - Build Tree
    
    /**
     Build Letter tree from json string in nsuserdefaul of container app and dispatch to dispatchQueue with completionHandler closure
     
     - parameter letterJsonString:  json string to build tree
     - parameter dispatchQueue:     queue to dispatch result
     - parameter completionHandler: closure to perform when tree is builded
     */
    func buildLetterTreeFromJsonString(letterJsonString : String, dispatchQueue : dispatch_queue_t, completionHandler : (rootNode : LetterNode, error : NSError?) -> Void) {
        
        dispatch_async(internalConcurentQueue) {
            let rootNode = self.buildLetterTreeFromJsonString(letterJsonString)
            
            dispatch_async(dispatchQueue, {
                completionHandler(rootNode: rootNode, error: nil)
            })
        }
    }
    
    /**
     Build Letter tree from json string in syncronize
     
     - parameter letterJsonString: Json string to build tree
     
     - returns: Return rootNode of tree
     */
    func buildLetterTreeFromJsonString(letterJsonString : String) -> LetterNode {
        
        let words = self.convertStringToDictionary(letterJsonString)
        let rootNode = LetterNode.init()
        
        for word : String in (words?.keys)! {
            var current = rootNode
            while (word.length != current.level) {
                let searchKey: String = word.substringToIndex(current.level + 1)
                current = current.addChild(withKey : searchKey)
            }
            
            current.isEnd = true
            current.count = words![word]?.valueForKey("count") as! Int
            current.time = words![word]?.valueForKey("time") as! String
        }
        
        
        print("build letter tree completed", rootNode.getAllEndNode().count)
        return rootNode
    }
    
    /**
     Build Phrase tree from json string in nsuserdefaul of container app
     
     - parameter letterJsonString:  json string to build tree
     - parameter dispatchQueue:     queue to dispatch result
     - parameter completionHandler: closure to perform when tree is builded
     */
    func buildPhraseTreeFromJsonString(phraseJsonString : String, dispatchQueue : dispatch_queue_t, completionHandler : (rootNode : LetterNode, error : NSError?) -> Void) {
        
        dispatch_async(internalConcurentQueue) {
            let rootNode = self.buildPhraseTreeFromJsonString(phraseJsonString)
            
            dispatch_async(dispatchQueue, {
                completionHandler(rootNode: rootNode, error: nil)
            })
        }
    }
    
    /**
     Build phrase tree from json String in syncronize
     
     - parameter phraseJsonString: Json string to build tree
     
     - returns: Return rootNode of tree
     */
    func buildPhraseTreeFromJsonString(phraseJsonString : String) -> LetterNode {
        
        let phrases = self.convertStringToDictionary(phraseJsonString)
        let rootNode = LetterNode.init()
        
        for phrase : String in (phrases?.keys)! {
            
            if phrase.words().count == 2 {
                
                var current = rootNode
                var words = phrase.words()
                
                while (words.count != current.level) {
                    //                let searchKey = words[current.level]
                    let searchKey : String
                    if current.level == 0 {
                        searchKey = words[current.level]
                    } else {
                        searchKey = phrase
                    }
                    current = current.addChild(withKey : searchKey)
                }
                
                current.isEnd = true
                current.count = (phrases![phrase]?.valueForKey("count"))! as! Int
                current.time = (phrases![phrase]?.valueForKey("time"))! as! String
            }
        }
        
        //print(phrases?.count)
        print("build phrase tree completed")
        return rootNode
    }
    
    /**
     Build Relative Letter tree from json string in nsuserdefaul of container app.
     
     - parameter letterJsonString:  json string to build tree
     - parameter dispatchQueue:     queue to dispatch result
     - parameter completionHandler: closure to perform when tree is builded
     */
    func buildRelativeLetterTreeFromJsonString(relativeJsonString : String, dispatchQueue : dispatch_queue_t, completionHandler : (rootNode : LetterNode, error : NSError?) -> Void) {
        
        dispatch_async(internalConcurentQueue) {
            let rootNode = self.buildRelativeLetterTreeFromJsonString(relativeJsonString)
            
            dispatch_async(dispatchQueue, {
                completionHandler(rootNode: rootNode, error: nil)
            })
        }

    }
    
    /**
     Build relative letter tree from json string in syncronize
     
     - parameter relativeJsonString: Json string to build tree
     
     - returns: Return rootNode of tree
     */
    func buildRelativeLetterTreeFromJsonString(relativeJsonString : String) -> LetterNode {
        
        let relativeLetter = self.convertStringToDictionary(relativeJsonString)
        let rootNode = LetterNode.init()
        
        for phrase : String in (relativeLetter?.keys)! {
            var current = rootNode
            var words = phrase.words()
            
            while (words.count != current.level) {
//              let searchKey = words[current.level]
                let searchKey : String
                if current.level == 0 {
                    searchKey = words[current.level]
                } else {
                    searchKey = phrase
                }
                current = current.addChild(withKey : searchKey)
            }
            
            current.isEnd = true
        }

        print("build relative letter tree completed")
        return rootNode
    }
    
    /**
     Build Common Phrase tree from json string in nsuserdefaul of container app.
     
     - parameter letterJsonString:  json string to build tree
     - parameter dispatchQueue:     queue to dispatch result
     - parameter completionHandler: closure to perform when tree is builded
     */
    func buildCommonPhraseTreeFromJsonString(commonJsonString : String, dispatchQueue : dispatch_queue_t, completionHandler : (rootNode : LetterNode, error : NSError) -> Void) {
        
    }
    
// MARK: - Public suggestion method
    
    /**
     Return a list suggestion of word or phrase
     
     - parameter input:              input is all text in a session of keyboard
     - parameter numberOfSuggestion: number of suggestion will return
     - parameter dispatchQueue:      queue will dispatch when completion
     - parameter completionHandler:  closure will perform when completion
     */
    func suggestWithInput(input : String, numberOfSuggestion : Int?, dispatchQueue : dispatch_queue_t, completionHandler : (Array<String>,NSError?) ->Void) {
        
        dispatch_async(internalSerialQueue) { 
            var result : Array<String> = Array<String>()
            
            if input.characters.last == " " {

                let word = input.substringToIndex((input.endIndex.predecessor()));
                result = suggestionManager.suggestPhraseWithWord(word.lastWord().lowercaseString, andNumberOfSuggestion: numberOfSuggestion!)
                if input.characterAtIndex(input.length - 2).isUnicodeCharacter() {
                    self.updateTreeWithWord(word)
                }
            } else {
                if ((input.characters.contains(" ")) == true) {
                    var words = input.words()
                    result = suggestionManager.suggestWordWithPrefix(words[(words.endIndex) - 1].lowercaseString, andNumberOfSuggestion: numberOfSuggestion!)
                    
                } else {
                    result = suggestionManager.suggestWordWithPrefix(input.lowercaseString, andNumberOfSuggestion: numberOfSuggestion!)
                }
            }
            dispatch_async(dispatchQueue) {
                completionHandler(result, nil)
            }        }
    }
    
    /**
     Return words are suggested, from prefix input, in special queue
     
     - parameter prefix:             prefix of word
     - parameter numberOfSuggestion: number of word will return
     - parameter dispatchQueue:      queue to return
     - parameter completionHandler:  closure perform when completion
     */
    func suggestWordWithPrefix(prefix : String, numberOfSuggestion : Int?, dispatchQueue : dispatch_queue_t, completionHandler : (Array<String>,NSError?) ->Void) {
        
        dispatch_async(internalConcurentQueue) {
            
            var result : [String] = []
            if let numberOfWord = numberOfSuggestion {
                
                // Return a number of word
                result = self.suggestWordWithPrefix(prefix, andNumberOfSuggestion: numberOfWord)
            } else {
                
                // Return all word are suggeted
                result = self.suggestAllWordWithPrefix(prefix)
            }
            
            dispatch_async(dispatchQueue) {
                completionHandler(result, nil)
            }
        }
    }
    
    /**
     Suggest all word with prefix input in syncronize
     
     - parameter prefix: Prefix to suggest word
     
     - returns: Return array of all word
     */
    func suggestAllWordWithPrefix(prefix : String) -> Array<String> {
        
        var result = Array<LetterNode>()
        var suggestionWords : [String] = []
        
        self.findWord(withPrefix: prefix, result: &result)
        self.findRelativeWord(withPrefix: prefix, result: &result)
        
        if result.count == 0 {
            suggestionWords.append(prefix)
            return suggestionWords
        }
        
        result.sortInPlace({ (node1:LetterNode, node2:LetterNode) -> Bool in
            node1.count >= node2.count
        })
        for node in result {
            suggestionWords.append(node.key)
        }
        
        return suggestionWords
    }
    
    /**
     Suggest a number of word with prefix in syncronize
     
     - parameter prefix: Prefix to suggest word
     - parameter number: Number of word will return
     
     - returns: Return array of suggested word
     */
    func suggestWordWithPrefix(prefix : String, andNumberOfSuggestion number : Int ) -> Array<String> {
        
        var result = Array<LetterNode>()
        var suggestionWords : [String] = []
        
        self.findWord(withPrefix: prefix, result: &result)
        if result.count < number {
            self.findRelativeWord(withPrefix: prefix, result: &result)
        }
        
        if result.count == 0 {
            suggestionWords.append(prefix)
            return suggestionWords
        }
        
        if result.count > number {
            result.sortInPlace({ (node1:LetterNode, node2:LetterNode) -> Bool in
                node1.count >= node2.count
            })
            for i in 0..<number  {
                suggestionWords.append(result[i].key)
            }
            
        } else {
            for node in result {
                suggestionWords.append(node.key)
            }
        }
        
        return suggestionWords
    }
    
    /**
     Suggest all phrase with first word
     
     - parameter word: First word to suggest phrase
     
     - returns: Array of all phrase suggest
     */
    func suggestPhraseWithWord(word : String) -> Array<String> {
        var result = Array<String>()
        if let brandToFind = rootNodeOfPhraseTree.childrentNode[word] {
            
            // Sort dictionary and return array tuble
            let array = brandToFind.childrentNode.sort({ (i1, i2) -> Bool in
                if i1.1.count > i2.1.count {
                    return true
                }
                return false
            })
            
            for (key, _) in array {
                result.append(key.words()[1])
            }
            
        } else {
            result.append(word)
        }
        
        return result
    }
    
    /**
     Suggest a number of phrase with first word
     
     - parameter word:   First word to suggest phrase
     - parameter number: Number of phrase will return
     
     - returns: Array of phrase
     */
    func suggestPhraseWithWord(word : String, andNumberOfSuggestion number : Int) -> Array<String> {
        var result = Array<String>()
        
        if let brandToFind = rootNodeOfPhraseTree.childrentNode[word] {
            
            // Sort dictionary and return array tuble
            let array = brandToFind.childrentNode.sort({ (i1, i2) -> Bool in
                if i1.1.count > i2.1.count {
                    return true
                }
                return false
            })
            
            //print(brandToFind.childrentNode)
            
            for (key, _) in array {
                result.append(key.words()[1])
                if result.count == Constants.numberOfSuggestion {
                    return result
                }
            }
            
        } else {
            result.append(word)
        }
        
        return result
    }
    
// MARK: - improve suggestion
    
    /**
     Learn new word or increase count of exist word when user typing
     
     - parameter sentence: All text when user typing
     */
    func improveSuggestionWithSentence(sentence : String) {
//        let words = sentence.words()
//        
//        for word in words {
//            self.updateTreeWithWord(word)
//        }
        let phrases = sentence.phrases()
        
        for phrase in phrases {
            self.updateTreeWithPhrase(phrase.lowercaseString)
        }
        //self.rootNodeOfPhraseTree.printNode("", leaf: true)
    }
    
    /**
     Update tree with word. If new word is create node, else is increase count of word
     
     - parameter word: Word to update tree
     */
    func updateTreeWithWord(word : String) {
        var current = rootNodeOfLetterTree
        var isNewNode : Bool = false
        
        while (word.length != current.level) {
            let searchKey: String = word.substringToIndex(current.level + 1)
            let (node, bool) = current.addChild(withKey : searchKey)
            current = node
            isNewNode = bool
        }
        
        current.isEnd = true
        if current.count < Int.max && current.level > 1 {
            current.count += 1
        }
        
        // init create time for new node
        if isNewNode {
            current.time = NSDate().description
        }
    }
    
    /**
     Update tree with phrase. If new phrase is create node, else is increase count of word
     
     - parameter phrase: Phrase to update tree
     */
    func updateTreeWithPhrase(phrase : String) {

        var current = rootNodeOfPhraseTree
        var words = phrase.words()
        var isNewNode : Bool = false
        
        while (words.count != current.level) {
            //                let searchKey = words[current.level]
            let searchKey : String
            if current.level == 0 {
                searchKey = words[current.level]
            } else {
                searchKey = phrase
            }
            let (node, bool) = current.addChild(withKey : searchKey)
            current = node
            isNewNode = bool
        }
        
        current.isEnd = true
        if  current.count < Int.max {
            current.count += 1
        }
        
        // init create time for new node
        if isNewNode {
            current.time = NSDate().description
        }
    }
    
    /**
     Optimize memory with removing a number of node in tree
     
     - parameter rootNode:         Root node of tree will optimize
     - parameter numberNodeRemove: Number of node will remove
     */
    func optimizeTree(rootNode : LetterNode, numberNodeRemove : Int) {
        
        var listNode = rootNode.getAllEndNode()
        
        listNode.sortInPlace { (node1, node2) -> Bool in
            node1.count < node2.count
        }
        
        for i in 0..<numberNodeRemove {
            self.removePhrase(listNode[i].key)
        }
    }
    
    
// MARK: - Private method
    
    // Converse Json String to Dictionary
    func convertStringToDictionary(text: String) -> [String:NSDictionary]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:NSDictionary]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    // Check existion of word in letter tree
    func existionOfWord(word : String) -> Bool {
        var currentNode = rootNodeOfLetterTree
        
        while (word.length != currentNode.level) {
            let searchKey : String = word.substringToIndex(currentNode.level + 1)
            if currentNode.find(searchKey).found {
                currentNode = currentNode.find(searchKey).node!
            } else {
                return false
            }
        }
        
        if currentNode.key == word && currentNode.isEnd == true {
            return true
        } else {
            return false
        }
    }
    
    // Check existion of phrase in phrase tree
    func existionOfPhrase(phrase : String) -> Bool {
        let firstWord = phrase.words()[0]
        
        if let brandToFind = rootNodeOfPhraseTree.childrentNode[firstWord] {
            if brandToFind.childrentNode[phrase] != nil {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    // Remove word from letter tree
    func removeWord(word : String) -> Bool {
        
        if self.existionOfWord(word) == false {
            return false
        }
        
        var currentNode = rootNodeOfLetterTree
        while word.length != currentNode.level {
            let searchKey = word.substringToIndex(currentNode.level + 1)
            currentNode = currentNode.childrentNode[searchKey]!
        }
        
        if currentNode.childrentNode.count > 0 {
            currentNode.setEndNode(false)
            
        } else {
            var key = currentNode.key
            while (currentNode.childrentNode.count == 0 &&  !currentNode.isRoot()) {
                currentNode = currentNode.parentNode!
                currentNode.childrentNode[key]!.parentNode = nil
                currentNode.childrentNode[key]!.key = ""
                currentNode.childrentNode[key] = nil
                key = currentNode.key
            }
        }
        
        return true
    }
    
    // Remove phrase from phrase tree
    func removePhrase(phrase : String) -> Bool {
        
        if self.existionOfPhrase(phrase) == false {
            return false
        }
        
        var currentNode = rootNodeOfPhraseTree.childrentNode[phrase.words()[0]]!
        currentNode = currentNode.childrentNode[phrase]!
        
        var key = currentNode.key
        while (currentNode.childrentNode.count == 0 &&  !currentNode.isRoot()) {
            currentNode = currentNode.parentNode!
            currentNode.childrentNode[key]!.parentNode = nil
            currentNode.childrentNode[key]!.key = ""
            currentNode.childrentNode[key] = nil
            key = currentNode.key
        }
        
        return true
    }
    
    // Find word with prefix
    func findWord(withPrefix prefix : String, inout result : Array<LetterNode>) {
        let branchToUse = self.findBranchToSearch(fromPrefix: prefix)
        
        if branchToUse.level == 0 {
            //result.append(prefix)
            return
        }
        if branchToUse.isEnd == true {
            result.append(branchToUse)
            //            if result.count == Constants.numberOfSuggestion {
            //                return
            //            }
        }
        
        self.findWord(fromBranch: branchToUse, result: &result)
    }
    
    // Find word from branch
    func findWord(fromBranch branch : LetterNode, inout result : Array<LetterNode>) {
        var remainNode = Array<LetterNode>()
        
        for (_, node) in branch.childrentNode {
            if node.isEnd == true {
                result.append(node)
                //                if result.count == Constants.numberOfSuggestion {
                //                    return
                //                }
                if node.childrentNode.count > 0 {
                    remainNode.append(node)
                }
            } else {
                remainNode.append(node)
            }
        }
        
        for node in remainNode {
            self.findWord(fromBranch: node, result: &result)
            //            if result.count == Constants.numberOfSuggestion {
            //                return
            //            }
        }
    }
    
    // Find branch to begin search from
    func findBranchToSearch(fromPrefix prefix : String) -> LetterNode {
        var currentNode = rootNodeOfLetterTree
        
        while prefix.length != currentNode.level {
            let searchKey = prefix.substringToIndex(currentNode.level + 1)
            
            if let temptNode = currentNode.childrentNode[searchKey] {
                currentNode = temptNode
            } else {
                return trieNode
            }
        }
        
        return currentNode
    }
    
    // find relative word with prefix
    func findRelativeWord(withPrefix prefix : String, inout result : Array<LetterNode>) {
        for i in (prefix.length - 1).stride(to: -1, by: -1) {
            //print(i)
            let key : String = prefix.characterAtIndex(i)
            var searchKey : String
            
            if let temptNode = self.rootNodeOfRelativeLetterTree.childrentNode[key] {
                for (letter , _) in temptNode.childrentNode {
                    if prefix.length >= 2 {
                        searchKey = prefix.replace(withCharacter: letter.words()[1], atIndex: i)
                    } else {
                        searchKey = letter.words()[1]
                    }
                    self.findWord(withPrefix: searchKey, result: &result)
                    //                    if result.count == Constants.numberOfSuggestion {
                    //                        return
                    //                    }
                }
            }
        }
    }
    
    // Random generate a string with specify length
    func randomString(length: Int) -> String {
        
        let allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        let allowedCharsCount = UInt32(allowedChars.characters.count)
        var randomString = ""
        
        for _ in (0..<length) {
            let randomNum = Int(arc4random_uniform(allowedCharsCount))
            let newCharacter = allowedChars[allowedChars.startIndex.advancedBy(randomNum)]
            randomString += String(newCharacter)
        }
        
        return randomString
    }
}