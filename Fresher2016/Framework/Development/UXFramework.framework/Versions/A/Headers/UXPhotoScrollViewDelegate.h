/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@class UXPhotoScrollView;

/**
 * The photo scroll view delegate.
 *
 */
@protocol UXPhotoScrollViewDelegate <NSObject>

@optional

#pragma mark Zooming /** @name [UXPhotoScrollViewDelegate] Zooming */

/**
 * The user has double-tapped the photo to zoom either in or out.
 *
 * @param photoScrollView  The photo scroll view that was tapped.
 * @param didZoomIn        YES if the photo was zoomed in. NO if the photo was zoomed out.
 */
- (void)photoScrollViewDidDoubleTapToZoom: (UXPhotoScrollView *)photoScrollView
                                didZoomIn: (BOOL)didZoomIn;

@end

