//
//  FontCell.m
//  CameraGPUImage
//
//  Created by qhcthanh on 6/24/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "FontCell.h"

@implementation FontCell {
    
    __weak IBOutlet UILabel *fontLabel;
    __weak IBOutlet UIView *selectHeaderView;
}

-(void)awakeFromNib {
    [super awakeFromNib];
    selectHeaderView.hidden = true;
}

-(void)cleanUI {
    fontLabel.text = @" ";
    
}

-(void)renderUI:(NSString*)fontName {
    fontLabel.text = fontName;
    [fontLabel setFont:[UIFont fontWithName:fontName size:14]];
}

-(void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    selectHeaderView.hidden = !selected;
}

@end
