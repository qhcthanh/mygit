//
//  PianoNotes.swift
//  PianoNotes
//
//  Created by qhcthanh on 9/7/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import Foundation
import AVFoundation

class PianoNotes: NSObject {
    
    
    var soundStringDictionary = Dictionary<String, (soundName: String,soundID: SystemSoundID?)>()
    
    var tongHuaNotes =
    "qrerq-qrerq-qrer-rrwwq-qrerq-qyttr-qrerw-wwroi-ttUUyy-yyetrrer-rerU-wiyyt-ttUUyy-yypp-opa-ayto-ooiii-qiUy-yUy-yUyUytr-ryio-ooi-ttUy-ryio-oopoiUyUytr-rtyw-wrrer"
    
    private static var instance = PianoNotes()
    
    private override init() {
        super.init()
        
        // Setup Keyboard Index notes
        let leterString = "qwertyuiopasdfghjklzxcvbnm"
        var appendTones = [String]()
        var letterArray = [String]()
        
        var i = 1
        for c in leterString.characters {
            
            // 1 and 4 không có âm đen
            if i % 7 != 1 && i%7 != 4 {
                appendTones.append(String(c).uppercaseString)
            }
            letterArray.append("\(c)")
            i += 1
        }
        
        // Thêm các nốt đen vào nốt đen là ký tự viết hoa
        for c in appendTones {
            if let indexOfCharacter = letterArray.indexOf(c.lowercaseString) {
                letterArray.insert(c, atIndex: indexOfCharacter)
            }
        }
        
        // Mảng sound name các nốt
        for i in 40...88 {
            if i-40 < letterArray.count {
                soundStringDictionary[letterArray[i-40]] = (soundName: "piano-ff-0\(i)", soundID: nil)
            }
        }
    }
    
    class func shareInstance() -> PianoNotes {
        return instance
    }
    
    func playSound(letter: String) {
        
        if var soundInfo = soundStringDictionary[letter] {
            if  let soundURL = NSBundle.mainBundle().URLForResource(soundInfo.soundName, withExtension: "wav") {
                
                var soundID: SystemSoundID
                if let soundIDInfo = soundInfo.soundID {
                    soundID = soundIDInfo
                } else {
                    let index = Array(soundStringDictionary.keys).indexOf(letter)
                    soundID = UInt32(index!)
                    
                    soundInfo.soundID = soundID
                }
                
                AudioServicesCreateSystemSoundID(soundURL, &soundID)
                AudioServicesPlaySystemSound(soundID);
            }
        }
    }
    
    func removeAllSound() {
        for soundInfo in soundStringDictionary.values {
            if let soundID = soundInfo.soundID {
                AudioServicesRemoveSystemSoundCompletion(soundID)
            }
        }
    }
    
    
}