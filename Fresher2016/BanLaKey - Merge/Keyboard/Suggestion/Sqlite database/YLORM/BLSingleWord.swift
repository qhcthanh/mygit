//
//  BLSingleWord.swift
//  KeyboardDatabaseHaha
//
//  Created by VanDao on 8/31/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

import Foundation

class BLSingleWord : BLObject {
    
    var word: String! = ""
    var asciiWord: String! = ""
    var score = 0
    
    /**
     Initialzed a new `BLSingleWord` object with specifier dictionary
     
     - parameter dictionary: The dictionary that contain value of property of object
     
     - returns: The initialization `BLSingleWord` object
     */
    override init(dictionary: Dictionary<String, AnyObject>) {
        
        word = (dictionary["word"] ?? "") as! String
        asciiWord = (dictionary["asciiWord"] ?? "") as! String
        score = Int((dictionary["score"] ?? 0) as! String)!
        
        super.init()
    }
    
    /**
     Initialized a new `BLSingleWord` object with specifier pharse string and score
     
     - parameter phrase: The pharse string of object
     - parameter _score: The score of object
     
     - returns: The initialization `BLSingleWord` object
     */
    init(_word: String, _score: Int) {
        
        word = _word
        score = _score
        asciiWord = _word.toASCII()
        
        super.init()
    }
    
    /**
     Create and return the create table sql command of model
     
     - returns: The create table sql command
     */
    override class func getSQLCommandForCreateTable() -> String {
        let sqlString: String! = "CREATE VIRTUAL TABLE IF NOT EXISTS \(String(BLSingleWord)) USING fts4(word, asciiWord, score);";

        
        return sqlString
    }
}