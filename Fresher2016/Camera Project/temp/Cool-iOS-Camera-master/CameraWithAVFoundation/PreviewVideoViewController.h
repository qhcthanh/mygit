//
//  PreviewVideoViewController.h
//  CameraWithAVFoundation
//
//  Created by qhcthanh on 6/13/16.
//  Copyright © 2016 Gabriel Alvarado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CaptureSessionManager.h"

@interface PreviewVideoViewController : UIViewController {
    
}
@property AVPlayer* avPlayer;
@end
