/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#if defined(DEBUG) || defined(UX_DEBUG)

@class UXOverviewPageView;

/**
 * The root scrolling page view of the Overview.
 *
 * @ingroup Overview
 */
@interface UXOverviewView : UIView

/**
 * Whether the view has a translucent background or not.
 */
@property (nonatomic, assign) BOOL translucent;

/**
 * Whether the view can be draggable vertically or not.
 */
@property (nonatomic, assign) BOOL enableDraggingVertically;

/**
 * Prepends a new page to the Overview.
 */
- (void)prependPageView:(UXOverviewPageView *)page;

/**
 * Adds a new page to the Overview.
 */
- (void)addPageView:(UXOverviewPageView *)page;

/**
 * Removes a page from the Overview.
 */
- (void)removePageView:(UXOverviewPageView *)page;

/**
 * Update all of the views.
 */
- (void)updatePages;

- (void)flashScrollIndicators;

@end

#endif

