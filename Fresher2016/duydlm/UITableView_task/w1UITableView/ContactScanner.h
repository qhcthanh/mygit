//
//  ContactScanner.h
//  w1UITableView
//
//  Created by manhduydl on 5/18/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Contacts/Contacts.h>
#import <AddressBook/AddressBook.h>
#import "PermisionType.h"
#import "ContactProtocol.h"

@class Contact;

/**
 *  The block will be perform after loading contacts completed
 *
 *  @param contacts  Array of contacts will be returned
 *  @param error     An NSError will be return if occur
 */
//typedef void(^completedLoadContactHandler)(NSArray <Contact *> * _Nullable contacts, NSError * _Nullable error);
typedef void(^completedLoadContactHandler)(NSArray <id<ContactProtocol>> * _Nullable contacts, NSError * _Nullable error);

/**
 *  The block will be perform after requesting permision access contacts completed
 *
 *  @param granted The grant will be return after request from user
 *  @param error   Error code will be return of issue happen
 */
typedef void(^completedRequestPermisionHandler)(BOOL granted, NSError * _Nullable error);

@interface ContactScanner : NSObject

+ (nonnull instancetype)sharedInstance;

/**
 *  Scan all contact and perform completedBlock in specific queue
 *
 *  @param queue           The queue is used to perform completedBlock
 *  @param completionBlock Block will perform when scanning completed
 */
- (void)scanAllContactWithCompletionHandler:(nonnull completedLoadContactHandler)completionBlock
                              andDispatchTo:(_Nullable dispatch_queue_t)queue;

/**
 *  Request permision to access contact of device
 *
 *  @param completionBlock The block will be perform after requesting permision completed
 */
- (void)requestPermisionWithCompletion:(nonnull completedRequestPermisionHandler)completionBlock;

@end
