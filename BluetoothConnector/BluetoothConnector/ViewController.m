//
//  ViewController.m
//  BluetoothConnector
//
//  Created by qhcthanh on 9/9/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import "ViewController.h"

#define IMMEDIATE_ALERT_SERVICE @"FFE0"
#define FIND_ME_SERVICE @"1802"
#define LINK_LOSS_SERVICE @"FFE4"

#define CLIENT_CHARACTERISTIC_CONFIG @"00002902-0000-1000-8000-00805f9b34fb"
#define ALERT_LEVEL_CHARACTERISTIC @"00002a06-0000-1000-8000-00805f9b34fb"
#define FIND_ME_CHARACTERISTIC @"0000ffe1-0000-1000-8000-00805f9b34fb"

/*public static final UUID IMMEDIATE_ALERT_SERVICE = UUID.fromString("00001802-0000-1000-8000-00805f9b34fb");
public static final UUID FIND_ME_SERVICE = UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb");
public static final UUID LINK_LOSS_SERVICE = UUID.fromString("00001803-0000-1000-8000-00805f9b34fb");
public static final UUID BATTERY_SERVICE = UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb");
public static final UUID CLIENT_CHARACTERISTIC_CONFIG = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
public static final UUID ALERT_LEVEL_CHARACTERISTIC = UUID.fromString("00002a06-0000-1000-8000-00805f9b34fb");
public static final UUID FIND_ME_CHARACTERISTIC = UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb");*/


@interface ViewController ()  <CBCentralManagerDelegate, CBPeripheralDelegate>

@property (nonatomic, strong) CBCentralManager *centralManager;
@property (nonatomic, strong) NSMutableArray<CBPeripheral *> *iTagPeripherals;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    CBCentralManager *centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    _iTagPeripherals = [[NSMutableArray alloc] init];
    
    NSLog(@"%@",[CBUUID UUIDWithString:IMMEDIATE_ALERT_SERVICE].UUIDString);
    
    
   
    self.centralManager = centralManager;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CBCentralManagerDelegate

// method called whenever you have successfully connected to the BLE peripheral
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    [peripheral setDelegate:self];
    [peripheral discoverServices:nil];
    NSString *connected = [NSString stringWithFormat:@"Connected: %@", peripheral.state == CBPeripheralStateConnected ? @"YES" : @"NO"];
    NSLog(@"%@", connected);
}

// CBCentralManagerDelegate - This is called with the CBPeripheral class as its main input parameter. This contains most of the information there is to know about a BLE peripheral.
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    
    if (advertisementData.allKeys.count > 1) {
        //        NSLog(@"CBAdvertisementDataLocalNameKey: %@",[advertisementData objectForKey:CBAdvertisementDataLocalNameKey]);
        //
        //        NSLog(@"CBAdvertisementDataTxPowerLevelKey: %@",[advertisementData objectForKey:CBAdvertisementDataTxPowerLevelKey]);
        //
        //        NSLog(@"CBAdvertisementDataServiceUUIDsKey: %@",[advertisementData objectForKey:CBAdvertisementDataServiceUUIDsKey]);
        //
        //        NSLog(@"CBAdvertisementDataServiceDataKey: %@",[advertisementData objectForKey:CBAdvertisementDataServiceDataKey]);
        //
        //        NSLog(@": %@",[advertisementData objectForKey:CBAdvertisementDataManufacturerDataKey]);
        //
        //        NSLog(@"CBAdvertisementDataOverflowServiceUUIDsKey: %@",[advertisementData objectForKey:CBAdvertisementDataOverflowServiceUUIDsKey]);
        //
        //        NSLog(@"CBAdvertisementDataIsConnectable: %@",[advertisementData objectForKey:CBAdvertisementDataIsConnectable]);
        //
        //        NSLog(@"CBAdvertisementDataSolicitedServiceUUIDsKey: %@",[advertisementData objectForKey:CBAdvertisementDataSolicitedServiceUUIDsKey]);
        
        NSArray *arrayServices = [advertisementData objectForKey:CBAdvertisementDataServiceUUIDsKey];
        NSString *uuid = ((CBUUID *)arrayServices.firstObject).UUIDString;
        
        [_iTagPeripherals addObject:peripheral];
        peripheral.delegate = self;
        
        [self.centralManager connectPeripheral:peripheral options:nil];
    }
    
   
}

// method called whenever the device state changes.
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    
    // Determine the state of the peripheral
    if ([central state] == CBCentralManagerStatePoweredOff) {
        NSLog(@"CoreBluetooth BLE hardware is powered off");
    }
    else if ([central state] == CBCentralManagerStatePoweredOn) {
        NSLog(@"CoreBluetooth BLE hardware is powered on and ready");
        
       NSArray *services = @[[CBUUID UUIDWithString:IMMEDIATE_ALERT_SERVICE], [CBUUID UUIDWithString:FIND_ME_SERVICE], [CBUUID UUIDWithString:LINK_LOSS_SERVICE]];
         [self.centralManager scanForPeripheralsWithServices:nil options:nil];
    }
    else if ([central state] == CBCentralManagerStateUnauthorized) {
        NSLog(@"CoreBluetooth BLE state is unauthorized");
    }
    else if ([central state] == CBCentralManagerStateUnknown) {
        NSLog(@"CoreBluetooth BLE state is unknown");
    }
    else if ([central state] == CBCentralManagerStateUnsupported) {
        NSLog(@"CoreBluetooth BLE hardware is unsupported on this platform");
    }
}

#pragma mark - CBPeripheralDelegate

// CBPeripheralDelegate - Invoked when you discover the peripheral's available services.
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    for (CBService *service in peripheral.services) {
        NSLog(@"Discovered service: %@", service.UUID);
        [peripheral discoverCharacteristics:nil forService:service];
    }
}

// Invoked when you discover the characteristics of a specified service.
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error{
    NSLog(@"%@", service.characteristics);
}

// Invoked when you retrieve a specified characteristic's value, or when the peripheral device notifies your app that the characteristic's value has changed.
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    
}

#pragma mark - CBCharacteristic helpers

// Instance method to get the heart rate BPM information
- (void) getHeartBPMData:(CBCharacteristic *)characteristic error:(NSError *)error {
    
}
// Instance method to get the manufacturer name of the device
- (void) getManufacturerName:(CBCharacteristic *)characteristic {
    
}
// Instance method to get the body location of the device
- (void) getBodyLocation:(CBCharacteristic *)characteristic {
    
}
// Helper method to perform a heartbeat animation
- (void)doHeartBeat {
    
}

@end
