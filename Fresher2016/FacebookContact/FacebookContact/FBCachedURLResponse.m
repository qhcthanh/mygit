//
//  FBCachedURLResponse.m
//  FacebookContact
//
//  Created by VanDao on 7/25/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import "FBCachedURLResponse.h"

@implementation FBCachedURLResponse

- (instancetype)initWithResponse:(NSURLResponse *)response data:(NSData *)data {
    
    self = [super initWithResponse:response data:data];
    
    if (self) {
        _lastFocusDate = [NSDate date];
    }
    
    return self;
}

- (instancetype)initWithResponse:(NSURLResponse *)response data:(NSData *)data userInfo:(nullable NSDictionary *)userInfo storagePolicy:(NSURLCacheStoragePolicy)storagePolicy {
    
    self = [super initWithResponse:response data:data userInfo:userInfo storagePolicy:storagePolicy];
    
    if (self) {
        _lastFocusDate = [NSDate date];
    }
    
    return self;
}

- (void)updateFocusDate {
    
    _lastFocusDate = [NSDate date];
}

- (BOOL)isResponseOutOfDate {
    
    return ([NSDate timeIntervalSinceReferenceDate] - [_lastFocusDate timeIntervalSinceReferenceDate]) > kTimeToLive;
}

- (BOOL)isResponseOutOfDateWithExpiredDate:(NSDate *)date {
    
    return ([date timeIntervalSinceReferenceDate] - [_lastFocusDate timeIntervalSinceReferenceDate]) > kTimeToLive;
}

@end
