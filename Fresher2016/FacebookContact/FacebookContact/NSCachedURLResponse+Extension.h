//
//  NSCachedURLResponse+Extension.h
//  FacebookContact
//
//  Created by VanDao on 7/25/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface NSCachedURLResponse (UIImage)

@property (readonly, nonatomic, getter=imageData) UIImage *imageData;

- (UIImage *)imageData;

@end
