//
//  VSViewController.h
//  CameraGPUImage
//
//  Created by manhduydl on 8/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <GLKit/GLKit.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import <GLKit/GLKit.h>
#import <AVFoundation/AVFoundation.h>
#import "MDCaptureManager.h"

#define LEADING_PHOTO_MODEVIEW_CONTRAINST -15
#define LEADING_VIDEO_MODEVIEW_CONTRAINST -63

@class OVLScript;
@interface VSViewController : GLKViewController <MDCaptureManagerDelegate, AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate>

@property (strong, nonatomic) EAGLContext *context;
@property (nonatomic) CMTime maxDuration;

@property (nonatomic) BOOL fHD;
@property (nonatomic) BOOL fPhotoRatio; // 4x3 instead of 16x9
@property (nonatomic) BOOL fWatermark;
@property (nonatomic, readonly) AVCaptureDevice *camera;
@property (nonatomic, readonly) NSInteger duration;
//@property (nonatomic) GLuint renderbufferEx; // for external monitor
@property (nonatomic) BOOL fProcessAudio;
@property (nonatomic) float speed;
@property (nonatomic) BOOL fLandscape;
@property (nonatomic) UIImage* imageTexture;
@property (nonatomic, readonly) GLKView* glkView;
@property (nonatomic) CMTime timeRecorded;
//@property (nonatomic) AVAsset* assetSrc;
//@property (nonatomic, readonly) AVCaptureSession* session;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingModeViewConstraint;
@property (weak, nonatomic) IBOutlet UIButton *flashButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *cameraRotateButton;
@property (weak, nonatomic) IBOutlet UIView *modeView;
@property (weak, nonatomic) IBOutlet UILabel *photoModeLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoModeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *shutterButton;

@end
