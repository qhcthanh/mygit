/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "UXShape.h"

@interface UXRoundedRectangleShape : UXShape {
}

@property (nonatomic) CGFloat topLeftRadius;
@property (nonatomic) CGFloat topRightRadius;
@property (nonatomic) CGFloat bottomRightRadius;
@property (nonatomic) CGFloat bottomLeftRadius;

+ (UXRoundedRectangleShape*)shapeWithRadius:(CGFloat)radius;

+ (UXRoundedRectangleShape*)shapeWithTopLeft:(CGFloat)topLeft topRight:(CGFloat)topRight
								 bottomRight:(CGFloat)bottomRight bottomLeft:(CGFloat)bottomLeft;

@end
