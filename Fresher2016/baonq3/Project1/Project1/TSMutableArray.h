//
//  TSMutableArray.h
//  Project1
//
//  Created by BaoNQ on 5/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Thread Safe MutableArray
 */
@interface TSMutableArray : NSMutableArray {
    
}
@end
