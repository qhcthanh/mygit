/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

extern const NSString* kSkinDidChangeNotification;
extern const NSString* kLanguageDidChangeNotification;

/**
 * @name The Skin Delegate protocol.
 */
@protocol UXSkinDelegate <NSObject>

/**
 * Notifies the view controller that language is changed.
 */
- (void)skinDidChange;

/**
 * Notifies the view controller that skin is changed.
 */
- (void)languageDidChange;

/**
 * Notifies the view controller that orientation is changed.
 */
- (void)deviceDidRotate;

@end


@interface UIViewController(Skin) <UXSkinDelegate>

/**
 * Starts observing for `kSkinDidChangeNotification` notifications.
 * @discussion It is recommended to call this method in `-viewWillAppear:`.
 */
- (void)startObservingSkinNotifications;

/**
 * Stops observing for `kSkinDidChangeNotification` notifications.
 * @discussion It is recommended to call this method in `-viewWillDisappear:`.
 */
- (void)stopObservingSkinNotifications;

/**
 * Starts observing for `kLanguageDidChangeNotification` notifications.
 * @discussion It is recommended to call this method in `-viewWillAppear:`.
 */
- (void)startObservingLanguageNotifications;

/**
 * Stops observing for `kLanguageDidChangeNotification` notifications.
 * @discussion It is recommended to call this method in `-viewWillDisappear:`.
 */
- (void)stopObservingLanguageNotifications;

/**
 * Starts observing for `kOrientationDidChangeNotification` notifications.
 * @discussion It is recommended to call this method in `-viewWillAppear:`.
 */
- (void)startObservingOrientationNotifications;

/**
 * Stops observing for `kOrientationDidChangeNotification` notifications.
 * @discussion It is recommended to call this method in `-viewWillDisappear:`.
 */
- (void)stopObservingOrientationNotifications;

@end

/**
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self startObservingOrientationNotifications];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self stopObservingOrientationNotifications];
 }*/

