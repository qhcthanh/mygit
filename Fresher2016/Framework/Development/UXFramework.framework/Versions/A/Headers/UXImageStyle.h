/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "UXStyle.h"

@interface UXImageStyle : UXStyle {	
}


@property (nonatomic) CGSize size;
@property (nonatomic, retain) UIImage* image;
@property (nonatomic, copy) NSString* imageURL;
@property (nonatomic, retain) UIImage* defaultImage;
@property (nonatomic) UIViewContentMode contentMode;

+ (UXImageStyle*)styleWithImageURL:(NSString*)imageURL next:(UXStyle*)next;
+ (UXImageStyle*)styleWithImageURL:(NSString*)imageURL defaultImage:(UIImage*)defaultImage
							  next:(UXStyle*)next;
+ (UXImageStyle*)styleWithImageURL:(NSString*)imageURL defaultImage:(UIImage*)defaultImage
					   contentMode:(UIViewContentMode)contentMode
							  size:(CGSize)size next:(UXStyle*)next;
+ (UXImageStyle*)styleWithImage:(UIImage*)image next:(UXStyle*)next;
+ (UXImageStyle*)styleWithImage:(UIImage*)image defaultImage:(UIImage*)defaultImage
						   next:(UXStyle*)next;
+ (UXImageStyle*)styleWithImage:(UIImage*)image defaultImage:(UIImage*)defaultImage
					contentMode:(UIViewContentMode)contentMode
						   size:(CGSize)size next:(UXStyle*)next;

@end
