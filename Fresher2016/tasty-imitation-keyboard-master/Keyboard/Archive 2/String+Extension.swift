//
//  String+Extension.swift
//  KeyboardDatabaseHaha
//
//  Created by VanDao on 8/26/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

import Foundation

extension String {
    func toASCII() -> String {
        var result : String = ""
        
        for c in characters {
            if let asciiChar = mapVADictionary[String(c)] {
                result += asciiChar
            } else {
                result.append(c)
            }
        }
        return result
    }
}

private let mapVADictionary = [
    "đ": "d",
    //a
    "á": "a",
    "à": "a",
    "ả": "a",
    "ã": "a",
    "ạ": "a",
    // ă
    "ă": "a",
    "ắ": "a",
    "ằ": "a",
    "ặ": "a",
    "ẳ": "a",
    "ẵ": "a",
    //â
    "â": "a",
    "ấ": "a",
    "ầ": "a",
    "ẩ": "a",
    "ẫ": "a",
    "ậ": "a",
    // ê
    "ê": "e",
    "ế": "e",
    "ề": "e",
    "ể": "e",
    "ễ": "e",
    "ệ": "e",
    // e
    "é": "e",
    "è": "e",
    "ẻ": "e",
    "ẽ": "e",
    "ẹ": "e",
    // u
    "ú": "u",
    "ù": "u",
    "ủ": "u",
    "ũ": "u",
    "ụ": "u",
    // ư
    "ư": "u",
    "ứ": "u",
    "ừ": "u",
    "ử": "u",
    "ữ": "u",
    "ự": "u",
    // i
    "í": "i",
    "ì": "i",
    "ỉ": "i",
    "ĩ": "i",
    "ị": "i",
    //o
    "ó": "o",
    "ò": "o",
    "ỏ": "o",
    "õ": "o",
    "ọ": "o",
    //ô
    "ô": "o",
    "ố": "o",
    "ồ": "o",
    "ổ": "o",
    "ỗ": "o",
    "ộ": "o",
    
    //ơ
    "ơ": "o",
    "ớ": "o",
    "ờ": "o",
    "ở": "o",
    "ỡ": "o",
    "ợ": "o",
    
    //y
    "ý": "y",
    "ỳ": "y",
    "ỷ": "y",
    "ỹ": "y",
    "ỵ": "y",
]