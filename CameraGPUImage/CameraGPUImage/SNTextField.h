//
//  SNTextField.h
//  CameraGPUImage
//
//  Created by BaoNQ on 8/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNTextField : UITextField

@property (nonatomic) CGPoint location;
@property (nonatomic, strong) UIPanGestureRecognizer *panGesture;
@property (nonatomic) CGFloat heightOfScreen;

- (id)initWithWidth:(CGFloat)width yPosition:(CGFloat)yPos screenHeight:(CGFloat)screenHeight;

- (void)handleTap;

@end
