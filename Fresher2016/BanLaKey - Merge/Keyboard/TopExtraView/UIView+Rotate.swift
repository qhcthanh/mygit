//
//  UIView+Rotate.swift
//  BanLaKey
//
//  Created by qhcthanh on 8/31/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import UIKit

public extension UIView
{
    
    /**
     Starts rotating the view around Z axis.
     
     @param degress The degress to rotate view
     @param duration Duration of one full 360 degrees rotation. One second is default.
     @param repeatCount How many times the spin should be done. If not provided, the view will spin forever.
     @param clockwise Direction of the rotation. Default is clockwise (true).
     */
    func startZRotation(degress degress: Double, duration: CFTimeInterval = 1, repeatCount: Float = Float.infinity, clockwise: Bool = true)
    {
        if self.layer.animationForKey("transform.rotation.z") != nil {
            return
        }
        let animation = CABasicAnimation(keyPath: "transform.rotation.z")
        let direction = clockwise ? 1.0 : -1.0
        animation.toValue = NSNumber(double: self.degressToRadian(degress) * direction)
        animation.duration = duration
        animation.cumulative = false
        animation.repeatCount = 0
        self.layer.addAnimation(animation, forKey:"transform.rotation.z")
    }
    
    private func degressToRadian(dergess: Double) -> Double {
        return dergess * M_PI_2 / 180.0
    }
    
    /// Stop rotating the view around Z axis.
    func stopZRotation()
    {
        self.layer.removeAnimationForKey("transform.rotation.z")
    }
    
}