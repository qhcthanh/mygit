//
//  ShowImageCaptureViewController.m
//  CameraWithAVFoundation
//
//  Created by qhcthanh on 6/8/16.
//  Copyright © 2016 Gabriel Alvarado. All rights reserved.
//

#import "ShowImageCaptureViewController.h"

@interface ShowImageCaptureViewController () {
    NSMutableArray *_filters;
    NSUInteger _curFilter;
    
    EAGLContext *_eaglContext;
    GLKView *_videoPreviewView;
    CGRect _videoPreviewViewBounds;
    CIContext *_ciContext;
}
@end


@implementation ShowImageCaptureViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    
    //Setup filters
    _curFilter = -1;
    _filters = [[NSMutableArray alloc] init];

    
    // remove the view's background color; this allows us not to use the opaque property (self.view.opaque = NO) since we remove the background color drawing altogether
    self.view.backgroundColor = [UIColor clearColor];
    
    // setup the GLKView for video/image preview
    
    _eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    _videoPreviewView = [[GLKView alloc] initWithFrame:self.view.bounds context:_eaglContext];
    _videoPreviewView.enableSetNeedsDisplay = NO;
    
    // because the native video image from the back camera is in UIDeviceOrientationLandscapeLeft (i.e. the home button is on the right), we need to apply a clockwise 90 degree transform so that we can draw the video preview as if we were in a landscape-oriented view; if you're using the front camera and you want to have a mirrored preview (so that the user is seeing themselves in the mirror), you need to apply an additional horizontal flip (by concatenating CGAffineTransformMakeScale(-1.0, 1.0) to the rotation transform)
    _videoPreviewView.transform = CGAffineTransformMakeRotation(M_PI_2);
    _videoPreviewView.frame = self.view.frame;
    
    // we make our video preview view a subview of the window, and send it to the back; this makes FHViewController's view (and its UI elements) on top of the video preview, and also makes video preview unaffected by device rotation
    [self.view addSubview:_videoPreviewView];
    
    
    UIButton* backButton = [[UIButton alloc] initWithFrame:CGRectMake( 20, self.view.frame.size.height - 100, 200, 40)];
    [backButton setTitle:@"Back" forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    
    // bind the frame buffer to get the frame buffer width and height;
    // the bounds used by CIContext when drawing to a GLKView are in pixels (not points),
    // hence the need to read from the frame buffer's width and height;
    // in addition, since we will be accessing the bounds in another queue (_captureSessionQueue),
    // we want to obtain this piece of information so that we won't be
    // accessing _videoPreviewView's properties from another thread/queue
    [_videoPreviewView bindDrawable];
    _videoPreviewViewBounds = CGRectZero;
    _videoPreviewViewBounds.size.width = _videoPreviewView.drawableWidth;
    _videoPreviewViewBounds.size.height = _videoPreviewView.drawableHeight;
    
    
    // create the CIContext instance, note that this must be done after _videoPreviewView is properly set up
    _ciContext = [CIContext contextWithEAGLContext:_eaglContext options:@{kCIContextWorkingColorSpace : [NSNull null]} ];
    
    
    
    //Load image
    CIImage *ciImage = [CIImage imageWithCGImage:_captureImage.CGImage];
    
    //Setup filters
    [_filters addObject:[CTFilter filterImage:ciImage withFilterName:CTBlur]];
    [_filters addObject:[CTFilter filterImage:ciImage withFilterName:CTToneCurve]];
    [_filters addObject:[CTFilter filterImage:ciImage withFilterName:CTColorMonochrome]];

    [self drawImage:ciImage];
    [_videoPreviewView display];
    
    
    
    
    // Swipe
    UISwipeGestureRecognizer *rightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandle:)];
    rightRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [rightRecognizer setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:rightRecognizer];
    
    UISwipeGestureRecognizer *leftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandle:)];
    leftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [leftRecognizer setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:leftRecognizer];
}

- (void)rightSwipeHandle:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSLog(@"rightSwipeHandle");
    if (_curFilter == -1)
        return;
    _curFilter--;
    
    CIImage *outputImage;
    
    if (_curFilter == -1)
        outputImage = [CIImage imageWithCGImage:_captureImage.CGImage];
    else
        outputImage = [[_filters objectAtIndex:_curFilter] outputImage];
    
    [self drawImage:outputImage];
    [_videoPreviewView display];
}

- (void)leftSwipeHandle:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSLog(@"leftSwipeHandle");
    if (_curFilter == _filters.count - 1)
        return;
    _curFilter++;
    
    CIImage *outputImage = [[_filters objectAtIndex:_curFilter] outputImage];
        
    [self drawImage:outputImage];
    [_videoPreviewView display];
}


-(void)backAction {
    _captureImage = nil;
    [self dismissViewControllerAnimated:true completion:nil];
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:true];
}

-(void)drawImage:(CIImage *)image {
    CGRect sourceExtent = image.extent;
    CGRect drawRect = sourceExtent;
    
    // Scale image
    /*
     CGFloat sourceAspect = sourceExtent.size.width / sourceExtent.size.height;
     CGFloat previewAspect = _videoPreviewViewBounds.size.width  / _videoPreviewViewBounds.size.height;
     
     // we want to maintain the aspect radio of the screen size, so we clip the video image
     
     if (sourceAspect > previewAspect)
     {
     // use full height of the video image, and center crop the width
     drawRect.origin.x += (drawRect.size.width - drawRect.size.height * previewAspect) / 2.0;
     drawRect.size.width = drawRect.size.height * previewAspect;
     }
     else
     {
     // use full width of the video image, and center crop the height
     drawRect.origin.y += (drawRect.size.height - drawRect.size.width / previewAspect) / 2.0;
     drawRect.size.height = drawRect.size.width / previewAspect;
     }
     */
    
    [_videoPreviewView bindDrawable];
    
    if (_eaglContext != [EAGLContext currentContext])
        [EAGLContext setCurrentContext:_eaglContext];
    
    // clear eagl view to grey
    glClearColor(0.5, 0.5, 0.5, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    
    // set the blend mode to "source over" so that CI will use that
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    
    NSLog(@"Inrect");
    if (image)
        [_ciContext drawImage:image inRect:_videoPreviewViewBounds fromRect:drawRect];
    
    
}
        
@end
