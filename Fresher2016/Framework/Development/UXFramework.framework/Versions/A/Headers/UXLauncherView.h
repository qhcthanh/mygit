/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXViewRecycler.h"

@protocol UXLauncherDataSource;
@protocol UXLauncherButtonView;
@protocol UXLauncherViewDelegate;

/**
 * Calculate the given field dynamically given the view and button dimensions.
 */
extern const NSInteger UXLauncherViewGridBasedOnButtonSize;

/**
 * A launcher view that simulates iOS' home screen launcher functionality.
 *
 */
@interface UXLauncherView : UIView

/**
 * The maximum number of buttons allowed on a given page.
 *
 * By default this value is NSIntegerMax.
 *
 */
@property (nonatomic, assign) NSInteger maxNumberOfButtonsPerPage; // Default: NSIntegerMax

/**
 * The distance that each page view insets its contents.
 *
 * Use this property to add to the area around the content of each page. The unit of size is points.
 * The default value is 10 points on all sides.
 *
 */
@property (nonatomic, assign) UIEdgeInsets contentInsetForPages;

/**
 * The size of each launcher button.
 *
 * @fn UXLauncherView::buttonSize
 */
@property (nonatomic, assign) CGSize buttonSize; // Default: 80x80

/**
 * The number of rows to display on each page.
 *
 */
@property (nonatomic, assign) NSInteger numberOfRows; // Default: UXLauncherViewGridBasedOnButtonSize

/**
 * The number of columns to display on each page.
 *
 */
@property (nonatomic, assign) NSInteger numberOfColumns; // Default: UXLauncherViewGridBasedOnButtonSize

/**
 * Reloads the pages of the receiver.
 *
 * Call this method to reload all the data that is used to construct the launcher, including pages
 * and buttons. For efficiency, the launcher redisplays only those pages that are visible or nearly
 * visible.
 *
 */
- (void)reloadData;

/**
 * The object that acts as the delegate of the receiving launcher view.
 *
 * The delegate must adopt the UXLauncherDelegate protocol. The delegate is not retained.
 *
 */
@property (nonatomic, weak) id<UXLauncherViewDelegate> delegate;

/**
 * The object that acts as the data source of the receiving table view.
 *
 * The data source must adopt the UXLauncherDataSource protocol. The data source is not retained.
 *
 */
@property (nonatomic, weak) id<UXLauncherDataSource> dataSource;

/**
 * Returns a reusable launcher button view object located by its identifier.
 *
 * @param identifier A string identifying the launcher button view object to be reused. By
 *                        default, a reusable view's identifier is its class name, but you can
 *                        change it to any arbitrary value.
 * @returns A UIView object with the associated identifier that conforms to the
 *               UXLauncherButtonView protocol, or nil if no such object exists in the reusable-cell
 *               queue.
 */
- (UIView<UXLauncherButtonView> *)dequeueReusableViewWithIdentifier:(NSString *)identifier;

/**
 * Stores the current state of the launcher view in preparation for rotation.
 *
 * This must be called in conjunction with willAnimateRotationToInterfaceOrientation:duration:
 * in the methods by the same name from the view controller containing this view.
 *
 */
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;

/**
 * Updates the frame of the launcher view while maintaining the current visible page's state.
 *
 */
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;

@end

/**
 * The launcher data source used to populate the view.
 *
 */
@protocol UXLauncherDataSource <NSObject>
@required

/** @name Configuring a Launcher View */

/**
 * Tells the receiver to return the number of rows in a given section of a table view (required).
 *
 * @param launcherView The launcher-view object requesting this information.
 * @param page The index locating a page in @c launcherView.
 * @returns The number of buttons in @c page.
 */
- (NSInteger)launcherView:(UXLauncherView *)launcherView numberOfButtonsInPage:(NSInteger)page;

/**
 * Tells the receiver to return a button view for inserting into a particular location of a given
 * page in the launcher view (required).
 *
 * @param launcherView The launcher-view object requesting this information.
 * @param page The index locating a page in @c launcherView.
 * @param index The index locating a button in a page.
 * @returns A UIView that conforms to UXLauncherButtonView that the launcher will display on
 the given page. An assertion is raised if you return nil.
 */
- (UIView<UXLauncherButtonView> *)launcherView:(UXLauncherView *)launcherView buttonViewForPage:(NSInteger)page atIndex:(NSInteger)index;

@optional

/**
 * Asks the receiver to return the number of pages in the launcher view.
 *
 * It is assumed that the launcher view has one page if this method is not implemented.
 *
 * @param launcherView The launcher-view object requesting this information.
 * @returns The number of pages in @c launcherView. The default value is 1.
 * @sa UXLauncherDataSource::launcherView:numberOfButtonsInPage:
 */
- (NSInteger)numberOfPagesInLauncherView:(UXLauncherView *)launcherView;

/**
 * Asks the receiver to return the number of rows of buttons each page can display in the
 * launcher view.
 *
 * This method will be called each time the frame of the launcher view changes. Notably, this will
 * be called when the launcher view has been rotated as a result of a device rotation.
 *
 * @param launcherView The launcher-view object requesting this information.
 * @returns The number of rows of buttons each page can display.
 * @sa UXLauncherDataSource::numberOfColumnsPerPageInLauncherView:
 */
- (NSInteger)numberOfRowsPerPageInLauncherView:(UXLauncherView *)launcherView;

/**
 * Asks the receiver to return the number of columns of buttons each page can display in the
 * launcher view.
 *
 * This method will be called each time the frame of the launcher view changes. Notably, this will
 * be called when the launcher view has been rotated as a result of a device rotation.
 *
 * @param launcherView The launcher-view object requesting this information.
 * @returns The number of columns of buttons each page can display.
 * @sa UXLauncherDataSource::numberOfRowsPerPageInLauncherView:
 */
- (NSInteger)numberOfColumnsPerPageInLauncherView:(UXLauncherView *)launcherView;

@end


/**
 * The launcher delegate used to inform of state changes and user interactions.
 *
 */
@protocol UXLauncherViewDelegate <NSObject>
@optional

/** @name Managing Selections */

/**
 * Informs the receiver that the specified item on the specified page has been selected.
 *
 * @param launcherView A launcher-view object informing the delegate about the new item
 *                          selection.
 * @param page A page index locating the selected item in @c launcher.
 * @param index An index locating the selected item in the given page.
 */
- (void)launcherView:(UXLauncherView *)launcherView didSelectItemOnPage:(NSInteger)page atIndex:(NSInteger)index;

@end

/**
 * The launcher delegate used to inform of state changes and user interactions.
 *
 */
@protocol UXLauncherButtonView <UXRecyclableView>
@required

/**
 * Requires the view to contain a button subview.
 */
@property (nonatomic, strong) UIButton* button;

@end


