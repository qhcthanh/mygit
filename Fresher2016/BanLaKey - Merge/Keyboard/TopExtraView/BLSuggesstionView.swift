//
//  BLSuggesstionView.swift
//  BanLaKey
//
//  Created by qhcthanh on 8/30/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import UIKit

public protocol BLSuggesstionViewDataSource: NSObjectProtocol {
    
    func numberOfSuggestion(suggesstionView: BLSuggestionView) -> Int
    
    func suggesstionView(suggesstionView: BLSuggestionView, viewAtIndex index: Int) -> BLSuggestionViewCell
}

@objc public protocol BLSuggesstionViewDelegate: NSObjectProtocol {
    
    optional func suggesstionView(suggesstionView: BLSuggestionView, didSelectAtIndex index: Int)
}

public class BLSuggestionView: UIView {
    
    var spacingForCell: CGFloat = 2
    
    public weak var delegate: BLSuggesstionViewDelegate?
    public weak var dataSource: BLSuggesstionViewDataSource? {
        didSet {
            self.reloadData()
        }
    }
    
    private var currentCell = [BLSuggestionViewCell]()
    public var currentArraySuggesstion = NSMutableArray()
    
    public convenience init() {
        self.init(frame: CGRectZero)
        
       
    }
    
    public func reloadData() {
        
        // Clean View
        for view in currentCell {
            view.removeFromSuperview()
        }
        currentCell.removeAll()

        
        if let numberOfItem = dataSource?.numberOfSuggestion(self) {
            
            var lastCell: BLSuggestionViewCell? = nil
            var lastRightContrainst: NSLayoutConstraint!
            
            for i in 0..<numberOfItem {
                
                let cellAtIndex: BLSuggestionViewCell = (dataSource?.suggesstionView(self, viewAtIndex: i))!
                self.addSubview(cellAtIndex)
                
                // First time run. Set View is constraint to self
                if lastCell == nil {
                    lastCell = cellAtIndex
                    
                    cellAtIndex.constrain(.Left, to: .Left, of: self)
                    lastRightContrainst = cellAtIndex.constrain(.Right, to: .Right, of: self)
                    cellAtIndex.constrain(.Top, to: .Top, of: self)
                    cellAtIndex.constrain(.Bottom, to: .Bottom, of: self)
                    
                }
                // If second view. Set second View equal Top and Bottom, Equal Width last cell, tralling lastCell
                else {
                    self.removeConstraint(lastRightContrainst)
                    //self.updateConstraintsIfNeeded()
                    
                    cellAtIndex.constrain(.Left, to: .Right, of: lastCell!, offsetBy: self.spacingForCell)
                    cellAtIndex.constrain(.Top, to: .Top, of: self)
                    cellAtIndex.constrain(.Bottom, to: .Bottom, of: self)
                    cellAtIndex.constrain(.Width, to: .Width, of: lastCell!)
                    
                    lastRightContrainst = cellAtIndex.constrain(.Right, to: .Right, of: self)
                    lastCell = cellAtIndex
                    
                }
                
                cellAtIndex.textLabel.text = self.getSuggesstionData(AtIndex: i) as? String
                cellAtIndex.suggesstionView = self
                
                currentCell.append(cellAtIndex)
            }
            
            //self.updateConstraintsIfNeeded()
            
        }
    }
    
    public func getCellAtIndex(index: Int) -> BLSuggestionViewCell? {
        if index < self.currentCell.count {
            return self.currentCell[index]
        }
        
        return nil
    }
    
    public func getSuggesstionData(AtIndex index: Int) -> AnyObject? {
        if index < self.currentArraySuggesstion.count {
            return self.currentArraySuggesstion.objectAtIndex(index)
        }
        
        return nil
    }
    
    public func setArraySuggest(data: NSMutableArray) {
        
        if self.currentCell.count == 0 {
            return
        }
        
        self.currentArraySuggesstion = data
        
        // Clean data
        for i in 0..<self.currentCell.count {
            if i < data.count {
                self.currentCell[i].textLabel.text = data.objectAtIndex(i) as? String
            } else {
                self.currentCell[i].textLabel.text = " "
            }
        }
    }
    
    public func didSelectSuggesstionCell(suggestionCell: BLSuggestionViewCell) {
        if let delegate = self.delegate, let didSelectItem = delegate.suggesstionView {
            if let indexOfCell = self.currentCell.indexOf(suggestionCell) {
                didSelectItem(self, didSelectAtIndex: indexOfCell)
            }
        }
    }
    
}
