//
//  ContactStore.m
//  Project2
//
//  Created by BaoNQ on 5/19/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "CTContactStore.h"
#import "Constants.h"
#import "ContactImageCaching.h"

@interface CTContactStore () {
    dispatch_queue_t _internalSerialQueue;
    BOOL _isIOS9;
}
@end

@implementation CTContactStore

//Singleton Method
+ (id)sharedContactStore {
    static CTContactStore *sharedContacts = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedContacts = [[self alloc] init];
    });
    return sharedContacts;
}

- (id)init {
    self = [super init];
    if (!self)
        return nil;
    self->_internalSerialQueue = dispatch_queue_create("com.vng.contactStoreQueue", NULL);
    self->_isIOS9 = IS_OS_9_OR_LATER;
    
    return self;
}

- (BOOL)checkContactAuthorizationStatus:(NSError **)error {
    *error = nil;
    if (_isIOS9) {
        NSLog(@"Version: iOS 9");
        
        CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
        NSString *errorInfo = @"This app previously was refused permissions to contacts. Please go to settings and grant permission to this app so it can use contacts";
        if (status == CNAuthorizationStatusDenied) {
            *error = [NSError errorWithDomain:DOMAIN_STRING
                                                 code:CTAuthorizationStatusDenied
                                             userInfo:@{NSLocalizedDescriptionKey: errorInfo}];
            return NO;
        }
        else if (status == CNAuthorizationStatusRestricted) {
            *error = [NSError errorWithDomain:DOMAIN_STRING
                                         code:CTAuthorizationStatusRestricted
                                     userInfo:@{NSLocalizedDescriptionKey: errorInfo}];
            return NO;
        }
        else if (status == CNAuthorizationStatusNotDetermined) {
            *error = [NSError errorWithDomain:DOMAIN_STRING
                                         code:CTAuthorizationStatusNotDetermined
                                     userInfo:@{NSLocalizedDescriptionKey: @"No authorization status could be determined"}];
            return NO;
        }
    }
    else {
        NSLog(@"Version: below iOS 9");
        ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
        
        NSString *errorInfo = @"This app previously was refused permissions to contacts. Please go to settings and grant permission to this app so it can use contacts";
        if (status == kABAuthorizationStatusDenied) {
            // User didn't grant access;
            *error = [NSError errorWithDomain:DOMAIN_STRING
                                                 code:CTAuthorizationStatusDenied
                                             userInfo:@{NSLocalizedDescriptionKey: errorInfo}];
             return NO;
        }
        else if (status == kABAuthorizationStatusRestricted) {
            *error = [NSError errorWithDomain:DOMAIN_STRING
                                         code:CTAuthorizationStatusRestricted
                                     userInfo:@{NSLocalizedDescriptionKey: errorInfo}];
            return NO;
        }
        else if (status == kABAuthorizationStatusNotDetermined) {
            *error = [NSError errorWithDomain:DOMAIN_STRING
                                         code:CTAuthorizationStatusNotDetermined
                                     userInfo:@{NSLocalizedDescriptionKey: @"No authorization status could be determined"}];
            return NO;
        }
    }
    return YES;
}

- (void)requestAccessWithCompletionHandler:(RequestAccessCompletionHandler)completionHandler dispatchTo:(dispatch_queue_t)returnedQueue{
    dispatch_queue_t dispatchQueue = (returnedQueue != nil) ? returnedQueue : dispatch_get_main_queue();
    
    dispatch_async(_internalSerialQueue, ^{
        NSError *authorizationStatusError;
        
        // Check the authorization status.
        if ([self checkContactAuthorizationStatus:&authorizationStatusError]) {
            if (completionHandler != nil) {
                dispatch_async(dispatchQueue, ^{
                    completionHandler(nil);
                });
            }
        }
        else {
            // If the authorization is not determined
            if (authorizationStatusError.code != CTAuthorizationStatusNotDetermined) {
                if (completionHandler != nil) {
                    dispatch_async(dispatchQueue, ^{
                        completionHandler(authorizationStatusError);
                    });
                }
                return;
            }
            else {
                if (_isIOS9) {
                    CNContactStore *store = [[CNContactStore alloc] init];
                    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
                        // make sure the user granted us access
                        if (!granted) {
                            // Message is dispatched to returned queue
                            // User didn't grant access;
                            // so, again, tell user here why app needs permissions in order to do it's job;
                            if (completionHandler != nil) {
                                NSError *requestError = [NSError errorWithDomain:DOMAIN_STRING
                                                                            code:CTAuthorizationStatusDenied
                                                                        userInfo:@{NSLocalizedDescriptionKey: @"User have denied the request"}];
                                dispatch_async(dispatchQueue, ^{
                                    completionHandler(requestError);
                                });
                            }
                        }
                        else {
                            if (completionHandler != nil) {
                                dispatch_async(dispatchQueue, ^{
                                    completionHandler(nil);
                                });
                            }
                        }
                    }];
                }
                else {
                    CFErrorRef ABAressBookCreationError = NULL;
                    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &ABAressBookCreationError);
                    if (!addressBook) {
                        if (completionHandler != nil) {
                            dispatch_async(dispatchQueue, ^{
                                completionHandler((__bridge NSError *)(ABAressBookCreationError));
                            });
                        }
                    }
                    
                    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
                        if (!granted) {
                            //if they didn't give you permission, handle it gracefully, for example...
                            if (completionHandler != nil) {
                                NSError *requestError = [NSError errorWithDomain:DOMAIN_STRING
                                                                            code:CTAuthorizationStatusDenied
                                                                        userInfo:@{NSLocalizedDescriptionKey: @"User have denied the request"}];
                                dispatch_async(dispatchQueue, ^{
                                    completionHandler(requestError);
                                });
                            }
                            return;
                        }
                        else {
                            if (completionHandler != nil) {
                                dispatch_async(dispatchQueue, ^{
                                    completionHandler(nil);
                                });
                            }
                        }
                    });
                }
            }
        }
    });
}

- (void)getAllContactsWithCompletionHandler:(GetAllContactsCompletionHandler)completionHandler dispatchTo:(dispatch_queue_t)returnedQueue {
    dispatch_queue_t dispatchQueue = (returnedQueue != nil) ? returnedQueue : dispatch_get_main_queue();
    
    //Request permission to contact
    [self requestAccessWithCompletionHandler:^(NSError *error){
        // If user refused permission to contact.
        if (error != nil) {
            if (completionHandler != nil) {
                dispatch_async(dispatchQueue, ^{
                    completionHandler(nil, error);
                });
            }
            return;
        }
        else {
            if (_isIOS9) {
                NSLog(@"Version: iOS 9");
                
                CNContactStore *store = [[CNContactStore alloc] init];
                
                ContactImageCaching *cachedImage = [ContactImageCaching sharedContactImageCaching];
                
                NSArray *keys = @[CNContactIdentifierKey, CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey, CNContactThumbnailImageDataKey];
                NSString *containerId = store.defaultContainerIdentifier;
                NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
                NSError *fetchingError;
                NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&fetchingError];
                if (fetchingError) {
                    if (completionHandler != nil) {
                        dispatch_async(dispatchQueue, ^{
                            completionHandler(nil, fetchingError);
                        });
                    }
                    return;
                }
                NSMutableArray *tempContactList = [[NSMutableArray alloc] init];
                for (CNContact *contact in cnContacts) {
                    
                    //Step 1: Copy data to my custom Contacts class.
                    CTContactDetail *contactDetail = [[CTContactDetail alloc] init];
                    contactDetail.firstName = contact.givenName;
                    contactDetail.lastName = contact.familyName;
                    if ([contactDetail.lastName isEqual:@""]) {
                        contactDetail.fullName = [NSString stringWithFormat:@"%@",contactDetail.firstName];
                    }else if ([contactDetail.firstName isEqual:@""]){
                        contactDetail.fullName = [NSString stringWithFormat:@"%@",contactDetail.lastName];
                    }
                    else{
                        contactDetail.fullName = [NSString stringWithFormat:@"%@ %@", contactDetail.firstName, contactDetail.lastName];
                    }
                    contactDetail.contactIdentifier = contact.identifier;
                    
                    if (contact.imageData != nil) {
                        // Create image from NSData
                        UIImage *profileImage = [UIImage imageWithData:contact.imageData];
                        // Mask image with circle image
                        UIImage *mask = [UIImage imageNamed:@"mask4.png"];
                        profileImage = [profileImage maskImageWith:mask];
                        
                        //Add image to cache
                        [cachedImage cacheImage:profileImage forKey:contactDetail.contactIdentifier];
                    }
                    
                    NSString *phone;
                    for (CNLabeledValue *label in contact.phoneNumbers) {
                        phone = [label.value stringValue];
                        if ([phone length] > 0) {
                            [contactDetail.phoneNumbers addObject:phone];
                        }
                    }
                    //Add this contact to contact list
                    [tempContactList addObject:contactDetail];
                }
                
                if (completionHandler != nil) {
                    dispatch_async(dispatchQueue, ^{
                        completionHandler(tempContactList, nil);
                    });
                }
            }
            else {
                NSLog(@"Version: below iOS 9");
                
                CFErrorRef ABAressBookCreationError = NULL;
                ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &ABAressBookCreationError);
                if (!addressBook) {
                    if (completionHandler != nil) {
                        dispatch_async(dispatchQueue, ^{
                            completionHandler(nil, (__bridge NSError *)(ABAressBookCreationError));
                        });
                    }
                    return;
                }
                
                ContactImageCaching *cachedImage = [ContactImageCaching sharedContactImageCaching];
                
                // if they gave you permission, then just carry on
                NSArray *allContacts = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
                NSInteger numberOfContacts = [allContacts count];
                
                NSMutableArray *tempContactList = [[NSMutableArray alloc] init];
                for (NSInteger i = 0; i < numberOfContacts; ++i) {
                    ABRecordRef ABContact = (__bridge ABRecordRef)allContacts[i];
                    //Copy data to my custom Contacts class.
                    CTContactDetail *contactDetail = [[CTContactDetail alloc] init];
                    contactDetail.firstName = CFBridgingRelease(ABRecordCopyValue(ABContact, kABPersonFirstNameProperty));
                    contactDetail.lastName = CFBridgingRelease(ABRecordCopyValue(ABContact, kABPersonLastNameProperty));;
                    if (contactDetail.lastName == nil) {
                        contactDetail.fullName = [NSString stringWithFormat:@"%@",contactDetail.firstName];
                    }
                    else if (contactDetail.firstName == nil){
                        contactDetail.fullName = [NSString stringWithFormat:@"%@",contactDetail.lastName];
                    }
                    else {
                        contactDetail.fullName = [NSString stringWithFormat:@"%@ %@", contactDetail.firstName, contactDetail.lastName];
                    }
                    contactDetail.contactIdentifier = [NSString stringWithFormat:@"%i", ABRecordGetRecordID(ABContact)];
                    
                    NSData *imageData = (__bridge_transfer NSData *)ABPersonCopyImageData(ABContact);
                    
                    if (imageData != nil) {
                        // Create image from NSData
                        UIImage *profileImage = [UIImage imageWithData:imageData];
                        // Mask image with circle image
                        UIImage *mask = [UIImage imageNamed:@"mask4.png"];
                        profileImage = [profileImage maskImageWith:mask];
                        
                        //Add image to cache
                        [cachedImage cacheImage:profileImage forKey:contactDetail.contactIdentifier];
                    }
                    
                    //Add this contact to contact list
                    [tempContactList addObject:contactDetail];
                }
                //Run call back block in returned Queue.
                if (completionHandler != nil) {
                    dispatch_async(dispatchQueue, ^{
                        completionHandler(tempContactList, nil);
                    });
                }
                CFRelease(addressBook);
            }
        }
    } dispatchTo:_internalSerialQueue];
}

- (void)addContact:(CTContactDetail *)newContact withImage:(UIImage *)newImage dispatchTo:(dispatch_queue_t)returnedQueue withCompletionHandler:(AddContactCompletionHandler)completionHandler {
    dispatch_queue_t dispatchQueue = (returnedQueue != nil) ? returnedQueue : dispatch_get_main_queue();
    
    if (newContact == nil) {
        if (completionHandler != nil) {
            NSError *error = [NSError errorWithDomain:DOMAIN_STRING
                                                 code:CTNilContactAdded
                                             userInfo:@{NSLocalizedDescriptionKey: @"ERROR!! New contact must not be nil"}];
            dispatch_async(dispatchQueue, ^{
                completionHandler(nil, error);
            });
        }
        return;
    }
    //Request permission to contact
    [self requestAccessWithCompletionHandler:^(NSError *error){
        if (error != nil) {
            if (completionHandler != nil) {
                dispatch_async(dispatchQueue, ^{
                    completionHandler(nil, error);
                });
            }
            return;
        }
        if (_isIOS9) {
            NSLog(@"Version: iOS 9");
            CNContactStore *store = [[CNContactStore alloc] init];
            
            //Create contact
            CNMutableContact *contact = [[CNMutableContact alloc] init];
            CTContactDetail *contactDetail = [[CTContactDetail alloc] init];
            
            contact.familyName = newContact.lastName;
            contactDetail.lastName = newContact.lastName;
            
            contact.givenName = newContact.firstName;
            contactDetail.firstName = newContact.firstName;
            
            if ([contactDetail.lastName isEqual:@""]) {
                contactDetail.fullName = [NSString stringWithFormat:@"%@",contactDetail.firstName];
            }else if ([contactDetail.firstName isEqual:@""]){
                contactDetail.fullName = [NSString stringWithFormat:@"%@",contactDetail.lastName];
            }
            else{
                contactDetail.fullName = [NSString stringWithFormat:@"%@ %@", contactDetail.firstName, contactDetail.lastName];
            }
            
            if (newContact.phoneNumbers != nil && newContact.phoneNumbers.count > 0) {
                CNLabeledValue *homePhone = [CNLabeledValue labeledValueWithLabel:CNLabelHome value:[CNPhoneNumber phoneNumberWithStringValue:[newContact.phoneNumbers objectAtIndex:0]]];
                contact.phoneNumbers = @[homePhone];
                
                [contactDetail.phoneNumbers addObject:[newContact.phoneNumbers objectAtIndex:0]];
            }
            
            CNSaveRequest *request = [[CNSaveRequest alloc] init];
            [request addContact:contact toContainerWithIdentifier:nil];
            
            contactDetail.contactIdentifier = contact.identifier;
            // save it
            NSError *saveError;
            if (![store executeSaveRequest:request error:&saveError]) {
                if (completionHandler != nil) {
                    dispatch_async(dispatchQueue, ^{
                        completionHandler(nil, saveError);
                    });
                }
                return;
            }
            
            if (completionHandler != nil) {
                dispatch_async(dispatchQueue, ^{
                    completionHandler(contactDetail, nil);
                });
            }
        }
        else {
            NSLog(@"Version: below iOS 9");
            
            CFErrorRef ABAressBookCreationError = NULL;
            ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &ABAressBookCreationError);
            if (!addressBook) {
                if (completionHandler != nil) {
                    dispatch_async(dispatchQueue, ^{
                        completionHandler(nil, (__bridge NSError *)(ABAressBookCreationError));
                    });
                }
                return;
            }
            CFErrorRef error = NULL;
            
            ABRecordRef newPerson = ABPersonCreate();
            CTContactDetail *contactDetail = [[CTContactDetail alloc] init];
            
            ABRecordSetValue(newPerson, kABPersonLastNameProperty, (__bridge CFTypeRef)(newContact.lastName), &error);
            contactDetail.lastName = newContact.lastName;
            
            ABRecordSetValue(newPerson, kABPersonFirstNameProperty, (__bridge CFTypeRef)(newContact.firstName), &error);
            contactDetail.firstName = newContact.firstName;
            
            if ([contactDetail.lastName isEqual:@""]) {
                contactDetail.fullName = [NSString stringWithFormat:@"%@",contactDetail.firstName];
            }else if ([contactDetail.firstName isEqual:@""]){
                contactDetail.fullName = [NSString stringWithFormat:@"%@",contactDetail.lastName];
            }
            else{
                contactDetail.fullName = [NSString stringWithFormat:@"%@ %@", contactDetail.firstName, contactDetail.lastName];
            }
            
            if (newContact.phoneNumbers != nil && newContact.phoneNumbers.count > 0) {
                ABMutableMultiValueRef multiPhone = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                ABMultiValueAddValueAndLabel(multiPhone, (__bridge CFTypeRef)([newContact.phoneNumbers objectAtIndex:0]), kABPersonPhoneMainLabel, NULL);
                ABRecordSetValue(newPerson, kABPersonPhoneProperty, multiPhone,nil);
                CFRelease(multiPhone);
                
                [contactDetail.phoneNumbers addObject:[newContact.phoneNumbers objectAtIndex:0]];
            }
            // ...
            // Set other properties
            // ...
            error = NULL;
            ABAddressBookAddRecord(addressBook, newPerson, &error);
            if (error != NULL)
            {
                if (completionHandler != nil) {
                    dispatch_async(dispatchQueue, ^{
                        completionHandler(nil, (__bridge NSError *)(error));
                    });
                }
                return;
            }
            
            error = NULL;
            ABAddressBookSave(addressBook, &error);
            if (error != NULL)
            {
                if (completionHandler != nil) {
                    dispatch_async(dispatchQueue, ^{
                        completionHandler(nil, (__bridge NSError *)(error));
                    });
                }
                return;
            }
            CFRelease(newPerson);
            CFRelease(addressBook);
            
            contactDetail.contactIdentifier = [NSString stringWithFormat:@"%i", ABRecordGetRecordID(newPerson)];

            if (completionHandler != nil) {
                dispatch_async(dispatchQueue, ^{
                    completionHandler(contactDetail, nil);
                });
            }
        }
    } dispatchTo:_internalSerialQueue];
}

@end
