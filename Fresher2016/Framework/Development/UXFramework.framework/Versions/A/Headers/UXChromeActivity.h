/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

@interface UXChromeActivity : UIActivity

@property (strong, nonatomic) NSString *callbackSource;

@property (strong, nonatomic) NSString *activityTitle;

@property (strong, nonatomic) NSURL *callbackURL;

- (id)initWithCallbackURL:(NSURL *)callbackURL;

@end
