//
//  ThemeViewController.swift
//  BanLaKey
//
//  Created by VanDao on 9/5/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import Foundation
import UIKit

let screenSize: CGSize = UIScreen.mainScreen().bounds.size
let themesName: [String] = ["Sweet Hamburger", "Dark & Light", "Chemistry Bomb", "Black Hat", "First Love", "Strawberry Candy", "Merry Chrismast", "Blue In The Dark"]
let themesImage: [String] = ["theme1.png", "theme2.jpg", "theme3.png", "theme4.jpg", "theme5.png", "theme6.png", "theme7.png", "theme8.jpg"]
let navigationTitle = "Chọn chủ đề"
let navigationBackButtonTitle = "Trở về"
var curSelectedIndexPath: NSIndexPath? = nil

class ThemeViewController : UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        self.navigationItem.title = navigationTitle
        self.navigationItem.leftBarButtonItem?.title = navigationBackButtonTitle
        
        //
        let rightBarButtonItem: UIBarButtonItem = UIBarButtonItem.init(title: "Chọn", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(ThemeViewController.didTapOnRightBarButton(_:)))
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBarHidden = false
    }
    
    func didTapOnRightBarButton(sender: AnyObject) {
       // print("You chosen theme at index " + curSelectedIndexPath.description)
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return themesName.count
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        if let curIndexPath = curSelectedIndexPath {
            let cell: CTCollectionViewCell = collectionView.cellForItemAtIndexPath(curIndexPath) as! CTCollectionViewCell
            cell.backgroundColor = UIColor.clearColor()
        }
        
        let cell: CTCollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath) as! CTCollectionViewCell
        cell.backgroundColor = UIColor.init(red: 236/255.0, green: 207/255.0, blue: 195/255.0, alpha: 0.8)
        curSelectedIndexPath = indexPath
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        return CGSize.init(width: screenSize.width / 2 - 5, height: (screenSize.width / 2 - 5) * 216 / 320 + 40)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell: CTCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("collectionviewcell", forIndexPath: indexPath) as! CTCollectionViewCell
        cell.themeName.text = themesName[indexPath.item]
        cell.themeImage.image = UIImage.init(named: themesImage[indexPath.item])
        
        return cell
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
}