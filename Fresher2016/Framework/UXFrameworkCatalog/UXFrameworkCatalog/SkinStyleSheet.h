/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UXFramework/UXFramework.h>

@interface SkinStyleSheet : UXJSONDefaultStyleSheet

@end
