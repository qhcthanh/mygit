//
//  ContactTableViewCell.h
//  w1UITableView
//
//  Created by manhduydl on 5/18/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDContactImageView.h"

@interface ContactTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *accessoryImageView;
@property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (weak, nonatomic) IBOutlet MDContactImageView *avatarImageView;

@end
