/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXURLDataResponse.h"

typedef NS_ENUM(NSInteger, HttpRequestType) {
    kHttpRequestTypeGET     = 0,
    kHttpRequestTypePOST    = 1,
    kHttpRequestTypePUT     = 2,
    kHttpRequestTypeDELETE  = 3
};

typedef void (^URLRequestWithSuccessBlock)(id responseObject);
typedef void (^URLRequestWithFailureBlock)(NSError *error);

@protocol UXMutableURLRequestDelegate;
@interface UXMutableURLRequest : NSMutableURLRequest {

}

- (instancetype)initWithURLString:(NSString*)url withParameters:(NSDictionary*)parameters;
- (instancetype)initWithURLString:(NSString*)url withParameters:(NSDictionary*)parameters withTimeout:(NSTimeInterval)timeout;

@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) BOOL shouldInModelQueue;
@property (nonatomic, assign) NSInteger logQOSRequestId;
@property (nonatomic, assign) HttpRequestType httpRequestType;
@property (nonatomic, strong) NSMutableDictionary *parameters;
@property (nonatomic, retain) id<UXURLDataResponse> response;
@property (nonatomic, weak) id<UXMutableURLRequestDelegate> delegate;
@property (nonatomic, copy) URLRequestWithSuccessBlock urlRequestWithSuccessBlock;
@property (nonatomic, copy) URLRequestWithFailureBlock urlRequestWithFailureBlock;

@end

@protocol UXMutableURLRequestDelegate <NSObject>

/**
 * The request has begun loading.
 *
 * This method will not be called if the data is loaded immediately from the cache.
 * @see accessDidStartLoad:
 */
- (void)requestDidStartLoad:(UXMutableURLRequest*)request;

/**
 * The request has loaded data and been processed into a response.
 *
 * If the request is served from the cache, this is the only delegate method that will be called.
 */
- (void)requestDidFinishLoad:(UXMutableURLRequest*)request;

/**
 * The request failed to load.
 */
- (void)request:(UXMutableURLRequest*)request didFailLoadWithError:(NSError*)error;

/**
 * The request was canceled.
 */
- (void)requestDidCancelLoad:(UXMutableURLRequest*)request;

@end

