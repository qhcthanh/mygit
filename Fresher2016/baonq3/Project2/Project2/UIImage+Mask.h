//
//  UIImage+Mask.h
//  Project2
//
//  Created by BaoNQ on 6/6/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage (Mask)

/**
 *  Mask an image into a specified layer
 *
 *  @param mask Mask image
 *
 *  @return An image masked into the layer
 */
- (UIImage *)maskImageWith:(UIImage *)mask;

@end
