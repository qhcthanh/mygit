//
//  CTFilterView.h
//  CameraGPUImage
//
//  Created by BaoNQ on 6/27/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SNFilter.h"

@interface PageView : UIImageView

- (void)updateMask:(CGRect)maskRect newXPosition:(CGFloat)nX;

@end
