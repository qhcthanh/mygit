//
//  FBUserManager.h
//  FacebookContact
//
//  Created by qhcthanh on 7/14/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FBUserInfo.h"

@interface FBUserManager : NSObject


/**
 *  Singleton method's of FBUserManager
 */
+ (id)shareManager;

/**
 *  Set current user FBUserInfo. CurrentUser is user login in app
 *
 *  @param currentUser A User login in app
 */
- (void)setCurrentUser:(FBUserInfo *)currentUser;

/**
 *  Get current in this app
 *
 *  @return A current user if user is not login current user is null
 */
- (FBUserInfo*)getCurrentUser;

@end
