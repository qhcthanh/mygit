/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXFileLogger.h"
#import "UXActionLogDetail.h"

@protocol UXLogFileManagerDefaultDelegate;
@interface UXActionLogManager : NSObject {
    NSInteger _logActionType;
    NSInteger _logActionInterval;
}

@property (nonatomic, assign) NSInteger logActionType;
@property (nonatomic, assign) NSInteger logActionInterval;

@property (nonatomic, assign) uint64_t didEnterBackgroundTime;
@property (nonatomic, assign) uint64_t didEnterForegroundTime;

+ (UXActionLogManager*)getInstance;
+ (UXActionLogManager*)createInstanceOfType:(uint16_t)logActionType withInterval:(uint32_t)logActionInterval withCallback:(id <UXLogFileManagerDefaultDelegate>)delegate;

+ (NSString *)formatLogMessage:(UXLogMessage *)logMessage;

+ (void)addDefaultActionLogWithId:(NSString*)logId completionBlock:(void(^)(void))completionBlock;

- (BOOL)addDetail:(UXActionLogDetail*) newLogAct;
- (UXActionLogDetail*)getDetail:(NSString*) key;
- (BOOL)isValidDetail:(UXActionLogDetail*) newLogAct;

@end
