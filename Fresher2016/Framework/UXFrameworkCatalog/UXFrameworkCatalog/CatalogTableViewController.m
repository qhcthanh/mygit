/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "CatalogTableViewController.h"

#import "StyledViewController.h"
#import "InputViewController.h"
#import "BadgeViewController.h"
#import "VerticalViewController.h"
#import "HorizontalViewController.h"
#import "LauncherViewController.h"
#import "InterAppViewController.h"
#import "MutableTableViewController.h"
#import "AttributedViewController.h"
#import "NetworkImageViewController.h"
#import "AccessoryTableViewController.h"
#import "PopupViewController.h"
#import "HorizontalImageViewController.h"

@interface CatalogTableViewController () {
    // The UITableView refers to the model and actions objects throughout its lifetime but does not
    // retain them. We must store the instances in this class.
    UXTableViewDataSource* _model;
    UXTableViewAction* _actions;
}

@end

@implementation CatalogTableViewController

- (id)initWithStyle:(UITableViewStyle)style {
    // We explicitly set the table view style in this controller's implementation because we want this
    // controller to control how the table view is displayed.
    self = [super initWithStyle:UITableViewStyleGrouped];
    
    //__weak typeof(self) weekSeft = self;
    
    if (self) {
        // We set the title in the init method because it will never change.
        self.titleString = @"Catalog";
        
        // When we instantiate the actions object we must provide it with a reference to the target to
        // whom actions will be sent.
        //
        // For block actions:
        // This value is passed to the block as an argument so that we can navigate to new
        // controllers without introducing retain cycles by otherwise having to access
        // self.navigationController in the block.
        //
        // For selector actions:
        // The selector will be executed on the target. For a demo of selector actions, see
        // ActionsTableModelViewController.m.
        _actions = [[UXTableViewAction alloc] initWithTarget:self];
        
        // This controller uses the UXFramwork table view model. In loose terms, UXFramwork models implement
        // data source protocols. They avoid code duplication and abstract the storage of the backing
        // data for views. There is a model for UITableView, UXTableViewDataSource, and a model for
        // UICollectionView, UXCollectionViewModel.
        //
        // We're going to use UXTableViewDataSource with a sectioned array of objects. A sectioned array of
        // objects is an NSArray where any instance of an NSString delimits the beginning of a new
        // section.
        NSArray* sectionedObjects =
        @[
          
          @"STYLE",
          
          [_actions attachToObject:
           [UXSubtitleTableViewCellObject objectWithTitle:@"Basic Style"
                                                 subtitle:@"How to create a simple Style"]
                   navigationBlock:UXPushViewControllerAction([StyledViewController class], nil)],
          // An NSString in a sectioned array denotes the start of a new section. It's also the label of
          // the section header.
          @"ATTRIBUTED LABEL",
          [_actions attachToObject:
           [UXSubtitleTableViewCellObject objectWithTitle:@"Customizing Text"
                                        subtitle:@"How to use NSAttributedString"]
                   navigationBlock:^BOOL(id object, UIViewController* controller, NSIndexPath *indexPath) {
                       
                       // Init attribute view controller
                       AttributedViewController* attributedViewController = [[AttributedViewController alloc] init];
                       
                       /**
                        * Push a view controller with completion handler
                        
                       UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:attributedViewController];
                       
                       [(UXNavigationController*)controller.navigationController presentViewController:navigationController animated:YES completion:^{
                           [(UXNavigationController*)controller.navigationController removeFromParentViewController];
                       }];*/
                       
                       [(UXNavigationController*)controller.navigationController pushViewController:attributedViewController
                                                                                           animated:YES
                                                                                         completion:^(UINavigationController *navigationController, UIViewController *viewController)
                        {
                            // Assign the button to the view controller.
                            [viewController setTitle:@"Completion!!!"];
                        }];
                       return NO;
                   }],
//          
//          [_actions attachToObject:
//           [UXSubtitleTableViewCellObject objectWithTitle:@"Long Taps"
//                                        subtitle:@"Configuring long tap action sheets"]
//                   navigationBlock:UXPushControllerAction([LongTapAttributedLabelViewController class])],
//          
          [_actions attachToObject:
           [UXSubtitleTableViewCellObject objectWithTitle:@"Interface Builder"
                                        subtitle:@"Using attributed labels in IB"]
                   navigationBlock:^BOOL(id object, id target, NSIndexPath *indexPath) {
                       
                       UXURLAction* urlaction = [[UXURLAction alloc] initWithURLPath:@"ux://IBAttributedLabelViewController"];
                       urlaction.animated = YES;
                       [[UXNavigationManager navigator] openURLAction:urlaction];
                       
                       return NO;
                   }],
//
//          [_actions attachToObject:
//           [UXSubtitleTableViewCellObject objectWithTitle:@"Alignment"
//                                        subtitle:@"Verical alignment in attributed labels"]
//                   navigationBlock:UXPushControllerAction([AlignmentAttributedLabelViewController class])],
//          
          @"Badge",
          
          [_actions attachToObject:
           [UXSubtitleTableViewCellObject objectWithTitle:@"Basic Instantiation"
                                        subtitle:@"How to create a simple UXBadgeView"]
                   navigationBlock:^BOOL(id object, id target, NSIndexPath *indexPath) {
                     
                       UXURLAction* urlaction = [[UXURLAction alloc] initWithURLPath:@"ux://BadgeViewController"];
                       urlaction.animated = YES;
                       [[UXNavigationManager navigator] openURLAction:urlaction];
                       
                       return NO;
                   }],
//
//          [_actions attachToObject:
//           [UXSubtitleTableViewCellObject objectWithTitle:@"Customizing Badges"
//                                        subtitle:@"How to customize badges"]
//                   navigationBlock:UXPushControllerAction([CustomizingBadgesViewController class])],
//          
//          [_actions attachToObject:
//           [UXSubtitleTableViewCellObject objectWithTitle:@"Interface Builder"
//                                        subtitle:@"Using badges in IB"]
//                   navigationBlock:UXPushControllerAction([InterfaceBuilderBadgeViewController class])],
//          
          @"Collections",
          [_actions attachToObject:
           [UXSubtitleTableViewCellObject objectWithTitle:@"Custom Nibs"
                                        subtitle:@"How to customize nibs"]
                   navigationBlock:^BOOL(id object, id target, NSIndexPath *indexPath) {
                       UXURLAction* urlaction = [[UXURLAction alloc] initWithURLPath:@"ux://CustomNibCollectionModelViewController"];
                       urlaction.animated = YES;
                       [[UXNavigationManager navigator] openURLAction:urlaction];
                       
                       return NO;
                   }],
          
          @"Interapp",
          
          [_actions attachToObject:
           [UXSubtitleTableViewCellObject objectWithTitle:@"All Actions"
                                        subtitle:@"A list of all available actions"]
                   navigationBlock:UXPushViewControllerAction([InterAppViewController class], nil)],

          @"Launcher",
          [_actions attachToObject:
           [UXSubtitleTableViewCellObject objectWithTitle:@"Modifying"
                                        subtitle:@"How to add new launcher buttons"]
                   navigationBlock:UXPushViewControllerAction([LauncherViewController class], nil)],
          @"Network Image",
          
          [_actions attachToObject:
           [UXSubtitleTableViewCellObject objectWithTitle:@"Network Image View"
                                        subtitle:@"How to create a UXNetworkImageView"]
                   navigationBlock:UXPushViewControllerAction([NetworkImageViewController class], nil)],
          
//          [_actions attachToObject:
//           [UXSubtitleTableViewCellObject objectWithTitle:@"Content Modes"
//                                        subtitle:@"Effects of each content mode"]
//                   navigationBlock:UXPushControllerAction([ContentModesNetworkImageViewController class])],
          
          @"Paging Scroll Views",
          
          [_actions attachToObject:
           [UXSubtitleTableViewCellObject objectWithTitle:@"Horizontal Paging"
                                        subtitle:@"Using a horizontal layout"]
                   navigationBlock:UXPushViewControllerAction([HorizontalViewController class], nil)],

          [_actions attachToObject:
           [UXSubtitleTableViewCellObject objectWithTitle:@"Vertical Paging"
                                        subtitle:@"Using a vertical layout"]
                   navigationBlock:UXPushViewControllerAction([VerticalViewController class], nil)],

          @"Table Models",
//
//          [_actions attachToObject:
//           [UXSubtitleTableViewCellObject objectWithTitle:@"Basic Instantiation"
//                                        subtitle:@"How to create a table view model"]
//                   navigationBlock:UXPushControllerAction([BasicInstantiationTableModelViewController class])],
//          
//          [_actions attachToObject:
//           [UXSubtitleTableViewCellObject objectWithTitle:@"Sectioned Model"
//                                        subtitle:@"Sectioned table view models"]
//                   navigationBlock:UXPushControllerAction([SectionedTableModelViewController class])],
//          
//          [_actions attachToObject:
//           [UXSubtitleTableViewCellObject objectWithTitle:@"Indexed Model"
//                                        subtitle:@"Indexed table view models"]
//                   navigationBlock:UXPushControllerAction([IndexedTableModelViewController class])],
//          
          [_actions attachToObject:
           [UXSubtitleTableViewCellObject objectWithTitle:@"Actions"
                                        subtitle:@"Handling actions in table views"]
                   navigationBlock:UXPushViewControllerAction([AccessoryTableViewController class], nil)],
          [_actions attachToObject:
           [UXSubtitleTableViewCellObject objectWithTitle:@"Form Cell Catalog"
                                        subtitle:@"Table view cells for forms"]
                   navigationBlock:UXPushViewControllerAction([InputViewController class], nil)],
   
          [_actions attachToObject:
           [UXSubtitleTableViewCellObject objectWithTitle:@"Mutable Models"
                                        subtitle:@"Mutating table view models"]
                   navigationBlock:UXPushViewControllerAction([MutableTableViewController class], nil)],
          
          @"Popover",
          [_actions attachToObject:
           [UXSubtitleTableViewCellObject objectWithTitle:@"Popup over"
                                        subtitle:@"How to create popup"]
                   navigationBlock:UXPushViewControllerAction([PopupViewController class], nil)],
          
          @"Filtered Image Scroll Views",
          
          [_actions attachToObject:
           [UXSubtitleTableViewCellObject objectWithTitle:@"Horizontal Paging"
                                                 subtitle:@"Using a horizontal layout"]
                   navigationBlock:UXPushViewControllerAction([HorizontalImageViewController class], nil)],

          ];
        
        // When we create the model we must provide it with a delegate that implements the
        // UXTableViewDataSourceDelegate protocol. This protocol has a single method that is used to create
        // cells given an object from the model. If we don't require any custom cell bindings then it's
        // often easiest to use the NICellFactory as the delegate. The NICellFactory class provides a
        // barebones implementation that is sufficient for nearly all applications.
        _model = [[UXTableViewDataSource alloc] initWithSectionedArray:sectionedObjects
                                                         delegate:(id)[UXTableViewCellFactory class]];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Once the tableView has loaded we attach the model to the data source. As mentioned above,
    // UXTableViewDataSource implements UITableViewDataSource so that you don't have to implement any
    // of the data source methods directly in your controller.
    self.tableView.dataSource = _model;
    
    // What we're doing here is known as "delegate chaining". It uses the message forwarding
    // functionality of Objective-C to insert the actions object between the table view
    // and this controller. The actions object forwards UITableViewDelegate methods along and
    // selectively intercepts methods required to make user interactions work.
    //
    // Experiment: try commenting out this line. You'll notice that you can no longer tap any of
    // the cells in the table view and that they no longer show the disclosure accessory types.
    // Cool, eh? That this functionality is all provided to you in one line should make you
    // heel-click.
    self.tableView.delegate = [_actions forwardingTo:self];
}

- (void)setRandomBackgroundColor:(UIViewController*)viewController {
    
    CGFloat red = arc4random() % 255; CGFloat green = arc4random() % 255; CGFloat blue = arc4random() % 255;
    UIColor *randomColor = [UIColor colorWithRed:red/255 green:green/255 blue:blue/255 alpha:1];
    viewController.view.backgroundColor = randomColor;
}

@end
