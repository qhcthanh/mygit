//
//  BLKeyboard.swift
//  BanLaKey
//
//  Created by BaoNQ on 8/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

protocol BLKeyboardDelegate: class {
    func textProxyWillChange(textDocumentProxy: UITextDocumentProxy, withCharacter: String)
    func textProxyRemoveCompleted(textDocumentProxy: UITextDocumentProxy)
    func textProxyWillRemove(textDocumentProxy: UITextDocumentProxy, numberOfCharacters: Int)
    func textProxyWillRemoveAll(textDocumentProxy: UITextDocumentProxy)
}

let isIPad: Bool = {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
        return true
    }
    return false
}()

#if isIPad
    typealias KeyboardMetrics = PadKeyboardMetrics
#else
    typealias KeyboardMetrics = PhoneKeyboardMetrics
#endif

class BLKeyboard : UIView {

    var textInputVC: UIInputViewController?
    //var imageView: UIImageView?
    var timer: NSTimer?
    
    var scribbleView: BLScribbleView?
    var delegate: BLKeyboardDelegate?

    var pages: [BLPage]
    
    var keyboardLayout: BLKeyLayout
    
    var currentPage: Int
      
    // Other keys
    var shiftButton: BLLockKey?
    var deleteButton: BLKey?
    var globeButton: BLKey?
    var spaceButton: BLKey?
    var returnButton: BLKey?
    var keyModeButton: BLKey?

    
//    // Number keys
//    var zeroButton: BLKey?
//    var oneButton: BLKey?
//    var twoButton: BLKey?
//    var threeButton: BLKey?
//    var fourButton: BLKey?
//    var fiveButton: BLKey?
//    var sixButton: BLKey?
//    var sevenButton: BLKey?
//    var eightButton: BLKey?
//    var nineButton: BLKey?
//    
//    var minusButton: BLKey? // -
//    var slashButton: BLKey? // /
//    var colonButton: BLKey? // :
//    var semicolonButton: BLKey? // ;
//    var leftParenthesisButton: BLKey? // (
//    var rightParenthesisButton: BLKey? // )
//    var dollarSignButton: BLKey? // $
//    var ampersandButton: BLKey? // &
//    var atSignButton: BLKey? // @
//    var quotationMarkButton: BLKey? // "
//    var fullStopButton: BLKey? // .
//    var commaButton: BLKey? // ,
//    var questionMarkButton : BLKey? // ?
//    var exclamationMarkButton: BLKey? // !
//    var apostropheButton: BLKey? // '
//    var graveAccentButton: BLKey? // `
//    
//    
//    // Number keys page 2
//    var leftSquareBracketButton: BLKey? // [
//    var rightSquareBracketButton: BLKey? // ]
//    var leftCurlyBracketButton: BLKey? // {
//    var rightCurlyBracketButton: BLKey? // }
//    var sharpButton: BLKey? // #
//    var percentSignButton: BLKey? // %
//    var circumflexAccentButton: BLKey? // ^
//    var asteriskButton: BLKey? // *
//    var plusSignButton: BLKey? // +
//    var equalSignButton: BLKey? // =
//    var lowLineButton: BLKey? // _
//    var backslashButton: BLKey? // \
//    var verticalBarButton: BLKey? // |
//    var tildeButton: BLKey? // ~
//    var lessThanSignButton: BLKey? // <
//    var greaterThanSignButton: BLKey? // >
//    var euroSignButton: BLKey? // €
//    var poundSignButton: BLKey? // £
//    var yenSignButton: BLKey? // ¥
//    var centSignButton: BLKey? // ¢
//    
//    var blackCircleButton: BLKey? // ●
//    var registerdSignButton: BLKey? // ®
//    var copyrightSignButton: BLKey? // ©
//    var degreeCelsiusButton: BLKey? // ℃
//    var degreeFahrenheitButton: BLKey? // ℉
    
    init(frame: CGRect, inputViewController: UIInputViewController) {
        
        self.textInputVC = inputViewController
        
        self.scribbleView = BLScribbleView.init(frame: CGRectMake(0, 0, frame.width, frame.height))
        
        self.keyboardLayout = BLKeyLayout.init(keyboardWidth: frame.size.width, keyboardHeight: frame.size.height)

        self.currentPage = 0
        

        var image: UIImage = UIImage.init(named: BLThemeManager.currentTheme.shiftDefaultButtonImage)!
        self.shiftButton = BLLockKey.init(frame: CGRectZero, title: "", image: image, keyType: BLKeyType.ShiftKey, backgroundColor: BLThemeManager.currentTheme.shiftButton)
        
        image = UIImage.init(named: BLThemeManager.currentTheme.deleteButtonImage)!
        self.deleteButton = BLKey.init(frame: CGRectZero, title: "", image: image, keyType: BLKeyType.BackspaceKey, backgroundColor: BLThemeManager.currentTheme.deleteButton)
        
        self.keyModeButton = BLKey.init(frame: CGRectZero, title: "123", keyType: BLKeyType.ChangeModeKey, backgroundColor: BLThemeManager.currentTheme.keyModeButton)
        
        image = UIImage.init(named: BLThemeManager.currentTheme.globeButtonImage)!
        self.globeButton = BLKey.init(frame: CGRectZero, title: "", image: image, keyType: BLKeyType.GlobeKey, backgroundColor: BLThemeManager.currentTheme.globeButton)
        
        self.spaceButton = BLKey.init(frame: CGRectZero, title: "Space", keyType: BLKeyType.SpaceKey, backgroundColor: BLThemeManager.currentTheme.spaceButton)
        self.returnButton = BLKey.init(frame: CGRectZero, title: "Return", keyType: BLKeyType.ReturnKey, backgroundColor: BLThemeManager.currentTheme.returnButton)
        
        self.touchToView = [:]
        self.pages = []
        
        super.init(frame: frame)
        
        self.contentMode = UIViewContentMode.Redraw
        self.multipleTouchEnabled = true
        self.userInteractionEnabled = true
        self.opaque = false
        self.backgroundColor = .clearColor()
        
//        if BLThemeManager.currentTheme.backgroundImage != "" {
//            self.imageView = UIImageView()
//            
//            self.imageView!.frame = self.frame
//            self.imageView!.image = UIImage.init(named: BLThemeManager.currentTheme.backgroundImage)
//            
//            if self.imageView!.image != nil {
//                self.addSubview(self.imageView!)
//            }
//        }

        
        self.addSubview(self.shiftButton!)
        self.addSubview(self.deleteButton!)
        self.addSubview(self.keyModeButton!)
        self.addSubview(self.globeButton!)
        self.addSubview(self.spaceButton!)
        self.addSubview(self.returnButton!)
        
        
        self.shiftButton?.addTarget(self, action: #selector(shiftButtonTapped), forControlEvents: UIControlEvents.TouchDown)
        self.shiftButton?.addTarget(self, action: #selector(shiftButtonDoubleTapped), forControlEvents: UIControlEvents.TouchDownRepeat)
        
        self.deleteButton?.addTarget(self, action: #selector(deleteButtonTapped), forControlEvents: UIControlEvents.TouchDown)
        self.deleteButton?.addTarget(self, action: #selector(deleteButtonReleased), forControlEvents: UIControlEvents.TouchUpInside)
        self.deleteButton?.addTarget(self, action: #selector(deleteButtonReleased), forControlEvents: UIControlEvents.TouchUpOutside)

        self.globeButton?.addTarget(self, action: #selector(globeButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.spaceButton?.addTarget(self, action: #selector(spaceButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        self.returnButton?.addTarget(self, action: #selector(returnButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        
        self.keyModeButton?.addTarget(self, action: #selector(changeModeButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
        
        
        self.addSubview(self.scribbleView!)
        self.bringSubviewToFront(self.scribbleView!)
        self.scribbleView?.layer.zPosition = self.scribbleViewLayerZPosition
    }
    
    
    // POPUP DELAY //
    
    var keyWithDelayedPopup: BLKey?
    var popupDelayTimer: NSTimer?
    
    func showPopup(sender: BLKey) {

        if sender == self.keyWithDelayedPopup {
            self.popupDelayTimer?.invalidate()
        }
        sender.showPopup()
    }
    
    func hidePopupDelay(sender: BLKey) {
        
        self.popupDelayTimer?.invalidate()
        
        if sender != self.keyWithDelayedPopup {
            self.keyWithDelayedPopup?.hidePopup()
            self.keyWithDelayedPopup = sender
        }
        
        if sender.popupShowed == true {
            self.popupDelayTimer = NSTimer.scheduledTimerWithTimeInterval(0.05, target: self, selector: #selector(hidePopupCallback), userInfo: nil, repeats: false)
        }
    }
    
    func hidePopupCallback() {
        
        self.keyWithDelayedPopup?.hidePopup()
        self.keyWithDelayedPopup = nil
        self.popupDelayTimer = nil
    }
    
    // POPUP DELAY END //
    
    func addKey(key: BLKey, row: Int, page: Int) {
        if self.pages.count <= page {
            for _ in self.pages.count...page {
                self.pages.append(BLPage())
            }
        }
        
        self.pages[page].addKey(key, row: row)
    }
    
    func updateState(frame: CGRect) {
        
        self.frame = frame
        //self.imageView?.frame = CGRectMake(0, 0, self.frame.width, self.frame.height)

        self.scribbleView?.frame = CGRectMake(0, 0, frame.width, frame.height)
        
        self.keyboardLayout.updateKeyboardMetrics(frame.size.width, keyboardHeight: frame.size.height)
        self.keyboardLayout.layoutKeyboard(self, currentPage: self.currentPage)
        
        self.setNeedsDisplay()
    }
    
    func characterButtonTapped(sender: BLKey) {
        
        if (self.delegate != nil && sender.getTitle() != nil) {
            self.delegate?.textProxyWillChange(self.textInputVC!.textDocumentProxy, withCharacter: sender.getTitle()!.lowercaseString)
        }
    }
    
    func shiftButtonTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Default {
            self.shiftButton!.lockState = BLLockKeyType.Once
            self.shiftButton!.selected = true
        }
        else {
            self.shiftButton!.lockState = BLLockKeyType.Default
            self.shiftButton!.selected = false
        }
    }
    func shiftButtonDoubleTapped() {
        if self.shiftButton!.lockState == BLLockKeyType.Forever {
            self.shiftButton!.lockState = BLLockKeyType.Default
            self.shiftButton!.selected = false
        }
        else {
            self.shiftButton!.lockState = BLLockKeyType.Forever
            self.shiftButton!.selected = true
        }
    }

    let deleteButtonTouchTimeInterval = 0.5
    
    
    var backspaceDelayTimer: NSTimer?
    var backspaceRepeatTimer: NSTimer?
    var backspaceRemoveAllTimer: NSTimer?
    var backspaceRemoveBlockTimer: NSTimer?
    
    let numberOfCharacterInBlock = 20
    let backspaceDelayTimeInterval = 0.5
    let backspaceRemoveBlockTimeInterval = 2.5
    let backspaceRemoveAllTimeInterval = 6.0
    
    func cancelBackspaceTimers() {
        self.backspaceDelayTimer?.invalidate()
        self.backspaceRepeatTimer?.invalidate()
        self.backspaceRemoveBlockTimer?.invalidate()
        self.backspaceRemoveAllTimer?.invalidate()
        
        self.backspaceDelayTimer = nil
        self.backspaceRepeatTimer = nil
        self.backspaceRemoveBlockTimer = nil
        self.backspaceRemoveAllTimer = nil
        
    }
    
    func deleteButtonTapped(sender: BLKey) {
       // self.cancelBackspaceTimers()
        
        self.delegate?.textProxyWillRemove(self.textInputVC!.textDocumentProxy, numberOfCharacters: 1)
        //self.setCapsIfNeeded()
        
        // trigger for subsequent deletes
        self.backspaceDelayTimer = NSTimer.scheduledTimerWithTimeInterval(backspaceDelayTimeInterval, target: self, selector: #selector(self.backspaceDelayCallback), userInfo: nil, repeats: false)
        self.backspaceRemoveBlockTimer = NSTimer.scheduledTimerWithTimeInterval(backspaceRemoveBlockTimeInterval, target: self, selector: #selector(self.backspaceRemoveBlockCallback), userInfo: nil, repeats: false)
        self.backspaceRemoveAllTimer = NSTimer.scheduledTimerWithTimeInterval(backspaceRemoveAllTimeInterval, target: self, selector: #selector(self.backspaceRemoveAllCallback), userInfo: nil, repeats: false)
    }
    
    func deleteButtonReleased(sender: BLKey) {
        self.delegate?.textProxyRemoveCompleted(self.textInputVC!.textDocumentProxy)
        self.cancelBackspaceTimers()
    }
    
    func backspaceRemoveBlockCallback() {
        self.backspaceRemoveBlockTimer = nil
        self.backspaceRepeatTimer?.invalidate()
        self.backspaceRepeatTimer = nil
        
        self.backspaceRepeatTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(backspaceRemoveBlockRepeatCallback), userInfo: nil, repeats: true)
    }
    
    func backspaceRemoveBlockRepeatCallback() {
        self.delegate?.textProxyWillRemove(self.textInputVC!.textDocumentProxy, numberOfCharacters: numberOfCharacterInBlock)
        //self.setCapsIfNeeded()
    }
    
    func backspaceDelayCallback() {
        self.backspaceDelayTimer = nil
        self.backspaceRepeatTimer = NSTimer.scheduledTimerWithTimeInterval(0.08, target: self, selector: #selector(backspaceRepeatCallback), userInfo: nil, repeats: true)
    }
    
    func backspaceRepeatCallback() {
        
        self.delegate?.textProxyWillRemove(self.textInputVC!.textDocumentProxy, numberOfCharacters: 1)
        //self.setCapsIfNeeded()
    }
    
    func backspaceRemoveAllCallback() {
        self.backspaceRemoveAllTimer = nil
        self.backspaceRepeatTimer?.invalidate()
        self.backspaceRepeatTimer = nil
        self.backspaceRemoveBlockTimer?.invalidate()
        self.backspaceRemoveBlockTimer = nil
        
        self.delegate?.textProxyWillRemoveAll(self.textInputVC!.textDocumentProxy)
    }
    
    func globeButtonTapped() {
        self.textInputVC?.advanceToNextInputMode()
    }
    
    func changeModeButtonTapped() {
        if self.currentPage == 0 {
            self.currentPage = 1
            self.keyModeButton?.setTitle("ABC")
        }
        else {
            self.currentPage = 0
            self.keyModeButton?.setTitle("123")
        }
        self.keyboardLayout.layoutKeyboard(self, currentPage: self.currentPage)
    }

    
    func spaceButtonTapped() {
        if (self.delegate != nil) {
            self.delegate?.textProxyWillChange(self.textInputVC!.textDocumentProxy, withCharacter: " ")
        }
    }
    func returnButtonTapped() {
        self.textInputVC!.textDocumentProxy.insertText("\n")
    }
    
    func deleteTimer(timer: NSTimer) {
        if self.deleteButton!.highlighted {
            self.deleteBackward()
        }
        else {
            timer.invalidate()
            self.timer = nil
        }
    }
    
    func deleteBackward() {
        self.textInputVC!.textDocumentProxy.deleteBackward()
        
        self.updateReturnButtonState()
        self.updateShiftButtonState()
    }
    
    func updateReturnButtonState() {
        if let textInputVC = self.textInputVC,
            let documentContextBeforeInput = textInputVC.textDocumentProxy.documentContextBeforeInput,
            let documentContextAfterInput = textInputVC.textDocumentProxy.documentContextAfterInput
            where
            documentContextBeforeInput.characters.count == 0 && documentContextAfterInput.characters.count == 0
        {
            
            self.returnButton!.enabled = false
        }
        else {
            self.returnButton!.enabled = true
        }
    }
    
    func updateShiftButtonState() {
//        if self.shiftButton!.lockState == BLLockKeyType.Forever {
//            return
//        }
//        
//        var selected: Bool = false
//        
//        let beforeInput: String = self.textInputVC!.textDocumentProxy.documentContextBeforeInput!
//        
//        switch self.textInputVC!.textDocumentProxy.autocapitalizationType {
//        case UITextAutocapitalizationType.AllCharacters:
//            selected = true
//            
//            break
//        case UITextAutocapitalizationType.Sentences:
//            self
//            if beforeInput.characters.count == 0 ||
//            
//            break
//        default:
//            
//        }
    }
    
    
    
    /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///
    
    var touchToView: [UITouch:UIView]
    var swipeTimer: NSTimer?
    let kBLSwipeTimeInterval: NSTimeInterval = 0.2
    var cancelTouchesEnd: Bool = false
    
    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
        if (self.hidden || self.alpha == 0 || !self.userInteractionEnabled) {
            return nil
        }
        else {
            return CGRectContainsPoint(self.bounds, point) ? self : nil
        }
    }
    
    func handleControl(view: UIView?, controlEvent: UIControlEvents) {
        if let control = view as? BLKey {
            let targets = control.allTargets()
            for target in targets {
                if let actions = control.actionsForTarget(target, forControlEvent: controlEvent) {
                    for action in actions {
                        let selectorString = action
                        let selector = Selector(selectorString)
                        
                        control.sendAction(selector, to: target, forEvent: nil)
                    }
                }
            }
        }
    }
    
    func handleControl(seletor selectorString: String, view: UIView?, controlEvent: UIControlEvents) {
        if let control = view as? BLKey {
            let targets = control.allTargets()
            for target in targets {
                if let actions = control.actionsForTarget(target, forControlEvent: controlEvent) {
                    for action in actions {
                        print(action)
                        if action == selectorString {
                            let selector = Selector(action)
                            
                            control.sendAction(selector, to: target, forEvent: nil)
                        }
                    }
                }
            }
        }
    }
    
    func findNearestView(position: CGPoint, eliminatedViews: [UIView]) -> UIView? {
        if !self.bounds.contains(position) {
            return nil
        }
        
        var closest: (UIView, CGFloat)? = nil
        
        for anyView in self.subviews {
            
            let view = anyView
            let index = eliminatedViews.indexOf(view)
            
            if (index == nil) {
                if !view.hidden {
                    view.alpha = 1
                    
                    let distance = distanceBetween(view.frame, point: position)
                    
                    if closest != nil {
                        if distance < closest!.1 {
                            closest = (view, distance)
                        }
                    }
                    else {
                        closest = (view, distance)
                    }
                }
            }
        }
        
        if (closest != nil) {
            return closest!.0
        }
        else {
            return nil
        }
    }
    
    func distanceBetween(rect: CGRect, point: CGPoint) -> CGFloat {
        if CGRectContainsPoint(rect, point) {
            return 0
        }
        
        var closest = rect.origin
        
        if (rect.origin.x + rect.size.width < point.x) {
            closest.x += rect.size.width
        }
        else if (point.x > rect.origin.x) {
            closest.x = point.x
        }
        if (rect.origin.y + rect.size.height < point.y) {
            closest.y += rect.size.height
        }
        else if (point.y > rect.origin.y) {
            closest.y = point.y
        }
        
        let a = pow(Double(closest.y - point.y), 2)
        let b = pow(Double(closest.x - point.x), 2)
        
        return CGFloat(sqrt(a + b));
    }
    
    func resetTrackedViews() {
        for view in self.touchToView.values {
            self.handleControl(view, controlEvent: .TouchCancel)
        }
        self.touchToView.removeAll(keepCapacity: true)
    }
    
    func ownView(newTouch: UITouch, viewToOwn: UIView?) -> Bool {
        var foundView = false
        
        if viewToOwn != nil {
            for (touch, view) in self.touchToView {
                if viewToOwn == view {
                    if touch == newTouch {
                        break
                    }
                    else {
                        self.touchToView[touch] = nil
                        foundView = true
                    }
                    break
                }
            }
        }
        self.touchToView[newTouch] = viewToOwn
        
        return foundView
    }
    
    private var swipeCharacters: String = ""
    
    func swipeHandler() {
        
        if let swipeTimer = self.swipeTimer  {
            if (swipeTimer.valid) {
                self.cancelTouchesEnd = true
                
                let touch: UITouch = self.swipeTimer!.userInfo as! UITouch
                
                if let view = self.touchToView[touch] {
                    self.swipeTimer?.invalidate()
                    
                    let touchPosition = touch.locationInView(self)
                    
                    if self.bounds.contains(touchPosition) {
                        let keyType = (view as! BLKey).keyType
                        
                        if keyType == .CharacterKey {
                            self.previousKey = (view as! BLKey)
                            self.swipeCharacters += self.previousKey!.getTitle()!
                        }
                        else if keyType == .SpaceKey {
                            // Do somethings for swiping handlers on spacebar
                            //self.handleControl(seletor: "spaceButtonTapped", view: view, controlEvent: .TouchUpInside)
                        }
                    }
                    else {
                        self.handleControl(view, controlEvent: .TouchCancel)
                    }
                }
                

            }
        }
    }
    
    private var touchedButtonType: BLKeyType? = nil
    
    private let scribbleViewLayerZPosition: CGFloat = 1001
    
    private var currentTouch: UITouch?
    
    private var previousKey: BLKey?
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.cancelTouchesEnd = false
        
        if self.currentTouch != nil {
            self.touchesCancelled([self.currentTouch!], withEvent: nil)
        }
        
        self.currentTouch = touches.first
        self.touchedButtonType = nil
        
        let position = self.currentTouch!.locationInView(self)
        //            let view = findNearestView(position, eliminatedViews: [self.scribbleView!, self.imageView!])
        let view = findNearestView(position, eliminatedViews: [self.scribbleView!])
        
        let viewChangedOwnership = self.ownView(self.currentTouch!, viewToOwn: view)
        
        if !viewChangedOwnership {
            
            let keyType = (view as! BLKey).keyType
            self.touchedButtonType = keyType
            
            if keyType == BLKeyType.CharacterKey {
                
                self.previousKey = (view as! BLKey)
                self.swipeCharacters += (view as! BLKey).getTitle()!
//                self.swipeTimer?.invalidate()
//                self.swipeTimer = NSTimer.scheduledTimerWithTimeInterval(kBLSwipeTimeInterval, target: self, selector: #selector(swipeHandler), userInfo: self.currentTouch, repeats: false)
            }
            else if keyType == BLKeyType.BackspaceKey {
                // Do something for backspace touched handler
            }
            
            self.handleControl(view, controlEvent: .TouchDown)
                
            if self.currentTouch!.tapCount > 1 {
                // Double taps
                self.handleControl(view, controlEvent: .TouchDownRepeat)
            }
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
//        var firstRun: Bool = true
        
        for touch in touches {
            if touch == self.currentTouch {
                
                let position = touch.locationInView(self)
                
                let oldView = self.touchToView[touch]
                //            let newView = findNearestView(position, eliminatedViews: [self.scribbleView!, self.imageView!])
                let newView = findNearestView(position, eliminatedViews: [self.scribbleView!])
                
                // If the finger move over the keyboard view. The nearest view will be nil.
                if (newView == nil) {
                    return
                }
                
                if self.touchedButtonType == BLKeyType.BackspaceKey {
                    // Do some thing for finger moving events on spaceKey
                    print("Do something for finger moving events on spaceKey")
                }
                else if (self.touchedButtonType == BLKeyType.CharacterKey){
                    
                    let keyType = (newView as! BLKey).keyType
                    
                    if (oldView != newView) {
                        self.handleControl(seletor: "hidePopupDelay:", view: oldView, controlEvent: .TouchUpInside)
                        self.ownView(touch, viewToOwn: newView)
                    }
                    
                    if keyType == BLKeyType.CharacterKey {
                        
//                        if (firstRun) {
//                            firstRun = false
                        
                        self.scribbleView?.show()
                        self.scribbleView?.drawToTouch(touch)
//                        }
                        
                        if oldView != newView {
                            self.swipeTimer?.invalidate()
                            self.swipeTimer = NSTimer.scheduledTimerWithTimeInterval(kBLSwipeTimeInterval, target: self, selector: #selector(swipeHandler), userInfo: touch, repeats: false)
                        }
                    }
                    
                    self.cancelTouchesEnd = true
                }
                else {
                    // If you want to eliminate which keytype. handler it here
                    // If the finger is moving to new key.
                    if oldView != newView {
                        
                        self.handleControl(oldView, controlEvent: .TouchDragExit)
                        
                        let viewChangedOwnership = self.ownView(touch, viewToOwn: newView)
                        
                        if !viewChangedOwnership {
                            self.handleControl(newView, controlEvent: .TouchDragEnter)
                        }
                        else {
                            self.handleControl(newView, controlEvent: .TouchDragInside)
                        }
                    }
                        // The finger is still in the old key
                    else {
                        self.handleControl(oldView, controlEvent: .TouchDragInside)
                    }

                }
            }
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for touch in touches {
            if touch == self.currentTouch {
                
                let view = self.touchToView[touch]

                if !self.cancelTouchesEnd {
                    
                    let touchPosition = touch.locationInView(self)
                    
                    if self.bounds.contains(touchPosition) {
                        self.handleControl(view, controlEvent: .TouchUpInside)
                    }
                    else {
                        self.handleControl(view, controlEvent: .TouchCancel)
                    }
                }
                else {
                    if (self.previousKey != (view as! BLKey)) {
                        self.swipeCharacters += (view as! BLKey).getTitle()!
                    }
                    self.textInputVC?.textDocumentProxy.insertText(self.swipeCharacters)

                    self.handleControl(seletor: "hidePopupDelay:", view: view, controlEvent: .TouchUpInside)
                }
                
                self.previousKey = nil
                self.swipeCharacters = ""
                self.currentTouch = nil
                self.scribbleView?.hide()
                self.touchedButtonType = nil
                self.swipeTimer?.invalidate()
            }
            self.touchToView[touch] = nil
        }
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        
        if let touches = touches {
            
            for touch in touches {
                if touch == self.currentTouch {
                    
                    let view = self.touchToView[touch]
                    
                    self.handleControl(view, controlEvent: .TouchCancel)
                    
                    self.currentTouch = nil
                    self.scribbleView?.reset()
                    self.touchedButtonType = nil
                    
                    self.previousKey = nil
                    self.swipeCharacters = ""
                    self.swipeTimer?.invalidate()
                }
                self.touchToView[touch] = nil
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
