//
//  AppDelegate.h
//  CaptureVideoOverlay
//
//  Created by manhduydl on 7/30/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

