//
//  main.m
//  FacebookContact
//
//  Created by qhcthanh on 7/13/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
