//
//  UXImageTransitionDetail.m
//  testUIView
//
//  Created by VanDao on 6/28/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import "UXImageTransitionDetail.h"
#import "UXImageTransitionView.h"

@interface UXImageTransitionDetail(){
    UIImage *transitionImage;
}

@end

@implementation UXImageTransitionDetail

- (id)initWithTransitionView:(UXImageTransitionView *)view{
    self = [super initWithTransitionView:view];
    
    if (self){
        transitionImage = view.image;
    }
    
    return self;
}

@end
