//
//  BLTopExtraView.swift
//  BanLaKey
//
//  Created by qhcthanh on 8/30/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import UIKit

@objc public protocol BLTopExtraViewDelegate: NSObjectProtocol {
    
    optional func didTouchOnSetting(topView: BLTopExtraView)
    
    optional func didDoubleTouchOnSetting(topView: BLTopExtraView)
    
    optional func didTouchOnEmoji(topView: BLTopExtraView)
    
    optional func didTouchOnGiphy(topView: BLTopExtraView)
    
    optional func didTouchOnSuggestion(topView: BLTopExtraView,suggestion: AnyObject)
    
    optional func didTouchOnHideSuggestion(topView: BLTopExtraView,isShowUp: Bool)
}

public enum BLKeyboardOption: String {
    case NormalKeyboard = "NormalKeyboard"
    case EmojiKeyboard = "EmojiKeyboard"
    case GiphyKeyboard = "GiphyKeyboard"
}

public class BLTopExtraView: UIView {
    
    // MARK: UI Properties
    var settingButton: UIButton!
    var hideSuggestionButton: UIButton!
    var emojiButton: UIButton!
    var giphyButton: UIButton!
    
    
    var suggesstionView: BLSuggestionView!
    
    
    weak var delegate: BLTopExtraViewDelegate?
    
    // MARK: Configure Properties
    
    var currentOption = BLKeyboardOption.NormalKeyboard {
        willSet {
            // If newButton is not NormalKeyboard type highlight it
            if let newButton = self.getButtonWithOption(newValue) {
                newButton.backgroundColor = BLThemeManager.currentTheme.spaceButton
            } // If newButton is NormalKeyboard. Get old value and clean highlight
            else {
                if let oldButton = self.getButtonWithOption(self.currentOption) {
                    oldButton.backgroundColor = .clearColor()
                }
            }
        }
    }
    
    let suggesstionCellColor: UIColor = UIColor(red: 172.0/255,green: 181.0/255,blue: 193.0/255,alpha: 1)
    
    var numberOfItemInSuggestion: Int =  UIScreen.mainScreen().interfaceOrientation() == .Portrait ? 3 : 5 {
        didSet {
            if let suggesstionView = self.suggesstionView {
                suggesstionView.reloadData()
            }
        }
    }
    
    public var isShowSuggesstionEmoji: Bool = userDefaults.boolForKey(kCurrentKeyboardHaveSuggesstionViewKey)
    
    // MARK: Initialize
    convenience init() {
        self.init(frame: CGRectZero)
        
        self.setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setupView()
    }
    
    
    
    func setupView() {
        
        // Setting Button
        if self.settingButton == nil {
            self.settingButton = UIButton()
            self.settingButton.setImage(UIImage(named: BLThemeManager.currentTheme.settingIconPath), forState: .Normal)
            self.settingButton.tintColor = .darkGrayColor()
            self.addSubview(self.settingButton)
            
            self.settingButton.addTarget(self, action: #selector(self.didSelectOption), forControlEvents: .TouchUpInside)
            self.settingButton.addTarget(self, action: #selector(self.didTouchOnSetting), forControlEvents: .TouchDownRepeat)
        }
        
        // hideSuggestionButton 
        if self.hideSuggestionButton == nil {
            self.hideSuggestionButton = UIButton()
            
            var imageHideSuggestionButtonPath = BLThemeManager.currentTheme.upIconPath
            if userDefaults.boolForKey(kCurrentKeyboardHaveSuggesstionViewKey) {
                imageHideSuggestionButtonPath = BLThemeManager.currentTheme.downIconPath
            }
            self.hideSuggestionButton.setImage(UIImage(named: imageHideSuggestionButtonPath), forState: .Normal)
            
            self.hideSuggestionButton.tintColor = .darkGrayColor()
            self.addSubview(self.hideSuggestionButton)
            
            self.hideSuggestionButton.addTarget(self, action: #selector(self.didSelectHideSuggestion), forControlEvents: .TouchUpInside)
        }
        
        // Emoji Button
        if self.emojiButton == nil {
            self.emojiButton = UIButton()
            self.emojiButton.setImage(UIImage(named: BLThemeManager.currentTheme.emojiIconPath), forState: .Normal)
            self.emojiButton.tintColor = .darkGrayColor()
            self.addSubview(self.emojiButton)
            
            self.emojiButton.addTarget(self, action: #selector(self.didSelectOption), forControlEvents: .TouchUpInside)
        }
        
        // Giphy Button
        if self.giphyButton == nil {
            self.giphyButton = UIButton()
            self.giphyButton.setImage(UIImage(named: BLThemeManager.currentTheme.giphyIconPath), forState: .Normal)
            self.giphyButton.tintColor = .darkGrayColor()
            self.addSubview(self.giphyButton)
            
             self.giphyButton.addTarget(self, action: #selector(self.didSelectOption), forControlEvents: .TouchUpInside)
        }
        
        // suggesstionView
        if self.suggesstionView == nil {
            self.suggesstionView = BLSuggestionView()
            self.suggesstionView.delegate = self
            self.suggesstionView.dataSource = self
            self.addSubview(self.suggesstionView)
            
        }
        
        self.setupContrainst()
    }
    
    var currentHideSuggestionWidthConstraints: NSLayoutConstraint!
    var isShowSettingDetails = false
    
    func setupContrainst() {
        
         // Setting Button Set contrainst Leading, top, bottom = 0 TopBarView, Width = 0125 * topbar.Width
        self.settingButton.constrain(.Left, to: .Left, of: self)
        self.settingButton.constrain(.Top, to: .Top, of: self, offsetBy: 2)
        self.settingButton.constrain(.Bottom, to: .Bottom, of: self, offsetBy: -2)
        self.settingButton.constrain(.Width, being: .Equal, to: .Width, of: self, multipliedBy: 0.1)
        
        // Hide Suggestion Button Set contrainst Leading, top, bottom = 0 TopBarView, Width = 0125 * topbar.Width
        self.hideSuggestionButton.constrain(.Left, to: .Right, of: self.settingButton)
        self.hideSuggestionButton.constrain(.Top, to: .Top, of: self)
        self.hideSuggestionButton.constrain(.Bottom, to: .Bottom, of: self)
        self.currentHideSuggestionWidthConstraints = self.hideSuggestionButton.constrain(.Width, being: .Equal, to: .Width, of: self, multipliedBy: 0)
        
        // Emoji Button Set contrainst Tralling, top, bottom = 0 TopBarView, Setting Button
        self.emojiButton.constrain(.Right, to: .Right, of: self)
        self.emojiButton.constrain(.Top, to: .Top, of: self.settingButton)
        self.emojiButton.constrain(.Bottom, to: .Bottom, of: self.settingButton)
        self.emojiButton.constrain(.Width, to: .Width, of: self.hideSuggestionButton)
        
        // Giphy Button Set contrainst  top, bottom = 0 TopBarView, Width = Setting Button, Tralling emojiButton
        self.giphyButton.constrain(.Right, to: .Left, of: self.emojiButton)
        self.giphyButton.constrain(.Top, to: .Top, of: self.settingButton)
        self.giphyButton.constrain(.Bottom, to: .Bottom, of: self.settingButton)
        self.giphyButton.constrain(.Width, to: .Width, of: self.hideSuggestionButton)
        
        
        // Setup SuggestionVIew it bettwen giphyButton and setting button
        self.suggesstionView.constrain(.Top, to: .Top, of: self, offsetBy: 2)
        self.suggesstionView.constrain(.Bottom, to: .Bottom, of: self, offsetBy: -1)
        self.suggesstionView.constrain(.Left, to: .Right, of: self.hideSuggestionButton)
        self.suggesstionView.constrain(.Right, to: .Left, of: self.giphyButton, offsetBy: -2)
    }
    var timerShowDetailSetting: NSTimer!
    func didSelectOption(sender: UIButton) {
        
        if let delegate = delegate {
            if sender == self.settingButton {
                
                self.timerShowDetailSetting = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(didTouchOnSettingToShowDetails), userInfo: nil, repeats: false)
            } else if sender == self.emojiButton {
                if let didTouchOnEmoji = delegate.didTouchOnEmoji {
                    didTouchOnEmoji(self)
                }
            } else if sender == self.giphyButton {
                if let didTouchOnGiphy = delegate.didTouchOnGiphy {
                    didTouchOnGiphy(self)
                }
            }
        }
    }
    
    func didTouchOnSettingToShowDetails() {
        
        if timerShowDetailSetting != nil {
            timerShowDetailSetting.invalidate()
        }
        
        if let delegate = delegate,
            let didTouchOnSetting = delegate.didTouchOnSetting {
            
            self.removeConstraint(self.currentHideSuggestionWidthConstraints)
            
            self.isShowSettingDetails = !self.isShowSettingDetails
            self.currentHideSuggestionWidthConstraints = self.hideSuggestionButton.constrain(.Width, being: .Equal, to: .Width, of: self, multipliedBy: self.isShowSettingDetails ? 0.1 : 0)
            UIView.animateWithDuration(0.3, animations: {
                self.layoutIfNeeded()
            })
            
            didTouchOnSetting(self)
        }
    }
    
    func didTouchOnSetting(sender: UIButton, withEvent event:UIEvent) {
        
        if timerShowDetailSetting != nil {
            timerShowDetailSetting.invalidate()
        }
        
        if let delegate = self.delegate,
            let didDoubleTouchOnSetting = delegate.didDoubleTouchOnSetting
        {
            didDoubleTouchOnSetting(self)
        }
    }
    
    func didSelectHideSuggestion(sender: UIButton) {
        
        // If self.currentOption == .GiphyKeyboard . isShowSuggesstionEmoji always true. Disable button select hide
        if self.currentOption == .GiphyKeyboard {
            return
        }
        
        if self.isShowSuggesstionEmoji {
            self.hideSuggestionButton.setImage(UIImage(named: BLThemeManager.currentTheme.upIconPath), forState: .Normal)
        } else {
            self.hideSuggestionButton.setImage(UIImage(named: BLThemeManager.currentTheme.downIconPath), forState: .Normal)
        }
        
        self.isShowSuggesstionEmoji = !self.isShowSuggesstionEmoji
        
        if let delegate = delegate, let didTouchOnHideSuggestion = delegate.didTouchOnHideSuggestion {
            didTouchOnHideSuggestion(self, isShowUp: self.isShowSuggesstionEmoji)
        }
    }
    
    private func getButtonWithOption(option: BLKeyboardOption) -> UIButton? {
        
        if option == .EmojiKeyboard {
            return emojiButton
        } else if option == .GiphyKeyboard {
            return giphyButton
        } else {
            return nil
        }
    }
    
}

extension BLTopExtraView: BLSuggesstionViewDelegate, BLSuggesstionViewDataSource {
    
    public func numberOfSuggestion(suggesstionView: BLSuggestionView) -> Int {
       return self.numberOfItemInSuggestion
    }
    
    public func suggesstionView(suggesstionView: BLSuggestionView, viewAtIndex index: Int) -> BLSuggestionViewCell {
        let cell = BLSuggestionViewCell()
        
        cell.backgroundColor = BLThemeManager.currentTheme.spaceButton
        return cell
    }
    
    public func suggesstionView(suggesstionView: BLSuggestionView, didSelectAtIndex index: Int) {
        
        if let delegate = self.delegate,
            touchDelegate = delegate.didTouchOnSuggestion,
            let text = suggesstionView.getSuggesstionData(AtIndex: index)
        {
            touchDelegate(self, suggestion: text)
        }
    }
    
}





