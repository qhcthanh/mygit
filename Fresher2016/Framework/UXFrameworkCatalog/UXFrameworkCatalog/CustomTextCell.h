/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */


@interface CustomTextCellObject : NSObject <UXCollectionViewNibCellObject>

+ (instancetype)objectWithTitle:(NSString *)title;

@property (nonatomic, copy) NSString* title;

@end

@interface CustomTextCell : UICollectionViewCell <UXCollectionViewCell>

@property (nonatomic, strong) IBOutlet UILabel* label;

@end
