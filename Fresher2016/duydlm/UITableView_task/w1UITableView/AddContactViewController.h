//
//  AddContactViewController.h
//  w1UITableView
//
//  Created by manhduydl on 6/3/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Contact;

@protocol AddContactDelegate <NSObject>

@required
- (void)addNewContact:(Contact *)newContact;

@end

@interface AddContactViewController : UIViewController

@property (nonatomic, weak) id<AddContactDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *givenNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *familyNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
- (IBAction)themButtonClick:(id)sender;

@end
