/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

#define CONFIG_DEBUG_FRAMEWORK 1

//! Project version number for ZXFramework.
FOUNDATION_EXPORT double ZXFrameworkVersionNumber;

//! Project version string for ZXFramework.
FOUNDATION_EXPORT const unsigned char ZXFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ZXFramework/PublicHeader.h>

#if CONFIG_DEBUG_FRAMEWORK

/////////////////////////////////////////////////////////////////////////////////////////
// UXLog
/////////////////////////////////////////////////////////////////////////////////////////
#import "UXLog.h"
#import "UXLogDefines.h"
#import "UXFileLogger.h"
#import "UXLogFileInfo.h"
#import "UXSystemLogger.h"
#import "UXTerminalLogger.h"
#import "UXLogFormatter.h"
#import "UXLogMessage.h"
#import "UXAbstractLogger.h"
#import "UXLogFileManager.h"
#import "UXDebugLogFormatter.h"
#import "UXLogFileManagerDefault.h"

#import "UXActionLogDetail.h"
#import "UXActionLogFormatter.h"
#import "UXActionLogManager.h"
#import "UXActionLogManagerExt.h"

#import "UXStaticLogDetail.h"
#import "UXStaticLogFormatter.h"
#import "UXStaticLogManager.h"
#import "UXStaticLogManagerExt.h"

/////////////////////////////////////////////////////////////////////////////////////////
// Common Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import "NSData+GZIP.h"
#import "UIImage+Circle.h"
#import "UIDevice+Extension.h"
#import "NSBundle+Extension.h"
#import "NSString+Security.h"
#import "NSString+Utility.h"
#import "NSData+Security.h"
#import "UXSessionContext.h"
#import "UXObjectConverter.h"
#import "UXSystemTimeInterval.h"
#import "NSData+CommonCrypto.h"
#import "UXSynthesizeSingleton.h"

/////////////////////////////////////////////////////////////////////////////////////////
// Contact Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import "UXTypes.h"
#import "UXContact.h"
#import "UXAddress.h"
#import "UXAddressBook.h"
#import "UXSocialProfile.h"
#import "UXPhoneWithLabel.h"

/////////////////////////////////////////////////////////////////////////////////////////
// Model Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import "NSArray+Extension.h"

#import "UXMobileNetworkNotify.h"
#import "UXMobileOperatorManager.h"

/////////////////////////////////////////////////////////////////////////////////////////
// Database Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import "UXSQLEntity.h"
#import "UXSQLBackgroundQueue.h"
#import "UXSQLDatabaseManager.h"
#import "UXSQLDatabaseRequest.h"
#import "UXSQLDatabaseResponse.h"

/////////////////////////////////////////////////////////////////////////////////////////
// APNS Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import "APNSHandler.h"

/////////////////////////////////////////////////////////////////////////////////////////
// APNS Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import "UXLocationDefines.h"
#import "UXLocationAddress.h"
#import "UXLocationGeocoder.h"
#import "UXLocationRequest.h"
#import "UXLocationManager.h"

/////////////////////////////////////////////////////////////////////////////////////////
// StoreKit Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import "UXStoreKit.h"

/////////////////////////////////////////////////////////////////////////////////////////
// Presentation Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import "UXTableView.h"
#import "UXNavigationController.h"
#import "UIViewController+Skin.h"
#import "UIViewController+Keyboard.h"
#import "UINavigationController+POPGesture.h"

/////////////////////////////////////////////////////////////////////////////////////////
// Vendor Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import "Reachability.h"

/////////////////////////////////////////////////////////////////////////////////////////
// AFNetworking Classes
/////////////////////////////////////////////////////////////////////////////////////////
#ifndef _AFNETWORKING_
#define _AFNETWORKING_

#define AF_IMAGE_NETWORKING 1

#import "AFURLRequestSerialization.h"
#import "AFURLResponseSerialization.h"
#import "AFSecurityPolicy.h"
#import "AFNetworkReachabilityManager.h"

#import "AFFileRequestOperation.h"

#if ( ( defined(__MAC_OS_X_VERSION_MAX_ALLOWED) && __MAC_OS_X_VERSION_MAX_ALLOWED >= 1090) || \
( defined(__IPHONE_OS_VERSION_MAX_ALLOWED) && __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000 ) )
#import "AFURLSessionManager.h"
#import "AFHTTPSessionManager.h"
#endif

#endif /* _AFNETWORKING_ */
#import "AFImageView.h"
#import "UIKit+AFNetworking.h"
#import "AFMemoryCache.h"
#import "AFAvatarInfo.h"
#import "UXMutableURLRequest.h"
#import "UXURLDataResponse.h"
#import "UXHTTPSessionManager.h"
#import "UXHTTPNetworkManager.h"

#else
/////////////////////////////////////////////////////////////////////////////////////////
// UXLog
/////////////////////////////////////////////////////////////////////////////////////////
#import <ZXFramework/UXLog.h>
#import <ZXFramework/UXLogDefines.h>
#import <ZXFramework/UXFileLogger.h>
#import <ZXFramework/UXLogFileInfo.h>
#import <ZXFramework/UXSystemLogger.h>
#import <ZXFramework/UXTerminalLogger.h>
#import <ZXFramework/UXLogFormatter.h>
#import <ZXFramework/UXLogMessage.h>
#import <ZXFramework/UXAbstractLogger.h>
#import <ZXFramework/UXLogFileManager.h>
#import <ZXFramework/UXDebugLogFormatter.h>
#import <ZXFramework/UXLogFileManagerDefault.h>

#import <ZXFramework/UXActionLogDetail.h>
#import <ZXFramework/UXActionLogFormatter.h>
#import <ZXFramework/UXActionLogManager.h>
#import <ZXFramework/UXActionLogManagerExt.h>

#import <ZXFramework/UXStaticLogDetail.h>
#import <ZXFramework/UXStaticLogFormatter.h>
#import <ZXFramework/UXStaticLogManager.h>
#import <ZXFramework/UXStaticLogManagerExt.h>

/////////////////////////////////////////////////////////////////////////////////////////
// Common Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import <ZXFramework/NSData+GZIP.h>
#import <ZXFramework/UIImage+Circle.h>
#import <ZXFramework/UIDevice+Extension.h>
#import <ZXFramework/NSBundle+Extension.h>
#import <ZXFramework/NSString+Security.h>
#import <ZXFramework/NSString+Utility.h>
#import <ZXFramework/NSData+Security.h>
#import <ZXFramework/UXSessionContext.h>
#import <ZXFramework/UXObjectConverter.h>
#import <ZXFramework/UXSystemTimeInterval.h>
#import <ZXFramework/NSData+CommonCrypto.h>
#import <ZXFramework/UXSynthesizeSingleton.h>

/////////////////////////////////////////////////////////////////////////////////////////
// Contact Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import <ZXFramework/UXTypes.h>
#import <ZXFramework/UXContact.h>
#import <ZXFramework/UXAddress.h>
#import <ZXFramework/UXAddressBook.h>
#import <ZXFramework/UXSocialProfile.h>
#import <ZXFramework/UXPhoneWithLabel.h>

/////////////////////////////////////////////////////////////////////////////////////////
// Common Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import <ZXFramework/QoSSimplePing.h>
#import <ZXFramework/NSArray+Extension.h>
#import <ZXFramework/UXMobileNetworkNotify.h>
#import <ZXFramework/UXMobileOperatorManager.h>

/////////////////////////////////////////////////////////////////////////////////////////
// Database Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import <ZXFramework/UXSQLEntity.h>
#import <ZXFramework/UXSQLBackgroundQueue.h>
#import <ZXFramework/UXSQLDatabaseManager.h>
#import <ZXFramework/UXSQLDatabaseRequest.h>
#import <ZXFramework/UXSQLDatabaseResponse.h>

/////////////////////////////////////////////////////////////////////////////////////////
// APNS Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import <ZXFramework/APNSHandler.h>

/////////////////////////////////////////////////////////////////////////////////////////
// APNS Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import <ZXFramework/UXLocationDefines.h>
#import <ZXFramework/UXLocationRequest.h>
#import <ZXFramework/UXLocationManager.h>
#import <ZXFramework/UXLocationAddress.h>
#import <ZXFramework/UXLocationGeocoder.h>
/////////////////////////////////////////////////////////////////////////////////////////
// StoreKit Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import <ZXFramework/UXStoreKit.h>

/////////////////////////////////////////////////////////////////////////////////////////
// Presentation Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import <ZXFramework/UXTableView.h>
#import <ZXFramework/UXNavigationController.h>
#import <ZXFramework/UIViewController+Skin.h>
#import <ZXFramework/UIViewController+Keyboard.h>
#import <ZXFramework/UINavigationController+POPGesture.h>

/////////////////////////////////////////////////////////////////////////////////////////
// Vendor Classes
/////////////////////////////////////////////////////////////////////////////////////////
#import <ZXFramework/Reachability.h>

/////////////////////////////////////////////////////////////////////////////////////////
// AFNetworking Classes
/////////////////////////////////////////////////////////////////////////////////////////
#ifndef _AFNETWORKING_
#define _AFNETWORKING_

#define AF_IMAGE_NETWORKING 1

#import <ZXFramework/AFURLRequestSerialization.h>
#import <ZXFramework/AFURLResponseSerialization.h>
#import <ZXFramework/AFSecurityPolicy.h>
#import <ZXFramework/AFNetworkReachabilityManager.h>

#import <ZXFramework/AFFileRequestOperation.h>

#if ( ( defined(__MAC_OS_X_VERSION_MAX_ALLOWED) && __MAC_OS_X_VERSION_MAX_ALLOWED >= 1090) || \
( defined(__IPHONE_OS_VERSION_MAX_ALLOWED) && __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000 ) )
#import <ZXFramework/AFURLSessionManager.h>
#import <ZXFramework/AFHTTPSessionManager.h>
#endif

#endif /* _AFNETWORKING_ */
#import <ZXFramework/AFImageView.h>
#import <ZXFramework/UIKit+AFNetworking.h>
#import <ZXFramework/AFMemoryCache.h>
#import <ZXFramework/AFAvatarInfo.h>
#import <ZXFramework/UXMutableURLRequest.h>
#import <ZXFramework/UXURLDataResponse.h>
#import <ZXFramework/UXHTTPSessionManager.h>
#import <ZXFramework/UXHTTPNetworkManager.h>

#endif

#define UXIMAGE(_URL) [[AFImageMemoryCache sharedAFImageMemoryCache] imageForURL:_URL onDisk:YES withPrefix:AF_IMAGE_CACHING_DEFAULT]

#define AFIMAGE(_URL,_DISK) [[AFImageMemoryCache sharedAFImageMemoryCache] imageForURL:_URL onDisk:_DISK withPrefix:AF_IMAGE_CACHING_TEMPORARY]

#define AFIMAGEPREFIX(_URL,_DISK,_PREFIX) [[AFImageMemoryCache sharedAFImageMemoryCache] imageForURL:_URL onDisk:_DISK withPrefix:_PREFIX]

