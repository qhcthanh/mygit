/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXLogFileManager.h"
#import "UXAbstractLogger.h"

@class UXLogFileInfo;
@interface UXFileLogger : UXAbstractLogger <UXLogger> {
	__strong id <UXLogFileManager> logFileManager;
	
	UXLogFileInfo *currentLogFileInfo;
    NSFileHandle *currentLogFileHandle;
    
    dispatch_source_t currentLogFileVnode;
    dispatch_source_t rollingTimer;
    
    unsigned long long maximumFileSize;
    NSTimeInterval rollingFrequency;
}

- (id)init;
- (id)initWithLogFileManager:(id <UXLogFileManager>)logFileManager;

@property (readonly) UXLogFileInfo* currentLogFileInfo;

// Configuration
// 
// maximumFileSize:
//   The approximate maximum size to allow log files to grow.
//   If a log file is larger than this value after a write,
//   then the log file is rolled.
// 
// rollingFrequency
//   How often to roll the log file.
//   The frequency is given as an NSTimeInterval, which is a double that specifies the interval in seconds.
//   Once the log file gets to be this old, it is rolled.
// 
// Both the maximumFileSize and the rollingFrequency are used to manage rolling.
// Whichever occurs first will cause the log file to be rolled.
// 
// For example:
// The rollingFrequency is 24 hours,
// but the log file surpasses the maximumFileSize after only 20 hours.
// The log file will be rolled at that 20 hour mark.
// A new log file will be created, and the 24 hour timer will be restarted.
// 
// logFileManager
//   Allows you to retrieve the list of log files,
//   and configure the maximum number of archived log files to keep.

@property (readwrite, assign) unsigned long long maximumFileSize;

@property (readwrite, assign) NSTimeInterval rollingFrequency;
@property (readwrite, assign, atomic) BOOL doNotReuseLogFiles;
@property (retain, nonatomic, readonly) id <UXLogFileManager> logFileManager;


// You can optionally force the current log file to be rolled with this method.
// CompletionBlock will be called on main queue.

- (void)rollLogFileWithCompletionBlock:(void (^)())completionBlock;

// Method is deprecated. Use rollLogFileWithCompletionBlock: method instead.

- (void)rollLogFile __attribute((deprecated));

// Inherited from UXAbstractLogger

// - (id <UXLogFormatter>)logFormatter;
// - (void)setLogFormatter:(id <UXLogFormatter>)formatter;

@end
