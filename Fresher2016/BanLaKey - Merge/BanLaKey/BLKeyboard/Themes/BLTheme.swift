//
//  BLTheme.swift
//  BanLaKey
//
//  Created by BaoNQ on 8/17/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

class BLTheme : NSObject {
    
    // General
    var backgroundColor: UIColor!
    var backgroundImage: UIImage?
    var buttonTitleFont: String!
    var buttonBorderColor: UIColor!
    
    var specialButtonTitleColor: UIColor!
    var characterButtonTitleColor: UIColor!
    
    var specialButtonHighLightedTitleColor: UIColor!
    var characterButtonHighLightedTitleColor: UIColor!
    
    
    var specialButtonShadowColor: UIColor!
    var characterButtonShadowColor: UIColor!
    
    var specialButtonHighlightedColor: UIColor!
    var characterButtonHightlightedColor: UIColor!
    
    var useSameColorForSpecialButtonHighlighted: Bool = false
    var useSameColorForCharacterButtonHighlighted: Bool = false
    
    var buttonShadowYOffset: CGFloat = 0.0 /*= 1.0*/
    var buttonLabelYOffset: CGFloat = 0.0 /*= -1.5*/
    var buttonImageYOffset: CGFloat = 0.0 /*= -0.5*/
    
    
    // iPhone
    var buttonPhoneBorderWidth: CGFloat = 0.0
    var buttonPhoneShadowWidth: CGFloat = 0.0
    var buttonPhoneCornerRadius: CGFloat = 0.0
    var buttonPhonePopupCornerRadius: CGFloat = 0.0
    var buttonPhonePortraitFontSize: CGFloat = 0.0
    var buttonPhoneLandscapeFontSize: CGFloat = 0.0
    var buttonPhonePopupPortraitFontSize: CGFloat = 0.0
    var buttonPhonePopupLandscapeFontSize: CGFloat = 0.0
    
    // iPad
    var buttonPadBorderWidth: CGFloat = 0.0
    var buttonPadShadowWidth: CGFloat = 0.0
    var buttonPadCornerRadius: CGFloat = 0.0
    var buttonPadPopupCornerRadius: CGFloat = 0.0
    var buttonPadPortraitFontSize: CGFloat = 0.0
    var buttonPadLandscapeFontSize: CGFloat = 0.0
    var buttonPadPopupPortraitFontSize: CGFloat = 0.0
    var buttonPadPopupLandscapeFontSize: CGFloat = 0.0
    
    var defaultLockKeyColor: UIColor!
    var onceLockKeyColor: UIColor!
    var foreverLockKeyColor: UIColor!
    
    // Buttons color
    // 1st row
    var QButton: UIColor!
    var WButton: UIColor!
    var EButton: UIColor!
    var RButton: UIColor!
    var TButton: UIColor!
    var YButton: UIColor!
    var UButton: UIColor!
    var IButton: UIColor!
    var OButton: UIColor!
    var PButton: UIColor!
    
    // 2nd row
    var AButton: UIColor!
    var SButton: UIColor!
    var DButton: UIColor!
    var FButton: UIColor!
    var GButton: UIColor!
    var HButton: UIColor!
    var JButton: UIColor!
    var KButton: UIColor!
    var LButton: UIColor!
    
    // 3rd row
    var ZButton: UIColor!
    var XButton: UIColor!
    var CButton: UIColor!
    var VButton: UIColor!
    var BButton: UIColor!
    var NButton: UIColor!
    var MButton: UIColor!
    
    // Other keys
    var shiftButton: UIColor!
    var deleteButton: UIColor!
    var globeButton: UIColor!
    var spaceButton: UIColor!
    var returnButton: UIColor!
    var keyModeButton: UIColor!
    
    var shiftDefaultButtonImage: String!
    var shiftOnceButtonImage: String!
    var shiftForeverButtonImage: String!
    var deleteButtonImage: String!
    var deleteButtonHighlightedImage: String!

    var globeButtonImage: String!
    var globeButtonHighlightedImage: String!

    var spaceButtonImage: String!
    var spaceButtonHighlightedImage: String!

    var returnButtonImage: String!
    var returnButtonHighlightedImage: String!

    var keyModeButtonImage: String!
    var keyModeButtonHighlightedImage: String!

    
    
    
    // Number keys
    var zeroButton: UIColor!
    var oneButton: UIColor!
    var twoButton: UIColor!
    var threeButton: UIColor!
    var fourButton: UIColor!
    var fiveButton: UIColor!
    var sixButton: UIColor!
    var sevenButton: UIColor!
    var eightButton: UIColor!
    var nineButton: UIColor!
    
    var minusButton: UIColor! // -
    var slashButton: UIColor! // /
    var colonButton: UIColor! // :
    var semicolonButton: UIColor! // ;
    var leftParenthesisButton: UIColor! // (
    var rightParenthesisButton: UIColor! // )
    var dollarSignButton: UIColor! // $
    var ampersandButton: UIColor! // &
    var atSignButton: UIColor! // @
    var quotationMarkButton: UIColor! // "
    var fullStopButton: UIColor! // .
    var commaButton: UIColor! // ,
    var questionMarkButton : UIColor! // ?
    var exclamationMarkButton: UIColor! // !
    var apostropheButton: UIColor! // '
    var graveAccentButton: UIColor! // `

    
    // Number keys page 2
    var leftSquareBracketButton: UIColor! // [
    var rightSquareBracketButton: UIColor! // ]
    var leftCurlyBracketButton: UIColor! // {
    var rightCurlyBracketButton: UIColor! // }
    var sharpButton: UIColor! // #
    var percentSignButton: UIColor! // %
    var circumflexAccentButton: UIColor! // ^
    var asteriskButton: UIColor! // *
    var plusSignButton: UIColor! // +
    var equalSignButton: UIColor! // =
    var lowLineButton: UIColor! // _
    var backslashButton: UIColor! // \
    var verticalBarButton: UIColor! // |
    var tildeButton: UIColor! // ~
    var lessThanSignButton: UIColor! // <
    var greaterThanSignButton: UIColor! // >
    var euroSignButton: UIColor! // €
    var poundSignButton: UIColor! // £
    var yenSignButton: UIColor! // ¥
    var centSignButton: UIColor! // ¢
    
    var blackCircleButton: UIColor! // ●
    var registerdSignButton: UIColor! // ®
    var copyrightSignButton: UIColor! // ©
    var degreeCelsiusButton: UIColor! // ℃
    var degreeFahrenheitButton: UIColor! // ℉

}


//    private var imageView: UIImageView? {
//        get {
//            if self.imageView == nil {
//                self.imageView = UIImageView.init()
//
//                self.imageView!.contentMode = UIViewContentMode.Center
//
//                // Set button image? Maybe!?
//
//                self.addSubview(self.imageView!)
//            }
//            return self.imageView
//        }
//        set {
//            self.imageView = newValue
//        }
//    }
//    func getImage() -> UIImage? {
//        return self.imageView!.image
//    }
//
//    func setImage(newImage: UIImage?) {
//        self.imageView!.image = newImage
//        self.updateState()
//    }

//    private var color: UIColor
//    private var shadowColor: UIColor
//
//    var style: BLKeyStyle {
//        get {
//            return self.style
//        }
//        set {
//            if self.style == newValue {
//                return
//            }
//            self.style = newValue
//            self.updateState()
//        }
//    }
//    var appearance: BLKeyAppearance {
//        get {
//            return self.appearance
//        }
//        set {
//            if (self.appearance == newValue) {
//                return
//            }
//            self.appearance = newValue
//            self.updateState()
//        }
//    }
//    var cornerRadius: CGFloat {
//        get {
//            return self.cornerRadius
//        }
//        set {
//            if (self.cornerRadius == newValue) {
//                return
//            }
//            self.cornerRadius = newValue
//            self.setNeedsDisplay()
//        }
//    }
//
//    func getTitleFont() -> UIFont! {
//        return self.label!.font
//    }
//
//    func setTitleFont(newFont: UIFont) {
//        self.label!.font = newFont
//    }
//
//    func updateState() {
//        switch self.appearance {
//        case BLKeyAppearance.Dark:
//
//            switch self.style {
//            case BLKeyStyle.Light:
//                self.label!.textColor = UIColor.whiteColor()
//
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLDarkAppearance.darkKeyColor()
//                    self.shadowColor = BLDarkAppearance.darkKeyShadowColor()
//
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLDarkAppearance.lightKeyColor()
//                    self.shadowColor = BLDarkAppearance.lightKeyShadowColor()
//
//                    break
//                }
//
//                break
//            case BLKeyStyle.Dark:
//                self.label?.textColor = UIColor.whiteColor()
//                self.imageView?.image = self.imageView?.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
//                self.imageView?.tintColor = BLDarkAppearance.whiteColor()
//
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLDarkAppearance.lightKeyColor()
//                    self.shadowColor = BLDarkAppearance.lightKeyShadowColor()
//
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLDarkAppearance.darkKeyColor()
//                    self.shadowColor = BLDarkAppearance.darkKeyShadowColor()
//
//                    break
//                }
//                break
//            default: // BLKeyStyle.Blue
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLDarkAppearance.lightKeyColor()
//                    self.shadowColor = BLDarkAppearance.lightKeyShadowColor()
//                    self.label?.textColor = BLDarkAppearance .blackColor()
//
//                    break
//                case UIControlState.Disabled:
//                    self.color = BLDarkAppearance.darkKeyColor()
//                    self.shadowColor = BLDarkAppearance.darkKeyShadowColor()
//                    self.label?.textColor = BLDarkAppearance.blueKeyDisabledTitleColor()
//
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLDarkAppearance.blueKeyColor()
//                    self.shadowColor = BLDarkAppearance.blueKeyShadowColor()
//                    self.label?.textColor = UIColor.whiteColor()
//
//                    break
//                }
//                break
//            }
//
//            break
//        default: // BLKeyAppearance.Light
//            switch self.style {
//            case BLKeyStyle.Light:
//                self.label!.textColor = UIColor.blackColor()
//
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLLightAppearance.darkKeyColor()
//                    self.shadowColor = BLLightAppearance.darkKeyShadowColor()
//
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLLightAppearance.lightKeyColor()
//                    self.shadowColor = BLLightAppearance.lightKeyShadowColor()
//
//                    break
//                }
//
//                break
//            case BLKeyStyle.Dark:
//                self.label?.textColor = UIColor.blackColor()
//
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLLightAppearance.lightKeyColor()
//                    self.shadowColor = BLLightAppearance.lightKeyShadowColor()
//                    self.imageView?.tintColor = BLLightAppearance.blackColor()
//
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLLightAppearance.darkKeyColor()
//                    self.shadowColor = BLLightAppearance.darkKeyShadowColor()
//                    self.imageView?.tintColor = BLLightAppearance.whiteColor()
//
//                    break
//                }
//                break
//            default: // BLKeyStyle.Blue
//                switch self.state {
//                case UIControlState.Highlighted:
//                    self.color = BLLightAppearance.lightKeyColor()
//                    self.shadowColor = BLLightAppearance.lightKeyShadowColor()
//                    self.label?.textColor = BLLightAppearance .blackColor()
//
//                    break
//                case UIControlState.Disabled:
//                    self.color = BLLightAppearance.darkKeyColor()
//                    self.shadowColor = BLLightAppearance.darkKeyShadowColor()
//                    self.label?.textColor = BLLightAppearance.blueKeyDisabledTitleColor()
//
//                    break
//                default: // UIControlState.Normal
//                    self.color = BLLightAppearance.blueKeyColor()
//                    self.shadowColor = BLLightAppearance.blueKeyShadowColor()
//                    self.label?.textColor = UIColor.whiteColor()
//
//                    break
//                }
//                break
//            }
//
//            break
//        }
//    }
