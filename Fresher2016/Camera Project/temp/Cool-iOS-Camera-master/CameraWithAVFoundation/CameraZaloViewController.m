////
////  CameraZaloViewController.m
////  CameraWithAVFoundation
////
////  Created by qhcthanh on 6/10/16.
////  Copyright © 2016 Gabriel Alvarado. All rights reserved.
////
//
//#import "CameraZaloViewController.h"
//#import "CameraSessionView.h"
//#import "ShowImageCaptureViewController.h"
//#import "PreviewVideoViewController.h"
//
//#define LEADING_PHOTO_MODEVIEW_CONTRAINST -15
//#define LEADING_VIDEO_MODEVIEW_CONTRAINST -63
//#define DEFAULT_VIDEO_TIME @"00:00"
//#define CAMERA_MINSCALE 1
//#define CAMERA_MAXSCALE 8
//
//@interface CameraZaloViewController () <CACameraSessionDelegate,UIGestureRecognizerDelegate>
//
//@end
//
//@implementation CameraZaloViewController {
//    __weak IBOutlet NSLayoutConstraint *leadingModeViewConstraint;
//    __weak IBOutlet UIButton *shutterButton;
//    __weak IBOutlet UIView *modeView;
//    __weak IBOutlet UILabel *photoModeLabel;
//    __weak IBOutlet UILabel *videoModeLabel;
//    __weak IBOutlet UILabel *timeVideoLabel;
//    AVPlayer* avPlayer;
//    NSTimer* countTimer;
//    CGFloat lastScale;
//    int timeCount;
//}
//
//-(void)viewDidLoad {
//    [super viewDidLoad];
//    
//    //set private property
//    lastScale = 1;
//    
//    //Set camera delegate
//    _cameraView.delegate = self;
//    
//    // add swipe gesture in mode view
//    UISwipeGestureRecognizer* swipeLeftGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeModeGesture:)];
//    swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
//    
//    UISwipeGestureRecognizer* swipeRightGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeModeGesture:)];
//    swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
//    
//    [modeView addGestureRecognizer: swipeRightGesture];
//    [modeView addGestureRecognizer: swipeLeftGesture];
//    
//    //Add tap gesture in label mode
//    UITapGestureRecognizer* photoTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeModePhoto)];
//    [photoTapGesture setNumberOfTapsRequired:1];
//    UITapGestureRecognizer* videoTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeModeVideo)];
//    [videoTapGesture setNumberOfTapsRequired:1];
//    [photoModeLabel addGestureRecognizer:photoTapGesture];
//    [videoModeLabel addGestureRecognizer:videoTapGesture];
//    
//    //Add pinch gesture in cameraview
//    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
//    pinchGesture.delegate = self;
//    [_cameraView addGestureRecognizer:pinchGesture];
//    
//}
//
//-(void)changeModePhoto {
//    photoModeLabel.textColor = [UIColor yellowColor];
//    videoModeLabel.textColor = [UIColor whiteColor];
//    [shutterButton setImage: [UIImage imageNamed:@"Circle Thin-96"] forState:UIControlStateNormal];
//    leadingModeViewConstraint.constant = LEADING_PHOTO_MODEVIEW_CONTRAINST;
//    timeVideoLabel.hidden = true;
//    [_cameraView stopRecording];
//    [UIView animateWithDuration:0.3 animations:^{
//        [self.view layoutIfNeeded];
//    }];
//}
//
//-(void)changeModeVideo {
//    photoModeLabel.textColor = [UIColor whiteColor];
//    videoModeLabel.textColor = [UIColor yellowColor];
//    [shutterButton setImage: [UIImage imageNamed:@"Record"] forState:UIControlStateNormal];
//    leadingModeViewConstraint.constant = LEADING_VIDEO_MODEVIEW_CONTRAINST;
//    [UIView animateWithDuration:0.3 animations:^{
//        [self.view layoutIfNeeded];
//    }];
//}
//
//-(void)swipeModeGesture:(UISwipeGestureRecognizer*)swipeGesture {
//    if (swipeGesture.direction == UISwipeGestureRecognizerDirectionRight) {
//        [self changeModePhoto];
//    } else {
//        [self changeModeVideo];
//    }
//}
//
//- (IBAction)onTapFlashAction:(id)sender {
//    [_cameraView onTapFlashButton];
//}
//
//- (IBAction)onTapBackAction:(id)sender {
//    //[_cameraView onTapDismissButton];
//}
//
//- (IBAction)onTapRotateAction:(id)sender {
//    [_cameraView onTapToggleButton];
//}
//
//- (IBAction)onTapShutterAction:(id)sender {
//    if (leadingModeViewConstraint.constant == LEADING_PHOTO_MODEVIEW_CONTRAINST) {
//        [_cameraView onTapShutterButton];
//    } else {
//        if (![_cameraView isRecording]) {
//            [_cameraView startRecording];
//        } else {
//            [countTimer invalidate];
//            [_cameraView stopRecording];
//        }
//    }
//}
//
//
//-(void)didCaptureImageWithData:(NSData *)imageData {
//    NSLog(@"Success");
//}
//
//-(void)didCaptureImage:(UIImage *)image {
//    NSLog(@"Success");
//    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
//    ShowImageCaptureViewController* showImageVC = [ShowImageCaptureViewController new];
//    showImageVC.captureImage = image;
//    
//    [self presentViewController:showImageVC animated:true completion:nil];
//}
//
//- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
//{
//    //Show error alert if image could not be saved
//    if (error) [[[UIAlertView alloc] initWithTitle:@"Error!" message:@"Image couldn't be saved" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
//}
//
//-(void)didStartRecordingVideo:(NSURL *)outputFileURL {
//    NSLog(@"didStartRecordingVideo %@",outputFileURL.absoluteString);
//    if(countTimer) {
//        timeCount = 0;
//        [countTimer invalidate];
//    }
//    timeVideoLabel.hidden = false;
//     countTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateCountdown) userInfo:nil repeats: YES];
//}
//
//-(void)didFinishRecordingVideo:(NSURL *)outputFileURL error:(NSError *)error cleanUp:(dispatch_block_t)cleanUpBlock {
////    cleanUpBlock();
//    timeVideoLabel.hidden = true;
//    timeVideoLabel.text = DEFAULT_VIDEO_TIME;
//    NSLog(@"didFinishRecordingVideo %@",outputFileURL.absoluteString);
//    avPlayer = [AVPlayer playerWithURL:outputFileURL];
//    PreviewVideoViewController* previewVC = [PreviewVideoViewController new];
//    previewVC.avPlayer = avPlayer;
//    
//    [self presentViewController:previewVC animated:true completion:nil];
//}
//
//-(void) updateCountdown
//{
//    timeCount++;
//    NSDate *now = [NSDate dateWithTimeIntervalSince1970:timeCount];
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    NSDateComponents *componentMint = [calendar components:NSCalendarUnitMinute fromDate:now];
//    NSDateComponents *componentSec = [calendar components:NSCalendarUnitSecond fromDate:now];
//                                      
//    NSString* minute = [NSString stringWithFormat:@"%02d",(componentMint.minute)];
//    NSString* second = [NSString stringWithFormat:@"%02d",(componentSec.second)];
//    
//    timeVideoLabel.text = [NSString stringWithFormat:@"%@:%@",minute,second];
//    
//    if (timeCount == 15) {
//        [countTimer invalidate];
//        [_cameraView stopRecording];
//    }
//}
//
//- (void)handlePinchGesture:(UIPinchGestureRecognizer *)pinchGesture {
//    
//    if (UIGestureRecognizerStateBegan == pinchGesture.state ||
//        UIGestureRecognizerStateChanged == pinchGesture.state) {
//        
//        float scale = pinchGesture.scale;
//        
//        if (scale < CAMERA_MINSCALE ) {
//            scale = CAMERA_MINSCALE;
//        } else if (scale > CAMERA_MAXSCALE) {
//            scale = CAMERA_MAXSCALE;
//        }
//        lastScale = scale;
//         NSLog(@"Delta: %f",scale);
//        [_cameraView zoomPreview:scale];
//        
//        // Reset to 1 for scale delta's
//        //  Note: not 0, or we won't see a size: 0 * width = 0
//        //pinchGesture.scale = 1;
//    }
//}
//
//
//@end
