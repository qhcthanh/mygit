//
//  BLPadMetrics.h
//  BanLaKey
//
//  Created by BaoNQ on 8/16/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>

typedef struct {
    
    // 1st row
    CGRect QButton;
    CGRect WButton;
    CGRect EButton;
    CGRect RButton;
    CGRect TButton;
    CGRect YButton;
    CGRect UButton;
    CGRect IButton;
    CGRect OButton;
    CGRect PButton;
    
    // 2nd row
    CGRect AButton;
    CGRect SButton;
    CGRect DButton;
    CGRect FButton;
    CGRect GButton;
    CGRect HButton;
    CGRect JButton;
    CGRect KButton;
    CGRect LButton;
    
    // 3rd row
    CGRect ZButton;
    CGRect XButton;
    CGRect CButton;
    CGRect VButton;
    CGRect BButton;
    CGRect NButton;
    CGRect MButton;
    
    CGRect leftShiftButton;
    CGRect rightShiftButton;
    CGRect deleteButton;

    CGRect globeButton;
    CGRect spaceButton;
    CGRect returnButton;
    CGRect KeyModeButton;
    CGRect settingButton;

    
    CGRect keyboardFrame;

} PadKeyboardMetrics;

PadKeyboardMetrics generatePadKeyboardMetrics(CGFloat keyboardWidth, CGFloat keyboardHeight);

CGRect calculatePadPopupFrame(CGSize keySize, CGFloat keyboardWidth, CGFloat keyboardHeight);
