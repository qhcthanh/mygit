//
//  VSPreviewImage.m
//  CameraGPUImage
//
//  Created by manhduydl on 8/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "VSPreviewImage.h"

@implementation VSPreviewImage

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _vsImageView.image = _imagePreview;
    [_vsImageView setImage:_imagePreview];
}
- (IBAction)backButtonTap:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)saveButtonTap:(id)sender {
}

@end
