/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#ifndef UXFrameworkDefines_h
#define UXFrameworkDefines_h

#define UX_DEPRECATED_ATTRIBUTE(message) __attribute((deprecated((message))))

/**
 * A constant denoting that a corner should be rounded.
 * @const -1
 */
extern const CGFloat kRounded;

/**
 * The standard height of a row in a table view controller.
 * @const 44 pixels
 */
extern const CGFloat kDefaultRowHeight;

extern const CGFloat kDefaultStatusBarHeight;

extern const CGFloat kDefaultNavigationBarHeight;

/**
 * The standard height of a toolbar in portrait orientation.
 * @const 44 pixels
 */
extern const CGFloat kDefaultPortraitToolbarHeight;

/**
 * The standard height of a toolbar in landscape orientation.
 * @const 33 pixels
 */
extern const CGFloat kDefaultLandscapeToolbarHeight;

/**
 * The standard height of the keyboard in portrait orientation.
 * @const 216 pixels
 */
extern const CGFloat kDefaultPortraitKeyboardHeight;

/**
 * The standard height of the keyboard in landscape orientation.
 * @const 160 pixels
 */
extern const CGFloat kDefaultLandscapeKeyboardHeight;

/**
 * The standard height of the keyboard in portrait orientation.
 * @const 216 pixels
 */
extern const CGFloat kDefaultPadPortraitKeyboardHeight;

/**
 * The standard height of the keyboard in landscape orientation.
 * @const 160 pixels
 */
extern const CGFloat kDefaultPadLandscapeKeyboardHeight;

/**
 * The space between the edge of the screen and the cell edge in grouped table views.
 * @const 10 pixels
 */
extern const CGFloat kGroupedTableCellInset;
extern const CGFloat kGroupedPadTableCellInset;

/**
 * The standard duration length for a transition.
 * @const 0.3 seconds
 */
extern const CGFloat kDefaultTransitionDuration;

/**
 * The standard duration length for a fast transition.
 * @const 0.2 seconds
 */
extern const CGFloat kDefaultFastTransitionDuration;

/**
 * The standard duration length for a flip transition.
 * @const 0.7 seconds
 */
extern const CGFloat kDefaultFlipTransitionDuration;

#endif /* UXFrameworkDefines_h */
