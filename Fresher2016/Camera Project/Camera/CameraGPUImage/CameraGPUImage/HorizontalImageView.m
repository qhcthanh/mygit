//
//  CTSliderView.m
//  CameraGPUImage
//
//  Created by BaoNQ on 6/27/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "HorizontalImageView.h"

@interface HorizontalImageView ()

@property NSInteger movedPage;
@property CGPoint lastContentOffset;

@property NSInteger numberOfPages;

@property int count;
@property BOOL swipedLeftInPreviousSwipe;

@property NSMutableArray<UITouch *> *touchList;
@property NSMutableArray *beginPointList;
@property NSMutableArray *movePointList;
@property NSMutableArray *timeList;
@property CGFloat distanceTransition;
@property int curOffset;

@end

@implementation HorizontalImageView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self == nil)
        return nil;
    
    _count = 0;
    _swipedLeftInPreviousSwipe = YES;
    _isDisabledResponder = NO;
    
    self.slider = [[UIScrollView alloc] initWithFrame:frame];
    
    self.slider.delegate = self;
    self.slider.pagingEnabled = true;
    self.slider.bounces = false;
    self.slider.showsHorizontalScrollIndicator = false;
    self.slider.showsVerticalScrollIndicator = false;
    self.slider.layer.zPosition = 1;
    
    [self addSubview:self.slider];
    
    self.pages = [[NSMutableArray alloc] init];
    
    _curOffset = 0;
    _distanceTransition = 0;
    _touchList = [[NSMutableArray alloc]init];
    _beginPointList = [[NSMutableArray alloc]init];
    _movePointList = [[NSMutableArray alloc]init];
    _timeList = [[NSMutableArray alloc]init];
    
    return self;
}


- (void)reloadData {
    
    self.numberOfImages = [self.dataSource numberOfImages:self];
    
    _curImageIndex = 0;
    _movedPage = -1;
    _lastContentOffset = CGPointZero;
    
    // Load data
    // We just use maximum 3 pages to present the image list.
    _numberOfPages = MIN(3, self.numberOfImages);
    
    self.slider.contentSize = CGSizeMake(self.frame.size.width * _numberOfPages, self.frame.size.height);
    
    for (int i = 0; i < _numberOfPages; ++i) {
        ImageView *filter = [[ImageView alloc] initWithFrame:self.frame];
        
        filter.image = [self.dataSource imageInHorizontalImageView:self atIndex:i];
        filter.layer.zPosition = 0;
        // [filter mask:filter.frame];
        [filter updateMask:filter.frame newXPosition:[self positionOfPageAtIndex:(i)]];
        
        [self.pages addObject:filter];
        [self addSubview:filter];
    }
    
    //Scroll the view to the starting offset
    [self.slider scrollRectToVisible:CGRectMake([self positionOfPageAtIndex:0], 0, self.frame.size.width, self.frame.size.height) animated:false];
}

- (CGFloat) positionOfPageAtIndex:(NSInteger)index {
    return self.frame.size.width * (float)(index);
}

- (NSInteger) previousPage:(NSInteger)curPage {
    // Get the previous page.
    // Ex: 2 -> 1, 1 -> 0, 0 -> 2
    NSInteger newPage = curPage - 1;
    if (newPage < 0)
        newPage = 2;
    return newPage;
}

//MARK: - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // Update new position for 3 pages depending on current content offset of scroll view.
    for (int i = 0; i < _numberOfPages; ++i) {
        int newXpos = ([self positionOfPageAtIndex:(i)] - scrollView.contentOffset.x);
        newXpos = newXpos % (int)(3 * self.frame.size.width);
        
        if (abs(newXpos) > 2 * self.frame.size.width) {
            newXpos = 3 * self.frame.size.width - abs(newXpos);
        }
        
        [self.pages[i] updateMask:self.pages[i].frame newXPosition:newXpos];
    }
    
    // If a page has been completely swiped
    if (fabs(_lastContentOffset.x - scrollView.contentOffset.x) >= self.frame.size.width) {
        if (_count < 2)
            _count++;
        
        //NSLog(@"Scroll");
        BOOL flag = NO;
        
        // Swiped to right
        if (_lastContentOffset.x - scrollView.contentOffset.x > 0) {
            _lastContentOffset.x -= self.frame.size.width;
            _curImageIndex--;
            
            if (_swipedLeftInPreviousSwipe == NO) {
                if (_count >= 2) {
                    _movedPage = [self previousPage:_movedPage];
                    //_movedPage = labs((--_movedPage)%3);
                }
            }
            _swipedLeftInPreviousSwipe = NO;
        }
        // Swiped to left
        else {
            _lastContentOffset.x += self.frame.size.width;
            _curImageIndex++;
            flag = YES;
            
            if (_swipedLeftInPreviousSwipe == YES) {
                if (_count >= 2) {
                    _movedPage = (++_movedPage)%3;
                }
            }
            _swipedLeftInPreviousSwipe = YES;
        }
        
        // If a page has been moved.
        if (_movedPage != -1) {
            NSInteger nextImageIndex = _curImageIndex;
            if (flag)
                nextImageIndex++;
            else
                nextImageIndex--;
            
            // Set new image for this page.
            if (nextImageIndex >= 0 && nextImageIndex < self.numberOfImages)
                self.pages[_movedPage].image = [self.dataSource imageInHorizontalImageView:self atIndex:nextImageIndex];
        }
    }
}

- (void)moveToPosition:(NSInteger)nextFilterIndex{
    int curIndex = _curOffset/self.frame.size.width;
    int sign = curIndex > nextFilterIndex ? -1 : 1;
    
    
    
    for (int index = curIndex * sign + 1; index <= nextFilterIndex * sign; index += 1){
        _curOffset = labs(index) * self.frame.size.width;
        [self.slider setContentOffset:CGPointMake(_curOffset, self.slider.contentOffset.y) animated:NO];
    }
}

- (void)disableReceiveResponder{
    _isDisabledResponder = YES;
    [_touchList removeAllObjects];
    [_beginPointList removeAllObjects];
    [_movePointList removeAllObjects];
    [_timeList removeAllObjects];
    _distanceTransition = 0;
    [self.slider setContentOffset:CGPointMake(_curOffset, self.slider.contentOffset.y) animated:YES];
}

- (void)enableReceiveResponder{
    _isDisabledResponder = NO;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (_isDisabledResponder)
        return;
    
    for (UITouch *touch in [touches allObjects])
    {
        NSNumber *time = [NSNumber numberWithDouble:[NSDate timeIntervalSinceReferenceDate]];
        // add new touch to list
        [_touchList addObject:touch];
        [_beginPointList addObject:[NSValue valueWithCGPoint:[touch locationInView:self]]];
        [_movePointList addObject:[NSValue valueWithCGPoint:[touch locationInView:self]]];
        [_timeList addObject:time];
    }
    
    [self.nextResponder touchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{    
    if (_isDisabledResponder)
        return;
    
    // Update move point list
    for (UITouch *touch in [touches allObjects])
    {
        NSInteger index = [_touchList indexOfObject:touch];
        [_movePointList replaceObjectAtIndex:index withObject:[NSValue valueWithCGPoint:[touch locationInView:self]]];
    }
    
    CGFloat newDistance = 0;
    for (UITouch *touch in [touches allObjects]) {
        NSInteger index = [_touchList indexOfObject:touch];
        CGPoint touchBegin = [[_beginPointList objectAtIndex:index] CGPointValue];
        CGPoint touchMove = [[_movePointList objectAtIndex:index] CGPointValue];
        CGFloat distance = touchBegin.x - touchMove.x;
        newDistance += distance;
    }
    _distanceTransition += newDistance/[touches count];
    
    if (_curOffset + _distanceTransition > 0 && _curOffset + _distanceTransition < self.slider.contentSize.width - self.frame.size.width){
        [self.slider setContentOffset:CGPointMake(_curOffset + _distanceTransition, self.slider.contentOffset.y) animated:NO];
    }
    
    for (UITouch *touch in [touches allObjects])
    {
        NSInteger index = [_touchList indexOfObject:touch];
        [_beginPointList replaceObjectAtIndex:index withObject:[NSValue valueWithCGPoint:[touch locationInView:self]]];
    }
    
    [self.nextResponder touchesMoved:touches withEvent:event];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (_isDisabledResponder)
        return;
    
    NSNumber *time = [NSNumber numberWithDouble:MAXFLOAT];
    NSNumber *currentTime = [NSNumber numberWithDouble:[NSDate timeIntervalSinceReferenceDate]];
    for (UITouch *touch in [touches allObjects]) {
        NSInteger index = [_touchList indexOfObject:touch];
        NSNumber *transitionTime = [NSNumber numberWithDouble:[currentTime doubleValue] - [[_timeList objectAtIndex:index] doubleValue]];
        time = [transitionTime doubleValue] < [time doubleValue] ? transitionTime : time;
    }

    if (_distanceTransition != 0){
        if ((fabs(_distanceTransition) > self.frame.size.width/2 || (fabs(_distanceTransition) > 40 && [time doubleValue] < 0.4))){
            if (_distanceTransition < 0)
                _curOffset -= self.frame.size.width;
            else
                _curOffset += self.frame.size.width;
            
            if (_curOffset < 0)
                _curOffset = 0;
            else if (_curOffset > self.slider.contentSize.width - self.frame.size.width)
                _curOffset = self.slider.contentSize.width - self.frame.size.width;
        }
        
        [self.slider setContentOffset:CGPointMake(_curOffset, self.slider.contentOffset.y) animated:YES];
        
        _distanceTransition = 0;
    }
    
    // Remove ended point from list
    for (UITouch *touch in [touches allObjects]) {
        NSInteger index = [_touchList indexOfObject:touch];
        
        [_touchList removeObjectAtIndex:index];
        [_beginPointList removeObjectAtIndex:index];
        [_movePointList removeObjectAtIndex:index];
        [_timeList removeObjectAtIndex:index];
    }
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (_isDisabledResponder)
        return;
    
    NSNumber *time = [NSNumber numberWithDouble:MAXFLOAT];
    NSNumber *currentTime = [NSNumber numberWithDouble:[NSDate timeIntervalSinceReferenceDate]];
    for (UITouch *touch in [touches allObjects]) {
        NSInteger index = [_touchList indexOfObject:touch];
        NSNumber *transitionTime = [NSNumber numberWithDouble:[currentTime doubleValue] - [[_timeList objectAtIndex:index] doubleValue]];
        time = [transitionTime doubleValue] < [time doubleValue] ? transitionTime : time;
    }
    if (_distanceTransition != 0){
        if ((fabs(_distanceTransition) > self.frame.size.width/2 || (fabs(_distanceTransition) > 40 && [time doubleValue] < 0.4))){
            if (_distanceTransition < 0)
                _curOffset -= self.frame.size.width;
            else
                _curOffset += self.frame.size.width;
            
            if (_curOffset < 0)
                _curOffset = 0;
            else if (_curOffset > self.slider.contentSize.width - self.frame.size.width)
                _curOffset = self.slider.contentSize.width - self.frame.size.width;
        }
        
        [self.slider setContentOffset:CGPointMake(_curOffset, self.slider.contentOffset.y) animated:YES];
        
        _distanceTransition = 0;
    }
    
    // Remove ended point from list
    for (UITouch *touch in [touches allObjects]) {
        NSInteger index = [_touchList indexOfObject:touch];
        
        [_touchList removeObjectAtIndex:index];
        [_beginPointList removeObjectAtIndex:index];
        [_movePointList removeObjectAtIndex:index];
        [_timeList removeObjectAtIndex:index];
    }
}

/*
 - (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
 
 if (scrollView.contentOffset.x == [self positionOfPageAtIndex:(-1)]) {
 [self.slider scrollRectToVisible:CGRectMake([self positionOfPageAtIndex:(self.numberOfPages - 1)], 0, self.frame.size.width, self.frame.size.height) animated:false];
 }
 else if (scrollView.contentOffset.x == [self positionOfPageAtIndex:self.numberOfPages]) {
 [self.slider scrollRectToVisible:CGRectMake([self positionOfPageAtIndex:0], 0, self.frame.size.width, self.frame.size.height) animated:false];
 }
 
 
 } */


@end
