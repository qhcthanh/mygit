/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@class UXNetworkModel;
@protocol UXCollectionViewDataSourceDelegate;

/**
 * Forked table model with previous framework
 */
@protocol UXModelCollectionViewDataSourceDelegate <UICollectionViewDataSource, UISearchDisplayDelegate>

@optional

- (NSIndexPath*)tableView:(UITableView*)tableView willUpdateObject:(id)object
              atIndexPath:(NSIndexPath*)indexPath;

- (NSIndexPath*)tableView:(UITableView*)tableView willInsertObject:(id)object
              atIndexPath:(NSIndexPath*)indexPath;

- (NSIndexPath*)tableView:(UITableView*)tableView willRemoveObject:(id)object
              atIndexPath:(NSIndexPath*)indexPath;

- (NSString*)titleForLoading:(BOOL)reloading;

- (UIImage*)imageForEmpty;

- (NSString*)titleForEmpty;

- (NSString*)subtitleForEmpty;

- (UIImage*)imageForError:(NSError*)error;

- (NSString*)titleForError:(NSError*)error;

- (NSString*)subtitleForError:(NSError*)error;

- (NSString*)titleForRetryButton:(NSError*)error;

@end

#pragma mark Sectioned Array Objects

// Classes used when creating UXCollectionViewDataSources.
@class UXCollectionViewDataSourceFooter;  // Provides the information for a footer.

/**
 * A non-mutable collection view model that complies to the UICollectionViewDataSource protocol.
 *
 * This model allows you to easily create a data source for a UICollectionView without having to
 * implement the UICollectionViewDataSource methods in your controller.
 *
 * This base class is non-mutable, much like an NSArray. You must initialize this model with
 * the contents when you create it.
 *
 * This model simply manages the data relationship with your collection view. It is up to you to
 * implement the collection view's layout object.
 *
 * @ingroup CollectionViewModels
 */
@interface UXCollectionViewDataSource : NSObject <UXModelCollectionViewDataSourceDelegate>

@property (nonatomic, strong) UXNetworkModel* model;

#pragma mark Creating Collection View Models

// Designated initializer.
- (id)initWithDelegate:(id<UXCollectionViewDataSourceDelegate>)delegate;
- (id)initWithListArray:(NSArray *)listArray delegate:(id<UXCollectionViewDataSourceDelegate>)delegate;
// Each NSString in the array starts a new section. Any other object is a new row (with exception of certain model-specific objects).
- (id)initWithSectionedArray:(NSArray *)sectionedArray delegate:(id<UXCollectionViewDataSourceDelegate>)delegate;

#pragma mark Accessing Objects

- (id)objectAtIndexPath:(NSIndexPath *)indexPath;

// This method is not appropriate for performance critical codepaths.
- (NSIndexPath *)indexPathForObject:(id)object;

#pragma mark Creating Collection View Cells

@property (nonatomic, weak) id<UXCollectionViewDataSourceDelegate> delegate;

@end

/**
 * A protocol for UXCollectionViewDataSource to fetch rows to be displayed for the collection view.
 *
 * @ingroup CollectionViewModels
 */
@protocol UXCollectionViewDataSourceDelegate <NSObject>
@required

/**
 * Fetches a collection view cell at a given index path with a given object.
 *
 * The implementation of this method will generally use object to customize the cell.
 */
- (UICollectionViewCell *)collectionViewModel:(UXCollectionViewDataSource *)collectionViewModel
                        cellForCollectionView:(UICollectionView *)collectionView
                                  atIndexPath:(NSIndexPath *)indexPath
                                   withObject:(id)object;

@optional

/**
 * Fetches a supplementary collection view element at a given indexPath.
 *
 * The value of the kind property and indexPath are implementation-dependent
 * based on the type of UICollectionViewLayout being used.
 */
- (UICollectionReusableView *)collectionViewModel:(UXCollectionViewDataSource *)collectionViewModel
                                   collectionView:(UICollectionView *)collectionView
                viewForSupplementaryElementOfKind:(NSString *)kind
                                      atIndexPath:(NSIndexPath *)indexPath;

@end

/**
 * An object used in sectioned arrays to denote a section footer title.
 *
 * Meant to be used in a sectioned array for UXCollectionViewDataSource.
 *
 * <h3>Example</h3>
 *
 * @code
 *  [UXCollectionViewDataSourceFooter footerWithTitle:@"Footer"]
 * @endcode
 */
@interface UXCollectionViewDataSourceFooter : NSObject

+ (id)footerWithTitle:(NSString *)title;
- (id)initWithTitle:(NSString *)title;

@property (nonatomic, copy) NSString* title;

@end

/** @name Creating Collection View Models */

/**
 * Initializes a newly allocated static model with the given delegate and empty contents.
 *
 * This method can be used to create an empty model.
 *
 * @fn UXCollectionViewDataSource::initWithDelegate:
 */

/**
 * Initializes a newly allocated static model with the contents of a list array.
 *
 * A list array is a one-dimensional array that defines a flat list of rows. There will be
 * no sectioning of contents in any way.
 *
 * <h3>Example</h3>
 *
 * @code
 * NSArray* contents =
 * [NSArray arrayWithObjects:
 *  [NSDictionary dictionaryWithObject:@"Row 1" forKey:@"title"],
 *  [NSDictionary dictionaryWithObject:@"Row 2" forKey:@"title"],
 *  [NSDictionary dictionaryWithObject:@"Row 3" forKey:@"title"],
 *  nil];
 * [[UXCollectionViewDataSource alloc] initWithListArray:contents delegate:self];
 * @endcode
 *
 * @fn UXCollectionViewDataSource::initWithListArray:delegate:
 */

/**
 * Initializes a newly allocated static model with the contents of a sectioned array.
 *
 * A sectioned array is a one-dimensional array that defines a list of sections and each
 * section's contents. Each NSString begins a new section and any other object defines a
 * row for the current section.
 *
 * <h3>Example</h3>
 *
 * @code
 * NSArray* contents =
 * [NSArray arrayWithObjects:
 *  @"Section 1",
 *  [NSDictionary dictionaryWithObject:@"Row 1" forKey:@"title"],
 *  [NSDictionary dictionaryWithObject:@"Row 2" forKey:@"title"],
 *  @"Section 2",
 *  // This section is empty.
 *  @"Section 3",
 *  [NSDictionary dictionaryWithObject:@"Row 3" forKey:@"title"],
 *  [UXCollectionViewDataSourceFooter footerWithTitle:@"Footer"],
 *  nil];
 * [[UXCollectionViewDataSource alloc] initWithSectionedArray:contents delegate:self];
 * @endcode
 *
 * @fn UXCollectionViewDataSource::initWithSectionedArray:delegate:
 */


/** @name Accessing Objects */

/**
 * Returns the object at the given index path.
 *
 * If no object exists at the given index path (an invalid index path, for example) then nil
 * will be returned.
 *
 * @fn UXCollectionViewDataSource::objectAtIndexPath:
 */

/**
 * Returns the index path of the given object within the model.
 *
 * If the model does not contain the object then nil will be returned.
 *
 * @fn UXCollectionViewDataSource::indexPathForObject:
 */


/** @name Creating Collection View Cells */

/**
 * A delegate used to fetch collection view cells for the data source.
 *
 * @fn UXCollectionViewDataSource::delegate
 */

