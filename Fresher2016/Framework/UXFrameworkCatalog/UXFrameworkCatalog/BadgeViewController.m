/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "BadgeViewController.h"

@interface BadgeViewController () {
    BOOL isModelController;
}

@property (nonatomic, strong) UXButton* button;
@property (nonatomic, strong) UXBadgeView* badgeView;

@end

@implementation BadgeViewController

- (id)initWithBool:(BOOL)isModel {
    if (self = [super init]) {
        isModelController = isModel;
    }
    
    return self;
}

- (id)initWithNavigatorURL:(NSURL*)URL query:(NSDictionary*)query {
    if (self = [super init]) {
        if (query) {
            NSLog(@"Query %@", query);
            isModelController = [[query objectForKey:@"isModelNumber"] boolValue];
        }
    }
    
    return self;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.titleString = @"Badge";
    }
    return self;
}

- (void)dealloc {
    [_button removeFromSuperview];
    [_badgeView removeFromSuperview];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // iOS 7-only.
    
    // The default view background color is transparent, so let's make sure the badge view has a
    // non-transparent background color when we assign it further down.
    self.view.backgroundColor = [UIColor whiteColor];
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 30)];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setText:UX_LOCALIZE_STRING(@"Click to green button now!")];
    [titleLabel setTextColor:[UIColor lightGrayColor]];
    [self.view addSubview:titleLabel];
    
    _button = [UXButton buttonWithStyle:@"avatarDefaultButtonStyle:"];
    _button.frame = CGRectMake(self.view.frame.size.width/2 - 100, titleLabel.bottom + 10, 200, 200);
    [_button addTarget:self action:@selector(onClickOnButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_button];
    
    // We don't know what the initial frame will be, so pass the empty rect.
    _badgeView = [[UXBadgeView alloc] init];
    
    // Avoid using transparency whenever possible. The badgeView backgroundColor is black by default.
    _badgeView.backgroundColor = [UIColor clearColor];
    _badgeView.textColor = [UIColor whiteColor];
    _badgeView.tintColor = [UIColor redColor];
    _badgeView.font = UXSTYLEVAR(customNormalFont14);
    
    // We can set any arbitrary text to the badge view.
    _badgeView.text = @"7";
    [_badgeView setFrame:CGRectMake(_button.frame.origin.x + _button.frame.size.width/2 + 40, _button.frame.origin.y, 30, 30)];
    [self.view addSubview:_badgeView];
    // Once we've set the text we allow the badge view to size itself to fit the contents of the
    // text. If we wanted to we could explicitly set the frame of the badge view to something larger,
    // but in nearly all cases the sizeToFit rect will be the one you want to use.
    [_badgeView sizeToFit];
    
//    self.navigationItem.rightBarButtonItem =
//    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
//                                                  target:self
//                                                  action:@selector(pushSelfController:)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pushSelfController:(id)sender {
    
    UIViewController* topViewController = [UXNavigationManager navigator].topViewController;

    if (isModelController) {
        [[UXNavigationManager navigator] popToRootViewController:topViewController completion:^{
            openURL(@"ux://viewController");
        }];
    } else {
        UXURLAction* urlaction = [[UXURLAction alloc] initWithURLPath:@"ux://BadgeViewController/1"];
        urlaction.query = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], @"isModelNumber", nil];
        urlaction.animated = YES;
        [[UXNavigationManager navigator] openURLAction:urlaction];
    }
    

}

- (void)onClickOnButton {
    
    ////////////////////////////////////////////////////////////////////////////////
    // DEMO ACTION SHEET
    ////////////////////////////////////////////////////////////////////////////////
    // Show an alert view that callbacks an URL completion handler
    [UXAlertController showURLAlertViewInViewController:self withTitle:UX_LOCALIZE_STRING(@"Alert")  message:UX_LOCALIZE_STRING(@"Message") cancelButtonTitle:UX_LOCALIZE_STRING(@"Cancel") cancelButtonURL:@"ux://cancel" destructiveButtonTitle:UX_LOCALIZE_STRING(@"Delete") destructiveButtonURL:@"ux://delete" otherButtonTitles:nil otherButtonURLs:nil completionHandler:^(UXAlertController *alert, NSString *buttonURL) {
        NSLog(@"On click button with URL %@", buttonURL);
    }];

//    // Show an action sheet that callbacks an URL completion handler
//    [UXAlertController showURLActionSheetInViewController:self withTitle:UX_LOCALIZE_STRING(@"Alert") message:UX_LOCALIZE_STRING(@"Message") cancelButtonTitle:UX_LOCALIZE_STRING(@"Cancel") cancelButtonURL:@"ux://cancel" destructiveButtonTitle:UX_LOCALIZE_STRING(@"Delete") destructiveButtonURL:@"ux://delete" otherButtonTitles:[NSArray arrayWithObjects:UX_LOCALIZE_STRING(@"Hide this activity"), UX_LOCALIZE_STRING(@"I don't want to see"), nil] otherButtonURLs:[NSArray arrayWithObjects:@"ux://hide", @"ux://report", nil] popoverPresentationControllerHandler:nil completionHandler:^(UXAlertController *alert, NSString *buttonURL) {
//         NSLog(@"On click button with URL %@", buttonURL);
//    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
