//
//  PreviewImageViewController.h
//  CameraGPUImage
//
//  Created by qhcthanh on 6/14/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"

#import "GPUStickerView.h"
#import "TextStickerNode.h"

@interface PreviewImageViewController : UIViewController

@property UIImage* previewImage;
@property NSInteger indexFilter;
@property NSMutableArray<Class> *filterList;

@property (weak, nonatomic) IBOutlet UIView *saveStateView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIView *bottomBar;
@property (weak, nonatomic) IBOutlet UIView *topBar;


@property (strong, nonatomic) GPUStickerView *gpuStickerView;

- (IBAction)textShowAction:(id)sender;
@end
