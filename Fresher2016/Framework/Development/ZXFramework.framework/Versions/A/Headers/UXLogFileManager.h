/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>


/**
 The LogFileManager protocol is designed to allow you to control all aspects of your log files.
 
 The primary purpose of this is to allow you to do something with the log files after they have been rolled.
 Perhaps you want to compress them to save disk space.
 Perhaps you want to upload them to an FTP server.
 Perhaps you want to run some analytics on the file.
 
 A default LogFileManager is, of course, provided.
 The default LogFileManager simply deletes old log files according to the maximumNumberOfLogFiles property.
 
 This protocol provides various methods to fetch the list of log files.
 
 There are two variants: sorted and unsorted.
 If sorting is not necessary, the unsorted variant is obviously faster.
 The sorted variant will return an array sorted by when the log files were created,
 with the most recently created log file at index 0, and the oldest log file at the end of the array.
 
 You can fetch only the log file paths (full path including name), log file names (name only),
 or an array of DDLogFileInfo objects.
 The DDLogFileInfo class is documented below, and provides a handy wrapper that
 gives you easy access to various file attributes such as the creation date or the file size. 
 */
@protocol UXLogFileManager <NSObject>

@required

// Public properties

@property (readwrite, assign) NSUInteger maximumNumberOfLogFiles;
@property (copy) NSString* filePrefix;

// Public methods

- (NSString *)logsDirectory;

- (NSArray *)unsortedLogFilePaths;
- (NSArray *)unsortedLogFileNames;
- (NSArray *)unsortedLogFileInfos;

- (NSArray *)sortedLogFilePaths;
- (NSArray *)sortedLogFileNames;
- (NSArray *)sortedLogFileInfos;

// Private methods (only to be used by UXFileLogger)
- (NSString *)createNewLogFile;

@optional

// Notifications from UXFileLogger
- (void)didArchiveLogFile:(NSString *)logFilePath;
- (void)didRollAndArchiveLogFile:(NSString *)logFilePath;

@end
