//
//  NewContactViewController.h
//  ZaloContactInviting
//
//  Created by VanDao on 6/2/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactDetail.h"
#import "ContactProtocol.h"

@interface NewContactController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITableViewDelegate, UITableViewDataSource>

// Delegate
@property(weak) id<ContactProtocol> delegate;

// Outlet
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *familyNameTextField;
@property (weak, nonatomic) IBOutlet UITableView *phoneNumberTableView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightOfTableView;


// Action
- (IBAction)firstNameEditing:(id)sender;
- (IBAction)familyNameEditing:(id)sender;

/**
 *  Insert new phone number row on table view 
 */
- (IBAction)newPhoneNumberButton:(id)sender;

/**
 *  Using Add contact protocol to delegate new contact to LoadContactController
 *
 */
- (IBAction)addContactBarItem:(id)sender;


@end
