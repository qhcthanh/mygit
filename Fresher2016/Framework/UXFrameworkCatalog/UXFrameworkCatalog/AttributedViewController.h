/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

#import <UXFramework/UXFramework.h>

@interface AttributedViewController : UXViewController

@end
