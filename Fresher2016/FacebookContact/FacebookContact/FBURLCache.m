//
//  FBURLCache.m
//  FacebookContact
//
//  Created by VanDao on 7/25/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import "FBURLCache.h"
#import "mach/mach.h"
#import "mach/mach_host.h"

@interface FBURLCache ()

@property (nonatomic, strong) NSMutableArray<NSURLRequest *> *responceArray;

@end

@implementation FBURLCache

+ (nonnull FBURLCache *)sharedURLCache {
    
    static FBURLCache *sharedCache;
    
    dispatch_once_t onceSharedURLCache;
    dispatch_once(&onceSharedURLCache, ^{
        NSUInteger freeMemory = [self getAvailableMemory];
        NSUInteger freeDisk = [self getAvailableDisk];
        
        sharedCache = [[FBURLCache alloc]initWithMemoryCapacity:freeMemory * 0.5 diskCapacity:freeDisk * 0.5 diskPath:@"/Users/admin/Documents/FBTempDirectory"];
    });
    
    return sharedCache;
}

- (instancetype)initWithMemoryCapacity:(NSUInteger)memoryCapacity diskCapacity:(NSUInteger)diskCapacity diskPath:(nullable NSString *)path{
    self = [super initWithMemoryCapacity:memoryCapacity diskCapacity:diskCapacity diskPath:path];
    
    if (self) {
        _responceArray = [[NSMutableArray alloc]init];
    }
    
    return self;
}

- (nullable FBCachedURLResponse *)cachedResponseForRequest:(nonnull NSURLRequest *)request {

    FBCachedURLResponse *responceForRequest = (FBCachedURLResponse *)[super cachedResponseForRequest:request];
    [responceForRequest updateFocusDate];
    
    return responceForRequest;
}

- (void)removeCachedResponseForRequest:(nonnull NSURLRequest *)request {
    
    [super removeCachedResponseForRequest:request];
    [_responceArray removeObject:request];
}

- (void)storeCachedResponse:(nonnull FBCachedURLResponse *)cachedResponse forRequest:(nonnull NSURLRequest *)request {
    
    [cachedResponse updateFocusDate];
    [super storeCachedResponse:cachedResponse forRequest:request];
    [_responceArray addObject:request];
}

- (void)removeCachedResponsesBeforDate:(nonnull NSDate *)date {
    
    for (NSURLRequest *request in _responceArray) {
        FBCachedURLResponse *response = [self cachedResponseForRequest:request];
        if ([response isResponseOutOfDateWithExpiredDate:date]) {
            [self removeCachedResponseForRequest:request];
        }
    }
}

- (void)removeCachedResponsesOutOfDate {
    
    for (NSURLRequest *request in _responceArray) {
        FBCachedURLResponse *response = [self cachedResponseForRequest:request];
        if ([response isResponseOutOfDate]) {
            [self removeCachedResponseForRequest:request];
        }
    }
}

- (BOOL)checkMemoryWarning {
    
    return self.memoryCapacity * 0.9 < self.currentMemoryUsage;
}

- (BOOL)checkDiskWarning {
    
    return self.diskCapacity * 0.9 < self.currentDiskUsage;
}

+ (unsigned int) getAvailableMemory {
    
    natural_t mem_free;
    //static natural_t mem_used;
    //static natural_t mem_total;
    
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;
    
    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &pagesize);
    
    vm_statistics_data_t vm_stat;
    
    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS) {
        NSLog(@"Failed to fetch vm statistics");
    }
    
    /* Stats in bytes */
    //mem_used = (vm_stat.active_count + vm_stat.inactive_count + vm_stat.wire_count) * (unsigned int)pagesize;
    mem_free = vm_stat.free_count * (unsigned int) pagesize;
    //mem_total = mem_used + mem_free;
    //NSLog(@"used: %u free: %u total: %u", mem_used, mem_free, mem_total);
    
    return mem_free;
}

+ (unsigned int)getAvailableDisk {
    
    unsigned int freeSpace = 0;
    
    NSError *error = nil;
    
    // Check if directory exists
    BOOL isDirectory = YES;
    BOOL directoryExists = [[NSFileManager defaultManager]fileExistsAtPath:@"/Users/admin/Documents/FBTempDirectory" isDirectory:&isDirectory];
    
    // Create a new directory if directory don't exist
    if (!directoryExists) {
        [[NSFileManager defaultManager] createDirectoryAtPath:@"/Users/admin/Documents/FBTempDirectory"
                                       withIntermediateDirectories:NO
                                                        attributes:nil
                                                        error:nil];
    }
    
    // Get info of directory
    NSDictionary *directoryInfo = [[NSFileManager defaultManager] attributesOfFileSystemForPath:@"/Users/admin/Documents/FBTempDirectory" error: &error];
    
    // Get free space of directory
    if (directoryInfo) {
        NSNumber *fileSystemFreeSizeInBytes = [directoryInfo objectForKey: NSFileSystemFreeSize];
        //NSLog(@"%@",fileSystemFreeSizeInBytes);
        freeSpace = [fileSystemFreeSizeInBytes intValue];
    } else {
        //Handle error
    }
    
    return freeSpace;
}

@end
