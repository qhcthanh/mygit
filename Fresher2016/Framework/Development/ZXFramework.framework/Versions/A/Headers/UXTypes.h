/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Alexey Belkevich, 2014
 * Updated by ThemND VNG Coporation, 2015
 * Change Log: Integrate Contact Framework that supports the iOS9
 * All rights reserved.
 */

#ifndef ZXFramework_UXTypes_h
#define ZXFramework_UXTypes_h

@class UXContact;

typedef NS_ENUM(NSUInteger, UXAddressBookAccessStatus)
{
    UXAddressBookAccessStatusUnknown = 0,
    UXAddressBookAccessStatusGranted = 1,
    UXAddressBookAccessStatusDenied  = 2
};

typedef BOOL(^UXContactFilterBlock)(UXContact *contact);

typedef NS_OPTIONS(NSUInteger , UXContactField)
{
    UXContactFieldFirstName        = 1 << 0,
    UXContactFieldLastName         = 1 << 1,
    UXContactFieldCompany          = 1 << 2,
    UXContactFieldPhones           = 1 << 3,
    UXContactFieldEmails           = 1 << 4,
    UXContactFieldPhoto            = 1 << 5,
    UXContactFieldThumbnail        = 1 << 6,
    UXContactFieldPhonesWithLabels = 1 << 7,
    UXContactFieldCompositeName    = 1 << 8,
    UXContactFieldAddresses        = 1 << 9,
    UXContactFieldRecordID         = 1 << 10,
    UXContactFieldCreationDate     = 1 << 11,
    UXContactFieldModificationDate = 1 << 12,
    UXContactFieldMiddleName       = 1 << 13,
    UXContactFieldSocialProfiles   = 1 << 14,
    UXContactFieldNote             = 1 << 15,
    UXContactFieldLinkedRecordIDs  = 1 << 16,
    UXContactFieldJobTitle         = 1 << 17,
    UXContactFieldDefault          = UXContactFieldFirstName | UXContactFieldLastName | UXContactFieldMiddleName |
    UXContactFieldPhones | UXContactFieldCreationDate | UXContactFieldModificationDate | UXContactFieldRecordID,
    UXContactFieldAll              = 0xFFFFFFFF
};

#endif
