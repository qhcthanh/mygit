/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXLauncherView.h"

/**
 * The UXLauncherViewController class creates a controller object that manages a launcher view.
 * It implements the following behavior:
 *
 * - It creates an unconfigured UXLauncherView object with the correct dimensions and autoresize
 *   mask. You can access this view through the launcherView property.
 * - UXLauncherViewController sets the data source and the delegate of the launcher view to self.
 * - When the launcher view is about to appear the first time it’s loaded, the launcher-view
 *   controller reloads the launcher view’s data.
 *
 * @image html UXLauncherViewControllerExample1.png "Example of an UXLauncherViewController."
 *
 */
@interface UXLauncherViewController : UIViewController <UXLauncherViewDelegate, UXLauncherDataSource>

/**
 * Returns the launcher view managed by the controller object.
 *
 */
@property (nonatomic, strong) UXLauncherView* launcherView;

@end
