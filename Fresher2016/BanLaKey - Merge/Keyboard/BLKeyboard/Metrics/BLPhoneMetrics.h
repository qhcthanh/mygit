//
//  BLPhoneMetrics.h
//  BanLaKey
//
//  Created by BaoNQ on 8/16/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>

typedef struct {
    
    CGFloat rowMargin;
    
    CGFloat columnMargin;
    
    CGFloat keyHeight;
    
    CGFloat letterKeyWidth;
    
    
    CGFloat nextKeyboardButtonWidth;
    
    CGFloat returnButtonWidth;
    
    CGFloat deleteButtonWidth;
    
    CGFloat shiftButtonWidth;
    
    
    CGFloat popupTotalHeight;
    
    CGFloat popupWidthIncrement;
    
    CGFloat popupConnectorHeight;
    
    CGFloat popupWidth;
    
    CGFloat popupHeight;
    
} PhoneKeyboardMetrics;

PhoneKeyboardMetrics generatePhoneKeyboardMetrics(CGFloat keyboardWidth, CGFloat keyboardHeight);

CGRect calculatePhonePopupFrame(CGSize keySize, CGFloat keyboardWidth, CGFloat keyboardHeight);
