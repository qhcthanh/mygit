//
//  SNTextField.m
//  CameraGPUImage
//
//  Created by BaoNQ on 8/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "SNTextField.h"

#define kTextFieldHeight 40
#define kTextFieldLayerZPosition 100
#define kTextFieldFontSize 16

@interface SNTextField () <UITextFieldDelegate, UIGestureRecognizerDelegate>

@end

@implementation SNTextField

- (id)initWithWidth:(CGFloat)width yPosition:(CGFloat)yPos screenHeight:(CGFloat)screenHeight {
    
    self = [super initWithFrame:CGRectMake(0, yPos, width, kTextFieldHeight)];
    
    if (self) {
        self.location = CGPointMake(0, yPos);
        self.heightOfScreen = screenHeight;
        
        self.layer.zPosition = kTextFieldLayerZPosition;
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.45];
        self.tintColor = [UIColor whiteColor];
        self.textColor = [UIColor whiteColor];
        self.placeholder = @"";
        self.font = [UIFont systemFontOfSize:kTextFieldFontSize];
        self.borderStyle = UITextBorderStyleNone;
        self.autocorrectionType = UITextAutocorrectionTypeNo;
        self.keyboardType = UIKeyboardTypeDefault;
        self.returnKeyType = UIReturnKeyDone;
        self.clearButtonMode = UITextFieldViewModeNever;
        self.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.textAlignment = NSTextAlignmentCenter;
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        self.delegate = self;
        self.hidden = YES;
        
        self.panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        self.panGesture.delegate = self;
        [self addGestureRecognizer:self.panGesture];
    }
    return self;
}

- (void)show {
    self.hidden = NO;
}

- (void)hideKeyboard {
    [self resignFirstResponder];
}

- (void)showKeyboard {
    [self becomeFirstResponder];
}

//MARK: - UITextField Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([self.text isEqualToString:@""]) {
        self.hidden = YES;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *text = [self.text stringByReplacingCharactersInRange:range withString:string];
    CGFloat contentWidth = [text sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:kTextFieldFontSize]}].width;
    
    return contentWidth <= (self.frame.size.width - 20);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self hideKeyboard];
    return YES;
}

//MARK: - Extension Gesture Recognizer Delegate and touch handler for TextField

- (void)handleTap {
    if (self.hidden == YES) {
        [self show];
        [self showKeyboard];
    }
    else {
        [self hideKeyboard];
    }
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    
    if ([self isFirstResponder])
        return;
    
    CGPoint position = [self.panGesture locationInView:self];
    CGFloat tempLocation = self.location.y + position.y;
    
    if (tempLocation < 0) {
        tempLocation = 0;
    }
    else if (tempLocation > self.heightOfScreen - self.frame.size.height) {
        tempLocation = self.heightOfScreen - self.frame.size.height;
    }
    
    self.location = CGPointMake(self.location.x, tempLocation);
    self.frame = CGRectMake(self.frame.origin.x, tempLocation, self.frame.size.width, self.frame.size.height);
}



@end
