//
//  ShowImageCaptureViewController.h
//  CameraWithAVFoundation
//
//  Created by qhcthanh on 6/8/16.
//  Copyright © 2016 Gabriel Alvarado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import "CTFilter.h"

@interface ShowImageCaptureViewController : UIViewController {
    
}
@property UIImage* captureImage;
@end
