/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@interface UIDevice (Extension)
- (NSString *) platform;
- (NSString *) platformString;
- (NSString *) modelPlatformString;
- (NSUInteger) cpuFrequency;
- (NSUInteger) busFrequency;
- (NSUInteger) totalMemory;
- (NSUInteger) userMemory;
- (double) availableMemory;

- (NSNumber *) totalDiskSpace;
- (NSNumber *) freeDiskSpace;

- (NSString *)getMacAddress;
- (NSString *)getIPAddress;
- (BOOL)isDeviceiPhone5;
- (BOOL)isDeviceiPhone6;
- (BOOL)isDeviceiPhone6Plus;
- (BOOL)isRentinaPhone;
- (BOOL)isDeviceJailbroken;
- (NSString*)currentWifiSSID;
@end
