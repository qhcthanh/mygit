//
//  AddContactViewController.m
//  ZaloContact
//
//  Created by qhcthanh on 6/2/16.
//  Copyright © 2016 qhcthanh. All rights reserved.
//

#import "AddContactViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "UIImageView+Mask.h"
#import "CTAddressBook.h"

@implementation AddContactViewController {
    
    __weak IBOutlet UIButton *phoneTypeButton;
    __weak IBOutlet UITextField *phoneAddTextField;
    __weak IBOutlet UITableView *phoneTableView;
    __weak IBOutlet UIView *addPhoneView;
    __weak IBOutlet UIButton *addPhotoButton;
    __weak IBOutlet UILabel *addPhotoLabel;
    __weak IBOutlet UIImageView *avatarImageView;
    __weak IBOutlet UITextField *lastNameTextField;
    __weak IBOutlet UITextField *firstNameTextField;
    
    NSMutableArray* phoneArray;
    UIImagePickerController *ipc;
    UIPopoverController *popover;
    UIImage* contactImage;
    PhoneType currentPhoneType;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    currentPhoneType = kPhoneTypeMobile;
    [self changeImagePhoneType];
    phoneArray = [[NSMutableArray alloc] init];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [avatarImageView maskAvatarCircle: CGRectMake(0, 0, 55, 55)];
    [self.view addGestureRecognizer:tapGesture];
}


#pragma mark - UITextField delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == firstNameTextField) {
        [lastNameTextField becomeFirstResponder];
    } else if (textField == lastNameTextField) {
        [phoneAddTextField becomeFirstResponder];
    } else {
        [self addPhoneAction:nil];
    }
    return true;
}

#pragma mark - Button Action vs Tap Action
-(void)tapAction {
    [self.view endEditing:true];
}

-(void)changeImagePhoneType {
    switch (currentPhoneType) {
        case kPhoneTypeMobile:
            [phoneTypeButton setImage:[UIImage imageNamed:@"Phone-50"] forState:UIControlStateNormal];
            break;
        case kPhoneTypeFax:
            [phoneTypeButton setImage:[UIImage imageNamed:@"Fax-50"] forState:UIControlStateNormal];
            break;
        case kPhoneTypeHome:
            [phoneTypeButton setImage:[UIImage imageNamed:@"Home-50"] forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}

- (IBAction)addPhoneAction:(id)sender {
    if(phoneAddTextField.text.length > 0) {
        [phoneArray addObject:phoneAddTextField.text];
        [phoneTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:phoneArray.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        phoneAddTextField.text = @"";
    }
}

- (IBAction)changePhoneTypeAction:(id)sender {
    if (currentPhoneType == kPhoneTypeMobile) {
        currentPhoneType = kPhoneTypeHome;
    } else if (currentPhoneType == kPhoneTypeHome) {
        currentPhoneType = kPhoneTypeFax;
    } else {
        currentPhoneType = kPhoneTypeMobile;
    }
    [self changeImagePhoneType];
}

- (IBAction)addPhotoAction:(id)sender {
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized) {
        // do your logic
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
    } else if(authStatus == AVAuthorizationStatusDenied){
        // denied
    } else if(authStatus == AVAuthorizationStatusRestricted){
        // restricted, normally won't happen
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        // not determined?!
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){
                NSLog(@"Granted access to %@", AVMediaTypeVideo);
                [self addPhoneAction:nil];
            } else {
                NSLog(@"Not granted access to %@", AVMediaTypeVideo);
            }
        }];
    } else {
        // impossible, unknown authorization status
    }
    
    
    
}

- (IBAction)addContactAction:(id)sender {
    if(firstNameTextField.text.length > 0 || lastNameTextField.text.length > 0) {
        NSString* identifield = [NSString stringWithFormat:@"%f", [NSDate timeIntervalSinceReferenceDate]];
        CTAddressBook* contact = [[CTAddressBook alloc] initWithInfo:identifield firstName:firstNameTextField.text lastName:lastNameTextField.text phones:phoneArray image: contactImage];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CREATECONTACT" object:nil userInfo:@{@"contact":contact}];
        [self.navigationController popViewControllerAnimated:true];
    }
}

#pragma mark - UITableViewDelegate + UITableView DataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return phoneArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PhoneCell* cell = [tableView dequeueReusableCellWithIdentifier:@"PhoneCell"];
    [cell cleanUI];
    [cell renderUI:currentPhoneType phone:phoneArray[indexPath.row]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return true;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(editingStyle == UITableViewCellEditingStyleDelete) {
        [phoneArray removeObjectAtIndex:indexPath.row];
        [phoneTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - ImagePickerController Delegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        [picker dismissViewControllerAnimated:YES completion:nil];
    } else {
        [popover dismissPopoverAnimated:YES];
    }
    contactImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    avatarImageView.image = contactImage;
   [self performSelector:@selector(renderUI) withObject:nil afterDelay:0];
}

-(void)renderUI {
    CGRect buttonFrame = addPhotoButton.frame;
    buttonFrame.origin.y += 50;
    [addPhotoButton setFrame:buttonFrame];
    
    addPhotoLabel.text = @"Edit";
    CGRect addPhotoFrame = addPhotoLabel.frame;
    [addPhotoLabel setFrame:addPhotoFrame];
}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
