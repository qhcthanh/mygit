//
//  MDActionType.h
//  CaptureVideoOverlay
//
//  Created by manhduydl on 8/2/16.
//  Copyright © 2016 admin. All rights reserved.
//

#ifndef MDActionType_h
#define MDActionType_h

typedef NS_ENUM(NSUInteger, MDActionType)
{
    MDNodAction = 0,
    MDAddSticker = 1,
    MDClearAllSticker  = 2,
    MDChangePositonSticker = 3,
    MDRecordAction = 4
};

#endif /* MDActionType_h */
