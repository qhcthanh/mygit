//
//  CTCIFilter.h
//  CameraGPUImage
//
//  Created by BaoNQ on 6/13/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreImage;


typedef enum  {
    CTColorMonochrome, //User denied contact permission
    CTToneCurve,
    CTBlur,
} CTFilterName;

@interface CTFilter : NSObject

@property (nonatomic, strong) CIImage *outputImage;

+ (CTFilter *)filterImage:(CIImage *)image withFilterName:(CTFilterName)filterName;

- (BOOL)clear;

@end
