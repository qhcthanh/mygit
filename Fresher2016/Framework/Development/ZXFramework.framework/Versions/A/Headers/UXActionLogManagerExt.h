/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@interface UXActionLogManagerExt : NSObject

+ (UXActionLogManagerExt*)getInstanceWithCallback:(id)delegate;

@end
