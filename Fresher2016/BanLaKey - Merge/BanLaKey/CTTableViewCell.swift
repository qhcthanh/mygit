//
//  CTTableViewCell.swift
//  BanLaKey
//
//  Created by VanDao on 9/5/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import Foundation
import UIKit

@objc protocol CTTableViewCellDelegate: NSObjectProtocol {
    
    optional func changeSwitchValue(tableViewCell: CTTableViewCell, value: Bool)
    
}

class CTTableViewCell : UITableViewCell {
    
    @IBOutlet var rightView: UIView!
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var label: UILabel!
    @IBOutlet var switchControl: UISwitch!
    
    weak var delegate: CTTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if let switchControl = self.switchControl {
            switchControl.addTarget(self, action: #selector(changeSwitchValue), forControlEvents: .ValueChanged)
            self.selectionStyle = .None
        }
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        if iconImageView != nil {
            self.iconImageView.image = nil
        }
        self.label.text = ""
    }
    
    func changeSwitchValue(sender: UISwitch) {
        
        if let delegate = self.delegate, let changeSwitchValue = delegate.changeSwitchValue {
            changeSwitchValue(self, value: sender.on)
        }
        
    }
    
}