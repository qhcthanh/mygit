//
//  ViewController.m
//  FacebookContact
//
//  Created by qhcthanh on 7/13/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import "AuthenticateViewController.h"
#import "FriendsFacebookViewController.h"
#import "FBUserInfo.h"
#import "FBUserManager.h"
#import "FBImageDownloader.h"
#import "FBAPIUtility.h"

@import FBSDKLoginKit;
@import FBSDKCoreKit;
@import CoreTelephony;
@import AVFoundation;

@interface AuthenticateViewController () <NSURLConnectionDelegate, AVAudioRecorderDelegate>

@property (weak, nonatomic) IBOutlet UIButton *loginFacebookButton;
@property (weak, nonatomic) IBOutlet UIButton *friendSegueButton;
@property UIImageView* pictureImageView;
@property UIDynamicAnimator* animator;
@property UIGravityBehavior* gravity;
@property NSMutableArray* friendArray;
@property NSURL *nextFriendsURLRequest;
@property NSArray* permissionFacebookArray;
@end

@implementation AuthenticateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //init gravity
    _animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    // Setting facebook login
    //_loginFacebookButton.readPermissions =
    _permissionFacebookArray = @[@"public_profile", @"email", @"user_friends",@"user_about_me",@"user_birthday"];
    
    _friendArray = [NSMutableArray new];
     _friendSegueButton.hidden = true;
    
    // Check isLogin
    if([FBAPIUtility isUserLoginFacebook]) {
        NSLog(@"is Login");
        [_loginFacebookButton setTitle:@"Log-Out" forState:UIControlStateNormal];
    } else {
        NSLog(@"Not login");
        [_loginFacebookButton setTitle:@"Login with Facebook" forState:UIControlStateNormal];
    }
    
    //Load Data
    [self requestLoadFriendFacebook];
    [self requestUserInfoFacebook];
    [self initialize];
}

- (void)initialize {
    
    // Rest _loginFacebookButton to add behavior
    CGRect currentOrigin = _loginFacebookButton.frame;
    [_loginFacebookButton removeFromSuperview];
    currentOrigin.origin.x = CGRectGetMidX(self.view.frame) - currentOrigin.size.width/2;
    currentOrigin.origin.y += currentOrigin.size.height/2;
    [_loginFacebookButton setFrame:currentOrigin];
    [self.view addSubview:_loginFacebookButton];
    
    // Add User ImageView with placeholder image
    _pictureImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.view.frame) - 25, _loginFacebookButton.frame.origin.y - 100, 50, 50)];
    _pictureImageView.image = [UIImage imageNamed:@"default_user"];
    
    [self.view addSubview:_pictureImageView];
    
    // Initiallize gravity
    _gravity = [[UIGravityBehavior alloc] initWithItems:@[_pictureImageView]];
    _gravity.gravityDirection = CGVectorMake(0, 3);
    
    // Add collision to views
    UICollisionBehavior* collision = [[UICollisionBehavior alloc] initWithItems:@[_pictureImageView,_loginFacebookButton,_friendSegueButton]];
    collision.translatesReferenceBoundsIntoBoundary= true;
    
    // Add UIDynamicItemBehavior item with max density. When max density a object will not fall in bottom view
    UIDynamicItemBehavior *loginFacebookButtonBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[_loginFacebookButton,_friendSegueButton]];
    loginFacebookButtonBehavior.allowsRotation = NO;
    loginFacebookButtonBehavior.density = 999999999.999;
    
    // Add UIDynamicItemBehavior to pictureImageView. pictureImageView can jump
    UIDynamicItemBehavior *pictureViewBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[_pictureImageView]];
    pictureViewBehavior.elasticity = 0.9;
    pictureViewBehavior.resistance = 0;
    
    [_animator addBehavior:pictureViewBehavior];
    [_animator addBehavior:loginFacebookButtonBehavior];
    [_animator addBehavior:collision];
    [_animator addBehavior:_gravity];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"FriendsFacebookSegue"]) {
        id destVC = segue.destinationViewController;
        if( destVC && [destVC isKindOfClass:[FriendsFacebookViewController class]]) {
            FriendsFacebookViewController* vc = (FriendsFacebookViewController*)destVC;
            
            vc.friendArray = _friendArray;
            vc.nextFriendsURLRequest = _nextFriendsURLRequest;
        }
    }
}

#pragma mark - Request Data
- (void)requestLoadFriendFacebook {
    if([FBAPIUtility isUserLoginFacebook]) {
        FBAPIUtility* request = [[FBAPIUtility alloc] initWithSessionManager:[FBSessionManager sharedDefaultSessionManager]];
        
        [request requestGraphDataWithGraphPath:@"me/friends/" parameters:@{ @"fields": @"picture{url},gender,bio,quotes,cover,birthday,name,hometown"} completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                 
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            NSArray* dataArray = [result valueForKey:@"data"];
            
            for(NSDictionary* fr in dataArray) {
                FBUserInfo* friendInfo = [[FBUserInfo alloc] initWithDictionary:fr];
                [_friendArray addObject:friendInfo];
            }
            
            _nextFriendsURLRequest = [NSURL URLWithString:[[result valueForKey:@"paging"] valueForKey:@"next"]];
            
            // Update UI
            dispatch_async(dispatch_get_main_queue(), ^{
                _friendSegueButton.hidden = NO;
            });
            
        }];
    }
}

- (void)requestUserInfoFacebook {
    if ([FBAPIUtility isUserLoginFacebook]) {
        FBAPIUtility* request = [[FBAPIUtility alloc] initWithSessionManager:[FBSessionManager sharedDefaultSessionManager]];
        
        [request requestGraphDataWithGraphPath:@"me/" parameters:@{ @"fields": @"picture{url},gender,bio,quotes,cover,birthday,name,hometown"} completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if(!error) {
                NSDictionary* result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
                
                [[FBUserManager shareManager] setCurrentUser:[[FBUserInfo alloc] initWithDictionary:result]];
                FBUserInfo* currentUser = [[FBUserManager shareManager] getCurrentUser];
                
                if(currentUser.pictureURL) {
                    
                    FBImageDownloader* downloader = [[FBImageDownloader alloc] initWithSessionManager:[FBSessionManager sharedDefaultSessionManager]];
                    NSURL* urlPicture = [NSURL URLWithString:currentUser.pictureURL];
                    [downloader downloadImageWithURL:urlPicture identifier:currentUser.userID completionHandler:^(id imageIdentifier, UIImage *image, NSURLResponse *response, NSError *error) {
                        if(image && !error)
                            // Update UI
                            dispatch_async(dispatch_get_main_queue(), ^{
                                _pictureImageView.image = image;
                            });
                    }];
                }
            }
        }];
    }
}

#pragma mark - FBSDKLoginDelegate
- (IBAction)authenticateAction:(id)sender {
    if(![FBAPIUtility isUserLoginFacebook]) {
        [FBAPIUtility logInFacebookWithReadPermissions:_permissionFacebookArray fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if(!error) {
                [self requestLoadFriendFacebook];
                [self requestUserInfoFacebook];
                [_loginFacebookButton setTitle:@"Log-Out" forState:UIControlStateNormal];
            } else {
                NSLog(@"Error: %@",error.localizedDescription);
            }
        }];
    } else {
        [FBAPIUtility logOutFacebook];
        _friendSegueButton.hidden = true;
        [_loginFacebookButton setTitle:@"Login with Facebook" forState:UIControlStateNormal];
    }
}

#pragma mark - Touch Events
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // Get touch position
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    static UIPushBehavior *pushBehavior = nil;
    
    // Remove and add new UIPushBehavior follow touch position
    [_animator removeBehavior:pushBehavior];
    pushBehavior = [[UIPushBehavior alloc] initWithItems:@[_pictureImageView] mode:UIPushBehaviorModeInstantaneous];
    pushBehavior.magnitude = 1.5;
    
    float xDelta = -0.5;
    if(touchLocation.x > self.view.frame.size.width/2) {
        xDelta = 0.5;
    }
    
    pushBehavior.pushDirection = CGVectorMake(xDelta,5);
    [_animator addBehavior:pushBehavior];

}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    CGFloat yPoint = _friendSegueButton.center.y;
    CGPoint paddleCenter = CGPointMake(touchLocation.x, yPoint);
    
    _friendSegueButton.center = paddleCenter;
    [self.animator updateItemUsingCurrentState:_friendSegueButton];
}

@end
