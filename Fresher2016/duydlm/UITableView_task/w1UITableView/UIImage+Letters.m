//
//  UIImage+Letters.m
//  w1UITableView
//
//  Created by manhduydl on 6/6/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "UIImage+Letters.h"
#define ARC4RANDOM_MAX      0x100000000

@implementation UIImage (Letters)

+ (UIImage *)createImageWithLetters:(NSString *)displayLetters {
    CGFloat fontSize = 39 * 0.5f;
    srand48(arc4random());
    
    NSDictionary *textAttributes = @{
                                     NSFontAttributeName: [UIFont systemFontOfSize:fontSize weight:0.4],
                                     NSForegroundColorAttributeName:[UIColor whiteColor]
                                     };
    //Random background color
    UIColor *backgroundColor = [UIColor colorWithRed:(double)arc4random() / ARC4RANDOM_MAX green:(double)arc4random() / ARC4RANDOM_MAX blue:(double)arc4random() / ARC4RANDOM_MAX alpha:1.0f];
    
    CGFloat scale = [UIScreen mainScreen].scale;
    
    CGSize size = CGSizeMake(39, 39);//imageView.bounds.size;
    CGRect rect = CGRectMake(0, 0, 39, 39);
    
    //Begin create image
    UIGraphicsBeginImageContextWithOptions(size, NO, scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //Fill backgrond of context
    CGContextSetFillColorWithColor(context, backgroundColor.CGColor);
    //Draw circle
    CGContextFillEllipseInRect(context, rect);
    
    //Draw text in the context
    CGSize textSize = [displayLetters sizeWithAttributes:textAttributes];
    
    [displayLetters drawInRect:CGRectMake(20 - textSize.width/2,
                                         20 - textSize.height/2,
                                         textSize.width,
                                         textSize.height)
               withAttributes:textAttributes];
    
    //Get image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    //End create iamge
    UIGraphicsEndImageContext();
    
    return image;

}

@end
