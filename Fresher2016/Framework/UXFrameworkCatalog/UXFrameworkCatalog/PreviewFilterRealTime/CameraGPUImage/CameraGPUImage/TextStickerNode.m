//
//  TextStickerNode.m
//  CameraGPUImage
//
//  Created by VanDao on 6/21/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "TextStickerNode.h"

@implementation TextStickerNode


- (id)initWithFrame:(CGRect)frame withContent:(NSString*)content {
    self = [super initWithFrame:frame];
    self.content = content;
    if (self){
        self.content = [[NSString alloc] init];
        self.color = [UIColor whiteColor];
    }
    
    
    return self;
}

//- (id)initWithFrame:(CGRect)frame{
//    self = [super initWithFrame:frame];
//    
//    
//}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [super drawRect:rect];
}

@end
