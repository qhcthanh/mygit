//
//  EmojiModel.swift
//  EmojiModel
//
//  Created by Quach Ha Chan Thanh on 8/13/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import Foundation
import UIKit

// Simple Model has only titleContent
public protocol SimpleModelProtocol: NSObjectProtocol {
    var titleContent: String {get}
    var details: [String]? {get}
}

// Image + Title Model
public protocol ImageModelProtocol: SimpleModelProtocol {
    var imageContent: UIImage {get}
}


public class BLEmojiInfo: NSObject, SimpleModelProtocol {
    
    public var keywords: [String]!
    public var char: String!
    public var name: String!
    
    public var emojiSkins: [String]?
    
    init(name: String, keywords: [String], char: String) {
        super.init()
        
        self.name = name
        self.keywords = keywords
        self.char = char
        
        if !self.keywords.contains("face") {
            if char.canHaveSkinToneModifier {
                // Ensure the unmodified Emoji is being used
                let unmodified = char.emojiUnmodified

                for modifier in char.emojiSkinToneModifiers {
                    let newEmoji = unmodified + modifier
                    
                    // add emoji to list
                    if let _ = self.emojiSkins {
                        self.emojiSkins!.append(newEmoji)
                    } else {
                        self.emojiSkins = [String]([newEmoji])
                    }
                }
            }
        }

    }
    
    init(jsonData: NSDictionary) {
        
    }
    
    public var titleContent: String {
        return char
    }
    
    public var details: [String]? {
        return emojiSkins
    }
    
}








