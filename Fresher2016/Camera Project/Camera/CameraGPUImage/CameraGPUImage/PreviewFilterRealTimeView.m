//
//  PreiviewFilterRealTimeView.m
//  CameraGPUImage
//
//  Created by qhcthanh on 6/30/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "PreviewFilterRealTimeView.h"
#import "InstaFilters.h"
#import "PreviewFilterGPUImageView.h"

#define NAME_LABEL_TAG 536

@interface PreviewFilterRealTimeView()

@property (weak, nonatomic) GPUImageStillCamera *videoCamera;
@property (weak, nonatomic) NSMutableArray<Class> *filters;
@property NSMutableArray* cameraViewFilter;
@property NSInteger curFilter;
@property CGFloat widthFilterItem;
@property CGFloat heightFilterItem;
@property BOOL isFirstTimeRun;

@end

@implementation PreviewFilterRealTimeView

-(id)initWithCamera:(GPUImageStillCamera *)videoCamera frame:(CGRect)frame filters:(id)filters {
    
    self = [super initWithFrame:frame];
    // Setup property
     _videoCamera = videoCamera;
    _isShowMenu = false;
    _filters = filters;
    _canTouch = true;
    _isFirstTimeRun = false;
    _cameraViewFilter = [[NSMutableArray alloc] initWithArray: @[@"",@"",@"",@"",@"",@"",@"",@"",@""]];
   [self setBackgroundColor:[UIColor blackColor]];
    
    // Size for each PreviewFilterGPUImageView
    _widthFilterItem = self.frame.size.width * 1/3 - 5;
    _heightFilterItem = self.frame.size.height * 1/3 - 5;

    // Create 9 PreviewFilterGPUImageView with index filter
    //  1 3 5
    //  7 0 2
    //  4 6 8
    
    // Start top space Y = 5, leading = 5
    CGFloat posY = 5;
    int startPos = 1;
    for(int i = 0; i<3 ;i++) {
        CGFloat posX = 5;
        for(int j = 0;j<3;j++) {
            // Create each GPUImageVIew display realtime for each filter
            PreviewFilterGPUImageView* imageV = [[PreviewFilterGPUImageView alloc]
                                    initWithFrame:CGRectMake(posX, posY, _widthFilterItem, _heightFilterItem)];
            imageV.tag = startPos;
            imageV.rPosition = imageV.frame.origin;
            
            // create name label of PreviewFilterGPUImageView
            UILabel* nameFilterLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, imageV.frame.size.height - 16, imageV.frame.size.width - 10 , 16)];
            [nameFilterLabel setFont:[UIFont fontWithName:@"Arial" size:10]];
            nameFilterLabel.textColor = [UIColor whiteColor];
            nameFilterLabel.tag = NAME_LABEL_TAG;
            nameFilterLabel.textAlignment = NSTextAlignmentRight;
            [imageV addSubview:nameFilterLabel];
            
            // Add Gesture when user tap in filterView to select filterView
            UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectFilterView:)];
            [imageV addGestureRecognizer:tapGesture];
            
            // Just show (normal filter - index 0)
            if (startPos == 0) {
                [self addSubview:imageV];
                nameFilterLabel.hidden = true;
            }
            
            // Get filter at tag index and add ImageV + Camera and add target
            id filter = [[[_filters objectAtIndex:startPos] alloc] init];
            [filter forceProcessingAtSize:imageV.sizeInPixels];
            [filter addTarget:imageV];
            [_videoCamera addTarget:filter];
            
            // Add filter and imageV to array at imageV.tag position
            _cameraViewFilter[startPos] = imageV;
            imageV.filter = filter;
            nameFilterLabel.text = [IFImageFilter getNameFilter:((IFImageFilter*)filter).name];
            
            // New posX + width Item + leading
            posX += _widthFilterItem + 5;
            startPos += 2;
            if (startPos > 8) {
                startPos = 0;
            }
        }
        // New posY + height + top
        posY += _heightFilterItem + 5;
    }
    
    //Set current select filter default
    _curFilter = 0;
    _isFirstTimeRun = true;
    [self showFilterSelect];
    
    return self;
}

-(void)hideMenuFilter {
    if (_isShowMenu && _canTouch) {
        GPUImageView* filterView = [_cameraViewFilter objectAtIndex:_curFilter];
        id tapGesture = [filterView gestureRecognizers].firstObject;
        if ([tapGesture isKindOfClass:[UITapGestureRecognizer class]]) {
            [self didSelectFilterView:tapGesture];
        }
    }
}

-(void)didSelectFilterView:(UITapGestureRecognizer*)filterViewGesture {
    if (_canTouch) {
        _canTouch = false;
        _isFirstTimeRun = false;
        GPUImageView* filterView = (GPUImageView*)filterViewGesture.view;
        _curFilter = filterView.tag;
        [filterView removeGestureRecognizer:filterViewGesture];
        [self showFilterSelect];
    }
}

-(void)showFilterSelect {
   
    // Get filterview current and bring to front display
    PreviewFilterGPUImageView* filterView = (PreviewFilterGPUImageView*)[_cameraViewFilter objectAtIndex:_curFilter];
    filterView.fillMode = kGPUImageFillModePreserveAspectRatioAndFill;
    [self bringSubviewToFront:filterView];
    UIView* nameView = [filterView viewWithTag:NAME_LABEL_TAG];
    nameView.hidden = true;
    
    // Get filter of view
    id filter = filterView.filter;
    
    // Rest filter and forceProcessingFull size
    [_videoCamera removeTarget:filter];
    [filter removeTarget:filterView];
    filter = [[[_filters objectAtIndex:filterView.tag] alloc] init];
    //[filter forceProcessingAtSize:CGSizeMake(self.frame.size.width, self.frame.size.height + 50 + 90)];
    [filter addTarget:filterView];
    filterView.filter = filter;
    [_videoCamera addTarget:filter];
    
    // animation for zoom out full screnn
    [UIView animateWithDuration:0.3 animations:^{
        CGRect filterFrame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        if (!_isFirstTimeRun) {
            for(int i = 0 ;i< 9;i++) {
                if (i != _curFilter) {
                    // Animation move otherFilterView with Vector(otherFilterView,filterView)
                    //           scale otherFilterView = superview
                    [UIView animateWithDuration:0.3 animations:^{
                        PreviewFilterGPUImageView* otherfilterView = (PreviewFilterGPUImageView*)[_cameraViewFilter objectAtIndex:i];
                        CGRect frame = filterFrame;
                        CGFloat x = (otherfilterView.rPosition.x - filterView.rPosition.x)/_widthFilterItem;
                        CGFloat y = (otherfilterView.rPosition.y - filterView.rPosition.y)/_heightFilterItem;
                        frame.origin.x += x * filterFrame.size.width;
                        frame.origin.y += y * filterFrame.size.height;
                        otherfilterView.frame = frame;
                    }];
                }
                
            }
        }
        // Animation move filterView to top left screen and  and scale to full screen
        // -50: topBarView
        // 90: bottomBarView
        filterView.frame = CGRectMake(0, -50, self.frame.size.width, self.frame.size.height + 50 + 90);
    } completion:^(BOOL success) {
        // set can touch when finish animation
        _canTouch = true;
        _isFirstTimeRun = false;
        _isShowMenu = false;
        
        for(int i = 0 ;i< 9;i++) {
            if (i != _curFilter) {
                PreviewFilterGPUImageView* filterView = (PreviewFilterGPUImageView*)[_cameraViewFilter objectAtIndex:i];
                [filterView removeFromSuperview];
            }
        }
    }];
    
    self.currentFilterView = filterView;
    // Call delegate to update layout in camera view controller
    if (_delegate && [_delegate respondsToSelector:@selector(didSelectFilter:)]) {
        [_delegate didSelectFilter: [_filters objectAtIndex:filterView.tag]];
    }
}

-(void)showFilterMenu {

    if (!_isShowMenu) {
        for(int i = 0 ;i< 9;i++) {
            if (i != _curFilter) {
                PreviewFilterGPUImageView* filterView = (PreviewFilterGPUImageView*)[_cameraViewFilter objectAtIndex:i];
                [self addSubview:filterView];
            }
        }
        // Get label in filterView and show
        PreviewFilterGPUImageView* filterView = (PreviewFilterGPUImageView*)[_cameraViewFilter objectAtIndex:_curFilter];
        [self bringSubviewToFront:filterView];
        UIView* nameView = [filterView viewWithTag:NAME_LABEL_TAG];
        nameView.hidden = false;
        
        // Get filter and resest
        id filter = filterView.filter;
        [_videoCamera removeTarget:filter];
        [filter removeTarget:filterView];
        filter = [[[_filters objectAtIndex:filterView.tag] alloc] init];
        [filter forceProcessingAtSize:CGSizeMake(_widthFilterItem, _heightFilterItem)];
        [filter addTarget:filterView];
        filterView.filter = filter;
        [_videoCamera addTarget:filter];
        
        // Animation for 9 view zoom and scale to default
        [UIView animateWithDuration:0.2 animations:^{
            for(PreviewFilterGPUImageView* tView in _cameraViewFilter) {
                tView.frame = CGRectMake(tView.rPosition.x, tView.rPosition.y, _widthFilterItem, _heightFilterItem);
            }
        } completion:^(BOOL success) {
            _isShowMenu = true;
            UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectFilterView:)];
            [filterView addGestureRecognizer:tapGesture];
        }];
    }
}

@end
