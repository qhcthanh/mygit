/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "NetworkPhotoViewController.h"

@interface NetworkPhotoViewController ()
#ifdef DEBUG
@property (nonatomic, retain) UXOverviewMemoryCachePageView* highQualityPage;
@property (nonatomic, retain) UXOverviewMemoryCachePageView* thumbnailPage;
#endif
@end

@implementation NetworkPhotoViewController




- (void)shutdown_NetworkPhotoAlbumViewController {
    [_queue cancelAllOperations];
    
#ifdef DEBUG
    [[UXOverview view] removePageView:self.highQualityPage];
    [[UXOverview view] removePageView:self.thumbnailPage];
#endif
}

- (void)dealloc {
    [self shutdown_NetworkPhotoAlbumViewController];
}

- (NSString *)cacheKeyForPhotoIndex:(NSInteger)photoIndex {
    return [NSString stringWithFormat:@"%ld", (long)photoIndex];
}

- (NSInteger)identifierWithPhotoSize:(UXPhotoScrollViewPhotoSize)photoSize
                          photoIndex:(NSInteger)photoIndex {
    BOOL isThumbnail = (UXPhotoScrollViewPhotoSizeThumbnail == photoSize);
    NSInteger identifier = isThumbnail ? -(photoIndex + 1) : photoIndex;
    return identifier;
}

- (id)identifierKeyFromIdentifier:(NSInteger)identifier {
    return [NSNumber numberWithInteger:identifier];
}

- (void)requestImageFromSource:(NSString *)source
                     photoSize:(UXPhotoScrollViewPhotoSize)photoSize
                    photoIndex:(NSInteger)photoIndex {
    //BOOL isThumbnail = (UXPhotoScrollViewPhotoSizeThumbnail == photoSize);
    NSInteger identifier = [self identifierWithPhotoSize:photoSize photoIndex:photoIndex];
    id identifierKey = [self identifierKeyFromIdentifier:identifier];
    
    // Avoid duplicating requests.
    if ([_activeRequests containsObject:identifierKey]) {
        return;
    }
}

#pragma mark - UIViewController


- (void)loadView {
    [super loadView];
    
    _activeRequests = [[NSMutableSet alloc] init];
    
    _highQualityImageCache = [[UXImageMemoryCache alloc] init];
    _thumbnailImageCache = [[UXImageMemoryCache alloc] init];
    
    [_highQualityImageCache setMaxNumberOfPixels:1024L*1024L*10L];
    [_thumbnailImageCache setMaxNumberOfPixelsUnderStress:1024L*1024L*3L];
    
    _queue = [[NSOperationQueue alloc] init];
    [_queue setMaxConcurrentOperationCount:5];
    
    // Set the default loading image.
    self.photoAlbumView.loadingImage = [UIImage imageWithContentsOfFile:
                                        UXPathForBundleResource(nil, @"UXFramworkPhotos.bundle/gfx/default.png")];
    
#ifdef DEBUG
    self.highQualityPage = [UXOverviewMemoryCachePageView pageWithCache:self.highQualityImageCache];
    [[UXOverview view] addPageView:self.highQualityPage];
    self.thumbnailPage = [UXOverviewMemoryCachePageView pageWithCache:self.thumbnailImageCache];
    [[UXOverview view] addPageView:self.thumbnailPage];
#endif
}

- (void)viewDidUnload {
    [self shutdown_NetworkPhotoAlbumViewController];
    
    [super viewDidUnload];
}

@end
