//
//  KeyboardViewController.swift
//  BanLaKeyBoard
//
//  Created by BaoNQ on 8/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

class KeyboardViewController: UIInputViewController {

    var keyboard: BLKeyboard?
    var firstRun: Bool = true
    var onceToken: dispatch_once_t = 0
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        // Add custom view sizing constraints here
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if (self.firstRun == false) {

            UIView.animateWithDuration(0.0, animations: {
                self.keyboard?.updateState(self.view.frame)

            })
        }
        
        dispatch_once(&onceToken, {
            self.firstRun = false;
            weak var weakSelf = self

            self.keyboard = BLKeyboard(frame: self.view.frame, inputViewController: weakSelf!)
            self.view.addSubview(self.keyboard!)
        })
    }
//    var subview: UIView?

//    override func loadView() {
//        super.loadView()
//        
//        weak var weakSelf = self
//        
//        self.keyboard = BLKeyboard(frame: self.view.frame, inputViewController: weakSelf!)
//        self.subview?.frame = CGRectMake(0, 0, self.view.bounds.width, 35)
//        self.view.addSubview(self.keyboard!)
//        
//        self.subview = UIView();
//        self.subview?.backgroundColor = UIColor.blueColor()
//        self.view.insertSubview(self.subview!, belowSubview: self.keyboard!)
//    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Perform custom UI setup here
        self.view.userInteractionEnabled = true
    }
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var kludge: UIView?
    func setupKludge() {
        if self.kludge == nil {
            let kludge = UIView()
            self.view.addSubview(kludge)
            kludge.translatesAutoresizingMaskIntoConstraints = false
            kludge.hidden = true
            
            let a = NSLayoutConstraint(item: kludge, attribute: NSLayoutAttribute.Left, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Left, multiplier: 1, constant: 0)
            let b = NSLayoutConstraint(item: kludge, attribute: NSLayoutAttribute.Right, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Left, multiplier: 1, constant: 0)
            let c = NSLayoutConstraint(item: kludge, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0)
            let d = NSLayoutConstraint(item: kludge, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0)
            self.view.addConstraints([a, b, c, d])
            
            self.kludge = kludge
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated
    }

    override func textWillChange(textInput: UITextInput?) {
        // The app is about to change the document's contents. Perform any preparation here.
    }

    override func textDidChange(textInput: UITextInput?) {
        // The app has just changed the document's contents, the document context has been updated.
    
        var textColor: UIColor
        let proxy = self.textDocumentProxy
        if proxy.keyboardAppearance == UIKeyboardAppearance.Dark {
            textColor = UIColor.whiteColor()
        } else {
            textColor = UIColor.blackColor()
        }
       // self.nextKeyboardButton.setTitleColor(textColor, forState: .Normal)
    }

}
