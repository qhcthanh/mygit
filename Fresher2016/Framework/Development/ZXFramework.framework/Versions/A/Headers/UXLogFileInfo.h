/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, LogFileInfoType) {
    kLogFileInfoTypeStatic = 0,
    kLogFileInfoTypeAction
};

/**
 UXLogFileInfo is a simple class that provides access to various file attributes.
 It provides good performance as it only fetches the information if requested,
 and it caches the information to prevent duplicate fetches.
 
 It was designed to provide quick snapshots of the current state of log files,
 and to help sort log files in an array.
 
 This class does not monitor the files, or update it's cached attribute values if the file changes on disk.
 This is not what the class was designed for.
 
 If you absolutely must get updated values,
 you can invoke the reset method which will clear the cache. 
 */
@interface UXLogFileInfo : NSObject 

@property (strong, nonatomic, readonly) NSString *filePath;
@property (strong, nonatomic, readonly) NSString *fileName;

@property (strong, nonatomic, readonly) NSDictionary *fileAttributes;

@property (strong, nonatomic, readonly) NSDate *creationDate;
@property (strong, nonatomic, readonly) NSDate *modificationDate;

@property (nonatomic, readonly) unsigned long long fileSize;

@property (nonatomic, readonly) NSTimeInterval age;

@property (nonatomic, readwrite) BOOL isArchived;

@property (nonatomic, assign) LogFileInfoType logFileInfoType;

+ (id)logFileWithPath:(NSString *)filePath;

- (id)initWithFilePath:(NSString *)filePath;

- (void)reset;
- (void)renameFile:(NSString *)newFileName;

#if TARGET_IPHONE_SIMULATOR

// So here's the situation.
// Extended attributes are perfect for what we're trying to do here (marking files as archived).
// This is exactly what extended attributes were designed for.
// 
// But Apple screws us over on the simulator.
// Everytime you build-and-go, they copy the application into a new folder on the hard drive,
// and as part of the process they strip extended attributes from our log files.
// Normally, a copy of a file preserves extended attributes.
// So obviously Apple has gone to great lengths to piss us off.
// 
// Thus we use a slightly different tactic for marking log files as archived in the simulator.
// That way it "just works" and there's no confusion when testing.
// 
// The difference in method names is indicative of the difference in functionality.
// On the simulator we add an attribute by appending a filename extension.
// 
// For example:
// log-ABC123.txt -> log-ABC123.archived.txt

- (BOOL)hasExtensionAttributeWithName:(NSString *)attrName;

- (void)addExtensionAttributeWithName:(NSString *)attrName;
- (void)removeExtensionAttributeWithName:(NSString *)attrName;

#else

// Normal use of extended attributes used everywhere else,
// such as on Macs and on iPhone devices.

- (BOOL)hasExtendedAttributeWithName:(NSString *)attrName;

- (void)addExtendedAttributeWithName:(NSString *)attrName;
- (void)removeExtendedAttributeWithName:(NSString *)attrName;

#endif

- (NSComparisonResult)reverseCompareByCreationDate:(UXLogFileInfo *)another;
- (NSComparisonResult)reverseCompareByModificationDate:(UXLogFileInfo *)another;

@end

@interface UXLogFileInfo (Compressor)

@property (nonatomic, readonly) BOOL isCompressed;

- (NSString *)tempFilePathByAppendingPathExtension:(NSString *)newExt;
- (NSString *)fileNameByAppendingPathExtension:(NSString *)newExt;

@end
