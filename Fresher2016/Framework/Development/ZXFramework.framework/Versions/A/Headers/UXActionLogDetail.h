/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import "UXLogMessage.h"

@interface UXActionLogDetail : UXLogMessage
{
    int16_t _execResult;
}

@property int16_t type;
@property (copy) NSString* actionType;
@property (copy) NSString* srcActOne;
@property (copy) NSString* srcActTwo;
@property uint64_t startExecTime;
@property uint64_t finishExecTime;
@property (getter = execResult, setter = setExecResult:)int16_t execResult;

@property (readonly) int16_t totalCount;
@property (readonly) int16_t failedCount;
@property (readonly) uint64_t totalExecTime;
@property (readonly) uint64_t minExecTime;
@property (readonly) uint64_t maxExecTime;
@property (readonly) NSString* param;

- (void)addDetail:(UXActionLogDetail*) newLogAct;

- (id)initWithType:(int16_t) type andActionType:(NSString*) actionType andSrcActOne:(NSString*) srcActOne andSrcActTwo:(NSString*) srcActTwo;

- (id)initWithString:(NSString*)line;
- (id)initContainer;
- (void)print;

@end
