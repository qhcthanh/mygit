//
//  UserInfoViewController.h
//  FacebookContact
//
//  Created by qhcthanh on 7/15/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBUserInfo.h"

@interface UserInfoViewController : UIViewController

/**
 *  A FBUserInfo is display info in this Controller
 */
@property (weak) FBUserInfo* userInfo;

@end
