//
//  BLPhoneMetrics.h
//  BanLaKey
//
//  Created by BaoNQ on 8/16/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>

typedef struct {
    
    // 1st row
    CGRect QButton;
    CGRect WButton;
    CGRect EButton;
    CGRect RButton;
    CGRect TButton;
    CGRect YButton;
    CGRect UButton;
    CGRect IButton;
    CGRect OButton;
    CGRect PButton;

    // 2nd row
    CGRect AButton;
    CGRect SButton;
    CGRect DButton;
    CGRect FButton;
    CGRect GButton;
    CGRect HButton;
    CGRect JButton;
    CGRect KButton;
    CGRect LButton;

    // 3rd row
    CGRect ZButton;
    CGRect XButton;
    CGRect CButton;
    CGRect VButton;
    CGRect BButton;
    CGRect NButton;
    CGRect MButton;
    
    
    CGRect leftShiftButton;
    CGRect deleteButton;

    CGRect globeButton;
    CGRect spaceButton;
    CGRect returnButton;
    CGRect keyModeButton;
    CGRect settingButton;
    
    CGRect popupFrame;
    
    CGRect keyboardFrame;
        
} PhoneKeyboardMetrics;

PhoneKeyboardMetrics generatePhoneKeyboardMetrics(CGFloat keyboardWidth, CGFloat keyboardHeight);
