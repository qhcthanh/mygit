//
//  temp.m
//  Project2
//
//  Created by BaoNQ on 5/24/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
//----------------------------
// build array of contacts
NSError *fetchError;
CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:@[CNContactIdentifierKey, CNContactImageDataKey, CNContactThumbnailImageDataKey,[CNContactFormatter descriptorForRequiredKeysForStyle:CNContactFormatterStyleFullName]]];

//This method waits until the enumeration is finished.
BOOL success = [store enumerateContactsWithFetchRequest:request error:&fetchError usingBlock:^(CNContact *contact, BOOL *stop) {
    //Step 1: add Contact to ContactGroup
    NSString *upperCaseContactName;
    
    CNContactFormatter *formatter = [[CNContactFormatter alloc] init];
    upperCaseContactName = [[formatter stringFromContact:contact] uppercaseString];
    
    unichar firstChar = [upperCaseContactName characterAtIndex:0];
    NSString *firstCharInString;
    
    if (firstChar >= 'A' && firstChar <= 'Z') {
        firstCharInString = [upperCaseContactName substringToIndex:1];
        
    }
    else {
        firstCharInString = @"#";
    }
    //Get group for this contact
    NSMutableArray *groupForThisContact = [self.contactGroups objectForKey:firstCharInString];
    
    CTContactDetail *contactDetail = [[CTContactDetail alloc] init];
    contactDetail.fullName = [formatter stringFromContact:contact];
    
    //If this group does not exist
    if (groupForThisContact == nil) {
        //Create new group and add contact to it.
        groupForThisContact = [[NSMutableArray alloc] init];
        [groupForThisContact addObject:contactDetail];
        
        //Then add this group to Dictionary with key
        [self.contactGroups setObject:groupForThisContact forKey:firstCharInString];
    }
    //This group already exist.
    else {
        //Just add contact to this group.
        [groupForThisContact addObject:contactDetail];
    }
}];
if (!success) {
    NSLog(@"error = %@", fetchError);
}
*/




/*
 //Get profile image
 __block UIImage *profileImage = [self.profileImageCaching objectForKey:[NSNumber numberWithInteger:indexOfContact]];
 //If we created an image for this contact, just load it up from cache by ID.
 if (profileImage != nil) {
 cell.profilePicture.image = profileImage;
 }
 else {
 cell.profilePicture.backgroundColor = curContact.profileColor;
 dispatch_queue_t imageProcessingQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
 
 dispatch_async(imageProcessingQueue, ^{
 if (curContact.imageData != nil) {
 //Generate image from data
 CTUIImage *imageFactory = [[CTUIImage alloc] init];
 profileImage = [imageFactory createWithData:curContact.imageData];
 //Add to cache
 [self.profileImageCaching setObject:profileImage forKey:[NSNumber numberWithInteger:indexOfContact]];
 }
 else {
 //Get 2 first characters from firstname and lastname
 NSString *text;
 if ([curContact.firstName length] > 0 && [curContact.lastName length] > 0)
 text = [NSString stringWithFormat:@"%@%@", [curContact.firstName substringToIndex:1], [curContact.lastName substringToIndex:1]];
 else {
 if ([curContact.firstName length] == 0)
 text = [NSString stringWithFormat:@"%@", [curContact.lastName substringToIndex:1]];
 else
 text = [NSString stringWithFormat:@"%@", [curContact.firstName substringToIndex:1]];
 }
 //Get image from cache
 profileImage = [self.characterImageCaching objectForKey:text];
 //If image does not exist
 if (profileImage == nil) {
 CTUIImage *imageFactory = [[CTUIImage alloc] init];
 //Generate image and add to cache
 profileImage = [imageFactory createWithText:text imageSize:cell.profilePicture.frame.size];
 [self.characterImageCaching setObject:profileImage forKey:text];
 }
 }
 dispatch_async(dispatch_get_main_queue(), ^{
 cell.profilePicture.image = profileImage;
 });
 });
 }
*/

/*
UIImage *profileImage = [self.profileImageCaching objectForKey:[checkedContacts objectAtIndex:[indexPath row]]];
//If we created an image for this contact, just load it up from cache by ID.
if (profileImage != nil) {
    cell.imageView.image = profileImage;
}
else {
    CTContactDetail *curContact = [self.contacts objectAtIndex:[[checkedContacts objectAtIndex:[indexPath row]] integerValue]];
    
    cell.imageView.backgroundColor = curContact.profileColor;
    
    //Get 2 first characters from firstname and lastname
    NSString *text;
    if ([curContact.firstName length] > 0 && [curContact.lastName length] > 0)
        text = [NSString stringWithFormat:@"%@%@", [curContact.firstName substringToIndex:1], [curContact.lastName substringToIndex:1]];
        else {
            if ([curContact.firstName length] == 0)
                text = [NSString stringWithFormat:@"%@", [curContact.lastName substringToIndex:1]];
            else
                text = [NSString stringWithFormat:@"%@", [curContact.firstName substringToIndex:1]];
        }
    //Get image from cache
    profileImage = [self.characterImageCaching objectForKey:text];
    if (profileImage != nil) {
        cell.imageView.image = profileImage;
    }
}
*/

/*
 self.profilePicture.layer.cornerRadius = self.profilePicture.frame.size.width/2;
 self.profilePicture.clipsToBounds = YES;
 self.profilePicture.layer.borderWidth = 2.0f;
 self.profilePicture.layer.borderColor = [UIColor whiteColor].CGColor;
*/