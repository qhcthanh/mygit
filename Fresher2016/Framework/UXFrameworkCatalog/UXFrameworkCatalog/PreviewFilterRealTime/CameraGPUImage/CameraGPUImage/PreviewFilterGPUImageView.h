//
//  PreviewFilterView.h
//  CameraGPUImage
//
//  Created by qhcthanh on 7/1/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "GPUImage.h"

@interface PreviewFilterGPUImageView : GPUImageView

@property CGPoint rPosition;
@property (weak,nonatomic) id filter;

@end
