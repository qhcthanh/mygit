//
//  String+Extention.swift
//  BanLaKey
//
//  Created by manhduydl on 8/31/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import Foundation

extension String {
    
    // Return String length
    var length: Int {
        return self.characters.count
    }
    
    // Substring to special index
    func substringToIndex(to: Int) -> String {
        return self.substringToIndex(self.startIndex.advancedBy(to))
    }
    
    /**
     Replace one character in String with another character at specify index
     
     - parameter character: charactor will replace
     - parameter atIndex:   index to replace
     
     - returns: return String after replace
     */
    func replace(withCharacter character : String, atIndex : Int) -> String {
        let s1 = self.substringToIndex(self.startIndex.advancedBy(atIndex))
        let s2 = self.substringFromIndex(self.startIndex.advancedBy(atIndex+1))
        
        //let te = s1 + character + s2
        return s1 + character + s2
    }
    
    /**
     Separate sentence to word array
     
     - returns: return word array
     */
    func words() -> [String] {
        let r = self.startIndex..<self.endIndex
        var words = [String]()
        
        self.enumerateSubstringsInRange(r, options: NSStringEnumerationOptions.ByWords) { (substring, _, _, _) -> () in
            words.append(substring!)
        }
        
        return words
    }
    
    func arrayCharacters() -> [String] {
        
        var array: [String] = []
        
        for character in self.characters {
            array.append(String(character))
        }
        
        return array
    }
    
    /**
     Get last word of sentence
     
     - returns: return last word of sentence
     */
    func lastWord() -> String? {
        
        let subString = self.substringFromIndex(self.endIndex.advancedBy(-10, limit: self.startIndex))
        let words = subString.words()
        
        if words.count > 0 {
            let index = words.endIndex - 1
            let lastWord = words[index]
            
            return lastWord
        }
        return nil
    }
    
    /**
     Get phrase with 2 word from String
     
     - returns: Return phrase array
     */
    func phrases() -> [String] {
        let words = self.words()
        var phrases = [String]()
        
        if words.count < 2 {
            return phrases
        }
        
        for i in 0..<words.count-1 {
            phrases.append(words[i] + " " + words[i+1])
        }
        
        return phrases
    }
    
    /**
     Get last phrase with 2 word from string
     
     - returns: Return phrase
     */
    func lastPhrase() -> String? {
        var words = self.words()
        
        if words.count > 1 {
            return words[words.count - 2] + " " + words[words.count - 1]
        }
        
        return nil
    }
    
    /**
     Get character at specify index
     
     - parameter index: index to get character
     
     - returns: return character like String
     */
    func characterAtIndex(index : Int) -> String {
        return self.substringWithRange(self.startIndex.advancedBy(index)..<self.startIndex.advancedBy(index+1))
    }
    
    /**
     Check charater as string is a unicode charater?
     
     - returns: Bool result
     */
    func isUnicodeCharacter() -> Bool {
        
        if self.length > 1 {
            return false
        }
        
        let s = self.unicodeScalars
        let uni = s[s.startIndex]
        
        let letter = NSCharacterSet.letterCharacterSet()
        return letter.longCharacterIsMember(uni.value)
    }
    
    /**
     Check if the word is a vietnamese word
     
     - parameter word: The word that want to check
     
     - returns: YES if word is a vietnamese word, otherwise, NO
     */
    func isVietnameseWord() -> Bool {
        
        var _hadBeginConsonant: Bool! = false
        var _hadVowel: Bool! = false
        var _hadEndConsonant: Bool! = false
        var beginConsonant = ""
        var vowel = ""
        var endConsonant = ""
        var leftOver = ""
        
        eachChar: for character in self.toASCII().characters {
            
            // Get begin consonant as long as possible
            if (!_hadBeginConsonant) {
                
                let temp = beginConsonant + String(character)
                
                for type in beginConsonantTypes {
                    if (beginConsonants[type]![list]!.contains(temp.lowercaseString)) {
                        beginConsonant = temp
                        continue eachChar
                    }
                }
                _hadBeginConsonant = true
            }
            
            // Get vowel as long as possible
            if (!_hadVowel) {
                let temp = vowel + String(character)
                
                for type in vowelTypes {
                    if (vowels[type]![list]!.contains(temp.lowercaseString)) {
                        vowel = temp
                        continue eachChar
                    }
                }
                _hadVowel = true
            }
            
            // Get end consonant as long as possible
            if (!_hadEndConsonant && leftOver == "") {
                let temp = endConsonant + String(character)
                
                for type in endConsonantTypes {
                    if (endConsonants[type]![list]!.contains(temp.lowercaseString)) {
                        endConsonant = temp
                        continue eachChar
                    }
                }
                
                _hadEndConsonant = true
            }
            leftOver = leftOver + String(character)
        }
        
        return (leftOver == "")
    }
    
    /**
     Check first word of string is capitalized
     
     - returns: Bool result
     */
    func isCapitalizedWord() -> Bool {
        
        let s = unicodeScalars
        let uni = s[s.startIndex]
        
        let letter = NSCharacterSet.uppercaseLetterCharacterSet()
        return letter.longCharacterIsMember(uni.value)
    }
    
    /**
     Check string is a vietnamese string
     
     - returns: Bool result
     */
    func isVietNamString() -> Bool {
        for c in characters {
            if let _ = mapVADictionary[String(c)] {
                return true
            }
        }
        return false
    }
    
    /**
     Convert string to ascii string
     
     - returns: String after convert
     */
    func toASCII() -> String {
        var result : String = ""
        
        for c in characters {
            if let asciiChar = mapVADictionary[String(c)] {
                result += asciiChar
            } else {
                result.append(c)
            }
        }
        return result
    }
}

let mapVADictionary = [
    "đ": "d",
    //a
    "á": "a",
    "à": "a",
    "ả": "a",
    "ã": "a",
    "ạ": "a",
    // ă
    "ă": "a",
    "ắ": "a",
    "ằ": "a",
    "ặ": "a",
    "ẳ": "a",
    "ẵ": "a",
    //â
    "â": "a",
    "ấ": "a",
    "ầ": "a",
    "ẩ": "a",
    "ẫ": "a",
    "ậ": "a",
    // ê
    "ê": "e",
    "ế": "e",
    "ề": "e",
    "ể": "e",
    "ễ": "e",
    "ệ": "e",
    // e
    "é": "e",
    "è": "e",
    "ẻ": "e",
    "ẽ": "e",
    "ẹ": "e",
    // u
    "ú": "u",
    "ù": "u",
    "ủ": "u",
    "ũ": "u",
    "ụ": "u",
    // ư
    "ư": "u",
    "ứ": "u",
    "ừ": "u",
    "ử": "u",
    "ữ": "u",
    "ự": "u",
    // i
    "í": "i",
    "ì": "i",
    "ỉ": "i",
    "ĩ": "i",
    "ị": "i",
    //o
    "ó": "o",
    "ò": "o",
    "ỏ": "o",
    "õ": "o",
    "ọ": "o",
    //ô
    "ô": "o",
    "ố": "o",
    "ồ": "o",
    "ổ": "o",
    "ỗ": "o",
    "ộ": "o",
    
    //ơ
    "ơ": "o",
    "ớ": "o",
    "ờ": "o",
    "ở": "o",
    "ỡ": "o",
    "ợ": "o",
    
    //y
    "ý": "y",
    "ỳ": "y",
    "ỷ": "y",
    "ỹ": "y",
    "ỵ": "y",
]

