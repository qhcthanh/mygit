//
//  ShowImageCaptureViewController.m
//  CameraWithAVFoundation
//
//  Created by qhcthanh on 6/8/16.
//  Copyright © 2016 Gabriel Alvarado. All rights reserved.
//

#import "ShowImageCaptureViewController.h"

@implementation ShowImageCaptureViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    UIImageView* imageView = [[UIImageView alloc] initWithFrame:(CGRect){0,50,self.view.frame.size.width,self.view.frame.size.height * 0.8}];
    [self.view addSubview:imageView];
    imageView.image = _captureImage;
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    UIButton* backButton = [[UIButton alloc] initWithFrame:CGRectMake( 20, self.view.frame.size.height - 100, 200, 40)];
    [backButton setTitle:@"Back" forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
}

-(void)backAction {
    [self dismissViewControllerAnimated:true completion:nil];
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:true];
}

@end
