/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

/**
 *  Initialize singleton
 *
 *  @param classname A template class
 *
 *  @return A singleton object
 */
#ifndef SYNTHESIZE_SINGLETON_FOR_CLASS
#define SYNTHESIZE_SINGLETON_FOR_CLASS(classname)           \
                                                            \
+ (classname *)shared##classname {                          \
    static dispatch_once_t pred;                            \
    static classname * shared##classname = nil;             \
    dispatch_once( &pred, ^{                                \
        shared##classname = [[self alloc] init];            \
    });                                                     \
    return shared##classname;                               \
}
#endif
