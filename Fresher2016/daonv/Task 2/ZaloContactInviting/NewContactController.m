//
//  NewContactViewController.m
//  ZaloContactInviting
//
//  Created by VanDao on 6/2/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import "NewContactController.h"
#import "LoadContactController.h"
#import "CTPhoneNumberCell.h"

@interface NewContactController (){
    UIImage *image;
    NSInteger numbersOfPhone;
}

@end

@implementation NewContactController
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    image = nil;
    numbersOfPhone = 0;
    self.navigationItem.rightBarButtonItem.enabled = NO;
    [self.firstNameTextField becomeFirstResponder];
    
    // Set default image to add photo view
    self.avatarImage.clipsToBounds = YES;
    self.avatarImage.layer.cornerRadius = 40;
    self.avatarImage.image = [UIImage imageNamed:@"icon-user.png"];
    
    // Add on touch event to add photo view
    UITapGestureRecognizer *imageSingleTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleImageSingleTap:)];
    [self.avatarImage setUserInteractionEnabled:YES];
    [self.avatarImage addGestureRecognizer:imageSingleTap];
    
    self.phoneNumberTableView.scrollEnabled = NO;
    [self.phoneNumberTableView setEditing:YES];
    self.phoneNumberTableView.allowsMultipleSelectionDuringEditing = NO;
    
    // Hide tableView
    self.heightOfTableView.constant = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    image = chosenImage;
    
    // Set image
    self.avatarImage.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {

    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - NavigationBar

- (IBAction)addContactBarItem:(id)sender {
    ContactDetail *newContact = nil;
    
    if (![self.firstNameTextField.text isEqual: @""] || ![self.familyNameTextField.text isEqual:@""] || numbersOfPhone > 0){
        newContact =[[ContactDetail alloc]init];
        newContact.givenName = self.firstNameTextField.text;
        newContact.familyName = self.familyNameTextField.text;
        
        NSLocale* currentLocale = [NSLocale currentLocale];
        NSString *identifier = [[[NSDate date] descriptionWithLocale:currentLocale] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        newContact.identifier = identifier;
        newContact.image = image;
        NSMutableArray<NSString *> *phoneNumbersArray = [[NSMutableArray alloc]init];
        
        // Get phone numbers
        static NSString *cellIdentifier = @"phoneCell";
        
        // Create a custom cell
        CTPhoneNumberCell *cell = [self.phoneNumberTableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        for (NSInteger i = 0; i < numbersOfPhone; i++){
            cell = [self.phoneNumberTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            if (![cell.phoneNumberTextField.text isEqualToString:@""])
                [phoneNumbersArray addObject:cell.phoneNumberTextField.text];
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    [delegate getNewContact:newContact];
}

#pragma mark - Handle tap

// The event handling photo method
- (void)handleImageSingleTap:(UITapGestureRecognizer *)recognizer {
    //  Location on tap
    //  CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    UIAlertAction* takePhotoAction = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action) {
                                                                if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                                                                    
                                                                    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                                                          message:@"Device has no camera"
                                                                                                                         delegate:nil
                                                                                                                cancelButtonTitle:@"OK"
                                                                                                                otherButtonTitles: nil];
                                                                    
                                                                    
                                                                    [myAlertView show];
                                                                }
                                                                else{
                                                                    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                                                    picker.delegate = self;
                                                                    picker.allowsEditing = YES;
                                                                    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                                    
                                                                    [self presentViewController:picker animated:YES completion:NULL];
                                                                }
                                                            }];
    
    UIAlertAction* selectPhotoAction = [UIAlertAction actionWithTitle:@"Select Photo" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                                                  picker.delegate = self;
                                                                  picker.allowsEditing = YES;
                                                                  picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                                  
                                                                  [self presentViewController:picker animated:YES completion:NULL];
                                                              }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                             [alert dismissViewControllerAnimated:YES completion:^{}];
                                                         }];
    
    [alert addAction:takePhotoAction];
    [alert addAction:selectPhotoAction];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - phoneNumberTableView
- (IBAction)newPhoneNumberButton:(id)sender {
    numbersOfPhone++;
    self.heightOfTableView.constant += 50;
    [self.phoneNumberTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:numbersOfPhone - 1 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return numbersOfPhone;
}

- (CTPhoneNumberCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"phoneCell";
    
    //Create a custom cell
    CTPhoneNumberCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil){
        cell = [[CTPhoneNumberCell alloc] init];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(CTPhoneNumberCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    [cell.phoneNumberTextField becomeFirstResponder];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    numbersOfPhone--;
    [self.phoneNumberTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    self.heightOfTableView.constant -= 50;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
/*
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 
 
 }*/

- (IBAction)firstNameEditing:(id)sender {
    if ([self.firstNameTextField.text length] == 0 && [self.familyNameTextField.text length] == 0)
        self.navigationItem.rightBarButtonItem.enabled = NO;
    else
        self.navigationItem.rightBarButtonItem.enabled = YES;
}

- (IBAction)familyNameEditing:(id)sender {
    if ([self.firstNameTextField.text length] == 0 && [self.familyNameTextField.text length] == 0)
        self.navigationItem.rightBarButtonItem.enabled = NO;
    else
        self.navigationItem.rightBarButtonItem.enabled = YES;
}
@end