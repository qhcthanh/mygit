//
//  BLTextField.swift
//  BanLaKey
//
//  Created by manhduydl on 9/8/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import Foundation

@objc public protocol BLTextFieldDelegate: UITextFieldDelegate {
    
    optional func textFieldDidChangeText(textField: BLTextField)
}

public class BLTextField: UITextField {
    
    // MARK: - Properties
    var cursorView : UIView!
    var isEditting : Bool = false
    public weak var blTextFieldDelegate: BLTextFieldDelegate?
    override public var text:String?  {
        didSet {
            let content = text! as NSString
            let rect = content.boundingRectWithSize(self.frame.size, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(16.0)], context: nil)
            cursorView.frame.origin.x = (rect.size.width + self.frame.size.width)/2
            
            if let _delegate = self.blTextFieldDelegate {
                _delegate.textFieldDidChangeText!(self)
            }
        }
    }
    
    // MARK: - Initialize
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        cursorView = UIView.init(frame: CGRectMake(0, 0, 1.5, 26))
        cursorView.backgroundColor = UIColor.whiteColor()
        cursorView.translatesAutoresizingMaskIntoConstraints = false
        addOpacityAnimation(cursorView)
        self.addSubview(cursorView)
        cursorView.hidden = !isEditting
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Override method
    
    override public func deleteBackward() {
        if text?.length > 1 {
            text = text?.substringToIndex((text?.endIndex.predecessor())!)
        } else {
            if  text?.length == 1 {
                text = " "
            }
        }
    }
    
    public override func canBecomeFirstResponder() -> Bool {
        return false
    }
    
    // MARK: - New method
    
    func addCharactor(charactor: String) {
        if text == " " {
            text?.removeAll()
        }
        text = text! + charactor
    }
    
    func beginEdit() {
        
        isEditting = true
        cursorView.hidden = !isEditting
        if text == "" {
            text = " "
        }
    }
    
    func endEdit() {
        
        isEditting = false
        cursorView.hidden = !isEditting
        if text == " " {
            text = ""
        }
    }
    
    // MARK: - Private method
    
    private func addOpacityAnimation(view: UIView) {
        let key = "opacity"
        let animation = CABasicAnimation(keyPath: key)
        animation.fromValue = 1.0
        animation.toValue = 0.0
        animation.duration = 0.5
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.autoreverses = true
        animation.repeatCount = FLT_MAX
        view.layer.addAnimation(animation, forKey: key)
    }
}