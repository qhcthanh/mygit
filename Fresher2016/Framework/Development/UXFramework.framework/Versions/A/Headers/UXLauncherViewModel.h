/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXLauncherView.h"

@protocol UXLauncherViewObject;
@protocol UXLauncherViewModelDelegate;

/**
 * A launcher view model that complies to the UXLauncherDataSource protocol.
 *
 * This model object allows you to keep all of your launcher view data together in one object.
 * It also conforms to the NSCoding protocol, allowing you to read and write your model to disk
 * so that you can store the state of your launcher.
 *
 */
@interface UXLauncherViewModel : NSObject <UXLauncherDataSource, NSCoding>

/**
 * Initializes a newly allocated launcher view model with an array of pages and a given delegate.
 *
 * This is the designated initializer.
 *
 * @param pages An array of arrays of objects that conform to the UXLauncherViewObject protocol.
 * @param delegate An object that conforms to the UXLauncherViewModelDelegate protocol.
 * @returns An initialized launcher view model.
 */
- (id)initWithArrayOfPages:(NSArray *)pages delegate:(id<UXLauncherViewModelDelegate>)delegate;

/**
 * Appends a page of launcher view objects.
 *
 * @param page An array of launcher view objects to add.
 */
- (void)appendPage:(NSArray *)page;

/**
 * Appends a launcher view object to a given page.
 *
 * @param object The object to add to the page.
 * @param pageIndex The index of the page to add this object to.
 */
- (void)appendObject:(id<UXLauncherViewObject>)object toPage:(NSInteger)pageIndex;

/**
 * Returns the object at the given index in the page at the given page index.
 *
 * Throws an assertion if the object index or page index are out of bounds.
 *
 * @param index The index within the page of the object to return.
 * @param pageIndex The index of the page to retrieve the object from.
 * @returns An object from a specific page.
 */
- (id<UXLauncherViewObject>)objectAtIndex:(NSInteger)index pageIndex:(NSInteger)pageIndex;

/**
 * The delegate for this launcher view model.
 */
@property (nonatomic, weak) id<UXLauncherViewModelDelegate> delegate;

@end

/**
 * The delegate for UXLauncherViewModel.
 *
 * This delegate allows you to configure the launcher button views before they are displayed.
 *
 */
@protocol UXLauncherViewModelDelegate <NSObject>
@required

/**
 * Tells the delegate to configure a button view in a given page.
 *
 * @param launcherViewModel The launcher-view model requesting this configuration.
 * @param buttonView The button view that should be configured.
 * @param launcherView The launcher-view object that will displaly this button view.
 * @param pageIndex The index of the page where this button view will be displayed.
 * @param buttonIndex The index of the button in the page.
 * @param object The object that will likely be used to configure this button view.
 */
- (void)launcherViewModel:(UXLauncherViewModel *)launcherViewModel
      configureButtonView:(UIView<UXLauncherButtonView> *)buttonView
          forLauncherView:(UXLauncherView *)launcherView
                pageIndex:(NSInteger)pageIndex
              buttonIndex:(NSInteger)buttonIndex
                   object:(id<UXLauncherViewObject>)object;

@end

/**
 * The minimal amount of information required to configure a button view.
 *
 */
@protocol UXLauncherViewObject <NSObject>
@required

/** @name Accessing the Object Attributes */

/**
 * The title that will be displayed on the launcher view button.
 */
@property (nonatomic, copy) NSString* title;

/**
 * The image that will be displayed on the launcher view button.
 */
@property (nonatomic, strong) UIImage* image;

/**
 * The class of button view that should be used to display this object.
 *
 * This class must conform to the UXLauncherButtonView protocol.
 */
- (Class)buttonViewClass;

@end

/**
 * A protocol that a launcher button view can implement to allow itself to be configured.
 *
 */
@protocol UXLauncherViewObjectView <NSObject>
@required

/** @name Updating a Launcher Button View */

/**
 * Informs the receiver that a new object should be used to configure the view.
 */
- (void)shouldUpdateViewWithObject:(id)object;

@end



