//
//  EmojiSuggestion.swift
//  EmotionDetect
//
//  Created by VanDao on 8/29/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

import Foundation

extension SuggestionManager {
    
    /**
     Suggest emoji with input is pagragraph
     
     - parameter input: The pagragraph to detect emotion
     
     - returns: String of emoji match with detected emotion
     */
    func suggestEmojiWithInput(input : String, dispatchQueue : dispatch_queue_t, completionHandler : (result : [String], error : NSError?) -> Void){
        
        dispatch_async(internalSerialQueue) { 
            let sentences: [String] = input.componentsSeparatedByCharactersInSet(NSCharacterSet.init(charactersInString: ".!?:"))
            var sentence: String = ""
            var curEmotion : (emotion : EmojiContent, isInterrogative : Bool) = (EmojiContent.none, false)
            
            for index in (sentences.startIndex..<sentences.endIndex).reverse() {
                
                if (sentences[index] != "" && sentences[index] != " ") {
                    sentence = sentences[index]
                    curEmotion = self.detectEmotionFromSentence(sentence)
                    
                    if curEmotion.emotion != EmojiContent.none {
                        break
                    }
                }
            }
            
            var result : [String] = []
            result = emojiDescriptions[curEmotion.emotion]![emojiList]!
            if curEmotion.isInterrogative {
                result += emojiDescriptions[EmojiContent.question]![emojiList]!
            }
            
            dispatch_async(dispatchQueue, { 
                completionHandler(result: result, error: nil)
            })
        }
    }
    
    /**
     Detect emotion for sentence
     
     - parameter sentence: Sentence to detect emotion
     
     - returns: Detected emotion and kind of sentence
     */
    private func detectEmotionFromSentence(sentence : String) -> (emotion : EmojiContent, isInterrogative : Bool) {
        let wordList: [String] = sentence.words()
        
        var curEmotion: EmojiContent = EmojiContent.none
        var previousWord : String = ""
        var isInterrogative : Bool = false
        
        for word in wordList.reverse() {
            
            var checkWords : [String] = []
            if previousWord != "" {
                checkWords.append(word + " " + previousWord)
            }
            checkWords.append(word)
            
            for checkWord in checkWords {
                let detect = detectWord(checkWord)
                var isBreakLoop = true
                switch detect.type {
                
                case WordDescription.emotion:
                    let detectedEmotion = EmojiContent(rawValue: detect.value as! String)!
                    if (curEmotion == EmojiContent.none) {
                        curEmotion = detectedEmotion
                    } else {
                        if revertEmojiContentTable[curEmotion]!.contains(detectedEmotion) {
                            curEmotion = EmojiContent.normal
                        }
                    }
                
                case WordDescription.negative:
                    if detect.value as! Bool {
                        if previousWord == "" && wordList.count > 1 {
                            isInterrogative = true
                        }
                        if wordList.count == 1 {
                            curEmotion = EmojiContent.denial
                        } else {
                            curEmotion = revertEmojiContentTable[curEmotion]!.first!
                        }
                    }
                    
                case WordDescription.interrogative:
                    if detect.value as! Bool {
                        if previousWord == "" && wordList.count > 1 {
                            isInterrogative = true
                        }
                        if wordList.count == 1 {
                            curEmotion = EmojiContent.normal
                        }
                    }
                    
                case WordDescription.dismiss:
                    if detect.value as! Bool {
                        
                        return (curEmotion, isInterrogative)
                    }
                    
                default:
                    isBreakLoop = false
                    break
                }
             
                if isBreakLoop {
                    break
                }
            }
            previousWord = word
        }

        return (curEmotion, isInterrogative)
    }
    
    /**
     Detect and return the EmojiContent type with specifier EmojiWord
     
     - parameter word: The EmojiWord that want to detect
     
     - returns: The EmojiContent type of EmojiWord
     */
    private func detectEmotionFromWord(word: String) -> EmojiContent {
        
        for emotion in emojiContents {
            if (emojiDescriptions[emotion]![wordCategories]!.contains(word)) {
                return emotion
            }
        }
        
        return EmojiContent.normal
    }
    
    /**
     Check if the specifier word is a negative word
     
     - parameter word: The word that want to check
     
     - returns: YES if word is a negative word, otherwise, NO
     */
    private func detectNagativeWord(word: String) -> Bool {
        
        return negativeWords.contains(word)
    }
    
    /**
     Check if the specifier word is a dismiss word
     
     - parameter word: The word that want to check
     
     - returns: YES if word is a dismiss word, otherwise, NO
     */
    private func detectDismissWord(word: String) -> Bool {
        
        return dismissWords.contains(word)
    }
    
    /**
     Check if the specifier word is a interrogative word
     
     - parameter word: The word that want to check
     
     - returns: YES if word is a interrogativ word, otherwise, NO
     */
    private func detectInterrogativeWord(word: String) -> Bool {
        
        return interrogativeWords.contains(word)
    }
    
    /**
     Detect and return what type of word want to check. The type of word can be one of those types: 
        EmojiWord: The word that has descripton emojies
        NegativeWord: The word that convert the EmojiWord to opposite EmojiWord
        InterrogativeWord: The word that descript a interrogative clause
        DismissWord: The word that make all word before become meaningless
     
     - parameter word: The word that want to detect
     
     - returns: YES if word is a negative word, otherwise, NO
     */
    private func detectWord(word: String) -> (type: WordDescription, value: AnyObject) {
        
        let res: EmojiContent = detectEmotionFromWord(word)
        if res != EmojiContent.normal {
            return (WordDescription.emotion, res.rawValue)
        }
        
        let res1: Bool = detectNagativeWord(word)
        if res1 {
            return (WordDescription.negative, res1)
        }
        
        let res2: Bool = detectInterrogativeWord(word)
        if res2 {
            return (WordDescription.interrogative, res2)
        }
        
        let res3 = detectDismissWord(word)
        if res3 {
            return (WordDescription.dismiss, res3)
        }
        
        return (WordDescription.none, "")
    }
}