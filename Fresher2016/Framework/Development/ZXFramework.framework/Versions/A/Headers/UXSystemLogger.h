/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <asl.h>

#import "UXLogFormatter.h"
#import "UXAbstractLogger.h"

/**
 * This class provides a logger for the Apple System Log facility.
 * 
 * As described in the "Getting Started" page,
 * the traditional NSLog() function directs it's output to two places:
 * 
 * - Apple System Log
 * - StdErr (if stderr is a TTY) so log statements show up in Xcode console
 * 
 * To duplicate NSLog() functionality you can simply add this logger and a tty logger.
 * However, if you instead choose to use file logging (for faster performance),
 * you may choose to use a file logger and a tty logger. 
 */
@interface UXSystemLogger : UXAbstractLogger <UXLogger> {
	aslclient client;
}

+ (UXSystemLogger *)sharedInstance;

// Inherited from UXAbstractLogger

// - (id <UXLogFormatter>)logFormatter;
// - (void)setLogFormatter:(id <UXLogFormatter>)formatter;

@end
