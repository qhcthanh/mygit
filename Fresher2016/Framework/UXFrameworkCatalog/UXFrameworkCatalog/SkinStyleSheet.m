/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "SkinStyleSheet.h"

@implementation SkinStyleSheet

- (UXStyle*)avatarDefaultButtonStyle:(id)object {
    
    UIControlState state = UIControlStateNormal;
    if ([object isKindOfClass:[NSNumber class]]) {
        state = (UIControlState)[object integerValue];
    } else {
        state = (UIControlState)object;
    }
    
    if (state == UIControlStateNormal) {
        return
        [UXShapeStyle styleWithShape:[UXRoundedRectangleShape shapeWithRadius:100] next:
         [UXLinearGradientFillStyle styleWithColor1:[UIColor greenColor]
                                             color2:[UIColor greenColor] next:
         [UXSolidBorderStyle styleWithColor:RGBCOLOR(191, 197, 208) width:0 next:
          [UXBoxStyle styleWithPadding:UIEdgeInsetsMake(9, 12, 10, 12)
                                  next:nil]]]];
    } else if (state == UIControlStateHighlighted)  {
        return
        [UXShapeStyle styleWithShape:[UXRoundedRectangleShape shapeWithRadius:100] next:
         [UXLinearGradientFillStyle styleWithColor1:RGBACOLOR(0, 0, 0, 0.3)
                                             color2:RGBACOLOR(0, 0, 0, 0.3) next:
          [UXSolidBorderStyle styleWithColor:RGBCOLOR(191, 197, 208) width:0 next:
           [UXBoxStyle styleWithPadding:UIEdgeInsetsMake(9, 12, 10, 12)
                                   next:nil]]]];
    }
    
    return nil;
}

@end
