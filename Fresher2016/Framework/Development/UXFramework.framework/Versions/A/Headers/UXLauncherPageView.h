/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXPagingScrollView.h"
#import "UXPagingScrollViewPage.h"

@class UXViewRecycler;
/**
 * A single page in a launcher view.
 *
 * This is a recyclable page view that can be used with NIPagingScrollView.
 *
 * Each launcher page contains a set of views. The page lays out each of the views in a grid based
 * on the given display attributes.
 *
 * Views will be laid out from left to right and then from top to bottom.
 *
 */
@interface UXLauncherPageView : UXPagingScrollViewPage

/**
 * A shared view recycler for this page's recyclable views.
 *
 * When this page view is preparing for reuse it will add each of its button views to the recycler.
 * This recycler should be the same recycler used by all pages in the launcher view.
 *
 */
@property (nonatomic, strong) UXViewRecycler* viewRecycler;

/**
 * Add a recyclable view to this page.
 *
 * @param view A recyclable view.
 */
- (void)addRecyclableView:(UIView<UXRecyclableView> *)view;

/**
 * All of the recyclable views that have been added to this page.
 *
 * @returns An array of recyclable views in the same order in which they were added.
 */
@property (nonatomic, readonly, strong) NSArray* recyclableViews;

/**
 * The distance that the recyclable views are inset from the enclosing view.
 *
 * Use this property to add to the area around the content. The unit of size is points.
 * The default value is UIEdgeInsetsZero.
 *
 */
@property (nonatomic, assign) UIEdgeInsets contentInset;

/**
 * The size of each recyclable view.
 *
 * The unit of size is points. The default value is CGSizeZero.
 *
 */
@property (nonatomic, assign) CGSize viewSize;

/**
 * The recommended horizontal and vertical distance between each recyclable view.
 *
 * This property is only a recommended value because the page view does its best to distribute the
 * views in a way that visually balances them.
 *
 * Width is the horizontal distance between each view. Height is the vertical distance between each
 * view. The unit of size is points. The default value is CGSizeZero.
 *
 */
@property (nonatomic, assign) CGSize viewMargins;

@end
