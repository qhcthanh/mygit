//
//  StickerCollectionViewController.m
//  CameraGPUImage
//
//  Created by qhcthanh on 6/16/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "StickerCollectionViewController.h"

@implementation StickerCollectionViewController {
    UICollectionViewFlowLayout *stickerLayout;
    CGPoint lastTouchPosition;
}

-(id)init:(NSMutableArray* _Nonnull)stickerArray {
    lastTouchPosition = CGPointZero;
    stickerLayout= [[UICollectionViewFlowLayout alloc] init];
    // set scroll dicrection
    [stickerLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    stickerLayout.itemSize = CGSizeMake(80, 80);
    stickerLayout.minimumLineSpacing = 10;
    stickerLayout.sectionInset = UIEdgeInsetsMake(0, 20, 0, 20);
    //Setup init
    self = [super initWithCollectionViewLayout:stickerLayout];
    self.collectionView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"StickerCell"];
    self.stickerArray = stickerArray;
    [self.collectionView reloadData];
    
    //Add gesture
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [tapGesture setNumberOfTapsRequired:1];
    [self.collectionView addGestureRecognizer:tapGesture];
    return self;
}

-(void)viewDidLoad {
    
}

//MARK: Gesture
-(void)handleTap:(UIGestureRecognizer*) gesture  {
    if (gesture.state == UIGestureRecognizerStateEnded) {
        lastTouchPosition = [gesture locationInView: self.collectionView];
        NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:lastTouchPosition];
        if (indexPath) {
            if (self.stickerDelegate &&
                [self.stickerDelegate respondsToSelector:@selector(didSelectItem:position:)]) {
                id item = [_stickerArray objectAtIndex:indexPath.row];
                lastTouchPosition.y -= self.collectionView.contentOffset.y;
                [self.stickerDelegate didSelectItem:item position:lastTouchPosition];
            }
            
        }
    }
}

//MARK: API func
-(void)setBackgroundColor:(UIColor* _Nonnull)backgroundColor {
    self.collectionView.backgroundColor = backgroundColor;
}

-(void)setSizeOfItem:(CGSize)sizeOfItem {
    stickerLayout.itemSize = sizeOfItem;
    [self.collectionView reloadData];
}

-(void)setMiniumSpacingLine:(CGFloat)spacing {
    stickerLayout.minimumLineSpacing = spacing;
    [self.collectionView reloadData];
}

-(void)setMarginLayout:(UIEdgeInsets)insetMake {
    stickerLayout.sectionInset = insetMake;
    [self.collectionView reloadData];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.stickerArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView
                                  dequeueReusableCellWithReuseIdentifier:@"StickerCell" forIndexPath:indexPath];
    UIImageView* stickImageView = [[UIImageView alloc]
                                   initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    
    id object = [self.stickerArray objectAtIndex:indexPath.row];
    if ([object isKindOfClass:[UIImage class]]) {
        stickImageView.image = (UIImage*)object;
    }
    
    [cell addSubview: stickImageView];
    //cell.backgroundColor=[UIColor greenColor];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    if (self.stickerDelegate &&
//        [self.stickerDelegate respondsToSelector:@selector(didSelectItem:position:)]) {
//        id item = [_stickerArray objectAtIndex:indexPath.row];
//        [self.stickerDelegate didSelectItem:item position:lastTouchPosition];
//    }
//    
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0,25,0, 25);  // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

@end
