/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Alexey Belkevich, 2014
 * Updated by ThemND VNG Coporation, 2015
 * Change Log: Integrate Contact Framework that supports the iOS9
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@interface UXPhoneWithLabel : NSObject

@property (nonatomic, readonly) NSString *phone;
@property (nonatomic, readonly) NSString *originalLabel;
@property (nonatomic, readonly) NSString *localizedLabel;
@property (nonatomic, readonly) NSString *label __attribute__((deprecated("Use 'localizedLabel' instead")));

- (id)initWithPhone:(NSString *)phone originalLabel:(NSString *)originalLabel localizedLabel:(NSString *)localizedLabel;

@end
