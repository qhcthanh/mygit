/* Copyright (c) 2016-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "UXImageTransitionView.h"

#define kImageScaledSize 100

@interface UXImageTransitionView  (){
    UIImageView *imageView;
}

@property (readwrite) UIImage *image;

@end

@implementation UXImageTransitionView

- (id _Nullable)initWithImage:(UIImage * _Nonnull)image{
    return [self initWithImage:image atCenterPosition:CGPointMake(kWidth/2, kHeight/2)];
}

- (id _Nullable)initWithImage:(UIImage * _Nonnull)image atCenterPosition:(CGPoint)position{
    
    CGFloat ratio = image.size.height / image.size.width;
    
    self = [super initWithFrame:CGRectMake(position.x - kImageScaledSize/2, position.y - kImageScaledSize * ratio / 2, kImageScaledSize, kImageScaledSize * ratio)];
    
    if (self){

        self.opaque = NO;
        self.userInteractionEnabled = NO;
        self.multipleTouchEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        
        imageView = [[UIImageView alloc] initWithImage:image];
        imageView.frame = CGRectMake(0, 0, kImageScaledSize, kImageScaledSize * ratio);
        [self addSubview:imageView];
        _image = image;
        
        self.vertexOne = CGPointMake(position.x - kImageScaledSize / 2,  position.y - kImageScaledSize * ratio /2);
        self.vertexTwo = CGPointMake(position.x + kImageScaledSize / 2,  position.y - kImageScaledSize * ratio /2);
        self.vertexThree = CGPointMake(position.x + kImageScaledSize / 2,  position.y + kImageScaledSize * ratio /2);
        self.vertexFour = CGPointMake(position.x - kImageScaledSize / 2,  position.y + kImageScaledSize * ratio /2);
        self.width = kImageScaledSize;
        self.height = kImageScaledSize * ratio;        
    }
    
    return self;
}

@end
