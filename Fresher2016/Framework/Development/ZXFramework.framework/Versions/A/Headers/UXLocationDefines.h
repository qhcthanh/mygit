/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#ifndef UXLocationDefines_h
#define UXLocationDefines_h

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#if __has_feature(objc_generics)
#   define __UX_GENERICS(type, ...)       type<__VA_ARGS__>
#else
#   define __UX_GENERICS(type, ...)       type
#endif

#ifdef NS_DESIGNATED_INITIALIZER
#   define __UX_DESIGNATED_INITIALIZER    NS_DESIGNATED_INITIALIZER
#else
#   define __UX_DESIGNATED_INITIALIZER
#endif

static const CLLocationAccuracy kUXHorizontalAccuracyThresholdCity =         5000.0;  // in meters
static const CLLocationAccuracy kUXHorizontalAccuracyThresholdNeighborhood = 1000.0;  // in meters
static const CLLocationAccuracy kUXHorizontalAccuracyThresholdBlock =         100.0;  // in meters
static const CLLocationAccuracy kUXHorizontalAccuracyThresholdHouse =          15.0;  // in meters
static const CLLocationAccuracy kUXHorizontalAccuracyThresholdRoom =            5.0;  // in meters

static const NSTimeInterval kUXUpdateTimeStaleThresholdCity =             600.0;  // in seconds
static const NSTimeInterval kUXUpdateTimeStaleThresholdNeighborhood =     300.0;  // in seconds
static const NSTimeInterval kUXUpdateTimeStaleThresholdBlock =             60.0;  // in seconds
static const NSTimeInterval kUXUpdateTimeStaleThresholdHouse =             15.0;  // in seconds
static const NSTimeInterval kUXUpdateTimeStaleThresholdRoom =               5.0;  // in seconds

// The possible states that location services can be in.
typedef NS_ENUM(NSInteger, UXLocationServicesState) {
    
    // User has already granted this app permissions to access location services, and they are enabled and ready for use by this app.
    // Note: this state will be returned for both the "When In Use" and "Always" permission levels.
    UXLocationServicesStateAvailable,
    
    // User has not yet responded to the dialog that grants this app permission to access location services.
    UXLocationServicesStateNotDetermined,
    
    // User has explicitly denied this app permission to access location services. (The user can enable permissions again for this app from the system Settings app.)
    UXLocationServicesStateDenied,
    
    // User does not have ability to enable location services (e.g. parental controls, corporate policy, etc).
    UXLocationServicesStateRestricted,
    
    // User has turned off location services device-wide (for all apps) from the system Settings app.
    UXLocationServicesStateDisabled
};

// A unique ID that corresponds to one location request.
typedef NSInteger UXLocationRequestID;

// An abstraction of both the horizontal accuracy and recency of location data.
// Room is the highest level of accuracy/recency; City is the lowest level.
typedef NS_ENUM(NSInteger, UXLocationAccuracy) {
    // 'None' is not valid as a desired accuracy.
    // Inaccurate (>5000 meters, and/or received >10 minutes ago).
    UXLocationAccuracyNone = 0,
    
    // The below options are valid desired accuracies.
    // 5000 meters or better, and received within the last 10 minutes. Lowest accuracy.
    UXLocationAccuracyCity,
    
    // 1000 meters or better, and received within the last 5 minutes.
    UXLocationAccuracyNeighborhood,
    
    // 100 meters or better, and received within the last 1 minute.
    UXLocationAccuracyBlock,
    
    // 15 meters or better, and received within the last 15 seconds.
    UXLocationAccuracyHouse,
    
    // 5 meters or better, and received within the last 5 seconds. Highest accuracy.
    UXLocationAccuracyRoom,
};

// A status that will be passed in to the completion block of a location request.
typedef NS_ENUM(NSInteger, UXLocationStatus) {
    
    // These statuses will accompany a valid location.
    // Got a location and desired accuracy level was achieved successfully.
    UXLocationStatusSuccess = 0,
    
    // Got a location, but the desired accuracy level was not reached before timeout. (Not applicable to subscriptions.)
    UXLocationStatusTimedOut,
    
    // These statuses indicate some sort of error, and will accompany a nil location.
    // User has not yet responded to the dialog that grants this app permission to access location services.
    UXLocationStatusServicesNotDetermined,
    
    // User has explicitly denied this app permission to access location services.
    UXLocationStatusServicesDenied,
    
    // User does not have ability to enable location services (e.g. parental controls, corporate policy, etc).
    UXLocationStatusServicesRestricted,
    
    // User has turned off location services device-wide (for all apps) from the system Settings app.
    UXLocationStatusServicesDisabled,
    
    // An error occurred while using the system location services.
    UXLocationStatusError
};

/**
 *  A block type for a location request, which is executed when the request succeeds, fails, or times out.
 *
 *  @param currentLocation  The most recent & accurate current location available when the block executes, or nil if no valid location is available.
 *  @param achievedAccuracy The accuracy level that was actually achieved (may be better than, equal to, or worse than the desired accuracy).
 *  @param status           The status of the location request - whether it succeeded, timed out, or failed due to some sort of error. This can be used to understand what the outcome of the request was, decide if/how to use the associated currentLocation, and determine whether other actions are required (such as displaying an error message to the user, retrying with another request, quietly proceeding, etc).
 */
typedef void(^UXLocationRequestBlock)(CLLocation *currentLocation, UXLocationAccuracy achievedAccuracy, UXLocationStatus status);

#endif /* UXLocationDefines_h */
