//
//  ContactImageCaching.m
//  Project2
//
//  Created by BaoNQ on 6/1/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "ContactImageCaching.h"

@interface ContactImageCaching () {
    NSMutableDictionary *_cachedImage;
    dispatch_queue_t _internalSerialQueue;
}
@end

@implementation ContactImageCaching

//Singleton Method
+ (id)sharedContactImageCaching {
    static ContactImageCaching *cachedImage = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cachedImage = [[self alloc] init];
    });
    return cachedImage;
}

- (id)init {
    self = [super init];
    if (!self)
        return nil;
    self->_cachedImage = [[NSMutableDictionary alloc] init];
    self->_internalSerialQueue = dispatch_queue_create("com.vng.imageCachingQueue", NULL);

    return self;
}

- (void)cacheImage:(UIImage *)image forKey:(id<NSCopying>)aKey {
    if (image == nil || aKey == nil)
        return;
    if ([_cachedImage objectForKey:aKey] != nil)
        return;
    
    dispatch_async(_internalSerialQueue, ^{
        [_cachedImage setObject:image forKey:aKey];
    });
}

- (UIImage *)imageForKey:(id<NSCopying>)aKey {
    __block UIImage *returnedImage;
    dispatch_sync(_internalSerialQueue, ^{
        returnedImage = [_cachedImage objectForKey:aKey];
    });
    return returnedImage;
}

- (void)removeObjectForKey:(id<NSCopying>)aKey {
    dispatch_async(_internalSerialQueue, ^{
        [_cachedImage removeObjectForKey:aKey];
    });
}

@end
