//
//  CTCategoryInfo.swift
//  BanLaKey
//
//  Created by Quach Ha Chan Thanh on 9/7/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import Foundation
import UIKit


public class CTCategoryInfo: NSObject {
    
    var headerTitle: String?
    var footerTitle: String?
    
    var headerHeight: CGFloat = 20
    var footerHeight: CGFloat = 20
    
    
    var headerView: UIView?
    var footerView: UIView?
    
    var items: [CTTableItem]!
    
    init(headerTitle: String? = nil, footerTitle: String? = nil, headerHeight: CGFloat = 20, footerHeight: CGFloat = 20, items: [CTTableItem]) {
        self.headerTitle = headerTitle
        self.footerTitle = footerTitle
        self.headerHeight = headerHeight
        self.footerHeight = footerHeight
        self.items = items
    }
}

public enum CTTableViewReuseCell: String {
    case ImageContent = "ImageTitleTableCell"
    case TitleContent = "TitleTableCell"
    case SwitchContent = "TitleSwitchTableCell"
}

protocol CTTableViewProtolcol: NSObjectProtocol {
    
    var title: String! {get set}
    var subTitle: String? {get set}
    var image: UIImage? {get set}
    var switchBlock: ((Bool) -> Void)? {get set}
    var navigationBlock: (() -> Void)? {get set}
    var selectBlock: (() -> Void)? {get}
    var deSelectBlock: (() -> Void)? {get}
    var itemHeight: CGFloat! {get set}
    var cellIdentifier: CTTableViewReuseCell  {get set}
    var switchOn: Bool? {get set}

}

public class CTTableItem: NSObject, CTTableViewProtolcol {
   
    
    var title: String!
    var subTitle: String?
    var image: UIImage?
    var switchOn: Bool?
    var switchBlock: ((Bool) -> Void)?
    var navigationBlock: (() -> Void)?
    var selectBlock: (() -> Void)?
    var deSelectBlock: (() -> Void)?
    var cellIdentifier: CTTableViewReuseCell = .TitleContent
    var itemHeight: CGFloat!
    
    var isCheckMark: Bool = false
    var isCheck: Bool = false
    
    init(title: String, subTitle: String? = nil, image: UIImage? = nil, itemHeight: CGFloat = 40, switchBlock: ((Bool) -> Void)? = nil, on: Bool = true ) {
        super.init()
        
        self.initialize(title: title, subTitle: subTitle, image: image, itemHeight: itemHeight)
        self.switchBlock = switchBlock
        
        if self.switchBlock != nil {
            self.cellIdentifier = .SwitchContent
            self.switchOn = on
        }
    }
    
    init(title: String, subTitle: String? = nil, image: UIImage? = nil, itemHeight: CGFloat = 40, navigationBlock: (() -> Void)? = nil ) {
        super.init()
        
        self.initialize(title: title, subTitle: subTitle, image: image, itemHeight: itemHeight)
        self.navigationBlock = navigationBlock
    }
    
    init(title: String, subTitle: String? = nil, image: UIImage? = nil, itemHeight: CGFloat = 40, isCheckMark: Bool = false, isCheck: Bool = false, selectBlock: (() -> Void)? = nil, deSelectBlock: (() -> Void)? = nil  ) {
        super.init()
        
        self.initialize(title: title, subTitle: subTitle, image: image, itemHeight: itemHeight)
        
        self.isCheck = isCheck
        self.isCheckMark = isCheckMark
        self.selectBlock = selectBlock
        self.deSelectBlock = deSelectBlock
    }
    
    private func initialize(title title: String, subTitle: String? = nil, image: UIImage? = nil, itemHeight: CGFloat = 40) {
        
        self.title = title
        self.subTitle = subTitle
        self.image = image
        self.itemHeight = itemHeight
        
        if image != nil {
             self.cellIdentifier = .ImageContent
        }
    }
}









