/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@class UXAlertController;
@class UXPopoverPresentationController;

typedef NS_ENUM(NSInteger, UniversalAlertIndex) {
    kAlertNoButtonExistsIndex = -1,
    kAlertCancelButtonIndex = 0,
    kAlertDestructiveButtonIndex = 1,
    kAlertFirstOtherButtonIndex = 2
};

typedef void (^UXAlertControllerWithIndexHandler)(UXAlertController* alert, NSInteger buttonIndex);
typedef void (^UXAlertControllerWithURLHandler)(UXAlertController* alert, NSString* buttonURL);

@interface UXAlertController : NSObject

/**
 *  Create and immediately display an AlertView.
 *
 *  @param viewController         The viewController where alert will show
 *  @param title                  The title of the AlertView (see UIAlertView)
 *  @param message                The message of the AlertView (see UIAlertView)
 *  @param cancelButtonTitle      The title for the "cancel" button (see UIAlertView)
 *  @param destructiveButtonTitle The title for the "destructive" button style (see UIAlertView)
 *  @param otherButtonTitles      A NSArray of NSStrings containing titles for the other buttons (see UIAlertView)
 *  @param completionHandler      The callback that handles click event on the button at index
 */
+ (instancetype)showAlertViewInViewController:(UIViewController *)viewController
                                    withTitle:(NSString *)title
                                      message:(NSString *)message
                            cancelButtonTitle:(NSString *)cancelButtonTitle
                       destructiveButtonTitle:(NSString *)destructiveButtonTitle
                            otherButtonTitles:(NSArray *)otherButtonTitles
                            completionHandler:(UXAlertControllerWithIndexHandler)completionHandler;

/**
 *  Create and immediately display an AlertView.
 *
 *  @param viewController         The viewController where alert will show
 *  @param title                  The title of the AlertView (see UIAlertView)
 *  @param message                The message of the AlertView (see UIAlertView)
 *  @param cancelButtonTitle      The title for the "cancel" button (see UIAlertView)
 *  @param cancelButtonURL        The URL of the "cancel" button
 *  @param destructiveButtonTitle The title for the "destructive" button style (see UIAlertView)
 *  @param destructiveButtonURL   The URL of the "destructive" button
 *  @param otherButtonTitles      The titles of other buttons
 *  @param otherButtonURLs        The URLs of other buttons
 *  @param completionHandler      The callback that handles click event on the button at URL
 */
+ (instancetype)showURLAlertViewInViewController:(UIViewController *)viewController
                                       withTitle:(NSString *)title
                                         message:(NSString *)message
                               cancelButtonTitle:(NSString *)cancelButtonTitle  cancelButtonURL:(NSString *)cancelButtonURL
                          destructiveButtonTitle:(NSString *)destructiveButtonTitle destructiveButtonURL:(NSString*)destructiveButtonURL
                               otherButtonTitles:(NSArray*)otherButtonTitles
                                 otherButtonURLs:(NSArray*)otherButtonURLs
                               completionHandler:(UXAlertControllerWithURLHandler)completionHandler;

/**
 *  Create and immediately display an ActionSheet.
 *
 *  @param viewController                       The viewController where actionSheet will show
 *  @param title                                The title of the ActionSheet (see UIActionSheet)
 *  @param message                              The message of the ActionSheet (see UIActionSheet)
 *  @param cancelButtonTitle                    The title for the "cancel" button (see UIActionSheet)
 *  @param cancelButtonURL                      The URL of the "cancel" button
 *  @param destructiveButtonTitle               The title for the "destructive" button style (see UIActionSheet)
 *  @param destructiveButtonURL                 The URL of the "destructive" button
 *  @param otherButtonTitles                    The title array of other buttons
 *  @param otherButtonURLs                      The URLs of other button
 *  @param popoverPresentationControllerHandler The popoverPresentationController block handler
 *  @param completionHandler                    The callback that handles click event on the button at URL
 *
 *  @return An action sheet instance that is prensented on screen
 */
+ (instancetype)showURLActionSheetInViewController:(UIViewController *)viewController
                                         withTitle:(NSString *)title
                                           message:(NSString *)message
                                 cancelButtonTitle:(NSString *)cancelButtonTitle cancelButtonURL:(NSString *)cancelButtonURL
                            destructiveButtonTitle:(NSString *)destructiveButtonTitle
                              destructiveButtonURL:(NSString*)destructiveButtonURL
                                 otherButtonTitles:(NSArray*)otherButtonTitles
                                   otherButtonURLs:(NSArray*)otherButtonURLs
              popoverPresentationControllerHandler:(void(^)(UXPopoverPresentationController * popover))popoverPresentationControllerHandler
                                 completionHandler:(UXAlertControllerWithURLHandler)completionHandler;

@property (nonatomic, strong) UIAlertView *alertView;
@property (nonatomic, strong) UIActionSheet *actionSheet;
@property (nonatomic, strong) UIAlertController *alertController;

@property (nonatomic, assign) NSInteger cancelButtonIndex;

@property (nonatomic, assign) NSInteger destructiveButtonIndex;

@property (nonatomic, readonly) NSInteger firstOtherButtonIndex;

@property (nonatomic, readonly) BOOL isVisible;

@property (nonatomic, assign) BOOL isCancelButtonIndex;
@property (nonatomic, assign) BOOL isDestructiveButtonIndex;
@property (nonatomic, assign) BOOL isFirstOtherButtonIndex;

@end
