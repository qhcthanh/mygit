/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

// All docs are in the .m.
@interface CustomNibCollectionModelViewController : UXCollectionViewController
@end
