/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#define kSessionUid         @"kSessionUid"
#define kSessionKey         @"kSessionKey"
#define kSessionUsername    @"kSessionUsername"
#define kSessionHashed      @"kSessionHashed"
#define kAccessToken        @"kAccessToken"
#define kPhoneNumber        @"kPhoneNumber"
#define kNativePhoneNumber  @"kNativePhoneNumber"

#define kKeySaveSequenceIdUniqueue  @"kKeySaveSequenceIdUniqueue"


#define kApnsTokenKey @"kApnsTokenKey"

@protocol UXSessionContextDelegate;

extern const NSString* kSessionContextApiPublicKey;
extern const NSString* kSessionContextApiSecretKey;
extern const NSString* kSessionContextApiCryptoKey;

@interface UXSessionContext : NSObject

+ (NSString*)advertisingIdentifier;

+ (UXSessionContext*)manager;
+ (UXSessionContext*)sessionForApplication:(NSString*)apiKey
                             withSecretKey:(NSString*)secretKey
                              withDelegate:(id<UXSessionContextDelegate>)delegate;

- (UXSessionContext*)initWithKey:(NSString*)apiKey
                   withSecretKey:(NSString*)secretKey;

- (void)updatePhoneNumber:(NSString*)phoneNumber nativePhoneNumber:(NSString*)nativePhoneNumber;

- (BOOL)parseLoginInfo:(NSDictionary*)dataDictionnary withLogin:(BOOL)login withCloud:(BOOL)cloud;

- (NSString*)generateSigWithParams:(NSMutableDictionary*)params;

/**
 * Resumes a previous session whose uid, session key, and secret are cached on disk.
 */
- (BOOL)resume;

/**
 * Cancels a login (no-op if the login is already complete).
 */
- (void)cancelLogin;

/**
 * Ends the current session and deletes the uid, session key, and secret from disk.
 */
- (void)logout;

/**
 * Alert copyright information
 */
- (void)alertSecurity;

/**
 * Properties
 */

/**
 * Delegates which implement UXSessionContextnDelegate.
 */
@property(nonatomic,readonly) NSMutableArray* delegates;

/**
 * The current user's id.
 */
@property(nonatomic,assign) BOOL isLoggedIn;

/**
 * Your application's API key, as passed to the constructor.
 */
@property(nonatomic, retain) NSString* apiKey;

/**
 * Your application's API secret, as passed to the constructor.
 */
@property(nonatomic, retain) NSString* apiSecret;

/**
 * The current user's session key.
 */
@property(nonatomic,readonly) NSString* sessionKey;
/**
 * The current user's session key.
 */
@property(nonatomic,readonly) NSString* avatarDefaultURL;

@property(nonatomic,readonly) NSString* userName;
@property(nonatomic,readonly) NSString* hashedPassword;

@property(nonatomic,readonly) NSString* userId;

@property(nonatomic,readonly) NSString* accessToken;
@property(nonatomic,readonly) NSString* phoneNumber;
@property(nonatomic,readonly) NSString* nativePhoneNumber;

@property(nonatomic,assign) BOOL isAutoLogging;

@property(nonatomic,assign) NSUInteger intUserId;

@property(nonatomic,retain) NSString* apnsTokenString;

@property(nonatomic,readonly) NSTimeInterval logTimeInterval;

@end

@protocol UXSessionContextDelegate <NSObject>

/**
 * Called when a user has successfully logged in and begun a session.
 */
- (void)session:(UXSessionContext*)session didLogin:(NSString*)uid;

@optional

/**
 * Called when a user closes the login dialog without logging in.
 */
- (void)sessionDidNotLogin:(UXSessionContext*)session;

/**
 * Called when a session is about to log out.
 */
- (void)session:(UXSessionContext*)session willLogout:(NSString*)uid;

/**
 * Called when a session has logged out.
 */
- (void)sessionDidLogout:(UXSessionContext*)session;
- (void)sessionDidLogin:(UXSessionContext *)session ;

@end
