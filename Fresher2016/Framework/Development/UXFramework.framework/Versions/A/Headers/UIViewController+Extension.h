/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@interface UIViewController (Extension)

/**
 * The default initializer sent to view controllers opened through UXNavigationManager.
 */
- (id)initWithNavigatorURL:(NSURL*)URL query:(NSDictionary*)query;

/**
 * The current URL that this view controller represents.
 */
@property (nonatomic, readonly) NSString* navigatorURL;

/**
 * The URL that was used to load this controller through UXNavigationManager.
 *
 * Do not ever change the value of this property.  UXNavigationManager will assign this
 * when creating your view controller, and it expects it to remain constant throughout
 * the view controller's life.  You can override navigatorURL if you want to specify
 * a different URL for your view controller to use when persisting and restoring it.
 */
@property (nonatomic, copy) NSString* originalNavigatorURL;

/**
 * Determines whether a controller is primarily a container of other controllers.
 */
@property (nonatomic, readonly) BOOL canContainControllers;

/**
 * Whether or not this controller should ever be counted as the "top" view controller. This is
 * used for the purposes of determining which controllers should have modal controllers presented
 * within them.
 *
 * @default YES; subclasses may override to NO if they so desire.
 */
@property (nonatomic, readonly) BOOL canBeTopViewController;

/**
 * The child of this view controller which is most visible.
 *
 * This would be the selected view controller of a tab bar controller, or the top
 * view controller of a navigation controller.  This property offers custom UIViewController
 * subclasses the chance to tell UXNavigationObject how to follow the hierarchy of view controllers.
 */
- (UIViewController*)topSubViewController;

/**
 * Displays a controller inside this controller.
 *
 * UXURLMap uses this to display newly created controllers.  The default does nothing --
 * UIViewController categories and subclasses should implement to display the controller
 * in a manner specific to them.
 */
- (void)addSubcontroller:(UIViewController*)controller animated:(BOOL)animated
              transition:(UIViewAnimationTransition)transition;

- (void)unsetNavigatorProperties;

@end
