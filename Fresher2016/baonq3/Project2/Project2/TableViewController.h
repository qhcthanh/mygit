//
//  ViewController.h
//  Project2
//
//  Created by BaoNQ on 5/17/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "CTContactStore.h"
#import "CTCollectionViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "AddContactController.h"

@interface TableViewController : UIViewController <UITableViewDataSource,UITableViewDelegate, UISearchResultsUpdating, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ContactDelegate>

//Properties
@property (nonatomic) NSMutableArray *filteredContacts;
@property (nonatomic) NSMutableArray *filteredSectionIndex;
@property (nonatomic) NSMutableArray *contacts;
@property (nonatomic) NSMutableArray *sectionIndex;
@property (nonatomic) NSMutableArray *sectionIndexTitles;
@property (nonatomic) BOOL isFiltered;
@property (nonatomic) NSMutableArray *checkedList;
@property (nonatomic) NSUInteger numberOfCheckedCell;

//Image caching
@property (nonatomic) ContactImageCaching *cachedImage;

//Tableview and search controller
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UISearchController *searchController;
@property (weak, nonatomic) IBOutlet UIView *searchBarView;

//Collection view
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;

//Actions
- (IBAction)editMode:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addContactButton;

//Outlets
@property (weak, nonatomic) IBOutlet UIBarButtonItem *leftBarButton;



@end