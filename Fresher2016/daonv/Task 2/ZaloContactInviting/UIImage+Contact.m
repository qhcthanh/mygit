//
//  UIImage+Contact.m
//  ZaloContactInviting
//
//  Created by VanDao on 6/6/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import "UIImage+Contact.h"
#define ARC4RANDOM_MAX      0x100000000

@implementation UIImage (Contact)

+ (UIImage * _Nonnull)createImageFromLetters:(NSString * _Nullable)displayString{
    if (displayString == nil)
        displayString = @"";
    
    CGFloat height = 40;
    srand48(arc4random());
    CGFloat fontSize = height * 0.4f;
    
    NSDictionary *textAttributes = @{
                                     NSFontAttributeName: [UIFont systemFontOfSize:fontSize weight:0.4],
                                     NSForegroundColorAttributeName:[UIColor whiteColor]
                                     };
    UIColor *backgroundColor = [UIColor colorWithRed:(double)arc4random() / ARC4RANDOM_MAX green:(double)arc4random() / ARC4RANDOM_MAX blue:(double)arc4random() / ARC4RANDOM_MAX alpha:1.0f];
    CGFloat scale = [UIScreen mainScreen].scale;
    CGSize size = CGSizeMake(height, height);
    
    //Begin create image
    UIGraphicsBeginImageContextWithOptions(size, NO, scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //Clip context to a circle
    CGPathRef path = CGPathCreateWithEllipseInRect(CGRectMake(0, 0, height, height), NULL);
    CGContextAddPath(context, path);
    CGContextClip(context);
    CGPathRelease(path);
    
    //Fill backgrond of context
    CGContextSetFillColorWithColor(context, backgroundColor.CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, size.width, size.height));
    CGSize textSize = [displayString sizeWithAttributes:textAttributes];
    [displayString drawInRect:CGRectMake(height/2 - textSize.width/2,
                                         height/2 - textSize.height/2,
                                         textSize.width,
                                         textSize.height)
               withAttributes:textAttributes];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (UIImage * _Nonnull)clipToCircleImage:(UIImage * _Nonnull)image withMask:(UIImage * _Nonnull)maskImage{
    if (image == nil || maskImage == nil)
        return nil;
    
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    
    CGImageRef maskedImageRef = CGImageCreateWithMask([image CGImage], mask);
    UIImage *maskedImage = [UIImage imageWithCGImage:maskedImageRef];
    
    CGImageRelease(mask);
    CGImageRelease(maskedImageRef);
    
    return maskedImage;
}

@end
