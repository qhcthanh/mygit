//
//  UIScreen+Orientation.swift
//  BanLaKey
//
//  Created by BaoNQ on 8/17/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

public enum BLInterfaceOrientation: Int {
    case Unknown = 0
    case Portrait
    case Landscape
}

public extension UIScreen {
    
    public func interfaceOrientation() -> BLInterfaceOrientation {
        if bounds.height > bounds.width {
            return BLInterfaceOrientation.Portrait
        } else {
            return BLInterfaceOrientation.Landscape
        }
    }
    
}
