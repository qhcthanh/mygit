//
//  GiphyToastView.swift
//  EmojiKeyboard
//
//  Created by qhcthanh on 8/24/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import UIKit

public class BLGiphyToastView: UIView {
    
    public var toastLabel: UILabel!
    public var isAnimated: Bool  = false
    
    public convenience init() {
        self.init(frame: CGRectZero)
        
        setupView()
    }
    
    public override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        setupView()
    }
    
    private func setupView() {
        self.toastLabel = UILabel()
        self.toastLabel.textAlignment = .Center
        self.toastLabel.textColor = .whiteColor()
        self.toastLabel.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.65)
        self.toastLabel.adjustsFontSizeToFitWidth = true
        
        self.addSubview(self.toastLabel)
        
        self.setupConstraint()
    }
    
    private func setupConstraint() {
        if self.toastLabel != nil {
            self.toastLabel.constrain(toEdgesOf: self)
        }
    }
    
//    public func setToastBackgroundColor(color: UIColor) {
//        self.toastLabel.backgroundColor = color
//    }
    
//    public var toastBackgroundColor: UIColor! {
//        get {
//            return self.toastLabel.backgroundColor
//        } set {
//            self.toastLabel.backgroundColor = UIColor.blueColor().colorWithAlphaComponent(0.5)
//        }
//    }
    
    
    public func animateShowToast(timer: NSTimeInterval = 2) {
        
        if let superView = self.superview where self.isAnimated == false {
            self.isAnimated = true
            superView.clipsToBounds = true
            let superFrame = superView.frame
            
            self.frame.origin.y = -self.frame.size.height
            
            let timeAnimation = timer/3
            
            
            UIView.animateWithDuration(timeAnimation, animations: {
                self.frame.origin.y = (superFrame.height - self.frame.size.height) / 2
                }, completion: {
                    success in
                    if success {
                        UIView.animateWithDuration(timeAnimation, delay: timeAnimation, options: .CurveEaseInOut, animations: {
                            self.frame.origin.y = superFrame.height
                            }, completion: { (_) in
                                self.frame.origin.y = -self.frame.size.height
                                self.isAnimated = false
                        })
                    }
            })
            
        }
    }
    
    
}
