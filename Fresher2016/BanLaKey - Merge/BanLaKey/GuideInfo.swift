//
//  GuideInfo.swift
//  BanLaKey
//
//  Created by qhcthanh on 9/8/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import UIKit

class GuideInfo: NSObject {

    var title: String!
    var subTitle: String!
    var isHaveDetailButton: Bool = false
    var detailActionBlock: (() -> Void)?
    
    var guideFilePath: String!
    
    init(title: String, subTitle: String, guideFilePath: String, isHaveDetailButton: Bool = false, detailActionBlock: (() -> Void)? = nil) {
        self.title = title
        self.subTitle = subTitle
        self.guideFilePath = guideFilePath
        self.isHaveDetailButton = isHaveDetailButton
        self.detailActionBlock = detailActionBlock
    }
}
