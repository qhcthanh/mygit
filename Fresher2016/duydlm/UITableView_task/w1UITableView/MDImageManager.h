//
//  MDImageManager.h
//  w1UITableView
//
//  Created by manhduydl on 5/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Contact.h"

@interface MDImageManager : NSObject

+ (instancetype)sharedInstance;

/**
 *  Create avatar image for contact if it not exist and caching it
 *
 *  @param contact The contact need to create avatar
 *
 *  @return        An UIImage is avatar of contact 
 */
- (UIImage *)loadImageOfContact:(Contact *)contact;

/**
 *  Get avatar image of contact from cache if it exist
 *
 *  @param contact  The contact need to get avatar
 *
 *  @return         An UIImage is avatar of contact (or nil)
 */
- (UIImage *)getImageFromCache:(Contact *)contact;

@end
