//
//  UXTextTransitionDetail.m
//  testUIView
//
//  Created by VanDao on 6/28/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import "UXTextTransitionDetail.h"
#import "UXTextTransitionView.h"

@interface UXTextTransitionDetail(){
    NSString *transitionText;
    //UIColor *textColor;
    UIFont *textFont;
}

@end

@implementation UXTextTransitionDetail

- (id)initWithTransitionView:(id)view{
    self = [super initWithTransitionView:view];
    
    if (self){
        UXTextTransitionView* transitionTextView = (UXTextTransitionView*)view;
        transitionText = transitionTextView.textContent;
        //textColor = view.textColor;
        textFont = transitionTextView.textFont;
    }
    
    return self;
}

@end
