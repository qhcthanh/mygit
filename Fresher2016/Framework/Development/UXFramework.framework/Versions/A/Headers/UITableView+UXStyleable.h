/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class UXDOM;
@class UXCSSRuleset;

@interface UITableView (UXStyleable)

/**
 * Applies the given rule set to this table view.
 *
 * This method is exposed primarily for subclasses to use when implementing the
 * applyStyleWithRuleSet: method from UXStyleable.
 */
- (void)applyTableViewStyleWithRuleSet:(UXCSSRuleset *)ruleSet inDOM: (UXDOM*) dom;

@end
