//
//  ViewController.m
//  CameraGPUImage
//
//  Created by BaoNQ on 6/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    
    NSMutableArray *_filters;
    NSUInteger _curFilter;
    
    GPUImageVideoCamera *videoCamera;
    GPUImageOutput<GPUImageInput> *filter;
    GPUImageMovieWriter *movieWriter;
    
    
    GPUImageStillCamera *stillCamera;

}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    /*
    // Setup camera
    videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPreset640x480 cameraPosition:AVCaptureDevicePositionBack];
    
    videoCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
    videoCamera.horizontallyMirrorFrontFacingCamera = NO;
    videoCamera.horizontallyMirrorRearFacingCamera = NO;
    
    // Turn on Audio
    //videoCamera.audioEncodingTarget = movieWriter;

     
    filter = [[GPUImageFilter alloc] init];

    [videoCamera addTarget:filter];
    GPUImageView *filterView = (GPUImageView *)self.view;

    NSString *pathToMovie = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Movie.m4v"];
    unlink([pathToMovie UTF8String]); // If a file already exists, AVAssetWriter won't let you record new frames, so delete the old movie
    NSURL *movieURL = [NSURL fileURLWithPath:pathToMovie];
    movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:movieURL size:CGSizeMake(480.0, 640.0)];
    movieWriter.encodingLiveVideo = YES;

    
    [filter addTarget:movieWriter];
    [filter addTarget:filterView];
    
    [videoCamera startCameraCapture];
    */
    
    // Setup photo
    stillCamera = [[GPUImageStillCamera alloc] init];
    stillCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
    
    
    filter = [[GPUImageSepiaFilter alloc] init];
    [stillCamera addTarget:filter];
    GPUImageView *filterView = (GPUImageView *)self.view;
    [filter addTarget:filterView];
    
    [stillCamera startCameraCapture];

    /*
    // Save photo
    [stillCamera capturePhotoAsImageProcessedUpToFilter:filter withCompletionHandler:^(UIImage *processedImage, NSError *error){
        NSData *dataForJPEGFile = UIImageJPEGRepresentation(processedImage, 0.8);
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSError *error2 = nil;
        if (![dataForJPEGFile writeToFile:[documentsDirectory stringByAppendingPathComponent:@"FilteredPhoto.jpg"] options:NSAtomicWrite error:&error2])
        {
            return;
        }
    }];
    
    */
    /*
    // Do any additional setup after loading the view, typically from a nib.
    _filters = [[NSMutableArray alloc] init];
    
    [_filters addObject:[[GPUImageGrayscaleFilter alloc] init]];
    [_filters addObject:[[GPUImageSepiaFilter alloc] init]];
    [_filters addObject:[[GPUImageSketchFilter alloc] init]];
    [_filters addObject:[[GPUImagePixellateFilter alloc] init]];
    [_filters addObject:[[GPUImageColorInvertFilter alloc] init]];
    [_filters addObject:[[GPUImageToonFilter alloc] init]];
    [_filters addObject:[[GPUImagePinchDistortionFilter alloc] init]];
    [_filters addObject:[[GPUImageFilter alloc] init]];
    
    _curFilter = 0;

    UIImage *originalImage = [UIImage imageNamed:@"image.png"];
    //UIImage *filteredImage = [[_filters objectAtIndex:_curFilter] imageByFilteringImage:originalImage];
    UIImage *filteredImage = [[_filters objectAtIndex:_curFilter] imageByFilteringImage:originalImage];

    self.imageView.image = filteredImage;
    
    
    UISwipeGestureRecognizer *rightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandle:)];
    rightRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [rightRecognizer setNumberOfTouchesRequired:1];
    
    //add the your gestureRecognizer , where to detect the touch..
    [self.view addGestureRecognizer:rightRecognizer];
    
    UISwipeGestureRecognizer *leftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandle:)];
    leftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [leftRecognizer setNumberOfTouchesRequired:1];
    
    [self.view addGestureRecognizer:leftRecognizer];
     */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)rightSwipeHandle:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSLog(@"rightSwipeHandle");
    if (_curFilter > 0)
    {
        _curFilter--;
        UIImage *originalImage = [UIImage imageNamed:@"image.png"];
        UIImage *filteredImage = [[_filters objectAtIndex:_curFilter] imageByFilteringImage:originalImage];
        self.imageView.image = filteredImage;    }
}

- (void)leftSwipeHandle:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSLog(@"leftSwipeHandle");
    if (_curFilter < _filters.count - 1)
    {
        _curFilter++;
        UIImage *originalImage = [UIImage imageNamed:@"image.png"];
        UIImage *filteredImage = [[_filters objectAtIndex:_curFilter] imageByFilteringImage:originalImage];
        self.imageView.image = filteredImage;    }
}

@end
