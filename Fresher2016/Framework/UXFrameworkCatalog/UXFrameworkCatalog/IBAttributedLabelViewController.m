/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "IBAttributedLabelViewController.h"

//
// What's going on in this file:
//
// This controller creates its view from the AttributedLabelMashup xib file found adjacent to this
// controller in the Xcode project. UILabel views are added to the interface and UXAttributedLabel
// is set as the custom class. We then configure the attributed labels further in viewDidLoad.

@interface IBAttributedLabelViewController() <UXAttributedLabelDelegate>
@property (nonatomic, retain) IBOutlet UIScrollView* scrollView;
@property (nonatomic, retain) IBOutlet UXAttributedLabel* titleLabel;
@property (nonatomic, retain) IBOutlet UXAttributedLabel* label1;
@property (nonatomic, retain) IBOutlet UXAttributedLabel* label2;
@end

@implementation IBAttributedLabelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  if ((self = [super initWithNibName:@"AttributedLabelMashup" bundle:nil])) {
    self.titleString = @"Interface Builder";
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];

  _titleLabel.strokeWidth = -3.0;
  _titleLabel.strokeColor = [UIColor blackColor];

  // Kerning modifies the spacing between letters.
  _titleLabel.textKern = 15.0;

  _label1.textAlignment = NSTextAlignmentJustified;

  _label2.underlineStyle = kCTUnderlineStyleDouble;
  _label2.underlineStyleModifier = kCTUnderlinePatternDot;

  self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, CGRectGetMaxY(_label2.frame));
}

#pragma mark - UXAttributedLabelDelegate

- (void)attributedLabel:(UXAttributedLabel *)attributedLabel didSelectTextCheckingResult:(NSTextCheckingResult *)result atPoint:(CGPoint)point {
  UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Link Selected" message:result.URL.relativeString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
  [alert show];
}

@end
