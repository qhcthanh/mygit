//
//  ViewController11.m
//  CameraGPUImage
//
//  Created by BaoNQ on 6/24/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "HorizontalPageViewController.h"
#import "SNTextField.h"
#import "SNTextField+KeyboardEvent.h"
#import "SNFilter.h"
#import "SNSticker.h"

@interface HorizontalPageViewController () <UIGestureRecognizerDelegate>

@end

@implementation HorizontalPageViewController {
    float _scaledRatio;
    CGPoint _initialPoint;
    NSMutableArray *_images;
    
    HorizontalPageView *sliderView;
    SNTextField *_textField;
    UITapGestureRecognizer *_tapGesture;
    NSMutableArray<SNFilter *> *_data;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.view.backgroundColor = [UIColor whiteColor];
    CGSize mainScreen = [UIScreen mainScreen].bounds.size;
    _textField = [[SNTextField alloc] initWithWidth:mainScreen.width yPosition:mainScreen.height/2 screenHeight:mainScreen.height];

    _tapGesture = [[UITapGestureRecognizer alloc] init];
    _data = [[NSMutableArray alloc] init];

    
    [_tapGesture addTarget:self action:@selector(handleTap1)];

    // Setup
    [self createData:[UIImage imageNamed:@"pic"]];
    
    
    sliderView = [[HorizontalPageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    sliderView.dataSource = self;
    
//    //Create data
//    _images = [[NSMutableArray alloc] init];
//    
//    // [_images addObject:[UIImage imageNamed:@"image1.png"]];
//    for (int i = 0; i < 3; ++i) {
//        NSString *name = [NSString stringWithFormat:@"image%i.png", i];
//        [_images addObject:[UIImage imageNamed:name]];
//    }
//    // [_images addObject:[UIImage imageNamed:@"image0.png"]];
//    
//    if (_images.count > 1) {
//        [_images insertObject:_images[_images.count - 1] atIndex:0];
//        [_images addObject:_images[1]];
//    }
    
    sliderView.userInteractionEnabled = YES;
    sliderView.multipleTouchEnabled = YES;
    sliderView.exclusiveTouch = NO;
    
    [self.view addSubview:sliderView];
    
    [sliderView reloadData];
    [self setupTextField];

}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:_textField];
}

- (void)setupTextField {
    [self.view addSubview:_textField];
    
    _tapGesture.delegate = self;
    [sliderView addGestureRecognizer:_tapGesture];
    
    [[NSNotificationCenter defaultCenter] addObserver:_textField selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:_textField selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:_textField selector:@selector(keyboardTypeChanged:) name:UIKeyboardDidShowNotification object:nil];
}

- (void)createData:(UIImage *)image {
    _data = [SNFilter generateFilters:image filterNames:[SNFilter filterNameList]];
    _data[0].image = [UIImage imageNamed:@"image0"];
    _data[1].image = [UIImage imageNamed:@"image1"];
    _data[2].image = [UIImage imageNamed:@"image2"];
    _data[3].image = [UIImage imageNamed:@"image3"];
    _data[4].image = [UIImage imageNamed:@"image4"];
    _data[5].image = [UIImage imageNamed:@"image5"];
    _data[6].image = [UIImage imageNamed:@"image6"];
    _data[7].image = [UIImage imageNamed:@"image7"];
    _data[8].image = [UIImage imageNamed:@"image8"];

    [_data[0] addSticker:[[SNSticker alloc] initWithFrame:CGRectMake(195, 30, 90, 90) image:[UIImage imageNamed:@"stick2"] atZPosition:0]];
    [_data[2] addSticker:[[SNSticker alloc] initWithFrame:CGRectMake(30, 100, 250, 250) image:[UIImage imageNamed:@"stick3"] atZPosition:0]];
    [_data[8] addSticker:[[SNSticker alloc] initWithFrame:CGRectMake(20, 0, 140, 140) image:[UIImage imageNamed:@"stick1"] atZPosition:0]];
    
    // Add the last image to first index and the first image to the last index to simulate the circle scrolling animation.
    // Ex: we have 4 images: 0, 1, 2, 3, 4
    // After these below statements we will have: [4, 0, 1, 2, 3, 4, 0].
    // And now, in this array, we have 2 "image 4" and 2 "image0".
    // When user scroll to the 2nd "image0", i will change the position of 3 pages to the position of the 1st "image0".
    
    if (_data.count > 1) {
        [_data addObject:[_data[0] copy]];
        [_data insertObject:[_data[_data.count - 2] copy] atIndex:0];
    }
}

//MARK: - HorizontalPageView DataSource
- (NSInteger)startAtIndex:(HorizontalPageView *)horizontalPageView {
    if (_data.count == 1)
        return 0;
    return 1;
}

- (NSInteger)numberOfImages:(HorizontalPageView *)horizontalPageView {
    //return _images.count;
    return _data.count;
}

- (SNFilter *)imageInHorizontalPageView:(HorizontalPageView *)horizontalPageView atIndex:(NSInteger)index {
    //return _images[index];
    return _data[index];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//MARK: - Extension Gesture Recognizer Delegate and touch Handler for TextField

- (void)handleTap1 {
    [_textField handleTap];
}

@end
