//
//  CTPhoneNumberTableViewCell.h
//  ZaloContactInviting
//
//  Created by Nguyen Van Dao on 6/4/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTPhoneNumberCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextField *phoneNumberTextField;

@end
