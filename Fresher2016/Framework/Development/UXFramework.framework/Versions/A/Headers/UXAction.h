/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class UXURLAction;
/**
 * @param object An action was performed on this object.
 * @param target The target that was attached to the UXActions instance.
 * @param indexPath The index path of the object.
 */
typedef BOOL (^UXActionBlock)(id object, id target, NSIndexPath* indexPath);
typedef void (^UXCompletionBlock)(UINavigationController *navigationController, UIViewController *viewController);
/**
 * The UXAction class provides a generic interface for attaching actions to objects.
 *
 * UXAction are used to implement user interaction in UITableViews and UICollectionViews via the
 * corresponding classes (UXTableViewActions and NICollectionViewActions) in the respective
 * feature. UXAction separates the necessity
 *
 * <h3>Types of Actions</h3>
 *
 * The three primary types of actions are:
 *
 * - buttons,
 * - detail views,
 * - and pushing a new controller onto the navigation controller.
 *
 * <h3>Attaching Actions</h3>
 *
 * Actions may be attached to specific instances of objects or to entire classes of objects. When
 * an action is attached to both a class of object and an instance of that class, only the instance
 * action should be executed.
 *
 * All attachment methods return the object that was provided. This makes it simple to attach
 * actions within an array creation statement.
 *
 * Actions come in two forms: blocks and selector invocations. Both can be attached to an object
 * for each type of action and both will be executed, with the block being executed first. Blocks
 * should be used for simple executions while selectors should be used when the action is complex.
 *
 * The following is an example of using UXTableViewActions:
 *
 @code
 NSArray *objects = @[
 [UXTitleCellObject objectWithTitle:@"Implicit tap handler"],
 [self.actions attachToObject:[UXTitleCellObject objectWithTitle:@"Explicit tap handler"]
 tapBlock:
 ^BOOL(id object, id target) {
 UXDWARNING(@"Object was tapped with an explicit action: %@", object);
 }]
 ];
 
 [self.actions attachToClass:[UXTitleCellObject class]
 tapBlock:
 ^BOOL(id object, id target) {
 UXDWARNING(@"Object was tapped: %@", object);
 }];
 @endcode
 *
 */
@interface UXAction : NSObject

// Designated initializer.
/**
 * Initializes a newly allocated table view actions object with the given controller.
 *
 * @attention This method is deprecated. Use the new method
 *            @link UXActions::initWithTarget: initWithTarget:@endlink.
 *
 * The controller is stored as a weak reference internally.
 *
 * @param controller The controller that will be used in action blocks.
 * @fn UXActions::initWithController:
 */
- (id)initWithTarget:(id)target;

#pragma mark Mapping Objects
/**
 * Attaches a tap action to the given object.
 *
 * A cell with an attached tap action will have its selectionStyle set to
 * @c tableViewCellSelectionStyle when the cell is displayed.
 *
 * The action will be executed when the object's corresponding cell is tapped. The object argument
 * of the block will be the object to which this action was attached. The target argument of the
 * block will be this receiver's @c target.
 *
 * Return NO if the tap action is used to present a modal view controller. This provides a visual
 * reminder to the user when the modal controller is dismissed as to which cell was tapped to invoke
 * the modal controller.
 *
 * The tap action will be invoked first, followed by the navigation action if one is attached.
 *
 * @param object The object to attach the action to. This object must be contained within
 *                    an UXTableViewDataSource.
 * @param action The tap action block.
 * @returns The object that you attached this action to.
 * @fn UXActions::attachToObject:tapBlock:
 * @sa UXActions::attachToObject:tapSelector:
 */
- (id)attachToObject:(id<NSObject>)object tapBlock:(UXActionBlock)action;

/**
 * Attaches a detail action to the given object.
 *
 * When a cell with a detail action is displayed, its accessoryType will be set to
 * UITableViewCellAccessoryDetailDisclosureButton.
 *
 * When a cell's detail button is tapped, the detail action block will be executed. The return
 * value of the block is ignored.
 *
 * @param object The object to attach the action to. This object must be contained within
 *                    an UXTableViewDataSource.
 * @param action The detail action block.
 * @returns The object that you attached this action to.
 * @fn UXActions::attachToObject:detailBlock:
 */
- (id)attachToObject:(id<NSObject>)object detailBlock:(UXActionBlock)action;

/**
 * Attaches a navigation action to the given object.
 *
 * When a cell with a navigation action is displayed, its accessoryType will be set to
 * UITableViewCellAccessoryDisclosureIndicator if there is no detail action, otherwise the
 * detail disclosure indicator takes precedence.
 *
 * When a cell with a navigation action is tapped the navigation block will be executed.
 *
 * If a tap action also exists for this object then the tap action will be executed first, followed
 * by the navigation action.
 *
 * @param object The object to attach the action to. This object must be contained within
 *                    an UXTableViewDataSource.
 * @param action The navigation action block.
 * @returns The object that you attached this action to.
 * @fn UXActions::attachToObject:navigationBlock:
 */
- (id)attachToObject:(id<NSObject>)object navigationBlock:(UXActionBlock)action;

- (id)attachToObject:(id<NSObject>)object tapSelector:(SEL)selector;
- (id)attachToObject:(id<NSObject>)object detailSelector:(SEL)selector;
- (id)attachToObject:(id<NSObject>)object navigationSelector:(SEL)selector;

#pragma mark Mapping Classes

- (void)attachToClass:(Class)aClass tapBlock:(UXActionBlock)action;
- (void)attachToClass:(Class)aClass detailBlock:(UXActionBlock)action;
- (void)attachToClass:(Class)aClass navigationBlock:(UXActionBlock)action;

- (void)attachToClass:(Class)aClass tapSelector:(SEL)selector;
- (void)attachToClass:(Class)aClass detailSelector:(SEL)selector;
- (void)attachToClass:(Class)aClass navigationSelector:(SEL)selector;

#pragma mark Object State

- (BOOL)isObjectActionable:(id<NSObject>)object;
- (void)removeAllActions;

+ (id)objectFromKeyClass:(Class)keyClass map:(NSMutableDictionary *)map;

@end

#if defined __cplusplus
extern "C" {
#endif
    
    /**
     * Returns a block that pushes an instance of the controllerClass onto the navigation stack.
     *
     * Allocates an instance of the controller class and calls the init selector.
     *
     * The target property of the UXActions instance must be an instance of UIViewController
     * with an attached navigationController.
     *
     * @param controllerClass The class of controller to instantiate.
     */
    UXActionBlock UXPushViewControllerAction(Class controllerClass, UXCompletionBlock completionHandler);
    UXActionBlock UXPresentViewControllerAction(Class controllerClass, UXCompletionBlock completionHandler);
    
#if defined __cplusplus
};
#endif

