/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXOperation.h"
#import "UXCSSParser.h"

@class UXCSSStyleSheet;
@class UXStylesheetCache;

extern NSString* const UXJSONDidChangeNotification;
extern NSString* const UXJSONDidChangeFilePathKey;
extern NSString* const UXJSONDidChangeNameKey;

/**
 * An observer for the Chameleon server.
 *
 * @ingroup UXCSS
 *
 * This observer connects to a Chameleon server and waits for changes in stylesheets. Once
 * a stylesheet change has been detected, the new stylesheet is retrieved from the server
 * and a notification is fired via UXStylesheetDidChangeNotification after the stylesheet
 * has been reloaded.
 *
 * Thanks to the use of NIOperations, the stylesheet loading and processing is accomplished
 * on a separate thread. This means that the UI will only be notified of stylesheet changes
 * once the request thread has successfully loaded and processed the changed stylesheet.
 */
@interface UXChameleonObserver : NSObject <UXOperationDelegate, UXCSSParserDelegate> {
@private
    UXStylesheetCache* _stylesheetCache;
    NSMutableArray* _stylesheetPaths;
    NSOperationQueue* _queue;
    NSString* _host;
}

/**
 * Initializes a newly allocated Chameleon observer with a given stylesheet cache and host.
 *
 * @fn UXChameleonObserver::initWithStylesheetCache:host:
 */
- (id)initWithStylesheetCache:(UXStylesheetCache *)stylesheetCache host:(NSString *)host;

/**
 * Returns a loaded stylesheet from the given path.
 *
 * @fn UXChameleonObserver::stylesheetForPath:
 */
- (UXCSSStyleSheet *)stylesheetForPath:(NSString *)path;

/**
 * Begins listening to the Chameleon server for changes.
 *
 * When changes are detected the Chameleon observer downloads the new CSS files, reloads them,
 * and then fires the appropriate notifications.
 *
 * @fn UXChameleonObserver::watchSkinChanges
 */
- (void)watchSkinChanges;

/**
 * Browses Bonjour for services with the given name (e.g. your username) and sets the host
 * automatically.
 *
 * @fn UXChameleonObserver::enableBonjourDiscovery:
 */
- (void)enableBonjourDiscovery: (NSString*) serviceName;

@end
