//
//  EmojiCollectionViewCell.swift
//  EmojiKeyboard
//
//  Created by Quach Ha Chan Thanh on 8/14/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import UIKit

let imageViewDetailTag = 1001

private var fontFitSizeDictionary = Dictionary<CGFloat, UIFont>()

protocol EmojiCollectionViewCellDelegate: NSObjectProtocol {
    func didSelectEmoji(emoji: String)
}

public class BLEmojiCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var emojiLabel: UILabel!
    
    weak public var itemProtocol: SimpleModelProtocol?
    weak var delegate: EmojiCollectionViewCellDelegate?
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    public func bindingUI(itemProtocol: SimpleModelProtocol) {

        self.itemProtocol = itemProtocol
        
        emojiLabel.text = itemProtocol.titleContent
        
        if let font = fontFitSizeDictionary[self.bounds.width] {
            emojiLabel.font = font
        } else {
            let font = emojiLabel.fitFontForSize(self.bounds.size)
            emojiLabel.font = font
            fontFitSizeDictionary[self.bounds.size.width] = font
        }
        
    }
    
    override public func prepareForReuse() {
        super.prepareForReuse()
        
        // Clean UI
        emojiLabel.text = ""
        self.viewWithTag(imageViewDetailTag)?.removeFromSuperview()
        
        itemProtocol = nil
        
    }
    
    func onTapEmojiGesture(gesture: UILongPressGestureRecognizer) {
        
        if let view = gesture.view,
            let emojiCustomLabel = view as? UILabel {
            
            if gesture.state == .Began {
                emojiCustomLabel.backgroundColor = BLThemeManager.currentTheme.characterButtonHightlightedColor
            } else if gesture.state != . Changed {
                if let emoji = emojiCustomLabel.text, let delegate = self.delegate {
                    delegate.didSelectEmoji(emoji)

                    emojiCustomLabel.backgroundColor = .clearColor()
                }
            }
        }
    }
    
    
    
}






