/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "AFURLResponseSerialization.h"
#import "AFURLRequestSerialization.h"
#import "AFSecurityPolicy.h"

#import "UXMutableURLRequest.h"

@interface UXHTTPNetworkManager : NSObject

@property (nonatomic, strong) NSMutableArray* requestMutableArray;
@property (nonatomic, readonly) NSMutableDictionary* agentDictionary;

/**
 *  Get an instance of network manager
 *
 *  @return A HTTPNetworkManager object
 */
+ (UXHTTPNetworkManager*)manager;

/**
 *  Apply HTTP header field to an URLRequest object
 *
 *  @param request The request is applied
 */
- (void)applyHttpHeadersForRequest:(NSMutableURLRequest*)request;

/**
 Excute a UXMutableURLRequest with a `GET` request.
 
 @param request The request will be excuted.
 @param success A block object to be executed when the request operation finishes successfully. This block has no return value and takes two arguments: the request operation, and the response object created by the client response serializer.
 @param failure A block object to be executed when the request operation finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data. This block has no return value and takes a two arguments: the request operation and the error describing the network or parsing error that occurred.
 */
+ (void)GET:(UXMutableURLRequest*)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

/**
 Excute a UXMutableURLRequest with a `GET` request.
 
 @param request The request will be excuted.
 @param success A block object to be executed when the request operation finishes successfully. This block has no return value and takes two arguments: the request operation, and the response object created by the client response serializer.
 @param failure A block object to be executed when the request operation finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data. This block has no return value and takes a two arguments: the request operation and the error describing the network or parsing error that occurred.
 @param dispatchQueue A dispatch queue for the `completionBlock` of request operations. If `NULL` (default), the main queue is used.
 @param dispatchGroup A dispatch group for the `completionBlock` of request operations. If `NULL` (default), a private dispatch group is used.
 */
+ (void)GET:(UXMutableURLRequest*)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure completionQueue:(dispatch_queue_t) dispatchQueue completionGroup:(dispatch_group_t)dispatchGroup;

/**
 Excute a UXMutableURLRequest with a `POST` request.
 
 @param request The request will be excuted.
 @param success A block object to be executed when the request operation finishes successfully. This block has no return value and takes two arguments: the request operation, and the response object created by the client response serializer.
 @param failure A block object to be executed when the request operation finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data. This block has no return value and takes a two arguments: the request operation and the error describing the network or parsing error that occurred.
 */
+ (void)POST:(UXMutableURLRequest*)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

/**
 Excute a UXMutableURLRequest with a `POST` request.
 
 @param request The request will be excuted.
 @param success A block object to be executed when the request operation finishes successfully. This block has no return value and takes two arguments: the request operation, and the response object created by the client response serializer.
 @param failure A block object to be executed when the request operation finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data. This block has no return value and takes a two arguments: the request operation and the error describing the network or parsing error that occurred.
 @param dispatchQueue A dispatch queue for the `completionBlock` of request operations. If `NULL` (default), the main queue is used.
 @param dispatchGroup A dispatch group for the `completionBlock` of request operations. If `NULL` (default), a private dispatch group is used.
 */
+ (void)POST:(UXMutableURLRequest*)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure completionQueue:(dispatch_queue_t) dispatchQueue completionGroup:(dispatch_group_t)dispatchGroup;

/**
 Excute a UXMutableURLRequest with a `PUT` request.
 
 @param request The request will be excuted.
 @param success A block object to be executed when the request operation finishes successfully. This block has no return value and takes two arguments: the request operation, and the response object created by the client response serializer.
 @param failure A block object to be executed when the request operation finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data. This block has no return value and takes a two arguments: the request operation and the error describing the network or parsing error that occurred.

 */
+ (void)PUT:(UXMutableURLRequest*)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

/**
 Excute a UXMutableURLRequest with a `PUT` request.
 
 @param request The request will be excuted.
 @param success A block object to be executed when the request operation finishes successfully. This block has no return value and takes two arguments: the request operation, and the response object created by the client response serializer.
 @param failure A block object to be executed when the request operation finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data. This block has no return value and takes a two arguments: the request operation and the error describing the network or parsing error that occurred.
 @param dispatchQueue A dispatch queue for the `completionBlock` of request operations. If `NULL` (default), the main queue is used.
 @param dispatchGroup A dispatch group for the `completionBlock` of request operations. If `NULL` (default), a private dispatch group is used.
 */
+ (void)PUT:(UXMutableURLRequest*)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure completionQueue:(dispatch_queue_t) dispatchQueue completionGroup:(dispatch_group_t)dispatchGroup;

/**
 Excute a UXMutableURLRequest with a `DELETE` request.
 
 @param request The request will be excuted.
 @param success A block object to be executed when the request operation finishes successfully. This block has no return value and takes two arguments: the request operation, and the response object created by the client response serializer.
 @param failure A block object to be executed when the request operation finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data. This block has no return value and takes a two arguments: the request operation and the error describing the network or parsing error that occurred.
 */
+ (void)DELETE:(UXMutableURLRequest*)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

/**
 Excute a UXMutableURLRequest with a `DELETE` request.
 
 @param request The request will be excuted.
 @param success A block object to be executed when the request operation finishes successfully. This block has no return value and takes two arguments: the request operation, and the response object created by the client response serializer.
 @param failure A block object to be executed when the request operation finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data. This block has no return value and takes a two arguments: the request operation and the error describing the network or parsing error that occurred.
 @param dispatchQueue A dispatch queue for the `completionBlock` of request operations. If `NULL` (default), the main queue is used.
 @param dispatchGroup A dispatch group for the `completionBlock` of request operations. If `NULL` (default), a private dispatch group is used.
 */
+ (void)DELETE:(UXMutableURLRequest*)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure completionQueue:(dispatch_queue_t) dispatchQueue completionGroup:(dispatch_group_t)dispatchGroup;

/**
 Creates and runs a `NSURLSession` with a multipart `POST` request.
 
 @param URLString The URL string used to create the request URL.
 @param parameters The parameters to be encoded according to the client request serializer.
 @param block A block that takes a single argument and appends data to the HTTP body. The block argument is an object adopting the `AFMultipartFormData` protocol.
 @param success A block object to be executed when the request operation finishes successfully. This block has no return value and takes two arguments: the request operation, and the response object created by the client response serializer.
 @param failure A block object to be executed when the request operation finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data. This block has no return value and takes a two arguments: the request operation and the error describing the network or parsing error that occurred.
 
 @see -HTTPRequestOperationWithRequest:success:failure:
 */
+ (void)POST:(NSString *)URLString
  parameters:(id)parameters
constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
     success:(void (^)(id responseObject))success
     failure:(void (^)(NSError *error))failure;

@end
