//
//  BLKey.swift
//  BanLaKey
//
//  Created by BaoNQ on 8/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit


// popup constraints have to be setup with the topmost view in mind; hence these callbacks
protocol KeyboardKeyProtocol: class {
    func frameForPopup(key: BLKey, direction: Direction) -> CGRect
    func willShowPopup(key: BLKey, direction: Direction) //may be called multiple times during layout
    func willHidePopup(key: BLKey)
}

enum BLKeyType: Int {
    case CharacterKey = 0
    case SpecialKey = 1
}

class BLKey: UIControl {
    
    weak var delegate: KeyboardKeyProtocol?

    var background: BLKeyBackground
    var popup: BLKeyBackground?
    var connector: BLKeyConnector?
    
    var displayView: ShapeView
    var borderView: ShapeView?
    var underView: ShapeView?
    
    var shadowView: UIView
    var shadowLayer: CALayer
    
    var popupDirection: Direction?

    
    private lazy var label: UILabel = {
        var temporaryLabel = UILabel()
        
        temporaryLabel.textAlignment = NSTextAlignment.Center
        temporaryLabel.baselineAdjustment = UIBaselineAdjustment.AlignCenters
        temporaryLabel.adjustsFontSizeToFitWidth = true
        temporaryLabel.minimumScaleFactor = CGFloat(0.1)
        temporaryLabel.userInteractionEnabled = false
        temporaryLabel.numberOfLines = 1
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
            if (UIScreen.mainScreen().interfaceOrientation() == BLInterfaceOrientation.Landscape) {
                temporaryLabel.font = UIFont.init(name: BLThemeManager.currentTheme.buttonTitleFont, size: BLThemeManager.currentTheme.buttonPadLandscapeFontSize)
            }
            else {
                temporaryLabel.font = UIFont.init(name: BLThemeManager.currentTheme.buttonTitleFont, size: BLThemeManager.currentTheme.buttonPadPortraitFontSize)
            }
        }
        else {
            if (UIScreen.mainScreen().interfaceOrientation() == BLInterfaceOrientation.Landscape) {
                temporaryLabel.font = UIFont.init(name: BLThemeManager.currentTheme.buttonTitleFont, size: BLThemeManager.currentTheme.buttonPhoneLandscapeFontSize)
            }
            else {
                temporaryLabel.font = UIFont.init(name: BLThemeManager.currentTheme.buttonTitleFont, size: BLThemeManager.currentTheme.buttonPhonePortraitFontSize)
            }
        }
        temporaryLabel.lineBreakMode = NSLineBreakMode.ByTruncatingMiddle
        
        self.addSubview(temporaryLabel)
        return temporaryLabel
    }()
    
    private lazy var imageView: UIImageView = {
        var temporaryImageView = UIImageView.init()
        
        temporaryImageView.contentMode = UIViewContentMode.Center
        temporaryImageView.sizeToFit()
        
        // Set button image? Maybe!?
        
        self.addSubview(temporaryImageView)
        return temporaryImageView
    }()
    
    func getImage() -> UIImage? {
        return self.imageView.image
    }
    
    func setImage(newImage: UIImage?) {
        self.imageView.image = newImage
        //self.updateState()
    }
    
    var keyType: BLKeyType
    
    var color: UIColor
    
    func getTitle() -> String? {
        return self.label.text
    }
    
    func setTitle(newTitle: String?) {

        self.label.text = newTitle
       // self.updateState()
    }
    
    init(frame: CGRect, title: String, image: UIImage? = nil, keyType: BLKeyType = .CharacterKey, backgroundColor: UIColor = UIColor.whiteColor()) {
        self.color = backgroundColor
        self.keyType = keyType
    
        let cornerRadius: CGFloat
        let shadowWidth: CGFloat
        let borderWidth: CGFloat
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
            cornerRadius = BLThemeManager.currentTheme.buttonPadCornerRadius
            shadowWidth = BLThemeManager.currentTheme.buttonPadShadowWidth
            borderWidth = BLThemeManager.currentTheme.buttonPadBorderWidth
        }
        else {
            cornerRadius = BLThemeManager.currentTheme.buttonPhoneCornerRadius
            shadowWidth = BLThemeManager.currentTheme.buttonPhoneShadowWidth
            borderWidth = BLThemeManager.currentTheme.buttonPhoneBorderWidth
        }
        
        self.background = BLKeyBackground.init(cornerRadius: cornerRadius, underOffset: shadowWidth)
        
        self.displayView = ShapeView()
        self.underView = ShapeView()
        self.borderView = ShapeView()
        
        self.shadowLayer = CAShapeLayer()
        self.shadowView = UIView()
        
        self.popupDirection = nil
        
        
        super.init(frame: frame)
        
        self.addSubview(self.shadowView)
        self.shadowView.layer.addSublayer(self.shadowLayer)
        
        self.addSubview(self.displayView)
        if let underView = self.underView {
            self.addSubview(underView)
        }
        
        if let borderView = self.borderView {
            self.addSubview(borderView)
        }
        
        
        self.displayView.opaque = false
        self.underView?.opaque = false
        self.borderView?.opaque = false
        
        self.shadowLayer.shadowOpacity = Float(0.2)
        self.shadowLayer.shadowRadius = 4
        self.shadowLayer.shadowOffset = CGSizeMake(0, 3)
        
        self.borderView?.lineWidth = CGFloat(borderWidth)
        self.borderView?.fillColor = UIColor.clearColor()
        
        
        self.backgroundColor = UIColor.clearColor()
        self.setTitle(title)
        self.setImage(image)
    }
    
    func layoutPopupIfNeeded() {
        if self.popup != nil && self.popupDirection == nil {
            self.shadowView.hidden = false
            self.borderView?.hidden = false
            
            self.popupDirection = Direction.Up
            
            self.layoutPopup(self.popupDirection!)
            self.configurePopup(self.popupDirection!)
            
            self.delegate?.willShowPopup(self, direction: self.popupDirection!)
        }
        else {
            self.shadowView.hidden = true
            self.borderView?.hidden = true
        }
    }
    func layoutPopup(dir: Direction) {
        assert(self.popup != nil, "popup not found")
        
        if let popup = self.popup {
            if let delegate = self.delegate {
                let frame = delegate.frameForPopup(self, direction: dir)
                popup.frame = frame
                self.label.frame = popup.bounds
            }
            else {
                popup.frame = CGRectZero
                popup.center = self.center
            }
        }
    }
    
    func configurePopup(direction: Direction) {
        assert(self.popup != nil, "popup not found")
        
        self.background.attach(direction)
        self.popup!.attach(direction.opposite())
        
        let kv = self.background
        let p = self.popup!
        
        let cornerRadius: CGFloat
        let shadowWidth: CGFloat
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
            cornerRadius = BLThemeManager.currentTheme.buttonPadCornerRadius
            shadowWidth = BLThemeManager.currentTheme.buttonPadShadowWidth
        }
        else {
            cornerRadius = BLThemeManager.currentTheme.buttonPhoneCornerRadius
            shadowWidth = BLThemeManager.currentTheme.buttonPhoneShadowWidth
        }
        
        self.connector?.removeFromSuperview()
        self.connector = BLKeyConnector(cornerRadius: cornerRadius, underOffset: shadowWidth, start: kv, end: p, startConnectable: kv, endConnectable: p, startDirection: direction, endDirection: direction.opposite())
        self.connector!.layer.zPosition = -1
        self.addSubview(self.connector!)
    }

    func showPopup() {
        if self.popup == nil {
            self.layer.zPosition = 1000
            
            let cornerRadius: CGFloat
            let shadowWidth: CGFloat
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
                cornerRadius = BLThemeManager.currentTheme.buttonPadPopupCornerRadius
                shadowWidth = BLThemeManager.currentTheme.buttonPadShadowWidth
            }
            else {
                cornerRadius = BLThemeManager.currentTheme.buttonPhonePopupCornerRadius
                shadowWidth = BLThemeManager.currentTheme.buttonPhoneShadowWidth
            }
            
            let popup = BLKeyBackground(cornerRadius: cornerRadius, underOffset: shadowWidth)
            self.popup = popup
            self.addSubview(popup)
            
            let popupLabel = UILabel()
            popupLabel.textAlignment = self.label.textAlignment
            popupLabel.baselineAdjustment = self.label.baselineAdjustment
            popupLabel.font = self.label.font.fontWithSize(22 * 2)
            popupLabel.adjustsFontSizeToFitWidth = self.label.adjustsFontSizeToFitWidth
            popupLabel.minimumScaleFactor = CGFloat(0.1)
            popupLabel.userInteractionEnabled = false
            popupLabel.numberOfLines = 1
            popupLabel.frame = popup.bounds
            popupLabel.text = self.label.text
            popup.addSubview(popupLabel)
            self.label = popupLabel
            
            self.label.hidden = true
        }
    }
    
    func updateState() {
        // Update state
        self.setNeedsDisplay()
    }
    
    override var selected: Bool {
//        get {
//            self.updateState()
//            return self.selected
//        }
        willSet {
            self.updateState()
        }
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        var shadowColor: UIColor
        
        if (self.keyType == BLKeyType.CharacterKey) {
            shadowColor = BLThemeManager.currentTheme.characterButtonShadowColor
        }
        else{
            shadowColor = BLThemeManager.currentTheme.specialButtonShadowColor
        }
        
        
        var backgroundColor: UIColor
        
        switch self.state {
        case UIControlState.Highlighted:
            if self.keyType == BLKeyType.SpecialKey {
                if (BLThemeManager.currentTheme.useSameColorForSpecialButtonHighlighted) {
                    backgroundColor = self.color
                }
                else {
                    backgroundColor = BLThemeManager.currentTheme.specialButtonHighlightedColor
                }
                self.label.textColor = BLThemeManager.currentTheme.specialButtonHighLightedTitleColor
            }
            else {
                if (BLThemeManager.currentTheme.useSameColorForCharacterButtonHighlighted) {
                    backgroundColor = self.color
                }
                else {
                    backgroundColor = BLThemeManager.currentTheme.characterButtonHightlightedColor
                }
                self.label.textColor = BLThemeManager.currentTheme.characterButtonHighLightedTitleColor
            }
            
            break
        default:
            if self.keyType == BLKeyType.SpecialKey {
                self.label.textColor = BLThemeManager.currentTheme.specialButtonTitleColor
            }
            else {
                self.label.textColor = BLThemeManager.currentTheme.characterButtonTitleColor
            }
            backgroundColor = self.color
            
            break
        }
        
        self.drawKeyRect(rect, color: backgroundColor, shadowColor: shadowColor)
    }
    
    func drawKeyRect(rect: CGRect, color: UIColor, shadowColor: UIColor) {
        let shadowRect: CGRect = CGRectOffset(CGRectInset(rect, 0, BLThemeManager.currentTheme.buttonShadowYOffset), 0, BLThemeManager.currentTheme.buttonShadowYOffset)
        let shadowPath: UIBezierPath = UIBezierPath.init()
        
        let cornerRadius: CGFloat
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
            cornerRadius = BLThemeManager.currentTheme.buttonPadCornerRadius
        }
        else {
            cornerRadius = BLThemeManager.currentTheme.buttonPhoneCornerRadius
        }
        
        // Bottom-left 1
        shadowPath.moveToPoint(CGPointMake(0.0, shadowRect.size.height - cornerRadius))
        // Bottom - left 2
        shadowPath.addCurveToPoint(CGPointMake(cornerRadius, shadowRect.size.height),
                                   controlPoint1: CGPointMake(0.0, shadowRect.size.height - cornerRadius/2),
                                   controlPoint2: CGPointMake(cornerRadius/2, shadowRect.size.height))
        
        // Bottom-right 1
        shadowPath.addLineToPoint(CGPointMake(shadowRect.size.width - cornerRadius, shadowRect.size.height))
        // Bottom-right 2
        shadowPath.addCurveToPoint(CGPointMake(shadowRect.size.width, shadowRect.size.height - cornerRadius),
                                   controlPoint1: CGPointMake(shadowRect.size.width - cornerRadius/2, shadowRect.size.height),
                                   controlPoint2: CGPointMake(shadowRect.size.width, shadowRect.size.height - cornerRadius/2))
        
        // Top-right 1
        shadowPath.addLineToPoint(CGPointMake(shadowRect.size.width, shadowRect.size.height - cornerRadius - BLThemeManager.currentTheme.buttonShadowYOffset))
        // Top-right 2
        shadowPath.addCurveToPoint(CGPointMake(shadowRect.size.width - cornerRadius, shadowRect.size.height - BLThemeManager.currentTheme.buttonShadowYOffset),
                                   controlPoint1: CGPointMake(shadowRect.size.width, shadowRect.size.height - BLThemeManager.currentTheme.buttonShadowYOffset - cornerRadius/2),
                                   controlPoint2: CGPointMake(shadowRect.size.width - cornerRadius/2, shadowRect.size.height - BLThemeManager.currentTheme.buttonShadowYOffset))
        
        // Top-left 1
        shadowPath.addLineToPoint(CGPointMake(cornerRadius, shadowRect.size.height - BLThemeManager.currentTheme.buttonShadowYOffset))
        // Top-left 2
        shadowPath.addCurveToPoint(CGPointMake(0.0, shadowRect.size.height - cornerRadius - BLThemeManager.currentTheme.buttonShadowYOffset),
                                   controlPoint1: CGPointMake(cornerRadius/2, shadowRect.size.height - BLThemeManager.currentTheme.buttonShadowYOffset),
                                   controlPoint2: CGPointMake(0.0, shadowRect.size.height - cornerRadius/2 - BLThemeManager.currentTheme.buttonShadowYOffset))
        
        // Bottom-left 1
        shadowPath.addLineToPoint(CGPointMake(0.0, shadowRect.size.height - cornerRadius))
        
        shadowPath.closePath()
        shadowPath.applyTransform(CGAffineTransformMakeTranslation(0, BLThemeManager.currentTheme.buttonShadowYOffset * 2))
        shadowColor.setFill()
        shadowPath.fill()
        
        let keyRect: CGRect = CGRectOffset(CGRectInset(rect, 0, BLThemeManager.currentTheme.buttonShadowYOffset/2), 0, -BLThemeManager.currentTheme.buttonShadowYOffset/2)
        let keyPath: UIBezierPath = UIBezierPath.init(roundedRect: keyRect, cornerRadius: cornerRadius)
        
        color.setFill()
        keyPath.fill()
    }
    
    override func systemLayoutSizeFittingSize(targetSize: CGSize) -> CGSize {
        return UILayoutFittingExpandedSize
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.label.frame = CGRectOffset(self.bounds, 0, BLThemeManager.currentTheme.buttonLabelYOffset)
        
        let imageRect: CGRect = CGRectMake(self.bounds.origin.x + self.bounds.size.width / 4, self.bounds.origin.y + self.bounds.size.height/4, self.bounds.size.width/2 , self.bounds.size.height/2)
        
        self.imageView.frame = CGRectOffset(imageRect, 0, BLThemeManager.currentTheme.buttonImageYOffset)
        self.imageView.contentMode = .ScaleAspectFit
        
        self.setNeedsDisplay()
    }
    
    override func sizeThatFits(size: CGSize) -> CGSize {
        let rect: CGSize = CGSizeMake(max(self.intrinsicContentSize().width, size.width), max(self.intrinsicContentSize().height, size.height))
        
        return rect
    }
    
    override func intrinsicContentSize() -> CGSize {
        return CGSizeMake(20.0, 20.0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

