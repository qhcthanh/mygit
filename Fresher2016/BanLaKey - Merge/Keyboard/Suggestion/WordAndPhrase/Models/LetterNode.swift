//
//  LetterNode.swift
//  testSuggestion
//
//  Created by manhduydl on 8/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

import Foundation


public class LetterNode {
    
    var key : String!
    weak var parentNode : LetterNode?
    var childrentNode : [String : LetterNode]
    var isEnd : Bool
    var level : Int
    var score : Int
    var count : Int
    var time : String
    
    // MARK: - Contructor method
    
    init(key : String) {
        self.key = key
        self.childrentNode = [:]
        self.isEnd = false
        self.level = 0
        self.score = 0
        self.count = 0
        self.time = ""
    }
    
    convenience init() {
        self.init(key : "-")
    }
    
    // MARK: - Detructor method
    
    deinit {
        print("letter node deinit", key)
        key.removeAll()
        time.removeAll()
        clear()
    }
    
    // Clear memory when deinit
    func clear() {
        for (_, node) in childrentNode {
            node.clear()
        }
        key = ""
        parentNode = nil
        childrentNode = [:]
    }
    
    // MARK: - Public method
    
    // Get number of child of current node
    func numberChild() -> Int {
        return self.childrentNode.count
    }
    
    // Check current node is endNode
    func isEndNode() -> Bool {
        return self.isEnd
    }
    
    // Set current node is endNode
    func setEndNode(bool : Bool) {
        self.isEnd = bool
    }
    
    // Check current node is root node
    func isRoot() -> Bool {
        return self.key == "-" && self.parentNode == nil
    }
    
    /**
     Find child of node with key
     
     - parameter key: Key to find child
     
     - returns: If find success return childNode and true else return nil and faulse
     */
    func find(key : String) -> (node : LetterNode?, found : Bool) {
        if let child = self.childrentNode[key] {
            return (child, true)
        }
        return (nil, false)
    }
    
    /**
     Add new child to current node, if dont exist is create
     
     - parameter key: Key of new child
     
     - returns: Child node
     */
    func addChild(withKey key : String!) -> LetterNode {
        var childNode : LetterNode?
        
        if let temptNode = self.childrentNode[key] {
            childNode = temptNode
        } else {
            childNode = LetterNode.init(key: key)
            childNode?.level = self.level + 1
            childNode?.parentNode = self
            self.childrentNode[key] = childNode
        }
        
        return childNode!
    }
    
    /**
     Add new child to current node, if dont exist is create
     
     - parameter key: Key of new child
     
     - returns: If create new node is return child node and true, else return child node and false
     */
    func addChild(withKey key : String!) -> (node : LetterNode, isNewNode : Bool) {
        var childNode : LetterNode?
        var isNewNode : Bool
        
        if let temptNode = self.childrentNode[key] {
            childNode = temptNode
            isNewNode = false
        } else {
            childNode = LetterNode.init(key: key)
            childNode?.level = self.level + 1
            childNode?.parentNode = self
            self.childrentNode[key] = childNode
            isNewNode = true
        }
        
        return (childNode!, isNewNode)
    }
    
//    func findChild(withKey key : String!) -> (node : LetterNode?, found : Bool) {
//        var currentNode = self
//        
//        while key.length != currentNode.level {
//            let searchKey = key.substringToIndex(currentNode.level + 1)
//            
//            if currentNode.childrentNode[searchKey] == nil {
//                return (nil, false)
//            }
//            
//            currentNode = currentNode.childrentNode[searchKey]!
//        }
//        
//        return (currentNode, currentNode.isEnd)
//    }
    
    func printNode( indent: String, leaf: Bool) {
        
        var indent = indent
        
        print(indent, terminator: "")
        if leaf {
            print("\\-", terminator: "")
            indent += " "
        } else {
            print("|-", terminator: "")
            indent += "| "
        }
        
        print(self.key)
        
        var i = 0
        for (_, node) in self.childrentNode {
            node.printNode(indent, leaf: i == self.childrentNode.count-1)
            i+=1
        }
    }
}
