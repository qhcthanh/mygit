//
//  PopupViewController.m
//  UXFrameworkCatalog
//
//  Created by VanDao on 6/29/16.
//  Copyright © 2016 VNG. All rights reserved.
//

#import "PopupViewController.h"

@interface PopupViewController () {
    NSArray *texts;
    PopoverView *popover;
    PopoverOptions *popoverOptions;
    
    UIBarButtonItem *rightBarButton;
    UIButton *rightBottomButton;
    UIButton *leftBottomButton;
}

@end


@implementation PopupViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor grayColor];
    texts = [[NSArray alloc]initWithObjects:@"Edit", @"Delete", @"Report", nil];
    popoverOptions = [[PopoverOptions alloc]init];
    popoverOptions.poverType = kPopoverTypeUp;
    popoverOptions.blackOverlayColor = [UIColor clearColor];
    
    rightBarButton = [[UIBarButtonItem alloc]initWithTitle:@"Tap me" style:UIBarButtonItemStylePlain target:self action:@selector(tapRightBarButton)];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    rightBottomButton = [[UIButton alloc]init];
    [rightBottomButton setTitle:@"Tap me" forState:UIControlStateNormal];
    rightBottomButton.frame = CGRectMake(self.view.width/2, self.view.height * 0.75, 80, 40);
    rightBottomButton.backgroundColor = [UIColor blueColor];
    rightBottomButton.layer.masksToBounds = YES;
    rightBottomButton.layer.cornerRadius = 5;
    [rightBottomButton addTarget:self action:@selector(tapRightBottomButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rightBottomButton];
    
    leftBottomButton = [[UIButton alloc]initWithFrame:CGRectMake(50, self.view.height * 0.5, 80, 40)];
    [leftBottomButton setTitle:@"Tap me" forState:UIControlStateNormal];
    leftBottomButton.backgroundColor = [UIColor blueColor];
    leftBottomButton.layer.masksToBounds = YES;
    leftBottomButton.layer.cornerRadius = 5;
    [leftBottomButton addTarget:self action:@selector(tapLeftBottomButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:leftBottomButton];
}

- (void)tapRightBarButton{
    CGPoint startPoint = CGPointMake(self.view.frame.size.width - 150, 55);
    UIView *aView = [[UIView alloc]initWithFrame:CGRectMake(30, 0, self.view.frame.size.width - 60, 300)];
    UIView *testView = [[UIView alloc]initWithFrame:CGRectMake(10, 20, aView.frame.size.width - 20, aView.frame.size.height - 20)];
    testView.backgroundColor = [UIColor blackColor];
 //   [aView addSubview:testView];

  //  testView.translatesAutoresizingMaskIntoConstraints = YES;
    
    popover = [[PopoverView alloc]initWithOption:nil showHandler:nil dismissHandler:nil];
    [popover addSubview:testView];
    [popover showContentView:aView point:startPoint];
}

- (IBAction)tapRightBottomButton:(id)sender{
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 135)];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.scrollEnabled = NO;
    popover = [[PopoverView alloc]initWithOption:popoverOptions showHandler:nil dismissHandler:nil];
    [popover showContentView:tableView fromView:rightBottomButton];
}

- (IBAction)tapLeftBottomButton:(id)sender{
    CGFloat width = self.view.frame.size.width/4;
    UIView *aView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, width)];
    PopoverOptions *options = [[PopoverOptions alloc]init];
    options.poverType = kPopoverTypeUp;
    options.cornerRadius = width/2;
    popover = [[PopoverView alloc]initWithOption:options showHandler:nil dismissHandler:nil];
    [popover showContentView:aView fromView:leftBottomButton];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [popover dismiss];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    cell.textLabel.text = texts[indexPath.row];
    
    return cell;
}

@end
