//
//  LetterNode+Extention.swift
//  BanLaKey
//
//  Created by manhduydl on 8/31/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import Foundation

extension LetterNode {
    
    /**
     Converse LetterNode to Json String
     
     - returns: return Json String
     */
    func toJsonString() -> String {
        var listNodes : [LetterNode] = [LetterNode]()
        self.getAllEndNodeFromNode(self, list: &listNodes)
        
        var dict : [String : NSDictionary] = [:]
        for item in listNodes {
            //            let tDict = ["time" : NSDate().description, "count" : 0]
            let tDict = ["time" : item.time, "count" : item.count]
            let key : String = item.key
            dict[key] = tDict
        }
        
        var jsonString = ""
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(dict, options: NSJSONWritingOptions.PrettyPrinted)
            jsonString = String(data: jsonData, encoding: NSUTF8StringEncoding)!
        } catch let error as NSError {
            print(error)
        }
        
        return jsonString
    }
    
    /**
     Get all end node from current node
     
     - returns: List endNode
     */
    func getAllEndNode() -> [LetterNode] {
        var listNode : [LetterNode] = [LetterNode]()
        self.getAllEndNodeFromNode(self, list: &listNode)
        return listNode
    }
    
    /**
     Get all EndNode from RootNode
     
     - parameter rootNode: rootNode to find EndNode
     - parameter list:     list EndNode to return
     */
    private func getAllEndNodeFromNode(node : LetterNode, inout list : [LetterNode]) {
        for (_, v1) in node.childrentNode {
            if v1.isEnd == true {
                list.append(v1)
            }
            self.getAllEndNodeFromNode(v1, list: &list)
        }
    }
}
