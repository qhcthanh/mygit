/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXLogMessage.h"

@interface UXStaticLogDetail : UXLogMessage {
    NSUInteger userId;
    NSInteger command;
    NSInteger subCommand;
    NSUInteger totalOfRequest;
    NSUInteger networkType;
    NSUInteger totalOfFailedRequest;
    NSUInteger totalOfSuccessedRequest;
    NSTimeInterval totalOfTimeRequest;
    NSTimeInterval startLogTimeInterval;
    NSTimeInterval minExcuteTimeInterval;
    NSTimeInterval maxExcuteTimeInterval;
    NSString* params;
}

@property (nonatomic, assign) NSUInteger userId;
@property (nonatomic, assign) NSInteger command;
@property (nonatomic, assign) NSInteger subCommand;
@property (nonatomic, assign) NSUInteger networkType;
@property (nonatomic, assign) NSUInteger totalOfRequest;
@property (nonatomic, assign) NSUInteger totalOfFailedRequest;
@property (nonatomic, assign) NSTimeInterval totalOfTimeRequest;
@property (nonatomic, assign) NSUInteger totalOfSuccessedRequest;
@property (nonatomic, assign) NSTimeInterval startLogTimeInterval;
@property (nonatomic, assign) NSTimeInterval minExcuteTimeInterval;
@property (nonatomic, assign) NSTimeInterval maxExcuteTimeInterval;
@property (nonatomic, copy) NSString* params;

@end
