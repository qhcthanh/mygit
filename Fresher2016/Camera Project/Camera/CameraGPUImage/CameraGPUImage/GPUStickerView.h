//
//  GPUStickerView.h
//  CameraGPUImage
//
//  Created by VanDao on 6/17/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <GPUImage.h>
#import "StickerNode.h"
#import "TextStickerNode.h"

#define kWidth [[UIScreen mainScreen] bounds].size.width
#define kHeight [[UIScreen mainScreen] bounds].size.height

@protocol TouchStickerViewDelegate <NSObject>

@optional
- (void)didTouchUpInsideTextSticker:(TextStickerNode * _Nonnull)textSticker;
- (void)didTouchMoveEndSticker:(id _Nonnull)sticker position:(CGPoint)position;
- (void)didTouchEndSticker:(id _Nonnull)sticker position:(CGPoint)position;

@end

@interface GPUStickerView : GPUImageView
@property (nullable, nonatomic) id<TouchStickerViewDelegate> touchDelegate;

- (instancetype _Nonnull)initWithFrame:(CGRect)frame;
- (void)addSticker:(StickerNode * _Nonnull)sticker;
- (void)removeSticker:(StickerNode * _Nonnull)sticker;
- (void)removeAllSticker;
- (NSArray * _Nullable)getStickerList;

@end


