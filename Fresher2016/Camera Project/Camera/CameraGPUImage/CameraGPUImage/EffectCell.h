//
//  EffectCell.h
//  CameraGPUImage
//
//  Created by qhcthanh on 6/23/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EffectCell : UICollectionViewCell
-(void)renderUI:(UIImage*)imageEffect name:(NSString*)nameEffect;
@end
