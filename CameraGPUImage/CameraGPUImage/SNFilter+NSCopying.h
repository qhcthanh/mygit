//
//  SNFilter+NSCopying.h
//  CameraGPUImage
//
//  Created by BaoNQ on 8/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SNFilter.h"

@interface SNFilter (NSCopying)

- (id)copyWithZone:(NSZone *)zone;

@end

