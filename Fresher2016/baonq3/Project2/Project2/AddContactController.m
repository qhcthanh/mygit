//
//  AddContactController.m
//  Project2
//
//  Created by BaoNQ on 6/2/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "AddContactController.h"

@interface AddContactController () {
    UIImage *_newProfileImage;
    CTContactDetail *_newContact;
}
@end

@implementation AddContactController

- (void)viewDidLoad {
    [super viewDidLoad];
    //Initialize
    _newContact = [[CTContactDetail alloc] init];
    _newProfileImage = nil;
    
    //Init default profile image
    UIImage *image = self.profileImage.image;
    UIImage *mask = [UIImage imageNamed:@"mask4.png"];
    
    CGImageRef imageReference = image.CGImage;
    CGImageRef maskReference = mask.CGImage;
    
    CGImageRef imageMask = CGImageMaskCreate(CGImageGetWidth(maskReference),
                                             CGImageGetHeight(maskReference),
                                             CGImageGetBitsPerComponent(maskReference),
                                             CGImageGetBitsPerPixel(maskReference),
                                             CGImageGetBytesPerRow(maskReference),
                                             CGImageGetDataProvider(maskReference),
                                             NULL, // Decode is null
                                             YES // Should interpolate
                                             );
    
    CGImageRef maskedReference = CGImageCreateWithMask(imageReference, imageMask);
    CGImageRelease(imageMask);
    
    self.profileImage.image = [UIImage imageWithCGImage:maskedReference];
    CGImageRelease(maskedReference);

    
    
    UITapGestureRecognizer *imageSingleTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleImageSingleTap:)];
    [self.profileImage setUserInteractionEnabled:YES];
    [self.profileImage addGestureRecognizer:imageSingleTap];
}

- (IBAction)lastNameChanged:(id)sender {
    if (self.lastNameTextField.text.length > 0)
        self.doneButton.enabled = YES;
    else
        self.doneButton.enabled = NO;
}

- (IBAction)firstNameChanged:(id)sender {
    if (self.firstNameTextField.text.length > 0)
        self.doneButton.enabled = YES;
    else
        self.doneButton.enabled = NO;
}

- (IBAction)phoneChanged:(id)sender {
    if (self.phoneTextField.text.length > 0)
        self.doneButton.enabled = YES;
    else
        self.doneButton.enabled = NO;
}

- (IBAction)doneButtonClicked:(id)sender {
    NSLog(@"donebutton clicked");

    _newContact.firstName = self.firstNameTextField.text;
    _newContact.lastName = self.lastNameTextField.text;
    [_newContact.phoneNumbers addObject:self.phoneTextField.text];
    
    //send back to tableview
    [self.delegate sendNewContactToTableViewController:_newContact  withImage:_newProfileImage];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelButtonClicked:(id)sender {
    //send back to tableview
    [self.delegate sendNewContactToTableViewController:nil withImage:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

//The event handling photo method
- (void)handleImageSingleTap:(UITapGestureRecognizer *)recognizer {
    //  Location on tap
    //  CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    //Do stuff here...
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    UIAlertAction* takePhotoAction = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action) {
                                                                if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                                                                    
                                                                    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                                                          message:@"Device has no camera"
                                                                                                                         delegate:nil
                                                                                                                cancelButtonTitle:@"OK"
                                                                                                                otherButtonTitles: nil];
                                                                    [myAlertView show];
                                                                }
                                                                else{
                                                                    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                                                    picker.delegate = self;
                                                                    picker.allowsEditing = YES;
                                                                    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                                    
                                                                    [self presentViewController:picker animated:YES completion:NULL];
                                                                }
                                                            }];
    
    UIAlertAction* selectPhotoAction = [UIAlertAction actionWithTitle:@"Select Photo" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                                                  picker.delegate = self;
                                                                  picker.allowsEditing = YES;
                                                                  picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                                  
                                                                  [self presentViewController:picker animated:YES completion:NULL];
                                                              }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                             [alert dismissViewControllerAnimated:YES completion:^{}];
                                                         }];
    
    [alert addAction:takePhotoAction];
    [alert addAction:selectPhotoAction];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    _newProfileImage = info[UIImagePickerControllerEditedImage];
    
    UIImage *mask = [UIImage imageNamed:@"mask4.png"];
    
    _newProfileImage = [_newProfileImage maskImageWith:mask];
    self.profileImage.image = _newProfileImage;

    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

@end
