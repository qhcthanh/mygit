/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@protocol UXCSSParserDelegate;
@class UXCSSRuleset;
@class UXDOM;

/**
 * The notification key for when a stylesheet has changed.
 *
 * @ingroup UXCSS
 *
 * This notification will be sent with the stylesheet as the object. Listeners should add
 * themselves using the stylesheet object that they are interested in.
 *
 * The NSNotification userInfo object will be nil.
 */
extern NSString* const UXStylesheetDidChangeNotification;

/**
 * Loads and caches information regarding a specific stylesheet.
 *
 * @ingroup UXCSS
 *
 * Use this object to load and parse a CSS stylesheet from disk and then apply the stylesheet
 * to views. Rulesets are cached on demand and cleared when a memory warning is received.
 *
 * Stylesheets can be merged using the addStylesheet: method.
 *
 * Cached rulesets are released when a memory warning is received.
 */
@interface UXCSSStyleSheet : NSObject {
@private
    NSDictionary* _rawRulesets;
    NSMutableDictionary* _ruleSets;
    NSDictionary* _significantScopeToScopes;
}

/**
 * A set of NSString filenames for the @htmlonly@imports@endhtmlonly in this stylesheet.
 *
 */
@property (nonatomic, readonly, copy) NSSet* dependencies;

/**
 * Loads and parses a CSS file from disk.
 *
 * @fn UXStylesheet::loadFromPath:pathPrefix:delegate:
 * @param path         The path of the file to be read.
 * @param pathPrefix   [optional] A prefix path that will be prepended to the given path
 *                          as well as any imported files.
 * @param delegate     [optional] A delegate that can reprocess paths.
 * @returns YES if the CSS file was successfully loaded and parsed, NO otherwise.
 */
- (BOOL)loadFromPath:(NSString *)path
          pathPrefix:(NSString *)pathPrefix
            delegate:(id<UXCSSParserDelegate>)delegate;
- (BOOL)loadFromPath:(NSString *)path pathPrefix:(NSString *)path;
- (BOOL)loadFromPath:(NSString *)path;

/**
 * Merge another stylesheet with this one.
 *
 * All property values in the given stylesheet will overwrite values in this stylesheet.
 * Non-overlapping values will not be modified.
 *
 */
- (void)addStylesheet:(UXCSSStyleSheet*)stylesheet;

/**
 * Apply any rulesets that match the className to the given view.
 *
 * @param view       The view for which styles should be applied.
 * @param className  Either the view's class as a string using NSStringFromClass([view class]);
 *                        or a CSS class selector such as ".myClassSelector".
 * @param dom        The DOM responsible for applying this style
 */
- (void)applyStyleToView:(UIView *)view withClassName:(NSString *)className inDOM: (UXDOM*)dom;

/**
 * Build a string describing the rules that would be applied to the view given a css class name in a DOM.
 * Current implementations output Objective-C code that use viewName as the target.
 *
 */
- (NSString*)descriptionForView:(UIView *)view withClassName:(NSString *)className inDOM: (UXDOM*)dom andViewName: (NSString*) viewName;

/**
 * Returns an autoreleased ruleset for the given class name.
 *
 * @param className  Either the view's class as a string using NSStringFromClass([view class]);
 *                        or a CSS class selector such as ".myClassSelector".
 */
- (UXCSSRuleset *)rulesetForClassName:(NSString *)className;

/**
 * The class to create for rule sets. Default is UXCSSRuleset
 */
+(Class)rulesetClass;
/**
 * Set the class created to hold rule sets. Default is UXCSSRuleset
 */
+(void)setRulesetClass: (Class) rulesetClass;

@end
