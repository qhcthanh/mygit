//
//  KeyStrokeViewController.swift
//  BanLaKey
//
//  Created by VanDao on 9/5/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import Foundation
import UIKit

private let indexPaths: [String : NSIndexPath] = [
    "Telex" : NSIndexPath.init(forRow: 0, inSection: 0),
    "VNI" : NSIndexPath.init(forRow: 0, inSection: 1)
]

let sections: [String] = ["Chọn kiểu gõ"]
let rows : [String : [String]] = [
    sections[0] : [
        "Telex",
        "VNI"
    ]
]

class KeyStrokeViewController : UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBarHidden = false
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows[sections[section]]!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: CTTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! CTTableViewCell
        cell.label.text = rows[sections[indexPath.section]]![indexPath.row]

        if (indexPath.compare(indexPaths["Telex"]!) == NSComparisonResult.OrderedSame) {
            cell.iconImageView.image = UIImage.init(named: "inputMethod.png")
        } else if (indexPath.compare(indexPaths["VNI"]!) == NSComparisonResult.OrderedSame) {
            cell.iconImageView.image = UIImage.init(named: "inputMethod.png")
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell: CTTableViewCell = tableView.cellForRowAtIndexPath(indexPath) as! CTTableViewCell
        let color: UIColor = UIColor.init(patternImage: UIImage.init(named: "selected.png")!)
        cell.backgroundColor = color
    }
}