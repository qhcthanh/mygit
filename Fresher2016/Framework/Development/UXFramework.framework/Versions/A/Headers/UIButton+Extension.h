/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@interface UIButton (Extension)

/**
 *  Create a buton with normal style
 *
 *  @param buttonText The title of button
 *
 *  @return An UIButton object
 */
+ (UIButton*)buttonWithText:(NSString*)buttonText;

/**
 *  Create a button with DONE style
 *
 *  @param buttonText The title of button
 *
 *  @return An UIButton object
 */
+ (UIButton*)buttonWithDoneText:(NSString*)buttonText;

/**
 *  Create a button with an image style
 *
 *  @param imageURL The local image URL
 *
 *  @return An UIButton object
 */
+ (UIButton*)buttonWithImageURL:(NSString*)imageURL;

/**
 *  Create a button with checkmark style
 *
 *  @param buttonText The title of button
 *
 *  @return An UIButton object
 */
+ (UIButton*)buttonWithCheckmarkText:(NSString*)buttonText;

/**
 *  Create a button with multiple image URLs
 *
 *  @param normalURL   The normal image URL
 *  @param selectedURL The selected image URL
 *
 *  @return An UIButton object
 */
+ (UIButton*)initWithMultipleImageURL:(NSString*)normalURL selectedURL:(NSString*)selectedURL;

/**
 *  Create a button with multiple images
 *
 *  @param normalImage   The normal image
 *  @param selectedImage The selected image
 *
 *  @return An UIButton object
 */
+ (UIButton*)buttonWithMultipleImage:(UIImage*)normalImage selectedImage:(UIImage*)selectedImage;

@end
