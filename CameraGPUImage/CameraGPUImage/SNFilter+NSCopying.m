//
//  SNFilter+NSCopying.m
//  CameraGPUImage
//
//  Created by BaoNQ on 8/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "SNFilter+NSCopying.h"

@implementation SNFilter (NSCopying)

- (id)copyWithZone:(NSZone *)zone {
    SNFilter *copy = [[SNFilter alloc] initWithImage:self.image];
    copy.name = self.name;
    
    for (SNSticker *sticker in self.stickers) {
        [copy.stickers addObject:[sticker copy]];
    }
    return copy;
}

@end
