/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXPagingScrollView.h"
#import "UXPhotoScrollViewDelegate.h"
#import "UXPhotoAlbumScrollViewDelegate.h"
#import "UXPhotoAlbumScrollViewDataSource.h"

/**
 * A paged scroll view that shows a collection of photos.
 *
 *
 * This view provides a light-weight implementation of a photo viewer, complete with
 * pinch-to-zoom and swiping to change photos. It is designed to perform well with
 * large sets of photos and large images that are loaded from either the network or
 * disk.
 *
 * It is intended for this view to be used in conjunction with a view controller that
 * implements the data source protocol and presents any required chrome.
 *
 * @see UXPhotoViewController
 */
@interface UXPhotoAlbumScrollView : UXPagingScrollView <UXPhotoScrollViewDelegate>

#pragma mark Data Source

/**
 * Use this method in your implementation of UXPhotoAlbumScrollViewDataSource's
 * pagingScrollView:pageForIndex:.
 *
 * Example:
 *
 @code
 - (id<UXPagingScrollViewPage>)pagingScrollView:(UXPagingScrollView *)pagingScrollView pageForIndex:(NSInteger)pageIndex {
 return [self.photoAlbumView pagingScrollView:pagingScrollView pageForIndex:pageIndex];
 }
 @endcode
 *
 * Automatically uses the paging scroll view's page recycling methods and creates
 * UXPhotoScrollViews as needed.
 *
 */
- (UIView<UXPagingScrollViewPage> *)pagingScrollView:(UXPagingScrollView *)pagingScrollView pageViewForIndex:(NSInteger)pageIndex;

/**
 * The data source for this photo album view.
 *
 * This is the only means by which this photo album view acquires any information about the
 * album to be displayed.
 *
 */
@property (nonatomic, weak) id<UXPhotoAlbumScrollViewDataSource> dataSource;

/**
 * The delegate for this photo album view.
 *
 * Any user interactions or state changes are sent to the delegate through this property.
 *
 */
@property (nonatomic, weak) id<UXPhotoAlbumScrollViewDelegate> delegate;

#pragma mark Configuring Functionality
/**
 * Whether zooming is enabled or not.
 *
 * Regardless of whether this is enabled, only original-sized images will be zoomable.
 * This is because we often don't know how large the final image is so we can't
 * calculate min and max zoom amounts correctly.
 *
 * By default this is YES.
 *
 */
@property (nonatomic, assign, getter=isZoomingEnabled) BOOL zoomingIsEnabled;

/**
 * Whether small photos can be zoomed at least until they fit the screen.
 *
 * @see NIPhotoScrollView::zoomingAboveOriginalSizeIsEnabled
 *
 * By default this is YES.
 *
 */
@property (nonatomic, assign, getter=isZoomingAboveOriginalSizeEnabled) BOOL zoomingAboveOriginalSizeIsEnabled;

/**
 * The background color of each photo's view.
 *
 * By default this is [UIColor blackColor].
 *
 */
@property (nonatomic, strong) UIColor* photoViewBackgroundColor;

#pragma mark Configuring Presentation
/**
 * An image that is displayed while the photo is loading.
 *
 * This photo will be presented if no image is returned in the data source's implementation
 * of photoAlbumScrollView:photoAtIndex:photoSize:isLoading:.
 *
 * Zooming is disabled when showing a loading image, regardless of the state of zoomingIsEnabled.
 *
 * By default this is nil.
 *
 */
@property (nonatomic, strong) UIImage* loadingImage;

@property (nonatomic, strong) NSArray* arrayURLThumb;
@property (nonatomic, strong) NSArray* arrayURLHigh;

#pragma mark Notifying the View of Loaded Photos
/**
 * Notify the scroll view that a photo has been loaded at a given index.
 *
 * You should notify the completed loading of thumbnails as well. Calling this method
 * is fairly lightweight and will only update the images of the visible pages. Err on the
 * side of calling this method too much rather than too little.
 *
 * The photo at the given index will only be replaced with the given image if photoSize
 * is of a higher quality than the currently-displayed photo's size.
 *
 */
- (void)didLoadPhoto: (UIImage *)image
             atIndex: (NSInteger)photoIndex
           photoSize: (UXPhotoScrollViewPhotoSize)photoSize;

@end
