//
//  ImageContactLoadManager.m
//  ZaloContactInviting
//
//  Created by VanDao on 5/26/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import "ImageCacheManager.h"
#import "ContactTableViewCell.h"

@interface ImageCacheManager(){
    dispatch_queue_t internalConcurrentQueue;
    NSCache *imageCache;
}

@end

@implementation ImageCacheManager

-(id)init{
    self = [super init];
    if (self){
        imageCache = [[NSCache alloc]init];
        internalConcurrentQueue = dispatch_queue_create("Cache image concurrent queue", DISPATCH_QUEUE_CONCURRENT);
    }
    
    return self;
}

- (void)setImage: (UIImage * _Nonnull)image withID:(NSString * _Nonnull)identifier{
    dispatch_barrier_async(internalConcurrentQueue, ^{
        if (image == nil || identifier == nil)
            return;
        
        if ([imageCache objectForKey:identifier] == nil)
            [imageCache setObject:image forKey:identifier];
    });
}

- (UIImage * _Nullable)getImageWithID: (NSString * _Nonnull)identifier{
    __block UIImage *image = nil;
    
    dispatch_sync(internalConcurrentQueue, ^{
        image = [imageCache objectForKey:identifier];
    });
    
    return image;
}

- (void)removeImageWithID: (NSString * _Nonnull)identifier{
    dispatch_barrier_async(internalConcurrentQueue, ^{
        if ([imageCache objectForKey:identifier] != nil){
            [imageCache removeObjectForKey:identifier];
        }
    });
}

- (void)cleanCache{
    dispatch_barrier_async(internalConcurrentQueue, ^{
        [imageCache removeAllObjects];
    });
}

@end
