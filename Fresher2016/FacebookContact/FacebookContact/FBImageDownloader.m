//
//  FBImageDownloader.m
//  FacebookContact
//
//  Created by BaoNQ on 7/15/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import "FBImageDownloader.h"
#import "FBCacheManager.h"

@interface FBImageDownloader ()

@property (readwrite, nonatomic, strong) FBSessionManager *sessionManager;

@end

@implementation FBImageDownloader

- (id)init {
    self = [super init];
    if (!self)
        return nil;
    // Create a session manager use defaultSessionConfiguration.
    self.sessionManager = [[FBSessionManager alloc] init];
    return self;
}

- (id)initWithSessionManager:(FBSessionManager *)sessionManager {
    self = [super init];
    if (!self)
        return nil;
    self.sessionManager = sessionManager;
    return self;
}

- (INTEGER_T)downloadImageWithURL:(NSURL *)url identifier:(id)imageIdentifier completionHandler:(DownloadImageWithURLCompletionHandler)completionBlock {

    return [self.sessionManager startDataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        UIImage *downloadedImage = nil;
        if (!error) {
            downloadedImage = [UIImage imageWithData:data];
            // Cache image.
            if (imageIdentifier)
                [[FBCacheManager sharedCache] cacheImage:downloadedImage forKey:imageIdentifier];
        }
        if (completionBlock)
            completionBlock(imageIdentifier, downloadedImage, response, error);
    }];
}

@end
