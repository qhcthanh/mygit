//
//  EmojiCollectionViewCell.swift
//  EmojiKeyboard
//
//  Created by Quach Ha Chan Thanh on 8/14/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import UIKit

public class EmojiCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var emojiLabel: UILabel!
    
    weak public var itemProtocol: SimpleModelProtocol?
    
    override public func awakeFromNib() {
        super.awakeFromNib()

    }
    
    public func bindingUI(itemProtocol: SimpleModelProtocol) {
        self.itemProtocol = itemProtocol
        
        emojiLabel.text = itemProtocol.titleContent
    }
    
    override public func prepareForReuse() {
        super.prepareForReuse()
        
        // Clean UI
        emojiLabel.text = ""
    }
    
    // MARK: UIGestureRecognizer
    func onTapEmoji(tapGesture: UITapGestureRecognizer) {
        print(tapGesture.state.rawValue)
        if tapGesture.state == .Began {
            print("Tap")
        } else if tapGesture.state == .Ended {
            print("End Tap")
        }
    }
    
    func onPressLongEmoji(longPressGesture: UILongPressGestureRecognizer) {
    
        if longPressGesture.state == .Began {
            print("LongPress Began ")
            emojiLabel.backgroundColor = .grayColor()
        } else if longPressGesture.state == .Ended {
            print("LongPress end")
            emojiLabel.backgroundColor = .clearColor()
        }
        
    }

}






