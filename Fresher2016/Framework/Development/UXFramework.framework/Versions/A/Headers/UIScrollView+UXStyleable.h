/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@class UXDOM;
@class UXCSSRuleset;

@interface UIScrollView (UXStyleable)

/**
 * Applies the given rule set to this scroll view.
 *
 * This method is exposed primarily for subclasses to use when implementing the
 * applyStyleWithRuleSet: method from UXStyleable.
 */
- (void)applyScrollViewStyleWithRuleSet:(UXCSSRuleset *)ruleSet inDOM:(UXDOM*) dom;

@end
