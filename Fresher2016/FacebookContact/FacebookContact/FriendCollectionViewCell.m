//
//  FriendCollectionViewCell.m
//  FacebookContact
//
//  Created by qhcthanh on 7/14/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import "FriendCollectionViewCell.h"
#import "FBImageDownloader.h"
#import "FBAPIUtility.h"
#import "FBCacheManager.h"

@interface FriendCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *pictureImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;


@end

@implementation FriendCollectionViewCell

- (void)bindingUIWithUserInfo:(FBUserInfo*)friendT {
    
    _friend = friendT;
    _nameLabel.text = _friend.userName;
    _infoLabel.text = [FBUserInfo getStringFBUserGender:_friend.gender];

    id cacheManager = [FBCacheManager sharedCache];
    
    UIImage* picture = [cacheManager imageForKey:_friend.userID];
    if(picture) {
        _pictureImageView.image = picture;
    } else {
        FBImageDownloader* imageDowloader = [[FBImageDownloader alloc] initWithSessionManager:[FBSessionManager sharedDefaultSessionManager]];
        NSURL* urlPicture = [NSURL URLWithString:_friend.pictureURL];
        
        [imageDowloader downloadImageWithURL:urlPicture  identifier:_friend.userID completionHandler:^(id imageIdentifier, UIImage *image, NSURLResponse *response, NSError *error) {
            if(image && !error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(imageIdentifier == _friend.userID) {
                        _pictureImageView.image = image;
                    }
                });
            }
        }];
    }
}

- (void)prepareForReuse {
    
    _nameLabel.hidden = NO;
    _infoLabel.hidden = NO;
    _pictureImageView.hidden = NO;
    
    _nameLabel.text = @"";
    _infoLabel.text = @"";
    _pictureImageView.image = [UIImage imageNamed:@"default_user"];
    UIView* view = [self viewWithTag:1001];
    if(view) {
        [view removeFromSuperview];
    }
    [super prepareForReuse];
}

- (void)hideAllUIInCell {
    
    _nameLabel.hidden = YES;
    _infoLabel.hidden = YES;
    _pictureImageView.hidden = YES;
    UIView* view = [self viewWithTag:1001];
    if(view) {
        [view removeFromSuperview];
    }

}

@end
