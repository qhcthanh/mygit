/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXStyle.h"

@interface UXFourBorderStyle : UXStyle {
}

@property (nonatomic, retain) UIColor*  top;
@property (nonatomic, retain) UIColor*  right;
@property (nonatomic, retain) UIColor*  bottom;
@property (nonatomic, retain) UIColor*  left;
@property (nonatomic)         CGFloat   width;

+ (UXFourBorderStyle*)styleWithTop:(UIColor*)top right:(UIColor*)right bottom:(UIColor*)bottom
                              left:(UIColor*)left width:(CGFloat)width next:(UXStyle*)next;
+ (UXFourBorderStyle*)styleWithTop:(UIColor*)top width:(CGFloat)width next:(UXStyle*)next;
+ (UXFourBorderStyle*)styleWithRight:(UIColor*)right width:(CGFloat)width next:(UXStyle*)next;
+ (UXFourBorderStyle*)styleWithBottom:(UIColor*)bottom width:(CGFloat)width next:(UXStyle*)next;
+ (UXFourBorderStyle*)styleWithLeft:(UIColor*)left width:(CGFloat)width next:(UXStyle*)next;


@end
