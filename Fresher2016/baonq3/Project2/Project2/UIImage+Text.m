//
//  UIImage+Text.m
//  Project2
//
//  Created by BaoNQ on 6/6/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "UIImage+Text.h"

@implementation UIImage (Text)

+ (UIImage *)imageWithText:(NSString *)text imageSize:(CGSize)size textColor:(UIColor *)textColor backgroundColor:(UIColor *)backgroundColor {
    //Set font type and size
    int a = size.width/2;
    UIFont *font = [UIFont systemFontOfSize:20];
    CGSize fontSize  = [text sizeWithAttributes:@{NSFontAttributeName:font}];
    fontSize.width += 0.8 * a;
    fontSize.height += 0.8 * a;
    UIGraphicsBeginImageContextWithOptions(fontSize, NO, 0.0);
    CGContextSetFillColorWithColor(UIGraphicsGetCurrentContext(), backgroundColor.CGColor);
    CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, size.width, size.height));
    
    // Draw in context
    [text drawAtPoint:CGPointMake(0.4 * a, 0.4 * a) withAttributes:@{NSFontAttributeName:font, NSForegroundColorAttributeName:textColor}];
    
    // Transfer image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
