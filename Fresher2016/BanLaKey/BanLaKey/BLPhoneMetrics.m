//
//  BLPhoneMetrics.m
//  BanLaKey
//
//  Created by BaoNQ on 8/16/16.
//  Copyright © 2016 admin. All rights reserved.
//


#import "BLPhoneMetrics.h"
#import "Linear.h"

#define kPhoneKeyboardPortraitWidth     320.0
#define kPhoneKeyboardLandscapeWidth    568.0

#define kPhoneKeyboardPortraitHeight    216.0
#define kPhoneKeyboardLandscapeHeight   162.0


#define edgeMargin      5.0
#define bottomMargin    5.0


PhoneKeyboardMetrics generatePhoneKeyboardMetrics(CGFloat keyboardWidth, CGFloat keyboardHeight) {
    
    CGFloat rowMargin = LINEAR_EQ(keyboardHeight,
                                  kPhoneKeyboardPortraitHeight, 15.0,
                                  kPhoneKeyboardLandscapeHeight, 7.0);
    
    CGFloat columnMargin = LINEAR_EQ(keyboardWidth,
                                     kPhoneKeyboardPortraitWidth, 6.0,
                                     kPhoneKeyboardLandscapeWidth, 7.0);
    
    CGFloat keyHeight = LINEAR_EQ(keyboardHeight,
                                  kPhoneKeyboardPortraitHeight, 39.0,
                                  kPhoneKeyboardLandscapeHeight, 33.0);
    
    CGFloat letterKeyWidth = LINEAR_EQ(keyboardWidth,
                                       kPhoneKeyboardPortraitWidth, 26.0,
                                       kPhoneKeyboardLandscapeWidth, 49.0);
    
    
    CGFloat nextKeyboardButtonWidth = LINEAR_EQ(keyboardWidth,
                                                kPhoneKeyboardPortraitWidth, 34.0,
                                                kPhoneKeyboardLandscapeWidth, 50.0);
    
    CGFloat returnButtonWidth = LINEAR_EQ(keyboardWidth,
                                          kPhoneKeyboardPortraitWidth, 74.0,
                                          kPhoneKeyboardLandscapeWidth, 107.0);
    
    CGFloat deleteButtonWidth = LINEAR_EQ(keyboardWidth,
                                          kPhoneKeyboardPortraitWidth, 36.0,
                                          kPhoneKeyboardLandscapeWidth, 69.0);

    PhoneKeyboardMetrics metrics = {
        
        .keyModeButton = {
            edgeMargin,
            keyboardHeight - bottomMargin - keyHeight,
            nextKeyboardButtonWidth,
            keyHeight
        },
        
        .returnButton = {
            keyboardWidth - edgeMargin - returnButtonWidth,
            keyboardHeight - bottomMargin - keyHeight,
            returnButtonWidth,
            keyHeight
        },
        
        .spaceButton = {
            edgeMargin + 2 * (nextKeyboardButtonWidth + columnMargin),
            keyboardHeight - bottomMargin - keyHeight,
            keyboardWidth - (edgeMargin + 2 * (nextKeyboardButtonWidth + columnMargin) + columnMargin + returnButtonWidth + edgeMargin),
            keyHeight
        },
        
        .deleteButton = {
            keyboardWidth - edgeMargin - deleteButtonWidth,
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight),
            deleteButtonWidth,
            keyHeight
        },
        
        .leftShiftButton = {
            edgeMargin,
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight),
            LINEAR_EQ(keyboardWidth,
                      kPhoneKeyboardPortraitWidth, 36.0,
                      kPhoneKeyboardLandscapeWidth, 68.0),
            keyHeight
        },
        
        .globeButton = {
            edgeMargin + nextKeyboardButtonWidth + columnMargin,
            keyboardHeight - bottomMargin - keyHeight,
            nextKeyboardButtonWidth,
            keyHeight
        },
//
//        .settingButton {
//            
//        },
        
        .QButton = {
            (keyboardWidth - columnMargin)/2 - letterKeyWidth - 4 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        .WButton = {
            (keyboardWidth - columnMargin)/2 - letterKeyWidth - 3 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        .EButton = {
            (keyboardWidth - columnMargin)/2 - letterKeyWidth - 2 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        .RButton = {
            (keyboardWidth - columnMargin)/2 - letterKeyWidth - 1 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        .TButton = {
            (keyboardWidth - columnMargin)/2 - letterKeyWidth,
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        
        .YButton = {
            (keyboardWidth + columnMargin)/2,
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        .UButton = {
            (keyboardWidth + columnMargin)/2 + 1 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        .IButton = {
            (keyboardWidth + columnMargin)/2 + 2 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        .OButton = {
            (keyboardWidth + columnMargin)/2 + 3 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        .PButton = {
            (keyboardWidth + columnMargin)/2 + 4 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        
        
        .AButton = {
            (keyboardWidth - letterKeyWidth)/2 - 4 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        
        .SButton = {
            (keyboardWidth - letterKeyWidth)/2 - 3 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },

        .DButton = {
            (keyboardWidth - letterKeyWidth)/2 - 2 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        
        .FButton = {
            (keyboardWidth - letterKeyWidth)/2 - 1 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        .GButton = {
            (keyboardWidth - letterKeyWidth)/2,
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        .HButton = {
            (keyboardWidth - letterKeyWidth)/2 + 1 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        .JButton = {
            (keyboardWidth - letterKeyWidth)/2 + 2 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        .KButton = {
            (keyboardWidth - letterKeyWidth)/2 + 3 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        .LButton = {
            (keyboardWidth - letterKeyWidth)/2 + 4 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        
        
        .ZButton = {
            (keyboardWidth - letterKeyWidth)/2 - 3 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        
        .XButton = {
            (keyboardWidth - letterKeyWidth)/2 - 2 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        
        .CButton = {
            (keyboardWidth - letterKeyWidth)/2 - 1 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        
        .VButton = {
            (keyboardWidth - letterKeyWidth)/2,
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        .BButton = {
            (keyboardWidth - letterKeyWidth)/2 + 1 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        .NButton = {
            (keyboardWidth - letterKeyWidth)/2 + 2 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },
        .MButton = {
            (keyboardWidth - letterKeyWidth)/2 + 3 * (letterKeyWidth + columnMargin),
            keyboardHeight - (bottomMargin + keyHeight + rowMargin + keyHeight),
            letterKeyWidth,
            keyHeight
        },

    };
    return metrics;
}