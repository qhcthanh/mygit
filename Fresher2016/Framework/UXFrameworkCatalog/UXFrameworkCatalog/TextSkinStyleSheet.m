/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "TextSkinStyleSheet.h"

#define USE_CUSTOM_FONT_SIZE 0

#if USE_CUSTOM_FONT_SIZE

    UIFont* gStyledSystemFont = nil;
    UIFont* gStyledBoldSystemFont = nil;
    UIFont* gStyledItalicSystemFont = nil;

    static NSString* kSystemFontName = @"Roboto-Regular";
    static NSString* kBoldSystemFontName = @"Roboto-Bold";
    static NSString* kItalicSystemFontName = @"Roboto-Italic";
#endif

typedef enum tagStyledSystemFont {
    kStyledSystemFontNormal = 0,
    kStyledSystemFontBold,
    kStyledSystemFontItalic
} StyledSystemFont;

@implementation SkinStyleSheet (TextSkinStyleSheet)

- (UIFont*)styledSystemFontOfSize:(CGFloat)fontSize {
    
    UIFont* customSystemFont = nil;
    
#if USE_CUSTOM_FONT_SIZE
    if (!gStyledSystemFont) {
        gStyledSystemFont = UXCSS_font(@"body.font.style.regular", UIControlStateNormal);
        if (gStyledSystemFont) {
            gStyledSystemFont = [gStyledSystemFont retain];
        } else {
            gStyledSystemFont = [self createRetainStyledFont:@"body.font.style.regular" styled:kStyledSystemFontNormal];
        }
    }
    
    if (gStyledSystemFont) {
        NSString* fontName = [gStyledSystemFont fontName];
        customSystemFont = [UIFont fontWithName:fontName size:fontSize];
    }
    
    if (!customSystemFont) {
        if (!customSystemFont) {
            customSystemFont = [UIFont systemFontOfSize:fontSize];
        }
    }
#else
    customSystemFont = [UIFont systemFontOfSize:fontSize];
#endif
    return customSystemFont;
}


- (UIFont*)boldStyledSystemFontOfSize:(CGFloat)fontSize {
    UIFont* customSystemFont = nil;
    
#if USE_CUSTOM_FONT_SIZE
    if (!gStyledBoldSystemFont) {
        gStyledBoldSystemFont = UXCSS_font(@"body.font.style.bold", UIControlStateNormal);
        if (gStyledBoldSystemFont) {
            gStyledBoldSystemFont = [gStyledBoldSystemFont retain];
        } else {
            gStyledBoldSystemFont = [self createRetainStyledFont:@"body.font.style.bold" styled:kStyledSystemFontBold];
        }
    }
    
    if (gStyledBoldSystemFont) {
        customSystemFont = [UIFont fontWithName:[gStyledBoldSystemFont fontName] size:fontSize];
    }
    
    if (!customSystemFont) {
        if (!customSystemFont) {
            customSystemFont = [UIFont boldSystemFontOfSize:fontSize];
        }
    }
#else
    customSystemFont = [UIFont boldSystemFontOfSize:fontSize];
#endif

    return customSystemFont;
}

- (UIFont*)italicStyledSystemFontOfSize:(CGFloat)fontSize {
    UIFont* customSystemFont = nil;
    
#if USE_CUSTOM_FONT_SIZE
    if (!gStyledItalicSystemFont) {
        gStyledItalicSystemFont = UXCSS_font(@"body.font.style.italic", UIControlStateNormal);
        if (gStyledItalicSystemFont) {
            gStyledItalicSystemFont = [gStyledItalicSystemFont retain];
        } else {
            gStyledItalicSystemFont = [self createRetainStyledFont:@"body.font.style.italic" styled:kStyledSystemFontItalic];
        }
    }
    
    if (gStyledItalicSystemFont) {
        customSystemFont = [UIFont fontWithName:[gStyledItalicSystemFont fontName] size:fontSize];
    }
    
    if (!customSystemFont) {
        if (!customSystemFont) {
            customSystemFont = [UIFont italicSystemFontOfSize:fontSize];
        }
    }
#else
    customSystemFont = [UIFont italicSystemFontOfSize:fontSize];
#endif
    
    return customSystemFont;
}

#pragma -
#pragma Text Field
- (UIColor*)textFieldColor {
    return UXJSONSTATE(@"textFieldColor", color, UIControlStateNormal);
}

- (UIColor*)textLabelColor {
    return UXJSONSTATE(@"textLabelColor", color, UIControlStateNormal);
}

- (UIColor*)textPlaceHolderColor {
    return UXJSONSTATE(@"textPlaceHolderColor", color, UIControlStateNormal);
}

- (UIColor*)lineTextFieldColorNormal {
    return UXJSONSTATE(@"lineTextFieldColor", color, UIControlStateNormal);
}

- (UIColor*)lineTextFieldColorSelected {
    return UXJSONSTATE(@"lineTextFieldColor", color, UIControlStateSelected);
}

#pragma -
#pragma Privates
- (UXStyle*)whiteText {
	return [UXTextStyle styleWithColor:[UIColor whiteColor] next:nil];
}
- (UXStyle*)blackText {
	return [UXTextStyle styleWithColor:[UIColor blackColor] next:nil];
}
- (UXStyle*)redText {
	return [UXTextStyle styleWithColor:[UIColor redColor] next:nil];
}
- (UXStyle*)greenText {
	return [UXTextStyle styleWithColor:[UIColor greenColor] next:nil];
}
- (UXStyle*)oliveText {
	return [UXTextStyle styleWithColor:RGBCOLOR(128, 128, 0) next:nil];
}
- (UXStyle*)blueText {
	return [UXTextStyle styleWithColor:[UIColor blueColor] next:nil];
}
- (UXStyle*)purpleText {
	return [UXTextStyle styleWithColor:[UIColor purpleColor] next:nil];
}
- (UXStyle*)tealText {
	return [UXTextStyle styleWithColor:RGBCOLOR(0, 128, 128) next:nil];
}
- (UXStyle*)orangeText {
	return [UXTextStyle styleWithColor:RGBCOLOR(255, 165, 0) next:nil];
}
- (UXStyle*)pinkText {
	return [UXTextStyle styleWithColor:RGBCOLOR(255, 192, 203) next:nil];
}
- (UXStyle*)grayText {
	return [UXTextStyle styleWithColor:[UIColor grayColor] next:nil];
}

- (UXStyle*)largeText {
	return [UXTextStyle styleWithFont:UXSTYLEVAR(customNormalFont32) next:nil];
}

- (UXStyle*)mediumText {
	return [UXTextStyle styleWithFont:UXSTYLEVAR(customNormalFont18) next:nil];
}

- (UXStyle*)mediumText2 {
	return [UXTextStyle styleWithFont:UXSTYLEVAR(customNormalFont20) next:nil];
}

- (UXStyle*)smallText {
	return [UXTextStyle styleWithFont:UXSTYLEVAR(customNormalFont13) next:nil];
}

- (UXStyle*)smallText2 {
	return [UXTextStyle styleWithFont:UXSTYLEVAR(customNormalFont15) next:nil];
}

- (UXStyle*)gray2Text {
	return [UXTextStyle styleWithColor:RGBCOLOR(63, 63, 63) next:nil];
}

- (UXStyle*)gray3Text {
    return [UXTextStyle styleWithFont:UXSTYLEVAR(font) color:RGBCOLOR(63, 63, 63) next:nil];
}

- (UXStyle*)absoluted {
	return //[UXSolidFillStyle styleWithColor:[UIColor blueColor] next:
	[UXBoxStyle styleWithMargin:UIEdgeInsetsMake(0, 0, 0, 0)
						padding:UIEdgeInsetsMake(0, 0, 0, 0)
						minSize:CGSizeZero position:UXPositionAbsolute next:nil];//];
}

#pragma mark -
#pragma mark StyledTextLabel
- (UXStyle*)floated {
	return
	[UXSolidFillStyle styleWithColor:[UIColor clearColor] next:
     [UXBoxStyle styleWithMargin:UIEdgeInsetsMake(3, 3, 3, 3)
                         padding:UIEdgeInsetsMake(0, 0, 0, 0)
                         minSize:CGSizeZero
                        position:UXPositionFloatLeft next:nil]];
}

- (UXStyle*)center {
	return [UXBoxStyle styleWithMargin:UIEdgeInsetsMake(5, 5, 5, 5)
							   padding:UIEdgeInsetsMake(10, 10, 10, 10)
							   minSize:CGSizeZero position:UXPositionAbsolute next:nil];
}

- (UXStyle*)docked {
	return
	[UXSolidFillStyle styleWithColor:[UIColor clearColor] next:
	 [UXBoxStyle styleWithMargin:UIEdgeInsetsMake(0, 0, 0, 0)
						 padding:UIEdgeInsetsMake(0, 0, 0, 0)
						 minSize:CGSizeZero
						position:UXPositionFloatRight next:nil]];
}

- (UXStyle*)inlineBox {
	return
	//[UXSolidFillStyle styleWithColor:[UIColor clearColor] next:
    [UXBoxStyle styleWithPadding:UIEdgeInsetsMake(5, 5, 5, 5) next:
     [UXSolidBorderStyle styleWithColor:[UIColor whiteColor] width:0 next:nil]];//];
}

- (UXStyle*)titleInlineBox {
	return
	//[UXSolidFillStyle styleWithColor:[UIColor clearColor] next:
    [UXBoxStyle styleWithPadding:UIEdgeInsetsMake(0, 0, 0, 0) next:
     [UXSolidBorderStyle styleWithColor:[UIColor whiteColor] width:0 next:nil]];//];
}

- (UXStyle*)inlineRightBox {
	return
	[UXLinearGradientFillStyle styleWithColor1:RGBCOLOR(255, 255, 255)
										color2:RGBCOLOR(216, 221, 231) next:
	 [UXBoxStyle styleWithPadding:UIEdgeInsetsMake(5, 50 + 5, 5, 5) next:
	  [UXSolidBorderStyle styleWithColor:[UIColor redColor] width:0 next:nil]]];
}

- (UIFont*)createRetainStyledFont:(NSString*)category styled:(StyledSystemFont)style {
#if USE_CUSTOM_FONT_SIZE
    if (style == kStyledSystemFontNormal) {
        if (gStyledSystemFont) {
            return gStyledSystemFont;
        }
    } else if (style == kStyledSystemFontBold) {
        if (gStyledBoldSystemFont) {
            return gStyledBoldSystemFont;
        }
    } else if (style == kStyledSystemFontItalic) {
        if (gStyledItalicSystemFont) {
            return gStyledItalicSystemFont;
        }
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    UIFont* downloadedFont = nil;
    NSDictionary* messageDictionary = UXJSONOBJECT(category, object);
    if (messageDictionary) {
        NSArray* fontArray = [messageDictionary objectForKey:@"font-file"];
        if (fontArray && [fontArray isKindOfClass:[NSArray class]]) {
            NSString* fontName = [fontArray objectAtIndex:0];
            NSString* fontPath =[[CSSSkinManager manager].skinStyleSheet findFilePathByUrl:fontName];
            NSString* filePath = [NSBundle getPathForDocumentResource:[fontPath substringFromIndex:12]];
            if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
                NSURL * fonturl = [NSURL fileURLWithPath:filePath];
                CGDataProviderRef fontDataProvider = CGDataProviderCreateWithURL((__bridge CFURLRef)fonturl);
                CGFontRef newFont = CGFontCreateWithDataProvider(fontDataProvider);
                NSString* newFontName = (__bridge NSString *)CGFontCopyPostScriptName(newFont);
                CGDataProviderRelease(fontDataProvider);
                CFErrorRef fonterror;
                CTFontManagerRegisterGraphicsFont(newFont, &fonterror);
                CGFontRelease(newFont);
                
                downloadedFont = [UIFont fontWithName:newFontName size:14.0f];
            }
        }
    }
    
    if (style == kStyledSystemFontNormal) {
        gStyledSystemFont = downloadedFont;
        return gStyledSystemFont;
    } else if (style == kStyledSystemFontBold) {
        gStyledBoldSystemFont = downloadedFont;
        return gStyledBoldSystemFont;
    } else if (style == kStyledSystemFontItalic) {
        gStyledItalicSystemFont = downloadedFont;
        return gStyledItalicSystemFont;
    }
#endif
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    return nil;
}

@end
