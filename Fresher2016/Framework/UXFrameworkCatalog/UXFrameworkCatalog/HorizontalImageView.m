//
//  CTSliderView.m
//  CameraGPUImage
//
//  Created by BaoNQ on 6/27/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "HorizontalImageView.h"

@implementation HorizontalImageView {
    NSInteger _movedPage;
    CGPoint _lastContentOffset;
    NSInteger _curImageIndex;
    NSInteger _numberOfPages;
    
    int count;
    BOOL swipedLeftInPreviousSwipe;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self == nil)
        return nil;
    
    count = 0;
    swipedLeftInPreviousSwipe = YES;
    
    self.slider = [[UIScrollView alloc] initWithFrame:frame];
    
    self.slider.delegate = self;
    self.slider.pagingEnabled = true;
    self.slider.bounces = false;
    self.slider.showsHorizontalScrollIndicator = false;
    self.slider.showsVerticalScrollIndicator = false;
    self.slider.layer.zPosition = 1;
    
    [self addSubview:self.slider];
    
    self.pages = [[NSMutableArray alloc] init];
    
    return self;
}

- (void)reloadData {
    
    self.numberOfImages = [self.dataSource numberOfImages:self];
    
    _curImageIndex = 0;
    _movedPage = -1;
    _lastContentOffset = CGPointZero;
    
    // Load data
    self.slider.contentSize = CGSizeMake(self.frame.size.width * self.numberOfImages, self.frame.size.height);
    
    _numberOfPages = MIN(3, self.numberOfImages);
    // We just use maximum 3 pages to present the image list.
    for (int i = 0; i < _numberOfPages; ++i) {
        ImageView *filter = [[ImageView alloc] initWithFrame:self.frame];
        
        filter.image = [self.dataSource imageInHorizontalImageView:self atIndex:i];
        filter.layer.zPosition = 0;
       // [filter mask:filter.frame];
        [filter updateMask:filter.frame newXPosition:[self positionOfPageAtIndex:(i)]];
        
        [self.pages addObject:filter];
        [self addSubview:filter];
    }
    
    //Scroll the view to the starting offset
    [self.slider scrollRectToVisible:CGRectMake([self positionOfPageAtIndex:0], 0, self.frame.size.width, self.frame.size.height) animated:false];
}

- (CGFloat) positionOfPageAtIndex:(NSInteger)index {
    return self.frame.size.width * (float)(index);
}

- (NSInteger) previousPage:(NSInteger)curPage {
    // Get the previous page.
    // Ex: 2 -> 1, 1 -> 0, 0 -> 2
    NSInteger newPage = curPage - 1;
    if (newPage < 0)
        newPage = 2;
    return newPage;
}

//MARK: - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    // Update new position for 3 pages depending on current content offset of scroll view.
    for (int i = 0; i < _numberOfPages; ++i) {
        int newXpos = ([self positionOfPageAtIndex:(i)] - scrollView.contentOffset.x);
        newXpos = newXpos % (int)(3 * self.frame.size.width);
        
        if (abs(newXpos) > 2 * self.frame.size.width) {
            newXpos = 3 * self.frame.size.width - abs(newXpos);
        }
        /*
        while (fabs(newXpos) > 2 * self.frame.size.width) {
            newXpos = self.frame.size.width - (fabs(newXpos) - 2 * self.frame.size.width);
        }*/
        [self.pages[i] updateMask:self.pages[i].frame newXPosition:newXpos];
    }
    
    // If a page has been completely swiped
    if (fabs(_lastContentOffset.x - scrollView.contentOffset.x) >= self.frame.size.width) {
        if (count < 2)
            count++;
        
        BOOL flag = NO;
        
        // Swiped to right
        if (_lastContentOffset.x - scrollView.contentOffset.x > 0) {
            _lastContentOffset.x -= self.frame.size.width;
            _curImageIndex--;
            
            if (swipedLeftInPreviousSwipe == NO) {
                if (count >= 2) {
                    _movedPage = [self previousPage:_movedPage];
                    //_movedPage = labs((--_movedPage)%3);
                }
            }
            swipedLeftInPreviousSwipe = NO;
        }
        // Swiped to left
        else {
            _lastContentOffset.x += self.frame.size.width;
            _curImageIndex++;
            flag = YES;
            
            if (swipedLeftInPreviousSwipe == YES) {
                if (count >= 2) {
                    _movedPage = (++_movedPage)%3;
                }
            }
            swipedLeftInPreviousSwipe = YES;
        }
        
        // If a page has been moved.
        if (_movedPage != -1) {
            NSInteger nextImageIndex = _curImageIndex;
            if (flag)
                nextImageIndex++;
            else
                nextImageIndex--;
            
            // Set new image for this page.
            if (nextImageIndex >= 0 && nextImageIndex < self.numberOfImages)
                self.pages[_movedPage].image = [self.dataSource imageInHorizontalImageView:self atIndex:nextImageIndex];
        }
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    /*
    if (scrollView.contentOffset.x == [self positionOfPageAtIndex:(-1)]) {
        [self.slider scrollRectToVisible:CGRectMake([self positionOfPageAtIndex:(self.numberOfPages - 1)], 0, self.frame.size.width, self.frame.size.height) animated:false];
    }
    else if (scrollView.contentOffset.x == [self positionOfPageAtIndex:self.numberOfPages]) {
        [self.slider scrollRectToVisible:CGRectMake([self positionOfPageAtIndex:0], 0, self.frame.size.width, self.frame.size.height) animated:false];
    }
     */
    
}


@end
