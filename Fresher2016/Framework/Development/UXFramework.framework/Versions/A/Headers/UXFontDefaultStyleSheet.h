/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXDefaultStyleSheet.h"

@interface UXDefaultStyleSheet (UXFont)

- (UIFont*)customNormalFont10;
- (UIFont*)customBoldFont10;
- (UIFont*)customNormalFont11;
- (UIFont*)customBoldFont11;
- (UIFont*)customNormalFont12;
- (UIFont*)customBoldFont12;
- (UIFont*)customNormalFont13;
- (UIFont*)customBoldFont13;
- (UIFont*)customItalicFont13;
- (UIFont*)customNormalFont14;
- (UIFont*)customBoldFont14;
- (UIFont*)customNormalFont15;
- (UIFont*)customBoldFont15;
- (UIFont*)customNormalFont16;
- (UIFont*)customBoldFont16;
- (UIFont*)customNormalFont17;
- (UIFont*)customBoldFont17;
- (UIFont*)customNormalFont18;
- (UIFont*)customBoldFont18;
- (UIFont*)customNormalFont19;
- (UIFont*)customBoldFont19;
- (UIFont*)customNormalFont20;
- (UIFont*)customBoldFont20;
- (UIFont*)customNormalFont21;
- (UIFont*)customBoldFont21;
- (UIFont*)customNormalFont22;
- (UIFont*)customNormalFont24;
- (UIFont*)customBoldFont22;
- (UIFont*)customBoldFont24;

- (UIFont*)customNormalFont25;
- (UIFont*)customBoldFont25;

- (UIFont*)customNormalFont26;
- (UIFont*)customBoldFont26;

- (UIFont*)customNormalFont27;
- (UIFont*)customBoldFont27;

- (UIFont*)customNormalFont28;
- (UIFont*)customBoldFont28;

- (UIFont*)customNormalFont29;
- (UIFont*)customBoldFont29;

- (UIFont*)customNormalFont30;
- (UIFont*)customBoldFont30;

- (UIFont*)customNormalFont31;
- (UIFont*)customBoldFont31;

- (UIFont*)customNormalFont32;
- (UIFont*)customBoldFont32;

@end
