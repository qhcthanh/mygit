//
//  PickEffectViewController.h
//  CameraGPUImage
//
//  Created by qhcthanh on 6/23/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"

@protocol PickEffectViewControllerDelegate <NSObject>

@optional
-(void)didSelectFilter:(GPUImageOutput<GPUImageInput>* _Nonnull) filter;
@end

@interface PickEffectViewController : UIViewController

@property (nullable, nonatomic) id<PickEffectViewControllerDelegate> effectDelegate;
@property (nullable, weak) NSMutableArray<Class> *filterList;
@end
