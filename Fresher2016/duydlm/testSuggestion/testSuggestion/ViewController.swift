//
//  ViewController.swift
//  testSuggestion
//
//  Created by manhduydl on 8/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

var trieNode : LetterNode = LetterNode.init()
var phraseRootNode : LetterNode = LetterNode.init()
var letterRootNode : LetterNode = LetterNode.init()
var spaceInText : Bool = false
let suggestionManager = SuggestionManager.shareIntance
var filePath : String = ""

class ViewController: UIViewController , UITextFieldDelegate{

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("yếu".toASCII())
        textField.delegate = self
        
        let methodStart = NSDate()
        
        /* ... Do whatever you need to do ... */
        "anh  ".characterAtIndex("anh  ".length - 2)
        
        let methodFinish = NSDate()
        let executionTime = methodFinish.timeIntervalSinceDate(methodStart)
        print("Execution time: \(executionTime)")
        
//        var string = ["ảnh", "ánh", "anh"]
//        string = string.sort()
//        print(string)
//        
        filePath = NSHomeDirectory() + "/Documents/" + "personalPhrase"
        print(filePath)
//
//        var phrases = self.arrayFromContentsOfFileWithName("tempt")
//        phrases = phrases?.sort()
//        
//        var c = 3
//        var tempt = ""
//        for phrase : String in phrases! {
//
//            if phrase.words()[0] != tempt {
//                tempt = phrase.words()[0]
//                c = 3
//            }
//            
//            if c > 0 {
//                self.writeToFile(phrase, filePath: filePath)
//                c -= 1
//            }
//            
//            
//        }
        
        
//        var letterJsonString = ""
//        var relativeLetterJsonString = ""
//        var phraseJsonString = ""
//        let path1 = NSBundle.mainBundle().pathForResource("letterJson", ofType: "json")
//        let path2 = NSBundle.mainBundle().pathForResource("relativeLetterJson", ofType: "json")
//        let path3 = NSBundle.mainBundle().pathForResource("phraseJson", ofType: "json")
//        
//        do {
//            letterJsonString = try String(contentsOfFile:path1!, encoding: NSUTF8StringEncoding)
//            relativeLetterJsonString = try String(contentsOfFile:path2!, encoding: NSUTF8StringEncoding)
//            phraseJsonString = try String(contentsOfFile:path3!, encoding: NSUTF8StringEncoding)
//        } catch _ as NSError {
//            
//        }
//        
//        let defaults = NSUserDefaults.init(suiteName: Constants.userDefaultKey)
//        defaults!.setObject(letterJsonString, forKey: Constants.letterJsonStringKey)
//        defaults!.setObject(relativeLetterJsonString, forKey: Constants.relativeLetterJsonStringKey)
//        defaults!.setObject(phraseJsonString, forKey: Constants.phraseJsonStringKey)
        
        

        suggestionManager.setup(Constants.userDefaultKey, dispatchQueue: dispatch_get_main_queue()) { (rootNodeDictionary, error) in
            print("setup completed")
            print(rootNodeDictionary[Constants.phraseKey]?.getAllEndNode().count)
            
//            let methodStart = NSDate()
//            
//            /* ... Do whatever you need to do ... */
//            rootNodeDictionary[Constants.letterKey]?.getAllEndNode().count
//            rootNodeDictionary[Constants.phraseKey]?.getAllEndNode().count
//            
//            let methodFinish = NSDate()
//            let executionTime = methodFinish.timeIntervalSinceDate(methodStart)
//            print("Execution time get all node: \(executionTime)")
            
//            print(suggestionManager.existionOfPhrase("nhọ nồi"))
//            rootNodeDictionary[Constants.letterKey]?.printNode("", leaf: true)
 //           print(suggestionManager.removePhrase("nhọ nồi"))
 //           print(suggestionManager.existionOfPhrase("nhọ nồi"))
//            rootNodeDictionary[Constants.letterKey]?.printNode("", leaf: true)
//            print(suggestionManager.existionOfWord("an"))
            //rootNodeDictionary[Constants.phraseKey]?.printNode("", leaf: true)
            
            let data = "Văn xuôi trong giai đoạn này chủ yếu là truyện ngắn và ký về đề tài người nông dân và người lính Vệ quốc quân. Những nhà văn mà phần nhiều đồng thời cũng là lính Vệ quốc quân đã ghi lại những gì có tính chất thời sự đang xảy ra trên chiến trường như Truyện và ký sự của Trần Đăng, Ký sự Cao Lạng của Nguyễn Huy Tưởng, Xung kích của Nguyễn Đình Thi, Bên đường 12 của Tú Nam, Đường vui, Tình chiến dịch của Nguyễn Tuân... Những ký sự đó đã khắc họa chân dung của người lính mà thời ấy gọi là bộ đội Cụ Hồ trong đó ca ngợi những phẩm chất của họ như lòng yêu nước, thương nhà, tình đồng đội, tinh thần dũng cảm trong chiến đấu...Tuy vậy, để điển hình hóa nhân vật, trong những tác phẩm ấy sự cường điệu nét này hay nét khác của cá tính, hoặc sự nhấn mạnh như một cách minh họa tính giai cấp, có làm cho nhân vật ít nhiều hoặc sa vào sự cá biệt, hoặc sự minh họa.[7]. Truyện và truyện ngắn phong phú hơn về đề tài, từ người lính và cuộc chiến đấu trên chiến trường đến nông thôn, vùng cao, công nhân, trí thức... nhưng đều gắn liền với cuộc chiến tranh chống Pháp. Trận Phố Ràng, Một lần tới thủ đô, Một cuộc chuẩn bị,... đã đủ xác định vị trí hàng đầu của truyện ngắn Trần Đăng trong những năm đầu kháng chiến chống Pháp[8]. Nam Cao có Đôi mắt, Ở rừng,... trong đó Đôi mắt với chiều sâu hiện thực và tâm lý có ý nghĩa lâu dài trong cuộc sống cũng như văn chương[9]; Hồ Phương có Thư nhà. Tô Hoài đã khắc họa cuộc sống, con người miền núi với Truyện Tây Bắc (gồm Mường Giơn, Cứu đất cứu mường và Vợ chồng A Phủ). Người Tây Nguyên sống và đánh Pháp được Nguyên Ngọc miêu tả trong Đất nước đứng"
            
//            let methodStart = NSDate()
//            
//            /* ... Do whatever you need to do ... */
//            //suggestionManager.improveSuggestionWithSentence(data)
//            suggestionManager.updateDatabase(withUserDefaultKey: Constants.userDefaultKey, dispatchQueue: dispatch_get_main_queue(), completionHandler: { (defaults, error) in
//                
//            })
//            
//            let methodFinish = NSDate()
//            let executionTime = methodFinish.timeIntervalSinceDate(methodStart)
//            print("Execution time: \(executionTime)")
            
        }
         /// /////
        
       let importer = Importer.init()
//        
//        importer.buildTreeFromFile("test", dispatchQueue: dispatch_get_main_queue()) { (rootNode, error) in
//            trieNode = rootNode
//            
//            let jsonString = trieNode.toJsonString()
//            //print(jsonString)
//            let fileManager : File = File.init()
//            fileManager.write("tree", content: jsonString, completionHandle: { (filePath, error) in
//                print(filePath)
//            })
//            
////            fileManager.read("tree", completionHandle: { (content, error) in
////                let json = self.convertStringToDictionary(content)
////                for (_, v) in json! {
////                    for (k1, _) in v {
////                        if k1 as! String == "time" {
////                            let formatter = NSDateFormatter()
////                            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
////                            //print(formatter.dateFromString(v1 as! String))
////                            //print(NSDate().secondsFrom(formatter.dateFromString(v1 as! String)!))
////                        }
////                    }
////                }
////            })
//            
//        }
        
//        importer.buildPhraseTreeFromFile("test2", dispatchQueue: dispatch_get_main_queue()) { (rootNode, error) in
//            phraseRootNode = rootNode
//            let jsonString = rootNode.toJsonString()
//            print(jsonString)
//            let fileManager : File = File.init()
//            fileManager.write("perphraseJson", content: jsonString, completionHandle: { (filePath, error) in
//                print(filePath)
//            })
//        }
//
//        //print("anh yeeu em".lastWord())
//        importer.buildPhraseTreeFromFile("relativeLetter", dispatchQueue: dispatch_get_main_queue()) { (rootNode, error) in
//            letterRootNode = rootNode
//        }
        
        
    }
    
    func writeToFile(content: String, filePath: String) {
        
        let contentToAppend = content+"\n"
        //let filePath = NSHomeDirectory() + "/Documents/" + fileName
        
        //Check if file exists
        if let fileHandle = NSFileHandle(forWritingAtPath: filePath) {
            //Append to file
            fileHandle.seekToEndOfFile()
            fileHandle.writeData(contentToAppend.dataUsingEncoding(NSUTF8StringEncoding)!)
        }
        else {
            //Create new file
            do {
                try contentToAppend.writeToFile(filePath, atomically: true, encoding: NSUTF8StringEncoding)
            } catch {
                print("Error creating \(filePath)")
            }
        }
    }
//
//    func arrayFromContentsOfFileWithName(fileName: String) -> [String]? {
//        guard let path = NSBundle.mainBundle().pathForResource(fileName, ofType: "txt") else {
//            return nil
//        }
//        
//        do {
//            let content = try String(contentsOfFile:path, encoding: NSUTF8StringEncoding)
//            return content.componentsSeparatedByString("\n")
//        } catch _ as NSError {
//            return nil
//        }
//    }
    
//    // Parse Json to Dictionary
//    func convertStringToDictionary(text: String) -> [String:NSDictionary]? {
//        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
//            do {
//                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:NSDictionary]
//            } catch let error as NSError {
//                print(error)
//            }
//        }
//        return nil
//    }
//    
//    func updateData(withTree rootNode : LetterNode, completionHandle : (String,NSError)->Void) {
//        
//        let fileManager : File = File.shareInstance
//        fileManager.write("tree", content: rootNode.toJsonString(), completionHandle: { (filePath, error) in
//            print(filePath)
//        })
//    }
//    
//    /**
//     Increase count property of Node
//     
//     - parameter word:     word use to search Node
//     - parameter rootNode: rootNode to start search
//     */
//    func increaseCount(ofWord word : String, fromRootNode rootNode : LetterNode) {
//        let nodeNeedFind = self.find(word, fromRootNode: rootNode)
//        if nodeNeedFind != nil {
//            nodeNeedFind?.count += 1
//        }
//    }
//    
//    /**
//     Find node with special key in rootNode
//     
//     - parameter word:     key to find Node
//     - parameter rootNode: rootNode to start search
//     
//     - returns: return Node
//     */
//    func find(word : String, fromRootNode rootNode : LetterNode) -> LetterNode? {
//        var currentNode : LetterNode? = rootNode
//        
//        while (word.length != currentNode!.level) {
//            let searchKey = word.substringToIndex(currentNode!.level + 1)
//            currentNode = currentNode!.findChild(withKey: searchKey)
//            if currentNode == nil {
//                return nil
//            }
//        }
//        return currentNode
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    func suggestionWordWithPrefix(prefix : String) -> Array<String>! {
//        var result = Array<LetterNode>()
//        self.findWord(withPrefix: prefix, result: &result)
//        if result.count < Constants.numberOfSuggestion {
//            self.findRelativeWord(withPrefix: prefix, result: &result)
//        }
//        
//        var suggestionWords : [String] = []
//        if result.count > Constants.numberOfSuggestion {
//            result.sortInPlace({ (node1:LetterNode, node2:LetterNode) -> Bool in
//                node1.count >= node2.count
//            })
//            
//            for i in 0..<Constants.numberOfSuggestion   {
//                suggestionWords.append(result[i].key)
//            }
//        } else {
//            for node in result {
//                suggestionWords.append(node.key)
//            }
//        }
//        
//        if result.count == 0 {
//            suggestionWords.append(prefix)
//        }
//        
//        return suggestionWords
//    }
//    
//    func findWord(withPrefix prefix : String, inout result : Array<LetterNode>) {
//        let branchToUse = self.findBranchToSearch(fromPrefix: prefix)
//        
//        if branchToUse.level == 0 {
//            //result.append(prefix)
//            return
//        }
//        if branchToUse.isEnd == true {
//            result.append(branchToUse)
////            if result.count == Constants.numberOfSuggestion {
////                return
////            }
//        }
//        
//        self.findWord(fromBranch: branchToUse, result: &result)
//    }
//    
//    // Find branch to begin search from
//    func findBranchToSearch(fromPrefix prefix : String) -> LetterNode {
//        var currentNode = trieNode
//        
//        while prefix.length != currentNode.level {
//            let searchKey = prefix.substringToIndex(currentNode.level + 1)
//            
//            if let temptNode = currentNode.childrentNode[searchKey] {
//                currentNode = temptNode
//            } else {
//                return trieNode
//            }
//        }
//        
//        return currentNode
//    }
//    
//    // Find word from branch
//    func findWord(fromBranch branch : LetterNode, inout result : Array<LetterNode>) {
//        var remainNode = Array<LetterNode>()
//        
//        for (_, node) in branch.childrentNode {
//            if node.isEnd == true {
//                result.append(node)
////                if result.count == Constants.numberOfSuggestion {
////                    return
////                }
//                if node.childrentNode.count > 0 {
//                    remainNode.append(node)
//                }
//            } else {
//                remainNode.append(node)
//            }
//        }
//        
//        for node in remainNode {
//            self.findWord(fromBranch: node, result: &result)
////            if result.count == Constants.numberOfSuggestion {
////                return
////            }
//        }
//    }
//    
//    func findRelativeWord(withPrefix prefix : String, inout result : Array<LetterNode>) {
//        for i in (prefix.length - 1).stride(to: -1, by: -1) {
//            print(i)
//            let key : String = prefix.characterAtIndex(i)
//            var searchKey : String
//            
//            if let temptNode = letterRootNode.childrentNode[key] {
//                for (letter , _) in temptNode.childrentNode {
//                    if prefix.length >= 2 {
//                        searchKey = prefix.replace(withCharacter: letter, atIndex: i)
//                    } else {
//                        searchKey = letter
//                    }
//                    self.findWord(withPrefix: searchKey, result: &result)
////                    if result.count == Constants.numberOfSuggestion {
////                        return
////                    }
//                }
//            }
//        }
//    }
//    
//    // Suggestion 3 phrase from word
//    func suggestionPhraseWithWord(word : String) -> Array<String> {
//        var result = Array<String>()
//        
//        if let brandToFind = phraseRootNode.childrentNode[word] {
//            
//            // Sort dictionary and return array tuble
//            let array = brandToFind.childrentNode.sort({ (i1, i2) -> Bool in
//                if i1.1.priority > i2.1.priority {
//                    return true
//                }
//                return false
//            })
//            
//            for (key, _) in array {
//                result.append(key)
//                if result.count == Constants.numberOfSuggestion {
//                    return result
//                }
//            }
//            
//        } else {
//            result.append(word)
//        }
//        
//        return result
//    }

    @IBAction func textChanged(sender: UITextField) {
        //print(sender.text!)
        //var array : Array<String> = Array<String>()
        
        for i in 0..<Constants.numberOfSuggestion {
            let label : UILabel = self.view.viewWithTag(i+1) as! UILabel
            label.text = ""
        }
        
        suggestionManager.suggestWithInput(sender.text!, numberOfSuggestion: Constants.numberOfSuggestion, dispatchQueue: dispatch_get_main_queue()){ (array, error) in
            for i in 0..<3 {
                let label : UILabel = self.view.viewWithTag(i+1) as! UILabel
                if i < array.count {
                    label.text = array[i]
                }
            }
        }
    }

    @IBAction func updateData(sender: AnyObject) {
//        suggestionManager.improveSuggestionWithSentence(textField.text!)
//        suggestionManager.updateDatabase(withUserDefaultKey: Constants.userDefaultKey, dispatchQueue: dispatch_get_main_queue()) { (userDefaults, error) in
//            
//        }
        let phrases = textField.text!.phrases()
        
        for phrase in phrases {
            self.writeToFile(phrase, filePath: filePath)
        }
    }
    
//    func textFieldShouldReturn(textField: UITextField) -> Bool {
//        let phrases = textField.text!.phrases()
//        
//        for phrase in phrases {
//            self.writeToFile(phrase, filePath: filePath)
//        }
//        
//        textField.text = ""
//        return false
//    }
}

