/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXViewRecycler.h"
#import "UXPagingScrollView.h"

/**
 * A skeleton implementation of a page view.
 *
 * This view simply implements the required properties of UXPagingScrollViewPage.
 *
 */
@interface UXPagingScrollViewPage : UXRecyclableView <UXPagingScrollViewPage>
@property (nonatomic) NSInteger pageIndex;
@end
