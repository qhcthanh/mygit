/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIBezierPath.h>

@interface UIBezierPath (Extension)

+ (UIBezierPath*)bezierPathWithCurvedShadowForRect:(CGRect)rect;
+ (UIBezierPath*)bezierPathWithCurvedInsetShadowForRect:(CGRect)rect;

@end
