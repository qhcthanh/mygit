/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXLogFormatter.h"

@interface UXLog : NSObject

/**
 * Provides access to the underlying logging queue.
 * This may be helpful to Logger classes for things like thread synchronization.
 */
+ (dispatch_queue_t)loggingQueue;

/**
 * This method is used by the macros above.
 * It is suggested you stick with the macros as they're easier to use.
 */
+ (void)log:(BOOL)synchronous
      level:(int)level
       flag:(int)flag
    context:(int)context
       file:(const char *)file
   function:(const char *)function
       line:(int)line
     format:(NSString *)format, ...;

/**
 * Since logging can be asynchronous, there may be times when you want to flush the logs.
 * The framework invokes this automatically when the application quits.
 */
+ (void)flushLog;

/**
 * If you want your log statements to go somewhere, you should create and add a logger.
 */
+ (void)addLogger:(id <UXLogger>)logger;
+ (void)addLogger:(id <UXLogger>)logger withLogLevel:(int)logLevel;

+ (void)removeLogger:(id <UXLogger>)logger;

+ (void)removeAllLoggers;

/**
 * Registered Dynamic Logging
 * These methods allow you to obtain a list of classes that are using registered dynamic logging,
 * and also provides methods to get and set their log level during run time.
 */

+ (NSArray *)registeredClasses;
+ (NSArray *)registeredClassNames;

+ (int)logLevelForClass:(Class)aClass;
+ (int)logLevelForClassWithName:(NSString *)aClassName;

+ (void)setLogLevel:(int)logLevel forClass:(Class)aClass;
+ (void)setLogLevel:(int)logLevel forClassWithName:(NSString *)aClassName;

@end
