/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

extern NSBundle* gLocalizeBundle;

@interface NSBundle (Extension)

+ (NSLocale*)getCurrentLocale;

+ (NSString*)getLocalizedString:(NSString*)key withComment:(NSString*)comment;

+ (NSString*)createMediaFilePathFromUid:(NSString*)mediaId
                          withExtension:(NSString*)extends;

+ (NSString*)createMediaFilePathFromUidByUserId:(NSString*)userid
                                    withMediaId:(NSString*)mediaId
                                  withExtension:(NSString*)extends;

+ (NSString*)createMediaFilePathFromUidByOwnerId:(NSString*)ownerId
                                    withToUserId:(NSString*)toUserId
                                     withMediaId:(NSString*)mediaId
                                   withExtension:(NSString*)extends;

+ (NSString*)getEncryptedFilePathByUserIdForDownloading:(NSString*)ownerId
                                           withToUserId:(NSString*)toUserId
                                            withMediaId:(NSString*)mediaId
                                          withExtension:(NSString*)extends;

+ (NSString*)getEncryptedFilePathByUserIdForUploading:(NSString*)ownerId
                                         withToUserId:(NSString*)toUserId
                                          withMediaId:(NSString*)mediaId
                                        withExtension:(NSString*)extends;

@end
