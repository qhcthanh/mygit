/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXPreprocessor.h"

@class UXNavigationAppearanceSnapshot;

/**
 * The UXNavigationAppearance provides support for saving and restoring the navigation appearance
 * state.
 *
 * This class is now deprecated due to the ease with which it may cause more problems than it
 * solves. It is recommended that instead of obfuscating navigation appearance by using this class
 * that you define and follow a standard practice of modifying navigation appearance throughout your
 * app that is more explicit.
 *
 * Use this when you are about to mutate the navigation bar style and/or status
 * bar style, and you want to be able to restore these bar styles sometime in the
 * future.
 *
 * An example of usage for this pattern is in NIToolbarPhotoViewController which
 * changes the navigation bar style to UIBarStyleBlack and the status bar style to
 * UIStatusBarStyleBlack* in viewWillAppear:.
 *
 * @code
 * [UXNavigationAppearance pushAppearanceForNavigationController:self.navigationController]
 *
 * UINavigationBar* navBar = self.navigationController.navigationBar;
 * navBar.barStyle = UIBarStyleBlack;
 * navBar.translucent = YES;
 * @endcode
 *
 * Note that the call to UXNavigationAppearance must occur before mutating any bar
 * states so that it is able to capture the original state correctly.
 *
 * Then when NIToolbarPhotoViewController is ready to restore the original navigation
 * appearance state, (in viewWillDisappear:), it calls the following:
 *
 * @code
 * [UXNavigationAppearance popAppearanceForNavigationController:self.navigationController]
 * @endcode
 *
 * which pops the last snapshot of the stack and applies it, restoring the original
 * navigation appearance state.
 *
 * @ingroup UXFrameworkCore
 */
@interface UXNavigationAppearance : NSObject

/**
 * Take a snapshot of the current navigation appearance.
 *
 * Call this method before mutating the nav bar style or status bar style.
 *
 *    @fn UXNavigationAppearance::pushAppearanceForNavigationController:
 */
+ (void)pushAppearanceForNavigationController:(UINavigationController *)navigationController;

/**
 * Restore the last navigation appearance snapshot.
 *
 * Pops the last appearance values off the stack and applies them.
 *
 *    @fn UXNavigationAppearance::popAppearanceForNavigationController:animated:
 */
+ (void)popAppearanceForNavigationController:(UINavigationController *)navigationController animated:(BOOL)animated;

/**
 * Returns the number of items in the appearance stack.
 *
 *    @fn UXNavigationAppearance::count
 */
+ (NSInteger)count;

/**
 * Remove all navigation appearance snapshots from the stack.
 *
 *    @fn UXNavigationAppearance::clear
 */
+ (void)clear;

@end

