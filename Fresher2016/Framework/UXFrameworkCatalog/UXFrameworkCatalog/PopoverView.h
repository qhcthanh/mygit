//
//  PopoverOption.h
//  PopOver
//
//  Created by VanDao on 6/28/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PopoverType) {
    kPopoverTypeUp,
    kPopoverTypeDown
};

typedef void (^didShowHandlerBlock)();
typedef void (^didDismissHandlerBlock)();

@interface PopoverOptions : NSObject

@property(nonatomic) CGSize arrowSize;
@property(nonatomic) NSTimeInterval animationIn;
@property(nonatomic) NSTimeInterval animationOut;
@property(nonatomic) CGFloat cornerRadius;
@property(nonatomic) CGFloat sideEdge;
@property(strong, nonatomic) UIColor *blackOverlayColor;

@property(nonatomic) UIBlurEffectStyle overlayBlur;

@property(nonatomic) PopoverType poverType;
@property(strong, nonatomic) UIColor *color;

@end

@interface PopoverView : UIView

@property (nonatomic) CGSize arrowSize;
@property (nonatomic) NSTimeInterval animationIn;
@property (nonatomic) NSTimeInterval animationOut;
@property (nonatomic) CGFloat cornerRadius;
@property (nonatomic) CGFloat sizeEdge;
@property (nonatomic) PopoverType popoverType;
@property (strong, nonatomic) UIColor *blackOverlayColor;

@property (weak, nonatomic) UIBlurEffect *overlayBlur;

@property (strong, nonatomic) UIColor *popoverColor;

- (id)init;
- (id)initWithShowHandler:(didShowHandlerBlock)showHandler dismissHandler:(didDismissHandlerBlock)dismissHandler;
- (id)initWithOption:(PopoverOptions *)options showHandler:(didShowHandlerBlock)showHandler dismissHandler:(didDismissHandlerBlock)dismissHandler;
- (void)showContentView:(UIView *)viewContent fromView:(UIView *)viewFrom;
- (void)showContentView:(UIView *)viewContent fromView:(UIView *)viewFrom inView:(UIView *)viewIn;
- (void)showContentView:(UIView *)viewContent point:(CGPoint)point;
- (void)showContentView:(UIView *)viewContent point:(CGPoint)point inView:(UIView *)viewIn;
- (void)dismiss;

@end
