/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "UXStyleSheet.h"

@class UXShape;

@interface UXDefaultStyleSheet : UXStyleSheet 

// Common styles
@property (nonatomic, readonly) UIColor*  textColor;
@property (nonatomic, readonly) UIColor*  highlightedTextColor;
@property (nonatomic, readonly) UIFont*   font;
@property (nonatomic, readonly) UIColor*  backgroundColor;
@property (nonatomic, readonly) UIColor*  loadingBackgroundColor;
@property (nonatomic, readonly) UIColor*  navigationBarTintColor;
@property (nonatomic, readonly) UIColor*  navigationBarTitleColor;
@property (nonatomic, readonly) UIColor*  toolbarTintColor;
@property (nonatomic, readonly) UIColor*  searchBarTextColor;
@property (nonatomic, readonly) UIColor*  searchBarTintColor;
@property (nonatomic, readonly) UIColor*  brandTintColor;
@property (nonatomic, readonly) UIColor*  textPlaceHolderColor;
@property (nonatomic, readonly) CGFloat   navigationBarTransluent;
@property (nonatomic, readonly) UIColor*  gradientDarkTintColor;
@property (nonatomic, readonly) UIColor*  gradientLightTintColor;

// Tables
@property (nonatomic, readonly) UIColor*  tablePlainBackgroundColor;
@property (nonatomic, readonly) UIColor*  tableGroupedBackgroundColor;
@property (nonatomic, readonly) UIColor*  searchTableBackgroundColor;
@property (nonatomic, readonly) UIColor*  searchTableSeparatorColor;

// Table Headers
@property (nonatomic, readonly) UIColor*  tableHeaderTextColor;
@property (nonatomic, readonly) UIColor*  tableHeaderShadowColor;
@property (nonatomic, readonly) CGSize    tableHeaderShadowOffset;
@property (nonatomic, readonly) UIColor*  tableHeaderTintColor;

// Table Cells
@property (nonatomic, readonly) UIColor*  tableContentViewBackgroundColor;
@property (nonatomic, readonly) UIColor*  tableSelectedViewBackgroundColor;
@property (nonatomic, readonly) UIColor*  tableHighlightViewBackgroundColor;
@property (nonatomic, readonly) UIColor*  groupTableContentViewBackgroundColor;
@property (nonatomic, readonly) UIColor*  messageTableContentViewBackgroundColor;
@property (nonatomic, readonly) UIColor*  tableSeparatorColor;

// Sectioned Title
@property (nonatomic, readonly) UIColor*  tableSectionedTitleColor;
@property (nonatomic, readonly) UIColor*  tableSectionedBackgroundColor;

// Photo Captions
@property (nonatomic, readonly) UIColor*  photoCaptionTextColor;
@property (nonatomic, readonly) UIColor*  photoCaptionTextShadowColor;
@property (nonatomic, readonly) CGSize    photoCaptionTextShadowOffset;

@property (nonatomic, readonly) UIColor*  timestampTextColor;
@property (nonatomic, readonly) UIColor*  linkTextColor;
@property (nonatomic, readonly) UIColor*  moreLinkTextColor;

@property (nonatomic, readonly) UIColor* screenBackgroundColor;

@property (nonatomic, readonly) UIColor* tableActivityTextColor;
@property (nonatomic, readonly) UIColor* tableErrorTextColor;
@property (nonatomic, readonly) UIColor* tableSubTextColor;
@property (nonatomic, readonly) UIColor* tableTitleTextColor;

@property (nonatomic, readonly) UIColor* tableDescriptionTextColor;

@property (nonatomic, readonly) UIColor* tabTintColor;
@property (nonatomic, readonly) UIColor* tabBarTintColor;

@property (nonatomic, readonly) UIColor* tabBarTextNormalColor;
@property (nonatomic, readonly) UIColor* tabBarTextSelectedColor;

@property (nonatomic, readonly) UIColor* messageFieldTextColor;
@property (nonatomic, readonly) UIColor* messageFieldSeparatorColor;

@property (nonatomic, readonly) UIColor* thumbnailBackgroundColor;

@property (nonatomic, readonly) UIColor* postButtonColor;
@property (nonatomic, readonly) UIColor* launcherBackgroundColor;

@property (nonatomic, readonly) UIColor* subTabbarBackgroundColor;
@property (nonatomic, readonly) UIColor* subTabbarButtonHightlightColor;
@property (nonatomic, readonly) UIColor* subTabbarVerticalSeperatorColor;
@property (nonatomic, readonly) UIColor* subTabbarHorizontalSeperatorColor;
@property (nonatomic, readonly) UIColor* bubbleBackgroundButtonNormalColor;
@property (nonatomic, readonly) UIColor* bubbleBackgroundButtonSelectedColor;

@property (nonatomic, readonly) UIFont* linkFont;
@property (nonatomic, readonly) UIFont* buttonFont;
@property (nonatomic, readonly) UIFont* tableFont;
@property (nonatomic, readonly) UIFont* tableSmallFont;
@property (nonatomic, readonly) UIFont* tableTitleFont;

// Sectioned title
@property (nonatomic, readonly) UIFont* tableSectionedTitleFont;

@property (nonatomic, readonly) UIFont* tableDescriptionFont;
@property (nonatomic, readonly) UIFont* tableTimestampFont;
@property (nonatomic, readonly) UIFont* tableButtonFont;
@property (nonatomic, readonly) UIFont* tableSummaryFont;
@property (nonatomic, readonly) UIFont* tableHeaderPlainFont;
@property (nonatomic, readonly) UIFont* tableHeaderGroupedFont;
@property (nonatomic, readonly) CGFloat tableBannerViewHeight;
@property (nonatomic, readonly) UIFont* photoCaptionFont;
@property (nonatomic, readonly) UIFont* messageFont;
@property (nonatomic, readonly) UIFont* errorTitleFont;
@property (nonatomic, readonly) UIFont* errorSubtitleFont;
@property (nonatomic, readonly) UIFont* activityLabelFont;
@property (nonatomic, readonly) UIFont* activityBannerFont;
@property (nonatomic, readonly) UIFont* navigationBarFont;

@property (nonatomic, readonly) UITableViewCellSelectionStyle tableSelectionStyle;

- (UXStyle*)pageDotWithColor:(UIColor*)color;

- (UXStyle*)selectionFillStyle:(UXStyle*)next;

- (UXStyle*)toolbarButtonForState:(UIControlState)state shape:(UXShape*)shape
						tintColor:(UIColor*)tintColor font:(UIFont*)font;

- (UXStyle*)launcherBackgroundGradientFillStyle;

@end
