/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@class UXSQLDatabaseQueue;

@interface UXSQLBackgroundQueue : NSObject

@property (nonatomic, assign) dispatch_queue_t dispatchQueue;
@property (nonatomic, retain) UXSQLDatabaseQueue* sqlDatabaseQueue;

/**
 *  Initialize a database instance with name.
 *
 *  @param databaseName The name of database that must be NOT NULL
 *  @param queue        The dispatch queue that dispatches result after executing SQL command.
 *
 *  @return A database instance
 */
- (instancetype)initWithDatabaseName:(NSString*)databaseName dispatchQueue:(dispatch_queue_t)queue;

/**
 *  Excute a SQL command asynchronuosly.
 *
 *  @param sqlCommand sqlCommand The sqlCommand will be excuted.
 *  @param success    A block object to be executed when the request finishes successfully.
 *  @param failure    A block object to be executed when the request finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data.
 *  @param queue      A dispatch queue that result is dispatched into the queue.
 */
- (void)excuteUpdateWithSQL:(NSString*)sqlCommand success:(void(^)(id responseObject))success failure:(void(^)(NSError* error))failure dispatchQueue:(dispatch_queue_t)queue;

/**
 *  Excute a SQL command asynchronuosly with a priority.
 *
 *  @param sqlCommand sqlCommand The sqlCommand will be excuted.
 *  @param priority   A priority you wanted to set.
 *  @param success    A block object to be executed when the request finishes successfully.
 *  @param failure    A block object to be executed when the request finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data.
 *  @param queue      A dispatch queue that result is dispatched into the queue.
 */
- (void)excuteUpdateWithSQL:(NSString*)sqlCommand priority:(NSOperationQueuePriority)priority success:(void(^)(id responseObject))success failure:(void(^)(NSError* error))failure dispatchQueue:(dispatch_queue_t)queue;

/**
 *  Excute a SQL command asynchronuosly.
 *
 *  @param sqlCommand The sqlCommand will be excuted.
 *  @param success    A block object to be executed when the request finishes successfully.
 *  @param failure    A block object to be executed when the request finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data.
 *  @param queue      A dispatch queue that result is dispatched into the queue.
 */
- (void)excuteQueryWithSQL:(NSString*)sqlCommand success:(void(^)(id responseObject))success failure:(void(^)(NSError* error))failure dispatchQueue:(dispatch_queue_t)queue;

/**
 *  Excute a SQL command asynchronuosly with a priority.
 *
 *  @param sqlCommand The sqlCommand will be excuted.
 *  @param success    A block object to be executed when the request finishes successfully.
 *  @param failure    A block object to be executed when the request finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data.
 *  @param queue      A dispatch queue that result is dispatched into the queue.
 */
- (void)excuteQueryWithSQL:(NSString*)sqlCommand priority:(NSOperationQueuePriority)priority  success:(void(^)(id responseObject))success failure:(void(^)(NSError* error))failure dispatchQueue:(dispatch_queue_t)queue;

/**
 *  Insert object array into a database instance.
 *
 *  @param dataSet The array will be inserted into database.
 *  @param success A block object to be executed when the request finishes successfully.
 *  @param failure A block object to be executed when the request finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data.
 *  @param queue   A dispatch queue that result is dispatched into the queue.
 */
- (void)insertOrReplaceWithDataSet:(NSArray*)dataSet success:(void(^)(id responseObject))success failure:(void(^)(NSError* error))failure dispatchQueue:(dispatch_queue_t)queue;

@end
