/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "HorizontalViewController.h"

#import "PageView.h"
#import "CTFilterView.h"
#import <UXFramework/UXFramework.h>

@interface HorizontalViewController ()<UXPagingScrollViewDataSource, UXPagingScrollViewDelegate>
// We must retain the paging scroll view in order to autorotate it correctly.
@property (nonatomic, retain) UXPagingScrollView* pagingScrollView;

@end

@implementation HorizontalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        self.titleString = @"Horizontal Scrollview";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // iOS 7-only.
    self.view.backgroundColor = [UIColor blackColor];
    
    // Create a paging scroll view the same way we would any other type of view.
    self.pagingScrollView = [[UXPagingScrollView alloc] initWithFrame:self.view.bounds];
    self.pagingScrollView.autoresizingMask = UIViewAutoresizingFlexibleDimensions;
    
    // A paging scroll view has a data source much like a UITableView.
    self.pagingScrollView.delegate = self;
    self.pagingScrollView.dataSource = self;
    
    [self.view addSubview:self.pagingScrollView];
    
    // Tells the paging scroll view to ask the dataSource for information about how to present itself.
    [self.pagingScrollView reloadData];
}

- (void)didReceiveMemoryWarning {
    self.pagingScrollView = nil;
    
    [super didReceiveMemoryWarning];
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    return UXIsSupportedOrientation(interfaceOrientation);
//}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    // The paging scroll view implements autorotation internally so that the current visible page
    // index is maintained correctly. It also provides an opportunity for each visible page view to
    // maintain zoom information correctly.
    [self.pagingScrollView willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                         duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    // The second part of the paging scroll view's autorotation functionality. Both of these methods
    // must be called in order for the paging scroll view to rotate itself correctly.
    [self.pagingScrollView willAnimateRotationToInterfaceOrientation:toInterfaceOrientation
                                                            duration:duration];
}

// The paging scroll view data source works similarly to UITableViewDataSource. We will return
// the total number of pages in the scroll view as well as each page as it is about to be displayed.
#pragma mark - UXPagingScrollViewDataSource

- (NSInteger)numberOfPagesInPagingScrollView:(UXPagingScrollView *)pagingScrollView {
    // For the sake of this example we'll show a fixed number of pages.
    return 10;
}

// Similar to UITableViewDataSource, we create each page view on demand as the user is scrolling
// through the page view.
// Unlike UITableViewDataSource, this method requests a UIView that conforms to a protocol, rather
// than requiring a specific subclass of a type of view. This allows you to use any UIView as long
// as it conforms to UXPagingScrollView.
- (UIView<UXPagingScrollViewPage> *)pagingScrollView:(UXPagingScrollView *)pagingScrollView
                                    pageViewForIndex:(NSInteger)pageIndex {
    // Check the reusable page queue.
    CTFilterView *page = (CTFilterView *)[pagingScrollView dequeueReusablePageWithIdentifier:NSStringFromClass([CTFilterView class])];
    // If no page was in the reusable queue, we need to create one.
    if (nil == page) {
        page = [[CTFilterView alloc] initWithReuseIdentifier:NSStringFromClass([CTFilterView class])];
    }
    
    NSString *name = [NSString stringWithFormat:@"image%li.png", (long)pageIndex];
    page.image = [UIImage imageNamed:name];
    
    return page;
}

- (CGFloat) positionOfPageAtIndex:(NSUInteger)index {
    return self.view.width * (CGFloat)(index);
}

- (void)pagingScrollViewDidScroll:(UXPagingScrollView *)pagingScrollView {
    UX_CONSOLE_LOGGER(@"pagingScrollViewDidScroll");
    
    NSInteger indx = 0;
    for (UIView <UXPagingScrollViewPage>* page in pagingScrollView.visiblePages) {
        if ([page isKindOfClass:[CTFilterView class]]) {
            int pos = ([self positionOfPageAtIndex:indx] - pagingScrollView.scrollView.contentOffset.x);
            [(CTFilterView*)page updateMask:page.frame newXPosition:pos];
            indx++;
            NSLog(@"postion %ld", indx);
        }
    }
}

#pragma mark Changing Pages /** @name [UXPagingScrollViewDelegate] Changing Pages */

/**
 * The current page will change.
 *
 * pagingScrollView.centerPageIndex will reflect the old page index, not the new
 * page index.
 */
- (void)pagingScrollViewWillChangePages:(UXPagingScrollView *)pagingScrollView {
    UX_CONSOLE_LOGGER(@"pagingScrollViewWillChangePages");
}

/**
 * The current page has changed.
 *
 * pagingScrollView.centerPageIndex will reflect the changed page index.
 */
- (void)pagingScrollViewDidChangePages:(UXPagingScrollView *)pagingScrollView {
    UX_CONSOLE_LOGGER(@"pagingScrollViewWillChangePages");
}

@end
