/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXLauncherView.h"
#import "UXLauncherViewModel.h"

/**
 * A launcher button view that displays an image and and a label beneath it.
 *
 * This view shows the icon anchored to the top middle of the view and the label anchored to the
 * bottom middle. By default the label is a single line label with NSLineBreakByTruncatingTail.
 *
 * @image html UXLauncherButtonExample1.png "Example of an UXLauncherButton"
 *
 */
@interface UXLauncherButtonView : UXRecyclableView <UXLauncherButtonView, UXLauncherViewObjectView>

/**
 * The button view that should be used to display the launcher icon.
 *
 */
@property (nonatomic, strong) UIButton* button;

/**
 * The label view that should show the title of the launcher item.
 *
 */
@property (nonatomic, copy) UILabel* label;

/**
 * The distance that the button and label are inset from the enclosing view.
 *
 * The unit of size is points. The default value is 5 points on all sides.
 *
 */
@property (nonatomic, assign) UIEdgeInsets contentInset;

@end

