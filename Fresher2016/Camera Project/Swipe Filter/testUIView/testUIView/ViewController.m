//
//  ViewController.m
//  testUIView
//
//  Created by VanDao on 6/23/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    UXOverlayView *stickerOverlayView;
    BOOL isInTrash;
    NSMutableArray *_images;
    HorizontalImageView *sliderView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //Scroll view
    sliderView = [[HorizontalImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    sliderView.dataSource = self;
    //Create data
    _images = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 18; ++i) {
        NSString *name = [NSString stringWithFormat:@"image%i.png", i];
        [_images addObject:[UIImage imageNamed:name]];
    }
    
    [self.view addSubview:sliderView];
    
    [sliderView reloadData];
    
    self.trashImage.image = [UIImage imageNamed:@"trash.png"];
    isInTrash = NO;
    
    stickerOverlayView = [[UXOverlayView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    [self.view addSubview:stickerOverlayView];
    [self.view bringSubviewToFront:self.addStickerOutlet];
    [self.view bringSubviewToFront:self.addTextOutlet];
    [self.view bringSubviewToFront:self.contentTextField];
    stickerOverlayView.touchDelegate = self;
    stickerOverlayView.userInteractionEnabled = YES;
    stickerOverlayView.nextView = sliderView;
    
    self.automaticallyAdjustsScrollViewInsets = YES;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.contentTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)SaveAction:(id)sender{
    static NSInteger nextIndex = 0;
    nextIndex = nextIndex == 0 ? 17 : 0;
    //nextIndex = random() % 17;
    
  //  NSLog(@"Next: %li", nextIndex);
    [sliderView moveToPosition:nextIndex];
   // nextIndex = random() % 17;
}

- (IBAction)addSticker:(id)sender {
    UXImageTransitionView *newSticker = [[UXImageTransitionView alloc] initWithImage:[UIImage imageNamed:@"image.png"]];
    [stickerOverlayView addSticker:[[UXImageTransitionDetail alloc] initWithTransitionView:newSticker]];
}
- (IBAction)addText:(id)sender {
    UXTextTransitionView *newSticker = [[UXTextTransitionView alloc]initWithText:self.contentTextField.text textColor:[UIColor redColor]];
    [stickerOverlayView addSticker:[[UXTextTransitionDetail alloc] initWithTransitionView:newSticker]];
}

- (void)didTouchMoveSticker:(id)sticker position:(CGPoint)position{
    if (CGRectContainsPoint(self.trashImage.frame, position) && !isInTrash){
       // [sticker setBlurImage];
        ((UXTransitionDetail *)sticker).transitionView.alpha = 0.4;
        
        CGRect frame = self.trashImage.frame;
        self.trashImage.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width * 2, frame.size.height * 2);
        isInTrash = YES;
    }
    else if (!CGRectContainsPoint(self.trashImage.frame, position) && isInTrash){
       // [sticker setNormalImage];
        ((UXTransitionDetail *)sticker).transitionView.alpha = 1;

        CGRect frame = self.trashImage.frame;
        self.trashImage.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width/2, frame.size.height/2);
        isInTrash = NO;
    }
}

- (void)didTouchEndSticker:(id)sticker position:(CGPoint)position{
    if (CGRectContainsPoint(self.trashImage.frame, position)){
        [stickerOverlayView removeSticker:sticker];
        
        CGRect frame = self.trashImage.frame;
        self.trashImage.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width/2, frame.size.height/2);
        isInTrash = NO;
    }
}

- (void)didTouchUpInsideTextSticker:(UXTextTransitionView *)textSticker{
    
}

//MARK: - HorizontalImageView DataSource

- (NSInteger)numberOfImages:(HorizontalImageView *)horizontalImageView {
    return _images.count;
}

- (UIImage *)imageInHorizontalImageView:(HorizontalImageView *)horizontalImageView atIndex:(NSInteger)index {
    return _images[index];
}

@end
