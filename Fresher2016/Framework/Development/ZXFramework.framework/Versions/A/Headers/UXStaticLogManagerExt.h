/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@class UXLogMessage;
@interface UXStaticLogManagerExt : NSObject

+ (UXStaticLogManagerExt*)getInstanceWithCallback:(id)delegate;

@end
