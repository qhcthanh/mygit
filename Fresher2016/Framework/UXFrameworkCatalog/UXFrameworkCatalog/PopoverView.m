//
//  PopoverOption.m
//  PopOver
//
//  Created by VanDao on 6/28/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import "PopoverView.h"

@interface PopoverView() {
    didShowHandlerBlock didShowHandler;
    didDismissHandlerBlock didDismissHandler;
    UIControl *blackOverlay;
    UIView *containerView;
    UIView *contentView;
    CGRect contentViewFrame;
    CGPoint arrowShowPoint;
    
    UIView *subView;
}

@end

@implementation PopoverView


- (id)init{
    self = [super init];
    
    if (self) {
        self.arrowSize = CGSizeMake(16.0f, 10.0f);
        self.animationIn = 0.6;
        self.animationOut = 0.3;
        self.cornerRadius = 6.0f;
        self.sizeEdge = 20.0f;
        self.popoverType = kPopoverTypeDown;
        self.blackOverlayColor = [UIColor clearColor];//[UIColor colorWithWhite:0.0f alpha:0.2f];

        if (UXIsSupportiOS8()) {
            self.overlayBlur = nil;
        }
        
        self.popoverColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];//RGBACOLOR(0, 0, 0, 0.5);
        
        blackOverlay = [[UIControl alloc] init];
        
        self.backgroundColor = [UIColor clearColor];
        self.frame = CGRectZero;
    }
    
    return self;
}

- (void)dealloc {
 //   UX_CONSOLE_LOGGER(@"dealloc popover...");
}

- (void)addContentSubview:(UIView *)view{
    subView = view;
}

- (id)initWithShowHandler:(didShowHandlerBlock)showHandler dismissHandler:(didDismissHandlerBlock)dismissHandler {
    self = [self init];
    
    if (self) {
        didShowHandler = showHandler;
        didDismissHandler = dismissHandler;
    }
    
    return self;
}

- (id)initWithOption:(PopoverOptions *)options showHandler:(didShowHandlerBlock)showHandler dismissHandler:(didDismissHandlerBlock)dismissHandler {
    self = [self initWithShowHandler:showHandler dismissHandler:dismissHandler];
    
    if (self) {
        [self setOptions:options];
    }
    
    return self;
}

- (void)setOptions:(PopoverOptions *)options {
    if (options) {
        self.arrowSize = options.arrowSize;
        self.animationIn = options.animationIn;
        self.animationOut = options.animationOut;
        self.cornerRadius = options.cornerRadius;
        self.sizeEdge = options.sideEdge;
        self.blackOverlayColor = options.blackOverlayColor;
        
        if (UXIsSupportiOS8()) {
            self.overlayBlur = [UIBlurEffect effectWithStyle:options.overlayBlur];
        }
        
        self.popoverType = options.poverType;
        self.popoverColor = options.color;
    }
}

- (void)create {
    CGRect frame = contentView.frame;
    frame.origin.x = arrowShowPoint.x - frame.size.width * 0.5;
    
    CGFloat sizeEdge = 0.0f;
    if (frame.size.width < containerView.frame.size.width) {
        sizeEdge = self.sizeEdge;
    }
    
    CGFloat outerSideEdge = CGRectGetMaxX(frame) - containerView.bounds.size.width;
    if (outerSideEdge > 0) {
        frame.origin.x = (containerView.bounds.size.width - frame.size.width)/2;
    } else {
        if (CGRectGetMinX(frame) < 0) {
            frame.origin.x = (containerView.bounds.size.width - frame.size.width)/2;
        }
    }
    
    self.frame = frame;
    
    CGPoint arrowPoint = [containerView convertPoint:arrowShowPoint toView:self];
    CGPoint anchorPoint;
    switch (self.popoverType) {
        case kPopoverTypeUp:
            frame.origin.y = arrowShowPoint.y - frame.size.height - self.arrowSize.height;
            anchorPoint = CGPointMake(arrowPoint.x / frame.size.width, 1);
            break;
            
        case kPopoverTypeDown:
            frame.origin.y = arrowShowPoint.y;
            anchorPoint = CGPointMake(arrowPoint.x / frame.size.width, 0);
            break;
    }
    
    CGPoint lastAnchor = self.layer.anchorPoint;
    self.layer.anchorPoint = anchorPoint;
    CGFloat x = self.layer.position.x + (anchorPoint.x - lastAnchor.x) * self.layer.bounds.size.width;
    CGFloat y = self.layer.position.y + (anchorPoint.y - lastAnchor.y) * self.layer.bounds.size.height;
    self.layer.position = CGPointMake(x, y);
    
    frame.size.height += self.arrowSize.height;
    self.frame = frame;
}

- (void)showContentView:(UIView *)viewContent fromView:(UIView *)viewFrom {
    [self showContentView:viewContent fromView:viewFrom inView:[[UIApplication sharedApplication] keyWindow]];
}

- (void)showContentView:(UIView *)viewContent fromView:(UIView *)viewFrom inView:(UIView *)viewIn {
    CGPoint point;
    switch (self.popoverType) {
        case kPopoverTypeUp:
            point = [viewIn convertPoint:CGPointMake(viewFrom.frame.origin.x + (viewFrom.frame.size.width / 2), viewFrom.frame.origin.y) fromView:viewFrom.superview];
            break;
            
        case kPopoverTypeDown:
            point = [viewIn convertPoint:CGPointMake(viewFrom.frame.origin.x + viewFrom.frame.size.width/2, viewFrom.frame.origin.y) fromView:viewFrom.superview];
            
            break;
    }
    
    [self showContentView:viewContent point:point inView:viewIn];
}
     
- (void)showContentView:(UIView *)viewContent point:(CGPoint)point {
    [self showContentView:viewContent point:point inView:[[UIApplication sharedApplication]keyWindow]];
}

- (void)showContentView:(UIView *)viewContent point:(CGPoint)point inView:(UIView *)viewIn {
    blackOverlay.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    blackOverlay.frame = viewIn.bounds;
    
    if (UXIsSupportiOS8()) {
        if (self.overlayBlur) {
            UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:self.overlayBlur];
            effectView.frame = blackOverlay.bounds;
            effectView.userInteractionEnabled = NO;
            [blackOverlay addSubview:effectView];
        } else {
            blackOverlay.backgroundColor = self.blackOverlayColor;
            blackOverlay.alpha = 0.0f;
        }
    } else {
        blackOverlay.backgroundColor = self.blackOverlayColor;
        blackOverlay.alpha = 0.0f;
    }
    
    
    [viewIn addSubview:blackOverlay];
    [blackOverlay addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    
    containerView = viewIn;
    contentView = viewContent;
    contentView.backgroundColor = [UIColor clearColor];
    contentView.layer.cornerRadius = self.cornerRadius;
    contentView.layer.masksToBounds = YES;
    arrowShowPoint = point;
    [self show];
}

- (void)show {
    [self setNeedsDisplay];
    CGRect frame = contentView.frame;
    switch (self.popoverType) {
        case kPopoverTypeUp:
            frame.origin.y = 0.0f;
            break;
        case kPopoverTypeDown:
            frame.origin.y = self.arrowSize.height;
            break;
    }
    contentView.frame = frame;
    
    [self addSubview:contentView];
    [containerView addSubview:self];
    
    [self create];
    
    [contentView addSubview:subView];
    /*
    frame.origin.y += 10;
    frame.size.height -= 20;
    frame.size.width  -= 2*contentView.left;
    contentView.frame = frame;
    */
    
    self.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
    [UIView animateWithDuration:self.animationIn delay:0 usingSpringWithDamping:0.7 initialSpringVelocity:3 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        if (finished) {
            if (didShowHandler)
                dispatch_async(dispatch_get_main_queue(), didShowHandler);
        }
    }];
    
    [UIView animateKeyframesWithDuration:self.animationIn/3 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
        blackOverlay.alpha = 1;
    } completion:nil];
}

- (void)dismiss {
    if (self.superview) {
        [UIView animateWithDuration:self.animationOut delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.transform = CGAffineTransformMakeScale(0.0001, 0.0001);
            blackOverlay.alpha = 0.0f;
        } completion:^(BOOL finished) {
            if (finished) {
                [contentView removeFromSuperview];
                [blackOverlay removeFromSuperview];
                [self removeFromSuperview];
                if (didDismissHandler)
                    dispatch_async(dispatch_get_main_queue(), didDismissHandler);
            }
        }];
    }
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    UIBezierPath *arrow = [[UIBezierPath alloc]init];
    UIColor *color = self.popoverColor;
    CGPoint arrowPoint = [containerView convertPoint:arrowShowPoint toView:self];
    switch (self.popoverType) {
        case kPopoverTypeUp:
            [arrow moveToPoint:CGPointMake(arrowPoint.x, self.bounds.size.height)];
            [arrow addLineToPoint:CGPointMake(arrowPoint.x - self.arrowSize.width * 0.5, [self isCornerLeftArrow] ? self.arrowSize.height : (self.bounds.size.height - self.arrowSize.height))];
            
            [arrow addLineToPoint:CGPointMake(self.cornerRadius, self.bounds.size.height - self.arrowSize.height)];
            [arrow addArcWithCenter:CGPointMake(self.cornerRadius, self.bounds.size.height - self.arrowSize.height - self.cornerRadius)
                             radius:self.cornerRadius
                         startAngle:[self radiansFromDegree:90]
                           endAngle:[self radiansFromDegree:180]
                          clockwise:YES];
            
            [arrow addLineToPoint:CGPointMake(0, self.cornerRadius)];
            [arrow addArcWithCenter:CGPointMake(self.cornerRadius, self.cornerRadius)
                             radius:self.cornerRadius
                         startAngle:[self radiansFromDegree:180]
                           endAngle:[self radiansFromDegree:270]
                          clockwise:YES];
            
            [arrow addLineToPoint:CGPointMake(self.bounds.size.width - self.cornerRadius, 0)];
            [arrow addArcWithCenter:CGPointMake(self.bounds.size.width - self.cornerRadius, self.cornerRadius)
                             radius:self.cornerRadius
                         startAngle:[self radiansFromDegree:270]
                           endAngle:[self radiansFromDegree:0]
                          clockwise:YES];
            
            
            [arrow addLineToPoint:CGPointMake(self.bounds.size.width, self.bounds.size.height - self.arrowSize.height - self.cornerRadius)];
            [arrow addArcWithCenter:CGPointMake(self.bounds.size.width - self.cornerRadius, self.bounds.size.height - self.arrowSize.height - self.cornerRadius)
                             radius:self.cornerRadius
                         startAngle:[self radiansFromDegree:0]
                           endAngle:[self radiansFromDegree:90]
                          clockwise:YES];
            
            [arrow addLineToPoint:CGPointMake(arrowPoint.x + self.arrowSize.width * 0.5, [self isCornerRightArrow] ? self.arrowSize.height : (self.bounds.size.height - self.arrowSize.height))];

            break;
            
        case kPopoverTypeDown:
            [arrow moveToPoint:CGPointMake(arrowPoint.x, 0)];
            [arrow addLineToPoint:CGPointMake(arrowPoint.x + self.arrowSize.width * 0.5, [self isCornerRightArrow] ? (self.arrowSize.height + self.bounds.size.height): self.arrowSize.height)];
        
            
            [arrow addLineToPoint:CGPointMake(self.bounds.size.width - self.cornerRadius, self.arrowSize.height)];
            [arrow addArcWithCenter:CGPointMake(self.bounds.size.width - self.cornerRadius, self.arrowSize.height + self.cornerRadius)
                             radius:self.cornerRadius
                         startAngle:[self radiansFromDegree:270]
                           endAngle:[self radiansFromDegree:0]
                          clockwise:YES];
            
            [arrow addLineToPoint:CGPointMake(self.bounds.size.width, self.bounds.size.height - self.cornerRadius)];
            [arrow addArcWithCenter:CGPointMake(self.bounds.size.width - self.cornerRadius, self.bounds.size.height - self.cornerRadius)
                             radius:self.cornerRadius
                         startAngle:[self radiansFromDegree:0]
                           endAngle:[self radiansFromDegree:90]
                          clockwise:YES];
            
             
            
            [arrow addLineToPoint:CGPointMake(0, self.bounds.size.height)];
            [arrow addArcWithCenter:CGPointMake(self.cornerRadius, self.bounds.size.height - self.cornerRadius)
                             radius:self.cornerRadius
                         startAngle:[self radiansFromDegree:90]
                           endAngle:[self radiansFromDegree:180]
                          clockwise:YES];
            
            
            [arrow addLineToPoint:CGPointMake(0, self.arrowSize.height + self.cornerRadius)];
            [arrow addArcWithCenter:CGPointMake(self.cornerRadius, self.arrowSize.height + self.cornerRadius)
                             radius:self.cornerRadius
                         startAngle:[self radiansFromDegree:180]
                           endAngle:[self radiansFromDegree:270]
                          clockwise:YES];
            
            [arrow addLineToPoint:CGPointMake(arrowPoint.x - self.arrowSize.width * 0.5, [self isCornerLeftArrow] ? (self.arrowSize.height + self.bounds.size.height) : self.arrowSize.height)];

            break;
    }
    
    [color setFill];
    [arrow fill];
}

- (BOOL)isCornerLeftArrow {
    return arrowShowPoint.x == self.frame.origin.x;
}

- (BOOL)isCornerRightArrow {
    return arrowShowPoint.x == self.frame.origin.x + self.bounds.size.width;
}

- (CGFloat)radiansFromDegree:(CGFloat)degrees {
    return (M_PI * degrees / 180);
}

@end

@implementation PopoverOptions

- (id)init {
    self = [super init];
    
    if (self) {
        self.arrowSize = CGSizeMake(16.0f, 10.0f);
        self.animationIn = 0.6f;
        self.animationOut = 0.3f;
        self.cornerRadius = 6.0f;
        self.sideEdge = 20.0f;
        self.blackOverlayColor = [UIColor clearColor];

        if (UXIsSupportiOS8()) {
            self.overlayBlur = UIBlurEffectStyleLight;
        }
        
        self.poverType = kPopoverTypeDown;
        self.color = [UIColor whiteColor];
    }
    
    return self;
}

@end
