//
//  PreviewVideoViewController.h
//  CameraGPUImage
//
//  Created by qhcthanh on 6/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface PreviewVideoViewController : UIViewController
@property AVPlayer* avPlayer;
@end
