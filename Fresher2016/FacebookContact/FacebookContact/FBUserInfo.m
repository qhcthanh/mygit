//
//  FBUserInfo.m
//  FacebookContact
//
//  Created by qhcthanh on 7/14/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import "FBUserInfo.h"


@implementation FBUserInfo

- (id)initWithUserID:(NSString*)userID userName:(NSString*)userName {
    self = [super init];
    
    self.userID = userID;
    self.userName = userName;
    
    return self;
}

- (id)initWithDictionary:(NSDictionary*)dictionary {
    self = [super init];
    
    if(self && dictionary) {
        // Get birthday
        self.birthDay = [dictionary valueForKey:@"birthday"];
        
        // Get pictureURL
        NSDictionary* picture = [dictionary valueForKey:@"picture"];
        if(picture) {
            NSDictionary* data = [picture valueForKey:@"data"];
            NSString* picture = [data valueForKey:@"url"];
            self.pictureURL = picture;
        }
        
        // Get user name
        self.userName = [dictionary valueForKey:@"name"];
        
        // Get biography
        self.biography = [dictionary valueForKey:@"bio"] ;
        
        // Get cover URL
        NSDictionary* cover = [dictionary valueForKey:@"cover"];
        if(cover) {
            self.coverURL = [cover valueForKey:@"source"];
        }
        
        // Get hometown
        NSDictionary* hometown = [cover valueForKey:@"hometown"];
        if(hometown) {
            self.homeTown = [hometown valueForKey:@"name"];
        }
        
        //Get user id
        self.userID = [dictionary valueForKey:@"id"];
        
        // Get Quote
        self.quote = [dictionary valueForKey:@"quote"];
        
        // Get Gender
        NSString* genderString = [dictionary valueForKey:@"gender"];
        if([genderString isEqualToString:@"male"]) {
            self.gender = kFBUserGenderMale;
        } else if ([genderString isEqualToString:@"female"]) {
            self.gender = kFBUserGenderFemale;
        } else {
            self.gender = kFBUserGenderNotDetermine;
        }
    }
    return self;
}

+ (NSString*)getStringFBUserGender:(FBUserGender)gender {
    if (gender == kFBUserGenderMale) {
        return @"Nam";
    } else if (gender == kFBUserGenderFemale) {
        return @"Nữ";
    } else {
        return @"Không xác định";
    }
}

@end





