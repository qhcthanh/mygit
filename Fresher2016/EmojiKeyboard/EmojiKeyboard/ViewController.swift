//
//  ViewController.swift
//  EmojiKeyboard
//
//  Created by Quach Ha Chan Thanh on 8/13/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import UIKit



class ViewController: UIViewController, EmojiViewDelegate, BLLongPressGestureDelegate {

    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let emojiManager = EmojiManager.shareInstance()
        //print(emojiManager)
        
//        let emojiView = EmojiView(frame: CGRectMake(0,100,self.view.frame.size.width, 217))
//        emojiView.backgroundColor = UIColor(red: 233.0/255, green: 233.0/255, blue: 233.0/255, alpha: 1)
//        self.view.addSubview(emojiView)
//        emojiView.constrain(.Height, to: 217)
//        emojiView.delegate = self
//        
//        emojiView.constrain(.Left, to: .Left, of: self.view)
//        emojiView.constrain(.Right, to: .Right, of: self.view)
//        emojiView.constrain(.Bottom, to: .Bottom, of: self.view)

        
//        GiphyRequestManager.shareManager().getTrendGiphy { (giphys, error) in
//            if let giphy = giphys?.first, let giphyRedition = giphy.fixedWidthDownsampled {
//                
//                dispatch_async(dispatch_get_main_queue(), {
//                    imageView.setGifImage(NSURL(string: giphyRedition.url)!, gifIdentifier: giphy.id)
//                })
//                
//
//            }
//        }
        
        let view = BLGiphyView(frame: CGRectMake(0,100,self.view.frame.size.width, 217))
        self.view.addSubview(view)
        
        view.constrain(.Left, to: .Left, of: self.view)
        view.constrain(.Right, to: .Right, of: self.view)
        view.constrain(.Top, to: .Top, of: self.view, offsetBy: 50)
        view.constrain(.Height, to: 217)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didSelectEmoji(emoji: String) {
       self.textLabel.text! += emoji
    }
    
}










