//
//  BLPage.swift
//  BanLaKey
//
//  Created by BaoNQ on 9/5/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import UIKit

class BLPage: NSObject {
    var rows: [[BLKey]]
    
    override init() {
        self.rows = []
    }
    
    func addKey(key: BLKey, row: Int) {
        if self.rows.count <= row {
            for _ in self.rows.count...row {
                self.rows.append([])
            }
        }
        
        self.rows[row].append(key)
    }
    
    var hidden: Bool {
        get {
            return self.hidden
        }
        set {
            for row in self.rows {
                for key in row {
                    key.hidden = newValue
                }
            }
            
            self.hidden = newValue
        }
    }
}
