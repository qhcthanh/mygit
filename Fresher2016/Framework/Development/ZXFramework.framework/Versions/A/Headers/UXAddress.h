/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Alexey Belkevich, 2014
 * Updated by ThemND VNG Coporation, 2015
 * Change Log: Integrate Contact Framework that supports the iOS9
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#if __IPHONE_9_0
    #import <Contacts/Contacts.h>
#endif

@interface UXAddress : NSObject

@property (nonatomic, readonly) NSString *street;
@property (nonatomic, readonly) NSString *city;
@property (nonatomic, readonly) NSString *state;
@property (nonatomic, readonly) NSString *zip;
@property (nonatomic, readonly) NSString *country;
@property (nonatomic, readonly) NSString *countryCode;

/**
 *  Initialize an address with a dictionary
 *
 *  @param dictionary The dictionary contains address information
 *
 *  @return An address object
 */
- (id)initWithAddressDictionary:(NSDictionary *)dictionary;

#if __IPHONE_9_0
/**
 *  Initialize an address with a CNPostalAddress
 *
 *  @param postalAddress The object contains address information
 *
 *  @return An address object
 */
- (id)initWithAddressPostalAddress:(CNPostalAddress*)postalAddress;
#endif

@end
