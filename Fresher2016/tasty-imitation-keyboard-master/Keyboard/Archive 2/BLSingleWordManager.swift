//
//  BLSingleWordManager.swift
//  KeyboardDatabaseHaha
//
//  Created by VanDao on 8/26/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

import Foundation

//
//  BLDatabaseManager.swift
//  KeyboardDatabase
//
//  Created by VanDao on 8/25/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

import Foundation

// MARK: constant variable
let singleWordDatabasePath: String! = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.CachesDirectory, NSSearchPathDomainMask.UserDomainMask, true).first!.stringByAppendingString("/SingleWordDB.db")
private var internalQueue: dispatch_queue_t = dispatch_queue_create("com.singleworddatabase.internalSerialQueue", DISPATCH_QUEUE_SERIAL)

// MARK: - BLDatabaseManager class

public class BLSingleWordManager: NSObject {
    
    class func insertToDictionary(newWords newWords: [String], completion: (error: Bool) -> Void) {
        
        dispatch_async(internalQueue) {
            
            NSDate.startCount()
            
            createTableFromDictionary(table)
            
            let result: Bool = sharedDatabase().executeCommand(createInsertCommand(newWords))
            
            print("Single insert: " + String(NSDate.endCount()))

            completion(error: !result)
        }
    }
    
    class func updateToDictionary(newWords newWords: [String], completion: (error: Bool) -> Void) {
        dispatch_async(internalQueue) {
            
            NSDate.startCount()
            
            let result: Bool = sharedDatabase().executeCommand(createUpdateCommand(newWords))
            
            print("Single update: " + String(NSDate.endCount()))

            
            completion(error: !result)
        }
    }
    
    class func fetchHighestScoreWords(firstWord: String, hasSign: Bool, number: Int, completion: (res: [String]) -> Void) {
        dispatch_async(internalQueue) {
            
            NSDate.startCount()
            
            let result: YLSQLResult = sharedDatabase().executeQueryCommand(createQueryCommand(firstWord, hasSign: hasSign, numbers: number))
            
            print("Single fetch: " + String(NSDate.endCount()))

            completion(res: result.rowsForColumnName("WORD") as! [String])
        }
    }
}

// MARK: - Private function

private let tableName: String! = "SingleWordTable"

private let id: String = "ID"
private let word: String = "WORD"
private let asciiWord: String = "ASCIIWORD"
private let score: String = "SCORE"
private let table: [String : String] = [
    id : "INTEGER",
    word : "TEXT",
    asciiWord : "TEXT",
    score : "INTEGER"
]

private func createTableFromDictionary(dictionary: Dictionary<String, String>) -> Bool {
    let sqlString: String! = "CREATE TABLE IF NOT EXISTS \(tableName)(" +
        "\(id) \(table[id]!) PRIMARY KEY AUTOINCREMENT," +
        "\(word)      \(table[word]!)    NOT NULL," +
        "\(asciiWord)  \(table[asciiWord]!)    NOT NULL," +
        "\(score)     \(table[score]!)     NOT NULL);";
    
    let sqlCommand: YLSQLCommand = YLSQLCommand.init(string: sqlString)
    let result: Bool = sharedDatabase().executeCommand(sqlCommand)
    
    return result
}

private func createQueryCommand(_firstWord: String, hasSign: Bool, numbers: Int) -> YLSQLCommand {
    
    let sqlString = "SELECT \(word) FROM \(tableName) WHERE \(hasSign ? word : asciiWord) LIKE '\(_firstWord)%' ORDER BY \(score) DESC LIMIT \(numbers)"
    
    return YLSQLCommand.init(string: sqlString)
}

private func createUpdateCommand(words: [String]) -> YLSQLCommand {
    
    var sqlString: String! = ""
    
    for index in (0..<words.count) {
        
        sqlString = sqlString + "UPDATE \(tableName) SET \(score) = \(score) + 1 WHERE \(word) = '\(words[index])';"
    }
    
    return YLSQLCommand.init(string: sqlString)
}

private func createInsertCommand(words: [String]) -> YLSQLCommand {
    
    var sqlString: String! = "INSERT OR REPLACE INTO \(tableName) (\(word), \(asciiWord), \(score)) VALUES "
    
    for index in (0..<words.count) {
        
        sqlString = sqlString +  "('\(words[index])', '\(words[index].toASCII())', 0)"
        sqlString = sqlString + ((index == words.count - 1) ? ";" : ",")
    }
    
    return YLSQLCommand.init(string: sqlString)
}

private func sharedDatabase() -> YLSQLDatabase {
    
    var database:YLSQLDatabase?
    
    var tokenOnce: dispatch_once_t = 0
    dispatch_once(&tokenOnce) {
        database = YLSQLDatabase.init(path: singleWordDatabasePath)
        database!.openDatabaseWithOption(DatabaseOpenOption.init(rawValue: 6)!)
    }
    
    return database!
}