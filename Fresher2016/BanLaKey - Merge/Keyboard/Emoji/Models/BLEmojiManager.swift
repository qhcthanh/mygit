//
//  BLEmojiManager.swift
//  BanLaKey
//
//  Created by Quach Ha Chan Thanh on 9/4/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import Foundation

private let emojiResourcePath = NSBundle.mainBundle().pathForResource("emojiBL", ofType: "json")!


public class BLEmojiManager: NSObject {
    
    private static var instance: BLEmojiManager?
    
    var emojiCategorys: [BLEmojiCategory]!
    
    private override init() {
        super.init()
        
        let emojiAllDictionary = readEmojiJsonCustom(emojiResourcePath)
        
        self.emojiCategorys = [BLEmojiCategory]()
        
        for key in emojiAllDictionary.allKeys {
            let category = key as! String
            let emojiCategory = BLEmojiCategory(category: category, jsonData: emojiAllDictionary[category] as! NSDictionary)
            
            self.emojiCategorys.append(emojiCategory)
        }
        
        self.emojiCategorys.sortInPlace({
            if $0.priority < $1.priority {
                return true
            }
            return false
        })
    }
    
    class func shareInstance() -> BLEmojiManager {
        var emojiManagerDispatchOne: dispatch_once_t  = 0
        
        dispatch_once(&emojiManagerDispatchOne, {
            if instance == nil {
                instance = BLEmojiManager()
            }
        })
        
        return instance!
    }
    
}

func readEmojiJsonCustom(path: String) -> NSDictionary {
    
    let data = NSData(contentsOfFile: path)
    let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves)
    
    return json as! NSDictionary
}

func writeEmojiJsonFromBase () {
    
    if let path = NSBundle.mainBundle().pathForResource("emojis", ofType: "json"),
        let data = NSData(contentsOfFile: path),
        let dictionaryEmojis = try? NSJSONSerialization.JSONObjectWithData(data, options: []) as! NSDictionary
    {
        var emojiDictionary = Dictionary<String,AnyObject>()
        
        for key in dictionaryEmojis.allKeys {
            if let emoji = dictionaryEmojis.objectForKey(key) as? Dictionary<String,AnyObject> {
                
                if let char = emoji["char"] as? String,
                    let keywords = emoji["keywords"] as? [String] {
                    let emojiData = [
                        "name": key,
                        "keywords": keywords,
                        "char": char
                    ]
                    
                    let category = emoji["category"] as! String
                    
                    if let emojiCategory = emojiDictionary[category] as? NSDictionary
                    {
                        let emojiCategory = NSMutableDictionary(dictionary: emojiCategory)
                        if let emojiArray = emojiCategory["emojis"] as? NSMutableArray {
                            emojiArray.addObject(emojiData)
                            emojiCategory["emojis"] = emojiArray
                            emojiDictionary[category] = emojiCategory
                        }
                    } else {
                        let emojis = NSMutableArray()
                        emojis.addObject(emojiData)
                        emojiDictionary[category] = [
                            "emojis": emojis,
                            "priority": 1,
                            "represent": "a"
                        ]
                    }
                }
            }
        }
        
        
        if let jsonData = try? NSJSONSerialization.dataWithJSONObject(emojiDictionary, options: .PrettyPrinted) {
            if let json = String(data: jsonData, encoding: NSUTF8StringEncoding) {
                try! json.writeToFile("/Users/thanhqhc/Desktop/emojiBL.json", atomically: false, encoding: NSUTF8StringEncoding)
            }
            
        }
        
        
    }
}


