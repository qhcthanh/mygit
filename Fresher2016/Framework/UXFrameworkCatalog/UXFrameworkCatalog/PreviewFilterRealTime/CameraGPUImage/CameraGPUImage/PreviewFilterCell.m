//
//  PreviewFilterCell.m
//  CameraGPUImage
//
//  Created by qhcthanh on 6/30/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "PreviewFilterCell.h"

@implementation PreviewFilterCell {
    GPUImageStillCamera *stillCamera;
    GPUImageView* cameraView;
    UILabel* nameFilterLabel;
}

-(id)initWithIdentifier {
    self = [super init];
    return self;
}

-(void)awakeFromNib {
    [super awakeFromNib];
    
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    return self;
}

-(void)renderUI:(id)filter camera:(GPUImageStillCamera*)videoCamera {
    _filter = filter;
    stillCamera = videoCamera;
   // add cameraview and label
    if (!cameraView) {
        cameraView = [[GPUImageView alloc]
                      initWithFrame:
                      CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        
        nameFilterLabel = [[UILabel alloc] initWithFrame:
                           CGRectMake(0, self.frame.size.height - 20,
                                      self.frame.size.width, 20)];
        [nameFilterLabel setFont:[UIFont fontWithName:@"Arial" size:13]];
        nameFilterLabel.textColor = [UIColor whiteColor];
        nameFilterLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
        nameFilterLabel.textAlignment = NSTextAlignmentCenter;
        
        [self addSubview:cameraView];
        [self addSubview:nameFilterLabel];
    }
    
     // Remove old filter
    if (stillCamera && cameraView) {
        [filter forceProcessingAtSize:cameraView.sizeInPixels];
        [stillCamera removeTarget:filter];
        [filter addTarget:cameraView];
        [stillCamera addTarget:filter];
        nameFilterLabel.text = [self getNameFilter];
    }
}

-(NSString*)getNameFilter {
    IFImageFilter* iFilter = (IFImageFilter*)_filter;
    return [IFImageFilter getNameFilter:iFilter.name];
    
}

-(void)cleanUI {
    if (stillCamera && cameraView && _filter) {
        [stillCamera removeTarget:_filter];
       // [stillCamera stopCameraCapture];
    }
}


@end
