/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@interface UIImage (Addition)

/**
 * Returns a CGRect positioned within rect given the contentMode.
 */
- (CGRect)convertRect:(CGRect)rect withContentMode:(UIViewContentMode)contentMode;

/**
 * Draws the image using content mode rules.
 */
- (void)drawInRect:(CGRect)rect contentMode:(UIViewContentMode)contentMode;

/**
 *  Scale an image to size
 *
 *  @param toSize Size of scaled image
 *
 *  @return An scaled image
 */
- (UIImage*)scaledToSize:(CGSize)toSize;

/**
 *  Scale an image to size
 *
 *  @param toSize Size of scaled image
 *
 *  @return An scaled image
 */
- (UIImage*)scaledToSizeWithRGBA:(CGSize)toSize;

/**
 *  Scale an image to size for texture
 *
 *  @param toSize Size of scaled image
 *
 *  @return An scaled image
 */
- (UIImage*)scaledToSizeWithRGBAForTexture:(CGSize)toSize;

@end
