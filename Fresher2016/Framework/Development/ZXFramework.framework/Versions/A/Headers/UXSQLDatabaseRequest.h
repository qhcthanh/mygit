/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXSQLDatabaseResponse.h"

typedef void (^DatabaseWithSuccessBlock)(id responseObject);
typedef void (^DatabaseWithFailureBlock)(NSError *error);
@class UXSQLBackgroundQueue;
@protocol UXSQLDatabaseRequestDelegate;
@interface UXSQLDatabaseRequest : NSObject

+ (instancetype)requestWithDatabaseQueue:(UXSQLBackgroundQueue*)databaseQueue withDataSet:(NSArray*)dataSet;
+ (instancetype)requestWithDatabaseQueue:(UXSQLBackgroundQueue*)databaseQueue withSQLCommand:(NSString*)sqlCommand;

@property (nonatomic, assign) BOOL isQuery;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) NSInteger retryNumber;
@property (nonatomic, retain) NSString* sqlCommand;
@property (nonatomic, retain) NSArray* dataSet;
@property (nonatomic, assign) NSOperationQueuePriority priority;
@property (nonatomic, assign) dispatch_queue_t  dispatchQueue;
@property (nonatomic, retain) id<UXSQLDatabaseResponse> response;
@property (nonatomic, weak) id<UXSQLDatabaseRequestDelegate> delegate;
@property (nonatomic, weak) UXSQLBackgroundQueue* sqlDatabaseQueue;

@property (nonatomic, copy) DatabaseWithSuccessBlock databaseWithSuccessBlock;
@property (nonatomic, copy) DatabaseWithFailureBlock databaseWithFailureBlock;

@end

@protocol UXSQLDatabaseRequestDelegate <NSObject>

/**
 * The request has begun loading.
 *
 * This method will not be called if the data is loaded immediately from the cache.
 * @see accessDidStartLoad:
 */
- (void)accessDidStartLoad:(UXSQLDatabaseRequest*)request;

/**
 * The request has loaded data and been processed into a response.
 *
 * If the request is served from the cache, this is the only delegate method that will be called.
 */
- (void)accessDidFinishLoad:(UXSQLDatabaseRequest*)request;

/**
 * The request failed to load.
 */
- (void)access:(UXSQLDatabaseRequest*)request didFailLoadWithError:(NSError*)error;

/**
 * The request was canceled.
 */
- (void)accessDidCancelLoad:(UXSQLDatabaseRequest*)request;

@end

