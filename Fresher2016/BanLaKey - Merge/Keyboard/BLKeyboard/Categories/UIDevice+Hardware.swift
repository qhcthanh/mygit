//
//  UIDevice+Hardware.swift
//  BanLaKey
//
//  Created by BaoNQ on 8/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

extension UIDevice {
    
    /** The name of the machine.
     * @note This works also for the Core Simulator.
     * @return The internal name of the machine, eg "iPhone7,1" for an iPhone 6 Plus.
     */
    func machine() -> String {

        struct StaticVar {
            static var machine: String = ""
            static var onceToken: dispatch_once_t = 0
        }
        
        dispatch_once(&StaticVar.onceToken, {
            #if TARGET_IPHONE_SIMULATOR
                
                let paths: [String] = NSSearchPathForDirectoriesInDomains(.LibraryDirectory, .UserDomainMask, true)
                let libraryDirectory: String = paths.first!
                
                let devicePlist: String = NSString.pathWithComponents((NSURL(string: libraryDirectory)!.pathComponents)! + ["..", "..", "device.plist"])

                let deviceDescription:NSDictionary = NSDictionary(contentsOfFile: devicePlist)!
                
                
                let deviceType:String = deviceDescription["deviceType"] as! String
                
                if (deviceType.hasSuffix("iPhone-4s")) {
                    StaticVar.machine = "iPhone4,";
                } else if (deviceType.hasSuffix("iPhone-5")) {
                    StaticVar.machine = "iPhone5,";
                } else if (deviceType.hasSuffix("iPhone-5s")) {
                    StaticVar.machine = "iPhone6,";
                } else if (deviceType.hasSuffix("iPhone-6")) {
                    StaticVar.machine = "iPhone7,2";
                } else if (deviceType.hasSuffix("iPhone-6-Plus")) {
                    StaticVar.machine = "iPhone7,1";
                } else if (deviceType.hasSuffix("iPad-2")) {
                    StaticVar.machine = "iPad2,";
                } else if (deviceType.hasSuffix("iPad-Retina")) {
                    StaticVar.machine = "iPad3,";
                } else if (deviceType.hasSuffix("iPad-Air")) {
                    StaticVar.machine = "iPad4,";
                } else {
                    assert(false, "Unknown simulator model");
                }
                
            #else
                var size: size_t = 0
                sysctlbyname("hw.machine", nil , &size, nil, 0)
                let machineName: UnsafeMutablePointer<Void> = malloc(size)
                sysctlbyname("hw.machine", machineName, &size, nil, 0)
                StaticVar.machine = String.fromCString(UnsafePointer.init(machineName))!
                
            #endif
        })
        
        return StaticVar.machine;
    }
}
