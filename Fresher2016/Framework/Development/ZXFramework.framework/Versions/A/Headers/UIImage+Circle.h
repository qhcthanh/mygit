/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

@interface UIImage (Circle)

/**
 *  Mask a source image with a masked image
 *
 *  @param sourceImage The source image to be masked
 *  @param maskImage   The mask image template
 *
 *  @return A masked image
 */
+ (UIImage*)maskImage:(UIImage*)sourceImage withMask:(UIImage*)maskImage;

@end
