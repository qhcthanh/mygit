/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "AFHTTPSessionManager.h"

#import "UXMutableURLRequest.h"

@interface UXHTTPSessionManager : AFHTTPSessionManager

@end
