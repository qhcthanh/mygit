/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

typedef enum tagMobileNetworkType {
	kMobileNetworkTypeWifi = 0,
	kMobileNetworkType2G,
	kMobileNetworkTypeEdge,
	kMobileNetworkType3G,
	kMobileNetworkTypeHSPDA,
	kMobileNetworkTypeUnknown
} MobileNetworkType;

@interface UXMobileOperatorManager : NSObject

@property (nonatomic, readonly) BOOL networkAvailable;
@property (nonatomic, assign) short mobileNetworkType;

@property (nonatomic, retain) NSString* ipLocalNetwork;
@property (nonatomic, retain) NSString* ipPublicAuthen;
@property (nonatomic, retain) NSString* mobileOperator;
@property (nonatomic, retain) NSString* isoCountryCode;
@property (nonatomic, retain) NSString* mobileNetworkCodeMNC;
@property (nonatomic, retain) NSString* mobileCountryCodeMCC;
@property (nonatomic, retain) NSString* mobileCombineMCCMNC;

- (BOOL)startSessionContext;
- (void)getMobileOperation;

+ (UXMobileOperatorManager*)sharedUXMobileOperatorManager;

@end
