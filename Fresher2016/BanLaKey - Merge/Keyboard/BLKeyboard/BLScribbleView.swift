//
//  BLScribbleView.swift
//  BanLaKey
//
//  Created by BaoNQ on 8/31/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import UIKit

class BLScribbleView: UIView {
    
    let MAX_LENGTH = 300
    
    var visible: Bool
    var redValue: CGFloat = 0.0
    var greenValue: CGFloat = 0.0
    var blueValue: CGFloat = 0.0
    
    private var points: Array<CGPoint>
    
    override init(frame: CGRect) {
        
        self.visible = false
        self.points = []
        
        var alphaValue: CGFloat = 1.0
        BLThemeManager.currentTheme.scribbleLineColor.getRed(&self.redValue, green: &self.greenValue, blue: &self.blueValue, alpha: &alphaValue)
        
        super.init(frame: frame)
        
        self.opaque = false
        self.backgroundColor = UIColor.clearColor()
        self.userInteractionEnabled = true
    }
    
    func length() -> CGFloat {
        if (self.points.count == 0) {
            return 0
        }
    
        var length: CGFloat = 0.0
        var point1: CGPoint = self.points[0]
        
        for i in 0.stride(to: self.points.count, by: 2) {
            let point2 = self.points[i]
            length += sqrt(pow(point2.x - point1.x, 2) + pow(point2.y - point1.y, 2))
            point1 = point2
        }
        
//        for var i = 0; i < self.points.count; i += 2 {
//            let point2 = self.points[i]
//            length += sqrt(pow(point2.x - point1.x, 2) + pow(point2.y - point1.y, 2))
//            point1 = point2
//        }
        
        return length
    }
    
    func show() {
        self.alpha = 1
    }
    
    func hide() {
        UIView.animateWithDuration(0.2, delay: 0, options: .CurveEaseOut, animations: {
            self.alpha = 0
            }, completion: { (finished: Bool) in
                self.reset()
        })
    }
    
    func reset() {
        self.points.removeAll()
        self.setNeedsDisplay()
    }
    
    let lineWidth: CGFloat = 7.0
    
    override func drawRect(rect: CGRect) {
        let numberOfPoints = self.points.count
        
        if (numberOfPoints > 0) {
            let context = UIGraphicsGetCurrentContext()
            CGContextSetLineCap(context, CGLineCap.Round)
            CGContextSetLineWidth(context, lineWidth)
            CGContextSetRGBStrokeColor(context, self.redValue, self.greenValue, self.blueValue, 1)
            
            var point = self.points[0]
            CGContextMoveToPoint(context, point.x, point.y)
            
            for i in 0 ..< numberOfPoints {
                point = self.points[i]

                CGContextSetRGBStrokeColor(context, self.redValue, self.greenValue, self.blueValue, 1)
                CGContextSetLineWidth(context, pow(CGFloat(i)/CGFloat(numberOfPoints), 0.5) * lineWidth)
                CGContextAddLineToPoint(context, point.x, point.y)
                CGContextStrokePath(context)
                CGContextMoveToPoint(context, point.x, point.y)
            }
        }
    }
    
    func drawToTouch(touch: UITouch) {
        let point = touch.locationInView(touch.view)
        
        var length = self.length()

        while (length >= CGFloat(self.MAX_LENGTH)) {
            
            self.points.removeAtIndex(0)
            length = self.length()
        }

        self.points.append(point)
        self.setNeedsDisplay()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

