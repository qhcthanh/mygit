//
//  CaptureSessionManager.h
//  CameraWithAVFoundation
//
//  Created by Gabriel Alvarado on 4/16/14.
//  Copyright (c) 2014 Gabriel Alvarado. All rights reserved.
//
#define kImageCapturedSuccessfully @"imageCapturedSuccessfully"
#import "Constants.h"
#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>

@import Photos;

///Protocol Definition
@protocol CaptureSessionManagerDelegate <NSObject>
@required - (void)cameraSessionManagerDidCaptureImage;
@required - (void)cameraSessionMaagerDidStartRecording:(NSURL *)fileURL;
@required - (void)cameraSessionMaagerDidFinsishRecording:(NSURL *)outputFileURL error:(NSError *)error cleanUp:(dispatch_block_t)cleanUpBlock;
@required - (void)cameraSessionManagerFailedToCaptureImage;
@required - (void)cameraSessionManagerDidReportAvailability:(BOOL)deviceAvailability forCameraType:(CameraType)cameraType;
@required - (void)cameraSessionManagerDidReportDeviceStatistics:(CameraStatistics)deviceStatistics; //Report every .125 seconds

@end

@interface CaptureSessionManager : NSObject

//Weak pointers
@property (nonatomic, weak) id<CaptureSessionManagerDelegate>delegate;
@property (nonatomic, weak) AVCaptureDevice *activeCamera;

//Strong Pointers
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *previewLayer;
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureStillImageOutput *stillImageOutput;
@property (nonatomic, strong) AVCaptureMovieFileOutput *movieFileOutput;
@property (nonatomic, strong) UIImage *stillImage;
@property (nonatomic, strong) NSData *stillImageData;

//Primative Variables
@property (nonatomic,assign,getter=isTorchEnabled) BOOL enableTorch;

@property CGRect videoPreviewViewBounds;

//API Methods
- (void)addStillImageOutput;
- (void)addMovieCameraFileOutput;
- (void)captureStillImage;
- (void)startRecording;
- (void)stopRecording;
- (bool)isRecording;
- (void)addVideoPreviewLayer;
- (void)initiateCaptureSessionForCamera:(CameraType)cameraType;
- (void)stop;
+ (AVAuthorizationStatus)getStatusCameraPermission;
+ (void)requestAccessCameraForMediaType: (NSString *)mediaType completionHandler:(void (^)(BOOL granted))handler;
@end
