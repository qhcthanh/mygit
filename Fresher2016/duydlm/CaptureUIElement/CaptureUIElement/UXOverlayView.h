/* Copyright (c) 2016-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

@class UXTextTransitionView;
@class UXImageTransitionView;
@class UXTextTransitionDetail;
@class UXImageTransitionDetail;
@class HorizontalImageView;
@class UXTransitionDetail;
@class UXTransitionView;
@protocol UXOverlayViewDelegate;

@interface UXOverlayView : UIView

@property (nullable, nonatomic) id<UXOverlayViewDelegate> touchDelegate;

@property (weak, nonatomic) HorizontalImageView *nextView;

@property (nonatomic)NSMutableArray *stickerList;
@property (nonatomic)NSMutableArray<UITouch *> *touchList;
@property (nonatomic)NSMutableArray *beginPointList;
@property (nonatomic)NSMutableArray *movePointList;
@property (nonatomic)id focusTranstionView;
@property (nonatomic)BOOL movedSticker;

/**
 *  Initializes an `UXOverlayView` object with CGRect frame on superview
 *
 *  @param frame The frame of `UXOverlayView`
 *
 *  @return The newly-initialized overlay view
 */
- (instancetype _Nonnull)initWithFrame:(CGRect)frame;

/**
 *  Add a new sticker (`UXTransitionView` object)  to `UXOverlayView`
 *
 *  @param newSticker The initialized `UXTransitionView` object
 */
- (void)addSticker:(id _Nonnull)newSticker;

/**
 *  Remove a sticker from `UXOVerlayView` object
 *
 *  @param sticker The `UXTransitionView` object contained by `UXOverlayView`
 */
- (void)removeSticker:(id _Nonnull)sticker;

/**
 *  Remove all sticker form `UXOverlayView` Object
 */
- (void)removeAllStickers;

/**
 *  Create an `UIImage` by apply all sticker view in `UXOverlayView` to original `UIImage`
 *
 *  @param picture The original `UIImage` want to apply
 *
 *  @return The newly `UIImage`
 */
- (UIImage* _Nonnull)drawStickersToPicture:(id _Nonnull)picture;

/**
 *  Get all current `UXTransitionView` object contains by `UXOverlayView` and return to a `NSArray`
 *
 *  @return The NSArray contains all `UXTransitionView` of `UXOverlayView`
 */
- (NSArray* _Nullable)getStickerList;

/**
 *  Tranform sticker depend on properties itself
 *
 *  @param sticker Sticker need to transform
 */
- (void)transformSticker:(UXTransitionDetail * _Nonnull)sticker;

/**
 *  Check touch point is inside sticker?
 *
 *  @param sticker Sticker need check
 *  @param point   Touch point need check
 *
 *  @return Bool value result
 */
- (BOOL)checkTransitionView:(UXTransitionView * _Nonnull)sticker containsPoint:(CGPoint)point;

/**
 *  Check line of 2 point is intersect sticker?
 *
 *  @param sticker Sticker need check
 *  @param point1  Point 1 of line
 *  @param point2  Point 2 of line
 *
 *  @return Bool value result
 */
- (BOOL)checkTransitionView:(UXTransitionView * _Nonnull)sticker intersectLineSegment:(CGPoint)point1 :(CGPoint)point2;

@end

@protocol UXOverlayViewDelegate <NSObject>

@optional

/**
 *  Call when touch up inside the `UXTextTransitionView` object contains by `UXOverlayView`
 *
 *  @param textSticker The `UXTextTransitionView` that `UXOverlayView` touched up inside
 */
- (void)didTouchUpInsideTextSticker:(UXTextTransitionView * _Nonnull)textSticker;

/**
 *  Call when move the `UXTransitionView` object contains by `UXOverlayView`
 *
 *  @param sticker  The `UXTransitionView` that `UXOverlayView` focussing
 *  @param position The first touch `CGPoint` position
 */
- (void)didTouchMoveSticker:(id _Nonnull)sticker position:(CGPoint)position;

/**
 *  Call when move and touch up inside the `UXTransitionView` object contain by `UXOverlayView`
 *
 *  @param sticker  The `UXTransitionView` that `UXOverlayView` focussing
 *  @param position The `UXTransitionView` that `UXOverlayView` MOVED AND TOUCHED UP inside
 */
- (void)didTouchEndSticker:(id _Nonnull)sticker position:(CGPoint)position;

@end


