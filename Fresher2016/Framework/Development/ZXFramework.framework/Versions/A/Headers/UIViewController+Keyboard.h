/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

/**
 * @name The Keyboard Delegate protocol.
 */
@protocol UXKeyboardDelegate <NSObject>

/**
 * Notifies the view controller that the keyboard will show or hide with specified parameters. This method is called before keyboard animation.
 
 * @param height The height of keyboard.
 * @param animationDuration The duration of keyboard animation.
 * @param animationCurve The animation curve.
 */
- (void)keyboardWillShowOrHideWithHeight:(CGFloat)height
                       animationDuration:(NSTimeInterval)animationDuration
                          animationCurve:(UIViewAnimationCurve)animationCurve;

/**
 * The keyboard animation. This method is called inside UIView animation block with the same animation parameters as keyboard animation.
 
 * @param height The height of keyboard.
 * @param animationDuration The duration of keyboard animation.
 * @param animationCurve The animation curve.
 */
- (void)keyboardShowOrHideAnimationWithHeight:(CGFloat)height
                            animationDuration:(NSTimeInterval)animationDuration
                               animationCurve:(UIViewAnimationCurve)animationCurve;

/**
 * Notifies the view controller that the keyboard animation finished. This method is called after keyboard animation.
 * @param height The height of keyboard.
 */
- (void)keyboardShowOrHideAnimationDidFinishedWithHeight:(CGFloat)height;

@end


#pragma mark - Category

/**
 * @name The UIViewController keyboard additions category.
 */
@interface UIViewController (Keyboard) <UXKeyboardDelegate>

/**
 * Return YES if keyboard height is > 0.
 */
@property (nonatomic, readonly) BOOL isKeyboardPresented;

/**
 * The height of keyboard.
 * @note Extracted from `UIKeyboardFrameEndUserInfoKey` on show or sets to 0 on hide.
 */
@property (nonatomic, readonly) CGFloat keyboardHeight;

/**
 * Starts observing for `UIKeyboardWillShowNotification` and `UIKeyboardWillHideNotification` notifications.
 * @discussion It is recommended to call this method in `-viewWillAppear:`.
 */
- (void)startObservingKeyboardNotifications;

/**
 * Stops observing for `UIKeyboardWillShowNotification` and `UIKeyboardWillHideNotification` notifications.
 * @discussion It is recommended to call this method in `-viewWillDisappear:`.
 */
- (void)stopObservingKeyboardNotifications;

@end

/**
 * Example
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // register to keyboard notifications
    [self startObservingKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // unregister from keyboard notifications
    [self stopObservingKeyboardNotifications];
}

#pragma mark - UXKeyboardDelegate

- (void)keyboardWillShowOrHideWithHeight:(CGFloat)height
animationDuration:(NSTimeInterval)animationDuration
animationCurve:(UIViewAnimationCurve)animationCurve {
    
    NSLog(@"Will %@ with height = %f, duration = %f",
          self.isKeyboardPresented ? @"show" : @"hide",
          height,
          animationDuration);
}

- (void)keyboardShowOrHideAnimationWithHeight:(CGFloat)height
animationDuration:(NSTimeInterval)animationDuration
animationCurve:(UIViewAnimationCurve)animationCurve {
    
    self.containerViewBottomConstraint.constant = height;
    [self.view layoutIfNeeded];
}

- (void)keyboardShowOrHideAnimationDidFinishedWithHeight:(CGFloat)height {
    
    NSLog(@"Animation finished with height = %f", height);
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
*/
