//
//  VSPreviewImage.h
//  CameraGPUImage
//
//  Created by manhduydl on 8/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VSPreviewImage : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIImageView *vsImageView;
@property (nonatomic) UIImage *imagePreview;

@end
