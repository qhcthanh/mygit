/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from LMinh, Github
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

// UXGeocoder service API.
typedef NS_ENUM(NSInteger, UXGeocoderService){
    kUXGeocoderGoogleService = 1,
    kUXGeocoderAppleService,
};

// UXGeocoder error codes, embedded in NSError.
typedef NS_ENUM(NSInteger, UXGeocoderErrorCode){
    kUXGeocoderErrorInvalidCoordinate = 1,
    kUXGeocoderErrorInvalidAddressString,
    kUXGeocoderErrorInternal,
};

// Handler that reports a geocoding response, or error.
typedef void (^UXLocationGeocodeCallback) (NSArray *results, NSError *error);

/**
 *  Exposes a service for geocoding and reverse geocoding.
 */
@interface UXLocationGeocoder : NSObject

+ (UXLocationGeocoder*)sharedUXLocationGeocoder;

// Indicating whether the receiver is in the middle of geocoding its value.
@property (nonatomic, readonly, assign) BOOL isGeocoding;

// To set google API key
@property (nonatomic, strong) NSString *googleAPIKey;

/**
 *  Submits a forward-geocoding request using the specified string.
 *  After initiating a forward-geocoding request, do not attempt to initiate another forward- or reverse-geocoding request.
 *  Geocoding requests are rate-limited for each app, so making too many requests in a short period of time may cause some of the requests to fail.
 *  When the maximum rate is exceeded, the geocoder passes an error object to your completion handler.
 *
 *  @param addressString The string describing the location you want to look up.
 *  @param service       The service API used to geocode.
 *  @param handler       The callback to invoke with the geocode results. The callback will be invoked asynchronously from the main thread.
 */
- (void)geocodeAddressString:(NSString *)addressString
                     service:(UXGeocoderService)service
           completionHandler:(UXLocationGeocodeCallback)handler;

/**
 *  Submits a reverse-geocoding request for the specified coordinate.
 *  After initiating a reverse-geocoding request, do not attempt to initiate another reverse- or forward-geocoding request.
 *  Geocoding requests are rate-limited for each app, so making too many requests in a short period of time may cause some of the requests to fail.
 *  When the maximum rate is exceeded, the geocoder passes an error object to your completion handler.
 *
 *  @param coordinate The coordinate to look up.
 *  @param service    The service API used to reverse geocode.
 *  @param handler    The callback to invoke with the reverse geocode results. The callback will be invoked asynchronously from the main thread.
 */
- (void)reverseGeocodeCoordinate:(CLLocationCoordinate2D)coordinate
                         service:(UXGeocoderService)service
               completionHandler:(UXLocationGeocodeCallback)handler;

/**
 *  Cancels a pending geocoding request.
 */
- (void)cancelGeocode;

@end
