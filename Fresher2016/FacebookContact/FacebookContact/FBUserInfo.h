//
//  FacebookFriendInfo.h
//  FacebookContact
//
//  Created by qhcthanh on 7/14/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, FBUserGender) {
    kFBUserGenderMale = 0,
    kFBUserGenderFemale,
    kFBUserGenderNotDetermine
};


@interface FBUserInfo : NSObject <NSCoding>

@property NSString* userID;
@property NSString* userName;
@property NSString* birthDay;
@property NSString* pictureURL;
@property NSString* biography;
@property NSString* coverURL;
@property NSString* homeTown;
@property FBUserGender gender;
@property NSString* quote;


/**
 *  Initializes a new user that use Facebook.
 *
 *  @param userID   The UserID in Facebook
 *  @param userName The display name user in facebook
 *  return new instance FBUserInfo
 */
- (id)initWithUserID:(NSString*)userID userName:(NSString*)userName;

/**
 *  Initializes a new user that use Facebook.
 *
 *  @param dictionary   The Json data when request Graph API
 *  return new instance FBUserInfo
 */
- (id)initWithDictionary:(NSDictionary*)dictionary;

/**
 *  Get string of FBUserGender (e.g: kFBUserGenderMale return "Nam")
 *
 *  @param gender A gender get string
 *
 *  @return A String of FBUserGender
 */
+ (NSString*)getStringFBUserGender:(FBUserGender)gender;


@end













