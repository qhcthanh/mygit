//
//  Contact.h
//  w1UITableView
//
//  Created by manhduydl on 5/20/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ContactProtocol.h"

@interface Contact : NSObject <ContactProtocol>

@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, copy) NSMutableArray *phones;
//@property (nonatomic, copy) NSString *fullName;
@property (nonatomic, copy) NSString *givenName;
@property (nonatomic, copy) NSString *familyName;
@property (nonatomic, copy) NSString *symbolOfName;
//@property (nonatomic, copy) UIImage *avatarImage;
@property NSIndexPath *indexPathOfContact;

- (instancetype)initContactWithGivenname:(NSString *)aGivenname
                              FamilyName:(NSString *)aFamilyName
                                FullName:(NSString *)aFullName
                                   Image:(UIImage *)anImage
                           andIdentifier:(NSString *)identifier;


@end
