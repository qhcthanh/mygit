//
//  CTFilterView.m
//  CameraGPUImage
//
//  Created by BaoNQ on 6/27/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "ImageView.h"

@implementation ImageView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self == nil)
        return nil;
    
    self.contentMode = UIViewContentModeScaleAspectFit;
    self.clipsToBounds = YES;
    
    return self;
}

- (void)updateMask:(CGRect)maskRect newXPosition:(CGFloat)nX {
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    CGMutablePathRef path = CGPathCreateMutable();
    CGRect rect = maskRect;
    
    rect.origin.x = nX;
    CGPathAddRect(path, nil, rect);
    maskLayer.path = path;
    self.layer.mask = maskLayer;
}

@end
