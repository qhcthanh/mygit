//
//  StickerCollectionViewController.h
//  CameraGPUImage
//
//  Created by qhcthanh on 6/16/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StickerCollectionDelegate <NSObject>

@optional
-(void) didSelectItem:(id _Nonnull)itemSelected position:(CGPoint)touchPosition;

@end

@interface StickerCollectionViewController : UICollectionViewController

@property (nonatomic,nonnull) NSMutableArray* stickerArray;
@property (nonatomic, nullable, weak) id<StickerCollectionDelegate> stickerDelegate;


-(id _Nonnull)init:(NSMutableArray* _Nonnull)stickerArray;

/**
 *  Set background color default is blackColor with alpha 0.8
 *
 *  @param backgroundColor
 */
-(void)setBackgroundColor:(UIColor* _Nonnull)backgroundColor;

/**
 *  set size of item. Default CGSize(80,80)
 *
 *  @param sizeOfItem
 */
-(void)setSizeOfItem:(CGSize)sizeOfItem;

/**
 *  set miniumspacing line. Default 10
 *
 *  @param spacing
 */
-(void)setMiniumSpacingLine:(CGFloat)spacing;

/**
 *  set margin in collectionview. Default Top: 0, Left: 20, Right:20 Bottom 0
 *
 *  @param insetMake
 */
-(void)setMarginLayout:(UIEdgeInsets)insetMake;
@end
