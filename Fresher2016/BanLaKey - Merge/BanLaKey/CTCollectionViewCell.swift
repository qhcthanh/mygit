//
//  CTCollectionViewCell.swift
//  BanLaKey
//
//  Created by VanDao on 9/5/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import Foundation
import UIKit

class CTCollectionViewCell : UICollectionViewCell {
    @IBOutlet var themeImage: UIImageView!
    @IBOutlet var themeName: UILabel!
}