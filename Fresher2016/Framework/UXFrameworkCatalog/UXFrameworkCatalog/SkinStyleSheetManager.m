/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "SkinStyleSheetManager.h"

#import "SkinStyleSheet.h"

@implementation SkinStyleSheetManager

+ (SkinStyleSheetManager*)manager {
    
    static SkinStyleSheetManager *gSkinStyleSheetManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        gSkinStyleSheetManager = [[SkinStyleSheetManager alloc] init];
    });
    
    return gSkinStyleSheetManager;
}

- (instancetype)init {
    if (self = [super init]) {
        skinStyleSheet = [[SkinStyleSheet alloc] init];
    }
    
    return self;
}

- (void)dealloc {
    skinStyleSheet = nil;
}

- (void)loadSkin {
    [UXStyleSheet setGlobalStyleSheet:skinStyleSheet];
}

@end
