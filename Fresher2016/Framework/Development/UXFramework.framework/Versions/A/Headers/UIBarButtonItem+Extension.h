/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@interface UIBarButtonItem (Extension)

/**
 *  Create a buton bar item with text
 *
 *  @param text The text display will display on button
 *
 *  @return An UIBarButtonItem object
 */
+ (UIBarButtonItem*)barButtonItemWithText:(NSString*)text;

/**
 *  Create a button bar item with text
 *
 *  @param text The text display will display on button
 *
 *  @return An UIBarButtonItem object
 */
+ (UIBarButtonItem*)barButtonItemWithDoneText:(NSString*)text;

/**
 *  Create a button bar item with checkmark style
 *
 *  @param text The text display will display on button
 *
 *  @return An UIBarButtonItem object
 */
+ (UIBarButtonItem*)barButtonItemWithCheckmarkText:(NSString*)text;

/**
 *  Create a button bar item with an image URL
 *
 *  @param imageURL The image URL
 *
 *  @return An UIBarButtonItem object
 */
+ (UIBarButtonItem*)barButtonItemWithImageURL:(NSString*)imageURL;

/**
 *  Create a button bar item with state images
 *
 *  @param normalImage   The normal state of button
 *  @param selectedImage The selected state of button
 *
 *  @return An UIBarButtonItem object
 */
+ (UIBarButtonItem*)barButtonItemWithMultipleImage:(UIImage*)normalImage withSelectedImage:(UIImage*)selectedImage;

/**
 *  Create a button bar item with state image URLs
 *
 *  @param normalURL   The normal state of button
 *  @param selectedURL The selected state of button
 *
 *  @return An UIBarButtonItem object
 */
+ (UIBarButtonItem*)barButtonItemWithMultipleImageURL:(NSString*)normalURL withSelectedIcon:(NSString*)selectedURL;

@end
