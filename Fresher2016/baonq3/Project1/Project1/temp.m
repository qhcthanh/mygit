//
//  temp.m
//  Project1
//
//  Created by admin on 5/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>


/*
 - (void)dealloc{
 dispatch_release(myQueue);
 dispatch_release(semaphore);
 }
 */

/*

int main(int argc, char * argv[]) {
    @autoreleasepool {
        
        dispatch_queue_t myQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_queue_t myQueue1 = dispatch_queue_create("com.VNG.Project1", DISPATCH_QUEUE_CONCURRENT);
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(1);
        NSMutableArray* array = [[NSMutableArray alloc] init];
        for (int i = 0; i < 10000; ++i)
        {
            dispatch_async(myQueue, ^{
                dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
                [array addObject:[NSNumber numberWithInt:i]];
                dispatch_semaphore_signal(semaphore);
            });
            //dispatch_barrier_async(myQueue1, ^{
            //[array addObject:[NSNumber numberWithInt:i]];
            //   NSLog(@"%i", [[array objectAtIndex:i] intValue]); });
        }
        for (int i = 0; i < 10000; ++i)
        {
            dispatch_async(myQueue1, ^{
                NSLog(@"%i", [[array objectAtIndex:i] intValue]);
            });
        }
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
 
 */




//extension
/*
 - (NSUInteger)count{
 return [array count];
 }
 
 - (void)insertObject:(id)anObject atIndex:(NSUInteger)index {
 dispatch_barrier_async(myQueue, ^{
 [array insertObject:anObject atIndex:index];
 });
 }
 
 - (void)removeLastObject{
 dispatch_barrier_async(myQueue, ^{
 [array removeLastObject];
 });
 }
 
 - (void)removeObjectAtIndex:(NSUInteger)index{
 dispatch_barrier_async(myQueue, ^{
 [array removeObjectAtIndex:index];
 });
 }
 
 
 - (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject{
 dispatch_barrier_async(myQueue, ^{
 [array replaceObjectAtIndex:index withObject:anObject];
 });
 }
 */



//extension1
/*
 
 - (NSUInteger)count{
 return [array count];
 }
 
 - (void)insertObject:(id)anObject atIndex:(NSUInteger)index {
 dispatch_async(myQueue, ^{
 dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
 [array insertObject:anObject atIndex:index];
 dispatch_semaphore_signal(semaphore);
 });
 }
 
 - (void)removeLastObject{
 dispatch_async(myQueue, ^{
 dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
 [array removeLastObject];
 dispatch_semaphore_signal(semaphore);
 });
 }
 
 - (void)removeObjectAtIndex:(NSUInteger)index{
 dispatch_async(myQueue, ^{
 dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
 [array removeObjectAtIndex:index];
 dispatch_semaphore_signal(semaphore);
 });
 }
 
 
 - (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject{
 dispatch_async(myQueue, ^{
 dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
 [array replaceObjectAtIndex:index withObject:anObject];
 dispatch_semaphore_signal(semaphore);
 });
 }
 */