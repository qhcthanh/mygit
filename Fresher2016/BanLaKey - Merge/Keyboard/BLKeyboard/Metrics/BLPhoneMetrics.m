//
//  BLPhoneMetrics.m
//  BanLaKey
//
//  Created by BaoNQ on 8/16/16.
//  Copyright © 2016 admin. All rights reserved.
//


#import "BLPhoneMetrics.h"
#import "Linear.h"

#define kPhoneKeyboardPortraitWidth     320.0
#define kPhoneKeyboardLandscapeWidth    568.0

#define kPhoneKeyboardPortraitHeight    216.0
#define kPhoneKeyboardLandscapeHeight   162.0


#define edgeMargin      5.0
#define bottomMargin    5.0


CGRect calculatePhonePopupFrame(CGSize keySize, CGFloat keyboardWidth, CGFloat keyboardHeight) {
    CGFloat popupTotalHeight = LINEAR_EQ(keyboardHeight,
                                         kPhoneKeyboardPortraitHeight, 100.0,
                                         kPhoneKeyboardLandscapeHeight, 80.0);
    
    CGFloat popupWidthIncrement = LINEAR_EQ(keyboardWidth,
                                            kPhoneKeyboardPortraitWidth, 26.0,
                                            kPhoneKeyboardLandscapeWidth, 26.0);
    
    CGFloat popupConnectorHeight = LINEAR_EQ(keyboardHeight,
                                             kPhoneKeyboardPortraitHeight, 8.0,
                                             kPhoneKeyboardLandscapeHeight, 8.0);
    
    CGFloat popupWidth = keySize.width + popupWidthIncrement;
    
    CGFloat popupHeight = popupTotalHeight - popupConnectorHeight - keySize.height;
    
    return CGRectMake((keySize.width - popupWidth)/2.0, -popupHeight - popupConnectorHeight, popupWidth, popupHeight);
}

PhoneKeyboardMetrics generatePhoneKeyboardMetrics(CGFloat keyboardWidth, CGFloat keyboardHeight) {
    
    PhoneKeyboardMetrics metrics;
    
    metrics.rowMargin = LINEAR_EQ(keyboardHeight,
                                  kPhoneKeyboardPortraitHeight, 15.0,
                                  kPhoneKeyboardLandscapeHeight, 7.0);
    
    metrics.columnMargin = LINEAR_EQ(keyboardWidth,
                                     kPhoneKeyboardPortraitWidth, 6.0,
                                     kPhoneKeyboardLandscapeWidth, 7.0);
    
    metrics.keyHeight = LINEAR_EQ(keyboardHeight,
                                  kPhoneKeyboardPortraitHeight, 39.0,
                                  kPhoneKeyboardLandscapeHeight, 33.0);
    
    metrics.letterKeyWidth = LINEAR_EQ(keyboardWidth,
                                       kPhoneKeyboardPortraitWidth, 26.0,
                                       kPhoneKeyboardLandscapeWidth, 49.0);
    
    
    metrics.nextKeyboardButtonWidth = LINEAR_EQ(keyboardWidth,
                                                kPhoneKeyboardPortraitWidth, 34.0,
                                                kPhoneKeyboardLandscapeWidth, 50.0);
    
    metrics.returnButtonWidth = LINEAR_EQ(keyboardWidth,
                                          kPhoneKeyboardPortraitWidth, 74.0,
                                          kPhoneKeyboardLandscapeWidth, 107.0);
    
    metrics.deleteButtonWidth = LINEAR_EQ(keyboardWidth,
                                          kPhoneKeyboardPortraitWidth, 36.0,
                                          kPhoneKeyboardLandscapeWidth, 69.0);

    metrics.shiftButtonWidth = LINEAR_EQ(keyboardWidth,
                                         kPhoneKeyboardPortraitWidth, 36.0,
                                         kPhoneKeyboardLandscapeWidth, 68.0);
    
    metrics.popupTotalHeight = LINEAR_EQ(keyboardHeight,
                                         kPhoneKeyboardPortraitHeight, 100.0,
                                         kPhoneKeyboardLandscapeHeight, 80.0);
    
    metrics.popupWidthIncrement = LINEAR_EQ(keyboardWidth,
                                            kPhoneKeyboardPortraitWidth, 26.0,
                                            kPhoneKeyboardLandscapeWidth, 26.0);
    
    metrics.popupConnectorHeight = LINEAR_EQ(keyboardHeight,
                                             kPhoneKeyboardPortraitHeight, 8.0,
                                             kPhoneKeyboardLandscapeHeight, 8.0);
    
    metrics.popupWidth = metrics.letterKeyWidth + metrics.popupWidthIncrement;
    
    metrics.popupHeight = metrics.popupTotalHeight - metrics.popupConnectorHeight - metrics.keyHeight;
        
    return metrics;
}