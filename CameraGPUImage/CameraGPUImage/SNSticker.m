//
//  SNSticker.m
//  CameraGPUImage
//
//  Created by BaoNQ on 8/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "SNSticker.h"

@implementation SNSticker

- (id)initWithFrame:(CGRect)frame image:(UIImage *)image atZPosition:(CGFloat)zIndex {
    
    id object = [self initWithFrame:frame image:image contentMode:UIViewContentModeScaleAspectFit atZPosition:zIndex];
    
    return object;
}

- (id)initWithFrame:(CGRect)frame image:(UIImage *)image contentMode:(UIViewContentMode)mode atZPosition:(CGFloat)zIndex {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        self.contentMode = mode;
        self.clipsToBounds = YES;
        self.image = image;
        self.layer.zPosition = zIndex;
    }
    return self;
}

@end
