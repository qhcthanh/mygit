/* Copyright (c) 2016-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "UXOverlayView.h"

#import "UXTransitionView.h"
#import "UXTextTransitionView.h"
#import "UXImageTransitionView.h"
#import "HorizontalImageView.h"
#import "UXTransitionDetail.h"
#import "UXTextTransitionDetail.h"
#import "UXImageTransitionDetail.h"

@interface UXOverlayView () {
    
    NSMutableArray *stickerList;
    NSMutableArray<UITouch *> *touchList;
    
    NSMutableArray *beginPointList;
    NSMutableArray *movePointList;
    
    id focusTranstionView;
    BOOL movedSticker;
}

@end

@implementation UXOverlayView

- (instancetype _Nonnull)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self){
        
        self.opaque = NO;
        self.userInteractionEnabled = YES;
        self.multipleTouchEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        
        touchList = [[NSMutableArray alloc]init];
        beginPointList = [[NSMutableArray alloc]init];
        movePointList = [[NSMutableArray alloc]init];
        stickerList = [[NSMutableArray alloc]init];
    }
    
    return self;
}

- (void)addSticker:(id _Nonnull)newSticker {
    [stickerList insertObject:newSticker atIndex:0];
    UXTransitionView *view = ((UXTransitionDetail *)newSticker).transitionView;
    [self addSubview:view];
    [self bringSubviewToFront:view];
}

- (void)removeSticker:(id _Nonnull)sticker{
    UXTransitionView *view = ((UXTransitionDetail *)sticker).transitionView;

    [[view borderLayer] removeFromSuperlayer];
    [view removeFromSuperview];
    [stickerList removeObject:sticker];
}

- (void)removeAllStickers {
    
//    for (id sticker in stickerList) {
//        [[sticker borderLayer] removeFromSuperlayer];
//        [sticker removeFromSuperview];
//    }
    for (id sticker in stickerList) {
        UXTransitionView *view = ((UXTransitionDetail *)sticker).transitionView;
        
        [[view borderLayer] removeFromSuperlayer];
        [view removeFromSuperview];
    }
    
    [stickerList removeAllObjects];
}

- (NSMutableArray * _Nullable)getStickerList {
    return stickerList;
}

- (UIImage *)drawStickersToPicture:(UIImage *)picture {
    
    UIGraphicsBeginImageContextWithOptions(picture.size, NO, picture.scale);
    
    [picture drawAtPoint:CGPointZero];
    
    CGFloat scale = picture.size.width / kWidth;
    CGContextScaleCTM(UIGraphicsGetCurrentContext(), scale, scale);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    for (UITouch *touch in [touches allObjects])
    {
        // add new touch to list
        [touchList addObject:touch];
        [beginPointList addObject:[NSValue valueWithCGPoint:[touch locationInView:self]]];
        [movePointList addObject:[NSValue valueWithCGPoint:[touch locationInView:self]]];
        
        CGPoint touchPosition = [[movePointList objectAtIndex:0] CGPointValue];
        
        // check if touch down a `UXTransitionView`
        if (!focusTranstionView){
            for (id sticker in stickerList){
                UXTransitionDetail *curSticker = sticker;
                if ([self checkTransitionView:curSticker.transitionView containsPoint:touchPosition]){
                    focusTranstionView = sticker;
                    
                    id firstSticker = focusTranstionView;
                    [stickerList removeObject:focusTranstionView];
                    [stickerList insertObject:firstSticker atIndex:0];
                    
                    [self bringSubviewToFront:curSticker.transitionView];
                    
                    movedSticker = NO;
                    
                    break;
                }
            }
        }
    }
    
    // Update current focus sticker
    if (!focusTranstionView && [beginPointList count] > 1){
        CGPoint touch1Position = [[beginPointList objectAtIndex:0] CGPointValue];
        CGPoint touch2Position = [[beginPointList objectAtIndex:1] CGPointValue];
        
        for (id sticker in stickerList){
            UXTransitionDetail *curSticker = sticker;
            
            if ([self checkTransitionView:curSticker.transitionView intersectLineSegment:touch1Position :touch2Position]){
                focusTranstionView = sticker;
                
                id firstSticker = focusTranstionView;
                [stickerList removeObject:firstSticker];
                [stickerList insertObject:focusTranstionView atIndex:0];
                
                [self bringSubviewToFront:curSticker.transitionView];
                break;
            }
        }
    }
    
    // Check if having sticker focussed
    if (focusTranstionView){
        [self.nextView disableReceiveResponder];
    }
    
    if (![self.nextView isDisabledResponder])
        [self.nextView touchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    // Update move point list
    for (UITouch *touch in [touches allObjects])
    {
        NSInteger index = [touchList indexOfObject:touch];
        [movePointList replaceObjectAtIndex:index withObject:[NSValue valueWithCGPoint:[touch locationInView:self]]];
    }
    
    // Check if having sticker focussed
    if (focusTranstionView){
        movedSticker = YES;
        if (self.touchDelegate && [self.touchDelegate respondsToSelector:@selector(didTouchMoveSticker:position:)])
            [self.touchDelegate didTouchMoveSticker:focusTranstionView position:[[movePointList objectAtIndex:0] CGPointValue]];
        
        [self transformSticker:focusTranstionView];
    }
    
    // Update begin point list
    for (NSInteger index = 0; index < [movePointList count]; index++){
        [beginPointList replaceObjectAtIndex:index withObject:[movePointList objectAtIndex:index]];
    }
    
    if (![self.nextView isDisabledResponder])
        [self.nextView touchesMoved:touches withEvent:event];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    // Check if touch up inside UXTextTransitionView
    if ([focusTranstionView isKindOfClass:[UXTextTransitionDetail class]] && !movedSticker){
        if (self.touchDelegate && [self.touchDelegate respondsToSelector:@selector(didTouchUpInsideTextSticker:)])
            [self.touchDelegate didTouchUpInsideTextSticker:focusTranstionView];
    }
    
    // Check if pan UXTextTransitionView
    if (focusTranstionView && movedSticker) {
        for (UITouch *touch in touches) {
            if ([touchList indexOfObject:touch] == 0){
                if ( self.touchDelegate && [self.touchDelegate respondsToSelector:@selector(didTouchEndSticker:position:)]) {
                    [self.touchDelegate didTouchEndSticker:focusTranstionView position:[[movePointList objectAtIndex:0] CGPointValue]];
                    break;
                }
            }
        }
    }
    
    // Remove ended point from list
    for (UITouch *touch in [touches allObjects]) {
        NSInteger index = [touchList indexOfObject:touch];
        
        [touchList removeObjectAtIndex:index];
        [beginPointList removeObjectAtIndex:index];
        [movePointList removeObjectAtIndex:index];
    }
    
    if ([beginPointList count] == 0)
        focusTranstionView = nil;
    
    if (![self.nextView isDisabledResponder])
        [self.nextView touchesEnded:touches withEvent:event];
    
    if (!focusTranstionView && [self.nextView isDisabledResponder]){
        [self.nextView enableReceiveResponder];
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    // Remove cancelled point from list
    for (UITouch *touch in [touches allObjects]) {
        NSInteger index = [touchList indexOfObject:touch];
        
        [touchList removeObjectAtIndex:index];
        [beginPointList removeObjectAtIndex:index];
        [movePointList removeObjectAtIndex:index];
    }
    
    if ([beginPointList count] == 0)
        focusTranstionView = nil;
    
    if (![self.nextView isDisabledResponder])
        [self.nextView touchesCancelled:touches withEvent:event];
    
    if (!focusTranstionView && [self.nextView isDisabledResponder]){
        [self.nextView enableReceiveResponder];
    }
}

- (void)transformSticker:(UXTransitionDetail * _Nonnull)sticker{
    [self bringSubviewToFront:sticker.transitionView];
    
    // Calculate transform value
    
    // Set default transform value
    CGPoint translate = CGPointMake(0.0f, 0.0f);
    CGFloat scale = 1.0f;
    CGFloat rotate = 0.0f;
    CGPoint anchorPoint = sticker.transitionView.center;
    
    if ([beginPointList count] == 1) {
        // If only have 1 touch
        
        CGPoint beginPoint = [[beginPointList objectAtIndex:0] CGPointValue];
        CGPoint movePoint = [[movePointList objectAtIndex:0] CGPointValue];
        
        // Translate
        CGFloat xDist = movePoint.x - beginPoint.x;
        CGFloat yDist = movePoint.y - beginPoint.y;
        
        translate.x = xDist;
        translate.y = yDist;
    }  else {
        // If have more than 1 touch
        
        CGPoint beginPoint = [[beginPointList objectAtIndex:0] CGPointValue];
        CGPoint movePoint = [[movePointList objectAtIndex:0] CGPointValue];
        CGPoint beginPoint1 = [[beginPointList objectAtIndex:1] CGPointValue];
        CGPoint movePoint1 = [[movePointList objectAtIndex:1] CGPointValue];
        anchorPoint = CGPointMake((movePoint.x + movePoint1.x)/2, (movePoint.y + movePoint1.y)/2);
        
        // Translate
        CGPoint point1 = CGPointMake((beginPoint.x + beginPoint1.x) / 2, (beginPoint.y + beginPoint1.y)/2);
        CGPoint point2 = CGPointMake((movePoint.x + movePoint1.x) / 2, (movePoint.y + movePoint1.y)/2);
        
        CGFloat xDist = point2.x - point1.x;
        CGFloat yDist = point2.y - point1.y;
        
        translate.x = xDist;
        translate.y = yDist;
        
        // Scale
        CGFloat xDist1 = beginPoint1.x - beginPoint.x;
        CGFloat yDist1 = beginPoint1.y - beginPoint.y;
        CGFloat dis1 = sqrt(xDist1 * xDist1 + yDist1 * yDist1);
        
        CGFloat xDist2 = movePoint1.x - movePoint.x;
        CGFloat yDist2 = movePoint1.y - movePoint.y;
        CGFloat dis2 = sqrt(xDist2 * xDist2 + yDist2 * yDist2);
        
        scale = dis2 / dis1;
        
        // Rotate
        CGFloat deltaX0 = (beginPoint.x - beginPoint1.x);
        CGFloat deltaY0 = (beginPoint.y - beginPoint1.y);
        CGFloat radian0 = atan2(deltaY0, deltaX0);
        
        CGFloat deltaX = (movePoint.x - movePoint1.x);
        CGFloat deltaY = (movePoint.y - movePoint1.y);
        CGFloat radian = atan2(deltaY, deltaX);
        
        rotate = (radian - radian0);
    }
    
    // Transform sticker
    [sticker updateTransformStickerWithTranslate:translate scale:scale rotate:rotate atAnchorPoint:anchorPoint];
}

- (BOOL)checkTransitionView:(UXTransitionView *)sticker containsPoint:(CGPoint)point{
    NSMutableArray *linearEquation1 = [self linearEquation:sticker.vertexOne :sticker.vertexTwo];
    NSMutableArray *linearEquation2 = [self linearEquation:sticker.vertexTwo :sticker.vertexThree];
    NSMutableArray *linearEquation3 = [self linearEquation:sticker.vertexThree :sticker.vertexFour];
    NSMutableArray *linearEquation4 = [self linearEquation:sticker.vertexFour :sticker.vertexOne];
    
    CGPoint origin = CGPointMake((sticker.vertexOne.x + sticker.vertexThree.x)/2, (sticker.vertexOne.y + sticker.vertexThree.y)/2);
    
    if (![self is2PointsLocatedOnTheSameSide:point :origin withLinearEquation:linearEquation1])
        return NO;
    if (![self is2PointsLocatedOnTheSameSide:point :origin withLinearEquation:linearEquation2])
        return NO;
    if (![self is2PointsLocatedOnTheSameSide:point :origin withLinearEquation:linearEquation3])
        return NO;
    if (![self is2PointsLocatedOnTheSameSide:point :origin withLinearEquation:linearEquation4])
        return NO;
    
    return YES;
}

- (BOOL)checkLineIntersection:(CGPoint)p1 :(CGPoint)p2 :(CGPoint)p3 :(CGPoint)p4
{
    CGFloat denominator = (p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y);
    CGFloat ua = (p4.x - p3.x) * (p1.y - p3.y) - (p4.y - p3.y) * (p1.x - p3.x);
    CGFloat ub = (p2.x - p1.x) * (p1.y - p3.y) - (p2.y - p1.y) * (p1.x - p3.x);
    if (denominator < 0) {
        ua = -ua; ub = -ub; denominator = -denominator;
    }
    return (ua > 0.0 && ua <= denominator && ub > 0.0 && ub <= denominator);
}

- (BOOL)checkTransitionView:(UXTransitionView *)sticker intersectLineSegment:(CGPoint)point1 :(CGPoint)point2{
    if ([self checkTransitionView:sticker containsPoint:point1] && [self checkTransitionView:sticker containsPoint:point2])
        return YES;
    if ([self checkLineIntersection:point1 :point2 :sticker.vertexOne :sticker.vertexTwo])
        return YES;
    if ([self checkLineIntersection:point1 :point2 :sticker.vertexTwo :sticker.vertexThree])
        return YES;
    if ([self checkLineIntersection:point1 :point2 :sticker.vertexThree :sticker.vertexFour])
        return YES;
    if ([self checkLineIntersection:point1 :point2 :sticker.vertexFour :sticker.vertexOne])
        return YES;
    
    return NO;
}

- (NSMutableArray * _Nonnull)linearEquation:(CGPoint)point1 :(CGPoint)point2 {
    
    CGPoint u = CGPointMake(point2.x - point1.x, point2.y - point1.y);
    float a = -u.y;
    float b = u.x;
    float c = -a*point1.x - b*point1.y;
    
    NSMutableArray *result = [[NSMutableArray alloc] init];
    [result addObject:[NSNumber numberWithFloat:a]];
    [result addObject:[NSNumber numberWithFloat:b]];
    [result addObject:[NSNumber numberWithFloat:c]];
    
    return result;
    
}

- (BOOL)is2PointsLocatedOnTheSameSide:(CGPoint)point1 :(CGPoint)point2 withLinearEquation:(NSMutableArray * _Nonnull)linearEquation {
    
    float a = [[linearEquation objectAtIndex:0] floatValue];
    float b = [[linearEquation objectAtIndex:1] floatValue];
    float c = [[linearEquation objectAtIndex:2] floatValue];
    
    CGFloat value1 = (a * point1.x + b * point1.y + c);
    CGFloat value2 = (a * point2.x + b * point2.y + c);
    
    if (value1 * value2 >= 0) {
        return YES;
    }
    
    return NO;
}

@end
