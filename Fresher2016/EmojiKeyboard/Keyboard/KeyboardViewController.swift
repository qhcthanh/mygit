//
//  KeyboardViewController.swift
//  Keyboard
//
//  Created by qhcthanh on 8/24/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import UIKit

class KeyboardViewController: UIInputViewController, EmojiViewDelegate {

    @IBOutlet var nextKeyboardButton: UIButton!
    static var currentInstance: KeyboardViewController!
    var emojiView: EmojiView!
    var giphyView: BLGiphyView!
    
    var heightConstraint: NSLayoutConstraint?
    
    var keyboardHeight: CGFloat {
        get {
            if let constraint = self.heightConstraint {
                return constraint.constant
            }
            else {
                return 0
            }
        }
        set {
            self.setHeight(newValue)
        }
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
    
        // Add custom view sizing constraints here
//        emojiView.updateConstraintsIfNeeded()
        //try! "emojiView".writeToFile("/Users/admin/Desktop/emojiView.txt", atomically: false, encoding: NSUTF8StringEncoding)
//        emojiView.updateConstraintsIfNeeded()
//        emojiView.setupConstraints()
        giphyView.updateConstraints()
        
//        if self.view.frame.width > self.view.frame.height {
//            keyboardHeight = 260
//        } else {
//            keyboardHeight = 100
//        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        KeyboardViewController.currentInstance = self
        // Perform custom UI setup here
        self.nextKeyboardButton = UIButton(type: .System)
    
        self.nextKeyboardButton.setTitle(NSLocalizedString("Next Keyboard", comment: "Title for 'Next Keyboard' button"), forState: .Normal)
        self.nextKeyboardButton.sizeToFit()
        self.nextKeyboardButton.translatesAutoresizingMaskIntoConstraints = false
    
        self.nextKeyboardButton.addTarget(self, action: #selector(advanceToNextInputMode), forControlEvents: .TouchUpInside)
        
        self.view.addSubview(self.nextKeyboardButton)
    
        self.nextKeyboardButton.leftAnchor.constraintEqualToAnchor(self.view.leftAnchor).active = true
        self.nextKeyboardButton.bottomAnchor.constraintEqualToAnchor(self.view.bottomAnchor).active = true
        
        // Setup View
//        emojiView = EmojiView(frame: CGRectMake(0,0,360, 217))
//        emojiView.backgroundColor = UIColor(red: 233.0/255, green: 233.0/255, blue: 233.0/255, alpha: 1)
//        emojiView.delegate = self
//        emojiView.clipsToBounds = false
//        self.view.clipsToBounds = false
//        
//        self.view.addSubview(emojiView)
//        emojiView.constrain(.Top, to: .Top, of: self.view)
//        emojiView.constrain(.Left, to: .Left, of: self.view)
//        emojiView.constrain(.Right, to: .Right, of: self.view)
//        emojiView.constrain(.Bottom, to: .Bottom, of: self.view)
        
        giphyView = BLGiphyView(frame: CGRectMake(0,100,360, 217))
        self.view.addSubview(giphyView)
        
        giphyView.constrain(.Left, to: .Left, of: self.view)
        giphyView.constrain(.Right, to: .Right, of: self.view)
        giphyView.constrain(.Top, to: .Top, of: self.view)
        giphyView.constrain(.Height, to: 217)

        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.keyboardHeight = 216 + 100
    }
    
    func setHeight(height: CGFloat) {
        if self.heightConstraint == nil {
            self.heightConstraint = NSLayoutConstraint(
                item:self.view,
                attribute:NSLayoutAttribute.Height,
                relatedBy:NSLayoutRelation.Equal,
                toItem:nil,
                attribute:NSLayoutAttribute.NotAnAttribute,
                multiplier:0,
                constant:height)
            self.heightConstraint!.priority = 1000
            
            self.view.addConstraint(self.heightConstraint!) // TODO: what if view already has constraint added?
        }
        else {
            self.heightConstraint?.constant = height
        }
    }

    
    func didTouchReturn() {
        advanceToNextInputMode()
    }
    
    func didSelectEmoji(emoji: String) {
        self.textDocumentProxy.insertText(emoji)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated
    }

    override func textWillChange(textInput: UITextInput?) {
        // The app is about to change the document's contents. Perform any preparation here.
    }

    override func textDidChange(textInput: UITextInput?) {
        // The app has just changed the document's contents, the document context has been updated.
    
        var textColor: UIColor
        let proxy = self.textDocumentProxy
        if proxy.keyboardAppearance == UIKeyboardAppearance.Dark {
            textColor = UIColor.whiteColor()
        } else {
            textColor = UIColor.blackColor()
        }
        self.nextKeyboardButton.setTitleColor(textColor, forState: .Normal)
    }

}
