//
//  BLLockKey.swift
//  BanLaKey
//
//  Created by BaoNQ on 8/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

enum BLLockKeyType : Int {
    case Once = 0 // The key is locked until next key pressed
    case Forever  // The key is locked until it is pressed again
    case Default // The key is not locked
}

internal class BLLockKey: BLKey {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(frame: CGRect, title: String, originalPopupFrame: CGRect, image: UIImage? = nil, keyType: BLKeyType = .CharacterKey, backgroundColor: UIColor = UIColor.whiteColor()) {
       
        self.init(frame: frame)
        
        let popupView = self.popupSubviewInitializer(title, extendedCharacters: [])
        
        self.keyInitializer(title, popupSubview: popupView, originalPopupFrame: originalPopupFrame, image: image, keyType: keyType, backgroundColor: backgroundColor)
    }
    
    convenience init(frame: CGRect, title: String, popupSubview: UIView!, originalPopupFrame: CGRect, image: UIImage? = nil, keyType: BLKeyType = .CharacterKey, backgroundColor: UIColor = UIColor.whiteColor()) {
        
        self.init(frame: frame)

        self.keyInitializer(title, popupSubview: popupSubview, originalPopupFrame: originalPopupFrame, image: image, keyType: keyType, backgroundColor: backgroundColor)
    }
    
    convenience init(frame: CGRect, title: String, extendedCharacters:[String]!, originalPopupFrame: CGRect, image: UIImage? = nil, keyType: BLKeyType = .CharacterKey, backgroundColor: UIColor = UIColor.whiteColor()) {
    
        self.init(frame: frame)

        let popupView = self.popupSubviewInitializer(title, extendedCharacters: extendedCharacters)
        
        self.keyInitializer(title, popupSubview: popupView, originalPopupFrame: originalPopupFrame, image: image, keyType: keyType, backgroundColor: backgroundColor)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var lockState: BLLockKeyType = BLLockKeyType.Default
    
}
