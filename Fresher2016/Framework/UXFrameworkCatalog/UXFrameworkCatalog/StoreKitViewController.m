/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "StoreKitViewController.h"

@interface StoreKitViewController ()

@end

@implementation StoreKitViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        self.titleString = @"Store Kit";
        
        // Do any additional setup after loading the view.
        [[NSNotificationCenter defaultCenter] addObserverForName:kUXStoreKitProductPurchasedNotification
                                                          object:nil
                                                           queue:[[NSOperationQueue alloc] init]
                                                      usingBlock:^(NSNotification *note) {
                                                          
                                                          NSLog(@"Purchased/Subscribed to product with id: %@", [note object]);
                                                      }];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:kUXStoreKitRestoredPurchasesNotification
                                                          object:nil
                                                           queue:[[NSOperationQueue alloc] init]
                                                      usingBlock:^(NSNotification *note) {
                                                          
                                                          NSLog(@"Restored Purchases");
                                                      }];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:kUXStoreKitRestoringPurchasesFailedNotification
                                                          object:nil
                                                           queue:[[NSOperationQueue alloc] init]
                                                      usingBlock:^(NSNotification *note) {
                                                          
                                                          NSLog(@"Failed restoring purchases with error: %@", [note object]);
                                                      }];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    UIButton* buyConsumableButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buyConsumableButton setFrame:CGRectMake(80, 100, 200, 40)];
    [buyConsumableButton setTitle:@"Buy Consumable" forState:UIControlStateNormal];
    [buyConsumableButton addTarget:self action:@selector(buyConsumable) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:buyConsumableButton];
    
    UIButton* buyNoneConsumableButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buyNoneConsumableButton setFrame:CGRectMake(80, 160, 200, 40)];
    [buyNoneConsumableButton setTitle:@"Buy None Consumable" forState:UIControlStateNormal];
    [buyNoneConsumableButton addTarget:self action:@selector(buyNoneConsumable) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buyNoneConsumableButton];
    
    UIButton* buySubcriptionButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buySubcriptionButton setFrame:CGRectMake(80, 200, 200, 40)];
    [buySubcriptionButton setTitle:@"Buy Subcription" forState:UIControlStateNormal];
    [buySubcriptionButton addTarget:self action:@selector(buySubcription) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buySubcriptionButton];
    
    UIButton* buyProductWithHostButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buyProductWithHostButton setFrame:CGRectMake(80, 240, 200, 40)];
    [buyProductWithHostButton setTitle:@"Buy Product with Host Content" forState:UIControlStateNormal];
    [buyProductWithHostButton addTarget:self action:@selector(buyProductWithHostContent) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buyProductWithHostButton];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buyConsumable {
    [[UXStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:@"vn.com.vng.zalo.store.sticker.consumable"];
}

- (void)buyNoneConsumable {
    [[UXStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:@"vn.com.vng.zalo.store.sticker.nonconsumablenocontent"];
}

- (void)buySubcription {
    [[UXStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:@"vn.com.vng.zalo.store.sticker.quarterly"];
}

- (void)buyProductWithHostContent {
    [[UXStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:@"vn.com.vng.zalo.store.sticker.nonconsumablewithcontent"];
}

@end
