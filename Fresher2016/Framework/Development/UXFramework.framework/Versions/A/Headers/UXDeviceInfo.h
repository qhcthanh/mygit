/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
/**
 * Formats a number of bytes in a human-readable format.
 *
 * Will create a string showing the size in bytes, KBs, MBs, or GBs.
 */
NSString* UXStringFromBytes(unsigned long long bytes);


/**
 * An interface for accessing device information.
 *
 * @ingroup Overview-Sensors
 *
 * This class is not meant to be instantiated. All methods are class implementations.
 *
 * This class aims to simplify the interface for collecting device information. The low-level
 * mach APIs provide a host of valuable information but it's often in formats that aren't
 * particularly ready for presentation.
 *
 * @attention When using this class on the simulator, the values returned will reflect
 *                 those of the computer within which you're running the simulator, not the
 *                 simulated device. This is because the simulator is a first-class citizen
 *                 on the computer and has full access to your RAM and disk space.
 */
@interface UXDeviceInfo : NSObject

#pragma mark Memory /** @name Memory */

/**
 * The number of bytes in memory that are free.
 *
 * Calculated using the number of free pages of memory.
 */
+ (unsigned long long)bytesOfFreeMemory;

/**
 * The total number of bytes of memory.
 *
 * Calculated by adding together the number of free, wired, active, and inactive pages of memory.
 *
 * This value may change over time on the device due to the way iOS partitions available memory
 * for applications.
 */
+ (unsigned long long)bytesOfTotalMemory;

/**
 * Simulate low memory warning
 *
 * Don't use this in production because it uses private API
 */
+ (void)simulateLowMemoryWarning;

#pragma mark Disk Space /** @name Disk Space */

/**
 * The number of bytes free on disk.
 */
+ (unsigned long long)bytesOfFreeDiskSpace;

/**
 * The total number of bytes of disk space.
 */
+ (unsigned long long)bytesOfTotalDiskSpace;


#pragma mark Battery /** @name Battery */

/**
 * The battery charge level in the range 0 .. 1.0. -1.0 if UIDeviceBatteryStateUnknown.
 *
 * This is a thin wrapper for [[UIDevice currentDevice] batteryLevel].
 */
+ (CGFloat)batteryLevel;

/**
 * The current battery state.
 *
 * This is a thin wrapper for [[UIDevice currentDevice] batteryState].
 */
+ (UIDeviceBatteryState)batteryState;


#pragma mark Caching

/**
 * Fetches the device's current information and then caches it.
 *
 * All subsequent calls to UXDeviceInfo methods will use this cached information.
 *
 * This can be a useful way to freeze the device info at a moment in time.
 *
 * Example:
 *
 * @code
 *  [UXDeviceInfo beginCachedDeviceInfo];
 *
 *  // All calls to UXDeviceInfo methods here will use the information retrieved when
 *  // beginCachedDeviceInfo was called.
 *
 *  [UXDeviceInfo endCachedDeviceInfo];
 * @endcode
 */
+ (BOOL)beginCachedDeviceInfo;

/**
 * Stop using the cache for the device info methods.
 */
+ (void)endCachedDeviceInfo;


@end

