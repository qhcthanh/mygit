/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@interface NSString (Security)

- (NSString*)md5HexDigest;
- (NSString*)sha1HexDigest;
- (NSString*)sha256HexDigest;
- (NSString *)obfuscate:(NSString*)string withKey:(NSString *)key;
+ (NSString*)encryptPasswordWithAESKey:(NSString*)key value:(NSString*)value;
+ (NSString*)decryptPasswordWithAESKey:(NSString*)key value:(NSString*)value;

@end
