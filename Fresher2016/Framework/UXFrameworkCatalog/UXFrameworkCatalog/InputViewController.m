/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */


#import "InputViewController.h"

#import <UIKit/UIKit.h>
#import <UXFramework/UXFramework.h>

@interface InputViewController () <UITextFieldDelegate> //UXRadioGroupDelegate
@property (nonatomic, retain) UXTableViewDataSource* model;

//// A radio group object allows us to easily maintain radio group-style interactions in a table view.
//@property (nonatomic, retain) UXRadioGroup* radioGroup;
//
//// Each radio group object maintains a specific set of table objects, so in order to have multiple
//// radio groups you need to instantiate multiple radio group objects.
//@property (nonatomic, retain) UXRadioGroup* subRadioGroup;
//
@end

@implementation InputViewController

- (id)initWithStyle:(UITableViewStyle)style {
    if ((self = [super initWithStyle:UITableViewStyleGrouped])) {
        self.title = @"Form Cell Catalog";
        
//        _radioGroup = [[UXRadioGroup alloc] init];
//        _radioGroup.delegate = self;
//        
//        _subRadioGroup = [[UXRadioGroup alloc] initWithController:self];
//        _subRadioGroup.delegate = self;
//        _subRadioGroup.cellTitle = @"Selection";
//        _subRadioGroup.controllerTitle = @"Make a Selection";
//        
//        [_subRadioGroup mapObject:[UXSubtitleTableViewCellObject objectWithTitle:@"Sub Radio 1"
//                                                               subtitle:@"First option"]
//                     toIdentifier:SubRadioOption1];
//        [_subRadioGroup mapObject:[UXSubtitleTableViewCellObject objectWithTitle:@"Sub Radio 2"
//                                                               subtitle:@"Second option"]
//                     toIdentifier:SubRadioOption2];
//        [_subRadioGroup mapObject:[UXSubtitleTableViewCellObject objectWithTitle:@"Sub Radio 3"
//                                                               subtitle:@"Third option"]
//                     toIdentifier:SubRadioOption3];
        
        NSArray* tableContents =
        [NSArray arrayWithObjects:
//         @"Radio Group",
//         [_radioGroup mapObject:[UXSubtitleTableViewCellObject objectWithTitle:@"Radio 1"
//                                                             subtitle:@"First option"]
//                   toIdentifier:RadioOption1],
//         [_radioGroup mapObject:[UXSubtitleTableViewCellObject objectWithTitle:@"Radio 2"
//                                                             subtitle:@"Second option"]
//                   toIdentifier:RadioOption2],
//         [_radioGroup mapObject:[UXSubtitleTableViewCellObject objectWithTitle:@"Radio 3"
//                                                             subtitle:@"Third option"]
//                   toIdentifier:RadioOption3],
//         @"Radio Group Controller",
//         _subRadioGroup,
        
         @"UXTextInputFormElement",
         [UXTextInputFormElement textInputElementWithID:0 placeholderText:@"Placeholder" value:nil],
         [UXTextInputFormElement textInputElementWithID:0 placeholderText:@"Placeholder" value:@"Initial value"],
         [UXTextInputFormElement textInputElementWithID:1 placeholderText:nil value:@"Disabled input field" delegate:self],
         [UXTextInputFormElement passwordInputElementWithID:0 placeholderText:@"Password" value:nil],
         [UXTextInputFormElement passwordInputElementWithID:0 placeholderText:@"Password" value:@"Password"],
         
         @"UXSwitchFormElement",
         [UXSwitchFormElement switchElementWithID:0 labelText:@"Switch" value:NO],
         [UXSwitchFormElement switchElementWithID:0 labelText:@"Switch with a really long label that will be cut off" value:YES],
         
         @"UXSliderFormElement",
         [UXSliderFormElement sliderElementWithID:0
                                        labelText:@"Slider"
                                            value:45
                                     minimumValue:0
                                     maximumValue:100],
         
         @"UXSegmentedControlFormElement",
         [UXSegmentedControlFormElement segmentedControlElementWithID:0
                                                            labelText:@"Text segments"
                                                             segments:[NSArray arrayWithObjects:
                                                                       @"one", @"two", nil]
                                                        selectedIndex:0],
         [UXSegmentedControlFormElement segmentedControlElementWithID:0
                                                            labelText:@"Image segments"
                                                             segments:[NSArray arrayWithObjects:
                                                                       [UIImage imageNamed:@"star.png"],
                                                                       [UIImage imageNamed:@"circle.png"],
                                                                       nil]
                                                        selectedIndex:-1
                                                      didChangeTarget:self
                                                    didChangeSelector:@selector(segmentedControlWithImagesDidChangeValue:)],
         @"UXDatePickerFormElement",
         [UXDatePickerFormElement datePickerElementWithID:0
                                                labelText:@"Date and time"
                                                     date:[NSDate date]
                                           datePickerMode:UIDatePickerModeDateAndTime],
         [UXDatePickerFormElement datePickerElementWithID:0
                                                labelText:@"Date only"
                                                     date:[NSDate date]
                                           datePickerMode:UIDatePickerModeDate],
         [UXDatePickerFormElement datePickerElementWithID:0
                                                labelText:@"Time only"
                                                     date:[NSDate date]
                                           datePickerMode:UIDatePickerModeTime
                                          didChangeTarget:self
                                        didChangeSelector:@selector(datePickerDidChangeValue:)],
         [UXDatePickerFormElement datePickerElementWithID:0
                                                labelText:@"Countdown"
                                                     date:[NSDate date]
                                           datePickerMode:UIDatePickerModeCountDownTimer],
         nil];
        
//        self.radioGroup.selectedIdentifier = RadioOption1;
//        self.subRadioGroup.selectedIdentifier = SubRadioOption1;
        
        // We let the UXFramwork cell factory create the cells.
        _model = [[UXTableViewDataSource alloc] initWithSectionedArray:tableContents
                                                         delegate:(id)[UXTableViewCellFactory class]];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = _model;
    
//    self.tableView.delegate = [self.radioGroup forwardingTo:
//                               [self.subRadioGroup forwardingTo:self.tableView.delegate]];
    
    // When including text editing cells in table views you should provide a means for the user to
    // stop editing the control. To do this we add a gesture recognizer to the table view.
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapTableView)];
    
    // We still want the table view to be able to process touch events when we tap.
    tap.cancelsTouchesInView = NO;
    
    [self.tableView addGestureRecognizer:tap];
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
//    return UXIsSupportedOrientation(toInterfaceOrientation);
//}

- (void)segmentedControlWithImagesDidChangeValue:(UISegmentedControl *)segmentedControl {
    NSLog(@"Segmented control changed value to index %ld", (long)segmentedControl.selectedSegmentIndex);
}

- (void)datePickerDidChangeValue:(UIDatePicker *)picker {
    NSLog(@"Time only date picker changed value to %@",
             [NSDateFormatter localizedStringFromDate:picker.date
                                            dateStyle:NSDateFormatterNoStyle
                                            timeStyle:NSDateFormatterShortStyle]);
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField.tag == 1) {
        return NO;
    }
    return YES;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Customize the presentation of certain types of cells.
    if ([cell isKindOfClass:[UXTextInputFormElementCell class]]) {
        UXTextInputFormElementCell* textInputCell = (UXTextInputFormElementCell *)cell;
        if (1 == cell.tag) {
            // Make the disabled input field look slightly different.
            textInputCell.textField.textColor = [UIColor colorWithRed:1 green:0.5 blue:0.5 alpha:1];
            
        } else {
            // We must always handle the else case because cells can be reused.
            textInputCell.textField.textColor = [UIColor blackColor];
        }
    }
}

#pragma mark - UXRadioGroupDelegate

//- (void)radioGroup:(UXRadioGroup *)radioGroup didSelectIdentifier:(NSInteger)identifier {
//    if (radioGroup == self.radioGroup) {
//        NSLog(@"Radio group selection: %d", identifier);
//    } else if (radioGroup == self.subRadioGroup) {
//        NSLog(@"Sub radio group selection: %d", identifier);
//    }
//}
//
//- (NSString *)radioGroup:(UXRadioGroup *)radioGroup textForIdentifier:(NSInteger)identifier {
//    switch (identifier) {
//        case SubRadioOption1:
//            return @"Option 1";
//        case SubRadioOption2:
//            return @"Option 2";
//        case SubRadioOption3:
//            return @"Option 3";
//    }
//    return nil;
//}

#pragma mark - Gesture Recognizers

- (void)didTapTableView {
    [self.view endEditing:YES];
}


@end
