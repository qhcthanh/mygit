/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#if defined __cplusplus
extern "C" {
#endif
    
    /**
     * For testing whether a collection is of a certain type and is non-empty.
     *
     * @ingroup UXFrameworkCore
     * @defgroup Non-Empty-Collection-Testing Non-Empty Collection Testing
     * @{
     *
     * Simply calling -count on an object may not yield the expected results when enumerating it if
     * certain assumptions are also made about the object's type. For example, if a JSON response
     * returns a dictionary when you expected an array, casting the result to an NSArray and
     * calling count will yield a positive value, but objectAtIndex: will crash the application.
     * These methods provide a safer check for non-emptiness of collections.
     */
    
    /**
     * Tests if an object is a non-nil array which is not empty.
     */
    BOOL UXIsArrayWithObjects(id object);
    
    /**
     * Tests if an object is a non-nil set which is not empty.
     */
    BOOL UXIsSetWithObjects(id object);
    
    /**
     * Tests if an object is a non-nil string which is not empty.
     */
    BOOL UXIsStringWithAnyText(id object);
    
#if defined __cplusplus
};
#endif

