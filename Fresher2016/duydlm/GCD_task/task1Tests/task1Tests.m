//
//  task1Tests.m
//  task1Tests
//
//  Created by admin on 5/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ViewController.h"
#import "MDMutableArray.h"

@interface task1Tests : XCTestCase

@end

@implementation task1Tests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testWriteMultiThreadCrash {
    dispatch_queue_t concurentQueue = dispatch_queue_create("com.duy.threadSafeNSMutableArray", DISPATCH_QUEUE_CONCURRENT);
    
    NSMutableArray *array = [NSMutableArray new];
    for (int i=0; i<100000; i++) {
        dispatch_async(concurentQueue, ^{
            [array addObject:[NSNumber numberWithInt:i]];
        });
    }
}

- (void)testWriteMultiThreadPass {
    dispatch_queue_t concurentQueue = dispatch_queue_create("com.duy.threadSafeNSMutableArray", DISPATCH_QUEUE_CONCURRENT);
    
    MDMutableArray *array = [MDMutableArray new];
    for (int i=0; i<100000; i++) {
        dispatch_async(concurentQueue, ^{
            [array addObject:[NSNumber numberWithInt:i]];
        });
    }
}

- (void)testWriteReadMultiThreadCrash {
    dispatch_queue_t concurentQueue = dispatch_queue_create("com.duy.threadSafeNSMutableArray", DISPATCH_QUEUE_CONCURRENT);

    // Create array with 10 object
    NSMutableArray *array = [NSMutableArray new];
    for (NSUInteger i=0; i<10; i++) {
        [array addObject:[NSNumber numberWithInteger:i]];
    }
    
    //Add object while reading object in array
    dispatch_async(concurentQueue, ^{
        [array addObject:[NSNumber numberWithInteger:999]];
    });
    for (NSNumber *num in array) {
        NSLog(@"%@", num);
    }
}

- (void)testWriteReadMultiThreadPass {
    dispatch_queue_t concurentQueue = dispatch_queue_create("com.duy.threadSafeNSMutableArray", DISPATCH_QUEUE_CONCURRENT);
    
    // Create array with 10 object
    MDMutableArray *array = [MDMutableArray new];
    for (NSUInteger i=0; i<10; i++) {
        [array addObject:[NSNumber numberWithInteger:i]];
    }
    
    //Replace object while reading object in array
    dispatch_async(concurentQueue, ^{
        [array addObject:[NSNumber numberWithInteger:999]];
    });
    for (NSNumber *num in array) {
        NSLog(@"%@", num);
    }
}

- (void)testCountCrash {
    dispatch_queue_t concurentQueue = dispatch_queue_create("com.duy.threadSafeNSMutableArray", DISPATCH_QUEUE_CONCURRENT);
    
    NSMutableArray *array = [NSMutableArray new];
    for (int i=0; i<100000; i++) {
        dispatch_async(concurentQueue, ^{
            [array addObject:[NSNumber numberWithInt:i]];
        });
    }
    dispatch_async(concurentQueue, ^{
        NSLog(@"%lu", (unsigned long)[array count]);
    });
}

- (void)testCountPass {
    dispatch_queue_t concurentQueue = dispatch_queue_create("com.duy.threadSafeNSMutableArray", DISPATCH_QUEUE_CONCURRENT);
    
    MDMutableArray *array = [MDMutableArray new];
    for (int i=0; i<100000; i++) {
        dispatch_async(concurentQueue, ^{
            [array addObject:[NSNumber numberWithInt:i]];
        });
    }
    dispatch_async(concurentQueue, ^{
        NSLog(@"%lu", (unsigned long)[array count]);
    });
}

@end
