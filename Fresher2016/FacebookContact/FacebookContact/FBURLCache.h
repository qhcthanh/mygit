//
//  FBURLCache.h
//  FacebookContact
//
//  Created by VanDao on 7/25/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FBCachedURLResponse.h"
#import "NSCachedURLResponse+Extension.h"

@interface FBURLCache : NSURLCache

+ (nonnull FBURLCache *)sharedURLCache;

/**
 *  Returns the FBCachedURLResponse stored in the cache with the given request.
 *
 *  @param request the NSURLRequest to use as a key for the lookup.
 *
 *  @return The FBCachedURLResponse stored in the cache with the given request, or nil if there is no FBCachedURLResponse stored with the given request.
 */
- (nullable FBCachedURLResponse *)cachedResponseForRequest:(nonnull NSURLRequest *)request;

/**
 *  Stores the given FBCachedURLResponse in the cache using the given request.
 *
 *  @param cachedResponse The cached response to store.
 *  @param request        the FBURLRequest to use as a key for the storage.
 */
- (void)storeCachedResponse:(nonnull FBCachedURLResponse *)cachedResponse forRequest:(nonnull NSURLRequest *)request;

/**
 *  Removes the NSCachedURLResponse from the cache that is stored using the given request. No action is taken if there is no NSCachedURLResponse stored with the given request.
 *
 *  @param request  the NSURLRequest to use as a key for the lookup.
 */
- (void)removeCachedResponseForRequest:(nonnull NSURLRequest *)request;

/**
 *  Clears the given cache of any cached respones before the provide date.
 *
 *  @param date The provide date
 */
- (void)removeCachedResponsesBeforDate:(nonnull NSDate *)date;

/**
 *  Clear the given cache of any cached respones are out of date
 */
- (void)removeCachedResponsesOutOfDate;

/**
 *  Determine and notice if percent of current usage memory out of memory capacity is more than 90%
 *
 *  @return YES if the percent is more than 90%, otherwise, return NO
 */
- (BOOL)checkMemoryWarning;

/**
 *  Determine and notice if percent of current usage disk out of disk capacity is more than 90%
 *
 *  @return YES if the percent is more than 90%, otherwise, return NO
 */
- (BOOL)checkDiskWarning;

@end
