/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

/**
 * This object bundles up a set of parameters and ships them off
 * to UXNavigation's openURLAction method. This object is designed with the chaining principle in
 * mind. Once you've created a UXURLAction object, you can apply any other property to the object
 * via the apply* methods. Each of these methods returns self, allowing you to chain them.
 *
 * Example:
 * [[UXURLAction actionWithURLPath:@"zm://some/path"] applyAnimated:YES];
 * Create an autoreleased URL action object with the path @"zm://some/path" that is animated.
 *
 * For the default values, see the apply method documentation below.
 */

@interface UXURLAction : NSObject {
    
    NSString*     _urlPath;
    NSString*     _parentURLPath;
    NSDictionary* _query;
    NSDictionary* _state;
    BOOL          _animated;
    BOOL          _withDelay;
    
    CGRect        _sourceRect;
    UIView*       _sourceView;
    UIBarButtonItem* _sourceButton;
    UIViewAnimationTransition _transition;
}

@property (nonatomic, strong)   NSString*     urlPath;
@property (nonatomic, strong)   NSString*     parentURLPath;
@property (nonatomic, strong) NSDictionary* query;
@property (nonatomic, strong) NSDictionary* state;
@property (nonatomic, assign) BOOL          animated;
@property (nonatomic, assign) BOOL          withDelay;
@property (nonatomic, assign) CGRect        sourceRect;
@property (nonatomic, strong) UIView*       sourceView;
@property (nonatomic, strong) UIBarButtonItem* sourceButton;

@property (nonatomic, assign) UIViewAnimationTransition transition;

/**
 * Create an autoreleased UXURLAction object with a URL path. The path is required.
 */
+ (id)actionWithURLPath:(NSString*)urlPath;

/**
 * Initialize a UXURLAction object with a URL path. The path is required.
 */
- (id)initWithURLPath:(NSString*)urlPath;

/**
 * @default nil
 */
- (UXURLAction*)applyParentURLPath:(NSString*)parentURLPath;

/**
 * @default nil
 */
- (UXURLAction*)applyQuery:(NSDictionary*)query;

/**
 * @default nil
 */
- (UXURLAction*)applyState:(NSDictionary*)state;

/**
 * @default NO
 */
- (UXURLAction*)applyAnimated:(BOOL)animated;

/**
 * @default NO
 */
- (UXURLAction*)applyWithDelay:(BOOL)withDelay;

/**
 * @default CGRectZero
 */
- (UXURLAction*)applySourceRect:(CGRect)sourceRect;

/**
 * @default nil
 */
- (UXURLAction*)applySourceView:(UIView*)sourceView;

/**
 * @default nil
 */
- (UXURLAction*)applySourceButton:(UIBarButtonItem*)sourceButton;

/**
 * @default UIViewAnimationTransitionNone
 */
- (UXURLAction*)applyTransition:(UIViewAnimationTransition)transition;

@end

