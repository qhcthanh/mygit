/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXStyle.h"

@class UXShape;

@interface UXShapeStyle : UXStyle {
}

@property (nonatomic, retain) UXShape* shape;

+ (UXShapeStyle*)styleWithShape:(UXShape*)shape next:(UXStyle*)next;

@end
