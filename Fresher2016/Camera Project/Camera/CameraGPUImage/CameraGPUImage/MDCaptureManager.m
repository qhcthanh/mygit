//
//  MDCaptureManager.m
//  CameraGPUImage
//
//  Created by manhduydl on 8/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MDCaptureManager.h"
#import <Accelerate/Accelerate.h>
#import <UIKit/UIKit.h>
#import "OVLFilter.h"

@interface MDCaptureManager() {

}

@end

@implementation MDCaptureManager

- (instancetype)initFromViewController:(VSViewController *)vsViewController {
    self = [super init];
    if (self) {
        _captureSession = [[AVCaptureSession alloc] init];

        AVCaptureDevice* microphone = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
        if (microphone) {
            NSError* error = nil;
            AVCaptureDeviceInput * microphone_input = [AVCaptureDeviceInput deviceInputWithDevice:microphone error:&error];
            if (microphone_input) {
                [_captureSession addInput:microphone_input];
                
                _audioOutput = [[AVCaptureAudioDataOutput alloc] init];
                [_audioOutput setSampleBufferDelegate:vsViewController queue:dispatch_get_main_queue()];
                [_captureSession addOutput:_audioOutput];
            } else {
                NSLog(@"OVLVC no microphone %@", error);
            }
        }
        
        [self addCamera];
        if (_inputCamera) {
            _videoOutput = [[AVCaptureVideoDataOutput alloc] init];
            _videoOutput.videoSettings = @{
                                           (id)kCVPixelBufferPixelFormatTypeKey:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA]
                                           };
            [_videoOutput setSampleBufferDelegate:vsViewController queue:dispatch_get_main_queue()];
            [_captureSession addOutput:_videoOutput];
            for (AVCaptureConnection* connection in _videoOutput.connections) {
                if (connection.isVideoOrientationSupported) {
                    //connection.videoOrientation = [[UIDevice currentDevice] orientation];
                }
            }
            
            // For still image
            _stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
            [_stillImageOutput setOutputSettings:@{AVVideoCodecKey:AVVideoCodecJPEG}];
            if (![_captureSession canAddOutput:_stillImageOutput]) {
                NSLog(@"OVLVC Can't add stillImageOutput");
            }
            [_captureSession addOutput:_stillImageOutput];
            
            [_captureSession startRunning];
            
            if (self.fps > 0) {
                [_camera lockForConfiguration:nil];
                _camera.activeVideoMinFrameDuration = CMTimeMake(1, self.fps);
                [_camera unlockForConfiguration];
            }
        }
    }
    return self;
}

- (void)addCamera {
    AVCaptureDevicePosition position = self.fFrontCamera ? AVCaptureDevicePositionFront : AVCaptureDevicePositionBack;
    _camera = nil;
    NSArray *devices = [AVCaptureDevice devices];
    for (AVCaptureDevice *device in devices) {
        if ([device hasMediaType:AVMediaTypeVideo]) {
            if (!_camera || ([device position] == position)) {
                _camera = device;
            }
        }
    }
    NSError *error = nil;
    if (_camera) {
        NSString* preset = AVCaptureSessionPreset1280x720;
        if ([_camera supportsAVCaptureSessionPreset:preset]) {
            _captureSession.sessionPreset = preset;
        } else {
            NSLog(@"VC falling back to medium resolution %@", preset);
            _captureSession.sessionPreset = AVCaptureSessionPreset640x480;
        }
        _inputCamera = [AVCaptureDeviceInput deviceInputWithDevice:_camera error:&error];
        if (_inputCamera) {
            [_captureSession addInput:_inputCamera];
        }
        //_glkView.transform = (_camera.position==AVCaptureDevicePositionFront) ? CGAffineTransformMakeScale(-1.0, 1.0) : CGAffineTransformIdentity;
    }
}

#pragma mark - public method

- (void)startRecording {
    self.urlVideo = [NSURL fileURLWithPath:[self tempFilePath:@"mov"]];
    NSFileManager* manager = [NSFileManager defaultManager];
    [manager removeItemAtURL:self.urlVideo error:nil];
    NSError* error = nil;
    _videoWriter = [[AVAssetWriter alloc] initWithURL:self.urlVideo fileType:AVFileTypeQuickTimeMovie error:&error];
    _videoInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo
                                                     outputSettings:@{
                                                                      AVVideoCodecKey:AVVideoCodecH264,
                                                                      //AVVideoCompressionPropertiesKey:compression,
                                                                      AVVideoWidthKey:[NSNumber numberWithInt:_videoInputSize.width],
                                                                      AVVideoHeightKey:[NSNumber numberWithInt:_videoInputSize.height]
                                                                      }];
    
    if (_assetReader) {
        _videoInput.transform = _assetTransform;
        _videoInput.expectsMediaDataInRealTime = YES;
    } else {
        AVCaptureVideoOrientation orientation = AVCaptureVideoOrientationPortrait;
        switch([UIDevice currentDevice].orientation) {
            case UIDeviceOrientationLandscapeLeft:
                orientation = AVCaptureVideoOrientationLandscapeRight;
                break;
            case UIDeviceOrientationLandscapeRight:
                orientation = AVCaptureVideoOrientationLandscapeLeft;
                break;
            case UIDeviceOrientationPortraitUpsideDown:
                orientation = AVCaptureVideoOrientationPortraitUpsideDown;
                break;
            default:
                break;
        }
        _videoInput.transform = [self _transformFromCurrentVideoOrientation:AVCaptureVideoOrientationLandscapeRight
                                                              toOrientation:orientation];
        _videoInput.expectsMediaDataInRealTime = YES;
    }
    [_videoWriter addInput:_videoInput];
    
    NSDictionary* attr = @{
                           (id)kCVPixelBufferPixelFormatTypeKey: [NSNumber numberWithInt:kCVPixelFormatType_32BGRA],
                           (id)kCVPixelBufferWidthKey: [NSNumber numberWithInteger:_videoInputSize.width],
                           (id)kCVPixelBufferHeightKey: [NSNumber numberWithInteger:_videoInputSize.height]
                           };
    _adaptor = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:_videoInput sourcePixelBufferAttributes:attr];
    
    if (!self.fNoAudio) {
        // Configure the channel layout as stereo.
        AudioChannelLayout stereoChannelLayout = {
            .mChannelLayoutTag = kAudioChannelLayoutTag_Stereo,
            .mChannelBitmap = 0,
            .mNumberChannelDescriptions = 0
        };
        // Convert the channel layout object to an NSData object.
        NSData *channelLayoutAsData = [NSData dataWithBytes:&stereoChannelLayout
                                                     length:offsetof(AudioChannelLayout, mChannelDescriptions)];
        NSDictionary* audioSettings = @{
                                        AVFormatIDKey: [NSNumber numberWithUnsignedInt:kAudioFormatMPEG4AAC],
                                        AVEncoderBitRateKey  : [NSNumber numberWithInteger:128000],
                                        AVSampleRateKey : [NSNumber numberWithInteger:44100],
                                        AVChannelLayoutKey : channelLayoutAsData,
                                        AVNumberOfChannelsKey : [NSNumber numberWithUnsignedInteger:2]
                                        };
        _audioInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeAudio outputSettings:audioSettings];
        [_videoWriter addInput:_audioInput];
    }
    
    [_videoWriter startWriting];
}

- (void)stopRecording {
    [_videoInput markAsFinished];
    [_videoWriter finishWritingWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            _videoWriter = nil;
            _videoInput = nil;
            _audioInput = nil;
            _adaptor = nil;
            if ([self.delegate respondsToSelector:@selector(didFinishRecordingToOutputFileAtURL:error:)]) {
                [self.delegate didFinishRecordingToOutputFileAtURL:self.urlVideo error:nil];
            }
        });
    }];
    
    if (_videoWriter.status == AVAssetWriterStatusFailed) {
        NSLog(@"finish failed %@", _videoWriter.error);
    }
}

- (void)rotateCamera {
    _fFrontCamera = !_fFrontCamera;
    [OVLFilter setFrontCameraMode:_fFrontCamera];
    [_captureSession removeInput:_inputCamera];
    _inputCamera = nil;
    [self addCamera];
}

#pragma mark - private method

- (NSString*)tempFilePath:(NSString*)extension {
    static NSUInteger s_index = 0;
    static NSString* s_pathFolder = nil;
    NSFileManager* fm = [NSFileManager defaultManager];
    if (s_pathFolder == nil) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *pathFolder = [paths objectAtIndex:0];
        NSString* appID = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
        s_pathFolder = [pathFolder stringByAppendingPathComponent:appID];
        s_pathFolder = [s_pathFolder stringByAppendingPathComponent:@"vmTemp"];
        if ([fm fileExistsAtPath:s_pathFolder]) {
            // LATER: empty this directory
        }
    }
    if (![fm fileExistsAtPath:s_pathFolder]) {
        [fm createDirectoryAtPath:s_pathFolder withIntermediateDirectories:NO attributes:nil error:nil];
    }
    
    s_index = (s_index + 1) % 100; // large number is fine as long as we delete them.
    NSString* filename = [NSString stringWithFormat:@"%lu.%@", (unsigned long)s_index, extension];
    return [s_pathFolder stringByAppendingPathComponent:filename];
}

- (CGAffineTransform)_transformFromCurrentVideoOrientation:(AVCaptureVideoOrientation)videoOrientation toOrientation:(AVCaptureVideoOrientation)orientation
{
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    // Calculate offsets from an arbitrary reference orientation (portrait)
    if (_camera.position==AVCaptureDevicePositionFront) {
        if (orientation == AVCaptureVideoOrientationLandscapeLeft) {
            orientation = AVCaptureVideoOrientationLandscapeRight;
        } else if (orientation == AVCaptureVideoOrientationLandscapeRight) {
            orientation = AVCaptureVideoOrientationLandscapeLeft;
        }
    }
    CGFloat orientationAngleOffset = [self _angleOffsetFromPortraitOrientationToOrientation:orientation];
    CGFloat videoOrientationAngleOffset = [self _angleOffsetFromPortraitOrientationToOrientation:videoOrientation];
    
    // Find the difference in angle between the passed in orientation and the current video orientation
    CGFloat angleOffset = orientationAngleOffset - videoOrientationAngleOffset;
    transform = CGAffineTransformMakeRotation(angleOffset);
    
    return transform;
}

- (CGFloat)_angleOffsetFromPortraitOrientationToOrientation:(AVCaptureVideoOrientation)orientation
{
    CGFloat angle = 0.0;
    
    switch (orientation) {
        case AVCaptureVideoOrientationPortrait:
            angle = 0.0;
            break;
        case AVCaptureVideoOrientationPortraitUpsideDown:
            angle = M_PI;
            break;
        case AVCaptureVideoOrientationLandscapeRight:
            angle = -M_PI_2;
            break;
        case AVCaptureVideoOrientationLandscapeLeft:
            angle = M_PI_2;
            break;
        default:
            break;
    }
    
    return angle;
}


@end
