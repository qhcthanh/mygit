//
//  MDCaptureManager.h
//  CameraGPUImage
//
//  Created by manhduydl on 8/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@protocol MDCaptureManagerDelegate <NSObject>
- (void)didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL
                                      error:(NSError *)error;
@end

@class VSViewController;
@interface MDCaptureManager : NSObject 

@property (nonatomic, assign) id<MDCaptureManagerDelegate> delegate;
@property (nonatomic) BOOL isRecording;
@property (nonatomic) int32_t fps;
@property (nonatomic) BOOL fFrontCamera;
@property (nonatomic) NSURL* urlVideo;
@property (nonatomic) BOOL fNoAudio;
@property (nonatomic) CGSize videoInputSize;

@property (nonatomic) AVCaptureSession* captureSession;
@property (nonatomic) AVCaptureVideoDataOutput* videoOutput;
@property (nonatomic) AVCaptureAudioDataOutput* audioOutput;
@property (nonatomic) AVCaptureStillImageOutput* stillImageOutput;
@property (nonatomic) AVCaptureDeviceInput *inputCamera;
@property (nonatomic) AVCaptureDevice* camera;

// asset writer
@property (nonatomic) AVAssetWriter* videoWriter;
@property (nonatomic) AVAssetWriterInput* videoInput;
@property (nonatomic) AVAssetWriterInputPixelBufferAdaptor* adaptor;
@property (nonatomic) AVAssetWriterInput* audioInput;

// asset reader
@property (nonatomic) AVAssetReader *assetReader;
@property (nonatomic) AVAssetReaderTrackOutput* assetReaderOutput;
@property (nonatomic) AVAssetReaderAudioMixOutput *audioMixOutput;
@property (nonatomic) CGAffineTransform assetTransform;

- (instancetype)initFromViewController:(VSViewController *)vsViewController;

/**
 *  Start record video
 */
- (void)startRecording;

/**
 *  Stop record video
 */
- (void)stopRecording;

/**
 *  Rotate camera
 */
- (void)rotateCamera;

@end
