/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Alexey Belkevich, 2014
 * Updated by ThemND VNG Coporation, 2015
 * Change Log: Integrate Contact Framework that supports the iOS9
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>
#import <UIKit/UIKit.h>
#if __IPHONE_9_0
    #import <Contacts/Contacts.h>
#endif

#import "UXTypes.h"

@interface UXContact : NSObject

@property (nonatomic, readonly) UXContactField fieldMask;
@property (nonatomic, readonly) NSString *firstName;
@property (nonatomic, readonly) NSString *middleName;
@property (nonatomic, readonly) NSString *lastName;
@property (nonatomic, readonly) NSString *compositeName;
@property (nonatomic, readonly) NSString *company;
@property (nonatomic, readonly) NSString *jobTitle;
@property (nonatomic, readonly) NSArray *phones;
@property (nonatomic, readonly) NSArray *phonesWithLabels;
@property (nonatomic, readonly) NSArray *emails;
@property (nonatomic, readonly) NSArray *addresses;
@property (nonatomic, readonly) UIImage *photo;
@property (nonatomic, readonly) UIImage *thumbnail;
@property (nonatomic, readonly) NSNumber *recordID;
@property (nonatomic, readonly) NSDate *creationDate;
@property (nonatomic, readonly) NSDate *modificationDate;
@property (nonatomic, readonly) NSArray *socialProfiles;
@property (nonatomic, readonly) NSString *note;
@property (nonatomic, readonly) NSArray *linkedRecordIDs;
@property (nonatomic, readonly) NSString *identifier;
@property (nonatomic, readonly) NSString *displayName;

/**
 *  Initialize a contact object
 *
 *  @param recordRef The record object is loaded by address book object
 *  @param fieldMask Allow filter contact properties such as first name, last name, etc,...
 *
 *  @return A contact object that contains information based on fieldMask parameter
 */
- (id)initWithRecordRef:(ABRecordRef)recordRef fieldMask:(UXContactField)fieldMask;

#if __IPHONE_9_0
/**
 *  Initialize a contact object
 *
 *  @param cnContact The contact object is loaded by address book object
 *  @param fieldMask Allow filter contact properties such as first name, last name, etc,...
 *
 *  @return A contact object that contains information based on fieldMask parameter
 */
- (id)initWithCNContact:(CNContact*)cnContact fieldMask:(UXContactField)fieldMask;
#endif

@end
