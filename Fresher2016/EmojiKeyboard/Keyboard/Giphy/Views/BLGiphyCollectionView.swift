//
//  BLGiphyCollectionView.swift
//  EmojiKeyboard
//
//  Created by Quach Ha Chan Thanh on 8/23/16.
//  Copyright © 2016 Quach Ha Chan Thanh. All rights reserved.
//

import UIKit
import MobileCoreServices

private let kGiphyViewCell = "kGiphyViewCell"

@objc public protocol BLGiphyCollectionViewDelegate: UICollectionViewDelegate {
    
}

public class BLGiphyCollectionView: UICollectionView {
    
    // MARK: Properties
    private var giphyDataSource: [GiphyInfo] = [GiphyInfo]()
    
    public var giphyRenditionOption: GiphyRenditionOption = .FixedHeightSmall
    public weak var giphyDelegate: BLGiphyCollectionViewDelegate?
    public var giphyCollectionViewLayout: BLCollectionViewFlowLayout!
    private var isLoadGif: Bool = true
    
    
    // MARK: Initialize
    public convenience init() {
        
        // Setup CollectionView layout
        let giphyCollectionViewLayout = BLCollectionViewFlowLayout()
        giphyCollectionViewLayout.minimumInteritemSpacing = 2
        giphyCollectionViewLayout.minimumLineSpacing = 2
        giphyCollectionViewLayout.scrollDirection = .Horizontal
        
        self.init(frame: CGRectZero,collectionViewLayout: giphyCollectionViewLayout)
        
        // Asign to self
        self.giphyCollectionViewLayout = giphyCollectionViewLayout
        self.giphyCollectionViewLayout.delegate = self
        
        // Setup view
        self.contentInset = UIEdgeInsetsMake(2, 5, 2, 5)
        self.showsHorizontalScrollIndicator = false
        
        self.dataSource = self
        self.delegate = self
        
        self.registerClass(BLGiphyCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: kGiphyViewCell)
    }
    
    /**
     Set giphy source to display in this collectionView. When set giphy source
     
     - parameter giphys: The giphy array binding to collectionView
     */
    public func setGiphyDataSource(giphys: [GiphyInfo]) {
        
        self.giphyDataSource = giphys
        
        // Clean Gif. It will clean in BLDispalyGifDisplay and all current item cell in collectionView
        BLDisplayGifManager.defaultManager.deleteAllImageView()
        if self.numberOfItemsInSection(0) != 0 {
            for item in 0..<self.numberOfItemsInSection(0) {
                let indexPath = NSIndexPath(forItem: item, inSection: 0)
                
                if let cell = self.cellForItemAtIndexPath(indexPath) as? BLGiphyCollectionViewCell {
                    cell.onInvisableCell()
                }
            }
        }
        
        // Reload UI
        self.reloadData()
        self.performBatchUpdates({ 
            
            }) { (success) in
                if success {
                    self.hidden = false
                    self.setContentOffset(CGPointZero, animated: true)
                    self.updateConstraintsIfNeeded()
                }
        }
    }
    
    public func insertGiphyDataSource(giphys: [GiphyInfo]) {
        
        var indexPathInsert = [NSIndexPath]()
        for giphy in giphys {
            indexPathInsert.append(NSIndexPath(forItem: self.giphyDataSource.count, inSection: 0))
            self.giphyDataSource.append(giphy)
        }
        self.insertItemsAtIndexPaths(indexPathInsert)
    }

    
}

extension BLGiphyCollectionView: BLFlowLayoutDelegate {
    
    func collectionView(collectionView: UICollectionView, widthForPhotoAtIndexPath indexPath: NSIndexPath, withHeight: CGFloat) -> CGFloat {
        if let giphyRendition = self.giphyDataSource[indexPath.row].getGiphyRendtion(giphyRenditionOption) {
            return CGFloat(giphyRendition.fixedWidth)
        }
        
        return 0
    }
    
}

extension BLGiphyCollectionView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
         return giphyDataSource.count
    }
    
    public func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        // Get Cell reuse and set delegate
        let giphyCell = collectionView.dequeueReusableCellWithReuseIdentifier(kGiphyViewCell, forIndexPath: indexPath) as! BLGiphyCollectionViewCell
        giphyCell.delegate = self
        
        // Binding UI
        let giphyRendition = self.giphyDataSource[indexPath.row].getGiphyRendtion(giphyRenditionOption)
        giphyCell.bindingUI(giphyRendition!)
        
        return giphyCell
    }
    
    public func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
        if let giphyCell = cell as? BLGiphyCollectionViewCell {
            if isLoadGif {
                giphyCell.onVisableCell()
            } else {
                giphyCell.loadThumbGIF()
            }
        }
    }
    
    public func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
        if let giphyCell = cell as? BLGiphyCollectionViewCell {
            giphyCell.onInvisableCell()
        }
    }

}

extension BLGiphyCollectionView {
    
    public func scrollViewDidScroll(scrollView: UIScrollView) {
        if let gDelgate = self.giphyDelegate,
            let scrollViewDidScroll = gDelgate.scrollViewDidScroll
        {
            scrollViewDidScroll(scrollView)
        }
    }
    
    public func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        print("Begin draging")
        isLoadGif = false
    }
    

    public func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
      //  print("End movement scroll")
        
        isLoadGif = true
        
        for visableCell in self.visibleCells() {
            if let giphyCell = visableCell as? BLGiphyCollectionViewCell {
                giphyCell.onVisableCell()
            }
        }
    }
    
    public func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        print("will end movement scroll")
        
//        isLoadGif = true
//        
//        for visableCell in self.visibleCells() {
//            if let giphyCell = visableCell as? BLGiphyCollectionViewCell {
//                if isLoadGif {
//                    giphyCell.onVisableCell()
//                } else {
//                    // Load thumbs if has
//                }
//            }
//        }
    }
}

extension BLGiphyCollectionView: BLGiphyCollectionViewCellDelegate {
    
    public func didSelectViewCell(viewCell: BLGiphyCollectionViewCell) {
        
        BLPrint("Select")
        
        // Get giphyRendition source ID
        let giphyID = viewCell.giphyRendition?.getID()
        
        // Check giphyID not nil and cacheGif, cacheData exits
        if let giphyID = giphyID,
            let cacheGIF = BLGIFCache.shareManager().gifForKey(giphyID),
            let gifData = cacheGIF.imageData
        {
            // Copy to clipboard
            UIPasteboard.generalPasteboard().setData(gifData, forPasteboardType: kUTTypeGIF as String)
            
            // Show animation or something when copy success
            if let superView = self.superview as? BLGiphyView, let toastView = superView.giphyToastView {
                toastView.animateShowToast(1.5)
            }
        }
        
    }
    
    public func didReviewViewCell(viewCell: BLGiphyCollectionViewCell) {
        
        BLPrint("review")
        let giphyReview = viewCell.giphyRendition?.giphyInfo?.getGiphyRendtion(GiphyRenditionOption.FixedHeightDownsampled)
        let gifID: String = (giphyReview?.getID())!
        
        let reviewView = BLGifView(gifImage: viewCell.gifView.gifImage!)
        reviewView.setGifImage(NSURL(string: giphyReview!.url)!, gifIdentifier: gifID, success: {
            identifier, gif in
            if identifier == gifID {
                dispatch_async(dispatch_get_main_queue(), {
                    if let gif = gif {
                        reviewView.setGifImage(gif, contentMode: .ScaleAspectFit)
                        reviewView.startAnimatingGif()
                    }
                })
            }
        })
        
        reviewView.tag = 101
        reviewView.contentMode = .ScaleAspectFit
        self.superview!.addSubview(reviewView)
        
        reviewView.constrain(toEdgesOf: self)
        
    }
    
    public func didEndReviewViewCell(viewCell: BLGiphyCollectionViewCell) {
        
        BLPrint("endreview")
        if let review = self.superview!.viewWithTag(101) {
            review.removeFromSuperview()
        }
    }
    
}









