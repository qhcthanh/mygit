/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

/**
 * A view that mimics the iOS notification badge style.
 *
 * Any NSString can be displayed in this view, though in practice you should only show numbers
 * ranging from 1...99 or the string @"99+". Apple is quite consistent about using the red badge
 * views to represent notification badges, so you should do your best not to attach additional
 * meaning to the red badge.
 *
 * On devices running operating systems that support the tintColor property on UIViews, these
 * badges will use the tintColor by default. This behavior may be overwritten by assigning a tint
 * color explicitly.
 *
 *  @image html badge.png "A default UXBadgeView"
 *  @image html badgetinted.png "A UXBadgeView on tintColor-supporting devices"
 *
 * @ingroup UXFrameworkBadge
 */
@interface UXBadgeView : UIView

// Text attributes
/**
 * The text displayed within the badge.
 *
 * As with a UILabel you should call sizeToFit after setting the badgeView properties so that it
 * will update its frame to fit the contents.
 *
 */
@property (nonatomic, copy) NSString* text;

/**
 * The font of the text within the badge.
 *
 * The default font is:
 *
 *   iOS 6: [UIFont boldSystemFontOfSize:17]
 *   iOS 7: [UIFont systemFontOfSize:16]
 *
 */
@property (nonatomic, strong) UIFont* font;

/**
 * The color of the text in the badge.
 *
 * The default color is [UIColor whiteColor].
 *
 */
@property (nonatomic, strong) UIColor* textColor;

// Badge attributes

/**
 * The tint color of the badge.
 *
 * This is the color drawn within the badge's borders.
 *
 * The default color is
 *
 *   iOS 6: [UIColor redColor].
 *   iOS 7: self.tintColor
 *
 * On devices that support global tintColor (iOS 7) the global tint color is used unless a tint
 * color has been explicitly assigned to this badge view, in which case the assigned tint color will be used.
 *
 */
@property (nonatomic, strong) UIColor* tintColor;

/**
 * The shadow color of the badge.
 *
 * This is the shadow drawn beneath the badge's outline.
 *
 * The default color is
 *
 *   iOS 6: [UIColor colorWithWhite:0 alpha:0.5].
 *   iOS 7: nil
 *
 * On devices that support global tintColor (iOS 7) it is possible, though not encouraged, to use
 * a shadow on badges.
 *
 * @sa shadowOffset
 * @sa shadowBlur
 */
@property (nonatomic, strong) UIColor* shadowColor;

/**
 * The shadow offset (measured in points) for the badge.
 *
 * This is the offset of the shadow drawn beneath the badge's outline.
 *
 * The default value is CGSizeMake(0, 3.f).
 *
 * @sa shadowColor
 * @sa shadowBlur
 */
@property (nonatomic, assign) CGSize shadowOffset;

/**
 * The shadow blur (measured in points) for the badge.
 *
 * This is the blur of the shadow drawn beneath the badge's outline.
 *
 * The default value is 3.
 *
 * @sa shadowOffset
 * @sa shadowColor
 */
@property (nonatomic, assign) CGFloat shadowBlur;

@end

