//
//  ContactImageCaching.h
//  Project2
//
//  Created by BaoNQ on 6/1/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UIImage+Text.h"

@interface ContactImageCaching : NSObject {
}
/**
 *  Singletion method of image caching.
 */
+ (id)sharedContactImageCaching;

/**
 *  Add an image associated with aKey to cache
 *
 *  @param image An image for aKey
 *               Raises an NSInvalidArgumentException if anObject is nil. If you need to represent a nil value in the dictionary, use NSNull.
 *  @param aKey  The key for value. The key is copied (using copyWithZone:; keys must conform to the NSCopying protocol). If aKey already exists in the cache, image takes its place.
 */
- (void)cacheImage:(UIImage *)image forKey:(id<NSCopying>)aKey;

/**
 *  Returns the value associated with a given key.
 *  The value associated with aKey, or nil if no value is associated with aKey.
 *
 *  @param aKey The key for which to return the corresponding value.
 *
 *  @return The image in cache which associated with aKey
 */
- (UIImage *)imageForKey:(id<NSCopying>)aKey;

/**
 *  Remove an image from cache associated with aKey
 *  Does nothing if aKey does not exist
 *
 *  @param aKey The key to remove
 */
- (void)removeObjectForKey:(id<NSCopying>)aKey;

@end
