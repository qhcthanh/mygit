//
//  GuideViewController.swift
//  BanLaKey
//
//  Created by qhcthanh on 9/8/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import UIKit

class GuideViewController: UIViewController {

    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        
        collectionViewFlowLayout.itemSize = self.collectionView.frame.size
        collectionViewFlowLayout.minimumLineSpacing = 0
        collectionViewFlowLayout.minimumInteritemSpacing = 0
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //var dataSources =
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBarHidden = false
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension GuideViewController: UICollectionViewDataSource {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(NSStringFromClass(CTGuideCollectionViewCell.classForCoder()).componentsSeparatedByString(".").last!, forIndexPath: indexPath) as! CTGuideCollectionViewCell
        
        return cell
    }
    
    
}

extension GuideViewController: UICollectionViewDelegate {
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if let cell = self.collectionView.visibleCells().first,
            let indexPath = self.collectionView.indexPathForCell(cell)
        {
            pageControl.currentPage = indexPath.row
        }
    }
    
}




