//
//  UIScreen+Orientation.swift
//  BanLaKey
//
//  Created by BaoNQ on 8/17/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

enum BLInterfaceOrientation: Int {
    case Unknown = 0
    case Portrait
    case Landscape
}

extension UIScreen {
    func interfaceOrientation() -> BLInterfaceOrientation {
        if (self.applicationFrame.size.width > self.applicationFrame.size.height) {
            return BLInterfaceOrientation.Landscape
        }
        else if (self.applicationFrame.size.width < self.applicationFrame.size.height) {
            return BLInterfaceOrientation.Portrait
        }
        else {
            return BLInterfaceOrientation.Unknown
        }
    }
}
