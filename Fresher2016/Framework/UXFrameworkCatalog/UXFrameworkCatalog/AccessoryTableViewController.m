/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "AccessoryTableViewController.h"

#import <UXFramework/UXFramework.h>

@interface AccessoryTableViewController ()

@property (nonatomic, retain) UXTableViewDataSource* model;

// The actions are stored in a separate object from the model.
@property (nonatomic, retain) UXTableViewAction* actions;
@end

@implementation AccessoryTableViewController

- (id)initWithStyle:(UITableViewStyle)style {
    if ((self = [super initWithStyle:UITableViewStyleGrouped])) {
        self.titleString = @"Actions";
        
        // We provide a weak reference to the controller object so that the actions object can pass
        // the controller to the block when the action is executed.
        _actions = [[UXTableViewAction alloc] initWithTarget:self];
        
        // This is what an action block looks like. The block is given the object that was tapped and
        // the containing controller. In the following two blocks we simply show an alert view.
        UXActionBlock tapAction = ^BOOL(id object, UIViewController *controller, NSIndexPath* indexPath) {
            
//            [UXAlertView showAlertWithTitle:@"Notice" message:@"You tapped a cell!" cancelButton:@"Cancel" okButton:@"Ok" buttonHandler:^(UXAlertView *alert, NSInteger buttonIndex) {
//                
//            }];
            // For tap actions specifically, returning YES means that we want the cell to deselect itself
            // after the action has performed. All other types of actions ignore the return value.
            return YES;
        };
        
        UXActionBlock tapAction2 = ^BOOL(id object, UIViewController *controller, NSIndexPath* indexPath) {
//            [UXAlertView showAlertWithTitle:@"Notice" message:@"Alternative tap action!" cancelButton:@"Cancel" okButton:@"Ok" buttonHandler:^(UXAlertView *alert, NSInteger buttonIndex) {
//                
//            }];
            return YES;
        };
        
        NSArray* tableContents =
        [NSArray arrayWithObjects:
         
         // As seen in previous examples, this will show a cell that is not actionable. Tapping it will
         // not highlight it, nor are any cell accessories shown.
         [UXTitleTableViewCellObject objectWithTitle:@"No action attached"],
         
         @"",
         // This attaches one of the tap action blocks defined above to this cell. Notice how we are
         // using the return value of attachTapAction:toObject: here. The attach:toObject: methods all
         // return the object that was passed in, allowing you to simultaneously add an object to a
         // table model and attach an action to it.
         [_actions attachToObject:[UXTitleTableViewCellObject objectWithTitle:@"Tap me"]
                         tapBlock:tapAction],
         
         @"",
         // This attaches a navigation action to the object. We use a standard method,
         // UXPushControllerAction, here which simply instantiates the given class and then pushes it
         // to the current navigation controller. This method does not work in all situations, in which
         // cases you should implement your own block. When this cell appears it will automatically be
         // assigned a navigation accessory type.
         [_actions attachToObject:[UXTitleTableViewCellObject objectWithTitle:@"Navigate elsewhere"]
                  navigationBlock:UXPushViewControllerAction([AccessoryTableViewController class], nil)],
         
         // This attaches a detail action to the object, displaying a detail accessory on the cell that
         // can be tapped. When the detail accessory is tapped, this action is executed.
         [_actions attachToObject:[UXTitleTableViewCellObject objectWithTitle:@"Detail action"]
                      detailBlock:UXPushViewControllerAction([AccessoryTableViewController class], nil)],
         
         // It is possible to attach multiple types of actions to a single object. In the next three
         // examples we show attaching different types of actions to objects.
         [_actions attachToObject:
          [_actions attachToObject:[UXTitleTableViewCellObject objectWithTitle:@"Navigate and detail"]
                   navigationBlock:UXPushViewControllerAction([AccessoryTableViewController class], nil)]
                      detailBlock:UXPushViewControllerAction([AccessoryTableViewController class], nil)],
         
         [_actions attachToObject:
          [_actions attachToObject:[UXTitleTableViewCellObject objectWithTitle:@"Tap and detail"]
                          tapBlock:tapAction]
                      detailBlock:UXPushViewControllerAction([AccessoryTableViewController class], nil)],
         
         // When you have a tap and navigation action attached to an object, both will be executed when
         // you tap the cell. Try tapping this cell and you will see that it navigates and shows an
         // alert view simultaneously.
         [_actions attachToObject:
          [_actions attachToObject:
           [_actions attachToObject:[UXTitleTableViewCellObject objectWithTitle:@"All actions"]
                    navigationBlock:UXPushViewControllerAction([AccessoryTableViewController class], nil)]
                          tapBlock:tapAction]
                      detailBlock:UXPushViewControllerAction([AccessoryTableViewController class], nil)],
         
         @"Implicit Actions",
         // It is possible to set up implicit actions by attaching an action to a class. When such an
         // attachment is made, all instances of that class (including subclasses of that class) will
         // implicitly have the given action. For example, we attach below a tap action to the
         // NISubtitleCellObject class. Now when we include any NISubtitleCellObject or subclass of
         // NISubtitleCellObject in the model they will automatically respond to taps.
         [UXSubtitleTableViewCellObject objectWithTitle:@"Tap me" subtitle:@"Implicit action"],
         
         // You can also explicitly override the implicit action by attaching an action directly to
         // the object.
         [_actions attachToObject:[UXSubtitleTableViewCellObject objectWithTitle:@"Override" subtitle:@"Explicit tap action"]
                         tapBlock:tapAction2],
         
         @"Selector Actions",
         // Consider attaching a selector to an object instead of a block when an action requires
         // complex logic. The selector will be performed on the UXTableViewActions target and the
         // attached object will be provided as the first argument.
         [_actions attachToObject:[UXTitleTableViewCellObject objectWithTitle:@"Tap me"]
                      tapSelector:@selector(didTapObject:)],
         
         nil];
        
        // This attaches a tap action to all instances of NISubtitleCellObject and is a great way to
        // handle common cells.
        [_actions attachToClass:[UXSubtitleTableViewCellObject class] tapBlock:tapAction];
        
        _model = [[UXTableViewDataSource alloc] initWithSectionedArray:tableContents
                                                         delegate:(id)[UXTableViewCellFactory class]];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self.model;
    
    // What we're doing here is known as "delegate chaining". It uses the message forwarding
    // functionality of Objective-C to insert the actions object between the table view
    // and this controller. The actions object forwards UITableViewDelegate methods along and
    // selectively intercepts methods required to make user interactions work.
    //
    // Experiment: try commenting out this line. You'll notice that you can no longer tap any of
    // the cells in the table view and that they no longer show the disclosure accessory types.
    // Cool, eh? That this functionality is all provided to you in one line should make you
    // heel-click.
    self.tableView.delegate = [self.actions forwardingTo:self];
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
//    return UXIsSupportedOrientation(toInterfaceOrientation);
//}

#pragma mark - Actions

- (BOOL)didTapObject:(id)object {
    NSLog(@"Did tap object %@", object);
    return YES;
}


@end
