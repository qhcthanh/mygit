//
//  CustomMutableArray.h
//  Task1_GCD
//
//  Created by VanDao on 5/31/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface STMutableArray : NSObject

/**
 *  Inserts a given object into the array’s contents at a given index.
 *
 *  @param anObject The object to add to the array's content. This value must not be nil.
 *  @param index    The index in the array at which to insert anObject. This value must not be greater than the count of elements in the array.
 */
- (void)insertObject:(id) anObject
             atIndex:(NSUInteger)index;

/**
 *  Removes the object at index .
 *
 *  @param index The index from which to remove the object in the array. The value must not exceed the bounds of the array.
 */
- (void)removeObjectAtIndex:(NSUInteger)index;

/**
 *  Inserts a given object at the end of the array.
 *
 *  @param anObject The object to add to the end of the array’s content. This value must not be nil.
 */

- (void)addObject:(id)anObject;

/**
 *  Count number of object in array
 *
 *  @return number of object in array
 */

- (NSUInteger)count;

/**
 *  Get 1 object in array
 *
 *  @param index index of object which is got
 *
 *  @return 1 object in array
 */
- (id)objectAtIndex:(NSUInteger)index;

/**
 *  Replaces the object at index with anObject.
 *
 *  @param index    The index of the object to be replaced. This value must not exceed the bounds of the array.
 *  @param anObject The object with which to replace the object at index index in the array. This value must not be nil.
 */
- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject;

/**
 *  Empties the array of all its elements.
 
 */
- (void)removeAllObjects;

@end
