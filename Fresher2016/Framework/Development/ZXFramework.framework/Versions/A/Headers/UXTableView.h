/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

@interface UXTableView : UITableView

@property (nonatomic, assign) BOOL allowTrackingFPS;
@property (nonatomic, assign, readonly) CGFloat averageFPS;

- (void)resetScrollingPerformanceCounters;

@end

@protocol UXTableViewDelegate <UITableViewDelegate>

- (void)tableView:(UITableView*)tableView touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event;
- (void)tableView:(UITableView*)tableView touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event;

@end
