/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
/**
 * Define our big multiline macros so all the other macros will be easy to read.
 */
extern int UXLogLevel;

#ifndef LOG_LEVEL_DEF
    #define LOG_LEVEL_DEF UXLogLevel
#endif

#define LOG_MACRO(isAsynchronous, lvl, flg, ctx, fnct, frmt, ...) \
[UXLog log:isAsynchronous                                       \
level:lvl                                                  \
flag:flg                                                  \
context:ctx                                                  \
file:__FILE__                                             \
function:fnct                                                 \
line:__LINE__                                             \
format:(frmt), ##__VA_ARGS__]


#define LOG_OBJC_MACRO(async, lvl, flg, ctx, frmt, ...) \
LOG_MACRO(async, lvl, flg, ctx, sel_getName(_cmd), frmt, ##__VA_ARGS__)

#define LOG_C_MACRO(async, lvl, flg, ctx, frmt, ...) \
LOG_MACRO(async, lvl, flg, ctx, __FUNCTION__, frmt, ##__VA_ARGS__)

#define  SYNC_LOG_OBJC_MACRO(lvl, flg, ctx, frmt, ...) \
LOG_OBJC_MACRO( NO, lvl, flg, ctx, frmt, ##__VA_ARGS__)

#define ASYNC_LOG_OBJC_MACRO(lvl, flg, ctx, frmt, ...) \
LOG_OBJC_MACRO(YES, lvl, flg, ctx, frmt, ##__VA_ARGS__)

#define  SYNC_LOG_C_MACRO(lvl, flg, ctx, frmt, ...) \
LOG_C_MACRO( NO, lvl, flg, ctx, frmt, ##__VA_ARGS__)

#define ASYNC_LOG_C_MACRO(lvl, flg, ctx, frmt, ...) \
LOG_C_MACRO(YES, lvl, flg, ctx, frmt, ##__VA_ARGS__)

#define LOG_MAYBE(async, lvl, flg, ctx, fnct, frmt, ...) \
do { if(lvl & flg) LOG_MACRO(async, lvl, flg, ctx, fnct, frmt, ##__VA_ARGS__); } while(0)

#define LOG_OBJC_MAYBE(async, lvl, flg, ctx, frmt, ...) \
LOG_MAYBE(async, lvl, flg, ctx, sel_getName(_cmd), frmt, ##__VA_ARGS__)

#define LOG_C_MAYBE(async, lvl, flg, ctx, frmt, ...) \
LOG_MAYBE(async, lvl, flg, ctx, __FUNCTION__, frmt, ##__VA_ARGS__)

#define  SYNC_LOG_OBJC_MAYBE(lvl, flg, ctx, frmt, ...) \
LOG_OBJC_MAYBE( NO, lvl, flg, ctx, frmt, ##__VA_ARGS__)

#define ASYNC_LOG_OBJC_MAYBE(lvl, flg, ctx, frmt, ...) \
LOG_OBJC_MAYBE(YES, lvl, flg, ctx, frmt, ##__VA_ARGS__)

#define  SYNC_LOG_C_MAYBE(lvl, flg, ctx, frmt, ...) \
LOG_C_MAYBE( NO, lvl, flg, ctx, frmt, ##__VA_ARGS__)

#define ASYNC_LOG_C_MAYBE(lvl, flg, ctx, frmt, ...) \
LOG_C_MAYBE(YES, lvl, flg, ctx, frmt, ##__VA_ARGS__)

/**
 * Define the standard options.
 * 
 * We default to only 4 levels because it makes it easier for beginners
 * to make the transition to a logging framework.
 * 
 * More advanced users may choose to completely customize the levels (and level names) to suite their needs.
 * For more information on this see the "Custom Log Levels" page:
 * https://github.com/robbiehanson/CocoaLumberjack/wiki/CustomLogLevels
 * 
 * Advanced users may also notice that we're using a bitmask.
 * This is to allow for custom fine grained logging:
 * https://github.com/robbiehanson/CocoaLumberjack/wiki/FineGrainedLogging
 * 
 * -- Flags --
 * 
 * Typically you will use the LOG_LEVELS (see below), but the flags may be used directly in certain situations.
 * For example, say you have a lot of warning log messages, and you wanted to disable them.
 * However, you still needed to see your error and info log messages.
 * You could accomplish that with the following:
 * 
 * static const int UXLogLevel = LOG_FLAG_ERROR | LOG_FLAG_INFO;
 * 
 * Flags may also be consulted when writing custom log formatters,
 * as the UXLogMessage class captures the individual flag that caused the log message to fire.
 * 
 * -- Levels --
 * 
 * Log levels are simply the proper bitmask of the flags.
 * 
 * -- Booleans --
 * 
 * The booleans may be used when your logging code involves more than one line.
 * For example:
 * 
 * if (LOG_VERBOSE) {
 *     for (id sprocket in sprockets)
 *         UXLogVerbose(@"sprocket: %@", [sprocket description])
 * }
 * 
 * -- Async --
 * 
 * Defines the default asynchronous options.
 * The default philosophy for asynchronous logging is very simple:
 * 
 * Log messages with errors should be executed synchronously.
 *     After all, an error just occurred. The application could be unstable.
 * 
 * All other log messages, such as debug output, are executed asynchronously.
 *     After all, if it wasn't an error, then it was just informational output,
 *     or something the application was easily able to recover from.
 * 
 * -- Changes --
 * 
 * You are strongly discouraged from modifying this file.
 * If you do, you make it more difficult on yourself to merge future bug fixes and improvements from the project.
 */

#define LOG_FLAG_ERROR    (1 << 0)  // 0...00001
#define LOG_FLAG_WARN     (1 << 1)  // 0...00010
#define LOG_FLAG_INFO     (1 << 2)  // 0...00100
#define LOG_FLAG_DEBUG    (1 << 3)  // 0...01000
#define LOG_FLAG_VERBOSE  (1 << 4)  // 0...10000
#define LOG_FLAG_ACTION   (1 << 5)  // 0..100000
#define LOG_FLAG_STATIC   (1 << 6)  // 0.1000000
#define LOG_FLAG_SOCKET   (1 << 7)  // 010000000
#define LOG_FLAG_VOIP     (1 << 8)  // 100000000

#define LOG_LEVEL_OFF     0
#define LOG_LEVEL_ERROR   (LOG_FLAG_ERROR)                                                                      // 0...00001
#define LOG_LEVEL_WARN    (LOG_FLAG_ERROR | LOG_FLAG_WARN)                                                      // 0...00011
#define LOG_LEVEL_INFO    (LOG_FLAG_ERROR | LOG_FLAG_WARN | LOG_FLAG_INFO)                                      // 0...00111
#define LOG_LEVEL_DEBUG   (LOG_FLAG_ERROR | LOG_FLAG_WARN | LOG_FLAG_INFO | LOG_FLAG_DEBUG)                     // 0...01111
#define LOG_LEVEL_VERBOSE (LOG_FLAG_ERROR | LOG_FLAG_WARN | LOG_FLAG_INFO | LOG_FLAG_DEBUG | LOG_FLAG_VERBOSE)  // 0...11111
#define LOG_LEVEL_ACTION  (LOG_FLAG_ACTION)
#define LOG_LEVEL_STATIC  (LOG_FLAG_STATIC)
#define LOG_LEVEL_SOCKET  (LOG_FLAG_SOCKET)
#define LOG_LEVEL_VOIP    (LOG_FLAG_VOIP)
#define LOG_LEVEL_ALL      0xFFFFFFFF // 1111....11111 (LOG_LEVEL_VERBOSE plus any other flags)

#define LOG_ERROR         (LOG_LEVEL_DEF & LOG_FLAG_ERROR)
#define LOG_WARN          (LOG_LEVEL_DEF & LOG_FLAG_WARN)
#define LOG_INFO          (LOG_LEVEL_DEF & LOG_FLAG_INFO)
#define LOG_DEBUG         (LOG_LEVEL_DEF & LOG_FLAG_DEBUG)
#define LOG_VERBOSE       (LOG_LEVEL_DEF & LOG_FLAG_VERBOSE)
#define LOG_ACTION        (LOG_LEVEL_DEF & LOG_FLAG_ACTION)
#define LOG_STATIC        (LOG_LEVEL_DEF & LOG_FLAG_STATIC)
#define LOG_SOCKET        (LOG_LEVEL_DEF & LOG_FLAG_SOCKET)
#define LOG_VOIP          (LOG_LEVEL_DEF & LOG_FLAG_VOIP)

#define LOG_ASYNC_ENABLED YES

#define LOG_ASYNC_ERROR    ( NO && LOG_ASYNC_ENABLED)
#define LOG_ASYNC_WARN     (YES && LOG_ASYNC_ENABLED)
#define LOG_ASYNC_INFO     (YES && LOG_ASYNC_ENABLED)
#define LOG_ASYNC_DEBUG    (YES && LOG_ASYNC_ENABLED)
#define LOG_ASYNC_VERBOSE  (YES && LOG_ASYNC_ENABLED)
#define LOG_ASYNC_ACTION   (YES && LOG_ASYNC_ENABLED)
#define LOG_ASYNC_STATIC   (YES && LOG_ASYNC_ENABLED)
#define LOG_ASYNC_SOCKET   (YES && LOG_ASYNC_ENABLED)
#define LOG_ASYNC_VOIP     (YES && LOG_ASYNC_ENABLED)

#define UXLogError(frmt, ...)   LOG_OBJC_MAYBE(LOG_ASYNC_ERROR,   LOG_LEVEL_DEF, LOG_FLAG_ERROR,   0, frmt, ##__VA_ARGS__)
#define UXLogWarn(frmt, ...)    LOG_OBJC_MAYBE(LOG_ASYNC_WARN,    LOG_LEVEL_DEF, LOG_FLAG_WARN,    0, frmt, ##__VA_ARGS__)
#define UXLogInfo(frmt, ...)    LOG_OBJC_MAYBE(LOG_ASYNC_INFO,    LOG_LEVEL_DEF, LOG_FLAG_INFO,    0, frmt, ##__VA_ARGS__)
#define UXLogDebug(frmt, ...)   LOG_OBJC_MAYBE(LOG_ASYNC_DEBUG,   LOG_LEVEL_DEF, LOG_FLAG_DEBUG,   0, frmt, ##__VA_ARGS__)
#define UXLogVerbose(frmt, ...) LOG_OBJC_MAYBE(LOG_ASYNC_VERBOSE, LOG_LEVEL_DEF, LOG_FLAG_VERBOSE, 0, frmt, ##__VA_ARGS__)
#define UXLogAction(frmt, ...)  LOG_OBJC_MAYBE(LOG_ASYNC_ACTION, LOG_LEVEL_DEF, LOG_FLAG_ACTION, 0, frmt, ##__VA_ARGS__)
#define UXLogStatic(frmt, ...)  LOG_OBJC_MAYBE(LOG_ASYNC_STATIC, LOG_LEVEL_DEF, LOG_FLAG_STATIC, 0, frmt, ##__VA_ARGS__)
#define UXLogSocket(frmt, ...)  LOG_OBJC_MAYBE(LOG_ASYNC_SOCKET, LOG_LEVEL_DEF, LOG_FLAG_SOCKET, 0, frmt, ##__VA_ARGS__)
#define UXLogVoip(frmt, ...)  LOG_OBJC_MAYBE(LOG_ASYNC_VOIP, LOG_LEVEL_DEF, LOG_FLAG_VOIP, 0, frmt, ##__VA_ARGS__)


#define UXLogCError(frmt, ...)   LOG_C_MAYBE(LOG_ASYNC_ERROR,   LOG_LEVEL_DEF, LOG_FLAG_ERROR,   0, frmt, ##__VA_ARGS__)
#define UXLogCWarn(frmt, ...)    LOG_C_MAYBE(LOG_ASYNC_WARN,    LOG_LEVEL_DEF, LOG_FLAG_WARN,    0, frmt, ##__VA_ARGS__)
#define UXLogCInfo(frmt, ...)    LOG_C_MAYBE(LOG_ASYNC_INFO,    LOG_LEVEL_DEF, LOG_FLAG_INFO,    0, frmt, ##__VA_ARGS__)
#define UXLogCDebug(frmt, ...)   LOG_C_MAYBE(LOG_ASYNC_DEBUG,   LOG_LEVEL_DEF, LOG_FLAG_DEBUG,   0, frmt, ##__VA_ARGS__)
#define UXLogCVerbose(frmt, ...) LOG_C_MAYBE(LOG_ASYNC_VERBOSE, LOG_LEVEL_DEF, LOG_FLAG_VERBOSE, 0, frmt, ##__VA_ARGS__)
#define UXLogCAction(frmt, ...)  LOG_C_MAYBE(LOG_ASYNC_ACTION, LOG_LEVEL_DEF, LOG_FLAG_ACTION, 0, frmt, ##__VA_ARGS__)
#define UXLogCStatic(frmt, ...)  LOG_C_MAYBE(LOG_ASYNC_STATIC, LOG_LEVEL_DEF, LOG_FLAG_STATIC, 0, frmt, ##__VA_ARGS__)
#define UXLogCSocket(frmt, ...)  LOG_C_MAYBE(LOG_ASYNC_SOCKET, LOG_LEVEL_DEF, LOG_FLAG_SOCKET, 0, frmt, ##__VA_ARGS__)
#define UXLogCVoip(frmt, ...)  LOG_C_MAYBE(LOG_ASYNC_VOIP, LOG_LEVEL_DEF, LOG_FLAG_VOIP, 0, frmt, ##__VA_ARGS__)

/**
 * The THIS_FILE macro gives you an NSString of the file name.
 * For simplicity and clarity, the file name does not include the full path or file extension.
 * 
 * For example: UXLogWarn(@"%@: Unable to find thingy", THIS_FILE) -> @"MyViewController: Unable to find thingy"
 **/

NSString *ExtractFileNameWithoutExtension(const char *filePath, BOOL copy);

#define THIS_FILE (ExtractFileNameWithoutExtension(__FILE__, NO))

/**
 * The THIS_METHOD macro gives you the name of the current objective-c method.
 * 
 * For example: UXLogWarn(@"%@ - Requires non-nil strings") -> @"setMake:model: requires non-nil strings"
 * 
 * Note: This does NOT work in straight C functions (non objective-c).
 * Instead you should use the predefined __FUNCTION__ macro.
 **/

#define THIS_METHOD NSStringFromSelector(_cmd)


// Default configuration and safety/sanity values.
// 
// maximumFileSize         -> DEFAULT_LOG_MAX_FILE_SIZE
// rollingFrequency        -> DEFAULT_LOG_ROLLING_FREQUENCY
// maximumNumberOfLogFiles -> DEFAULT_LOG_MAX_NUM_LOG_FILES
// 
// You should carefully consider the proper configuration values for your application.
#define DEFAULT_LOG_MAX_FILE_SIZE     (1024 * 1024)   //  1 MB
#define DEFAULT_LOG_ROLLING_FREQUENCY (60 * 60 * 24)  // 24 Hours
#define DEFAULT_LOG_MAX_NUM_LOG_FILES (2)             //  5 Files

// We probably shouldn't be using UXLog() statements within the UXLog implementation.
// But we still want to leave our log statements for any future debugging,
// and to allow other developers to trace the implementation (which is a great learning tool).
// 
// So we use primitive logging macros around NSLog.
// We maintain the NS prefix on the macros to be explicit about the fact that we're using NSLog.

#define LOG_LEVEL 2

//#define NSLogError(frmt, ...)    do{ if(LOG_LEVEL >= 1) NSLog((frmt), ##__VA_ARGS__); } while(0)
//#define NSLogWarn(frmt, ...)     do{ if(LOG_LEVEL >= 2) NSLog((frmt), ##__VA_ARGS__); } while(0)
//#define NSLogInfo(frmt, ...)     do{ if(LOG_LEVEL >= 3) NSLog((frmt), ##__VA_ARGS__); } while(0)
//#define NSLogVerbose(frmt, ...)  do{ if(LOG_LEVEL >= 4) NSLog((frmt), ##__VA_ARGS__); } while(0)
