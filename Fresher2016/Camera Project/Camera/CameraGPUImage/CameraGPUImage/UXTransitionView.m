/* Copyright (c) 2016-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "UXTransitionView.h"

@interface UXTransitionView () {
@protected
    CGPoint translation;
    CGFloat scale;
    CGFloat rotation;
}

@end

@implementation UXTransitionView

- (void)drawBorder {
    
    if (self.borderLayer)
        [self.borderLayer removeFromSuperlayer];
    
    // Draw rectange border by four vetices
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:self.vertexOne];
    [path addLineToPoint:self.vertexTwo];
    [path moveToPoint:self.vertexTwo];
    [path addLineToPoint: self.vertexThree];
    [path moveToPoint:self.vertexThree];
    [path addLineToPoint:self.vertexFour];
    [path moveToPoint:self.vertexFour];
    [path addLineToPoint:self.vertexOne];
    
    // Set border property
    self.borderLayer = [CAShapeLayer layer];
    self.borderLayer.path = [path CGPath];
    self.borderLayer.strokeColor = [[UIColor redColor]CGColor];
    self.borderLayer.lineWidth = 2.0;
    self.borderLayer.fillColor = [[UIColor clearColor]CGColor];
    
    // Add border to superview
    [self.superview.layer addSublayer:self.borderLayer];
}

- (UIImage *)getImage {
    return nil;
}

@end
