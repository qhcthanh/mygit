//
//  CTTableView.swift
//  BanLaKey
//
//  Created by qhcthanh on 9/8/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import UIKit

public class CTTableView: UITableView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    var dataSources = [CTCategoryInfo]() {
        didSet {
            self.reloadData()
        }
    }
    
    private var tableViewCellReuseIdentifiers = [
        "ImageTitleTableCell",
        "TitleTableCell",
        "TitleSwitchTableCell",
        ]
    
    convenience init() {
        self.init(frame: CGRectZero, style: .Grouped)
    }
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        self.setupTableView()
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupTableView()
    }
    
    private func setupTableView() {
        
        self.delegate = self
        self.dataSource = self
        
        self.separatorStyle = .SingleLine
        self.allowsSelection = true
        self.backgroundColor = .clearColor()
        
        for tableViewCellReuseIdentifier in tableViewCellReuseIdentifiers {
            self.registerNib(UINib(nibName: tableViewCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: tableViewCellReuseIdentifier)
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }

}

extension CTTableView: UITableViewDataSource {
    
    public func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return dataSources.count
    }
    
    public func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSources[section].items.count
    }
    
    public func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let item = dataSources[indexPath.section].items[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier(item.cellIdentifier.rawValue) as! CTTableViewCell
        
        if item.navigationBlock != nil {
            cell.accessoryType = .DisclosureIndicator
        }
        
        cell.label.text = item.title
        if let iconImageView = cell.iconImageView {
            iconImageView.image = item.image
        }
        
        if item.isCheck {
            cell.accessoryType = .Checkmark
        }
        
        cell.delegate = self
        
        cell.separatorInset.left = cell.label.frame.origin.x
        
        return cell
    }
    
    
    
    public func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return dataSources[section].footerTitle
    }
    
    public func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return dataSources[section].headerTitle
    }
    
    public func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return dataSources[section].footerHeight
    }
    
    public func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return dataSources[section].headerHeight
    }
    
    public func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return dataSources[indexPath.section].items[indexPath.row].itemHeight
    }
    
}

extension CTTableView: UITableViewDelegate {
    
    public func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        
         let item = dataSources[indexPath.section].items[indexPath.row]
        
        if let navigationBlock = item.navigationBlock {
            navigationBlock()
            return nil
        }
        
        if item.isCheckMark {
            if let cell = tableView.cellForRowAtIndexPath(indexPath) {
                if cell.accessoryType != .Checkmark {
                    
                    for i in 0..<dataSources[indexPath.section].items.count {
                        if let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forItem: i, inSection: indexPath.section))
                        {
                            cell.accessoryType = .None
                        }
                    }
                    
                    cell.accessoryType = .Checkmark
                }
            }
        }
        
        return nil
    }
    
    public func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let item = dataSources[indexPath.section].items[indexPath.row]
        if let selectBlock = item.selectBlock {
            selectBlock()
            
        }
    }
    
    public func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        let item = dataSources[indexPath.section].items[indexPath.row]
        
        if let deSelectBlock = item.deSelectBlock {
            deSelectBlock()
            
        }
    }
}


extension CTTableView: CTTableViewCellDelegate {
    
    func changeSwitchValue(tableViewCell: CTTableViewCell, value: Bool) {
        if let indexPath = self.indexPathForCell(tableViewCell) {
            let item = dataSources[indexPath.section].items[indexPath.row]
            
            if let switchBlock = item.switchBlock {
                switchBlock(value)
            }
        }
    }
    
}




