//
//  MDMutableArray.m
//  task1
//
//  Created by manhduydl on 5/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MDMutableArray.h"

@interface MDMutableArray() {
    NSMutableArray *internalMutableArray;
    dispatch_queue_t concurentQueue;
}

@end

@implementation MDMutableArray

- (id)init {
    if (self = [super init]) {
        internalMutableArray = [NSMutableArray new];
        concurentQueue = dispatch_queue_create("com.duy.concurentQueue", DISPATCH_QUEUE_CONCURRENT);
    }
    return self;
}

/**
 *  Count number of object in array
 *
 *  @return number of object in array
 */
-(NSUInteger)count {
    __block NSUInteger count;
    dispatch_sync(concurentQueue, ^{
        count = [internalMutableArray count];
    });
    return count;
}

/**
 *  Get 1 object in array
 *
 *  @param index index of object which is got
 *
 *  @return 1 object in array
 */
- (id)objectAtIndex:(NSUInteger)index {
    __block id obj;
    dispatch_sync(concurentQueue, ^{
        obj = [internalMutableArray objectAtIndex:index];
    });
    return obj;
}

/**
 *  Add 1 object into array
 *
 *  @param anObject Object is added
 */
- (void)addObject:(id)anObject {
    dispatch_barrier_async(concurentQueue, ^{
        [internalMutableArray addObject:anObject];
    });
}

/**
 *  Insert 1 object into array
 *
 *  @param anObject Object is inserted
 *  @param index    Index at which object is inserted
 */
- (void)insertObject:(id)anObject atIndex:(NSUInteger)index {
    dispatch_barrier_async(concurentQueue, ^{
        [internalMutableArray insertObject:anObject atIndex:index];
    });
}

/**
 *  Remove 1 object from  array
 *
 *  @param index Index of object is removed
 */
- (void)removeObjectAtIndex:(NSUInteger)index {
    dispatch_barrier_async(concurentQueue, ^{
        [internalMutableArray removeObjectAtIndex:index];
    });
}

@end
