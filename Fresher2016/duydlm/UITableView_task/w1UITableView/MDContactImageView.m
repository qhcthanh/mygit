//
//  MDContactImageView.m
//  w1UITableView
//
//  Created by manhduydl on 5/23/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MDContactImageView.h"
#import "MDImageManager.h"
#import "Contants.h"

@interface MDContactImageView()

@property UIImageView *internalUIImageView;

@end

@implementation MDContactImageView

- (void)setImageForContact:(Contact *)contact {
    __weak __typeof(self)wself = self;
    __block UIImage *image = [[MDImageManager sharedInstance] getImageFromCache:contact];
    if (image != nil) {
        dispatch_main_sync_safe(^{
            if (!wself) return;
            if (image != nil) {
                wself.image = image;
                [wself setNeedsLayout];
            }
        });
    } else {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            __weak __typeof(self)wself = self;
            image = [[MDImageManager sharedInstance] loadImageOfContact:contact];
            if (!wself) return;
            dispatch_main_sync_safe(^{
                if (!wself) return;
                if (image != nil) {
                    wself.image = image;
                    [wself setNeedsLayout];
                }
            });
        });
    }
}

@end
