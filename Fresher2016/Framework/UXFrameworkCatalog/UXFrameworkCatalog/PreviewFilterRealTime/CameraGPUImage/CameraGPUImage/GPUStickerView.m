//
//  GPUStickerView.m
//  CameraGPUImage
//
//  Created by VanDao on 6/17/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "GPUStickerView.h"

@implementation GPUStickerView{
    dispatch_queue_t serialQueue;
    
    NSMutableArray *stickerList;
    
    NSInteger touchCount;
    NSMutableArray<UITouch *> *touchList;
    NSMutableArray *beginPointList;
    NSMutableArray *movePointList;
    
    id curFocusSticker;
    BOOL isMoved;
}

- (instancetype _Nonnull)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if (self){
        [self setOpaque:NO];
        self.userInteractionEnabled = YES;
        self.multipleTouchEnabled = YES;
        self.backgroundColor = [UIColor clearColor];

        touchList = [[NSMutableArray alloc]init];
        beginPointList = [[NSMutableArray alloc]init];
        movePointList = [[NSMutableArray alloc]init];
        stickerList = [[NSMutableArray alloc]init];
        
        serialQueue = dispatch_queue_create("daonv.com.serialqueue.GPUStickerView", NULL);
    }
    
    return self;
};

- (NSArray * _Nullable)getStickerList{
    return stickerList;
}

- (void)removeAllSticker{
    for (id sticker in stickerList){
        [sticker removeLayer];
        [sticker removeFromSuperview];
    }
    
    [stickerList removeAllObjects];
}


- (void)addSticker:(StickerNode * _Nonnull)sticker{
    [self addSubview:sticker];
    [self bringSubviewToFront:sticker];
    [stickerList insertObject:sticker atIndex:0];
}

-(void)removeSticker:(StickerNode * _Nonnull)sticker{
    [sticker removeLayer];
    [sticker removeFromSuperview];
    [stickerList removeObject:sticker];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    touchCount += [touches count];
    
    for (UITouch *touch in [touches allObjects])
    {
        [touchList addObject:touch];
        [beginPointList addObject:[NSValue valueWithCGPoint:[touch locationInView:self]]];
        [movePointList addObject:[NSValue valueWithCGPoint:[touch locationInView:self]]];
        
        CGPoint touchPoint = [[movePointList objectAtIndex:0] CGPointValue];
        NSLog(@"Touch: %f %f", touchPoint.x, touchPoint.y);
        
        if (curFocusSticker == nil){
            for (StickerNode *sticker in stickerList){
                if ([self isAPointLocatedInTheRectangle:touchPoint rectangle:[sticker getVerticesArray]]){
                    curFocusSticker = sticker;
                    
                    StickerNode* firstSticker = curFocusSticker;
                    [stickerList removeObject:curFocusSticker];
                    [stickerList insertObject:firstSticker atIndex:0];
                    
                    [self bringSubviewToFront:curFocusSticker];
                    
                    isMoved = NO;
                    
                    break;
                }
            }
        }
    }
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    for (UITouch *touch in [touches allObjects])
    {
        NSInteger index = [touchList indexOfObject:touch];
        [movePointList replaceObjectAtIndex:index withObject:[NSValue valueWithCGPoint:[touch locationInView:self]]];
    }
    
    if (curFocusSticker)
        [self transformSticker:curFocusSticker];
    else if (curFocusSticker == nil && [movePointList count] > 1){
        CGPoint finger1 = [[movePointList objectAtIndex:0] CGPointValue];
        CGPoint finger2 = [[movePointList objectAtIndex:1] CGPointValue];
        
        for (StickerNode *sticker in stickerList){
            if ([self touchedInRect:[sticker getVerticesArray] byPoints:finger1 :finger2]){
                curFocusSticker = sticker;
                
                StickerNode* firstSticker = curFocusSticker;
                [stickerList removeObject:curFocusSticker];
                [stickerList insertObject:firstSticker atIndex:0];
                
                [self bringSubviewToFront:curFocusSticker];
                
                break;
            }
        }
    }
    
    if (curFocusSticker){
        isMoved = YES;
        if (self.touchDelegate && [self.touchDelegate respondsToSelector:@selector(didTouchMoveEndSticker:position:)])
            [self.touchDelegate didTouchMoveEndSticker:curFocusSticker position:[[movePointList objectAtIndex:0] CGPointValue]];
    }
    
    //Update new loc
    for (UITouch *touch in [touches allObjects])
    {
        NSInteger index = [touchList indexOfObject:touch];
        [beginPointList replaceObjectAtIndex:index withObject:[NSValue valueWithCGPoint:[touch locationInView:self]]];
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    //Check if touch up inside textstickernode
    if ([curFocusSticker isKindOfClass:[TextStickerNode class]] && !isMoved){
        if (self.touchDelegate && [self.touchDelegate respondsToSelector:@selector(didTouchUpInsideTextSticker:)])
            [self.touchDelegate didTouchUpInsideTextSticker:curFocusSticker];
    }
    if (curFocusSticker && isMoved){
        for (UITouch *touch in touches){
            if ([touchList indexOfObject:touch] == 0){
                if ( self.touchDelegate && [self.touchDelegate respondsToSelector:@selector(didTouchEndSticker:position:)]) {
                    [self.touchDelegate didTouchEndSticker:curFocusSticker position:[[movePointList objectAtIndex:0] CGPointValue]];
                    break;
                }
            }
        }
    }
    
    touchCount -= [touches count];
    
    if (touchCount == 0)
        curFocusSticker = nil;
    
    for (UITouch *touch in [touches allObjects])
    {
        CGPoint loc1 = [touch locationInView:self];
        int i = 0;
        for (i = 0; i < [touchList count]; i++)
        {
            CGPoint loc2 = [[touchList objectAtIndex:i] locationInView:self];
            
            if (loc1.x == loc2.x && loc1.y == loc2.y)
                break;
        }
        
        [touchList removeObjectAtIndex:i];
        [beginPointList removeObjectAtIndex:i];
        [movePointList removeObjectAtIndex:i];
    }
}

- (void)transformSticker:(StickerNode * _Nonnull)sticker{
    [self bringSubviewToFront:sticker];
    
    CGPoint translate = CGPointMake(0.0f, 0.0f);
    CGFloat scale = 1.0f;
    CGFloat rotate = 0.0f;
    
    if (touchCount == 1)
    {
        CGPoint beginPoint = [[beginPointList objectAtIndex:0] CGPointValue];
        CGPoint movePoint = [[movePointList objectAtIndex:0] CGPointValue];
        
        CGFloat xDist = movePoint.x - beginPoint.x;
        CGFloat yDist = movePoint.y - beginPoint.y;
        
        translate.x = xDist;
        translate.y = yDist;
    }
    else
    {
        CGPoint beginPoint = [[beginPointList objectAtIndex:0] CGPointValue];
        CGPoint movePoint = [[movePointList objectAtIndex:0] CGPointValue];
        CGPoint beginPoint1 = [[beginPointList objectAtIndex:1] CGPointValue];
        CGPoint movePoint1 = [[movePointList objectAtIndex:1] CGPointValue];
        
        //Translate
        CGPoint point1 = CGPointMake((beginPoint.x + beginPoint1.x) / 2, (beginPoint.y + beginPoint1.y)/2);
        CGPoint point2 = CGPointMake((movePoint.x + movePoint1.x) / 2, (movePoint.y + movePoint1.y)/2);
        
        CGFloat xDist = point2.x - point1.x;
        CGFloat yDist = point2.y - point1.y;
        
        translate.x = xDist;
        translate.y = yDist;
        
        //Scale
        CGFloat xDist1 = beginPoint1.x - beginPoint.x;
        CGFloat yDist1 = beginPoint1.y - beginPoint.y;
        CGFloat dis1 = sqrt(xDist1 * xDist1 + yDist1 * yDist1);
        
        CGFloat xDist2 = movePoint1.x - movePoint.x;
        CGFloat yDist2 = movePoint1.y - movePoint.y;
        CGFloat dis2 = sqrt(xDist2 * xDist2 + yDist2 * yDist2);
        
        scale = dis2 / dis1;
        
        //Rotate
        CGFloat deltaX0 = (beginPoint.x - beginPoint1.x);
        CGFloat deltaY0 = (beginPoint.y - beginPoint1.y);
        CGFloat radian0 = atan2(deltaY0, deltaX0);
        
        CGFloat deltaX = (movePoint.x - movePoint1.x);
        CGFloat deltaY = (movePoint.y - movePoint1.y);
        CGFloat radian = atan2(deltaY, deltaX);
        
        rotate = (radian - radian0);
    }
    
    [sticker setTransformWithTranslate:translate scale:scale rotate:rotate];
    
    [sticker render];
}

- (BOOL)isAPointLocatedInTheRectangle:(CGPoint)point rectangle:(NSMutableArray * _Nonnull)rect {
    CGPoint point0 = [[rect objectAtIndex:0] CGPointValue];
    CGPoint point1 = [[rect objectAtIndex:1] CGPointValue];
    CGPoint point2 = [[rect objectAtIndex:2] CGPointValue];
    CGPoint point3 = [[rect objectAtIndex:3] CGPointValue];
    
    NSMutableArray *linearEquation1 = [self linearEquation:point0 :point1];
    NSMutableArray *linearEquation2 = [self linearEquation:point1 :point2];
    NSMutableArray *linearEquation3 = [self linearEquation:point2 :point3];
    NSMutableArray *linearEquation4 = [self linearEquation:point0 :point3];
    
    CGPoint origin = CGPointMake((point0.x + point2.x)/2, (point0.y + point2.y)/2);
    
    if (![self is2PointsLocatedOnTheSameSide:point :origin withLinearEquation:linearEquation1])
        return NO;
    if (![self is2PointsLocatedOnTheSameSide:point :origin withLinearEquation:linearEquation2])
        return NO;
    if (![self is2PointsLocatedOnTheSameSide:point :origin withLinearEquation:linearEquation3])
        return NO;
    if (![self is2PointsLocatedOnTheSameSide:point :origin withLinearEquation:linearEquation4])
        return NO;
    return YES;
}

- (BOOL)touchedInRect:(NSMutableArray * _Nonnull)rect byPoints:(CGPoint)beginPoint :(CGPoint)endPoint {
    CGPoint point0 = [[rect objectAtIndex:0] CGPointValue];
    CGPoint point1 = [[rect objectAtIndex:1] CGPointValue];
    CGPoint point2 = [[rect objectAtIndex:2] CGPointValue];
    CGPoint point3 = [[rect objectAtIndex:3] CGPointValue];
    
    NSMutableArray *ptDT = [self linearEquation:beginPoint :endPoint];
    NSMutableArray *ptDuongCheo1 = [self linearEquation:point0 :point2];
    NSMutableArray *ptDuongCheo2 = [self linearEquation:point1 :point3];
    
    float a,b,c;
    a = [[ptDT objectAtIndex:0] floatValue];
    b = [[ptDT objectAtIndex:1] floatValue];
    c = [[ptDT objectAtIndex:2] floatValue];
    
    /* Tim he toa do giao diem cua pt duong thang va duong cheo 1 */
    float a1,b1,c1;
    a1 = [[ptDuongCheo1 objectAtIndex:0] floatValue];
    b1 = [[ptDuongCheo1 objectAtIndex:1] floatValue];
    c1 = [[ptDuongCheo1 objectAtIndex:2] floatValue];
    
    float x,y;
    float A,B;
    
    if (b == 0) {
        x = -b/a;
        
        A = b1;
        B = c1 - (a1*c/a);
        
        // PT co vo so nghiem
        if (A == 0 && B == 0)
            return YES;
        
        // PT vo nghiem
        if (A == 0)
            return NO;
        
        y = -B/A;
    }
    else {
        A = a1 - (b1*a/b);
        B = (-b1*c/b) + c1;
        
        // PT co vo so nghiem
        if (A == 0 && B == 0)
            return YES;
        
        // PT vo nghiem
        if (A == 0)
            return NO;
        
        x = -B/A;
        y = - (a*x +c)/b;
    }
    
    if ([self isAPointLocatedInTheRectangle:CGPointMake(x, y) rectangle:rect] && (x < fmaxf(beginPoint.x, endPoint.x)) && (x > fminf(beginPoint.x, endPoint.x)) && (y < fmaxf(beginPoint.y, endPoint.y)) && (y > fminf(beginPoint.y, endPoint.y)))
        return YES;
    
    /* Tim he toa do giao diem cua pt duong thang va duong cheo 2 */
    a1 = [[ptDuongCheo2 objectAtIndex:0] floatValue];
    b1 = [[ptDuongCheo2 objectAtIndex:1] floatValue];
    c1 = [[ptDuongCheo2 objectAtIndex:2] floatValue];
    
    
    if (b == 0) {
        x = -b/a;
        
        A = b1;
        B = c1 - (a1*c/a);
        
        // PT co vo so nghiem
        if (A == 0 && B == 0)
            return YES;
        
        // PT vo nghiem
        if (A == 0)
            return NO;
        
        y = -B/A;
    }
    else {
        A = a1 - (b1*a/b);
        B = (-b1*c/b) + c1;
        
        // PT co vo so nghiem
        if (A == 0 && B == 0)
            return YES;
        
        // PT vo nghiem
        if (A == 0)
            return NO;
        
        x = -B/A;
        y = - (a*x +c)/b;
    }
    
    if ([self isAPointLocatedInTheRectangle:CGPointMake(x, y) rectangle:rect] && (x < fmaxf(beginPoint.x, endPoint.x)) && (x > fminf(beginPoint.x, endPoint.x)) && (y < fmaxf(beginPoint.y, endPoint.y)) && (y > fminf(beginPoint.y, endPoint.y)))
        return YES;
    
    return NO;
}


- (NSMutableArray * _Nonnull)linearEquation:(CGPoint)point1 :(CGPoint)point2 {
    CGPoint u = CGPointMake(point2.x - point1.x, point2.y - point1.y);
    float a = -u.y;
    float b = u.x;
    float c = -a*point1.x - b*point1.y;
    
    NSMutableArray *result = [[NSMutableArray alloc] init];
    [result addObject:[NSNumber numberWithFloat:a]];
    [result addObject:[NSNumber numberWithFloat:b]];
    [result addObject:[NSNumber numberWithFloat:c]];
    
    return result;
    
}

- (BOOL) is2PointsLocatedOnTheSameSide:(CGPoint)point1 :(CGPoint)point2 withLinearEquation:(NSMutableArray * _Nonnull)linearEquation {
    float a = [[linearEquation objectAtIndex:0] floatValue];
    float b = [[linearEquation objectAtIndex:1] floatValue];
    float c = [[linearEquation objectAtIndex:2] floatValue];
    
    CGFloat value1 = (a * point1.x + b * point1.y + c);
    CGFloat value2 = (a * point2.x + b * point2.y + c);
    
    if (value1 * value2 >= 0)
        return YES;
    return NO;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [super drawRect:rect];
}

@end








