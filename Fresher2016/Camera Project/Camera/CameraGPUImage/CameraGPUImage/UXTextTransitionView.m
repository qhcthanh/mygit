/* Copyright (c) 2016-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "UXTextTransitionView.h"

#define kTextMaxFontSize 100

@interface UXTextTransitionView () {
    UILabel *contentLabel;
}

@property (readwrite) NSString *textContent;
@property (readwrite) UIFont *textFont;
@property (readwrite) UIColor *textColor;

@end

@implementation UXTextTransitionView

- (id _Nullable)initWithText:(NSString * _Nonnull)text colorValue:(CGFloat)colorValue font:(UIFont * _Nullable)font {
    return [self initWithText:text colorValue:colorValue font:font position:CGPointMake(kWidth/2, kHeight/2)];
}

- (id _Nullable)initWithText:(NSString * _Nonnull)text colorValue:(CGFloat)colorValue font:(UIFont * _Nullable)font position:(CGPoint)position {
    
    CGSize sizeTextT = [text sizeWithAttributes:@{NSFontAttributeName:font}];
    CGFloat width = MIN(sizeTextT.width, kWidth);
    int numberLine = ceil(sizeTextT.width / width);
    CGFloat height = sizeTextT.height * numberLine;
    NSLog(@"line: %d width: %f height: %f",numberLine, width, height);
    
    self = [super initWithFrame:CGRectMake(position.x - width/2, position.y - height / 2, width, height)];
    
    if (self){
        
        self.opaque = NO;
        self.userInteractionEnabled = NO;
        self.multipleTouchEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        
        contentLabel = [[UILabel alloc]init];
        contentLabel.numberOfLines = 5;
        contentLabel.backgroundColor = [UIColor clearColor];
        contentLabel.minimumScaleFactor = 1/kTextMaxFontSize;
        contentLabel.adjustsFontSizeToFitWidth = YES;
        contentLabel.textAlignment = NSTextAlignmentLeft;
        contentLabel.text = text;
        contentLabel.textColor = [self colorForValue:colorValue];
        contentLabel.font = [font fontWithSize:kTextMaxFontSize];
        _colorValue = colorValue;
        contentLabel.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        [self addSubview:contentLabel];
        _textContent = contentLabel.text;
        _textFont = font;
        _textColor = contentLabel.textColor;
        self.vertexOne = CGPointMake(position.x - width/2, position.y - height/2);
        self.vertexTwo = CGPointMake(position.x + width/2, position.y - height/2);
        self.vertexThree = CGPointMake(position.x + width/2, position.y + height/2);
        self.vertexFour = CGPointMake(position.x - width/2, position.y + height/2);
        self.width = width;
        self.height = height;
    }
    
    return self;
}

-(void)updateFrameContentLabel:(CGRect)frame {
    contentLabel.frame = CGRectMake(contentLabel.frame.origin.x, contentLabel.frame.origin.y, frame.size.width, frame.size.height);
    self.frame = contentLabel.frame;
}

- (void)updateText:(NSString * _Nullable)text colorValue:(CGFloat)colorValue font:(UIFont * _Nullable)font {
    if (text)
        contentLabel.text = text;
    contentLabel.textColor = [self colorForValue:colorValue];
    _colorValue = colorValue;
    if (font)
        contentLabel.font = [font fontWithSize:kTextMaxFontSize];
}

- (UIImage * _Nullable)getImage {
    /*
     UIGraphicsBeginImageContextWithOptions(imageView.image.size, NO, imageView.image.scale);
     
     [imageView.image drawAtPoint:CGPointZero];
     CGFloat scale = imageView.image.size.width / kWidth;
     CGContextScaleCTM(UIGraphicsGetCurrentContext(), scale, scale);
     [imageView.layer renderInContext:UIGraphicsGetCurrentContext()];
     
     UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     */
    return nil;
}

- (UIColor*)colorForValue:(CGFloat)value
{
    if(value < 1/3.0){
        return [UIColor colorWithWhite:value/0.3 alpha:1];
    }
    return [UIColor colorWithHue:((value-1/3.0)/0.7)*2/3.0 saturation:1 brightness:1 alpha:1];
}


@end
