/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXLogFormatter.h"

/**
 * The UXLogger protocol specifies that an optional formatter can be added to a logger.
 * Most (but not all) loggers will want to support formatters.
 * 
 * However, writting getters and setters in a thread safe manner,
 * while still maintaining maximum speed for the logging process, is a difficult task.
 * 
 * To do it right, the implementation of the getter/setter has strict requiremenets:
 * - Must NOT require the logMessage method to acquire a lock.
 * - Must NOT require the logMessage method to access an atomic property (also a lock of sorts).
 * 
 * To simplify things, an abstract logger is provided that implements the getter and setter.
 * 
 * Logger implementations may simply extend this class,
 * and they can ACCESS THE FORMATTER VARIABLE DIRECTLY from within their logMessage method!
 */

static void *const GlobalLoggingQueueIdentityKey = (void *)&GlobalLoggingQueueIdentityKey;

@interface UXAbstractLogger : NSObject <UXLogger> {
	id <UXLogFormatter> formatter;
	
	dispatch_queue_t loggerQueue;
}

- (id <UXLogFormatter>)logFormatter;
- (void)setLogFormatter:(id <UXLogFormatter>)formatter;

// For thread-safety assertions
- (BOOL)isOnGlobalLoggingQueue;
- (BOOL)isOnInternalLoggerQueue;

@end
