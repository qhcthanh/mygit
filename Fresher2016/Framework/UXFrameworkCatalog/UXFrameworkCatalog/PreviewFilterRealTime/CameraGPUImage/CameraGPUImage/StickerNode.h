//
//  StickerNode.h
//  CameraGPUImage
//
//  Created by VanDao on 6/18/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <GPUImage.h>

#define kWidth [[UIScreen mainScreen] bounds].size.width
#define kHeight [[UIScreen mainScreen] bounds].size.height
#define __PI 3.14159265359

@interface StickerNode : GPUImageView
{
    GPUImagePicture *texture;
    
    //Tranform detail
    CGFloat rotate;
    CGFloat scale;
    CGPoint translate;
    
    NSMutableArray *vertices;
}

- (NSMutableArray * _Nonnull)getVerticesArray;
- (UIImage * _Nonnull)getImage;
- (void)addTarget:(id<GPUImageInput> _Nonnull)newTarget;
- (void)addStickerWithImage:(UIImage * _Nonnull)newImageSource;
- (void)setTransformWithTranslate:(CGPoint)translate scale:(CGFloat)scale rotate:(CGFloat)rotate;
- (void)render;

- (void)removeLayer;

@end
