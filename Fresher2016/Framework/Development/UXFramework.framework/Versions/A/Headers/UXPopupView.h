/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>


#import "UXView.h"

@interface UXPopupView : UXView

@property (nonatomic, assign) BOOL isShowing;

/**
 *  Show a popup view
 *
 *  @param animation Apply animation if the animation is set YES
 */
- (void)showPopupAnimate:(BOOL)animation;

/**
 *  Hide a popup view
 *
 *  @param animation Apply animation if the animation is set YES
 */
- (void)hidePopupAnimate:(BOOL)animation;

@end
