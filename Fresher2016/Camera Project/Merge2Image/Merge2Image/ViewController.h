//
//  ViewController.h
//  Merge2Image
//
//  Created by BaoNQ on 6/17/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

