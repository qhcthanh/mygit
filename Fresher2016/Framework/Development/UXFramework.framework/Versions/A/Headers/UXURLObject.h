/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@protocol UXURLObject <NSObject>
@optional

/**
 * Converts the object to a URL using UXURLMap.
 */
@property (nonatomic, readonly) NSString* URLValue;

/**
 * Converts the object to a specially-named URL using UXURLMap.
 */
- (NSString*)URLValueWithName:(NSString*)name;

@end
