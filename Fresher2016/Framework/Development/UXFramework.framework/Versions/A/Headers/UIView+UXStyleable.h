/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@class UXCSSRuleset;
@class UXDOM;

extern NSString* const UXCSSViewKey;
extern NSString* const UXCSSViewIdKey;
extern NSString* const UXCSSViewCssClassKey;
extern NSString* const UXCSSViewTextKey;
extern NSString* const UXCSSViewTagKey;
extern NSString* const UXCSSViewTargetSelectorKey;
extern NSString* const UXCSSViewSubviewsKey;
extern NSString* const UXCSSViewAccessibilityLabelKey;
extern NSString* const UXCSSViewBackgroundColorKey;

@interface UIView (UXStyleable)

/**
 * Applies the given rule set to this view. Call applyViewStyleWithRuleSet:inDOM: instead.
 *
 * This method is exposed primarily for subclasses to use when implementing the
 * applyStyleWithRuleSet: method from UXStyleable.
 */
- (void)applyViewStyleWithRuleSet:(UXCSSRuleset *)ruleSet DEPRECATED_ATTRIBUTE;

/**
 * Applies the given rule set to this view.
 *
 * This method is exposed primarily for subclasses to use when implementing the
 * applyStyleWithRuleSet: method from UXStyleable.
 */
- (void)applyViewStyleWithRuleSet:(UXCSSRuleset *)ruleSet inDOM: (UXDOM*) dom;

/**
 * Describes the given rule set when applied to this view.
 *
 * This method is exposed primarily for subclasses to use when implementing the
 * descriptionWithRuleSetFor:forPseudoClass:inDOM:withViewName: method from UXStyleable.
 */
- (NSString*) descriptionWithRuleSetForView: (UXCSSRuleset*) ruleSet forPseudoClass: (NSString*) pseudo inDOM: (UXDOM*) dom withViewName: (NSString*) name;

/**
 * Build a view hierarchy. The array is a list of view specs, where viewSpec is a loosely formatted
 * sequence delineated by UIViews. After a UIView, the type of the next object determines what is done
 * with it:
 *   UIView instance - following values will be applied to this UIView (other than Class, which "starts anew")
 *   Class - a UIView subclass that will be alloc'ed and init'ed
 *   NSString starting with a hash - view id (for style application)
 *   NSString starting with a dot - CSS Class (for style application) (you can do this multiple times per view)
 *   NSString - accessibility label for the view.
 *   NIUserInterfaceString - .text or .title(normal) on a button. Asserts otherwise
 *   NSNumber - tag
 *   NSInvocation - selector for TouchUpInside (e.g. on a UIButton)
 *   NSArray - passed to build on the active UIView and results added as subviews
 *   NSDictionary - if you're squeamish about this whole Javascript duck typing auto detecting fancyness
 *        you can pass a boring old dictionary with named values:
 *        UXCSSViewKey, UXCSSViewIdKey, UXCSSViewCssClassKey, UXCSSViewTextKey, UXCSSViewTagKey,
 *        UXCSSViewTargetSelectorKey, UXCSSViewSubviewsKey, UXCSSViewAccessibilityLabelKey
 *
 *   Example (including inline setting of self properties):
 *    [self.view buildSubviews: @[
 *       self.buttonContainer = UIView.alloc.init, @"#LoginContainer", @[
 *         self.myButton = UIButton.alloc.init, @"#Login", @".primary"
 *       ]
 *    ]];
 */
- (NSArray*) buildSubviews: (NSArray*) viewSpecs inDOM: (UXDOM*) dom;

/// View frame and bounds manipulation helpers
@property (nonatomic) CGFloat frameWidth;
@property (nonatomic) CGFloat frameHeight;
@property (nonatomic) CGFloat frameMinX;
@property (nonatomic) CGFloat frameMidX;
@property (nonatomic) CGFloat frameMaxX;
@property (nonatomic) CGFloat frameMinY;
@property (nonatomic) CGFloat frameMidY;
@property (nonatomic) CGFloat frameMaxY;

@end

