/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

#import "UXViewController.h"

@interface UXPopupViewController : UXViewController

@property (nonatomic, retain) id userInfo;

- (void)showInView:(UIView*)view animated:(BOOL)animated;

- (void)dismissPopupViewControllerAnimated:(BOOL)animated;

- (void)showFromBarButtonItem:(UIBarButtonItem *)item animated:(BOOL)animated;

- (void)showFromRect:(CGRect)rect inView:(UIView *)view animated:(BOOL)animated;

@end
