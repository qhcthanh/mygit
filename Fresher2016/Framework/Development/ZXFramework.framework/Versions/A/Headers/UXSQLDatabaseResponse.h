/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@class UXSQLDatabaseRequest;
@protocol UXSQLDatabaseResponse <NSObject>
@required

/**
 * Processes the data from a successful request and determines if it is valid.
 *
 * If the data is not valid, return an error. The data will not be cached if there is an error.
 *
 * @param  request    The request this response is bound to.
 * @param  response   The response object, useful for getting the status code.
 * @param  data       The data received from the IMURLRequest.
 * @return NSError	  if there was an error accessing the data.
 *
 * @required
 */
- (NSError*)access:(UXSQLDatabaseRequest*)request processResponse:(id)response data:(id)data;

@end
