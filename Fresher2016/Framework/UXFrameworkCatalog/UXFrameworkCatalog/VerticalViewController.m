/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import "VerticalViewController.h"

#import <UXFramework/UXFramework.h>

#import "PageView.h"



@interface VerticalViewController ()<UXPagingScrollViewDataSource, UXPagingScrollViewDelegate>
@property (nonatomic, retain) UXPagingScrollView* pagingScrollView;

@end

@implementation VerticalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        self.titleString = @"Vertical Scrollview";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // iOS 7-only.
    self.view.backgroundColor = [UIColor blackColor];
    
    self.pagingScrollView = [[UXPagingScrollView alloc] initWithFrame:self.view.bounds];
    
    // This is the only change from the BasicInstantiation example.
    self.pagingScrollView.type = UXPagingScrollViewVertical;
    
    self.pagingScrollView.autoresizingMask = UIViewAutoresizingFlexibleDimensions;
    self.pagingScrollView.delegate = self;
    self.pagingScrollView.dataSource = self;
    [self.view addSubview:self.pagingScrollView];
    [self.pagingScrollView reloadData];
}

- (void)didReceiveMemoryWarning {
    self.pagingScrollView = nil;
    
    [super didReceiveMemoryWarning];
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    return UXIsSupportedOrientation(interfaceOrientation);
//}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self.pagingScrollView willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                         duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self.pagingScrollView willAnimateRotationToInterfaceOrientation:toInterfaceOrientation
                                                            duration:duration];
}

#pragma mark - UXPagingScrollViewDataSource

- (NSInteger)numberOfPagesInPagingScrollView:(UXPagingScrollView *)pagingScrollView {
    return 10;
}

- (UIView<UXPagingScrollViewPage> *)pagingScrollView:(UXPagingScrollView *)pagingScrollView
                                    pageViewForIndex:(NSInteger)pageIndex {
    // Check the reusable page queue.
    PageView *page = (PageView *)[pagingScrollView dequeueReusablePageWithIdentifier:NSStringFromClass([PageView class])];
    // If no page was in the reusable queue, we need to create one.
    if (nil == page) {
        page = [[PageView alloc] initWithReuseIdentifier:NSStringFromClass([PageView class])];
    }
    return page;
}


- (void)pagingScrollViewDidScroll:(UXPagingScrollView *)pagingScrollView {
    UX_CONSOLE_LOGGER(@"pagingScrollViewDidScroll");
}

#pragma mark Changing Pages /** @name [UXPagingScrollViewDelegate] Changing Pages */

/**
 * The current page will change.
 *
 * pagingScrollView.centerPageIndex will reflect the old page index, not the new
 * page index.
 */
- (void)pagingScrollViewWillChangePages:(UXPagingScrollView *)pagingScrollView {
     UX_CONSOLE_LOGGER(@"pagingScrollViewWillChangePages");
}

/**
 * The current page has changed.
 *
 * pagingScrollView.centerPageIndex will reflect the changed page index.
 */
- (void)pagingScrollViewDidChangePages:(UXPagingScrollView *)pagingScrollView {
    UX_CONSOLE_LOGGER(@"pagingScrollViewWillChangePages");
}

@end
