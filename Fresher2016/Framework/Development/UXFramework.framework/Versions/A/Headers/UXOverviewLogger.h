/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

extern NSString* const UXOverviewLoggerDidAddDeviceLog;
extern NSString* const UXOverviewLoggerDidAddConsoleLog;
extern NSString* const UXOverviewLoggerDidAddEventLog;

@class UXOverviewDeviceLogEntry;
@class UXOverviewConsoleLogEntry;
@class UXOverviewEventLogEntry;

/**
 * The Overview logger.
 *
 * @ingroup Overview-Logger
 *
 * This object stores all of the historical information used to draw the graphs in the
 * Overview memory and disk pages, as well as the console log page.
 *
 * The primary log should be accessed by calling [UXOverview @link UXOverview::logger logger@endlink].
 */
@interface UXOverviewLogger : NSObject

#pragma mark Configuration Settings

/**
 * The oldest age of a memory or disk log entry.
 *
 * Log entries older than this number of seconds will be pruned from the log.
 *
 * By default this is 1 minute.
 */
@property (nonatomic, assign) NSTimeInterval oldestLogAge;


+ (UXOverviewLogger*)sharedLogger;


#pragma mark Adding Log Entries

/**
 * Add a device log.
 *
 * This method will first prune expired entries and then add the new entry to the log.
 */
- (void)addDeviceLog:(UXOverviewDeviceLogEntry *)logEntry;

/**
 * Add a console log.
 *
 * This method will not prune console log entries.
 */
- (void)addConsoleLog:(UXOverviewConsoleLogEntry *)logEntry;

/**
 * Add a event log.
 *
 * This method will first prune expired entries and then add the new entry to the log.
 */
- (void)addEventLog:(UXOverviewEventLogEntry *)logEntry;


#pragma mark Accessing Logs

/**
 * The linked list of device logs.
 *
 * Log entries are in increasing chronological order.
 */
@property (nonatomic, readonly, strong) NSMutableOrderedSet* deviceLogs;

/**
 * The linked list of console logs.
 *
 * Log entries are in increasing chronological order.
 */
@property (nonatomic, readonly, strong) NSMutableOrderedSet* consoleLogs;

/**
 * The linked list of events.
 *
 * Log entries are in increasing chronological order.
 */
@property (nonatomic, readonly, strong) NSMutableOrderedSet* eventLogs;

@end


/**
 * The basic requirements for a log entry.
 *
 * @ingroup Overview-Logger-Entries
 *
 * A basic log entry need only define a timestamp in order to be particularly useful.
 */
@interface UXOverviewLogEntry : NSObject

#pragma mark Creating an Entry /** @name Creating an Entry */

/**
 * Designated initializer.
 */
- (id)initWithTimestamp:(NSDate *)timestamp;


#pragma mark Entry Information

/**
 * The timestamp for this log entry.
 */
@property (nonatomic, retain) NSDate* timestamp;

@end


/**
 * A device log entry.
 *
 * @ingroup Overview-Logger-Entries
 */
@interface UXOverviewDeviceLogEntry : UXOverviewLogEntry

#pragma mark Entry Information

/**
 * The number of bytes of free memory.
 */
@property (nonatomic, assign) unsigned long long bytesOfFreeMemory;

/**
 * The number of bytes of total memory.
 */
@property (nonatomic, assign) unsigned long long bytesOfTotalMemory;

/**
 * The number of bytes of free disk space.
 */
@property (nonatomic, assign) unsigned long long bytesOfFreeDiskSpace;

/**
 * The number of bytes of total disk space.
 */
@property (nonatomic, assign) unsigned long long bytesOfTotalDiskSpace;

/**
 * The battery level.
 */
@property (nonatomic, assign) CGFloat batteryLevel;

/**
 * The state of the battery.
 */
@property (nonatomic, assign) UIDeviceBatteryState batteryState;

@end


/**
 * A console log entry.
 *
 * @ingroup Overview-Logger-Entries
 */
@interface UXOverviewConsoleLogEntry : UXOverviewLogEntry

#pragma mark Creating an Entry /** @name Creating an Entry */

/**
 * Designated initializer.
 */
- (id)initWithLog:(NSString *)log;


#pragma mark Entry Information

/**
 * The text that was written to the console log.
 */
@property (nonatomic, copy) NSString* log;

@end


typedef enum {
    UXOverviewEventDidReceiveMemoryWarning,
} UXOverviewEventType;

/**
 * An event log entry.
 *
 * @ingroup Overview-Logger-Entries
 */
@interface UXOverviewEventLogEntry : UXOverviewLogEntry

#pragma mark Creating an Entry /** @name Creating an Entry */

/**
 * Designated initializer.
 */
- (id)initWithType:(NSInteger)type;


#pragma mark Entry Information /** @name Entry Information */

/**
 * The type of event.
 */
@property (nonatomic, assign) NSInteger type;

@end
