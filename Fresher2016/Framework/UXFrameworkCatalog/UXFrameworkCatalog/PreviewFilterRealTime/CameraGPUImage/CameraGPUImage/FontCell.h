//
//  FontCell.h
//  CameraGPUImage
//
//  Created by qhcthanh on 6/24/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FontCell : UICollectionViewCell

-(void)renderUI:(NSString*)fontName;
-(void)cleanUI;
@end
