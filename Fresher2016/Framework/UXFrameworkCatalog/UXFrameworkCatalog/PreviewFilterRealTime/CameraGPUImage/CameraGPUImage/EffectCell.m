//
//  EffectCell.m
//  CameraGPUImage
//
//  Created by qhcthanh on 6/23/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "EffectCell.h"

@protocol EffectCellDelegate <NSObject>

@optional
-(void)didSelectEffect;

@end

@implementation EffectCell {
    
    __weak IBOutlet UIImageView *effectImageView;
    __weak IBOutlet UILabel *effectNameLabel;
}

-(void)renderUI:(UIImage*)imageEffect name:(NSString*)nameEffect {
    effectNameLabel.text = nameEffect;
    effectImageView.image = imageEffect;
}

-(void)prepareForReuse {
    effectImageView.image = nil;
    effectNameLabel.text = @"";
}

@end
