/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXPagingScrollViewPage.h"
#import "UXPhotoAlbumScrollViewDataSource.h"

@protocol UXPhotoScrollViewDelegate;
@class UXCenteringScrollView;

/**
 * A single photo view that supports zooming and rotation.
 *
 */
@interface UXPhotoScrollView : UIView <UIScrollViewDelegate, UXPagingScrollViewPage>

#pragma mark Configuring Functionality
/**
 * Whether the photo is allowed to be zoomed.
 *
 * By default this is YES.
 *
 */
@property (nonatomic, assign, getter=isZoomingEnabled) BOOL zoomingIsEnabled; // default: yes

/**
 * Whether small photos can be zoomed at least until they fit the screen.
 *
 * If this is disabled, images smaller than the view size can not be zoomed in beyond
 * their original dimensions.
 *
 * If this is enabled, images smaller than the view size can be zoomed in only until
 * they fit the view bounds.
 *
 * The default behavior in Photos.app allows small photos to be zoomed in.
 *
 * @attention This will allow photos to be zoomed in even if they don't have any more
 *                 pixels to show, causing the photo to blur. This can look ok for photographs,
 *                 but might not look ok for software design mockups.
 *
 * By default this is YES.
 *
 */
@property (nonatomic, assign, getter=isZoomingAboveOriginalSizeEnabled) BOOL zoomingAboveOriginalSizeIsEnabled; // default: yes

/**
 * Whether double-tapping zooms in and out of the image.
 *
 * Available on iOS 3.2 and later.
 *
 * By default this is YES.
 *
 */
@property (nonatomic, assign, getter=isDoubleTapToZoomEnabled) BOOL doubleTapToZoomIsEnabled; // default: yes

/**
 * The maximum scale of the image.
 *
 * By default this is 0, meaning the view will automatically determine the maximum scale.
 * Setting this to a non-zero value will override the automatically-calculated maximum scale.
 *
 */
@property (nonatomic, assign) CGFloat maximumScale; // default: 0 (autocalculate)

/**
 * The photo scroll view delegate.
 *
 */
@property (nonatomic, weak) id<UXPhotoScrollViewDelegate> photoScrollViewDelegate;

#pragma mark State
/**
 * The currently-displayed photo.
 *
 */
- (UIImage *)image;

/**
 * The current size of the photo.
 *
 * This is used to replace the photo only with successively higher-quality versions.
 *
 */
- (UXPhotoScrollViewPhotoSize)photoSize;


/**
 * Set a new photo with a specific size.
 *
 * If image is nil then the photoSize will be overridden as UXPhotoScrollViewPhotoSizeUnknown.
 *
 * Resets the current zoom levels and zooms to fit the image.
 *
 */
- (void)setImage:(UIImage *)image photoSize:(UXPhotoScrollViewPhotoSize)photoSize animate:(BOOL)animate;

@property (nonatomic, assign, getter = isLoading) BOOL loading;

/**
 * The index of this photo within a photo album.
 *
 */
@property (nonatomic, assign) NSInteger pageIndex;

/**
 * The largest dimensions of the photo.
 *
 * This is used to show the thumbnail at the final image size in case the final image size
 * is smaller than the album's frame. Without this value we have to assume that the thumbnail
 * will take up the full screen. If the final image doesn't take up the full screen, then
 * the photo view will appear to "snap" to the smaller full-size image when the final image
 * does load.
 *
 * CGSizeZero is used to signify an unknown final photo dimension.
 *
 */
@property (nonatomic, assign) CGSize photoDimensions;

/**
 * The gesture recognizer for double-tapping zooms in and out of the image.
 *
 * This is used mainly for setting up dependencies between gesture recognizers.
 *
 */
@property (nonatomic, readonly, strong) UITapGestureRecognizer* doubleTapGestureRecognizer;

@end
