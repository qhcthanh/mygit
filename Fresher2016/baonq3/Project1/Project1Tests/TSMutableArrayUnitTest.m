//
//  TSMutableArrayUnitTest.m
//  Project1Tests
//
//  Created by BaoNQ on 5/13/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TSMutableArray.h"

@interface Project1Tests : XCTestCase

@end

@implementation Project1Tests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

/**
 *  Bộ test khởi tạo NSMutableArray và thêm 100.000 phần tử vào bằng hàm addObject sử dụng Concurrent Queue
 *  Kết quả mong muốn là bộ test bị crash.
 */
- (void)testMultiWriteCrashWithNSMutableArray {
    NSMutableArray* mutableArray = [[NSMutableArray alloc] init];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_apply(100000, queue, ^(size_t index){
        [mutableArray addObject:[NSNumber numberWithInt:index]];
    });
}

/**
 *  Bộ test khởi tạo ThreadSafeMutableArray và thêm 100.000 phần tử vào bằng hàm addObject sử dụng Concurrent Queue
 *  Kết quả mong muốn là mảng được khởi tạo thành công và bộ test không bị crash.
 */
- (void)testMultiWriteWithThreadSafeMutableArray {
    TSMutableArray* mutableArray = [[TSMutableArray alloc] init];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_apply(100000, queue, ^(size_t index){
        [mutableArray addObject:[NSNumber numberWithInt:index]];
    });
}

@end
