//
//  WordSplitting.swift
//  NVDTelex
//
//  Created by VanDao on 8/24/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

import Foundation

public class WordSplitting: NSObject {
    
    // MARK: - Readonly property
    private(set) public var beginConsonant: String! = ""
    private(set) public var vowel: String! = ""
    private(set) public var endConsonant: String! = ""
    private(set) public var beginConsonantMark: LetterMarkType = LetterMarkType.None // for d only
    private(set) public var toneMark: ToneMarkType = ToneMarkType.None
    private(set) public var vowelMark: LetterMarkType =  LetterMarkType.None
    private(set) public var leftOver: String! = ""
    private(set) public var rawWord: String?

    // MARK: - Private property
    private var vowelType: VowelType = VowelType.None
    private var beginConsonantType: BeginConsonantType = BeginConsonantType.None
    private var endConsonantType: EndConsonantType = EndConsonantType.None
    
    // MARK: - Public method
    
    /**
     Initialized `WordSplitting` object with specifier raw word and typing type
     
     - parameter _rawWord:   The origin word before convert to vietnamese word, example: thuwong, yeeu, thich1..
     - parameter typingType: The type that define what current style typing is using (VNI or Telex)
     
     - returns: The initialization `WordSplitting` object
     */
    init(_rawWord: String, typingType: TypingType) {
        super.init()
        
        rawWord = _rawWord
        separateWord(_rawWord, typingType: typingType)
    }
    
    /**
     Check if the `wordParts` object is available to convert to vietnamese word
     
     - parameter word: The `wordParts` object that want to check
     
     - returns: YES if `wordPart` object is a vietnamese word, otherwise, return NO
     */
    func isAvailableToConvert() -> Bool {
        
        if  //(vowels[vowelType]![AvailablePart.EndConsonant.rawValue]!.contains(endConsonant.lowercaseString)) &&
           // leftOver == "" &&
            (beginConsonants[beginConsonantType]![AvailablePart.Vowel.rawValue]!.contains(vowel.lowercaseString)) &&
                (endConsonantType == EndConsonantType.None || endConsonants[endConsonantType]![AvailablePart.ToneMark.rawValue]!.contains(toneMark.rawValue))
        {
            return true
        }
        
        return false
    }
    
    func count() -> Int {
        
        var count = String((beginConsonant + vowel + endConsonant + leftOver)).characters.count
        if (vowelMark != LetterMarkType.None) {
            count += 1
        }
        if (toneMark != ToneMarkType.None) {
            count += 1
        }
        
        return count
    }
    
    /**
     Convert a word to vietnamese word with specifier raw word and typing type
     
     - parameter rawString:  The original word, example: 'nguyenex', 'nguye6n4', 'nguyeexn'...
     - parameter typingType: The current typing (VNI or Telex)
     
     - returns: The new word afther convert to vietnamese, if the word is not vietnamese word, return rawWord
     
     - Note: You don't need to use isVietnameseWord method before use this method, because this method used that method internal code
     */
    func getVietnameseWord() -> String? {
        
        var resString: String? = rawWord
        
        if (isAvailableToConvert()) {
            var _beginConsonant = beginConsonant
            var _vowel = vowel
            let _endConsonant = endConsonant
            let _leftOver = leftOver
            
            // begin consonant mark
            if beginConsonantMark == LetterMarkType.Stroke && _beginConsonant.lowercaseString == "d" {
                _beginConsonant = wordTransformTable[LetterMarkType.Stroke.rawValue]![beginConsonant]
            }
            
            // get chars need to adding mark
            if (vowelType != VowelType.None && (vowelMark != LetterMarkType.None || toneMark != ToneMarkType.None)) {
                
                var charAddingMark = getCharactersNeedToAddMark()
                
                // vowel mark
                if (vowelMark != LetterMarkType.None &&
                    letterMarks[vowelMark]![AvailablePart.Vowel.rawValue]!.contains(vowel.lowercaseString)) {
                    
                    if _vowel.lowercaseString.containsString("uo") &&
                        (_vowel.lowercaseString != "uo" || (_vowel.lowercaseString == "uo" && endConsonantType != EndConsonantType.None)) &&
                        vowelMark == LetterMarkType.Breve {
                        
                        let range = (charAddingMark.range.startIndex.advancedBy(-1)..<charAddingMark.range.startIndex)
                        _vowel.replaceRange(range, with: wordTransformTable[LetterMarkType.Breve.rawValue]![_vowel.substringWithRange(range)]!)
                    }
                    
                    charAddingMark.characters = wordTransformTable[vowelMark.rawValue]![charAddingMark.characters]!
                    _vowel.replaceRange(charAddingMark.range, with: charAddingMark.characters)
                }
                
                // tone mark
                if toneMark != ToneMarkType.None {
                    
                    charAddingMark.characters = wordTransformTable[toneMark.rawValue]![charAddingMark.characters]!
                    _vowel.replaceRange(charAddingMark.range, with: charAddingMark.characters)
                }
            }
            
            resString = _beginConsonant + _vowel + _endConsonant + _leftOver
        }
        
        return resString
    }
    
    func deleteCharacters(numberOfCharacter: Int, typingType: TypingType) {
        for _ in (0..<numberOfCharacter) {
            deleteCharacter()
        }
        generateRawWord(typingType)
    }
    
    func generateRawWord(typingType: TypingType) {
        rawWord = ""
        rawWord = rawWord! + beginConsonant
        if (beginConsonantMark != LetterMarkType.None) {
            rawWord = rawWord! + letterMarks[beginConsonantMark]![typingType.rawValue]!.first!
        }
        rawWord = rawWord! + vowel
        if (vowelMark != LetterMarkType.None) {
            rawWord = rawWord! + letterMarks[vowelMark]![typingType.rawValue]!.first!
        }
        if (toneMark != ToneMarkType.None) {
            rawWord = rawWord! + toneMarks[toneMark]![typingType.rawValue]!.first!
        }
        
        rawWord = rawWord! + endConsonant + leftOver
    }

    // MARK: - Private method
    /**
     Detect and return a letter need to add tone mark or vowel mark, and letter's range in vowel of word
     
     - parameter word: The `wordParts` object
     
     - returns: The letter need to add mark and its range in the vowel of the word
     
     - Note: If words with any vowel like "loe", "loet", "khuya"... you need to detect what letter need to add mark. So, you need to use this method. It will return ecxactly letter and its range
     
     */
    private func getCharactersNeedToAddMark() -> (characters: String, range: Range<String.Index>) {
        
        var characters: String!
        var range: Range<String.Index>!
        
        if endConsonantType != EndConsonantType.None ||
            (endConsonantType == EndConsonantType.None && (!vowels[vowelType]![AvailablePart.EndConsonant.rawValue]!.contains(EndConsonantType.None.rawValue) || vowel.lowercaseString == "uo" || vowel.lowercaseString == "oa")){
            characters = vowel.substringWithRange((vowel.endIndex.advancedBy(-1)..<vowel.endIndex))
            range = vowel.rangeOfString(characters)
        } else {
            
            let count = vowel.characters.count
            characters = vowel.substringWithRange((vowel.startIndex.advancedBy((count - 1)/2)..<vowel.startIndex.advancedBy((count - 1)/2 + 1)))
            range = vowel.rangeOfString(characters)
        }
        
        return (characters, range)
    }
    
    /**
     Convert a vietnamese word to a string that can be revert to vietnamese word with specifier typing type
     
     - parameter vietnamese: The vietnamese word
     - parameter tyingType:  The type of current typing
     
     - returns: The string that can be revert to vietnamese word
     */
    private func convertVietnameseToOriginWord(vietnamese: String, tyingType: TypingType) -> String {
        
        var originWord = ""
        
        for character in vietnamese.characters {
            
            var chars = String(character)
            if let key = VNsTransformTable[chars.lowercaseString] {
                let isCapitalizedWord: Bool = chars.isCapitalizedWord()
                chars = key[tyingType.rawValue]!
                chars = isCapitalizedWord ? chars.uppercaseString : chars
            }
            
            originWord = originWord + chars
        }
        
        return originWord
    }
    
    /**
     Separate a word to `wordParts` object
     
     - parameter word:       A word that need to separate
     - parameter typingType: The current typing (VNI or Telext)
     */
    private func separateWord(word: String, typingType: TypingType) {
        
        var _hadBeginConsonant: Bool! = false
        var _hadVowel: Bool! = false
        var _hadEndConsonant: Bool! = false
        var _hadStrokeMark: Bool! = false
        let originWord = convertVietnameseToOriginWord(word, tyingType: typingType)
        
        eachChar: for character in originWord.characters {
            
            // Get begin consonant as long as possible
            if (!_hadBeginConsonant) {
                
                let temp = beginConsonant + String(character)
                
                for type in beginConsonantTypes {
                    if (beginConsonants[type]![list]!.contains(temp.lowercaseString)) {
                        beginConsonant = temp
                        beginConsonantType = type
                        continue eachChar
                    }
                }
                _hadBeginConsonant = true
            }
            
            // Get stroke mark if not exists and the begin consonant is letter 'd'
            if (!_hadStrokeMark) {
                
                if (beginConsonant.lowercaseString != "d") {
                    _hadStrokeMark = true
                } else if letterMarks[LetterMarkType.Stroke]![typingType.rawValue]!.contains(String(character).lowercaseString) && beginConsonant.lowercaseString == "d" {
                    
                    if (beginConsonantMark != LetterMarkType.Stroke) {
                        beginConsonantMark = LetterMarkType.Stroke
                    } else {
                        beginConsonantMark = LetterMarkType.None
                        leftOver = leftOver + String(character)
                        _hadStrokeMark = true
                    }
                    
                    continue eachChar
                }
                
            }
            
            // Get vowel as long as possible
            if (!_hadVowel) {
                let temp = vowel + String(character)
                
                for type in vowelTypes {
                    if (vowels[type]![list]!.contains(temp.lowercaseString) && letterMarks[vowelMark]![AvailablePart.Vowel.rawValue]!.contains(temp.lowercaseString)) {
                        vowel = temp
                        vowelType = type
                        continue eachChar
                    }
                }
            }
            
            // Get lastest tone mark
            if (leftOver == "") {
                for type in toneMarkTypes {
                    
                    if toneMarks[type]![typingType.rawValue]!.contains(String(character).lowercaseString) && (vowelType != VowelType.None || beginConsonantType == BeginConsonantType.Five ) {
                        
                        if toneMark != type {
                            toneMark = type
                        } else {
                            toneMark = ToneMarkType.None
                            leftOver = leftOver + String(character)
                            _hadEndConsonant = true
                        }
                        
                        continue eachChar
                    }
                }
            }
            
            // Get lastest vowel mark
            if (leftOver == "") {
                for type in letterMarkTypes {
                    
                    if type != LetterMarkType.Stroke && letterMarks[type]![typingType.rawValue]!.contains(String(character).lowercaseString) &&
                        letterMarks[type]![AvailablePart.Vowel.rawValue]!.contains(vowel.lowercaseString){
                        
                        if vowelMark != type {
                            vowelMark = type
                        } else {
                            vowelMark = LetterMarkType.None
                            leftOver = leftOver + String(character)
                            _hadEndConsonant = true
                        }
                        continue eachChar
                    }
                }
            }
            
            // Get end consonant as long as possible
            if (!_hadEndConsonant && leftOver == "") {
                let temp = endConsonant + String(character)
                _hadVowel = true
                
                for type in endConsonantTypes {
                    if (endConsonants[type]![list]!.contains(temp.lowercaseString)) {
                        endConsonant = temp
                        endConsonantType = type
                        continue eachChar
                    }
                }
                
                _hadEndConsonant = true
            }
            leftOver = leftOver + String(character)
        }
        
        // Check if begin consonant is 'gi' and have not vowel, so we need to separate begin consonant to letter 'g', and vowel is letter 'i'
        if (beginConsonantType == BeginConsonantType.Five && vowelType == VowelType.None) {
            vowel = beginConsonant.substringFromIndex(beginConsonant.startIndex.advancedBy(1))
            beginConsonant = beginConsonant.substringToIndex(beginConsonant.startIndex.advancedBy(1))
            beginConsonantType = BeginConsonantType.One
            vowelType = VowelType.Two
        }
    }
    
    private func deleteCharacter() {
        
        // delete left over
        if (leftOver != "") {
            leftOver =  leftOver.substringToIndex(leftOver.characters.count - 1)
            
            return
        }
        
        // delete sign
        if (toneMark != ToneMarkType.None) {
            toneMark = ToneMarkType.None
            
            return
        }
        
        // delete end consonant
        if (endConsonant != "") {
            endConsonant = endConsonant.substringToIndex(endConsonant.characters.count - 1)
            
            endConsonantType = EndConsonantType.None
            for type in endConsonantTypes {
                if (endConsonants[type]![list]!.contains(endConsonant.lowercaseString)) {
                    endConsonantType = type
                    break
                }
            }
            
            return
        }
        
        // delete mark
        if (vowelMark != LetterMarkType.None) {
            vowelMark = LetterMarkType.None
            
            return
        }
        
        // delete vowel
        if (vowel != "") {
            vowel = vowel.substringToIndex(vowel.characters.count - 1)
            
            vowelType = VowelType.None
            for type in vowelTypes {
                if (vowels[type]![list]!.contains(vowel.lowercaseString)) {
                    vowelType = type
                    break
                }
            }
            
            if (!(vowelMark != LetterMarkType.None && letterMarks[vowelMark]![AvailablePart.Vowel.rawValue]!.contains(vowel))) {
                vowelMark = LetterMarkType.None
            }
            
            return
        }
        
        // delete begin consonant
        if (beginConsonant != "") {
            beginConsonant = beginConsonant.substringToIndex(beginConsonant.characters.count - 1)
            
            beginConsonantType = BeginConsonantType.None
            for type in beginConsonantTypes {
                if (beginConsonants[type]![list]!.contains(vowel.lowercaseString)) {
                    beginConsonantType = type
                    break
                }
            }
            
            return
        }
    }
    
    deinit {
        beginConsonant.removeAll()
        vowel.removeAll()
        endConsonant.removeAll()
        leftOver.removeAll()
        rawWord?.removeAll()        
    }
}