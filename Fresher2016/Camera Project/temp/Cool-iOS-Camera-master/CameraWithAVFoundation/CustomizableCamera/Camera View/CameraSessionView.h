//
//  CACameraSessionDelegate.h
//
//  Created by Christopher Cohen & Gabriel Alvarado on 1/23/15.
//  Copyright (c) 2015 Gabriel Alvarado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
//Custom UI classes
#import "CameraShutterButton.h"
#import "CameraToggleButton.h"
#import "CameraFlashButton.h"
#import "CameraDismissButton.h"
#import "CameraFocalReticule.h"
#import "CaptureSessionManager.h"
#import <ImageIO/ImageIO.h>


#define UISCREEN_BOUND_SIZE_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define  UISCREEN_BOUND_SIZE_WIDTH

typedef NS_ENUM(NSInteger, CameraDefaultButton) {
  //  CameraDefaultButtonShutterButton = ShutterButtonTag,
    CameraDefaultButtonToggleButton = ToggleButtonTag,
    CameraDefaultButtonFlashButton,
    CameraDefaultButtonDismissButton,
};

///Protocol Definition
@protocol CACameraSessionDelegate <NSObject>

@optional - (void)didCaptureImage:( UIImage * _Nullable )image;
@optional - (void)didCaptureImageWithData:(NSData * _Nullable)imageData;
@optional - (void)didStartRecordingVideo:(NSURL * _Nullable)outputFileURL;
@optional - (void)didFinishRecordingVideo:(NSURL *_Nullable )outputFileURL error:(NSError * _Nullable)error cleanUp:(dispatch_block_t _Nonnull)cleanUpBlock;
@end

@interface CameraSessionView : UIView {
    //Size of the UI elements variables
    CGSize shutterButtonSize;
    CGSize topBarSize;
    CGSize bottomBarSize;
    CGSize barButtonItemSize;
    
    //Position of the UI element default TopLeft, BottomLeft, BottomRight
    CGPoint topLeftPosition;
    CGPoint bottomLeftPosition;
    CGPoint bottomRightPosition;
    
    //Variable vith the current camera being used (Rear/Front)
    CameraType cameraBeingUsed;
    UIColor *defaultBarColor;
    
    BOOL animationInProgress;
    
    CaptureSessionManager *captureManager;
    CameraFocalReticule *focalReticule;
}

//Delegate Property
@property (nullable, nonatomic, weak) id  <CACameraSessionDelegate> delegate;


//API Functions
- (void)onTapShutterButton;
- (void)onTapFlashButton;
- (void)onTapToggleButton;
- (void)onTapDismissButton;
- (void)startRecording;
- (void)stopRecording;
- (BOOL)isRecording;
- (void)zoomPreview:(CGFloat)scale;
- (CGFloat)currentScale;

-(void)cameraSessionManagerDidCaptureImage;
-(void)cameraSessionManagerFailedToCaptureImage;
-(void)cameraSessionManagerDidReportAvailability:(BOOL)deviceAvailability forCameraType:(CameraType)cameraType;
-(void)cameraSessionManagerDidReportDeviceStatistics:(CameraStatistics)deviceStatistics;
-(void)cameraSessionMaagerDidStartRecording:(NSURL * _Nullable)outputFileURL;
-(void)cameraSessionMaagerDidFinsishRecording:(NSURL * _Nullable)outputFileURL error:(NSError * _Nullable)error cleanUp:(dispatch_block_t _Nullable)cleanUpBlock;

- (void)animateFocusReticuleToPoint:(CGPoint)targetPoint;
- (void)focusAtPoint:(CGPoint)point completionHandler:(void(^ _Nullable)())completionHandler;

@end
