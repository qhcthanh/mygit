//
//  BLMultiWord.swift
//  KeyboardDatabaseHaha
//
//  Created by VanDao on 8/31/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

import Foundation

class BLMultiWord : BLObject {
    
    var firstWord: String! = ""
    var secondWord: String! = ""
    var score = 0

    /**
     Initialzed a new `BLMultiWord` object with specifier dictionary
     
     - parameter dictionary: The dictionary that contain value of property of object
     
     - returns: The initialization `BLMultiWord` object
     */
    override init(dictionary: Dictionary<String, AnyObject>) {

        firstWord = (dictionary["firstWord"] ?? "") as! String
        secondWord = (dictionary["secondWord"] ?? "") as! String
        score = Int((dictionary["score"] ?? 0) as! String)!
        
        super.init()
    }
    
    /**
     Initialized a new `BLMuliWord` object with specifier pharse string and score
     
     - parameter phrase: The pharse string of object
     - parameter _score: The score of object
     
     - returns: The initialization `BLMuliWord` object
     */
    init?(phrase: String, _score: Int) {
        super.init()
        
        let splitWord = phrase.componentsSeparatedByString(" ")
        if (splitWord.count > 1) {
            firstWord = splitWord[0]
            secondWord = splitWord[1]
            score = _score
        } else {
            return nil
        }
    }
    
    /**
     Create and return the create table sql command of model
     
     - returns: The create table sql command
     */
    override class func getSQLCommandForCreateTable() -> String {
        let sqlString: String! = "CREATE VIRTUAL TABLE IF NOT EXISTS \(String(BLMultiWord)) USING fts4(firstWord, secondWord, score);";

        
        return sqlString
    }
}
