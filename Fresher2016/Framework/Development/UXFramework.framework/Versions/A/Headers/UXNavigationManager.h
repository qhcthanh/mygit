/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXNavigationObject.h"

UIViewController* openURL(NSString* URL);

#define kHideNavigationBarNotification @"kHideNavigationBarNotification"
#define kRemoveViewControllerNotification @"kRemoveViewControllerNotification"

@interface UXNavigationManager : UXNavigationObject

+ (UXNavigationManager*)navigator;


/**
 *  Dismiss all alert views or popup views
 */
+ (void)dimissAllPopupViews;

/**
 *  Back to root view controller
 *
 *  @param completeHandler The callback after pop/dismiss all view controllers
 */
+ (void)backToRootViewControllerWithCompletionHandler:(void (^)(void))completeHandler;

+ (void)backToViewController:(UIViewController*)controller withCompletionHandler:(void (^)(void))completeHandler;
    
/**
 *  Open an URL string with a source indentify
 *
 *  @param URLString An url string that contains prefix http/https or ux://
 *  @param sourceId  A source index
 */
+ (void)openURL:(NSString*)URLString withSourceId:(NSInteger)sourceId;

/**
 * Reloads the content in the visible view controller.
 */
- (void)reload;

@end
