//
//  ViewController.m
//  CaptureUIElement
//
//  Created by manhduydl on 8/2/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "ViewController.h"
#import <AssetsLibrary/ALAssetsLibrary.h>
#import "UXOverlayView.h"
#import "UXImageTransitionView.h"
#import "UXImageTransitionDetail.h"

#define kWidth [[UIScreen mainScreen] bounds].size.width
#define kHeight [[UIScreen mainScreen] bounds].size.height

@interface ViewController () <UXOverlayViewDelegate> {
    GPUImageView *previewCamera;
    GPUImageVideoCamera *camera;
    GPUImageMovieWriter *movieWriter;
    GPUImageUIElement *uiElementInput;
    GPUImageOutput<GPUImageInput> *filter;
    UXOverlayView *stickerOverlayView;
    
    BOOL isRecording;
    NSURL *movieURL;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    isRecording = NO;
    filter = [[GPUImageSepiaFilter alloc] init];
    [(GPUImageSepiaFilter *)filter setIntensity:0.0];
    previewCamera = [[GPUImageView alloc] initWithFrame:self.view.frame];
    [self.view insertSubview:previewCamera atIndex:0];
    
    camera = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPreset640x480 cameraPosition:AVCaptureDevicePositionBack];
    camera.outputImageOrientation = UIInterfaceOrientationPortrait;
    
    GPUImageAlphaBlendFilter *blendFilter = [[GPUImageAlphaBlendFilter alloc] init];
    blendFilter.mix = 1.0;
    
    stickerOverlayView = [[UXOverlayView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    //[self.view addSubview:stickerOverlayView];
    //[self.view bringSubviewToFront:stickerOverlayView];
    stickerOverlayView.touchDelegate = self;
    stickerOverlayView.userInteractionEnabled = YES;
    stickerOverlayView.nextView = nil;
    
    uiElementInput = [[GPUImageUIElement alloc] initWithView:stickerOverlayView];
    
    [camera addTarget:filter];
    [filter addTarget:blendFilter];
    [uiElementInput addTarget:blendFilter];
    
    [blendFilter addTarget:previewCamera];
    
    __unsafe_unretained GPUImageUIElement *weakUIElementInput = uiElementInput;
    
    [filter setFrameProcessingCompletionBlock:^(GPUImageOutput * filter, CMTime frameTime){
        [weakUIElementInput update];
    }];

    
    NSString *pathToMovie = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Movie.m4v"];
    unlink([pathToMovie UTF8String]); // If a file already exists, AVAssetWriter won't let you record new frames, so delete the old movie
    movieURL = [NSURL fileURLWithPath:pathToMovie];
    movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:movieURL size:CGSizeMake(480.0, 640.0)];
    movieWriter.encodingLiveVideo = YES;
  
    [blendFilter addTarget:movieWriter];
    [camera startCameraCapture];
}
- (IBAction)addStickerButtonClick:(id)sender {
    UIImage *image = [UIImage imageNamed:@"sticker.png"];
    CGPoint touchPosition =  CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    UXImageTransitionView *newSticker = [[UXImageTransitionView alloc] initWithImage:image atCenterPosition:touchPosition];
    [stickerOverlayView addSticker:[[UXImageTransitionDetail alloc] initWithTransitionView:newSticker]];
}
- (IBAction)clearStickerButtonClick:(id)sender {
    [stickerOverlayView removeSticker:[stickerOverlayView.stickerList lastObject]];
}

- (IBAction)recordButtonClick:(id)sender {
    if (isRecording) {
        [self stopRecord];
    } else {
        [self startRecord];
    }
}

- (void)startRecord {
    isRecording = YES;
    camera.audioEncodingTarget = movieWriter;
    [movieWriter startRecording];
    
}

- (void)stopRecord {
    isRecording = NO;
    [filter removeTarget:movieWriter];
    camera.audioEncodingTarget = nil;
    [movieWriter finishRecording];
    NSLog(@"Movie completed");
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:movieURL])
    {
        [library writeVideoAtPathToSavedPhotosAlbum:movieURL completionBlock:^(NSURL *assetURL, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 if (error) {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Video Saving Failed"
                                                                    delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [alert show];
                 } else {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video Saved" message:@"Saved To Photo Album"
                                                                    delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [alert show];
                 }
             });
         }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -
#pragma mark - touch handler
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    for (UITouch *touch in [touches allObjects])
    {
        // add new touch to list
        [stickerOverlayView.touchList addObject:touch];
        [stickerOverlayView.beginPointList addObject:[NSValue valueWithCGPoint:[touch locationInView:self.view]]];
        [stickerOverlayView.movePointList addObject:[NSValue valueWithCGPoint:[touch locationInView:self.view]]];
        
        CGPoint touchPosition = [[stickerOverlayView.movePointList objectAtIndex:0] CGPointValue];
        
        // check if touch down a `UXTransitionView`
        if (!stickerOverlayView.focusTranstionView){
            for (id sticker in stickerOverlayView.stickerList){
                UXTransitionDetail *curSticker = sticker;
                if ([stickerOverlayView checkTransitionView:curSticker.transitionView containsPoint:touchPosition]){
                    stickerOverlayView.focusTranstionView = sticker;
                    
                    id firstSticker = stickerOverlayView.focusTranstionView;
                    [stickerOverlayView.stickerList removeObject:stickerOverlayView.focusTranstionView];
                    [stickerOverlayView.stickerList insertObject:firstSticker atIndex:0];
                    
                    [stickerOverlayView bringSubviewToFront:curSticker.transitionView];
                    
                    stickerOverlayView.movedSticker = NO;
                    
                    break;
                }
            }
        }
    }
    
    // Update current focus sticker
    if (!stickerOverlayView.focusTranstionView && [stickerOverlayView.beginPointList count] > 1){
        CGPoint touch1Position = [[stickerOverlayView.beginPointList objectAtIndex:0] CGPointValue];
        CGPoint touch2Position = [[stickerOverlayView.beginPointList objectAtIndex:1] CGPointValue];
        
        for (id sticker in stickerOverlayView.stickerList){
            UXTransitionDetail *curSticker = sticker;
            
            if ([stickerOverlayView checkTransitionView:curSticker.transitionView intersectLineSegment:touch1Position :touch2Position]){
                stickerOverlayView.focusTranstionView = sticker;
                
                id firstSticker = stickerOverlayView.focusTranstionView;
                [stickerOverlayView.stickerList removeObject:firstSticker];
                [stickerOverlayView.stickerList insertObject:stickerOverlayView.focusTranstionView atIndex:0];
                
                [stickerOverlayView bringSubviewToFront:curSticker.transitionView];
                break;
            }
        }
    }
    
//    // Check if having sticker focussed
//    if (focusTranstionView){
//        [self.nextView disableReceiveResponder];
//    }
//    
//    if (![self.nextView isDisabledResponder])
//        [self.nextView touchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    // Update move point list
    for (UITouch *touch in [touches allObjects])
    {
        NSInteger index = [stickerOverlayView.touchList indexOfObject:touch];
        [stickerOverlayView.movePointList replaceObjectAtIndex:index withObject:[NSValue valueWithCGPoint:[touch locationInView:self.view]]];
    }
    
    // Check if having sticker focussed
    if (stickerOverlayView.focusTranstionView){
        stickerOverlayView.movedSticker = YES;
//        if (self.touchDelegate && [self.touchDelegate respondsToSelector:@selector(didTouchMoveSticker:position:)])
//            [self.touchDelegate didTouchMoveSticker:focusTranstionView position:[[movePointList objectAtIndex:0] CGPointValue]];
        
        [stickerOverlayView transformSticker:stickerOverlayView.focusTranstionView];
    }
    
    // Update begin point list
    for (NSInteger index = 0; index < [stickerOverlayView.movePointList count]; index++){
        [stickerOverlayView.beginPointList replaceObjectAtIndex:index withObject:[stickerOverlayView.movePointList objectAtIndex:index]];
    }
    
//    if (![self.nextView isDisabledResponder])
//        [self.nextView touchesMoved:touches withEvent:event];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
//    // Check if touch up inside UXTextTransitionView
//    if ([focusTranstionView isKindOfClass:[UXTextTransitionDetail class]] && !movedSticker){
//        if (self.touchDelegate && [self.touchDelegate respondsToSelector:@selector(didTouchUpInsideTextSticker:)])
//            [self.touchDelegate didTouchUpInsideTextSticker:focusTranstionView];
//    }
//    
//    // Check if pan UXTextTransitionView
//    if (focusTranstionView && movedSticker) {
//        for (UITouch *touch in touches) {
//            if ([touchList indexOfObject:touch] == 0){
//                if ( self.touchDelegate && [self.touchDelegate respondsToSelector:@selector(didTouchEndSticker:position:)]) {
//                    [self.touchDelegate didTouchEndSticker:focusTranstionView position:[[movePointList objectAtIndex:0] CGPointValue]];
//                    break;
//                }
//            }
//        }
//    }
    
    // Remove ended point from list
    for (UITouch *touch in [touches allObjects]) {
        NSInteger index = [stickerOverlayView.touchList indexOfObject:touch];
        
        [stickerOverlayView.touchList removeObjectAtIndex:index];
        [stickerOverlayView.beginPointList removeObjectAtIndex:index];
        [stickerOverlayView.movePointList removeObjectAtIndex:index];
    }
    
    if ([stickerOverlayView.beginPointList count] == 0)
        stickerOverlayView.focusTranstionView = nil;
    
//    if (![self.nextView isDisabledResponder])
//        [self.nextView touchesEnded:touches withEvent:event];
//    
//    if (!focusTranstionView && [self.nextView isDisabledResponder]){
//        [self.nextView enableReceiveResponder];
//    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    // Remove cancelled point from list
    for (UITouch *touch in [touches allObjects]) {
        NSInteger index = [stickerOverlayView.touchList indexOfObject:touch];
        
        [stickerOverlayView.touchList removeObjectAtIndex:index];
        [stickerOverlayView.beginPointList removeObjectAtIndex:index];
        [stickerOverlayView.movePointList removeObjectAtIndex:index];
    }
    
    if ([stickerOverlayView.beginPointList count] == 0)
        stickerOverlayView.focusTranstionView = nil;
    
//    if (![self.nextView isDisabledResponder])
//        [self.nextView touchesCancelled:touches withEvent:event];
//    
//    if (!focusTranstionView && [self.nextView isDisabledResponder]){
//        [self.nextView enableReceiveResponder];
//    }
}


@end
