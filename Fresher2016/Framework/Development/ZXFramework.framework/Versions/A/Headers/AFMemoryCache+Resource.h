/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "AFMemoryCache.h"

@interface AFMemoryCache(Bundle)

+ (BOOL)isBundleURL:(NSString*)URL;
+ (BOOL)isDocumentURL:(NSString*)URL;
+ (NSString*)doubleImageURLPath:(NSString*)urlPath;
+ (NSString*)getPathForBundleResource:(NSString*)relativePath;
+ (NSString*)getPathForDocumentResource:(NSString*)relativePath;

@end
