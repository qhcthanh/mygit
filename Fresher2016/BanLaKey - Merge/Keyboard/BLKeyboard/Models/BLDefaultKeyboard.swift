//
//  BLDefaultKeyboard.swift
//  BanLaKey
//
//  Created by BaoNQ on 9/5/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import UIKit

func defaultKeyboard(frame: CGRect, inputViewController: UIInputViewController) -> BLKeyboard {
    let defaultKeyboard = BLKeyboard(frame: frame, inputViewController: inputViewController)
    
    var screenWidth: CGFloat = frame.size.width
    var screenHeight: CGFloat = frame.size.height
    
    var keyboardMetrics: KeyboardMetrics
    
    #if isIPad
        keyboardMetrics = generatePadKeyboardMetrics(screenWidth, screenHeight)
    #else
        keyboardMetrics = generatePhoneKeyboardMetrics(screenWidth, screenHeight)
    #endif
    
    func generateKey(titles: [String], buttonColors: [UIColor!], extendedCharacters: [[String]], row: Int, page: Int) {
        
        var index: Int = 0
        
        for key in titles {
            let keyModel: BLKey
            if extendedCharacters[index].count == 0 {
                keyModel = BLKey.init(frame: CGRectZero, title: key, originalPopupFrame: defaultKeyboard.keyboardLayout.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: buttonColors[index])
            }
            else {
                keyModel = BLKey.init(frame: CGRectZero, title: key, extendedCharacters: extendedCharacters[index], originalPopupFrame: defaultKeyboard.keyboardLayout.popupFrame, keyType: BLKeyType.CharacterKey, backgroundColor: buttonColors[index])
            }
            defaultKeyboard.addKey(keyModel, row: row, page: page)
            
            keyModel.addTarget(defaultKeyboard, action: #selector(defaultKeyboard.showPopup(_:)), forControlEvents: [.TouchDown, .TouchDragInside, .TouchDragEnter])
            keyModel.addTarget(keyModel, action: #selector(keyModel.hidePopup), forControlEvents: [.TouchDragExit, .TouchCancel])
            keyModel.addTarget(defaultKeyboard, action: #selector(defaultKeyboard.hidePopupDelay(_:)), forControlEvents: [.TouchUpInside, .TouchUpOutside, .TouchDragOutside])
            
            keyModel.addTarget(defaultKeyboard, action: #selector(defaultKeyboard.characterButtonTapped(_:)), forControlEvents: .TouchUpInside)
            
            defaultKeyboard.addSubview(keyModel)
            
            index += 1
        }
    }
    
    // Page 1
    
    var buttonColors = [BLThemeManager.currentTheme.QButtonColor, BLThemeManager.currentTheme.WButton,
                        BLThemeManager.currentTheme.EButton, BLThemeManager.currentTheme.RButton,
                        BLThemeManager.currentTheme.TButton, BLThemeManager.currentTheme.YButton,
                        BLThemeManager.currentTheme.UButton, BLThemeManager.currentTheme.IButton,
                        BLThemeManager.currentTheme.OButton, BLThemeManager.currentTheme.PButton]
    
    var extendedCharacters = [[], [], [], [], [], [], [], [], [], []]
    
    generateKey(["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P"], buttonColors: buttonColors, extendedCharacters: extendedCharacters as! [[String]], row: 0, page: 0)
    
    
    buttonColors = [BLThemeManager.currentTheme.AButton, BLThemeManager.currentTheme.SButton,
                    BLThemeManager.currentTheme.DButton, BLThemeManager.currentTheme.FButton,
                    BLThemeManager.currentTheme.GButton, BLThemeManager.currentTheme.HButton,
                    BLThemeManager.currentTheme.JButton, BLThemeManager.currentTheme.KButton,
                    BLThemeManager.currentTheme.LButton]
    
    extendedCharacters = [["Â", "Ă", "Á", "À"], [], [], [], [], [], [], [], []]
    
    generateKey(["A", "S", "D", "F", "G", "H", "J", "K", "L"], buttonColors: buttonColors, extendedCharacters: extendedCharacters as! [[String]], row: 1, page: 0)
    
    
    buttonColors = [BLThemeManager.currentTheme.ZButton, BLThemeManager.currentTheme.XButton,
                    BLThemeManager.currentTheme.CButton, BLThemeManager.currentTheme.VButton,
                    BLThemeManager.currentTheme.BButton, BLThemeManager.currentTheme.NButton,
                    BLThemeManager.currentTheme.MButton]

    extendedCharacters = [[], [], [], [], [], [], []]

    generateKey(["Z", "X", "C", "V", "B", "N", "M"], buttonColors: buttonColors, extendedCharacters: extendedCharacters as! [[String]], row: 2, page: 0)

    
    // Page 2
    buttonColors = [BLThemeManager.currentTheme.QButtonColor, BLThemeManager.currentTheme.WButton,
                        BLThemeManager.currentTheme.EButton, BLThemeManager.currentTheme.RButton,
                        BLThemeManager.currentTheme.TButton, BLThemeManager.currentTheme.YButton,
                        BLThemeManager.currentTheme.UButton, BLThemeManager.currentTheme.IButton,
                        BLThemeManager.currentTheme.OButton, BLThemeManager.currentTheme.PButton]
    
    extendedCharacters = [[], [], [], [], [], [], [], [], [], []]
    
    generateKey(["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"], buttonColors: buttonColors, extendedCharacters: extendedCharacters as! [[String]], row: 0, page: 1)

    
    buttonColors = [BLThemeManager.currentTheme.QButtonColor, BLThemeManager.currentTheme.WButton,
                    BLThemeManager.currentTheme.EButton, BLThemeManager.currentTheme.RButton,
                    BLThemeManager.currentTheme.TButton, BLThemeManager.currentTheme.YButton,
                    BLThemeManager.currentTheme.UButton, BLThemeManager.currentTheme.IButton,
                    BLThemeManager.currentTheme.OButton, BLThemeManager.currentTheme.PButton]
    
    extendedCharacters = [[], [], [], [], [], [], [], [], [], []]
    
    generateKey(["-", "/", ":", ";", "(", ")", "$", "&", "@", "\""], buttonColors: buttonColors, extendedCharacters: extendedCharacters as! [[String]], row: 1, page: 1)
    
    
    
    buttonColors = [BLThemeManager.currentTheme.ZButton, BLThemeManager.currentTheme.XButton,
                    BLThemeManager.currentTheme.CButton, BLThemeManager.currentTheme.VButton,
                    BLThemeManager.currentTheme.BButton, BLThemeManager.currentTheme.NButton,
                    BLThemeManager.currentTheme.MButton]
    
    extendedCharacters = [[], [], [], [], [], [], []]
    
    generateKey([".", ",", "?", "!", "'", "℃", "℉"], buttonColors: buttonColors, extendedCharacters: extendedCharacters as! [[String]], row: 2, page: 1)
    
    return defaultKeyboard
}