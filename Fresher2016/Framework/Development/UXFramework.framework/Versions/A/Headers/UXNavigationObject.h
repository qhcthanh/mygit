/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXNavigationObjectDelegate.h"

typedef NS_ENUM(NSInteger, UXNavigationMode) {
    UXNavigationModeNone,
    UXNavigationModeCreate,		// a new view controller is created each time
    UXNavigationModeShare,		// a new view controller is created, cached and re-used
    UXNavigationModeModal,		// a new view controller is created and presented modally
    UXNavigationModePopover,	// a new view controller is created and presented in a popover
    UXNavigationModeExternal	// an external app will be opened
};

@class UXURLMap;
@class UXURLAction;
@class UXURLPattern;
@class UXURLNavigationPattern;
@interface UXNavigationObject : NSObject <UIPopoverControllerDelegate>  {
    UXURLMap*                   _URLMap;
    UIWindow*                   _window;
    BOOL                        _opensExternalURLs;
    __unsafe_unretained id<UXNavigationObjectDelegate> _delegate;
    UIViewController*           _rootViewController;
    UIPopoverController*        _popoverController;
}

/**
 * A modal controller is a view controller that is presented over another controller and hides
 * the original controller completely. Classic examples include the Safari login controller when
 * authenticating on a network, creating a new contact in Contacts, and the Camera controller.
 *
 * If the controller that is being presented is not a UINavigationController, then a
 * UINavigationController is created and the controller is pushed onto the navigation controller.
 * The navigation controller is then displayed instead.
 *
 */
- (void)presentModalController: (UIViewController*)controller
              parentController: (UIViewController*)parentController
                      animated: (BOOL)animated
                    transition: (NSInteger)transition;

- (void)presentPopoverController: (UIViewController*)controller
                    sourceButton: (UIBarButtonItem*)sourceButton
                      sourceView: (UIView*)sourceView
                      sourceRect: (CGRect)sourceRect
                        animated: (BOOL)animated;

/**
 * Removes all view controllers from the window and releases them.
 */
- (void)removeAllViewControllers;

/**
 * Load and display the view controller with a pattern that matches the URL.
 *
 * This method replaces all other openURL methods by using the chainable UXURLAction object.
 *
 * If there is not yet a rootViewController, the view controller loaded with this URL
 * will be assigned as the rootViewController and inserted into the keyWindow. If there is not
 * a keyWindow, a UIWindow will be created and displayed.
 *
 * Example UXURLAction initialization:
 * [[UXURLAction actionWithURLPath:@"tt://some/path"]
 *                   applyAnimated:YES]
 *
 * Each apply* method on the UXURLAction object returns self, allowing you to chain methods
 * when initializing the object. This allows for a flexible method that requires a shifting set
 * of parameters that have specific defaults. The old openURL* methods are being phased out, so
 * please start using openURLAction instead.
 */
- (UIViewController*)initializeURLAction:(UXURLAction*)URLAction;
- (UIViewController*)openURLAction:(UXURLAction*)URLAction;

/**
 * Opens a sequence of URLs.
 *
 * @return The view controller of the last opened URL.
 */
- (UIViewController*)openURLs:(NSString*)URL,...;

/**
 * Gets a view controller for the URL without opening it.
 *
 * @return The view controller mapped to URL.
 */
- (UIViewController*)viewControllerForURL:(NSString*)URL;

/**
 * Gets a view controller for the URL without opening it.
 *
 * @return The view controller mapped to URL.
 */
- (UIViewController*)viewControllerForURL:(NSString*)URL query:(NSDictionary*)query;

/**
 * Gets a view controller for the URL without opening it.
 *
 * @return The view controller mapped to URL.
 */
- (UIViewController*)viewControllerForURL:(NSString*)URL query:(NSDictionary*)query
                                  pattern:(UXURLNavigationPattern**)pattern;
/**
 * Pop to root view controller
 */
- (void)popToRootViewController:(UIViewController*)controller completion:(void (^)(void))completeHandler;

- (void)popToViewController:(UIViewController*)controller fromController:(UIViewController*)currentController completion:(void (^)(void))completeHandler;
    
/**
 * The URL map used to translate between URLs and view controllers.
 *
 * @see UXURLMap
 */
@property (nonatomic, readonly) UXURLMap* URLMap;

/**
 * The window that contains the view controller hierarchy.
 *
 * By default retrieves the keyWindow.
 */
@property (nonatomic, retain) UIWindow* window;

/**
 * The controller that is at the root of the view controller hierarchy.
 */
@property (nonatomic, readonly) UIViewController* rootViewController;

/**
 * The currently visible view controller.
 */
@property (nonatomic, readonly) UIViewController* visibleViewController;

/**
 * The view controller that is currently on top of the navigation stack.
 *
 * This differs from visibleViewController in that it ignores things like search
 * display controllers which are visible, but not part of navigation.
 */
@property (nonatomic, readonly) UIViewController* topViewController;

/**
 *  The view controller that is currently on top of alert controller
 */
@property (nonatomic, weak) UIViewController *parrentAlertController;

/**
 * Allows URLs to be opened externally if they don't match any patterns.
 *
 * @default NO
 */
@property (nonatomic) BOOL opensExternalURLs;

@property (nonatomic, assign) BOOL isBeingTransition;

@property (nonatomic, assign) id<UXNavigationObjectDelegate> delegate;

@end
