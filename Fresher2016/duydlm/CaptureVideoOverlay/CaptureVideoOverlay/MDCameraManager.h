//
//  MDCameraManager.h
//  CaptureVideoOverlay
//
//  Created by manhduydl on 7/30/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GPUImage.h"
#import "MDFrameDrawer.h"

@interface MDCameraManager : NSObject

@property (nonatomic, assign) BOOL shouldUseCaptureTime;
@property (nonatomic, assign) BOOL shouldCaptureAudio;

@property (nonatomic, readonly) BOOL isCapturing;
@property (nonatomic, readonly) BOOL isRecording;
@property (nonatomic, readonly) BOOL isPaused;

@property (nonatomic, readonly) GPUImageTwoInputFilter *composer;
@property (nonatomic, readonly) MDFrameDrawer *overlay;

@property (nonatomic, readonly) GPUImageVideoCamera *camera;
@property (nonatomic, readonly) GPUImageMovieWriter *writer;
@property (nonatomic, readonly) GPUImageOutput<GPUImageInput> *filter;

/// @name Initialization and teardown

/** Initialize camera recorder with sessionPreset and device
 
 @param sessionPreset Session preset to use
 @param cameraPosition Camera to capture from
 */
- (id)initWithSessionPreset:(NSString *)sessionPreset cameraPosition:(AVCaptureDevicePosition)cameraPosition;

- (id)initWithStillCamera:(GPUImageStillCamera *)stillCamera filter:(GPUImageOutput<GPUImageInput> *)filter;

- (void)updateWithFilter:(GPUImageOutput<GPUImageInput> *)filter;

/// @name Manage classes and options

-(void) setComposer:(GPUImageTwoInputFilter *) framesComposer;

-(void) setOverlay:(MDFrameDrawer *) framesOverlay;

/// @name Manage the camera video stream

/** Start camera capturing
 */
- (void)startCameraCapture;

/** Stop camera capturing
 */
- (void)stopCameraCapture;

/** Pause camera capturing
 */
- (void)pauseCameraCapture;

/** Resume camera capturing
 */
- (void)resumeCameraCapture;

/// @name Manage the camera recording

/** Start camera recording
 */
- (void)startCameraRecordingWithURL:(NSURL*) url size:(CGSize) size;

- (void)startCameraRecordingWithURL:(NSURL*) url size:(CGSize) size filter:(GPUImageOutput<GPUImageInput> *)filter;

/** Stop camera recording
 */
- (void)stopCameraRecordingWithCompetionHandler:(void (^)(void))handler;

/** Cancel current recording
 */
- (void)cancelCameraRecording;

@end
