//
//  ViewController.h
//  testUIView
//
//  Created by VanDao on 6/23/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UXOverlayView.h"
#import "UXTransitionView.h"
#import "UXTextTransitionView.h"
#import "UXImageTransitionView.h"
#import "UXTransitionDetail.h"
#import "UXTextTransitionDetail.h"
#import "UXImageTransitionDetail.h"
#import "HorizontalImageView.h"

#define __PI 3.14159265359
#define kWidth [[UIScreen mainScreen] bounds].size.width
#define kHeight [[UIScreen mainScreen] bounds].size.height

@interface ViewController : UIViewController <UXOverlayViewDelegate, HorizontalImageViewDataSource>

- (IBAction)addSticker:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addStickerOutlet;
@property (weak, nonatomic) IBOutlet UIButton *addTextOutlet;
- (IBAction)addText:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *contentTextField;
- (IBAction)SaveAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *trashImage;

@end

