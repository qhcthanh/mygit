//
//  SNSticker.h
//  CameraGPUImage
//
//  Created by BaoNQ on 8/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNSticker : UIImageView

- (id)initWithFrame:(CGRect)frame image:(UIImage *)image atZPosition:(CGFloat)zIndex;

- (id)initWithFrame:(CGRect)frame image:(UIImage *)image contentMode:(UIViewContentMode)mode atZPosition:(CGFloat)zIndex;


@end
