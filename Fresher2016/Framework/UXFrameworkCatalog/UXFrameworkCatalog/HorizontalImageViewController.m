//
//  ViewController11.m
//  CameraGPUImage
//
//  Created by BaoNQ on 6/24/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "HorizontalImageViewController.h"

@implementation HorizontalImageViewController {
    float _scaledRatio;
    CGPoint _initialPoint;
    NSMutableArray *_images;
    
    HorizontalImageView *sliderView;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // Do any additional setup after loading the view, typically from a nib.
    sliderView = [[HorizontalImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    sliderView.dataSource = self;
    //Create data
    _images = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 18; ++i) {
        NSString *name = [NSString stringWithFormat:@"image%i.png", i];
        [_images addObject:[UIImage imageNamed:name]];
    }
    
    sliderView.userInteractionEnabled = YES;
    [self.view addSubview:sliderView];
    
    [sliderView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//MARK: - HorizontalImageView DataSource

- (NSInteger)numberOfImages:(HorizontalImageView *)horizontalImageView {
    return _images.count;
}

- (UIImage *)imageInHorizontalImageView:(HorizontalImageView *)horizontalImageView atIndex:(NSInteger)index {
    return _images[index];
}

@end
