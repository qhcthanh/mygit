//
//  PreviewFilterCell.h
//  CameraGPUImage
//
//  Created by qhcthanh on 6/30/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"
#import "InstaFilters.h"
@interface PreviewFilterCell : UICollectionViewCell

@property (weak,nonatomic) id filter;

-(void)renderUI:(id)filter camera:(GPUImageStillCamera*)videoCamera;
-(void)cleanUI;
@end
