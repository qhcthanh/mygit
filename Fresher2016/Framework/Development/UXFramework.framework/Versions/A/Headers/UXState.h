/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@class UXImageMemoryCache;

@interface UXState : NSObject

/**
 * Access the global image memory cache.
 *
 * If a cache hasn't been assigned via UXFramework::setGlobalImageMemoryCache: then one will be created
 * automatically.
 *
 * @remarks The default image cache has no upper limit on its memory consumption. It is
 *               up to you to specify an upper limit in your application.
 */
+ (UXImageMemoryCache *)imageMemoryCache;

/**
 * Access the global network operation queue.
 *
 * The global network operation queue exists to be used for asynchronous network requests if
 * you choose. By defining a global operation queue in the core of UXFramework, we can ensure that
 * all libraries that depend on core will use the same network operation queue unless configured
 * otherwise.
 *
 * If an operation queue hasn't been assigned via UXFramework::setGlobalNetworkOperationQueue: then
 * one will be created automatically with the default iOS settings.
 */
+ (NSOperationQueue *)networkOperationQueue;

#pragma mark Modifying Global State /** @name Modifying Global State */

/**
 * Set the global image memory cache.
 *
 * The cache will be retained and the old cache released.
 */
+ (void)setImageMemoryCache:(UXImageMemoryCache *)imageMemoryCache;

/**
 * Set the global network operation queue.
 *
 * The queue will be retained and the old queue released.
 */
+ (void)setNetworkOperationQueue:(NSOperationQueue *)queue;

@end
