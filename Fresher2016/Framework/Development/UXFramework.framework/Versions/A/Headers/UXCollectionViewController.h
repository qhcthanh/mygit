/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

@interface UXCollectionViewController : UICollectionViewController {
@protected
    UIStatusBarStyle  _statusBarStyle;
    UIBarStyle        _navigationBarStyle;
    UIColor*          _navigationBarTintColor;
    
    BOOL _isViewAppearing;
    BOOL _hasViewAppeared;
    
    NSString* _titleString;
}

/**
 * The style of the navigation bar when this view controller is pushed onto
 * a navigation controller.
 *
 * @default UIBarStyleDefault
 */
@property (nonatomic) UIBarStyle navigationBarStyle;

/**
 * The color of the navigation bar when this view controller is pushed onto
 * a navigation controller.
 *
 * @default UXSTYLEVAR(navigationBarTintColor)
 */
@property (nonatomic, strong) UIColor* navigationBarTintColor;

/**
 * The style of the status bar when this view controller is appearing.
 *
 * @default UIStatusBarStyleDefault
 */
@property (nonatomic) UIStatusBarStyle statusBarStyle;

/**
 * The view has appeared at least once and hasn't been removed due to a memory warning.
 */
@property (nonatomic, readonly) BOOL hasViewAppeared;

/**
 * The view is about to appear and has not appeared yet.
 */
@property (nonatomic, readonly) BOOL isViewAppearing;

/**
 *  The title of navigation bar when this view controller is appearing
 */
@property (nonatomic, retain) NSString* titleString;

/**
 *  Display navigation bar with gray and white style
 */
@property (nonatomic, assign) BOOL displayGrayAndWhiteStyle;

@end
