/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXStyleSheet.h"

/**
 * Deprecated macros for common constants.
 */
#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 \
alpha:(a)]

#define HSVCOLOR(h,s,v) [UIColor colorWithHue:(h) saturation:(s) value:(v) alpha:1]
#define HSVACOLOR(h,s,v,a) [UIColor colorWithHue:(h) saturation:(s) value:(v) alpha:(a)]

#define RGBA(r,g,b,a) (r)/255.0, (g)/255.0, (b)/255.0, (a)


#define UXSTYLE(_SELECTOR) [[UXStyleSheet globalStyleSheet] styleWithSelector:@#_SELECTOR]

#define UXSTYLESTATE(_SELECTOR, _STATE) [[UXStyleSheet globalStyleSheet] \
styleWithSelector:@#_SELECTOR forState:_STATE]

#define UXSTYLESHEET ((id)[UXStyleSheet globalStyleSheet])

#define UXSTYLEVAR(_VARNAME) [UXSTYLESHEET _VARNAME]
#define UXSTYLESHEETPROPERTY(_VARNAME) [UXSTYLESHEET _VARNAME]

typedef enum tagPosition{
	UXPositionStatic,
	UXPositionAbsolute,
	UXPositionFloatLeft,
	UXPositionFloatRight,
} UXPosition;

@interface UXStyleDefines : NSObject {

}

@end
