//
//  FriendCollectionViewCell.h
//  FacebookContact
//
//  Created by qhcthanh on 7/14/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBUserInfo.h"
#define kIndicatorViewTag 1001

@interface FriendCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) FBUserInfo* friend;

/**
 *  <#Description#>
 *
 *  @param user <#user description#>
 */
- (void)bindingUIWithUserInfo:(FBUserInfo*)user;

/**
 *  Hide all UI in cell to clean up empty cell
 */
- (void)hideAllUIInCell;


@end
