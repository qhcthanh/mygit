//
//  PreiviewFilterRealTimeView.h
//  CameraGPUImage
//
//  Created by qhcthanh on 6/30/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstaFilters.h"
#import "PreviewFilterGPUImageView.h"
#define kWidth [[UIScreen mainScreen] bounds].size.width
#define kHeight [[UIScreen mainScreen] bounds].size.height

@protocol PreviewFilterRealTimeViewDelegate <NSObject>

@optional
-(void)didSelectFilter:(id)filter;

@end

@interface PreviewFilterRealTimeView : UIView

@property (nonatomic,weak) id<PreviewFilterRealTimeViewDelegate> delegate;
@property BOOL isShowMenu;
@property BOOL canTouch;
@property (nonatomic) PreviewFilterGPUImageView *currentFilterView;

/**
 *  Initializes an `PreviewFilterRealTimeView` object with CGRect frame on superview
 *
 *  @param videoCamera The videoCamera is GPUImageStillCamera use to preview filters
 *  @param frame       The frame menu PreviewFilterRealTimeView to display
 *  @param filters     The list filters to display. Menu will get 9 filter first
 *
 *  @return The newly-initialized PreviewFilterRealTimeView
 */
-(id)initWithCamera:(GPUImageVideoCamera *)videoCamera frame:(CGRect)frame filters:(id)filters;

-(void)showFilterMenu;

-(void)showFilterSelect;

-(void)hideMenuFilter;

@end
