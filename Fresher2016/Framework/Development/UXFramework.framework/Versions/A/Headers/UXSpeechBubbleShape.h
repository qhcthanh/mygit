/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "UXShape.h"

@interface UXSpeechBubbleShape : UXShape {
}

@property (nonatomic) CGFloat radius;
@property (nonatomic) CGFloat pointLocation;
@property (nonatomic) CGFloat pointAngle;
@property (nonatomic) CGSize pointSize;

+ (UXSpeechBubbleShape*)shapeWithRadius:(CGFloat)radius pointLocation:(CGFloat)pointLocation
							 pointAngle:(CGFloat)pointAngle pointSize:(CGSize)pointSize;

@end
