//
//  SNFilter.m
//  CameraGPUImage
//
//  Created by BaoNQ on 8/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "SNFilter.h"

@implementation SNFilter


+ (NSMutableArray<SNFilter *> *)generateFilters:(UIImage *)originalImage filterNames:(NSArray<NSString *> *)filters {
    
    NSMutableArray<SNFilter *> *finalFilters = [[NSMutableArray alloc] init];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_queue_attr_t qos_attr = dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL, QOS_CLASS_UTILITY, 0);
    dispatch_queue_t syncQueue = dispatch_queue_create("com.vng.filterScrolling", qos_attr);
    
    // Each filter can be generated on a different thread
    dispatch_apply(filters.count, queue, ^(size_t i){
        
        SNFilter * computedFilter = [[SNFilter alloc] initWithImage:originalImage];
        [computedFilter applyFilter:filters[i]];
        
        dispatch_sync(syncQueue, ^{
            [finalFilters addObject:computedFilter];
            // return;
        });
    });
    return finalFilters;
}

+ (NSArray *)filterNameList {
    return @[@"No Filter" , @"CIPhotoEffectFade", @"CIPhotoEffectChrome", @"CIPhotoEffectTransfer", @"CIPhotoEffectInstant", @"CIPhotoEffectMono", @"CIPhotoEffectNoir", @"CIPhotoEffectProcess", @"CIPhotoEffectTonal"];
}

+ (BOOL)isFilterExisting:(NSString *)filterName {
    
    for (NSString *filter in [SNFilter filterNameList]) {
        if ([filterName isEqualToString:filter])
            return YES;
    }
    return NO;
}

- (id)initWithImage:(UIImage *)originalImage {
    
    self = [super init];
    
    if (self) {
        self.stickers = [[NSMutableArray alloc] init];
        self.image = originalImage;
    }
    return self;
}

- (void)applyFilter:(NSString *)filterName {
    
    self.name = filterName;
    
    if (![SNFilter isFilterExisting:filterName]) {
        NSLog(@"Filter not existing");
    }
    else if (![filterName isEqualToString:@"No Filter"]) {
        // Create and apply filter
        // 1. create source image
        CIImage *sourceImage = [[CIImage alloc] initWithImage:self.image];
        
        // 2. create filter using filter name.
        CIFilter *myFilter = [CIFilter filterWithName:filterName];
        [myFilter setDefaults];
        
        // 3. set source image
        [myFilter setValue:sourceImage forKey:kCIInputImageKey];
        
        // 4. create core image context
        CIContext *context = [CIContext contextWithOptions:nil];
        
        // 5. output filtered image as CGImage with dimension
        CGImageRef cgImageRef = [context createCGImage:myFilter.outputImage fromRect:myFilter.outputImage.extent];
        
        // 6. convert to UIImage
        UIImage *filteredImage = [UIImage imageWithCGImage:cgImageRef];
        

        self.image = filteredImage;
    }
}

- (void)addSticker:(SNSticker *)sticker {
    [self.stickers addObject:sticker];
}

@end
