//
//  UserInfoViewController.m
//  FacebookContact
//
//  Created by qhcthanh on 7/15/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

#import "UserInfoViewController.h"
#import "FBImageDownloader.h"
#import "FBSessionManager.h"
#import "FBAPIUtility.h"
#import "FBCacheManager.h"

@interface UserInfoViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *pictureImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end

@implementation UserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(_userInfo) {
        _nameLabel.text = _userInfo.userName;
        _pictureImageView.image = [[FBCacheManager sharedCache] imageForKey:_userInfo.userID];
        _genderLabel.text = [FBUserInfo getStringFBUserGender:_userInfo.gender];
        
        UIImage* cvImage = [[FBCacheManager sharedCache] imageForKey:_userInfo.coverURL];
        if (cvImage) {
            _coverImageView.image = cvImage;
        } else {
            FBImageDownloader* dowloader = [[FBImageDownloader alloc] initWithSessionManager:[FBSessionManager sharedDefaultSessionManager]];
            NSURL* coverURL = [NSURL URLWithString:_userInfo.coverURL];
            
            [dowloader downloadImageWithURL:coverURL identifier:_userInfo.coverURL completionHandler:^(id imageIdentifier, UIImage *image, NSURLResponse *response, NSError *error) {
                if (!error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.coverImageView.image = image;
                    });
                }
            }];
        }
        
        _infoLabel.text = [NSString stringWithFormat:@"Sinh nhật: %@\nTiểu sử: %@\nQuê quán: %@\nGhi chú: %@",_userInfo.birthDay,_userInfo.biography,_userInfo.homeTown,_userInfo.quote];
        _infoLabel.text = [_infoLabel.text stringByReplacingOccurrencesOfString:@"(null)" withString:@"Chưa cập nhật"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

@end
