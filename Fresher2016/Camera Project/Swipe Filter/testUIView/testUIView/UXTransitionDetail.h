//
//  UXTransitionDetail.h
//  testUIView
//
//  Created by VanDao on 6/28/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"
@class UXTransitionView;
@class UXImageTransitionView;
@class UXTextTransitionView;

#define __PI 3.14159265359
#define kWidth [[UIScreen mainScreen] bounds].size.width
#define kHeight [[UIScreen mainScreen] bounds].size.height

@interface UXTransitionDetail : NSObject

@property(strong, nonatomic) UXTransitionView *transitionView;

- (id)initWithTransitionView:(UXTransitionView *)view;

/**
 *  Set transform values of `UXTransitionView` object
 *
 *  @param initTranslate The translation of `UXTransitionView`
 *  @param initScale     The scale of `UXTransitionView`
 *  @param initRotate    The rotation of `UXTransitionView`
 */
- (void)setTransformStickerWithTranslate:(CGPoint)initTranslate scale:(CGFloat)initScale rotate:(CGFloat)initRotate;

/**
 * Update transform values of `UXTransitionView` object
 *  - currentTranslation += newTranslate
 *  - currentScale *= newScale
 *  - currentRotate += newRotate
 *
 *  @param newTranslate The translation deviation of `UXTransitionView`
 *  @param newScale     The scale deviation of `UXTransitionView`
 *  @param newRotate    The rotation deviation of `UXTransitionView`
 */
- (void)updateTransformStickerWithTranslate:(CGPoint)newTranslate scale:(CGFloat)newScale rotate:(CGFloat)newRotate;

/**
 * Update transform values of `UXTransitionView` object
 *  - currentTranslation += newTranslate
 *  - currentScale *= newScale
 *  - currentRotate += newRotate
 *
 *  @param newTranslate The translation deviation of `UXTransitionView`
 *  @param newScale     The scale deviation of `UXTransitionView`
 *  @param newRotate    The rotation deviation of `UXTransitionView`
 *  @param anchorPoint  The position to apply rotate, scale transform. Default: center
 */
- (void)updateTransformStickerWithTranslate:(CGPoint)newTranslate scale:(CGFloat)newScale rotate:(CGFloat)newRotate atAnchorPoint:(CGPoint)anchorPoint;

/**
 *  Return an image create by original image after apply transformation
 *
 *  @return The result image
 */
- (UIImage *)getImage;



@end
