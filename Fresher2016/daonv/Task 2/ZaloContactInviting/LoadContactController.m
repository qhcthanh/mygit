//
//  ContactViewController.m
//  ZaloContactInviting
//
//  Created by VanDao on 5/26/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import "LoadContactController.h"
#import "ContactTableViewCell.h"
#import "CTCollectionViewCell.h"

const NSInteger kMaxRowSelect = 99;

@interface LoadContactController (){
    ContactManager *contactManager;
}

@end

@implementation LoadContactController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.contactFilterList = [NSMutableArray new];
    self.contactList = [NSMutableArray new];
    self.sectionList = [NSMutableArray new];
    self.imageManager = [[ImageCacheManager alloc]init];
    self.collectionContact = [NSMutableArray new];
    
    // "Choose friend" label
    self.navigationTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 35)];;
    self.navigationTitleLabel.backgroundColor = [UIColor clearColor];
    self.navigationTitleLabel.numberOfLines = 1;
    self.navigationTitleLabel.font = [UIFont boldSystemFontOfSize:16];
    self.navigationTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.navigationTitleLabel.textColor = [UIColor blackColor];
    self.navigationTitleLabel.text = @"Choose friend";
    // Selected person number label
    self.selectedPersonNumberLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 20, 200, 20)];
    self.selectedPersonNumberLabel.backgroundColor = [UIColor clearColor];
    self.selectedPersonNumberLabel.numberOfLines = 1;
    self.selectedPersonNumberLabel.font = [UIFont boldSystemFontOfSize:8];
    self.selectedPersonNumberLabel.textAlignment = NSTextAlignmentCenter;
    self.selectedPersonNumberLabel.textColor = [UIColor blackColor];
    self.selectedPersonNumberLabel.text = [NSString stringWithFormat:@"%i/%i", (int)self.selectedCount, (int)kMaxRowSelect];
    // Set navigation title
    self.navigationBar.titleView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 200, 50)];
    [self.navigationBar.titleView addSubview:self.navigationTitleLabel];
    [self.navigationBar.titleView addSubview:self.selectedPersonNumberLabel];

    // Set TableView to Editing state
    [self.tableView allowsMultipleSelectionDuringEditing];
    [self.tableView setEditing:YES animated:NO];
    self.isSearching = NO;
    self.isEditing = NO;

    // Hide collection view
    self.heightCollectionView.constant = 0;
    
    // Request access to contact database
    contactManager = [ContactManager sharedManager];
    [contactManager getAllContactWithCallbackBlock:^(NSMutableArray<ContactDetail *> * _Nullable contactList, NSError * _Nullable error) {
        if (error){
            // If fail to get contact -> notify to user
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:[NSString stringWithFormat:@"Error code: %lu", [error code]]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Close"
                                                  otherButtonTitles:nil];
            [alert show];
        } else {
            // If get contact successfully, sort contact by ascending, generate section and reload data
            self.contactList = contactList;
            [self sortContactList];
            [self generateSectionList];
            [self.tableView reloadData];
        }
    } onQueue: nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:NO];
}

#pragma mark - Navigation bar
- (IBAction)editBarButton:(id)sender {
    self.isEditing = !self.isEditing;
    
    if (!self.isEditing){
        self.navigationItem.leftBarButtonItem.title = @"Edit";
        [self.tableView setEditing:YES animated:YES];
        self.tableView.allowsMultipleSelection = YES;
        
        self.selectedPersonNumberLabel.text = [NSString stringWithFormat:@"%lu/%lu", self.selectedCount, kMaxRowSelect];
    }else{
        self.navigationItem.leftBarButtonItem.title = @"Done";
        [self.tableView setEditing:NO animated:YES];
        self.tableView.allowsMultipleSelection = NO;
        
        self.selectedPersonNumberLabel.text = @"";

        //Remove contact selected and hide collection view
        if (self.heightCollectionView > 0){
            [self.collectionContact removeAllObjects];
            self.selectedCount = 0;
            
            NSArray<NSIndexPath *> *indexPathArray = [self.tableView indexPathsForSelectedRows];
            for (NSInteger index = 0; index < [indexPathArray count]; index++){
                [self.tableView deselectRowAtIndexPath:indexPathArray[index] animated:NO];
            }
            
            [self.colectionView reloadData];
            self.heightCollectionView.constant = 0;
            [UIView animateWithDuration:.5 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    }
}

#pragma mark - UITableViewDataSource

// ------- Section in table view ----------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.isSearching)
        return 1;
    else
        return [self.sectionList count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (self.isSearching)
        return nil;
    else
        return self.sectionList[section].name;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    if (self.isSearching)
        return nil;
    else {
        NSMutableArray *sectionTitle = [[NSMutableArray alloc]init];
        for (SectionInfo *section in self.sectionList){
            [sectionTitle addObject:section.name];
        }
        return sectionTitle;
    }
}

// ---------Cell in table view ----------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return 70;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.isSearching)
        return [self.contactFilterList count];
    else
        return self.sectionList[section].count;
}

- (void)tableView: (UITableView *)tableView willDisplayCell:(nonnull ContactTableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    // Get contact information
    ContactDetail *contact;


    if (self.isSearching){
        NSInteger index = indexPath.row;
        contact = self.contactFilterList[index];
    }
    else{
        NSInteger index = self.sectionList[indexPath.section].indexOfFirstRow + indexPath.row;
        contact = self.contactList[index];
    }
    
    // Set cell information
    cell.fullNameLabel.text = [contact getFullName];
    cell.imageView.image = [self.imageManager getImageWithID:contact.identifier];
    
    if (cell.imageView.image == nil){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            if (contact.image != nil){
                // If have image
                [self.imageManager setImage:[UIImage clipToCircleImage:contact.image withMask:[UIImage imageNamed:@"mask4.png"]] withID:contact.identifier];
            }else{
                // If don't have image
                NSString *letters = @"";
                if ([contact.givenName length] > 0){
                    letters = [letters stringByAppendingString:[NSString stringWithFormat:@"%c", [contact.givenName characterAtIndex:0]]];
                }
                if ([contact.familyName length] > 0){
                    letters = [letters stringByAppendingString:[NSString stringWithFormat:@"%c", [contact.familyName characterAtIndex:0]]];
                }
                
                [self.imageManager setImage:[UIImage createImageFromLetters:[letters uppercaseString]] withID:contact.identifier];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.imageView.image = [self.imageManager getImageWithID:contact.identifier];
            });
        });
    }
}

- (ContactTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier = @"Cell";
    // Create a custom cell
    ContactTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil){
        cell = [[ContactTableViewCell alloc] init];
    }

    ContactDetail *contact;
    if (self.isSearching){
        NSInteger index = indexPath.row;
        contact = self.contactFilterList[index];
    }
    else{
        NSInteger index = self.sectionList[indexPath.section].indexOfFirstRow + indexPath.row;
        contact = self.contactList[index];
    }
    // Check if this contact selected
    BOOL isSelected = NO;
    for (int i = 0; i < self.selectedCount; i++){
        if ([self.collectionContact[i].identifier compare:contact.identifier] == NSOrderedSame){
            isSelected = YES;
            break;
        }
    }
    // Set the row selected if found contact in collection contact list
    if (isSelected){
        [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.isEditing){        
        return;
    }
    
    if (self.selectedCount >= kMaxRowSelect){
        // If numbers of selected person is maximum
        
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notice"
                                                        message:[NSString stringWithFormat:@"Your invite list only maximum of %lu people", kMaxRowSelect]
                                                       delegate:nil
                                              cancelButtonTitle:@"Close"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else{
        // If select this row is valid (numbers of selected row is not maximum)
        
        // If this is first select -> Show Collection view
        if (self.selectedCount == 0){
            // Show collection view
            self.heightCollectionView.constant = 50;
            [UIView animateWithDuration:.5 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
        
        // Increase selected count
        self.selectedCount++;
        
        // Add selected contact to collection view
        ContactDetail *contact;
        if (self.isSearching){
            NSInteger index = indexPath.row;
            contact = self.contactFilterList[index];
        }
        else{
            NSInteger index = self.sectionList[indexPath.section].indexOfFirstRow + indexPath.row;
            contact = self.contactList[index];
        }
        
        // Insert contact to collection contact and show on collection view
        if ([self.collectionContact count] == 0)
        {
            [self.collectionContact insertObject:contact atIndex:0];
            [self.colectionView insertItemsAtIndexPaths:[NSArray arrayWithObject:                [NSIndexPath indexPathForRow:0 inSection:0]]];
        }else{
            NSArray<NSIndexPath*> *indexPathVisibleArray = [self.colectionView indexPathsForVisibleItems];
            
            [self.collectionContact insertObject:contact atIndex:indexPathVisibleArray[0].row];
            [self.colectionView insertItemsAtIndexPaths:[indexPathVisibleArray subarrayWithRange:NSMakeRange(0, 1)]];
        }
        
        // Update navigation title
        self.selectedPersonNumberLabel.text = [NSString stringWithFormat:@"%i/%i", (int)self.selectedCount, (int)kMaxRowSelect];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.isEditing)
        return;
    
    // Remove contact from collection view
    ContactDetail *contact;
    
    if (self.isSearching){
        NSInteger index = indexPath.row;
        contact = self.contactFilterList[index];
    }
    else{
        NSInteger index = self.sectionList[indexPath.section].indexOfFirstRow + indexPath.row;
        contact = self.contactList[index];
    }
    
    // Get index of contact in collection view
    NSInteger row = [self.collectionContact indexOfObject:contact];
    
    // Remove from collection view
    NSIndexPath *collectionIndex = [NSIndexPath indexPathForRow:row inSection:0];
    self.selectedCount--;
    [self.colectionView deleteItemsAtIndexPaths:[[NSArray alloc]initWithObjects:collectionIndex, nil]];
    
    // Remove contact
    [self.collectionContact removeObjectAtIndex:row];
    
    // If number of selected row is 0 -> Hide collection view
    if (self.selectedCount == 0){
        //Hide collection view
        self.heightCollectionView.constant = 0;
        [UIView animateWithDuration:.5 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
    
    // Update title
    self.selectedPersonNumberLabel.text = [NSString stringWithFormat:@"%i/%i", (int)self.selectedCount, (int)kMaxRowSelect];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    // Hide keyboard if scroll table view
    [self.searchBar resignFirstResponder];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        ContactDetail *contact;
        if (self.isSearching){
            contact = self.contactFilterList[indexPath.row];
            [self.contactFilterList removeObject:contact];
        }else{
            contact = self.contactList[self.sectionList[indexPath.section].indexOfFirstRow + indexPath.row];
        }
        
        [self.contactList removeObject:contact];
        self.sectionList[indexPath.section].count--;
        for (NSInteger i = indexPath.section + 1; i < [self.sectionList count]; i++){
            self.sectionList[i].indexOfFirstRow--;
        }

        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
        

        if (self.sectionList[indexPath.section].count == 0){
            [self.sectionList removeObjectAtIndex:indexPath.section];
            
            for (NSInteger i = indexPath.section + 1; i < [self.sectionList count]; i++){
                self.sectionList[i].indexOfSection--;
            }
            
            if (!self.isSearching){
                [self.tableView beginUpdates];
                [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
                [self.tableView endUpdates];
            }
        }
    }
}

#pragma mark - UIColectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.selectedCount;
}

- (CTCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"CollectionCell";
    
    CTCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    if (cell == nil){
        cell = [[CTCollectionViewCell alloc]init];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(CTCollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    cell.imageView.image = [self.imageManager getImageWithID:self.collectionContact[indexPath.row].identifier];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    // Descrease selected count
    self.selectedCount--;
    
    // Update title
    self.selectedPersonNumberLabel.text = [NSString stringWithFormat:@"%i/%i", (int)self.selectedCount, (int)kMaxRowSelect];
    
    // Uncheck contact from table view
    // Get indexPath at table view
    NSArray<NSIndexPath *> *indexPathSelectedList = [self.tableView indexPathsForSelectedRows];
    NSIndexPath *recheckIndexPath = nil;
    for (NSIndexPath *index in indexPathSelectedList){
        ContactDetail* contact;
        if (self.isSearching){
            contact = self.contactFilterList[index.row];
        }
        else{
            contact = self.contactList[self.sectionList[index.section].indexOfFirstRow + index.row];
        }
        
        if ([contact.identifier isEqualToString:self.collectionContact[indexPath.row].identifier]){
            recheckIndexPath = index;
            break;
        }
    }
    
    // Remove from collection view
    [self.colectionView deleteItemsAtIndexPaths:[[NSArray alloc]initWithObjects:indexPath, nil]];
    
    // Remove contact
    [self.collectionContact removeObjectAtIndex:indexPath.row];
    
    // Recheck contact at table view
    if (recheckIndexPath)
        [self.tableView deselectRowAtIndexPath:recheckIndexPath animated:YES];
    
    // If number of selected row is 0 -> Hide collection view
    if (self.selectedCount == 0){
        // Hide collection view
        self.heightCollectionView.constant = 0;
        [UIView animateWithDuration:.5 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [self.searchBar becomeFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if ([searchText length] > 0){
        // Search contact in another main thread
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.isSearching = YES;
            
            [self.contactFilterList removeAllObjects];
            
            for (ContactDetail *contact in self.contactList){
                if ([[contact.getFullName lowercaseString] containsString:[searchText lowercaseString]]){
                    [self.contactFilterList addObject:contact];
                }
            };
            
            // Reload table view after search successfully
            [self.tableView reloadData];
        });
    }else{
        self.isSearching = NO;
        [self.tableView reloadData];
    }
}

#pragma mark - NewContactViewController

// Add contact delegate
- (void)getNewContact:(ContactDetail *)newContact{
    if (!newContact)
        return;
    
    __weak typeof(self) weakSelf = self;
    [self addNewContact:newContact withCallbackBlock:^(BOOL newSection, NSIndexPath *indexPath){
        if (!weakSelf.isSearching){
            [weakSelf.tableView beginUpdates];
            if (newSection){
                [weakSelf.tableView insertSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            [weakSelf.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [weakSelf.tableView endUpdates];
        }
    }];
}

#pragma mark - Contact List

/**
 *  Sort contact list by ascending of full name
 */
 - (void)sortContactList{
     // Sort contact list
     [self.contactList sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
         ContactDetail *p1 = (ContactDetail *)obj1;
         ContactDetail *p2 = (ContactDetail *)obj2;
         
         
         char c1 = [[p1.getFullName uppercaseString] characterAtIndex:0];
         char c2 = [[p2.getFullName uppercaseString] characterAtIndex:0];
         
         if ((c1 >= 'A' && c1 <= 'Z')){
             if (!(c2 >= 'A' && c2 <= 'Z')){
                 return NSOrderedAscending;
             }
         }
         else if ((c2 >= 'A' && c2 <= 'Z')){
             if (!(c1 >= 'A' && c1 <= 'Z')){
                 return NSOrderedDescending;
             }
         }
         return ([[p1.getFullName uppercaseString] compare:[p2.getFullName uppercaseString]]);
     }];
 }
 
/**
 *  generate section list from contact list
 */
 - (void)generateSectionList{
     [self.sectionList removeAllObjects];
     
     // Get section list
     if ([self.contactList count] > 0){
         NSString *currentSection = [[self.contactList[0].getFullName substringWithRange:NSMakeRange(0, 1)] uppercaseString];
         if (!([currentSection characterAtIndex:0] >= 'A' && [currentSection characterAtIndex:0] <= 'Z') || [currentSection isEqual: @""])
             currentSection = @"#";
         
         NSInteger indexOfSection = 0;
         NSInteger indexOfFirstRow = 0;
         NSInteger count = 0;
         
         for (int i = 0; i < [self.contactList count]; i++){
             NSString *firstLetters = [[self.contactList[i].getFullName substringWithRange:NSMakeRange(0, 1)] uppercaseString];
             
             // If firstLetters not in alphabet, set the contact to section #
             char c = [firstLetters characterAtIndex:0];
             if (!(c >= 'A' && c <= 'Z')){
                 firstLetters = @"#";
             }
             
             // If currentSection different from firstLetter => the contact is at other section
             // Add the current section to section list, and set value of current section by firstLetter
             if ([currentSection compare:firstLetters] != NSOrderedSame){
                 [self.sectionList addObject:[SectionInfo sectionWithLetter:currentSection indexOfSection:indexOfSection indexOfFirstRow:indexOfFirstRow count:count]];
                 
                 char c = [firstLetters characterAtIndex:0];
                 if (!(c >= 'A' && c <= 'Z')){
                     currentSection = @"#";
                 }
                 else
                     currentSection = [firstLetters copy];
                 
                 indexOfSection++;
                 indexOfFirstRow = i;
                 count = 1;
             }
             else{
                 count++;
             }
         }
         
         // The last section have not added to section list yet
        [self.sectionList addObject:[SectionInfo sectionWithLetter:currentSection indexOfSection:indexOfSection indexOfFirstRow:indexOfFirstRow count:count]];
     }
 }
 
/**
 *  Add new contact to contactList by order acsending
 *
 *  @param newContact    contact object want to add
 *  @param callbackBlock The callback block called by dispatch_get_main_queue
 */
- (void)addNewContact: (ContactDetail *)newContact withCallbackBlock:(void(^)(BOOL isNewSection, NSIndexPath* indexPath))callbackBlock{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self.contactList insertObject:newContact atIndex:0];
        [self sortContactList];
        NSInteger index = [self.contactList indexOfObject:newContact];
        NSString *sectionName = [[newContact.getFullName uppercaseString] substringToIndex:1];
        if (!([sectionName characterAtIndex:0] >= 'A' && [sectionName characterAtIndex:0] <= 'Z')){
            sectionName = @"#";
        }
        NSInteger oldSection = [self.sectionList count];
        [self generateSectionList];
        BOOL isNewSection = (oldSection != [self.sectionList count]);
        NSInteger indexOfSection = 0;
        for (indexOfSection = 0; indexOfSection < [self.sectionList count]; indexOfSection++){
            if ([sectionName isEqualToString:self.sectionList[indexOfSection].name]){
                break;
            }
        }
        NSInteger row = index - self.sectionList[indexOfSection].indexOfFirstRow;
        NSInteger section = indexOfSection;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            callbackBlock(isNewSection, [NSIndexPath indexPathForRow:row inSection:section]);
        });
    });
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NewContactController* newContactViewController = segue.destinationViewController;
    newContactViewController.delegate = self;
}

@end
