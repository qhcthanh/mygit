/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

/**
 * UITextField leaves a little to be desired on the visual customization front.
 * UXTextField attempts to solve the most basic of those gaps so that
 * the CSS subsystem can function properly.
 */
@interface UXTextField : UITextField

/**
 * If non-nil, this color will be used to draw the placeholder text.
 * If nil, we will use the system default.
 */
@property (nonatomic, strong) UIColor* placeholderTextColor;

/**
 * If non-nil, this font will be used to draw the placeholder text.
 * else the text field font will be used.
 */
@property (nonatomic, strong) UIFont* placeholderFont;

/**
 * The amount to inset the text by, or zero to use default behavior
 */
@property (nonatomic, assign) UIEdgeInsets textInsets;

@end

