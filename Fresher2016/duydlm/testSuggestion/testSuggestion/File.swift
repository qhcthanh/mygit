//
//  File.swift
//  testSuggestion
//
//  Created by manhduydl on 8/18/16.
//  Copyright © 2016 admin. All rights reserved.
//

import Foundation

public class File {
    var internalQueue : dispatch_queue_t
    static let shareInstance = File()
    
    init() {
        internalQueue = dispatch_queue_create("com.vng.banlakey", DISPATCH_QUEUE_SERIAL)
    }
    
    func read(fileName : String, completionHandle : (String, NSError?) -> Void) {
       
        dispatch_async(internalQueue) {
            let fileURL = self.getFileUrl(fileName)
            var content = ""
            var err : NSError? = nil
            do {
                content = try String(contentsOfURL: fileURL)
            } catch let error as NSError {
                //print("Failed reading from URL: \(fileURL), Error: " + error.localizedDescription)
                err = error
            }
            
            completionHandle(content, err)
        }
    }
    
    func write(fileName : String, content : String, completionHandle : (String, NSError?) -> Void) {
        dispatch_async(internalQueue) { 
            let fileURL = self.getFileUrl(fileName)
            var err : NSError? = nil
            do {
                try content.writeToURL(fileURL, atomically: true, encoding: NSUTF8StringEncoding)
            } catch let error as NSError {
                //print("Failed writing to URL: \(fileURL), Error: " + error.localizedDescription)
                err = error
            }
            
            completionHandle(fileURL.path!, err)
        }
    }
    
    func getFileUrl(fileName : String) -> NSURL {
        let DocumentDirURL = try! NSFileManager.defaultManager().URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true)
        
        let fileURL = DocumentDirURL.URLByAppendingPathComponent(fileName).URLByAppendingPathExtension("txt")
        return fileURL
    }
}