//
//  SectionInfo.m
//  ZaloContactInviting
//
//  Created by VanDao on 6/6/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import "SectionInfo.h"

@implementation SectionInfo

- (id)initWithLetter: (NSString *)name indexOfSection:(NSInteger)sectionIndex indexOfFirstRow:(NSInteger)firstRowIndex count:(NSInteger)count{
    self = [super init];
    
    if (self){
        self.name = [name copy];
        self.indexOfSection = sectionIndex;
        self.indexOfFirstRow = firstRowIndex;
        self.count = count;
    }
    
    return self;
}

+ (SectionInfo * _Nonnull)sectionWithLetter: (NSString * _Nonnull)name indexOfSection:(NSInteger)sectionIndex indexOfFirstRow:(NSInteger)firstRowIndex count:(NSInteger)count{
    SectionInfo *section = [[SectionInfo alloc]init];
    section.name = name;
    section.indexOfSection = sectionIndex;
    section.indexOfFirstRow = firstRowIndex;
    section.count = count;
    
    return section;
}

@end

