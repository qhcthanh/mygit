//
//  CTFilterView.h
//  CameraGPUImage
//
//  Created by BaoNQ on 6/27/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTFilterView : UIImageView <UXPagingScrollViewPage>

- (void)mask:(CGRect)maskRect;
- (void)updateMask:(CGRect)maskRect newXPosition:(CGFloat)nX;
- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;
@end
