//
//  Contact.m
//  w1UITableView
//
//  Created by manhduydl on 5/20/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "Contact.h"

@implementation Contact
@synthesize fullName, avatarImage;

- (instancetype)initContactWithGivenname:(NSString *)aGivenname FamilyName:(NSString *)aFamilyName andImage:(UIImage *)anImage {
    if ([super init]) {
        self.fullName = [NSString stringWithFormat:@"%@ %@", aGivenname, aFamilyName];
        self.givenName = aGivenname;
        self.familyName = aFamilyName;
        self.avatarImage = anImage;
        if ([aGivenname isEqualToString:@""] && [aFamilyName isEqualToString:@""]) {
            self.symbolOfName = @"#";
        } else {
            if ([aGivenname isEqualToString:@""]) {
                self.symbolOfName = [aFamilyName substringToIndex:1];
            } else {
                if ([aFamilyName isEqualToString:@""]) {
                    self.symbolOfName = [aGivenname substringToIndex:1];
                } else {
                    self.symbolOfName = [NSString stringWithFormat:@"%@%@", [aGivenname substringToIndex:1], [aFamilyName substringToIndex:1]];
                }
            }
        }
    }
    return self;
}

- (instancetype)initContactWithGivenname:(NSString *)aGivenname FamilyName:(NSString *)aFamilyName FullName:(NSString *)aFullName Image:(UIImage *)anImage andIdentifier:(NSString *)identifier {
    if ([super init]) {
        //self.fullName = [NSString stringWithFormat:@"%@ %@", aGivenname, aFamilyName];
        self.fullName = aFullName;
        self.givenName = aGivenname;
        self.familyName = aFamilyName;
        self.avatarImage = anImage;
        self.identifier = identifier;
        if ([aGivenname isEqualToString:@""] && [aFamilyName isEqualToString:@""]) {
            self.symbolOfName = @"#";
        } else {
            if ([aGivenname isEqualToString:@""]) {
                self.symbolOfName = [aFamilyName substringToIndex:1];
            } else {
                if ([aFamilyName isEqualToString:@""]) {
                    self.symbolOfName = [aGivenname substringToIndex:1];
                } else {
                    self.symbolOfName = [NSString stringWithFormat:@"%@%@", [aGivenname substringToIndex:1], [aFamilyName substringToIndex:1]];
                }
            }
        }
    }
    return self;
}

@end
