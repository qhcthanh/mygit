/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Three20, ©2011-2014 Jverkoey
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXDataPopulatorDelegate.h"

/**
 * A general purpose JSON style sheet object for accessing a CSS style sheet's properties.
 *
 * This is useful if you want to use style sheets to manually customize certain aspects of
 * your UI.
 *
 */
@class UXJSONRuleSet;
@interface UXJSONStyleSheet : NSObject <UXDataPopulatorDelegate> {
@private
    
    // The "crude" Styles readed from CSS file.
    NSDictionary*         _cssStyles;
    
    // An collection of loaded UXJSONRuleSet.
    NSDictionary*			_cssRulesSet;
    
    // CSS mapped properties.
    NSDictionary*		    _propertiesMap;
    
    // Map for all objects
    __unsafe_unretained NSDictionary* _sourceDictionary;
    
    
    BOOL                    _loadedStyleMap;
}

@property (nonatomic, readonly) NSDictionary* cssStyles;

/**
 * An collection of loaded UXJSONRuleSet.
 */
@property (readonly) NSDictionary* cssRulesSet;

@property (nonatomic, assign) BOOL loadedStyleMap;

@property (nonatomic, assign) NSDictionary* sourceDictionary;

/**
 * Load the style sheet into memory from disk.
 *
 * @return NO if the file does not exist.
 */
- (BOOL)loadFromFilepath:(NSString*)filename;

/**
 * Get (text) color from a specific rule set, also accept an specific state.
 */
- (UIColor*)colorWithCssSelector:(NSString*)selector forState:(UIControlState)state;

/**
 * Get (text) color from a specific rule set.
 */
- (UIColor*)colorWithCssSelector:(NSString*)selector;

/**
 * Get opacity from a specific rule set.
 */
- (NSNumber*)opacityWithCssSelector:(NSString*)selector;

/**
 * Get background-color from a specific rule set, also accept an specific state.
 */
- (UIColor*)backgroundColorWithCssSelector:(NSString*)selector forState:(UIControlState)state;

/**
 * Get border-color from a specific rule set, also accept an specific state.
 */
- (UIColor*)borderColorWithCssSelector:(NSString*)selector forState:(UIControlState)state;

/**
 * Get background-image from a specific rule set, also accept an specific state.
 */
- (NSString*)backgroundImageWithCssSelector:(NSString*)selector forState:(UIControlState)state;

/**
 * Get background-color from a specific rule set.
 */
- (UIColor*)backgroundColorWithCssSelector:(NSString*)selector;

/**
 * Get border-color from a specific rule set.
 */
- (UIColor*)borderColorWithCssSelector:(NSString*)selector;

/**
 * Get background-image from a specific rule set.
 */
- (NSString*)backgroundImageWithCssSelector:(NSString*)selector;

/**
 * Get font from a specific rule set, also accept an specific state.
 */
- (UIFont*)fontWithCssSelector:(NSString*)selector forState:(UIControlState)state;

/**
 * Get font from a specific rule set and 'normal' state.
 */
- (UIFont*)fontWithCssSelector:(NSString*)selector;

/**
 * Get text shadow color from a specific rule set, also accept an specific state.
 */
- (UIColor*)textShadowColorWithCssSelector:(NSString*)selector forState:(UIControlState)state;

/**
 * Get text shadow color from a specific rule set.
 */
- (UIColor*)textShadowColorWithCssSelector:(NSString*)selector;

/**
 * Get text shadow offset from a specific rule set, also accept an specific state.
 */
- (CGSize)textShadowOffsetWithCssSelector:(NSString*)selector forState:(UIControlState)state;

/**
 * Get text shadow offset from a specific rule set.
 */
- (CGSize)textShadowOffsetWithCssSelector:(NSString*)selector;

/**
 * Get text shadow radius from a specific rule set.
 */
- (CGFloat)textShadowRadiusWithCssSelector:(NSString*)selector;

/**
 * Get text shadow radius from a specific rule set, also accept an specific state.
 */
- (CGFloat)textShadowRadiusWithCssSelector:(NSString*)selector forState:(UIControlState)state;

/**
 * CSS Rule Set.
 */
-(UXJSONRuleSet*)css:(NSString*)selectorName;

/**
 * CSS Rule Set, also accept an specific state.
 */
-(UXJSONRuleSet*)css:(NSString*)selectorName forState:(UIControlState)state;

-(id)objectWithCssSelector:(NSString*)selector;

/**
 * Release all cached data.
 */
- (void)freeMemory;


@end

