/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXTableViewDataSource.h"

/**
 * A simple factory for creating table view cells from objects.
 *
 * This factory provides a single method that accepts an object and returns a UITableViewCell
 * for use in a UITableView. A cell will only be returned if the object passed to the factory
 * conforms to the UXTableViewCellObject protocol. The created cell should ideally conform to
 * the UXTableViewCell protocol. If it does, the object will be passed to it via
 * @link UXTableViewCell::shouldUpdateCellWithObject: shouldUpdateCellWithObject:@endlink before the
 * factory method returns.
 *
 * This factory is designed to be used with UXTableViewDataSources, though one could easily use
 * it with other table view data source implementations simply by providing nil for the table
 * view model.
 *
 * If you instantiate an UXTableViewCellFactory then you can provide explicit mappings from objects
 * to cells. This is helpful if the effort required to implement the UXTableViewCell protocol on
 * an object outweighs the benefit of using the factory, i.e. when you want to map
 * simple types such as NSString to cells.
 *
 * @ingroup UXTableViewCellFactory
 */

@interface UXTableViewCellFactory : NSObject <UXTableViewDataSourceDelegate>

/**
 * Creates a cell from a given object if and only if the object conforms to the UXTableViewCellObject
 * protocol.
 *
 * This method signature matches the UXTableViewDataSourceDelegate method so that you can
 * set this factory as the model's delegate:
 *
 * @code
 // Must cast to id to avoid compiler warnings.
 _model.delegate = (id)[UXTableViewCellFactory class];
 * @endcode
 *
 * If you would like to customize the factory's output, implement the model's delegate method
 * and call the factory method. Remember that if the factory doesn't know how to map
 * the object to a cell it will return nil.
 *
 * @code
 - (UITableViewCell *)tableViewCellObject:(UXTableViewDataSource *)dataSource
 cellForTableView:(UITableView *)tableView
 atIndexPath:(NSIndexPath *)indexPath
 withObject:(id)object {
 UITableViewCell* cell = [UXTableViewCellFactory tableViewDataSource:tableViewModel
 cellForTableView:tableView
 atIndexPath:indexPath
 withObject:object];
 if (nil == cell) {
 // Custom cell creation here.
 }
 return cell;
 }
 * @endcode
 */
+ (UITableViewCell *)tableViewCellObject:(UXTableViewDataSource *)dataSource cellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath withObject:(id)object;

/**
 * Map an object's class to a cell's class.
 *
 * If an object implements the UXTableViewCell protocol AND is found in this factory
 * mapping, the factory mapping will take precedence. This allows you to
 * explicitly override the mapping on a case-by-case basis.
 */
- (void)mapObjectClass:(Class)objectClass toCellClass:(Class)cellClass;

/**
 * Returns the height for a row at a given index path.
 *
 * Uses the heightForObject:atIndexPath:tableView: selector from the UXTableViewCell protocol to ask the
 * object at indexPath in the model what its height should be. If a class mapping has been made for
 * the given object in this factory then that class mapping will be used over the result of
 * cellClass from the UXTableViewCellObject protocol.
 *
 * If the cell returns a height of zero then tableView.rowHeight will be used.
 *
 * Example implementation:
 *
 @code
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
 return [self.cellFactory tableView:tableView heightForRowAtIndexPath:indexPath model:self.model];
 }
 @endcode
 *
 * @param tableView The table view within which the cell exists.
 * @param indexPath The location of the cell in the table view.
 * @param model The backing model being used by the table view.
 * @returns The height of the cell mapped to the object at indexPath, if it implements
 *               heightForObject:atIndexPath:tableView:; otherwise, returns tableView.rowHeight.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath dataSource:(UXTableViewDataSource *)dataSource;

/**
 * Returns the height for a row at a given index path.
 *
 * Uses the heightForObject:atIndexPath:tableView: selector from the UXTableViewCell protocol to ask the
 * object at indexPath in the model what its height should be. Only implicit mappings will be
 * checked with this static implementation. If you would like to provide explicit mappings you must
 * create an instance of UXTableViewCellFactory.
 *
 * If the cell returns a height of zero then tableView.rowHeight will be used.
 *
 * Example implementation:
 *
 @code
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
 return [UXTableViewCellFactory tableView:tableView heightForRowAtIndexPath:indexPath model:self.model];
 }
 @endcode
 *
 * @param tableView The table view within which the cell exists.
 * @param indexPath The location of the cell in the table view.
 * @param model The backing model being used by the table view.
 * @returns The height of the cell mapped to the object at indexPath, if it implements
 *               heightForObject:atIndexPath:tableView:; otherwise, returns tableView.rowHeight.
 */
+ (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath dataSource:(UXTableViewDataSource *)dataSource;

@end

/**
 * The protocol for an object that can be used in the UXTableViewCellFactory.
 *
 * @ingroup UXTableViewCellFactory
 */
@protocol UXTableViewCellObject <NSObject>
@required
/** The class of cell to be created when this object is passed to the cell factory. */
- (Class)cellClass;

@optional
/** The style of UITableViewCell to be used when initializing the cell for the first time. */
- (UITableViewCellStyle)cellStyle;
@end

/**
 * The protocol for an object that can be used in the UXTableViewCellFactory with Interface Builder nibs.
 *
 * @ingroup UXTableViewCellFactory
 */
@protocol UXNibCellObject <NSObject>
@required

/** A nib that contains a table view cell to display this object's contents. */
- (UINib *)cellNib;

@end

/**
 * The protocol for a cell created in the UXTableViewCellFactory.
 *
 * Cells that implement this protocol are given the object that implemented the UXTableViewCellObject
 * protocol and returned this cell's class name in @link UXTableViewCellObject::cellClass cellClass@endlink.
 *
 * @ingroup UXTableViewCellFactory
 */
@protocol UXTableViewCellDelegate <NSObject>
@required
/**
 * Called when a cell is created and reused.
 *
 * Implement this method to customize the cell's properties for display using the given object.
 */
- (BOOL)shouldUpdateCellWithObject:(id)object;

@optional

/**
 * Asks the receiver whether the mapped object class should be appended to the reuse identifier
 * in order to create a unique cell.object identifier key.
 *
 * This is useful when you have a cell that is intended to be used by a variety of different
 * objects.
 */
+ (BOOL)shouldAppendObjectClassToReuseIdentifier;

/**
 * Asks the receiver to calculate its height.
 *
 * The following is an appropiate implementation in your tableView's delegate:
 *
 @code
 -(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
 CGFloat height = tableView.rowHeight;
 id object = [(UXTableViewDataSource *)tableView.dataSource objectAtIndexPath:indexPath];
 id class = [object cellClass];
 if ([class respondsToSelector:@selector(heightForObject:atIndexPath:tableView:)]) {
 height = [class heightForObject:object atIndexPath:indexPath tableView:tableView];
 }
 return height;
 }
 @endcode
 *
 * You may also use the
 * @link UXTableViewCellFactory::tableView:heightForRowAtIndexPath:model: tableView:heightForRowAtIndexPath:model:@endlink
 * methods on UXTableViewCellFactory to achieve the same result. Using the above example allows you to
 * customize the logic according to your specific needs.
 */
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;

@end

/**
 * A light-weight implementation of the UXTableViewCellObject protocol.
 *
 * Use this object in cases where you can't set up a hard binding between an object and a cell,
 * or when you simply don't want to.
 *
 * For example, let's say that you want to show a cell that shows a loading indicator.
 * Rather than create a new interface, LoadMoreObject, simply for the cell and binding it
 * to the cell view, you can create an UXTableViewCellObject and pass the class name of the cell.
 *
 @code
 [tableContents addObject:[UXTableViewCellObject objectWithCellClass:[LoadMoreCell class]]];
 @endcode
 */
@interface UXTableViewCellObject : NSObject <UXTableViewCellObject>

// Designated initializer.
- (id)initWithCellClass:(Class)cellClass userInfo:(id)userInfo;
- (id)initWithCellClass:(Class)cellClass;

+ (id)objectWithCellClass:(Class)cellClass userInfo:(id)userInfo;
+ (id)objectWithCellClass:(Class)cellClass;

@property (nonatomic, strong) id userInfo;
@property (nonatomic, strong) id entityObject;
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, assign) UIInterfaceOrientation orientation;

@end

/**
 * An object that can be used to populate information in the cell.
 *
 * @fn UXTableViewCellObject::userInfo
 */

// Private classes for use in UXFramework.
@interface UXTableViewCellObject ()

// A property to change the cell class of this cell object.
@property(nonatomic, assign) Class cellClass;

@end

