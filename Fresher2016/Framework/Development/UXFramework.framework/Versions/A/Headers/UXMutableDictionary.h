/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@interface UXMutableDictionary : NSMutableDictionary

@end
