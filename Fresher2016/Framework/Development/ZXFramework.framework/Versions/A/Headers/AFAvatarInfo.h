/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@interface AFAvatarInfo : NSObject

@property (nonatomic, strong) NSString* text;
@property (nonatomic, assign) NSUInteger diameter;
@property (nonatomic, strong) UIFont* styleFont;
@property (nonatomic, strong) UIColor* textColor;
@property (nonatomic, strong) UIColor* backgroundColor;

@end
