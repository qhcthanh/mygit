//
//  AppDelegate.h
//  Task1
//
//  Created by VanDao on 5/31/16.
//  Copyright © 2016 VanDao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

