/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UXPreprocessor.h"

/**
 * Released on July 11, 2008
 */
#define UXIOS_2_0     20000

/**
 * Released on September 9, 2008
 */
#define UXIOS_2_1     20100

/**
 * Released on November 21, 2008
 */
#define UXIOS_2_2     20200

/**
 * Released on June 17, 2009
 */
#define UXIOS_3_0     30000

/**
 * Released on September 9, 2009
 */
#define UXIOS_3_1     30100

/**
 * Released on April 3, 2010
 */
#define UXIOS_3_2     30200

/**
 * Released on June 21, 2010
 */
#define UXIOS_4_0     40000

/**
 * Released on September 8, 2010
 */
#define UXIOS_4_1     40100

/**
 * Released on November 22, 2010
 */
#define UXIOS_4_2     40200

/**
 * Released on March 9, 2011
 */
#define UXIOS_4_3     40300

/**
 * Released on October 12, 2011.
 */
#define UXIOS_5_0     50000

/**
 * Released on March 7, 2012.
 */
#define UXIOS_5_1     50100

/**
 * Released on September 19, 2012.
 */
#define UXIOS_6_0     60000

/**
 * Released on January 28, 2013.
 */
#define UXIOS_6_1     60100

/**
 * Released on September 18, 2013
 */
#define UXIOS_7_0     70000

#ifndef kCFCoreFoundationVersionNumber_iPhoneOS_2_0
#define kCFCoreFoundationVersionNumber_iPhoneOS_2_0 478.23
#endif

#ifndef kCFCoreFoundationVersionNumber_iPhoneOS_2_1
#define kCFCoreFoundationVersionNumber_iPhoneOS_2_1 478.26
#endif

#ifndef kCFCoreFoundationVersionNumber_iPhoneOS_2_2
#define kCFCoreFoundationVersionNumber_iPhoneOS_2_2 478.29
#endif

#ifndef kCFCoreFoundationVersionNumber_iPhoneOS_3_0
#define kCFCoreFoundationVersionNumber_iPhoneOS_3_0 478.47
#endif

#ifndef kCFCoreFoundationVersionNumber_iPhoneOS_3_1
#define kCFCoreFoundationVersionNumber_iPhoneOS_3_1 478.52
#endif

#ifndef kCFCoreFoundationVersionNumber_iPhoneOS_3_2
#define kCFCoreFoundationVersionNumber_iPhoneOS_3_2 478.61
#endif

#ifndef kCFCoreFoundationVersionNumber_iOS_4_0
#define kCFCoreFoundationVersionNumber_iOS_4_0 550.32
#endif

#ifndef kCFCoreFoundationVersionNumber_iOS_4_1
#define kCFCoreFoundationVersionNumber_iOS_4_1 550.38
#endif

#ifndef kCFCoreFoundationVersionNumber_iOS_4_2
#define kCFCoreFoundationVersionNumber_iOS_4_2 550.52
#endif

#ifndef kCFCoreFoundationVersionNumber_iOS_4_3
#define kCFCoreFoundationVersionNumber_iOS_4_3 550.52
#endif

#ifndef kCFCoreFoundationVersionNumber_iOS_5_0
#define kCFCoreFoundationVersionNumber_iOS_5_0 675.00
#endif

#ifndef kCFCoreFoundationVersionNumber_iOS_5_1
#define kCFCoreFoundationVersionNumber_iOS_5_1 690.10
#endif

#ifndef kCFCoreFoundationVersionNumber_iOS_6_0
#define kCFCoreFoundationVersionNumber_iOS_6_0 793.00
#endif

#ifndef kCFCoreFoundationVersionNumber_iOS_6_1
#define kCFCoreFoundationVersionNumber_iOS_6_1 793.00
#endif

#if defined(__cplusplus)
extern "C" {
#endif
    
    /**
     * Checks whether the device the app is currently running on is an iPad or not.
     *
     * @returns YES if the device is an iPad.
     */
    BOOL UXIsPad(void);
    
    /**
     * Checks whether the device the app is currently running on is an
     * iPhone/iPod touch or not.
     *
     * @returns YES if the device is an iPhone or iPod touch.
     */
    BOOL UXIsPhone(void);
    
    /**
     * Checks whether the device supports tint colors on all UIViews.
     *
     * @returns YES if all UIView instances on the device respond to tintColor.
     */
    BOOL UXIsTintColorGloballySupported(void);
    
    /**
     * Checks whether the device's OS version is at least the given version number.
     *
     * Useful for runtime checks of the device's version number.
     *
     * @param versionNumber  Any value of kCFCoreFoundationVersionNumber.
     *
     * @attention Apple recommends using respondsToSelector where possible to check for
     *                 feature support. Use this method as a last resort.
     */
    BOOL UXDeviceOSVersionIsAtLeast(double versionNumber);
    
    /**
     * Fetch the screen's scale.
     */
    CGFloat UXScreenScale(void);
    
    /**
     * Returns YES if the screen is a retina display, NO otherwise.
     */
    BOOL UXIsRetina(void);
    
    /**
     * Return YES if OS version is iOS7
     */
    BOOL UXIsSupportiOS7(void);
    
    /**
     * Return YES if OS version is iOS8
     */
    BOOL UXIsSupportiOS8(void);
    
    /**
     * Return YES if OS version is iOS9
     */
    BOOL UXIsSupportiOS9(void);
    
    /**
     * Return YES if OS version is beta
     */
    BOOL UXIsSupportiOSBeta(void);
    
    /**
     * Return YES if Device is iPhone 6 || iPhone 6 Plus
     */
    BOOL UXIsHighResolution();
    
    /**
     * Return height of separator that depend on screen scale
     */
    CGFloat UXSeparatorHeight();
    
    /**
     * This method is now deprecated. Use [UIPopoverController class] instead.
     *
     * MAINTENANCE: Remove by Feb 28, 2014.
     */
    Class UXUIPopoverControllerClass(void) __UX_DEPRECATED_METHOD;
    
    /**
     * This method is now deprecated. Use [UITapGestureRecognizer class] instead.
     *
     * MAINTENANCE: Remove by Feb 28, 2014.
     */
    Class UXUITapGestureRecognizerClass(void) __UX_DEPRECATED_METHOD;
    
#if defined(__cplusplus)
} // extern "C"
#endif

#pragma mark Building with Old SDKs

// Define methods that were introduced in iOS 7.0.
#if __IPHONE_OS_VERSION_MAX_ALLOWED < UXIOS_7_0

@interface UIViewController (UX7SDKAvailability)

@property (nonatomic, assign) UIRectEdge edgesForExtendedLayout;
- (void)setNeedsStatusBarAppearanceUpdate;

@end

#endif

