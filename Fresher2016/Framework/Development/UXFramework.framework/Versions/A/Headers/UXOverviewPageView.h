/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#if defined(DEBUG) || defined(UX_DEBUG)

#import "UXOverviewGraphView.h"

@class UXMemoryCache;

/**
 * A page in the Overview.
 *
 * @ingroup Overview-Pages
 */
@interface UXOverviewPageView : UIView

#pragma mark Creating a Page /** @name Creating a Page */

/**
 * Returns an autoreleased instance of this view.
 */
+ (UXOverviewPageView *)page;


#pragma mark Updating a Page /** @name Updating a Page */

/**
 * Request that this page update its information.
 *
 * Should be implemented by the subclass. The default implementation does nothing.
 */
- (void)update;


#pragma mark Configuring a Page /** @name Configuring a Page */

/**
 * The title of the page.
 */
@property (nonatomic, copy) NSString* pageTitle;


/**
 * The following methods are provided to aid in subclassing and are not meant to be
 * used externally.
 */
#pragma mark Subclassing /** @name Subclassing */

/**
 * The title label for this page.
 *
 * By default this label will be placed flush to the bottom middle of the page.
 */
@property (nonatomic, readonly, strong) UILabel* titleLabel;

/**
 * Creates a generic label for use in the page.
 */
- (UILabel *)label;

@end


/**
 * A page that renders a graph and two labels.
 *
 * @ingroup Overview-Pages
 */
@interface UXOverviewGraphPageView : UXOverviewPageView <
UXOverviewGraphViewDataSource
> {
@private
    UILabel* _label1;
    UILabel* _label2;
    UXOverviewGraphView* _graphView;
    NSEnumerator* _eventEnumerator;
}

@property (nonatomic, readonly, strong) UILabel* label1;
@property (nonatomic, readonly, strong) UILabel* label2;
@property (nonatomic, readonly, strong) UXOverviewGraphView* graphView;

@end


/**
 * A page that renders a graph showing free memory.
 *
 * @image html overview-memory1.png "The memory page."
 *
 * @ingroup Overview-Pages
 */
@interface UXOverviewMemoryPageView : UXOverviewGraphPageView {
@private
    NSEnumerator* _enumerator;
    unsigned long long _minMemory;
}

@end


/**
 * A page that renders a graph showing free disk space.
 *
 * @image html overview-disk1.png "The disk page."
 *
 * @ingroup Overview-Pages
 */
@interface UXOverviewDiskPageView : UXOverviewGraphPageView {
@private
    NSEnumerator* _enumerator;
    unsigned long long _minDiskUse;
}

@end


/**
 * A page that shows all of the logs sent to the console.
 *
 * @image html overview-log1.png "The log page."
 *
 * @ingroup Overview-Pages
 */
@interface UXOverviewConsoleLogPageView : UXOverviewPageView {
@private
    UIScrollView* _logScrollView;
    UILabel* _logLabel;
}

@end


/**
 * A page that allows you to modify NIMaxLogLevel.
 *
 * @image html overview-maxloglevel1.png "The max log level page."
 *
 * @ingroup Overview-Pages
 */
@interface UXOverviewMaxLogLevelPageView : UXOverviewPageView {
@private
    UISlider* _logLevelSlider;
    UILabel* _errorLogLevelLabel;
    UILabel* _warningLogLevelLabel;
    UILabel* _infoLogLevelLabel;
}

@end


/**
 * A page that shows information regarding an in-memory cache.
 *
 * @ingroup Overview-Pages
 */
@interface UXOverviewMemoryCachePageView : UXOverviewGraphPageView

/**
 * Returns an autoreleased instance of this page with the given cache.
 */
+ (id)pageWithCache:(UXMemoryCache *)cache;

@property (nonatomic, strong) UXMemoryCache* cache;
@end


/**
 * A page that adds run-time inspection features.
 *
 * @ingroup Overview-Pages
 */
@interface UXInspectionOverviewPageView : UXOverviewPageView
@end


#endif

