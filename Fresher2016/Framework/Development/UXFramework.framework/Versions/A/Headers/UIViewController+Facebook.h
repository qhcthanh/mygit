/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <UIKit/UIKit.h>

/** How to use push back animation
 -(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self animationPopFrontScaleUp];
 }
 
 PresentViewController* viewController = [[PresentViewController alloc] init];
 UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
 [self presentViewController:navigationController animated:YES completion:nil];
 
 /////////////////////////////////////////////////////
 // Call the method where you want to put animation.
 /////////////////////////////////////////////////////
 [self animationPushBackScaleDown];
 
 // Dismiss model view controller
 [self dismissViewControllerAnimated:YES completion:nil];
 **/

@interface UIViewController (Facebook)

@property (assign, nonatomic) BOOL pushBackState;

// Public methods
-(void) animationPopFront;
-(void) animationPushBack;

-(void) animationPopFrontScaleUp;
-(void) animationPushBackScaleDown;

-(void) animationPushBackLikeGmail;
-(void) animationPopFrontLikeGmail;

@end
