/* Copyright (c) 2015-Zalo, VNG Corp.
 * Forked from Alexey Belkevich, 2014
 * Updated by ThemND VNG Coporation, 2015
 * Change Log: Integrate Contact Framework that supports the iOS9
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#if __IPHONE_9_0
    #import <Contacts/Contacts.h>
#endif

typedef NS_ENUM(NSUInteger, UXSocialNetworkType)
{
    UXSocialNetworkUnknown = 0,
    UXSocialNetworkFacebook = 1,
    UXSocialNetworkTwitter = 2,
    UXSocialNetworkLinkedIn = 3,
    UXSocialNetworkFlickr = 4,
    UXSocialNetworkGameCenter = 5
};

@interface UXSocialProfile : NSObject

@property (nonatomic, readonly) UXSocialNetworkType socialNetwork;
@property (nonatomic, readonly) NSString *username;
@property (nonatomic, readonly) NSString *userIdentifier;
@property (nonatomic, readonly) NSURL *url;

/**
 *  Initialize a social profile object
 *
 *  @param dictionary The dictionary contains social profile's information
 *
 *  @return A social profile object
 */
- (instancetype)initWithSocialDictionary:(NSDictionary *)dictionary;

#if __IPHONE_9_0
/**
 *  Initialize a social profile object
 *
 *  @param profile The native social profile that provides information
 *
 *  @return A social profile object
 */
- (instancetype)initWithSocialProfile:(CNSocialProfile*)profile;
#endif

@end
