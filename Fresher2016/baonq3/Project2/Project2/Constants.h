//
//  Constants.h
//  Project2
//
//  Created by BaoNQ on 5/19/16.
//  Copyright © 2016 admin. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define IS_OS_9_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)
#define DOMAIN_STRING @"com.vng.ZaloContact"

#endif /* Constants_h */
