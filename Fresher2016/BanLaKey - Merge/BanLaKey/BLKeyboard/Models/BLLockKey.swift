//
//  BLLockKey.swift
//  BanLaKey
//
//  Created by BaoNQ on 8/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

enum BLLockKeyType : Int {
    case Once = 0 // The key is locked until next key pressed
    case Forever  // The key is locked until it is pressed again
    case Default // The key is not locked
}

class BLLockKey: BLKey {

    override init(frame: CGRect, title: String, image: UIImage? = nil, keyType: BLKeyType = .CharacterKey, backgroundColor: UIColor = UIColor.whiteColor()) {
        
        super.init(frame: frame, title: title, image: image, keyType: keyType, backgroundColor: backgroundColor)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var lockState: BLLockKeyType = BLLockKeyType.Default
    
}
