/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

@interface NSString (Extension)

- (NSString*)stringWithoutAccents;

/**
 * Check string contains another substring
 */
- (BOOL)containsString:(NSString*)substring;

/**
 * Encode an URL string
 */
- (NSString*)encodeURLString;

/**
 * Parses a URL query string into a dictionary.
 */
- (NSDictionary*)queryDictionaryUsingEncoding:(NSStringEncoding)encoding;

/**
 * Parses a URL, adds query parameters to its query, and re-encodes it as a new URL.
 */
- (NSString*)stringByAddingQueryDictionary:(NSDictionary*)query;

/**
 * Compares two strings expressing software versions.
 *
 * The comparison is (except for the development version provisions noted below) lexicographic
 * string comparison. So as long as the strings being compared use consistent version formats,
 * a variety of schemes are supported. For example "3.02" < "3.03" and "3.0.2" < "3.0.3". If you
 * mix such schemes, like trying to compare "3.02" and "3.0.3", the result may not be what you
 * expect.
 *
 * Development versions are also supported by adding an "a" character and more version info after
 * it. For example "3.0a1" or "3.01a4". The way these are handled is as follows: if the parts
 * before the "a" are different, the parts after the "a" are ignored. If the parts before the "a"
 * are identical, the result of the comparison is the result of NUMERICALLY comparing the parts
 * after the "a". If the part after the "a" is empty, it is treated as if it were "0". If one
 * string has an "a" and the other does not (e.g. "3.0" and "3.0a1") the one without the "a"
 * is newer.
 */
- (NSComparisonResult)versionStringCompare:(NSString *)other;

/**
 *  Get dynamic size of text
 *
 *  @param styledFont The styled font of the text
 *  @param size  The size of control should be rendered. By default, the size is set CGSizeMake([UIScreen mainScreen].size.width, CGFLOAT_MAX)
 *
 *  @return Size of text
 */
- (CGSize)sizeOfStringWithStyledFont:(UIFont*)styledFont withSize:(CGSize)size;

/**
 *  Get dynamic size of text with lineBreakMode
 *
 *  @param styledFont    The styled font of the text
 *  @param sizeWidth     The size of control should be rendered. By default, the size is set CGSizeMake([UIScreen mainScreen].size.width, CGFLOAT_MAX)
 *  @param lineBreakMode The lineBreakMode of the text
 *
 *  @return Size of text
 */
- (CGSize)sizeOfStringWithStyledFont:(UIFont*)styledFont withSize:(CGSize)size withLineBreakMode:(NSLineBreakMode)lineBreakMode;

/**
 *  Get dynamic size of text based on drawing option
 *
 *  @param styledFont The styled font of the text
 *  @param sizeWidth  The size of control should be rendered. By default, the size is set CGSizeMake([UIScreen mainScreen].size.width, CGFLOAT_MAX)
 *  @param options    The drawing options of the text
 *
 *  @return Size of text
 */
- (CGSize)sizeOfStringWithStyledFont:(UIFont*)styledFont withSize:(CGSize)size withOptions:(NSStringDrawingOptions)options;

@end
