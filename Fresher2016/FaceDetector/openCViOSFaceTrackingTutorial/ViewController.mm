//
//  ViewController.m
//  openCViOSFaceTrackingTutorial
//
//  Created by Evangelos Georgiou on 16/03/2013.
//  Copyright (c) 2013 Evangelos Georgiou. All rights reserved.
//

#import "ViewController.h"
#include <sys/sysctl.h>
#import <sys/utsname.h>

NSString* const faceCascadeFilename = @"lbpcascade_frontalface";
NSString* const eyes_cascade_name = @"haarcascade_eye_tree_eyeglasses";

const int HaarOptions = CV_HAAR_DO_ROUGH_SEARCH;

@interface ViewController ()

@property UIImage* maskImage;
@property Mat maskMat;
@property CGSize cameraResolutionSize;
@property CGSize minFaceRect;
@property CGSize maxFaceRect;

@end

@implementation ViewController

@synthesize videoCamera;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    self.videoCamera = [[CvVideoCamera alloc] initWithParentView:imageView];
    self.videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionFront;
    self.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPresetMedium;
    self.videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    self.videoCamera.defaultFPS = 50;
    self.videoCamera.grayscaleMode = NO;
    self.videoCamera.delegate = self;
    
    NSString* faceCascadePath = [[NSBundle mainBundle] pathForResource:faceCascadeFilename ofType:@"xml"];
    NSString* eyesCascadePath = [[NSBundle mainBundle] pathForResource:eyes_cascade_name ofType:@"xml"];
    
    faceCascade.load([faceCascadePath UTF8String]);
    eyesCascade.load([eyesCascadePath UTF8String]);
    
    // 1 load
    // cover data
    // data -> Mat
    
    
    //
    _maskImage = [UIImage imageNamed:@"sticker.png"];
    UIImageToMat(_maskImage, _maskMat, true);
    
    cvtColor(_maskMat, _maskMat, CV_RGBA2BGRA, 4);
    
    // Load Resolution Size
    NSDictionary* dic = [ViewController cameraResolutions];
    NSString* frontResolutionString = [dic valueForKey:@"front"];
    NSArray* frontResolutionArray = [frontResolutionString componentsSeparatedByString:@","];
    
    NSString* currentResolution = [frontResolutionArray objectAtIndex: [self getNumberPreset:self.videoCamera.defaultAVCaptureSessionPreset]];
    NSArray* resolutionSizeCamera = [currentResolution componentsSeparatedByString:@"x"];
    
    _cameraResolutionSize = CGSizeMake([resolutionSizeCamera[1] floatValue], [resolutionSizeCamera[0] floatValue]);
    _minFaceRect = CGSizeMake(_cameraResolutionSize.width * 0.2, _cameraResolutionSize.height * 0.25);
    _maxFaceRect = CGSizeMake(_cameraResolutionSize.width * 0.8, _cameraResolutionSize.height * 0.8);
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Protocol CvVideoCameraDelegate

#ifdef __cplusplus
- (void)processImage:(Mat&)image;
{
    Mat grayscaleFrame = image;
//    cvtColor(image, grayscaleFrame, CV_BGR2GRAY);
//    equalizeHist(grayscaleFrame, grayscaleFrame);
    
    
   // std::cout << cv::getBuildInformation() << std::endl;
    std::vector<cv::Rect> faces;
    //faceCascade.detectMultiScale(grayscaleFrame, faces, 1.1, 2, HaarOptions,cv::Size(_minFaceRect.width, _minFaceRect.height), cv::Size(_maxFaceRect.width, _maxFaceRect.height));
    faceCascade.detectMultiScale(grayscaleFrame, faces, 1.1, 2, HaarOptions,cv::Size(_minFaceRect.width, _minFaceRect.height));
    
//    NSLog(@"%i", _maskMat.channels());
    
    
    for (int i = 0; i < faces.size(); i++)
    {
        //cv::Mat result;
        cv::Point pt1(faces[i].x + faces[i].width, faces[i].y + faces[i].height);
        cv::Point pt2(faces[i].x, faces[i].y);

 //       resize(_maskMat,_maskMat,cv::Size(faces[i].width, faces[i].height));//resize image
 //       overlayImage(image,_maskMat, image, cv::Point((pt1.x + pt2.x)/2 - _maskMat.cols/2, (pt2.y + pt1.y)/2 - _maskMat.rows/2 ));
        
//        cv::rectangle(image, pt1, pt2, cvScalar(0, 255, 0, 0), 1, 8 ,0);
        
        
        // Eye
        Mat faceROI = grayscaleFrame( faces[i] );
        std::vector<cv::Rect> eyes;
        
        //-- In each face, detect eyes
        eyesCascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 |CV_HAAR_SCALE_IMAGE, cv::Size(60,60) );
        
        for( size_t j = 0; j < eyes.size(); j++ )
        {
            cv::Point center( faces[i].x + eyes[j].x + eyes[j].width*0.5, faces[i].y + eyes[j].y + eyes[j].height*0.5 );
            int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
           // circle( image, center, radius, Scalar( 255, 0, 0 ), 4, 8, 0 );
            resize(_maskMat,_maskMat,cv::Size(radius, radius));//resize image
            overlayImage(image,_maskMat, image, cv::Point(center.x - _maskMat.cols/2, center.y - _maskMat.rows/2) );
        }
        
    }
}

- (cv::Mat)cvMatFromUIImage:(UIImage *)image
{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels (color channels + alpha)
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    return cvMat;
}


void overlayImage(const cv::Mat &background, const cv::Mat &foreground,
                  cv::Mat &output, cv::Point2i location)
{
    background.copyTo(output);
    
    
    // start at the row indicated by location, or at row 0 if location.y is negative.
    for(int y = std::max(location.y , 0); y < background.rows; ++y)
    {
        int fY = y - location.y; // because of the translation
        
        // we are done of we have processed all rows of the foreground image.
        if(fY >= foreground.rows)
            break;
        
        // start at the column indicated by location,
        
        // or at column 0 if location.x is negative.
        for(int x = std::max(location.x, 0); x < background.cols; ++x)
        {
            int fX = x - location.x; // because of the translation.
            
            // we are done with this row if the column is outside of the foreground image.
            if(fX >= foreground.cols)
                break;
            
            // determine the opacity of the foregrond pixel, using its fourth (alpha) channel.
            double opacity = ((double)foreground.data[fY * foreground.step + fX * foreground.channels() + 3]) / 255.;
            
            
            // and now combine the background and foreground pixel, using the opacity,
            
            // but only if opacity > 0.
            for(int c = 0; opacity > 0 && c < output.channels(); ++c)
            {
                unsigned char foregroundPx = foreground.data[fY * foreground.step + fX * foreground.channels() + c];
                unsigned char backgroundPx = background.data[y * background.step + x * background.channels() + c];
                output.data[y*output.step + output.channels()*x + c] = backgroundPx * (1.-opacity) + foregroundPx * opacity;
            }
        }
    }
}

void UIImageToMat(const UIImage* image,
                  cv::Mat& m, bool alphaExist) {
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width, rows = image.size.height;
    CGContextRef contextRef;
    CGBitmapInfo bitmapInfo = kCGImageAlphaPremultipliedLast;
    
    if (CGColorSpaceGetModel(colorSpace) == 0)
    {
        m.create(rows, cols, CV_8UC1); // 8 bits per component, 1 channel
        bitmapInfo = kCGImageAlphaNone;
        if (!alphaExist)
            bitmapInfo = kCGImageAlphaNone;
        contextRef = CGBitmapContextCreate(m.data, m.cols, m.rows, 8,
                                           m.step[0], colorSpace,
                                           bitmapInfo);
    }
    else
    {
        m.create(rows, cols, CV_8UC4); // 8 bits per component, 4 channels
        if (!alphaExist)
            bitmapInfo = kCGImageAlphaNoneSkipLast |
            kCGBitmapByteOrderDefault;
        contextRef = CGBitmapContextCreate(m.data, m.cols, m.rows, 8,
                                           m.step[0], colorSpace,
                                           bitmapInfo);
    }
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows),
                       image.CGImage);
    CGContextRelease(contextRef);
}

#endif

#pragma mark - UI Actions

- (IBAction)startCamera:(id)sender
{
    [self.videoCamera start];
}

- (IBAction)stopCamera:(id)sender
{
    [self.videoCamera stop];
}

//+ (NSString *)getModel {
//    size_t size;
//    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
//    char* model = malloc(size);
//    sysctlbyname("hw.machine", model, &size, NULL, 0);
//    NSString *deviceModel = [NSString stringWithCString:model encoding:NSUTF8StringEncoding];
//    free(model);
//    return deviceModel;
//}

+ (NSString *)ntnu_deviceDescription
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
}

+ (NSDictionary*) cameraResolutions {
    NSString* deviceModel = [self ntnu_deviceDescription];
    
    // iPhone 4S
    if ([deviceModel isEqualToString:@"iPhone4,1"]) {
        
        NSString* lFrontCam = @"640x480,480x360,192x144";
        NSString* lBackCam = @"3264x2448,1920x1080,1280x720,640x480,480x360,192x144";
        NSDictionary* lCamResolutions = @{@"front":lFrontCam,
                                          @"back":lBackCam};
        return lCamResolutions;
    }
    // iPhone 5/5C/5S/6/6+/iPod 6
    else if ([deviceModel isEqualToString:@"iPhone5,1"]
             || [deviceModel isEqualToString:@"iPhone5,2"]
             || [deviceModel isEqualToString:@"iPhone5,3"]
             || [deviceModel isEqualToString:@"iPhone5,4"]
             || [deviceModel isEqualToString:@"iPhone6,1"]
             || [deviceModel isEqualToString:@"iPhone6,2"]
             || [deviceModel isEqualToString:@"iPhone7,1"]
             || [deviceModel isEqualToString:@"iPhone7,2"]
             || [deviceModel isEqualToString:@"iPod7,1"]) {
        
        NSString* lFrontCam = @"1280x960,1280x720,640x480,480x360,192x144";
        NSString* lBackCam = @"3264x2448,1920x1080,1280x720,640x480,480x360,192x144";
        NSDictionary* lCamResolutions = @{@"front":lFrontCam,
                                          @"back":lBackCam};
        return lCamResolutions;
    }
    // iPhone 6S/6S+
    else if ([deviceModel isEqualToString:@"iPhone8,1"]
             || [deviceModel isEqualToString:@"iPhone8,2"]) {
        NSString* lFrontCam = @"1280x960,1280x720,640x480,480x360,192x144";
        NSString* lBackCam = @"4032x3024,1920x1080,1280x720,640x480,480x360,192x144";
        NSDictionary* lCamResolutions = @{@"front":lFrontCam,
                                          @"back":lBackCam};
        return lCamResolutions;
    }
    // iPad 2
    else if ([deviceModel isEqualToString:@"iPad2,1"]
             || [deviceModel isEqualToString:@"iPad2,2"]
             || [deviceModel isEqualToString:@"iPad2,3"]
             || [deviceModel isEqualToString:@"iPad2,4"]) {
        
        
        NSString* lFrontCam = @"640x480,480x360,192x144";
        NSString* lBackCam = @"1280x720,960x720,480x360,192x144,640x480";
        NSDictionary* lCamResolutions = @{@"front":lFrontCam,
                                          @"back":lBackCam};
        return lCamResolutions;
    }
    // iPad 3
    else if ([deviceModel isEqualToString:@"iPad3,1"]
             || [deviceModel isEqualToString:@"iPad3,2"]
             || [deviceModel isEqualToString:@"iPad3,3"]) {
        
        
        NSString* lFrontCam = @"640x480,480x360,192x144";
        NSString* lBackCam = @"2592x1936,1920x1080,1280x720,640x480,480x360,192x144";
        NSDictionary* lCamResolutions = @{@"front":lFrontCam,
                                          @"back":lBackCam};
        return lCamResolutions;
    }
    // iPad 4/Air/Mini/Mini 2/Mini 3/iPod 5G
    else if ([deviceModel isEqualToString:@"iPad3,4"]
             || [deviceModel isEqualToString:@"iPad3,5"]
             || [deviceModel isEqualToString:@"iPad3,6"]
             || [deviceModel isEqualToString:@"iPad4,1"]
             || [deviceModel isEqualToString:@"iPad4,2"]
             || [deviceModel isEqualToString:@"iPad4,3"]
             || [deviceModel isEqualToString:@"iPad4,4"]
             || [deviceModel isEqualToString:@"iPad4,5"]
             || [deviceModel isEqualToString:@"iPad4,6"]
             || [deviceModel isEqualToString:@"iPad4,7"]
             || [deviceModel isEqualToString:@"iPad4,8"]
             || [deviceModel isEqualToString:@"iPod5,1"]) {
        
        NSString* lFrontCam = @"1280x960,1280x720,640x480,480x360,192x144";
        NSString* lBackCam = @"2592x1936,1920x1080,1280x720,640x480,480x360,192x144";
        NSDictionary* lCamResolutions = @{@"front":lFrontCam,
                                          @"back":lBackCam};
        return lCamResolutions;
    }
    // iPad Air 2/Mini 4/Pro
    else if ([deviceModel isEqualToString:@"iPad5,3"]
             || [deviceModel isEqualToString:@"iPad5,4"]) {
        NSString* lFrontCam = @"1280x960,1280x720,640x480,480x360,192x144";
        NSString* lBackCam = @"3264x2448,1920x1080,1280x720,640x480,480x360,192x144";
        NSDictionary* lCamResolutions = @{@"front":lFrontCam,
                                          @"back":lBackCam};
        return lCamResolutions;
    }
    return nil;
}

- (NSInteger)getNumberPreset:(NSString*)preset {
    
    if ([preset isEqualToString:@"AVCaptureSessionPresetPhoto"]) {
        return 0;
    } else if ([preset isEqualToString:@"AVCaptureSessionPresetHigh"]) {
        return 1;
    } else if ([preset isEqualToString:@"AVCaptureSessionPresetMedium"]) {
        return 2;
    } else if ([preset isEqualToString:@"AVCaptureSessionPresetLow"]) {
        return 3;
    } else if ([preset isEqualToString:@"AVCaptureSessionPreset640x480"]) {
        return 4;
    } else if ([preset isEqualToString:@"AVCaptureSessionPreset1280x720"]) {
        return 5;
    } else if ([preset isEqualToString:@"AVCaptureSessionPreset1920x1080"]) {
        return 6;
    }
    return 0;
}

@end
