/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXDefaultStyleSheet.h"

@interface UXDefaultStyleSheet (UXNavigation)

@property (nonatomic, readonly) UIImage* navigationBackImageNormal;
@property (nonatomic, readonly) UIImage* navigationBackImageSelected;

@property (nonatomic, readonly) UIImage* navigationMenuImageNormal;
@property (nonatomic, readonly) UIImage* navigationMenuImageSelected;

@property (nonatomic, readonly) UIImage* navigationCheckmarkImageNormal;
@property (nonatomic, readonly) UIImage* navigationCheckmarkImageSelected;

@property (nonatomic, readonly) UIImage* navigationBarBackgroundImage;
@property (nonatomic, readonly) UIImage* applicationBackgroundImage;

@property (nonatomic, readonly) UIImage* searchBarBackgroundImage;
@property (nonatomic, readonly) UIImage* searchBoxBackgroundImage;
@property (nonatomic, readonly) UIImage* searchCancelButtonImageNormal;
@property (nonatomic, readonly) UIImage* searchCancelButtonImageSelected;

@property (nonatomic, readonly) UIImage* tabbarBackgroundImageNormal;
@property (nonatomic, readonly) UIImage* tabbarBackgroundImageSelected;

@property (nonatomic, readonly) UIImage* searchBarIconImage;
@property (nonatomic, readonly) UIImage* cancelBarIconImageNormal;
@property (nonatomic, readonly) UIImage* cancelBarIconImageSelected;

@property (nonatomic, readonly) UIImage* tableArrowImageNormal;

@end
