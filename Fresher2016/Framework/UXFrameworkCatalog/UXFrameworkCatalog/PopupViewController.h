//
//  PopupViewController.h
//  UXFrameworkCatalog
//
//  Created by VanDao on 6/29/16.
//  Copyright © 2016 VNG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopoverView.h"

@interface PopupViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
