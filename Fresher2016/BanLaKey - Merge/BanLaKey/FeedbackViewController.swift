//
//  FeedbackViewController.swift
//  BanLaKey
//
//  Created by qhcthanh on 9/8/16.
//  Copyright © 2016 VNG Corp. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController {

    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var contentTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var topIconImageViewConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.sendButton.addTarget(self, action: #selector(onTouchUpInsideSendButton), forControlEvents: .TouchUpInside)
        
        let endEditingGesture = UITapGestureRecognizer(target: self, action: #selector(endEditing))
        self.view.addGestureRecognizer(endEditingGesture)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide), name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow), name: UIKeyboardWillShowNotification, object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "Góp ý và đánh giá"
        self.navigationController?.navigationBarHidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.endEditing()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }

    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    func keyboardWillShow(notification: NSNotification) {
        
        let heightKeyboard = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue().height
        
        let delta = (self.sendButton.frame.origin.y + self.sendButton.frame.height) - (self.view.frame.height - heightKeyboard)
        
        if delta > 0 {
            
            self.topIconImageViewConstraint.constant -= delta
            
            UIView.animateWithDuration(notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        self.topIconImageViewConstraint.constant = 10
        
        UIView.animateWithDuration(notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval, animations: {
            self.view.layoutIfNeeded()
        })
        
    }
    
    
    func endEditing() {
        self.view.endEditing(true)
    }
    
    func onTouchUpInsideSendButton(sender: UIButton!) {
        
    }
    
    @IBAction func dismissViewController() {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FeedbackViewController: UITextFieldDelegate {
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if self.nameTextField.text != "" && self.contentTextField.text != "" {
            self.sendButton.enabled = true
        } else {
            self.sendButton.enabled = false
        }
        
        return true
    }
    
}






