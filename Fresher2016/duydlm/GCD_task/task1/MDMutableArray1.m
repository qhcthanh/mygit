//
//  MDMutableArray1.m
//  task1
//
//  Created by manhduydl on 5/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MDMutableArray1.h"

@interface MDMutableArray1() {
    NSMutableArray *internalMutableArray;
    dispatch_queue_t concurentQueue;
}
@end

@implementation MDMutableArray1

- (id)init {
    if (self = [super init]) {
        internalMutableArray = [NSMutableArray new];
        concurentQueue = dispatch_queue_create("com.duy.concurentQueue", DISPATCH_QUEUE_CONCURRENT);
    }
    return self;
}

-(NSUInteger)count {
    __block NSUInteger count;
    dispatch_sync(concurentQueue, ^{
        count = [internalMutableArray count];
    });
    return count;
}

//override method to support thread-safe use dispatch_barrier_async
- (id)objectAtIndex:(NSUInteger)index {
    __block id obj;
    dispatch_sync(concurentQueue, ^{
        obj = [internalMutableArray objectAtIndex:index];
    });
    return obj;
}

//override method to support thread-safe use dispatch_barrier_async
- (void)addObject:(id)anObject {
    dispatch_barrier_async(concurentQueue, ^{
        [internalMutableArray addObject:anObject];
    });
}

//override method to support thread-safe use dispatch_barrier_async
- (void)insertObject:(id)anObject atIndex:(NSUInteger)index {
    dispatch_barrier_async(concurentQueue, ^{
        [internalMutableArray insertObject:anObject atIndex:index];
    });
}

//override method to support thread-safe use dispatch_barrier_async
- (void)removeObjectAtIndex:(NSUInteger)index {
    dispatch_barrier_async(concurentQueue, ^{
        [internalMutableArray removeObjectAtIndex:index];
    });
}

- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject {
    dispatch_barrier_async(concurentQueue, ^{
        [internalMutableArray replaceObjectAtIndex:index withObject:anObject];
    });
}

@end
