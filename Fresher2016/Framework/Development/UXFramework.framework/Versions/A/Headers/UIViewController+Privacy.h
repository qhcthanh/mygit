/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXPrivacyDataSource.h"
#import "UXPrivacyViewController.h"

@interface UIViewController (Privacy)

/**
 *  Personal App Icon name for UXPrivacyTypeNotifications
 */
@property (nonatomic, strong) NSString *appIcon;

/**
 *  Show the privacy helper
 *
 *  @param type The type of privacy
 */
- (void)showPrivacyHelperForType:(UXPrivacyType)type;

/**
 *  Show the privacy controller with customization data
 *
 *  @param type                     The type of privacy
 *  @param controllerBlock          Customize the UXPrivacyViewController view controller
 *  @param presentCompletionHandler Customize the completion block of presentViewController:animated:completion:
 *  @param dismissCompletionhandler Customize the completion block of dismissViewControllerAnimated:completion:
 *  @param defaultSettingPane       If NO force to use UXPrivacyViewController instead of the default settings pane on iOS 8. Only for iOS 8. Default value is YES.
 */
- (void)showPrivacyControllerWithType:(UXPrivacyType)type
                           controller:(void(^)(UXPrivacyViewController *controller))controllerBlock
             presentCompletionHandler:(UXPresentCompletionHandler)presentCompletionHandler
             dismissCompletionhandler:(UXDismissCompletionHandler)dismissCompletionhandler
                useDefaultSettingPane:(BOOL)defaultSettingPane;

/**
 *  Snapshot of your Window
 *
 *  @return An UIImage of your Window
 */
- (UIImage*)snapshot;

@end
