/* Copyright (c) 2015-Zalo, VNG Corp.
 * All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "UXLocationDefines.h"

/**
 *  An abstraction around CLLocationManager that provides a block-based asynchronous API for obtaining the device's location. UXLocationManager automatically starts and stops system location services as needed to minimize battery drain.
 */
@interface UXLocationManager : NSObject

// Returns the current state of location services for this app, based on the system settings and user authorization status.
+ (UXLocationServicesState)locationServicesState;

// Returns the singleton instance of this class.
+ (UXLocationManager*)sharedUXLocationManager;

/**
 *  Asynchronously requests the current location of the device using location services.
 *
 *  @param desiredAccuracy The accuracy level desired (refers to the accuracy and recency of the location).
 *  @param timeout         The maximum amount of time (in seconds) to wait for a location with the desired accuracy before completing. If this value is 0.0, no timeout will be set (will wait indefinitely for success, unless request is force completed or canceled).
 *  @param block           The block to execute upon success, failure, or timeout.
 *  @discussion
 *      - The current location (the most recent one acquired, regardless of accuracy level), or nil if no valid location was acquired
 *      - The achieved accuracy for the current location (may be less than the desired accuracy if the request failed)
 *      - The request status (if it succeeded, or if not, why it failed)
 *
 *  @return The location request ID, which can be used to force early completion or cancel the request while it is in progress.
 */
- (UXLocationRequestID)requestLocationWithDesiredAccuracy:(UXLocationAccuracy)desiredAccuracy
                                                    timeout:(NSTimeInterval)timeout
                                                      block:(UXLocationRequestBlock)block;

/**
 *  Asynchronously requests the current location of the device using location services, optionally delaying the timeout countdown until the user has responded to the dialog requesting permission for this app to access location services.
 *
 *  @param desiredAccuracy      The accuracy level desired (refers to the accuracy and recency of the location).
 *  @param timeout              The maximum amount of time (in seconds) to wait for a location with the desired accuracy before completing. If this value is 0.0, no timeout will be set (will wait indefinitely for success, unless request is force completed or canceled).
 *  @param delayUntilAuthorized A flag specifying whether the timeout should only take effect after the user responds to the system prompt requesting permission for this app to access location services. If YES, the timeout countdown will not begin until after the app receives location services permissions. If NO, the timeout countdown begins immediately when calling this method.
 *  @param block                The block to execute upon success, failure, or timeout.
 *  @discussion
 *      As above API
 *  @return The location request ID, which can be used to force early completion or cancel the request while it is in progress.
 */
- (UXLocationRequestID)requestLocationWithDesiredAccuracy:(UXLocationAccuracy)desiredAccuracy
                                                    timeout:(NSTimeInterval)timeout
                                       delayUntilAuthorized:(BOOL)delayUntilAuthorized
                                                      block:(UXLocationRequestBlock)block;

/**
 *  Creates a subscription for location updates that will execute the block once per update indefinitely (until canceled), regardless of the accuracy of each location. This method instructs location services to use the highest accuracy available (which also requires the most power). If an error occurs, the block will execute with a status other than UXLocationStatusSuccess, and the subscription will be canceled automatically.
 *
 *  @param block The block to execute every time an updated location is available. The status will be UXLocationStatusSuccess unless an error occurred; it will never be UXLocationStatusTimedOut.
 *
 *  @return The location request ID, which can be used to cancel the subscription of location updates to this block.
 */
- (UXLocationRequestID)subscribeToLocationUpdatesWithBlock:(UXLocationRequestBlock)block;

/**
 *  Creates a subscription for location updates that will execute the block once per update indefinitely (until canceled), regardless of the accuracy of each location. The specified desired accuracy is passed along to location services, and controls how much power is used, with higher accuracies using more power. If an error occurs, the block will execute with a status other than UXLocationStatusSuccess, and the subscription will be canceled automatically.
 *
 *  @param desiredAccuracy The accuracy level desired, which controls how much power is used by the device's location services.
 *  @param block           The block to execute every time an updated location is available. Note that this block runs for every update, regardless of whether the achievedAccuracy is at least the desiredAccuracy. The status will be UXLocationStatusSuccess unless an error occurred; it will never be UXLocationStatusTimedOut.
 *
 *  @return The location request ID, which can be used to cancel the subscription of location updates to this block.
 */
- (UXLocationRequestID)subscribeToLocationUpdatesWithDesiredAccuracy:(UXLocationAccuracy)desiredAccuracy
                                                                 block:(UXLocationRequestBlock)block;
/**
 *  Creates a subscription for significant location changes that will execute the block once per change indefinitely (until canceled). If an error occurs, the block will execute with a status other than UXLocationStatusSuccess, and the subscription will be canceled automatically.
 *
 *  @param block The block to execute every time an updated location is available. The status will be UXLocationStatusSuccess unless an error occurred; it will never be UXLocationStatusTimedOut.
 *
 *  @return The location request ID, which can be used to cancel the subscription of significant location changes to this block.
 */
- (UXLocationRequestID)subscribeToSignificantLocationChangesWithBlock:(UXLocationRequestBlock)block;

/**
 *  Immediately forces completion of the location request with the given requestID (if it exists), and executes the original request block with the results. For one-time location requests, this is effectively a manual timeout, and will result in the request completing with status UXLocationStatusTimedOut.
 *
 *  @param requestID If the requestID corresponds to a subscription, then the subscription will simply be canceled.
 */

- (void)forceCompleteLocationRequest:(UXLocationRequestID)requestID;

/**
 *  Immediately cancels the location request (or subscription) with the given requestID (if it exists), without executing the original request block.
 *
 *  @param requestID The location request ID
 */
- (void)cancelLocationRequest:(UXLocationRequestID)requestID;

@end
