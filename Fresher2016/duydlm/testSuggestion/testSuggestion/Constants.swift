//
//  Constants.swift
//  testSuggestion
//
//  Created by manhduydl on 8/16/16.
//  Copyright © 2016 admin. All rights reserved.
//

import Foundation

class Constants {
    
    // MARK: List of Constants
    
    static let numberOfSuggestion = 3
    static let phrasesWillRemove = 1000
    
    static let userDefaultKey = "com.vng.banlakey.fresher1"
    
    static let letterJsonStringKey = "letterJsonStringKey"
    static let relativeLetterJsonStringKey = "relativeLetterJsonStringKey"
    static let phraseJsonStringKey = "phraseJsonStringKey"
    static let commonPhraseJsonStringKey = "commonPhraseJsonStringKey"
    
    static let letterKey = "letterKey"
    static let relativeLetterKey = "relativeLetterKey"
    static let phraseKey = "phraseKey"
    static let commonPhraseKey = "commonPhraseKey"
    
}
